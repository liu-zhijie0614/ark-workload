#!/bin/bash

swift_path=ts-swift-workload

arg_name=""
true="true"
false="false"
run_count=1
date=$(date +'%Y%m%d%H%M%S')
swift_sdk=~/tools/swift-5.7.3-RELEASE-ubuntu22.04/usr/bin
android_ndk=~/tools/android-ndk-r25c
toolspath=./toolspath.txt
Ninja_ReleaseAssert=~/apple/build/Ninja-ReleaseAssert

if [ -s "$toolspath" ]; then
    while IFS= read -r line; do
        case $line in
            *--ts-tools-path\ *)
                sdk_path="${line#*--ts-tools-path\ }"
                ;;
            *--ark_js_vm\ *)
                ark_js_vm="${line#*--ark_js_vm\ }"
                ;;
            *--es2abc\ *)
                es2abc="${line#*--es2abc\ }"
                ;;
            *--ark_aot_compiler\ *)
                ark_aot_compiler="${line#*--ark_aot_compiler\ }"
                ;;
            *--tools-type\ *)
                tools_type="${line#*--tools-type\ }"
                ;;
        esac
    done < "toolspath.txt"
fi

arg_name=""
true="true"
false="false"
run_count=1
aarch64=""
date=$(date +'%Y%m%d%H%M%S')
swift2path="swift_build/${date}"

function help_explain(){
    echo "[--help,--h]"
    echo "[--run-count <count>]"
    echo "[--case <case-name>]"
    echo "[--date <date>]"
    echo "[--build]"
    echo "[--excel]"
    echo "[--aarch64]"
    echo "[--code-v] <code_path>"
}
if [ -z "$1" ]; then
    echo "No input provided."
else
    for arg in "$@"; do
        case $arg_name in
            "--case")
                arg_name=""
                case_name=$arg
                ;;
            "--date")
                arg_name=""
                date=$arg
                ;;
            "--run-count")
                arg_name=""
                run_count=$arg
                ;;
            "--code-v")
                arg_name=""
                echo $arg
                swift_path=$arg
                swift2path="swift_build-${swift_path}/${date}"
                case $arg in
                    "0")
                    swift_path=daily
                    swift2path="swift_build-${swift_path}/${date}"
                        ;;
                    "1")
                    swift_path=ts-swift-workload
                    swift2path="swift_build/${date}"
                        ;;
                    "ts-swift-workload")
                    swift_path=ts-swift-workload
                    swift2path="swift_build/${date}"
                        ;;
                    "2")
                    swift_path=mix-case
                    swift2path="swift_build-${swift_path}/${date}"
                        ;;
                    "3")
                    swift_path=weekly_workload
                    swift2path="swift_build-${swift_path}/${date}"
                        ;;
                esac
                ;;
        esac
        case $arg in
            "--h")
                help_explain
                exit
                ;;
            "--help")
                help_explain
                exit
                ;;
            "--build")
                build="true"
                ;;
            "--case")
                arg_name=$arg
                ;;
            "--date")
                arg_name=$arg
                ;;
            "--excel")
                excel="true"
                ;;
            "--run-count")
                arg_name=$arg
                ;;
            "--aarch64")
                aarch64="true"
                ;;
            "--code-v")
                arg_name=$arg
                ;;
        esac
    done
fi

function build_swift(){
    local swift_file=$1
    local file_name=$2
    echo "build : ${swift_file} to ${swift2path}"
    if [ "${aarch64}" = "true" ]; then
        echo "swiftc -O -whole-module-optimization\
        -tools-directory ${android_ndk}/toolchains/llvm/prebuilt/linux-x86_64/bin/\
        -target aarch64-unknown-linux-android21\
        -sdk ${android_ndk}/toolchains/llvm/prebuilt/linux-x86_64/sysroot\
        -resource-dir ${Ninja_ReleaseAssert}/swift-linux-x86_64/lib/swift ${swift_file}\
        -o ${swift2path}/${file_name}"
        
        swiftc -O -whole-module-optimization\
        -tools-directory ${android_ndk}/toolchains/llvm/prebuilt/linux-x86_64/bin\
        -target aarch64-unknown-linux-android21\
        -sdk ${android_ndk}/toolchains/llvm/prebuilt/linux-x86_64/sysroot\
        -resource-dir ${Ninja_ReleaseAssert}/swift-linux-x86_64/lib/swift ${swift_file}\
        -o ${swift2path}/${file_name}
        
    else
        swiftc -O -whole-module-optimization ${swift_file} -o ${swift2path}/${file_name}
    fi
}

function run_swift(){
    local file_name=$1
    local swift_file=$swift2path/${file_name}
    echo ${file_name}
    $swift_file
}
function run(){
    if [[ "$build" = "true" ]]; then
        if [ ! -d $swift2path ];then
            mkdir -p $swift2path
        fi
        if [ -z "$case_name" ]; then
            txt_files=($(find $swift_path -type f -name "*.swift"))
            for file in "${txt_files[@]}"; do
                echo "Content of $file:"
                local swift_file_path=$file
                local file_name=$(basename "${file}" | cut -f1 -d'.')
                build_swift ${swift_file_path} ${file_name}
                
                if [[ "$excel" = "true" ]]; then
                    python swift2excel.py --case-dir $swift2path --run-case $file_name --run-count $run_count
                fi
            done
        else
            local swift_file_path=$(find ${swift_path} -name "${case_name}.swift")
            local file_name=$(basename "$swift_file_path" | cut -f1 -d'.')
            if [ -f "${swift_file_path}" ]; then
                build_swift ${swift_file_path} ${file_name}
                if [[ "$excel" = "true" ]]; then
                    python swift2excel.py --case-dir $swift2path --run-case $file_name --run-count $run_count
                fi
            fi
        fi
    fi

    if [ ! -d $swift2path ];then
        echo "${swift2path} No such file or directory"
        exit
    fi
}
run