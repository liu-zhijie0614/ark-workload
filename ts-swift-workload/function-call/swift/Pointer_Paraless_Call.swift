import Glibc
class Timer {
    private let CLOCK_REALTIME = 0
    private var start_timespec = timespec()
    private var end_timespec = timespec()
    private var time_spec = timespec()
    func start() {
        clock_gettime(Int32(CLOCK_REALTIME),&start_timespec)
    }
    
    func stop() -> Double {
        clock_gettime(Int32(CLOCK_REALTIME),&end_timespec)
        let start_time = Double(start_timespec.tv_sec * 1_000_000 + start_timespec.tv_nsec / 1_000)
        let end_time = Double(end_timespec.tv_sec * 1_000_000 + end_timespec.tv_nsec / 1_000)
        let time = end_time - start_time
        return time / 1_000
    }
    func getTime() -> Double {
        clock_gettime(Int32(CLOCK_REALTIME),&time_spec)
        return Double(time_spec.tv_sec * 1_000_000 + time_spec.tv_nsec / 1_000)
    }
}

var global = 0

class Obj {
    var value: Int = 0
    init(value: Int) {
        self.value = value
    }
}

func GenerateFakeRandomObject() -> [Obj] {
    let resource: [Obj] = [Obj](repeating: Obj(value: Int.random(in: 1..<10)), count: 15)
    return resource
}

func GenerateFakeRandomInteger() -> [Int] {
    let resource: [Int] = [12, 43, 56, 76, 89, 54, 45, 32, 35, 47, 46, 44, 21, 37, 84]
    return resource;
}

var arr : [Int] = GenerateFakeRandomInteger()

/***** Without parameters *****/
func parameterlessFoo() -> Obj {
    var res: [Obj] = [Obj](repeating: Obj(value: Int.random(in: 1..<10)), count: 5)
    let resources = GenerateFakeRandomObject()
    for i in 0..<200 {
        res[i % 5] = resources[i % 15]
    }
    return res[1]
}
func callParameterlessFoo(_ f: () -> Obj) -> Obj {
    var i = f()
    return i
}

// Pointer  no parameters function call
func RunParameterlessFunctionPtr() -> Int {
    let count = 10000
    let timer = Timer()
    global = 0
    var i3 = Obj(value:1);
    timer.start()
    for _ in 0..<count {
        i3 = callParameterlessFoo(parameterlessFoo)
    }
    let time = timer.stop()
    print("Function Call - RunParameterlessFunctionPtr:\t"+String(time)+"\tms");
    return Int(time)
}
_ = RunParameterlessFunctionPtr()

print("Swift Method Call Is End, global value : \(global)")