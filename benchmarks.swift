import Foundation
class BenchmarkSuite {
    private var arrayTests: [(String, () -> Int)] = []
    private var cocosTests: [(String, () -> Int)] = []
    private var splayTests: [(String, () -> Int)] = []
    private var propertyAccess: [(String, () -> Int)] = []
    private var nobody: [(String, () -> Int)] = []
    private var functionCall: [(String, () -> Int)] = []
    private var numericCal: [(String, () -> Int)] = []
    
    private var showDetails = false
    
    init(showDetails: Bool = false) {
        self.showDetails = showDetails
    }
    func initTests() {
        functionCall.append(("Function Pointer No Parameters Call", runParameterlessFunctionPtr))
        functionCall.append(("Function Pointer Different Parameters Call", runDifFunctionPtr))
        functionCall.append(("Function Pointer Variable Parameters Call", runVariableFuncPtr))
        functionCall.append(("Function Pointer Default Parameters Call", runDeffunctionPtr))
        functionCall.append(("Function Pointer ...Args Parameters Call", runArgsfunctionPtr))
        functionCall.append(("Method Call", runMethodCall))
        functionCall.append(("Method No Parameters Call", runParameterlessMethodCall))
        functionCall.append(("Method Different Parameters Call", runDifMethodCall))
        functionCall.append(("Method Variable Parameters Call", runVariableMethodCall))
        functionCall.append(("Method Default Parameters Call", runDefMethodCall))
        functionCall.append(("Method ...Args Parameters Call", runArgsMethodCall))
        functionCall.append(("Normal Call", runNormalCall))
        functionCall.append(("Normal No Parameters Call", runParameterlessCall))
        functionCall.append(("Normal Different Parameters Call", runNormalDifCall))
        functionCall.append(("Normal Variable Parameters Call", runNormalVariableFCall))
        functionCall.append(("Normal Default Parameters Call", runNormalDefCall))
        functionCall.append(("Normal ...Args Parameters Call", runNormalArgsCall))
        functionCall.append(("Static Normal No Parameters Call", runParameterlessStaticFunction))
        functionCall.append(("Static Normal Call", runStaticFunction))
        functionCall.append(("Static Normal Different Parameters Call", runNormalDifStaticFunc))
        functionCall.append(("Static Normal Variable Parameters Call", runNormalVariableFStaticFunc))
        functionCall.append(("Static Normal Default Parameters Call", runNormalDefCall))
        functionCall.append(("Static Normal ...Args Parameters Call", runNormalArgsStaticCall))
        functionCall.append(("BuildIn Call", runBuildInCall))
        functionCall.append(("Complex BuildIn Parameters Call", runComplexBuildInCall))
    }

    func runSingleGenera(genera: String, testCases: [(String, () -> Int)]) {
        for testCase in testCases {
            let suite = BenchmarkRunner(testCase.1)
            print("Benchmark - \(genera) - TS - \(testCase.0) running: ")
            let finalScore = suite.computeScore(showAllScore: showDetails)
            print("Final Score: \(finalScore)")
        }
    }
    
    func runAllTests() {
        print("NOTE: The Lower Score the Better Performance! :)")
        initTests()
        runSingleGenera(genera: "Array Access", testCases: arrayTests)
        runSingleGenera(genera: "Cocos", testCases: cocosTests)
        runSingleGenera(genera: "Splay", testCases: splayTests)
        runSingleGenera(genera: "PropertyAccess", testCases: propertyAccess)
        runSingleGenera(genera: "No-Body", testCases: nobody)
        runSingleGenera(genera: "Function Call", testCases: functionCall)
        runSingleGenera(genera: "Numerical Calculation", testCases: numericCal)
    }
}

class BenchmarkRunner {
    // N is the total number of iteration times
    // default N value is 120
    private var N: Int = 120

    // M is the number of scores that are ranked last
    // default M value is 4
    private var M: Int = 4

    // allRecords is an Array records all running results
    // except the first result
    private var allRecords: [Int?]
    
    // worstRecords is an Array records M number of worst results
    private var worstRecords: [Int?]

    // initScore is the first score
    private var initScore: Int = 0

    // IterationScore is the average value of (N results - firstIteration)
    private var iterationScore: Double = 0

    // WorstCasesScore is the average value of M worstRecords excluding firstIteration
    private var worstCasesScore: Double = 0

    var target: () -> Int
    
    init(_ target: @escaping () -> Int, totalCount: Int? = nil, worstCount: Int? = nil) {
        self.target = target
        if let totalCount = totalCount {
            self.N = totalCount
        }
        if let worstCount = worstCount {
            self.M = worstCount
        }
        self.allRecords = Array(repeating: 0, count: self.N)
        self.worstRecords = Array(repeating: 0, count: self.M)
    }

    func sampleAllData() {
        var sum = 0
        for i in 0..<self.N {
            let data = self.target()
            if i == 0 {
                self.initScore = data
                continue
            }
            self.allRecords[i] = data
            sum += data
        }
        self.iterationScore = Double(sum) / Double(self.N - 1)
        self.allRecords.sort(by: { $0! > $1! })

        sum = 0
        for j in 0..<self.M {
            self.worstRecords[j] = self.allRecords[j]
            sum += self.allRecords[j]!
        }
        self.worstCasesScore = Double(sum) / Double(self.M)
    }

    // Strategy:
    // Geometric Average of three scores
    func computeScore(showAllScore: Bool) -> Double {
        self.sampleAllData()
        let productsOfScores = Double(self.initScore) * self.iterationScore * self.worstCasesScore
        if showAllScore {
            print("InitScore: \(self.initScore)")
            print("IterationScore: \(self.iterationScore)")
            print("WorstCasesScore: \(self.worstCasesScore)")
        }
        return pow(productsOfScores, 1/3)
    }

    func getInitScore() -> Int {
        return self.initScore
    }

    func getIterationScore() -> Double {
        return self.iterationScore
    }

    func getWorstCasesScore() -> Double {
        return self.worstCasesScore
    }
}
