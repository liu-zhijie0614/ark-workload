set -x
declare -A testSets
testSets["arrayAccess"]="../ark-workload/ts-swift-workload/array-access/ts/"
testSets["fannkuc-redux"]="../ark-workload/ts-swift-workload/array-access/ts/"
testSets["fastarepeat"]="../ark-workload/ts-swift-workload/array-access/ts/"
testSets["fastarandom1"]="../ark-workload/ts-swift-workload/array-access/ts/"
testSets["fastarandom2"]="../ark-workload/ts-swift-workload/array-access/ts/"
testSets["mandelbrot"]="../ark-workload/ts-swift-workload/array-access/ts/"
testSets["spectralnorm"]="../ark-workload/ts-swift-workload/array-access/ts/"
testSets["threeDCube"]="../ark-workload/ts-swift-workload/array-access/ts/"
testSets["threeDRaytrace"]="../ark-workload/ts-swift-workload/array-access/ts/"
testSets["Method_Call"]="../ark-workload/ts-swift-workload/function-call/ts/"
testSets["Normal_Call"]="../ark-workload/ts-swift-workload/function-call/ts/"
testSets["Pointer_Call"]="../ark-workload/ts-swift-workload/function-call/ts/"
testSets["Pointer_Paraless_Call"]="../ark-workload/ts-swift-workload/function-call/ts/"
testSets["StaticFunc_Call"]="../ark-workload/ts-swift-workload/function-call/ts/"
testSets["nbody"]="../ark-workload/ts-swift-workload/nbody/ts/"
testSets["cocos"]="../ark-workload/ts-swift-workload/cocos/ts/"
testSets["bitops"]="../ark-workload/ts-swift-workload/numerical-calculation/ts/"
testSets["cordic"]="../ark-workload/ts-swift-workload/numerical-calculation/ts/"
testSets["numericalComputation"]="../ark-workload/ts-swift-workload/numerical-calculation/ts/"
testSets["binarySearchFloat"]="../ark-workload/ts-swift-workload/numerical-calculation/ts/"
testSets["binarySearchInteger"]="../ark-workload/ts-swift-workload/numerical-calculation/ts/"
testSets["partialSum"]="../ark-workload/ts-swift-workload/numerical-calculation/ts/"
testSets["propertyaccess2"]="../ark-workload/ts-swift-workload/property-access/ts/"
testSets["splay"]="../ark-workload/ts-swift-workload/splay/ts/"
testSets["allocate"]="../ark-workload/ts-swift-workload/allocate/ts"

sdk_path="../anzhuo/anzhuo"

$sdk_path/out/x64.release/arkcompiler/ets_frontend/es2abc $sdk_path/arkcompiler/ets_runtime/ecmascript/ts_types/lib_ark_builtins.d.ts --type-extractor --type-dts-builtin --module --merge-abc --extension=ts --output $sdk_path/arkcompiler/ets_runtime/ecmascript/ts_types/lib_ark_builtins.d.abc

$sdk_path/out/x64.release/arkcompiler/ets_frontend/es2abc ../ark-workload/utils/assert.ts --merge-abc --module --output ../ark-workload/utils/assert.abc
$sdk_path/out/x64.release/arkcompiler/ets_frontend/es2abc ../ark-workload/utils/benchmarkTsSuite.ts --merge-abc --module --output ../ark-workload/utils/benchmarkTsSuite.abc

export LD_LIBRARY_PATH=$sdk_path/out/x64.release/arkcompiler/ets_runtime:$sdk_path/out/x64.release/test/test:$sdk_path/out/x64.release/thirdparty/icu:$sdk_path/out/x64.release/thirdparty/zlib

for key in ${!testSets[@]};do
    ts_file=${testSets[${key}]}${key}.ts
    file_name=${key}
    abc_file=${testSets[${key}]}${key}.abc
    echo ${ts_file}
    echo ${file_name}
    echo ${abc_file}
    timeout 900s $sdk_path/out/x64.release/arkcompiler/ets_frontend/es2abc  ${ts_file} --type-extractor --module --merge-abc --extension=ts --output ${abc_file}
    timeout 900s $sdk_path/out/x64.release/arkcompiler/ets_runtime/ark_js_vm --icu-data-path "$sdk_path/third_party/icu/ohos_icu4j/data"  --enable-pgo-profiler=true --compiler-pgo-profiler-path=./module.ap --asm-interpreter=true --compiler-opt-inlining=true --entry-point=${file_name} ${abc_file} 
    # if [[ $key == "Method_Call" || $key == "Normal_Call" || $key == "Pointer_Call" || $key == "StaticFunc_Call" ]]; then
    #     echo $key 666
    #     timeout 900s $sdk_path/out/x64.release/arkcompiler/ets_runtime/ark_aot_compiler --builtins-dts=$sdk_path/arkcompiler/ets_runtime/ecmascript/ts_types/lib_ark_builtins.d.abc --compiler-fast-compile=false --compiler-opt-loop-peeling=true --compiler-pgo-profiler-path=./modules.ap --compiler-target-triple=aarch64-unknown-linux-gnu --compiler-opt-inlining=false --aot-file=${file_name} ${abc_file}       
    # else
    #     timeout 900s $sdk_path/out/x64.release/arkcompiler/ets_runtime/ark_aot_compiler --builtins-dts=$sdk_path/arkcompiler/ets_runtime/ecmascript/ts_types/lib_ark_builtins.d.abc --compiler-fast-compile=false --compiler-opt-loop-peeling=true --compiler-pgo-profiler-path=./modules.ap --compiler-target-triple=aarch64-unknown-linux-gnu --compiler-opt-inlining=true --aot-file=${file_name} ${abc_file}
    # fi
    timeout 900s $sdk_path/out/x64.release/arkcompiler/ets_runtime/ark_aot_compiler --compiler-opt-loop-peeling=true --compiler-fast-compile=false --compiler-opt-inlining=true --compiler-opt-track-field=true  --compiler-max-inline-bytecodes=45 --compiler-opt-level=2 --builtins-dts=${sdk_path}/arkcompiler/ets_runtime/ecmascript/ts_types/lib_ark_builtins.d.abc --compiler-pgo-profiler-path=./module.ap --compiler-target-triple=aarch64-unknown-linux-gnu ${aarch64} --aot-file=${file_name} ${abc_file}    
done
set +x
