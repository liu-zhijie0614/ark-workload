#!/bin/bash

sdk_path=~/workspace/daily/dev/code

toolspath=./toolspath.txt
tools_type="dev"
# ts_path=ts-swift-workload
ts_path=ts-swift-workload
code_ts_lib="BenchmarkMeasure"
target_path=""

# if [ -s "$toolspath" ]; then
#     while IFS= read -r line; do
#         case $line in
#             *--ts-tools-path\ *)
#                 sdk_path="${line#*--ts-tools-path\ }"
#                 ;;
#             *--ark_js_vm\ *)
#                 ark_js_vm="${line#*--ark_js_vm\ }"
#                 ;;
#             *--es2abc\ *)
#                 es2abc="${line#*--es2abc\ }"
#                 ;;
#             *--ark_aot_compiler\ *)
#                 ark_aot_compiler="${line#*--ark_aot_compiler\ }"
#                 ;;
#             *--tools-type\ *)
#                 tools_type="${line#*--tools-type\ }"
#                 ;;
#             *--case-path\ *)
#                 ts_path="${line#*--case-path\ }"
#                 ;;
#         esac
#     done < "toolspath.txt"
# fi

# 将带“~”的路径转换成绝对路径，保证脚本获取路径的准确性，部分命令不能使用“~”
# evaluated_path=$("echo $sdk_path")
# sdk_path=$(realpath "$evaluated_path")

arg_name=""
true="true"
false="false"
run_count=1
aarch64=""
date=$(date +'%Y%m%d%H%M%S')
code_v=1

pgo2path="pgo_build/${date}"

function help_explain(){
    echo "[--help,--h]"
    echo "[--run-count <count>]"
    echo "[--case <case-name>]"
    echo "[--date <date>]"
    echo "[--build]"
    echo "[--excel]"
    echo "[--aarch64]"
    echo "[--code-v] <code_path>"
}
if [ -z "$1" ]; then
    echo "No input provided."
else
    for arg in "$@"; do
        case $arg_name in
            "--case")
                arg_name=""
                case_name=$arg
                ;;
            "--date")
                arg_name=""
                date=$arg
                ;;
            "--run-count")
                arg_name=""
                run_count=$arg
                ;;
            "--code-v")
                arg_name=""
                echo $arg
                ts_path=$arg
                pgo2path="pgo_build-${ts_path}/${date}"
                case $arg in
                    "0")
                    ts_path=daily
                    pgo2path="pgo_build-${ts_path}/${date}"
                        ;;
                    "1")
                    ts_path=ts-swift-workload
                    pgo2path="pgo_build/${date}"
                        ;;
                    "ts-swift-workload")
                    ts_path=ts-swift-workload
                    pgo2path="pgo_build/${date}"
                        ;;
                    "2")
                    ts_path=mix-case
                    pgo2path="pgo_build-${ts_path}/${date}"
                        ;;
                    "3")
                    ts_path=weekly_workload
                    pgo2path="pgo_build-${ts_path}/${date}"
                        ;;
                esac
                ;;
        esac
        case $arg in
            "--h")
                help_explain
                exit
                ;;
            "--help")
                help_explain
                exit
                ;;
            "--build")
                build="true"
                ;;
            "--case")
                arg_name=$arg
                ;;
            "--date")
                arg_name=$arg
                ;;
            "--excel")
                excel="true"
                ;;
            "--run-count")
                arg_name=$arg
                ;;
            "--aarch64")
                aarch64=--compiler-target-triple=aarch64-unknown-linux-gnu
                ;;
            "--code-v")
                arg_name=$arg
                ;;
        esac
    done
fi

# echo $ts_path
# exit
echo "" > log.log

case $tools_type in
    "dev")
        # export LD_LIBRARY_PATH=${sdk_path}/out/x64.release/arkcompiler/ets_runtime:${sdk_path}/out/x64.release/thirdparty/icu:${sdk_path}/prebuilts/clang/ohos/linux-x86_64/llvm/lib:${sdk_path}/out/x64.release/thirdparty/zlib
        target_path=x64.release
        ;;
    "rk3568")
        # export LD_LIBRARY_PATH=${sdk_path}/out/rk3568/clang_x64/arkcompiler/ets_runtime:${sdk_path}/out/rk3568/clang_x64/thirdparty/icu:${sdk_path}/out/rk3568/clang_x64/thirdparty/zlib:${sdk_path}/out/rk3568/clang_x64/thirdparty/cjson:${sdk_path}/prebuilts/clang/ohos/linux-x86_64/llvm/lib
        target_path=rk3568/clang_x64
        ;;
    "hispark_taurus")
        # export LD_LIBRARY_PATH=${sdk_path}/out/hispark_taurus/clang_x64/arkcompiler/ets_runtime:${sdk_path}/out/hispark_taurus/clang_x64/thirdparty/icu:${sdk_path}/out/hispark_taurus/clang_x64/thirdparty/zlib:${sdk_path}/prebuilts/clang/ohos/linux-x86_64/llvm/lib
        target_path=hispark_taurus/clang_x64
        ;;
esac

# user_lib=${sdk_path}/out/${target_path}/lib.unstripped
user_lib=${sdk_path}/out/${target_path}
# user_exe=${sdk_path}/out/${target_path}/exe.unstripped
user_exe=${sdk_path}/out/${target_path}
export LD_LIBRARY_PATH=${user_lib}/arkcompiler/ets_runtime:${user_lib}/thirdparty/icu:${user_lib}/thirdparty/zlib:${user_lib}/thirdparty/cjson:${sdk_path}/prebuilts/clang/ohos/linux-x86_64/llvm/lib        

echo "  \
export LD_LIBRARY_PATH=${user_lib}/arkcompiler/ets_runtime:${user_lib}/thirdparty/icu:${user_lib}/thirdparty/zlib:${user_lib}/thirdparty/cjson:${sdk_path}/prebuilts/clang/ohos/linux-x86_64/llvm/lib \
" >> log.log


es2abc=${user_exe}/arkcompiler/ets_frontend/es2abc
ark_aot_compiler=${user_exe}/arkcompiler/ets_runtime/ark_aot_compiler
ark_js_vm=${user_exe}/arkcompiler/ets_runtime/ark_js_vm

chmod a+x ${es2abc}
chmod a+x ${ark_aot_compiler}
chmod a+x ${ark_js_vm}
echo "  \
chmod a+x ${es2abc} \
" >> log.log
echo "  \
chmod a+x ${ark_aot_compiler} \
" >> log.log
echo "  \
chmod a+x ${ark_js_vm} \
" >> log.log

$es2abc $sdk_path/arkcompiler/ets_runtime/ecmascript/ts_types/lib_ark_builtins.d.ts --type-extractor --type-dts-builtin --module --merge-abc --extension=ts --output $sdk_path/arkcompiler/ets_runtime/ecmascript/ts_types/lib_ark_builtins.d.abc

function build_pgo(){
    local ts_file=$1
    local file_name=$2
    # local abc_file=${pgo2path}/${file_name}.abc
    local abc_file=${file_name}.abc
    echo "build : ${ts_file} to ${pgo2path}"
    ${es2abc} ${ts_file} \
    --type-extractor \
    --module \
    --merge-abc \
    --extension=ts \
    --output ${pgo2path}/${file_name}.abc
    
    echo "  \
    ${es2abc} ${ts_file} \
    --type-extractor \
    --module \
    --merge-abc \
    --extension=ts \
    --output ${pgo2path}/${file_name}.abc \
    " >> log.log
    

    if [ "${code_ts_lib}" == "${file_name}" ];then
        echo "code_ts_lib: ${code_ts_lib}   :    file_name:${file_name}"
        return 1
    fi

    cd ${pgo2path}
    # --icu-data-path ${sdk_path}/third_party/icu/ohos_icu4j/data 
    # --asm-interpreter=true 
    ${ark_js_vm} \
    --log-level=info \
    --enable-pgo-profiler=true \
    --compiler-opt-inlining=true \
    --compiler-pgo-profiler-path=./${file_name}.ap \
    --asm-interpreter=true \
    --entry-point=${file_name} \
    ${abc_file}

    # --icu-data-path ${sdk_path}/third_party/icu/ohos_icu4j/data 
    # --asm-interpreter=true 
    echo "  \
    ${ark_js_vm} \
    --log-level=info \
    --enable-pgo-profiler=true \
    --compiler-opt-inlining=true \
    --compiler-pgo-profiler-path=./${file_name}.ap \
    --asm-interpreter=true \
    --entry-point=${file_name} \
    ${abc_file} \
    " >> log.log

    # timeout 300s ${ark_aot_compiler} --compiler-opt-loop-peeling=true --compiler-fast-compile=false --compiler-opt-track-field=true --compiler-opt-inlining=true --compiler-max-inline-bytecodes=45 --compiler-opt-level=2 --builtins-dts=${sdk_path}/arkcompiler/ets_runtime/ecmascript/ts_types/lib_ark_builtins.d.abc --compiler-pgo-profiler-path=./${pgo2path}/${file_name}.ap ${aarch64} --aot-file=./${pgo2path}/${file_name} ${abc_file}
    ${ark_aot_compiler} \
    --log-level=info \
    --compiler-opt-loop-peeling=true \
    --compiler-fast-compile=false \
    --compiler-opt-inlining=true \
    --compiler-max-inline-bytecodes=45 \
    --compiler-opt-level=3 \
    --builtins-dts=${sdk_path}/arkcompiler/ets_runtime/ecmascript/ts_types/lib_ark_builtins.d.abc \
    --compiler-pgo-profiler-path=./${file_name}.ap \
    --aot-file=./${file_name} \
    ${abc_file}

    echo "  \
    ${ark_aot_compiler} \
    --log-level=info \
    --compiler-opt-loop-peeling=true \
    --compiler-fast-compile=false \
    --compiler-opt-inlining=true \
    --compiler-max-inline-bytecodes=45 \
    --compiler-opt-level=3 \
    --builtins-dts=${sdk_path}/arkcompiler/ets_runtime/ecmascript/ts_types/lib_ark_builtins.d.abc \
    --compiler-pgo-profiler-path=./${file_name}.ap \
    --aot-file=./${file_name} \
    ${abc_file} \
    " >> log.log


    ${ark_js_vm} \
    --log-level=info \
    --enable-pgo-profiler=true \
    --compiler-opt-inlining=true \
    --compiler-pgo-profiler-path=./${file_name}.ap \
    --asm-interpreter=true \
    --aot-file=./${file_name} \
    --entry-point=${file_name} \
    ${abc_file}

    # --icu-data-path ${sdk_path}/third_party/icu/ohos_icu4j/data 
    # --asm-interpreter=true 
    echo "  \
    ${ark_js_vm} \
    --log-level=info \
    --enable-pgo-profiler=true \
    --compiler-opt-inlining=true \
    --compiler-pgo-profiler-path=./${file_name}.ap \
    --asm-interpreter=true \
    --aot-file=./${file_name} \
    --entry-point=${file_name} \
    ${abc_file} \
    " >> log.log

    # timeout 300s ${ark_aot_compiler} --compiler-opt-loop-peeling=true --compiler-fast-compile=false --compiler-opt-track-field=true --compiler-opt-inlining=true --compiler-max-inline-bytecodes=45 --compiler-opt-level=2 --builtins-dts=${sdk_path}/arkcompiler/ets_runtime/ecmascript/ts_types/lib_ark_builtins.d.abc --compiler-pgo-profiler-path=./${pgo2path}/${file_name}.ap ${aarch64} --aot-file=./${pgo2path}/${file_name} ${abc_file}
    ${ark_aot_compiler} \
    --log-level=info \
    --compiler-opt-loop-peeling=true \
    --compiler-fast-compile=false \
    --compiler-opt-inlining=true \
    --compiler-max-inline-bytecodes=45 \
    --compiler-opt-level=3 \
    --builtins-dts=${sdk_path}/arkcompiler/ets_runtime/ecmascript/ts_types/lib_ark_builtins.d.abc \
    --compiler-pgo-profiler-path=./${file_name}.ap \
    ${aarch64} \
    --aot-file=./${file_name} \
    ${abc_file}

    echo "  \
    ${ark_aot_compiler} \
    --log-level=info \
    --compiler-opt-loop-peeling=true \
    --compiler-fast-compile=false \
    --compiler-opt-inlining=true \
    --compiler-max-inline-bytecodes=45 \
    --compiler-opt-level=3 \
    --builtins-dts=${sdk_path}/arkcompiler/ets_runtime/ecmascript/ts_types/lib_ark_builtins.d.abc \
    --compiler-pgo-profiler-path=./${file_name}.ap \
    ${aarch64} \
    --aot-file=./${file_name} \
    ${abc_file} \
    " >> log.log

    cd -

}

function run_pgo(){
    
    if [ "${code_ts_lib}" == "${file_name}" ];then
        echo "code_ts_lib: ${code_ts_lib}   :    file_name:${file_name}"
        return 1
    fi
    cd ${pgo2path}
    local file_name=$1
    local abc_file=${pgo2path}/${file_name}.abc
    echo run ${file_name}
    ${ark_js_vm} --entry-point=${file_name} --aot-file=./${pgo2path}/${file_name} ${abc_file}
    cd -
}

function run(){
    if [[ "$build" = "true" ]]; then
        if [ ! -d $pgo2path ];then
            mkdir -p $pgo2path
        fi
        code_ts_lib_file=$(find $ts_path -type f -name ${code_ts_lib}.ts)
        if [ "${code_ts_lib_file}" != "" ]; then
            file_name=$(basename "${code_ts_lib_file}" | cut -f1 -d'.')
            pgo_build_path=$(dirname "${pgo2path}")
            ${es2abc} ${code_ts_lib_file} --type-extractor --module --merge-abc --extension=ts --output ${pgo_build_path}/${file_name}.abc
        fi
        if [ -z "$case_name" ]; then
            txt_files=($(find $ts_path -type f -name "*.ts"))
            for file in "${txt_files[@]}"; do
                echo "Content of $file:"
                local ts_file_path=$file
                local file_name=$(basename "${file}" | cut -f1 -d'.')
                build_pgo ${ts_file_path} ${file_name}
                if [[ "$excel" = "true" ]]; then
                    if [ "${code_ts_lib}" == "${file_name}" ];then
                        echo "code_ts_lib: ${code_ts_lib}   :    file_name:${file_name}"
                        continue
                    fi
                    python pgo2excel.py --case-dir $pgo2path --run-case $file_name --run-count $run_count --ark_js_vm $ark_js_vm
                fi
                done
        else
            local ts_file_path=$(find ${ts_path} -name "${case_name}.ts")
            local file_name=$(basename "$ts_file_path" | cut -f1 -d'.')
            if [ -f "${ts_file_path}" ]; then
                build_pgo ${ts_file_path} ${file_name}
                if [[ "$excel" = "true" ]]; then
                    if [ "${code_ts_lib}" == "${file_name}" ];then
                        echo "code_ts_lib: ${code_ts_lib}   :    file_name:${file_name}"
                        continue
                    fi
                    python pgo2excel.py --case-dir $pgo2path --run-case $file_name --run-count $run_count --ark_js_vm $ark_js_vm
                fi
            fi
        fi
    fi

    if [ ! -d $pgo2path ];then
        echo "${pgo2path} No such file or directory"
        exit
    fi
}
run

