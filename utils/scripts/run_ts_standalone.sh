ts_file=$1
abc_file=${ts_file:: -3}.abc
file_name=${ts_file:: -3}
echo ${abc_file}
echo ${file_name}


../../../../../out/x64.release/arkcompiler/ets_frontend/es2abc ../../../utils/assert.ts --merge-abc --module --output ../../../utils/assert.abc
../../../../../out/x64.release/arkcompiler/ets_frontend/es2abc ../../../utils/benchmarkTsSuite.ts --merge-abc --module --output ../../../utils/benchmarkTsSuite.abc

export LD_LIBRARY_PATH=../../../../../out/x64.release/lib.unstripped/arkcompiler/ets_runtime:../../../../../out/x64.release/lib.unstripped/test/test:../../../../../out/x64.release/lib.unstripped/thirdparty/icu:../../../../../prebuilts/clang/ohos/linux-x86_64/llvm/lib:../../../../../out/x64.release/lib.unstripped/thirdparty/zlib

../../../../../out/x64.release/arkcompiler/ets_frontend/es2abc ${ts_file} --module --merge-abc --extension=ts --output ${abc_file}

../../../../../out/x64.release/arkcompiler/ets_runtime/ark_js_vm --icu-data-path "../../../../../third_party/icu/ohos_icu4j/data" --log-level=info --asm-interpreter=true --entry-point=${file_name} ${abc_file}

