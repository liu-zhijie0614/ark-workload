target_device=$1
js_file=$2
abc_file=${js_file:: -3}.abc
file_name=${js_file:: -3}
echo ${abc_file}
echo ${file_name}

node --expose-gc ../../../../../out/${target_device}/clang_x64/arkcompiler/ets_frontend/build/src/index.js --merge-abc -m ${js_file}

export LD_LIBRARY_PATH=../../../../../out/${target_device}/clang_x64/lib.unstripped/clang_x64/arkcompiler/ets_runtime:../../../../../out/${target_device}/clang_x64/lib.unstripped/clang_x64/test/test:../../../../../out/${target_device}/clang_x64/lib.unstripped/clang_x64/thirdparty/icu:prebuilts/clang/ohos/linux-x86_64/llvm/lib:../../../../../out/${target_device}/clang_x64/lib.unstripped/clang_x64/thirdparty/zlib

../../../../../out/${target_device}/clang_x64/exe.unstripped/clang_x64/arkcompiler/ets_runtime/ark_js_vm --icu-data-path "../../../../../third_party/icu/ohos_icu4j/data"  --log-level=info --asm-interpreter=true --entry-point=${file_name} ${abc_file}

