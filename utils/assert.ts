export function ASSERT_TRUE(t: boolean) {
    if(t == true) {
        return;
    }
    let message : string = 'Expected true but got ' + t.toString();
    throw new Error(message);
}

export function ASSERT_FALSE(f: boolean) {
    if(f == false) {
        return;
    }
    let message : string = 'Expected false but got ' + f.toString();
    throw new Error(message);
}

export function ASSERT_NUMBER_EQ(a: number, b: number) {
    if(a === b) {
        return;
    }
    let message : string = 'Expected ' + a.toString() + ' equal to ' + b.toString();
    throw new Error(message);
}

export function ASSERT_NUMBER_NE(a: number, b: number) {
    if(a !== b) {
        return;
    }
    let message : string = 'Expected ' + a.toString() + ' not equal to ' + b.toString();
    throw new Error(message);
}

export function ASSERT_FLOAT_EQ(a: number, b: number) {
    if(Math.abs(a - b) < Number.EPSILON) {
        return;
    }
    let message : string = 'Expected ' + a.toString() + ' equal to ' + b.toString();
    throw new Error(message);
}

export function ASSERT_FLOAT_NE(a: number, b: number) {
    if(Math.abs(a - b) > Number.EPSILON) {
        return;
    }
    let message : string = 'Expected ' + a.toString() + ' not equal to ' + b.toString();
    throw new Error(message);
}

export function ASSERT_EQ(a: any, b: any) {
    if(a === b) {
        return;
    }
    let message : string = 'Expected ' + a.toString() + ' equal to ' + b.toString();
    throw new Error(message);
}

export function ASSERT_NE(a: any, b: any) {
    if(a !== b) {
        return;
    }
    let message : string = 'Expected ' + a.toString() + ' not equal to ' + b.toString();
    throw new Error(message);
}
