import Foundation

func assertTrue(_ t: Bool) throws {
    if t == true {
        return
    }
    let message = "Expected true but got \(t)"
    throw NSError(domain: "", code: 0, userInfo: [NSLocalizedDescriptionKey: message])
}

func assertFalse(_ f: Bool) throws {
    if(!f) {
        return
    }
    let message : String = "Expected false but got \(f)"
    throw NSError(domain: "Assertion Error", code: 0, userInfo: [NSLocalizedDescriptionKey: message])
}

func assertNumberEqual(_ a: Double, _ b: Double) throws {
    if(a == b) {
        return
    }
    let message: String = "Expected \(a) equal to \(b)"
    throw NSError(domain: "Assertion Error", code: 0, userInfo: [NSLocalizedDescriptionKey: message])
}

func assertNumberNotEqual(_ a: Double, _ b: Double) throws {
    if(a != b) {
        return
    }
    let message: String = "Expected \(a) not equal to \(b)"
    throw NSError(domain: "Assertion Error", code: 0, userInfo: [NSLocalizedDescriptionKey: message])
}

func assertFloatEqual(_ a: Double, _ b: Double) throws {
    if(abs(a - b) < Double.ulpOfOne) {
        return
    }
    let message: String = "Expected \(a) equal to \(b)"
    throw NSError(domain: "Assertion Error", code: 0, userInfo: [NSLocalizedDescriptionKey: message])
}

func assertFloatNotEqual(_ a: Double, _ b: Double) throws {
    if(abs(a - b) > Double.ulpOfOne) {
        return
    }
    let message: String = "Expected \(a) not equal to \(b)"
    throw NSError(domain: "Assertion Error", code: 0, userInfo: [NSLocalizedDescriptionKey: message])
}

func assertEqual<T: Equatable>(_ a: T, _ b: T) throws {
    if(a == b) {
        return
    }
    let message: String = "Expected \(a) equal to \(b)"
    throw NSError(domain: "Assertion Error", code: 0, userInfo: [NSLocalizedDescriptionKey: message])
}

func assertNotEqual<T: Equatable>(_ a: T, _ b: T) throws {
    if(a != b) {
        return
    }
    let message: String = "Expected \(a) not equal to \(b)"
    throw NSError(domain: "Assertion Error", code: 0, userInfo: [NSLocalizedDescriptionKey: message])
}