#! /bin/bash

declare -A testSets
testSets["arrayAccess"]="./ts-swift-workload/array-access/swift/"
testSets["fannkuc-redux"]="./ts-swift-workload/array-access/swift/"
testSets["fasta"]="./ts-swift-workload/array-access/swift/"
testSets["mandelbrot"]="./ts-swift-workload/array-access/swift/"
testSets["spectralnorm"]="./ts-swift-workload/array-access/swift/"
testSets["threeDCube"]="./ts-swift-workload/array-access/swift/"
testSets["threeDRaytrace"]="./ts-swift-workload/array-access/swift/"
# testSets["Deep_Nested_Call"]="./ts-swift-workload/function-call/swift/"
testSets["Method_Call"]="./ts-swift-workload/function-call/swift/"
testSets["Normal_Call"]="./ts-swift-workload/function-call/swift/"
testSets["Pointer_Call"]="./ts-swift-workload/function-call/swift/"
testSets["Pointer_Paraless_Call"]="./ts-swift-workload/function-call/swift/"
# testSets["Shallow_Nested_Call"]="./ts-swift-workload/function-call/swift/"
testSets["nbody"]="./ts-swift-workload/nbody/swift/"
testSets["cocos"]="./ts-swift-workload/cocos/swift/"
testSets["cocos_airplane"]="./ts-swift-workload/cocos/swift/"
testSets["bitops"]="./ts-swift-workload/numerical-calculation/swift/"
testSets["cordic"]="./ts-swift-workload/numerical-calculation/swift/"
testSets["numericalComputation"]="./ts-swift-workload/numerical-calculation/swift/"
testSets["partialSum"]="./ts-swift-workload/numerical-calculation/swift/"
# testSets["propertyaccess1"]="./ts-swift-workload/property-access/swift/"
testSets["propertyaccess2"]="./ts-swift-workload/property-access/swift/"
testSets["splay"]="./ts-swift-workload/splay/swift/"

if [ $# -eq 0 ]; then
    benchmark="ALL"
    echo  "all"
else
    benchmark=$1
fi
benchmarkALL="ALL"
swift_path=ts-swift-workload
swift_files=""
LogDate=$(date '+%Y%m%d%H%M%S')
result_file_path=result/$LogDate
function read_dir(){
    if [ "$benchmark" = "$benchmarkALL" ]; then
        if [ ! -d $result_file_path ];then
            mkdir -p $result_file_path
        fi
        # echo  "----- Run $benchmark Benchmark Case -----" >> result/benchmark_result$LogDate.log
        for key in ${!testSets[@]};
        do
            swift_file=${testSets[${key}]}${key}.swift
            file_name=${key}
            echo ${ts_file}
            echo ${file_name}
            
            if [ -f $swift_file ]; then
                if [ ! -d $result_file_path/$file_name ]; then
                    mkdir -p $result_file_path/$file_name
                fi
                swiftc -O -whole-module-optimization $swift_file -o $result_file_path/$file_name/$file_name
                # $result_file_path/$file_name/$file_name >> $result_file_path/${filename}-result.log
            else
                echo "File does not exist."
            fi
        done
    else
        if [ "$benchmark" = "--help" ]; then
            echo run all test files by default when there are no parameters
            echo [ALL]             run all swift test suites
            echo [path]              run the test suite in the path
        else
            if [ -f $benchmark ];then
                filename=$(basename "$benchmark")
                filename="${filename%.*}"
                if [ ! -d $result_file_path/$filename ];then
                    mkdir -p $result_file_path/$filename
                fi
                echo "Run $filename Case"  >> $result_file_path/${filename}-result.log
                if [ -f $result_file_path/$filename/$filename ]
                then
                    rm $result_file_path/$filename/$filename
                fi
                swiftc  -O -whole-module-optimization  $benchmark -o $result_file_path/$filename/$filename
                # $result_file_path/$filename/$filename  >> $result_file_path/${filename}-result.log
                    # rm $result_file_path/$filename/$filename
            fi
        fi
    fi


} 
read_dir $swift_path