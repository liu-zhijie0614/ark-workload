# ArkCompiler WorkLoad

### Introduction

Performance test cases, used to test the performance of the ark compiler
1) ts workload
2) swift workload
3) napi workload

### Running tests
1) DownLoads the project to your OpenHarmony env root
2) `chmod +x *.sh`
3) `chmod +x *.py`
4) `pip install openpyxl`
5) Configure your environment
    Open the toolspath.txt file and configure your environment<br>
    ![输入图片说明](img/toolspath.png)<br>
    `--tools-path`<br>
        openharmony code path<br>
    `--tools-type <dev | rk3568 | hispark_taurus>`<br>
        tools type,dev or rk3568 or hispark_taurus<br>
    `--swift-tools-path path`<br>
        swift tools path<br>
    `--android-ndk path`<br>
        android-ndk path<br>
    `--android-ndk path`<br>
        android-ndk path<br>
    `--Ninja-ReleaseAssert`<br>
        swift lib<br>
        
6) build your project
    `./run_pgo.sh --build --case case-name --run-count 10 --excel --date 20230723`<br>
    ![输入图片说明](img/run_pgo_help.png)

#####   Parameter Description
    --build
        Compile and build, without compilation and build by default, The generated file is in the current directory's `pgo_ Build`

    --date <date>
        When `--build` does not exist, it is necessary to add `--date`, These two parameters can exist simultaneously, This parameter allows you to run a history compiled case, These cases are in `pgo_build`, followed by the date, for example: `--date 20230712`

    --case <case-name>
        Compile or execute a single use case, `--case` followed by case name, By default it means all<br>

    --excel`
        Run the case and generate Excel after the case ends, Do not execute case by default<br>

    --run-count <count>
        The number of times a case is run, which exists simultaneously with Excel, for example: `--run-count 10`, which means executing the case 10

    --aarch64
        Used to compile arrch64 devices

    --code-v
        case tpye,
        parameter：
            0，daily；
            1，ts-swift-workload；
            2，mix-case
            3，weekly_workload
            other，other file paths
        ps:./run_pgo.sh --build --code-v 3
           ./run_pgo.sh --build --code-v weekly_workload

    times and saving the results in an Excel table
        `The parameters of pgo and swift are the same`

#####   example:
Compile case：<br>
    `./run_pgo.sh --build`.  Compile all cases<br>
    `./run_pgo.sh --build --aarch64`.  Compile all use cases into executable files that can run on arrch64 devices<br>
    `./run_pgo.sh --build --case splay`.  Compile splay<br>
    `./run_pgo.sh --build --date 20230712 --case splay`.  Compile splay in 20230712<br>
    `./run_pgo.sh --build --excel --run-count 10`.  Compile all cases, execute the case 10 times, and generate an Excel table<br>
    `./run_pgo.sh --date 20230712 --excel --run-count 10`.  Run all cases under 20230712 10 times and generate Excel tables<br>
    `./run_pgo.sh --build --code-v weekly_workload --aarch64`.  Compile weekly_workload use cases into executables that run on arrch64 devices<br>

### ts-swift-workload notes
#### fannkuc-redux Benchmark
  - https://benchmarksgame-team.pages.debian.net/benchmarksgame/description/fannkuchredux.html#fannkuchredux
  - target input is 12 => decrease number to decrease difficulty of the benchmark

#### fasta Benchmark
  - https://benchmarksgame-team.pages.debian.net/benchmarksgame/description/fasta.html#fasta
  - target input is 25000000 => decrease number to decrease difficulty of the benchmark

#### mandelbrot Benchmark
  - https://benchmarksgame-team.pages.debian.net/benchmarksgame/description/mandelbrot.html#mandelbrot
  - target input is 16000 => decrease number to decrease difficulty of the benchmark

#### spectralnorm Benchmark
  - https://benchmarksgame-team.pages.debian.net/benchmarksgame/description/spectralnorm.html#spectralnorm
  - target input is 5500 => decrease number to decrease difficulty of the benchmark
