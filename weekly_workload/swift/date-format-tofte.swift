import Glibc

class MyDate {
    

    public func timeIntervalSince1970() -> Int {
        return 1167585071
    }
    public func timeStamp() -> Int{
        return 1167585071
    }
    public func milliStamp() -> Int {
        return  1167585071000
    }
    init(_ str: String){
    }
    
    public func getDate() -> Int {
        //Returns a day of the month
        return 1
    }
    public func getTimezoneOffset() -> Int{
        //Returns the time difference between Greenwich Mean Time and local time:
        return  -480
    }
    public  func getYear() -> Int{
        //
        return 2007
    }
    public  func getFullYear() -> Int {
        //return a year
        return 2007
    }
    public func getMonth() -> Int{
        //return a month
        return 1
    }
    public func getHours() -> Int{
        //Returns the hour field of the time based on the specified time:
        return 1
    }
    public func getMinutes() -> Int{
        //Returns the minute field of the time based on the specified time:
        return 11
    }
    public  func getSeconds() -> Int {
        //Returns the second3 field of the time based on the specified time:
        return 11
    }
    public  func getDay() -> Int {
        //Returns the number of a day of the week.
        return 1
    }

    public func getTime() -> Int{
        //Returns the number of milliseconds between January 1, 1970:
        let timeInterval = self.timeIntervalSince1970()
        return   timeInterval * 1000
    }
    public  func setTime(_ millisedcond: Int)  {
        //Method to set the Date object in milliseconds.
    }
}
class DateFormatter{
    public var dateFormat: String?
    public var timeZone: TimeZone?
    init() {
    }
    public func newDate(from: String) -> MyDate {
        return MyDate(from)
    }
}

class TimeZone {
    var identifier: String
    public init(identifier: String) {
        self.identifier = identifier
    }
}

func arrayExists(_ array: [String], _ x: String) -> Bool{
    for i in 0..<array.count {
        if array[i] == x {
            return true
        }
    }
    return false
}

class DateClass: MyDate {
    let switches =    ["a", "A", "B", "d", "D", "F", "g", "G", "h", "H",
                        "i", "j", "l", "L", "m", "M", "n", "O", "r", "s",
                        "S", "t", "U", "w", "W", "y", "Y", "z"];
    let daysLong =    ["Sunday", "Monday", "Tuesday", "Wednesday",
                        "Thursday", "Friday", "Saturday"];
    let daysShort =   ["Sun", "Mon", "Tue", "Wed",
                        "Thu", "Fri", "Sat"];
    let monthsShort = ["Jan", "Feb", "Mar", "Apr",
                        "May", "Jun", "Jul", "Aug", "Sep",
                        "Oct", "Nov", "Dec"];
    let monthsLong =  ["January", "February", "March", "April",
                        "May", "June", "July", "August", "September",
                        "October", "November", "December"];
    let daysSuffix = ["st", "nd", "rd", "th", "th", "th", "th",   // 1st - 7th
                        "th", "th", "th", "th", "th", "th", "th", // 8th - 14th
                        "th", "th", "th", "th", "th", "th", "st", // 15th - 21st
                        "nd", "rd", "th", "th", "th", "th", "th", // 22nd - 28th
                        "th", "th", "st"];                        // 29th - 31st
    
    override init(_ str: String?) {
        super.init(str!)
    }
    func formatDate(_ input: String, _ time: Int?) -> String {
    // formatDate :
    // a PHP date like function, for formatting date strings
    // See: http://www.php.net/date
    //
    // input : format string
    // time : epoch time (seconds, and optional)
    //
    // if time is not passed, formatting is based on
    // the current "this" date object's set time.
    //
    // supported:
    // a, A, B, d, D, F, g, G, h, H, i, j, l (lowercase L), L,
    // m, M, n, O, r, s, S, t, U, w, W, y, Y, z
    //
    // unsupported:
    // I (capital i), T, Z
    var prevTime = 0
    if (time != nil) {
        // save time
        prevTime = self.getTime();
        self.setTime(time!);
    }
    var arr = stringToArray(input)
    for index in 0..<arr.count {
        if (arrayExists(switches,arr[index])) {
            var value: String = arr[index]
            switch arr[index] {
            case "a":
                value =  a()
                break
            case "A":
                value =  A()
                break
            case "B":
                value =  B()
                break
            case "d":
                value =  d()
                break
            case "D":
                value =  D()
                break
            case "F":
                value =  F()
                break
            case "g":
                value =  g()
                break
            case "G":
                value = "\(G())"
                break
            case "h":
                value =  h()
                break
            case "H":
                value =  H()
                break
            case "i":
                value =  i()
                break
            case "j":
                value =  "\(j())"
                break
            case "l":
                value =  l()
                break
            case "L":
                value =  "\(L())"
                break
            case "m":
                value =  m()
                break
            case "M":
                value =  M()
                break
            case "n":
                value =  "\(n())"
                break
            case "O":
                value =  O()
                break
            case "r":
                value =  r()
                break
            case "s":
                value =  s()
                break
            case "S":
                value = S()
                break
            case "t":
                value = "\(t())"
                break
            case "U":
                value = "\(U())"
                break
            case "w":
                value =  "\(w())"
                break
            case "W":
                value =  W()
                break
            case "y":
                value = y()
                break
            case "Y":
                value = "\(Y())"
                break
            case "z":
                value =  "\(z())"
                break
            default: break

            }
            arr[index] = value;
        }
    }
    //reset time, back to what it was
    if (prevTime != 0) {
        self.setTime(prevTime);
    }
    return arr.joined(separator: "")
}
    func a() -> String {
        // Lowercase Ante meridiem and Post meridiem
        return self.getHours() > 11 ? "pm" : "am"
    }
    func A() -> String {
        // Uppercase Ante meridiem and Post meridiem
        return self.getHours() > 11 ? "PM" : "AM";
    }

    func B() -> String{
        // Swatch internet time. code simply grabbed from ppk,
        // since I was feeling lazy:
        // http://www.xs4all.nl/~ppk/js/beat.html
        let off = (self.getTimezoneOffset() + 60)*60;
        let theSeconds = (self.getHours() * 3600) + (self.getMinutes() * 60) + self.getSeconds() + off;
        var beat = floor(Double(theSeconds)/86.4);
        if beat > 1000 {
            beat -= 1000
        };
        if beat < 0 {
            beat += 1000
        };
        var betaStr = "\(beat)"
        if betaStr.count == 1{
            betaStr =  "00\(beat)"
        }
        if betaStr.count == 2 {
            betaStr = "0\(beat)"
        }
        return betaStr
    }

    func d() -> String{
        // Day of the month, 2 digits with leading zeros
        return  "\(self.getDate())".count == 1 ?
        "0\(self.getDate())": "\(self.getDate())"
    }
    func D() -> String{
        // A textual representation of a day, three letters
        return daysShort[self.getDay()];
    }
    func F() -> String{
        // A full textual representation of a month
        return monthsLong[self.getMonth() - 1];
    }
    func g() -> String{
        // 12-hour format of an hour without leading zeros
        return self.getHours() > 12 ? "\(self.getHours()-12)" : "\(self.getHours())";
    }
    func G() -> Int{
        // 24-hour format of an hour without leading zeros
        return self.getHours();
    }
    func h() -> String{
        // 12-hour format of an hour with leading zeros
        if (self.getHours() > 12) {
            let s = "\(self.getHours() - 12)"
            return s.count == 1 ? "0\(self.getHours() - 12)" : "\(self.getHours() - 12)";
        } else {
            let s = "\( self.getHours())";
            return s.count == 1 ? "0\( self.getHours())" : "\( self.getHours())"
        }
    }
    func H() -> String{
        // 24-hour format of an hour with leading zeros
        return "\(self.getHours())".count == 1 ? "0\(self.getHours())" : "\(self.getHours())"
    }
    func i() -> String{
        // Minutes with leading zeros
        return "\(self.getMinutes())".count == 1 ? "0\(self.getMinutes())" : "\(self.getMinutes())"
    }
    func j() -> Int{
        // Day of the month without leading zeros
        return self.getDate();
    }
    func l() -> String{
        // A full textual representation of the day of the week
        return daysLong[self.getDay() - 1];
    }
    func L() -> Int{
        // leap year or not. 1 if leap year, 0 if not.
        // the logic should match iso's 8601 standard.
        let y_ = Y();
        if (
            (y_ % 4 == 0 && y_ % 100 != 0) ||
            (y_ % 4 == 0 && y_ % 100 == 0 && y_ % 400 == 0)
        ) {
            return 1;
        } else {
            return 0;
        }
    }
    func m() -> String{
        // Numeric representation of a month, with leading zeros
        return self.getMonth() < 9 ? "0\(self.getMonth() + 1)" : "\(self.getMonth() + 1)";
    }
    func M() -> String{
        // A short textual representation of a month, three letters
        return monthsShort[self.getMonth()];
    }
    func n() -> Int{
        // Numeric representation of a month, without leading zeros
        return self.getMonth() + 1;
    }
    func O() -> String{
        // Difference to Greenwich time (GMT) in hours
        let os = abs(self.getTimezoneOffset())
        var h = "\(Int(floor(Double(os/60))))"
        var m = "\(os%60)"
        h = h.count == 1 ? "0"+h:"1"
        m = m.count == 1 ? "0"+m:"1"
        return self.getTimezoneOffset() < 0 ? "+\(h)\(m)":"-\(h)\(m)"
    }
    func r() -> String{
        // RFC 822 formatted date
        var r: String
            r = "\(D()),\(j()) \(M()) \(Y()) \(H()):\(i()):\(s()) \(O())"// result
        return r;
    }
    func S() -> String{
        // English ordinal suffix for the day of the month, 2 characters
        return daysSuffix[self.getDate()-1];
    }
    func s()-> String {
        // Seconds, with leading zeros
        return "\(self.getSeconds())".count == 1 ?  "0\(getSeconds())" : "\(getSeconds())"
    }
    func t() -> Int{
        // thanks to Matt Bannon for some much needed code-fixes here!
        let daysinmonths = [nil,31,28,31,30,31,30,31,31,30,31,30,31];
        if (L()==1 && n()==2){
            return 29;// leap day
        }
        return daysinmonths[n()]!;
    }
    func U() -> Double{
        // Seconds since the Unix Epoch (January 1 1970 00:00:00 GMT)
        return round(Double(self.getTime()/1000));
    }
    func W() -> String{
        // Weeknumber, as per ISO specification:
        // http://www.cl.cam.ac.uk/~mgk25/iso-time.html

        // if the day is three days before newyears eve,
        // there's a chance it's "week 1" of next year.
        // here we check for that.
        let beforeNY = 364+L() - Int(z());
        let afterNY  = z();
        let weekday = w() != 0 ? w()-1 : 6; // makes sunday (0), into 6.
        if (beforeNY <= 2 && weekday <= 2-beforeNY) {
            return "1";
        }
        // similarly, if the day is within threedays of newyears
        // there's a chance it belongs in the old year.

        let ny = stringToDate("1/1/\(Y()) 00:00:00")
        let nyDay = ny.getDay() != 0 ? ny.getDay()-1 : 6;
        if (
            (afterNY <= 2) &&
            (nyDay >= 4)  &&
            (Int(afterNY) >= (6-nyDay))
        ) {
            // Since I'm not sure we can just always return 53,
            // i call the function here again, using the last day
            // of the previous year, as the date, and then just
            // return that week.

            let prevNY = DateClass("December 31 " + "\(Y()-1)" + " 00:00:00")
            return  prevNY.formatDate("W",nil)
        }

        // week 1, is the week that has the first thursday in it.
        // note that this value is not zero index.
        if (nyDay <= 3) {
            // first day of the year fell on a thursday, or earlier.
            return "\( 1 + Int(floor( ( z() + Double(nyDay) ) / 7 )))";
        } else {
            // first day of the year fell on a friday, or later.
            return "\(1 + Int(floor( ( z() - ( 7 - Double(nyDay) ) ) / 7 )))";
        }
    }
    func w() -> Int{
        // Numeric representation of the day of the week
        return self.getDay();
    }
    func Y() -> Int {
        // A full numeric representation of a year, 4 digits

        // we first check, if getFullYear is supported. if it
        // is, we just use that. ppks code is nice, but wont
        // work with dates outside 1900-2038, or something like that
        if (self.getFullYear() != 0) {
            let newDate = DateClass("January 1 2001 00:00:00 +0000")
            let x = newDate.getYear()
            if (x == 2001) {
                // i trust the method now
                return getFullYear();
            }
        }
        // else, do this:
        // codes thanks to ppk:
        // http://www.xs4all.nl/~ppk/js/introdate.html
        let x = self.getYear();
        var y = x % 100;
        y += (y < 38) ? 2000 : 1900;
        return y;
    }
    func y() -> String {
        // A two-digit representation of a year
        let str = "\(Y())";
        let startIndex = str.index(str.startIndex, offsetBy: 2)
        let endIndex = str.index(str.startIndex, offsetBy: 3)
        let substring = str[startIndex...endIndex]
        return "\(substring)"
    }
    func z() -> Double{
        // The day of the year, zero indexed! 0 through 366
        let t =  DateClass("January 1 \(Y()) 00:00:00");
        let diff = self.getTime() - t.getTime();
        return floor(Double(diff/1000/60/60/24));
    }
    func stringToArray(_ string: String) -> [String] {
       var arr = [String]()
       for index in string.indices {
           arr.append("\(string[index])")
       }
       return arr
   }
    func stringToDate(_ str: String) -> MyDate {
       let dateFormatter = DateFormatter()
       dateFormatter.dateFormat = "dd-MM-yyyy HH:mm:ss"
       dateFormatter.timeZone = TimeZone(identifier: "Asia/Shanghai")
       let dateObject = dateFormatter.newDate(from: str)
        return dateObject
    }

}
func run()  {
    let date = DateClass("1/1/2007 01:11:11")
    for _ in 0..<500 {
        let shortformat = date.formatDate("Y-m-d",nil)
        let longformat = date.formatDate("l, F d, Y g:i:s A",nil)
        date.setTime(date.getTime()  + 84266956)
    }
}
func runIterationTime() {
    let start = Timer().getTime()
    for _ in 0..<80 {
        run()
 }
    let end = Timer().getTime()
    let duration = (end - start)/1000
    print("date-format-tofte: ms = \(duration)")
}
runIterationTime()


class Timer {
    private let CLOCK_REALTIME = 0
    private var time_spec = timespec()

    func getTime() -> Double {
        clock_gettime(Int32(CLOCK_REALTIME),&time_spec)
        return Double(time_spec.tv_sec * 1_000_000 + time_spec.tv_nsec / 1_000)
    }
}
