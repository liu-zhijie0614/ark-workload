public struct OptionParams {
    var lazy: Int?
    var compressionType: CompressionType?
    var outputBuffer: [UInt8]?
    var outputIndex: Int?
}
public struct TreeSymbols {
    var codes: [UInt32]
    var freqs: [UInt8]
}

public enum CompressionType: Int {
    case NONE = 0
    case FIXED = 1
    case DYNAMIC = 2
    case RESERVED = 3
}

public class Lz77Match {
    public var length: Int
    public var backwardDistance: Int
    
    init(_ length: Int, _ backwardDistance: Int) {
        self.length = length
        self.backwardDistance = backwardDistance
    }
    
    static var LengthCodeTable: [UInt32] {
        func code(_ length: UInt32) -> [UInt32] {
            switch true {
            case length == 3:
                return [257, length - 3, 0]
            case length == 4:
                return [258, length - 4, 0]
            case length == 5:
                return [259, length - 5, 0]
            case length == 6:
                return [260, length - 6, 0]
            case length == 7:
                return [261, length - 7, 0]
            case length == 8:
                return [262, length - 8, 0]
            case length == 9:
                return [263, length - 9, 0]
            case length == 10:
                return [264, length - 10, 0]
            case length <= 12:
                return [265, length - 11, 1]
            case length <= 14:
                return [266, length - 13, 1]
            case length <= 16:
                return [267, length - 15, 1]
            case length <= 18:
                return [268, length - 17, 1]
            case length <= 22:
                return [269, length - 19, 2]
            case length <= 26:
                return [270, length - 23, 2]
            case length <= 30:
                return [271, length - 27, 2]
            case length <= 34:
                return [272, length - 31, 2]
            case length <= 42:
                return [273, length - 35, 3]
            case length <= 50:
                return [274, length - 43, 3]
            case length <= 58:
                return [275, length - 51, 3]
            case length <= 66:
                return [276, length - 59, 3]
            case length <= 82:
                return [277, length - 67, 4]
            case length <= 98:
                return [278, length - 83, 4]
            case length <= 114:
                return [279, length - 99, 4]
            case length <= 130:
                return [280, length - 115, 4]
            case length <= 162:
                return [281, length - 131, 5]
            case length <= 194:
                return [282, length - 163, 5]
            case length <= 226:
                return [283, length - 195, 5]
            case length <= 257:
                return [284, length - 227, 5]
            case length == 258:
                return [285, length - 258, 0]
            default:
                fatalError("invalid length: \(length)")
            }
        }
        
        var table = Array(repeating: UInt32(0), count: 258)
        var c: [UInt32] = []
        
        for i in 3...258 {
            c = code(UInt32(i))
            table[i] = (c[2] << 24) | (c[1] << 16) | c[0]
        }
        
        return table
    }
    
    private func getDistanceCode_(_ dist: Int) -> [Int] {
        /** @type {!Array.<number>} distance code table. */
        var r: [Int]
        switch true {
        case dist == 1:
            r = [0, dist - 1, 0]
            break
        case dist == 2:
            r = [1, dist - 2, 0]
            break
        case dist == 3:
            r = [2, dist - 3, 0]
            break
        case dist == 4:
            r = [3, dist - 4, 0]
            break
        case dist <= 6:
            r = [4, dist - 5, 1]
            break
        case dist <= 8:
            r = [5, dist - 7, 1]
            break
        case dist <= 12:
            r = [6, dist - 9, 2]
            break
        case dist <= 16:
            r = [7, dist - 13, 2]
            break
        case dist <= 24:
            r = [8, dist - 17, 3]
            break
        case dist <= 32:
            r = [9, dist - 25, 3]
            break
        case dist <= 48:
            r = [10, dist - 33, 4]
            break
        case dist <= 64:
            r = [11, dist - 49, 4]
            break
        case dist <= 96:
            r = [12, dist - 65, 5]
            break
        case dist <= 128:
            r = [13, dist - 97, 5]
            break
        case dist <= 192:
            r = [14, dist - 129, 6]
            break
        case dist <= 256:
            r = [15, dist - 193, 6]
            break
        case dist <= 384:
            r = [16, dist - 257, 7]
            break
        case dist <= 512:
            r = [17, dist - 385, 7]
            break
        case dist <= 768:
            r = [18, dist - 513, 8]
            break
        case dist <= 1024:
            r = [19, dist - 769, 8]
            break
        case dist <= 1536:
            r = [20, dist - 1025, 9]
            break
        case dist <= 2048:
            r = [21, dist - 1537, 9]
            break
        case dist <= 3072:
            r = [22, dist - 2049, 10]
            break
        case dist <= 4096:
            r = [23, dist - 3073, 10]
            break
        case dist <= 6144:
            r = [24, dist - 4097, 11]
            break
        case dist <= 8192:
            r = [25, dist - 6145, 11]
            break
        case dist <= 12288:
            r = [26, dist - 8193, 12]
            break
        case dist <= 16384:
            r = [27, dist - 12289, 12]
            break
        case dist <= 24576:
            r = [28, dist - 16385, 13]
            break
        case dist <= 32768:
            r = [29, dist - 24577, 13]
            break
        default:
            fatalError("invalid distance")
        }
        return r
    }
    
    public func toLz77Array() -> [Int] {
        let length = self.length
        let dist = self.backwardDistance
        var codeArray: [Int] = []
        var pos: Int = 0
        var code: Int
        
        // length
        code = Int(Lz77Match.LengthCodeTable[length])
        codeArray[pos] = code & 0xffff
        codeArray[pos + 1] = (code >> 16) & 0xff
        codeArray[pos + 2] = code >> 24
        pos += 3
        
        // distance
        let distCode = self.getDistanceCode_(dist)
        codeArray[pos] = distCode[0]
        codeArray[pos + 1] = distCode[1]
        codeArray[pos + 2] = distCode[2]
        
        return codeArray
    }
}

/**
 * @Generator
 */
public class RawDeflate {
   
    public var compressionType: CompressionType
    public var lazy: Int = 0
    public var freqsLitLen: [UInt32] = Array()
    public var freqsDist: [UInt32] = Array()
    public var input: [UInt8] = Array()
    public var output: [UInt8] = Array()
    public var op: Int = 0
    public var length: Int = 0
    public var backwardDistance: Int
    public static let Lz77MaxLength: Int = 258
    public static let WindowSize: Int = 0x8000
    public static let MaxCodeLength: Int = 16
    public static let HUFMAX: Int = 286
    public static let Lz77MinLength: Int = 3
    
    public init(_ input: [UInt8], _ opt_params: OptionParams? = nil) {
        self.compressionType = CompressionType.DYNAMIC
        self.lazy = 0
        self.length = 0
        self.backwardDistance = 0
        self.input = input
        self.op = 0
        // option parameters
        if let opt_params = opt_params {
            if (opt_params.lazy != nil) {
                self.lazy = opt_params.lazy!
            }
            if let compressionType = opt_params.compressionType {
                self.compressionType = compressionType
            }
            if let outputBuffer = opt_params.outputBuffer {
                self.output = outputBuffer
            }
            if let outputIndex = opt_params.outputIndex {
                self.op = outputIndex
            }
        }
        
        self.output = [UInt8](repeating: 0, count: 0x8000)
    }
    
    public static var FixedHuffmanTable: [[Int]] {
        var table: [[Int]] = []
        
        for i in 0..<288 {
            switch true {
            case i <= 143:
                table.append([i + 0x030, 8])
                break
            case i <= 255:
                table.append([i - 144 + 0x190, 9])
                break
            case i <= 279:
                table.append([i - 256 + 0x000, 7])
                break
            case i <= 287:
                table.append([i - 280 + 0x0c0, 8])
                break
            default:
                fatalError("invalid literal: \(i)")
            }
        }
        return table
    }
    
    public func compress() -> [UInt8] {
        var blockArray: [UInt8]
        let input: [UInt8] = self.input
        
        switch self.compressionType {
        case CompressionType.NONE:
            // each 65535-Byte (length header: 16-bit)
            var position = 0
            let length = input.count
            while position < length {
                blockArray = Array(input[position..<min((position + 0xffff), length)])
                position += blockArray.count
                self.makeNocompressBlock(blockArray, position == length)
            }
            break
        case CompressionType.FIXED:
            self.output = self.makeFixedHuffmanBlock(input, true)
            self.op = self.output.count
            break
        case CompressionType.DYNAMIC:
            self.output = self.makeDynamicHuffmanBlock(input, true)
            self.op = self.output.count
            break
        default:
            fatalError("invalid compression type")
        }
        return self.output
    }
    
    func makeNocompressBlock(_ blockArray: [UInt8], _ isFinalBlock: Bool) -> [UInt8] {
        var bfinal: Int
        var btype: CompressionType
        var len: Int
        var nlen: Int
        
        var output = self.output
        var op = self.op
        
        // expand buffer
        
        output = Array(self.output)
        while output.count <= op + blockArray.count + 5 {
            output = [UInt8](repeating: 0, count: output.count << 1)
        }
        output.replaceSubrange(0..<self.output.count, with: self.output)
    
        // header
        bfinal = isFinalBlock ? 1 : 0
        btype = CompressionType.NONE
        output[op] = UInt8(bfinal) | (UInt8(btype.rawValue) << 1)
        
        // length
        len = blockArray.count
        nlen = (~len + 0x10000) & 0xffff
        output[op+1] = UInt8(len & 0xff)
        output[op+2] = UInt8((len >> 8) & 0xff)
        output[op+3] = UInt8(nlen & 0xff)
        output[op+4] = UInt8((nlen >> 8) & 0xff)
        op += 5
        // copy buffer
        
        output.replaceSubrange(op..<blockArray.count, with: blockArray)
        op += blockArray.count
        output = Array(output[0..<op])
	

        self.op = op
        self.output = output
        
        return output
    }
    
    public func makeFixedHuffmanBlock(_ blockArray: [UInt8], _ isFinalBlock: Bool) -> [UInt8] {
        var stream = BitStream(Array(self.output), self.op)
        var bfinal: Int = 0
        var btype: CompressionType = CompressionType.FIXED
        var data: [UInt16]
        
        // header
        bfinal = isFinalBlock ? 1 : 0
        
        stream.writeBits(bfinal, 1, true)
        stream.writeBits(btype.rawValue, 2, true)
        
        data = self.lz77(blockArray)
        self.fixedHuffman(data, stream)
        
        return stream.finish()
    }
    
    func makeDynamicHuffmanBlock(_ blockArray: [UInt8], _ isFinalBlock: Bool) -> [UInt8] {
        let stream = BitStream(self.output, self.op)
        var bfinal: Int
        var btype: CompressionType
        var data: [UInt16]
        let hlit: Int = 0
        let hdist: Int = 0
        let hclen: Int = 0
        let hclenOrder = [16, 17, 18, 0, 8, 7, 9, 6, 10, 5, 11, 4, 12, 3, 13, 2, 14, 1, 15]
        var litLenLengths: [UInt8]
        var litLenCodes: [UInt16]
        var distLengths: [UInt8]
        var distCodes: [UInt16]
        var treeSymbols: TreeSymbols
        var treeLengths: [UInt8]
        var transLengths = [Int](repeating: 0, count: 19)
        var treeCodes: [UInt16]
        var code: Int
        var bitlen: Int
        
        // header
        bfinal = isFinalBlock ? 1 : 0
        btype = CompressionType.DYNAMIC
        
        stream.writeBits(bfinal, 1, true)
        stream.writeBits(btype.rawValue, 2, true)
        
        data = self.lz77(blockArray)
        
        litLenLengths = self.getLengths_(self.freqsLitLen, 15)
        litLenCodes = self.getCodesFromLengths_(litLenLengths)
        distLengths = self.getLengths_(self.freqsDist, 7)
        distCodes = self.getCodesFromLengths_(distLengths)
        
        for hlit in stride(from: 286, to: 257, by: -1) {
            if litLenLengths[hlit - 1] == 0 {
                break
            }
        }
        for hdist in stride(from: 30, to: 1, by: -1) {
            if distLengths[hdist - 1] == 0 {
                break
            }
        }
        
        // HCLEN
        treeSymbols = self.getTreeSymbols_(hlit, litLenLengths, hdist, distLengths)
        treeLengths = self.getLengths_(treeSymbols.freqs, 7)
        for i in 0..<19 {
            transLengths[i] = Int(treeLengths[hclenOrder[i]])
        }
        for hclen in stride(from: 19, to: 4, by: -1) {
            if transLengths[hclen - 1] == 0 {
                break
            }
        }
        
        treeCodes = self.getCodesFromLengths_(treeLengths)
        
        stream.writeBits(hlit - 257, 5, true)
        stream.writeBits(hdist - 1, 5, true)
        stream.writeBits(hclen - 4, 4, true)
        for i in 0..<hclen {
            stream.writeBits(transLengths[i], 3, true)
        }
        
        var i : Int = 0
        let il = treeSymbols.codes.count
        while i < il {
            code = Int(treeSymbols.codes[i])
            
            stream.writeBits(Int(treeCodes[code]), Int(treeLengths[code]), true)
            
            // extra bits
            if code >= 16 {
                i += 1
                switch code {
                case 16:
                    bitlen = 2
            	    break;
                case 17:
                    bitlen = 3
           	    break;
                case 18:
                    bitlen = 7
           	    break;
                default:
                    fatalError("invalid code: \(code)")
                }
                
                stream.writeBits(Int(treeSymbols.codes[i]), bitlen, true)
            }
            
            i += 1
        }
        
        self.dynamicHuffman(data, (litLenCodes, litLenLengths), (distCodes, distLengths), stream)
        
        return stream.finish()
    }
    
    func dynamicHuffman(_ dataArray: [UInt16], _ litLen: ([UInt16],[UInt8]), _ dist: ([UInt16],[UInt8]), _ stream: BitStream) -> BitStream {
        var index = 0
        var literal: UInt16
        var code: UInt16
        let litLenCodes = litLen.0
        let litLenLengths = litLen.1
        let distCodes = dist.0
        let distLengths = dist.1
        
        // write symbols to BitStream
        while index < dataArray.count {
            literal = dataArray[index]
            
            // literal or length
            stream.writeBits(Int(litLenCodes[Int(literal)]), Int(litLenLengths[Int(literal)]), true)
            
            // length/distance symbol
            if literal > 256 {
                // length extra
                stream.writeBits(Int(dataArray[index+1]), Int(dataArray[index+2]), true)
                // distance
                code = dataArray[index+3]
                stream.writeBits(Int(distCodes[Int(code)]), Int(distLengths[Int(code)]), true)
                // distance extra
                stream.writeBits(Int(dataArray[index+4]), Int(dataArray[index+5]), true)
                // end marker
                index += 5
            } else if literal == 256 {
                break
            }
            index += 1
        }
        return stream
    }
    
    func fixedHuffman(_ dataArray: [UInt16], _ stream: BitStream) -> BitStream {
        var index: Int = 0
        var literal: UInt16

        while index < dataArray.count {
            literal = dataArray[index]
            stream.writeBits(Int(RawDeflate.FixedHuffmanTable[Int(literal)][0]), Int(RawDeflate.FixedHuffmanTable[Int(literal)][1]))
            if literal > 0x100 {
                stream.writeBits(Int(dataArray[index+1]), Int(dataArray[index+2]), true)
                stream.writeBits(Int(dataArray[index+3]), 5)
                stream.writeBits(Int(dataArray[index+4]), Int(dataArray[index+5]), true)
                index += 5
            } else if literal == 0x100 {
                break
            }
        }
        return stream
    }
    
    func lz77(_ dataArray: [UInt8]) -> [UInt16] {
        
        var matchKey: UInt32 = 0
        var table: [UInt32: [Int]] = Dictionary()
        let windowSize:Int = RawDeflate.WindowSize
        var matchsList: [Int] = [Int]()
        var longestMatch: Lz77Match
        var prevMatch: Lz77Match? = nil
        var lz77buf = [UInt16](repeating: 0, count: dataArray.count * 2)
        var pos:Int = 0
        var skipLength:Int = 0
        var freqsLitLen:[UInt32] = [UInt32](repeating: 0, count: 286)
        var freqsDist:[UInt32] = [UInt32](repeating: 0, count: 30)
        let lazy: Int = self.lazy
        var tmp: UInt8
        
	
        freqsLitLen[256] = 1
        
        func writeMatch(_ match: Lz77Match, _ offset: Int) {
            let lz77Array: [Int] = match.toLz77Array()
            
            for i in 0..<lz77Array.count {
                lz77buf[pos] = UInt16(lz77Array[i])
                pos += 1
            }
            
            freqsLitLen[Int(lz77Array[0])] += 1
            freqsDist[Int(lz77Array[3])] += 1
            skipLength = match.length + offset - 1
            prevMatch = nil
        }
        
        for position in 0..<dataArray.count {
            let end:Int = min(position + RawDeflate.Lz77MinLength, dataArray.count)
            for i in 0..<RawDeflate.Lz77MinLength {
                if (position + i == dataArray.count) {
                    break
                }
                matchKey = (matchKey << 8) | UInt32(dataArray[position + i])
            }
            
            if let matches = table[matchKey] {
                matchsList = matches
            } else {
                
              matchsList = [];
              table[matchKey] = matchsList;
            }
            
            if skipLength > 0 {
                skipLength -= 1;
                matchsList.append(position)
                continue
            }
            
            while matchsList.count > 0 && position - matchsList[0] > windowSize {
                matchsList.removeFirst()
            }
            
            if position + RawDeflate.Lz77MinLength >= dataArray.count {
                if let prevMatch = prevMatch {
                    writeMatch(prevMatch, -1)
                }
                
                for i in position..<dataArray.count {
                    tmp = dataArray[i]
                    lz77buf[pos] = UInt16(tmp)
                    pos += 1
                    freqsLitLen[Int(tmp)] += 1
                }
                
                break
            }
            
            if matchsList.count > 0 {
                longestMatch = self.searchLongestMatch(
        dataArray,
                                                  position,
                                                  matchsList)
                
                if let prevMatch = prevMatch {
                    if prevMatch.length < longestMatch.length {
                        // write previous literal
                        tmp = dataArray[position - 1]
                        lz77buf[pos] = UInt16(tmp)
                        pos += 1
                        freqsLitLen[Int(tmp)] += 1

                        // write current match
                        writeMatch(longestMatch, 0)
                    } else {
                        // write previous match
                        writeMatch(prevMatch, -1)
                    }
                } else if longestMatch.length < lazy {
                    prevMatch = longestMatch
                } else {
                    writeMatch(longestMatch, 0)
                }
            } else if let prevMatch = prevMatch {
                writeMatch(prevMatch, -1)
            } else {
                tmp = dataArray[position]
                lz77buf[pos] = UInt16(tmp)
                pos += 1
                freqsLitLen[Int(tmp)] += 1
            }
            
            matchsList.append(position)
        }
        
        lz77buf[pos] = 256
	pos += 1
        freqsLitLen[256] += 1
        
        self.freqsLitLen = freqsLitLen
        self.freqsDist = freqsDist
        /** @type {!(Uint16Array|Array.<number>)} */
        return Array(lz77buf[0..<pos])
    }

    func searchLongestMatch(_ data: [UInt8], _ position: Int, _ matchList: [Int] ) -> Lz77Match {
        var match: Int
        var currentMatch: Int? = nil
        var matchMax = 0;
        var matchLength: Int
        let dl: Int = data.count
        
    permatch: for i in 0..<matchList.count {
        match = matchList[matchList.count - i - 1]
        matchLength = RawDeflate.Lz77MinLength
        
        if matchMax > RawDeflate.Lz77MinLength {
            for j in stride(from: matchMax, to: RawDeflate.Lz77MinLength+1, by: -1) {
                if data[match + j - 1] != data[position + j - 1] {
                    continue permatch
                }
            }
            matchLength = matchMax
        }
        
        while (
            matchLength < RawDeflate.Lz77MaxLength &&
            position + matchLength < dl &&
            data[match + matchLength] == data[position + matchLength]
        ) {
            matchLength += 1
        }
        
        if matchLength > matchMax {
            currentMatch = match
            matchMax = matchLength
        }
        
        if matchLength == RawDeflate.Lz77MaxLength {
            break
        }
    }
        return Lz77Match(matchMax, position - (currentMatch ?? 0))
    }
    
    func getTreeSymbols_ (_ hlit: Int, _ litlenLengths: [UInt8], _ hdist: Int, _ distLengths: [UInt8] ) -> TreeSymbols {
            var src = [UInt32](repeating: 0, count: hlit + hdist)
            var j: Int = 0
            var runLength: Int
            var result:[UInt32] = [UInt32](repeating: 0, count:286 + 30)
            var nResult: Int
            var rpt: Int
            var freqs:[UInt8] = [UInt8](repeating: 0, count: 19)
        
        for i in 0..<hlit {
            src[j] = UInt32(litlenLengths[i])
            j += 1
        }
        for i in 0..<hdist {
            src[j] = UInt32(distLengths[i])
            j += 1
        }
        
        nResult = 0
        var i:Int = 0 
        while  i < src.count {
            j = 1
            while i + j < src.count && src[i + j] == src[i] {
                j += 1
            }
	    
	     runLength = j
            
            if src[i] == 0 {
                if runLength < 3 {
                    while runLength > 0 {
                        result[nResult] = 0
                        freqs[0] += 1
                        nResult += 1
                        runLength -= 1
                    }
                } else {
                    while runLength > 0 {
                        rpt = min(runLength, 138)
                        
                        if rpt > runLength - 3 && rpt < runLength {
                            rpt = runLength - 3
                        }
                        
                        if rpt <= 10 {
                            result[nResult] = 17
                            result[nResult + 1] = UInt32(rpt - 3)
                            freqs[17] += 1
                            nResult += 2
                        } else {
                            result[nResult] = 18
                            result[nResult + 1] = UInt32(rpt - 11)
                            freqs[18] += 1
                            nResult += 2
                        }
                        
                        runLength -= rpt
                    }
                }
            } else {
                result[nResult] = src[i]
        	nResult += 1;
                freqs[Int(src[i])] += 1
                runLength -= 1
                
                if runLength < 3 {
                    while runLength > 0 {
                        result[nResult] = src[i]
                        freqs[Int(src[i])] += 1
                        nResult += 1
                        runLength -= 1
                    }
                } else {
                    while runLength > 0 {
                        rpt = min(runLength, 6)
                        
                        if rpt > runLength - 3 && rpt < runLength {
                            rpt = runLength - 3
                        }
                        
                        result[nResult] = 16
                        result[nResult + 1] = UInt32(rpt - 3)
                        freqs[16] += 1
                        nResult += 2
                        runLength -= rpt
                    }
                }
            }
            i += j
        }
        return TreeSymbols(codes: Array(result[0..<nResult]), freqs: freqs)
    }
    
    func getLengths_ (
    _  freqs: [Any],
     _ limit: Int 
     ) -> [UInt8] {
        let nSymbols:Int = freqs.count
        let heap:Heap = Heap(length: 2 * RawDeflate.HUFMAX)
        var length:[UInt8] = [UInt8](repeating: 0, count: nSymbols)
        var nodes: [PopObject]
        var values: [UInt32]
        var codeLength: [UInt8]
        
        for i in 0..<nSymbols {
            let intfreqs:Int = Int(freqs[i] as! Int)
            if intfreqs > 0 {
                heap.push(index: i, value: intfreqs)
            }
        }
        nodes = Array(repeating: PopObject(index: 0, value: 0, length: 0), count: heap.length / 2)
        values = [UInt32](repeating: 0, count: heap.length / 2)
        
        if nodes.count == 1 {
            length[heap.pop().index] = 1
            return length
        }
         for i in 0..<heap.length / 2 {
             nodes[i] = heap.pop()
             values[i] = UInt32(nodes[i].value)
         }
         codeLength = self.reversePackageMerge_(values, values.count, limit)
        
        for i in 0..<nodes.count {
            length[nodes[i].index] = codeLength[i]
        }
        
        return length
    }
    
    func reversePackageMerge_(
            _ freqs: Any,
            _ symbols: Int,
            _ limit: Int
        ) -> [UInt8] {
            var minimumCost = [UInt16](repeating: 0, count: limit)
            var flag:[UInt8] = [UInt8](repeating: 0, count: limit)
            var codeLength:[UInt8] = [UInt8](repeating: 0, count: symbols)
            var value = [[UInt32]](repeating: [], count: Int(limit))
            var type = [[UInt32]](repeating: [], count: Int(limit))
            var currentPosition:[Int] = [Int](repeating:0,count:limit)

            var excess:Int = (1 << limit) - symbols
            let half:Int = 1 << (limit - 1)
            var weight: Int = 0
            var next: Int = 0

            func takePackage(_ index: UInt32) {
                let x = type[Int(index)][Int(currentPosition[Int(index)])]

                if x == symbols {
                    takePackage(index + 1)
                    takePackage(index + 1)
                } else {
                    codeLength[Int(x)] -= 1
                }

                currentPosition[Int(index)] += 1
            }

            minimumCost[Int(limit) - 1] = UInt16(symbols)

            for j in 0..<Int(limit) {
                if excess < half {
                    flag[j] = 0
                } else {
                    flag[j] = 1
                    excess -= half
                }
                excess <<= 1
                minimumCost[Int(limit) - 2 - j] =
                ((UInt16(minimumCost[Int(limit) - 1 - j] / 2)) as UInt16) + UInt16(symbols)
            }
            minimumCost[0] = UInt16(flag[0])

            value[0] = [UInt32](repeating: 0, count: Int(minimumCost[0]))
            type[0] = [UInt32](repeating: 0, count: Int(minimumCost[0]))
            for j in 1..<Int(limit) {
                if minimumCost[j] > 2 * minimumCost[j - 1] + UInt16(flag[j]) {
                    minimumCost[j] = 2 * minimumCost[j - 1] + UInt16(flag[j])
                }
                value[j] = [UInt32](repeating: 0, count: Int(minimumCost[j]))
                type[j] = [UInt32](repeating: 0, count: Int(minimumCost[j]))
            }

            for i in 0..<Int(symbols) {
                codeLength[i] = UInt8(limit)
            }

            for t in 0..<Int(minimumCost[Int(limit) - 1]) {
                value[Int(limit) - 1][t] = (freqs as! [UInt32])[t]
                type[Int(limit) - 1][t] = UInt32(t)
            }

            for i in 0..<Int(limit) {
                currentPosition[i] = 0
            }

            if flag[Int(limit) - 1] == 1 {
                codeLength[0] -= 1
                currentPosition[Int(limit) - 1] += 1
            }

            var j = Int(limit) - 2
            while j >= 0 {
                var i: Int = 0
                weight = 0
                next = currentPosition[j + 1]
                var t:Int = 0
                while t < minimumCost[j] {
                    weight = Int(value[j + 1][Int(next)] + value[j + 1][Int(next) + 1])

                    if weight > (freqs as! [Int])[Int(i)] {
                        value[j][t] = UInt32(weight)
                        type[j][t] = UInt32(symbols)
                        next += 2
                    } else {
                        value[j][t] = (freqs as! [UInt32])[i]
                        type[j][t] = UInt32(i)
                        i += 1
                    }
                    t += 1
                }
                
                currentPosition[j] = 0
                if flag[j] == 1 {
                    takePackage(UInt32(j))
                }
                j -= 1
            }

            return codeLength
        }
    
    public func getCodesFromLengths_(_ lengths: [UInt8]) -> [UInt16] {
        var codes = [UInt16](repeating: 0, count: lengths.count)
        var count = Array<Int>()
        var startCode = Array<Int>()
        var code: Int = 0
        
        // Count the codes of each length.
        for i in 0..<lengths.count {
            count[Int(lengths[i])] = (count[Int(lengths[i])] | 0) + 1;
        }
        
        // Determine the starting code for each length block.
        for i in 1...Int(RawDeflate.MaxCodeLength) {
            startCode[i] = code
            code += count[i] | 0
            code <<= 1
        }
        
        // Determine the code for each symbol. Mirrored, of course.
        for i in 0..<lengths.count {
            code = startCode[Int(lengths[i])]
            startCode[Int(lengths[i])] += 1
            codes[i] = 0
            
            for j in 0..<Int(lengths[i]) {
                codes[i] = (codes[i] << 1) | UInt16((code & 1))
                code >>= 1
            }
        }
        return codes
    }
}

