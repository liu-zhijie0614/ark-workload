
class BitStream {
    public static let DefaultBlockSize = 0x8000
    private var index:Int
    private var bitindex: Int
    private var buffer:[UInt8]
    public static var ReverseTable:[UInt8] = [UInt8]()
    
    init(_ buffer:[UInt8],_ bufferPosition:Int) {
        self.index = bufferPosition
        self.bitindex = 0
        self.buffer = buffer
        BitStream.ReverseTable = getReverseTable()
        if self.buffer.count * 2 <= self.index {
            fatalError("invalid index")
        } else if (buffer.count <= index) {
            expandBuffer()
        }
    }
    
    func expandBuffer() -> [UInt8] {
        let oldbuf = self.buffer;
        let il = oldbuf.count;
      // copy buffer
        var uint8Buffer = [UInt8](repeating: 0, count: (il << 1))
        uint8Buffer.replaceSubrange(0..<oldbuf.count, with: oldbuf)
        self.buffer = uint8Buffer
        return self.buffer;
    }
    
    public func writeBits(_ number:Int, _ n:Int, _ reverse:Bool = false) {
        var numberTemp: Int = number
        var buffer: [UInt8] = self.buffer
        var index: Int = self.index
        var bitindex: Int = self.bitindex
        
        var current = buffer[index]
        
        let rev32_ = { (num:Int) -> Int32 in
            let A = BitStream.ReverseTable[num & 0xFF] << 24
            let B = BitStream.ReverseTable[num >> 8 & 0xFF] << 16
            let C = BitStream.ReverseTable[num >> 16 & 0xFF] << 8
            let D = BitStream.ReverseTable[num >> 24 & 0xff]
            return Int32(A|B|C|D)
        }
        
        if (reverse && n > 1) {
            numberTemp = n > 8 ? Int(rev32_(number) >> (32 - n)):Int((BitStream.ReverseTable[number]) >> (8 - n));
        }
        
        if n + bitindex < 8 {
            current = (current << n) | UInt8(numberTemp)
            bitindex += n
        } else {
            for i in 0..<n {
                current = (current << 1) | UInt8(((number >> n - i - 1) & 1));
               
                // next byte
                if (bitindex == 8) {
		 
                 bitindex = 0;
                 buffer[index] = BitStream.ReverseTable[Int(current)];
                 index += 1
                 current = 0;
                    
                 // expand
                 if (index == buffer.count) {
                     buffer = self.expandBuffer()
                 }
               }
            }
        }
        buffer[index] = current;
        self.buffer = buffer;
        self.bitindex = bitindex;
        self.index = index;
    }
    
    public func finish() -> [UInt8] {
      var buffer: [UInt8] = self.buffer
        var index:Int = self.index
        var output:[UInt8];

        if (self.bitindex > 0) {
            buffer[index] <<= UInt8(8 - self.bitindex)
            buffer[index] = BitStream.ReverseTable[Int(buffer[index])];
            index += 1;
      }
	// array truncation

      output = buffer.ranges(of: 0...index as! [UInt8]) as! [UInt8]

      return output;
    }

    private func getReverseTable() -> [UInt8] {
      
        var table = [UInt8](repeating: 0, count: 256)
        
        for i in 0..<256 {
            table[i] = UInt8({ (n:Int) -> Int8 in
                var N = n
                var r = n
                var s = 7
                N >>= 1
                while N != 0
                {
                    r <<= 1
                    r |= N & 1
                    s -= 1
                    N >>= 1
                }
                return Int8((r << s & 0xff) >> 0)
            }(Int(i)))
        }
      return table;
    }
}
