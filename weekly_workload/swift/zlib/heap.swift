struct PopObject {
    var index: Int
    var value: Int
    var length: Int
}

class Heap {
    var buffer: [UInt16]
    var length: Int

    init(length: Int) {
        self.buffer = [UInt16](repeating: 0, count: length * 2);
        self.length = 0
    }

    func getParent(_ index: Int) -> Int {
        return ((index - 2)/4 | 0) * 2
    }

    func getChild(_ index: Int) -> Int {
        return 2 * index + 2
    }

    func push(index: Int, value: Int) -> Int {
        var current:Int = self.length
        var parent:Int
        var heap: [UInt16] = buffer
        var swap: UInt16

        heap[self.length] = UInt16(value)
        heap[self.length + 1] = UInt16(index)
        self.length += 2

        while current > 0 {
            parent = self.getParent(current)

            if heap[current] > heap[parent] {
                swap = heap[current]
                heap[current] = heap[parent]
                heap[parent] = swap

                swap = heap[current + 1]
                heap[current + 1] = heap[parent + 1]
                heap[parent + 1] = swap

                current = parent
            } else {
                break
            }
        }
        return self.length
    }

    func pop() -> PopObject {
        var index:Int = 0
        var value:Int = 0
        var swap: UInt16 = 0
        var current:Int = 0
        var parent: Int = 0
        var heap:[UInt16] = self.buffer

        value = Int(heap[0])
        index = Int(heap[1])

        self.length -= 2
        heap[0] = heap[self.length]
        heap[1] = heap[self.length + 1]

        parent = 0
        while true {
            current = self.getChild(parent)

            if current >= self.length {
                break
            }

            if current + 2 < self.length && heap[current + 2] > heap[current] {
                current += 2
            }

            if heap[current] > heap[parent] {
                swap = heap[parent]
                heap[parent] = heap[current]
                heap[current] = UInt16(swap)

                swap = heap[parent + 1]
                heap[parent + 1] = heap[current + 1]
                heap[current + 1] = swap
            } else {
                break
            }

            parent = current
        }

        return PopObject(index: index, value: value, length: self.length)
    }
}

