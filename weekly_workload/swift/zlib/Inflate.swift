
struct InOptionParams {
    var index: Int?
    var bufferSize: Int?
    var bufferType: BufferType?
    var resize: Bool?
}

struct ExpandOption {
    var fixRatio: Int?
    var addRatio: Int?
}

enum BufferType {
    case BLOCK
    case ADPTIVE
}

/*
 *@Generator
 */
struct RawInflate {
    static let ZLIB_RAW_INFLATE_BUFFER_SIZE:Int = 0x8000
    static let buildHuffmanTable:(_ lengths: [UInt8]) -> ([UInt32], Int, Int) = Huffman.buildHuffmanTable
    static let MaxBackwardLength:Int = 32768
    static let MaxCopyLength:Int = 258
    var currentLitlenTable: Any? = nil
    
    static let order: [UInt16] = {
        let table:[UInt16] = [16, 17, 18, 0, 8, 7, 9, 6, 10, 5, 11, 4, 12, 3, 13, 2, 14, 1, 15]
        return Array<UInt16>(table)
    }()
    
    static let LengthCodeTable:([UInt16]) = {(_ table:[UInt16]) in
        return Array<UInt16>(table)
    }([
        0x0003, 0x0004, 0x0005, 0x0006, 0x0007, 0x0008, 0x0009, 0x000a, 0x000b,
        0x000d, 0x000f, 0x0011, 0x0013, 0x0017, 0x001b, 0x001f, 0x0023, 0x002b,
        0x0033, 0x003b, 0x0043, 0x0053, 0x0063, 0x0073, 0x0083, 0x00a3, 0x00c3,
        0x00e3, 0x0102, 0x0102, 0x0102
    ])
    
    static let LengthExtraTable: [UInt8] = {(_ table:[UInt8]) in
        return Array<UInt8>(table)
    }([
        0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 2, 2, 2, 2, 3, 3, 3, 3, 4, 4, 4, 4, 5, 5,
        5, 5, 0, 0, 0
    ])
    
    static let DistCodeTable: [UInt16] = {(_ table:[UInt16]) in
        return Array<UInt16>(table)
    }([
        0x0001, 0x0002, 0x0003, 0x0004, 0x0005, 0x0007, 0x0009, 0x000d, 0x0011,
        0x0019, 0x0021, 0x0031, 0x0041, 0x0061, 0x0081, 0x00c1, 0x0101, 0x0181,
        0x0201, 0x0301, 0x0401, 0x0601, 0x0801, 0x0c01, 0x1001, 0x1801, 0x2001,
        0x3001, 0x4001, 0x6001
    ])
    
    static let DistExtraTable: [UInt8] = {
        let table:[UInt8] = [
            0, 0, 0, 0, 1, 1, 2, 2, 3, 3, 4, 4, 5, 5, 6, 6, 7, 7, 8, 8, 9, 9, 10, 10, 11,
            11, 12, 12, 13, 13
        ]
        return Array<UInt8>(table)
    }()
    
    static let FixedLiteralLengthTable: ([UInt32], Int, Int) = {
        var lengths = [UInt8](repeating: 0, count: 288)
        
        for i in 0..<lengths.count {
            lengths[i] =
            (i <= 143) ? 8 :
            (i <= 255) ? 9 :
            (i <= 279) ? 7 :
            8
        }
        return RawInflate.buildHuffmanTable(lengths)
    }()
    static let FixedDistanceTable: ([UInt32], Int, Int) = {
        var lengths = [UInt8](repeating: 5, count: 30)

        for i in 0..<lengths.count {
            lengths[i] = 5;
          }

        return RawInflate.buildHuffmanTable(lengths)
    }()
    var buffer: [UInt8]? = nil
    var blocks: [[UInt8]]
    var bufferSize: Int
    var totalpos: Int
    var ip: Int
    var bitsbuf: UInt
    var bitsbuflen: Int
    var input: [UInt8]
    var output: [UInt8]
    var op: Int
    var bfinal: Bool = false
    var bufferType: BufferType = BufferType.ADPTIVE
    var resize: Bool = false
    
    init(_ input: [UInt8], _ optParams: InOptionParams? = nil) {
        self.blocks = []
        self.bufferSize = RawInflate.ZLIB_RAW_INFLATE_BUFFER_SIZE
        self.totalpos = 0
        self.ip = 0
        self.bitsbuf = 0
        self.bitsbuflen = 0
        self.input = Array<UInt8>(input)
        self.bfinal = false
        self.bufferType = BufferType.ADPTIVE
        self.resize = false
        
        // option parameters
        if let optParams = optParams {
            if let index = optParams.index {
                self.ip = index
            }
            if let bufferSize = optParams.bufferSize {
                self.bufferSize = bufferSize
            }
            if let bufferType = optParams.bufferType {
                self.bufferType = bufferType
            }
            if let resize = optParams.resize {
                self.resize = resize
            }
        }
        
        // initialize
        switch self.bufferType {
        case BufferType.BLOCK:
            self.op = RawInflate.MaxBackwardLength
            self.output = [UInt8](repeating: 0, count: 
                RawInflate.MaxBackwardLength + self.bufferSize + RawInflate.MaxCopyLength
	    )
            break
        case BufferType.ADPTIVE:
            self.op = 0
            self.output = [UInt8](repeating: 0, count: self.bufferSize)
            break
        default:
            fatalError("invalid inflate mode")
        }
    }
    
    mutating func decompress() -> [UInt8] {
        while !self.bfinal {
            self.parseBlock()
        }
        switch self.bufferType {
        case BufferType.BLOCK:
            return self.concatBufferBlock()
        case BufferType.ADPTIVE:
            return self.concatBufferDynamic()
        default:
            fatalError("invalid inflate mode")
        }
    }
    
    mutating func parseBlock() {
        var hdr:UInt = self.readBits(3)
        
        // BFINAL
        if (hdr & 0x1) != 0 {
            self.bfinal = true
        }
        
        // BTYPE
        hdr >>= 1
        switch hdr {
            // uncompressed
        case 0:
            self.parseUncompressedBlock()
            break
            // fixed huffman
        case 1:
            self.parseFixedHuffmanBlock()
            break
            // dynamic huffman
        case 2:
            self.parseDynamicHuffmanBlock()
            break
            // reserved or other
        default:
            fatalError("unknown BTYPE: \(hdr)")
        }
    }
    
    mutating func readBits(_ length: Int) -> UInt {
        var bitsbuf: UInt = self.bitsbuf
        var bitsbuflen: Int = self.bitsbuflen
        let input: [UInt8] = self.input
        var ip: Int = self.ip
        /** @type {int} */
        let inputLength: Int = input.count
        /** @type {UInt} input and output byte. */
        var octet: UInt
        
        // input byte
        if ip + ((length - bitsbuflen + 7) >> 3) >= inputLength {
            fatalError("input buffer is broken")
        }
        
        // not enough buffer
        while bitsbuflen < length {
            bitsbuf |= UInt(input[ip]) << bitsbuflen
            ip += 1
            bitsbuflen += 8
        }
        
        // output byte
        octet = bitsbuf & ((1 << length) - 1)
        bitsbuf >>= length
        bitsbuflen -= length
        
        self.bitsbuf = bitsbuf
        self.bitsbuflen = bitsbuflen
        self.ip = ip
        
        return octet
    }
    
    mutating func readCodeByTable(_ table: Any) -> Int {
        var bitsbuf:UInt = self.bitsbuf
        var bitsbuflen:Int = self.bitsbuflen
        let input:[UInt8] = self.input
        var ip:Int = self.ip
        
        let inputLength:Int = input.count
        let codeTable:[UInt32] = (table as! ([UInt32],Int,Int)).0;
        let maxCodeLength:Int = (table as! [Int])[1]
        var codeWithLength: UInt32
        var codeLength: Int
        
        // not enough buffer
        while bitsbuflen < maxCodeLength {
            if ip >= inputLength {
                break
            }
            bitsbuf |= UInt(input[ip]) << bitsbuflen
            ip += 1
            bitsbuflen += 8
        }
        
        // read max length
        codeWithLength = codeTable[Int(bitsbuf & UInt((1 << maxCodeLength) - 1))]
        codeLength = Int(codeWithLength >> 16)
        
        if codeLength > bitsbuflen {
            fatalError("invalid code length: \(codeLength)")
        }
        
        bitsbuf >>= codeLength
        bitsbuflen -= codeLength
        ip += 1
        
        self.bitsbuf = bitsbuf
        self.bitsbuflen = bitsbuflen
        self.ip = ip
        
        return Int(codeWithLength & 0xffff)
    }
    
    mutating func parseUncompressedBlock() ->Void {
        let input:[UInt8] = self.input
        var ip:Int = self.ip
        var output: [UInt8] = self.output
        var op:Int = self.op
        
        let inputLength:Int = input.count
        var len: Int
        var nlen: Int
        let olength:Int = output.count
        var preCopy: Int
        
        // skip buffered header bits
        self.bitsbuf = 0
        self.bitsbuflen = 0
        
        // len
        if ip + 1 >= inputLength {
            fatalError("invalid uncompressed block header: LEN")
        }
        len = Int(input[ip]) | (Int(input[ip + 1]) << 8)
        ip += 2
        
        // nlen
        if ip + 1 >= inputLength {
            fatalError("invalid uncompressed block header: NLEN")
        }
        nlen = Int(input[ip]) | (Int(input[ip + 1]) << 8)
        ip += 2
        
        // check len & nlen
        if len == ~nlen {
            fatalError("invalid uncompressed block header: length verify")
        }
        
        // check size
        if ip + len > input.count {
            fatalError("input buffer is broken")
        }
        
        // expand buffer
        switch self.bufferType {
        case BufferType.BLOCK:
            // pre copy
            while op + len > output.count {
                preCopy = olength - op
                len -= preCopy
                
                output.replaceSubrange(op..<op + preCopy, with: input[ip..<ip + preCopy])
                op += preCopy
                ip += preCopy
                
                self.op = op
                output = self.expandBufferBlock()
                op = self.op
            }
            break
        case BufferType.ADPTIVE:
            while op + len > output.count {                
                output = self.expandBufferAdaptive(ExpandOption(fixRatio: 2))
            }
            break
        default:
            fatalError("invalid inflate mode")
        }
        
        // copy

        output.replaceSubrange(op..<op + len, with: input[ip..<ip + len])
        op += len
        ip += len
        
        self.ip = ip
        self.op = op
        self.output = output
    }
    
    mutating func parseFixedHuffmanBlock() {
        switch self.bufferType {
        case BufferType.ADPTIVE:
            self.decodeHuffmanAdaptive(
                RawInflate.FixedLiteralLengthTable,
                RawInflate.FixedDistanceTable
	    )
            break
        case BufferType.BLOCK:
            self.decodeHuffmanBlock(
                RawInflate.FixedLiteralLengthTable,
                RawInflate.FixedDistanceTable
            )
            break
        default:
            fatalError("invalid inflate mode");
        }
    }
    
    mutating func parseDynamicHuffmanBlock() {
        let hlit:Int = Int(self.readBits(5)) + 257
        let hdist:Int = Int(self.readBits(5)) + 1
        let hclen:Int = Int(self.readBits(4)) + 4
        var codeLengths:[UInt8] = [UInt8](repeating: 0, count: RawInflate.order.count)
        var codeLengthsTable: ([UInt32], Int, Int)
        var litlenTable : Any?
        var distTable: Any?
        var lengthTable: [UInt8]
        var code: UInt8
        var prev: UInt8? = nil
        var repeats: Int
        // decode code lengths
        for i in 0..<hclen {
            codeLengths[Int(RawInflate.order[i])] = UInt8(self.readBits(3))
        }

	// decode length table
        codeLengthsTable = RawInflate.buildHuffmanTable(codeLengths)
        lengthTable = [UInt8](repeating: 0, count: hlit + hdist)
        
        var i = 0
        while(i < (hlit + hdist)) {
            code = UInt8(self.readCodeByTable(codeLengthsTable))
            switch code {
            case 16:
                repeats = 3 + Int(self.readBits(2))
                while repeats > 0 {
                    lengthTable[i] = prev ?? 0
                    i += 1
                    repeats -= 1
                }
                break
            case 17:
                repeats = 3 + Int(self.readBits(3))
                while repeats > 0 {
                    lengthTable[i] = 0
                    i += 1
                    repeats -= 1
                }
                prev = 0
                break
            case 18:
                repeats = 11 + Int(self.readBits(7))
                while repeats > 0 {
                    lengthTable[i] = 0
                    repeats -= 1
                    i += 1
                }
                prev = 0
                break
            default:
                lengthTable[i] = code
                prev = code
                i += 1
                break
            }
        }
        
        litlenTable = RawInflate.buildHuffmanTable(Array(lengthTable[0...hlit]))
        
        distTable = RawInflate.buildHuffmanTable(Array(lengthTable[hlit...lengthTable.count-1]))
        
           switch (self.bufferType) {
             case BufferType.ADPTIVE:
               self.decodeHuffmanAdaptive(litlenTable, distTable);
               break;
           case BufferType.BLOCK:
               self.decodeHuffmanBlock(litlenTable, distTable);
               break
             default:
               fatalError("invalid inflate mode")
           }
    }
    
    mutating func decodeHuffmanBlock(_ litlen: Any, _ dist: Any) {
        var output:[UInt8] = self.output
        var op:Int = self.op

        self.currentLitlenTable = litlen
        
        let olength = output.count - RawInflate.MaxCopyLength
        var code: Int
        var ti: Int
        var codeDist: Int
        var codeLength: UInt32

        let lengthCodeTable = RawInflate.LengthCodeTable
        let lengthExtraTable = RawInflate.LengthExtraTable
        let distCodeTable = RawInflate.DistCodeTable
        let distExtraTable = RawInflate.DistExtraTable

        code = self.readCodeByTable(litlen)
        while code != 256 {
            // literal
            if code < 256 {
                if op >= olength {
                    self.op = op
                    output = self.expandBufferBlock()
                    op = self.op
                }
                output[op] = UInt8(code)
                op += 1
                continue
            }

            // length code
            ti = code - 257
            codeLength = UInt32(lengthCodeTable[Int(ti)])
            if lengthExtraTable[Int(ti)] > 0 {
                codeLength += UInt32(self.readBits(Int(lengthExtraTable[Int(ti)])))
            }

            // dist code
            code = self.readCodeByTable(dist)
            codeDist = Int(distCodeTable[code])
            if distExtraTable[code] > 0 {
                codeDist += Int(self.readBits(Int(distExtraTable[code])))
            }

            // lz77 decode
            if op >= olength {
                self.op = op
                output = self.expandBufferBlock()
                op = self.op
            }
            while codeLength > 0 {
                output[op] = output[Int(op) - Int(codeDist)]
                op += 1
                codeLength -= 1
            }
            code = self.readCodeByTable(litlen)
        }

        while self.bitsbuflen >= 8 {
            self.bitsbuflen -= 8
            self.ip -= 1
        }
        self.op = op
    }
    
    mutating func decodeHuffmanAdaptive(_ litlen: Any, _ dist: Any) {
        var output: [UInt8] = self.output
        var op: Int = self.op
	
        self.currentLitlenTable = litlen

        var olength: Int = output.count
        var code: Int
        var ti: Int
        var codeDist: UInt32
        var codeLength: Int

        let lengthCodeTable: [UInt16] = RawInflate.LengthCodeTable
        let lengthExtraTable: [UInt8] = RawInflate.LengthExtraTable
        let distCodeTable: [UInt16] = RawInflate.DistCodeTable
        let distExtraTable: [UInt8] = RawInflate.DistExtraTable
        
        code = self.readCodeByTable(litlen)
        while code != 256 {
            // literal
            if code < 256 {
                if op >= olength {
                    output = self.expandBufferAdaptive(nil)
                    olength = output.count
                }
                output[Int(op)] = UInt8(code)
                op += 1
                continue
            }

            // length code
            ti = code - 257
            codeLength = Int(lengthCodeTable[ti])
            if lengthExtraTable[ti] > 0 {
                codeLength += Int(self.readBits(Int(lengthExtraTable[ti])))
            }

            // dist code
            code = self.readCodeByTable(dist)
            codeDist = UInt32(distCodeTable[code])
            if distExtraTable[Int(code)] > 0 {
                codeDist += UInt32(self.readBits(Int(distExtraTable[Int(code)])))
            }

            // lz77 decode
            if op + codeLength > olength {
                output = self.expandBufferAdaptive(nil)
                olength = output.count
            }
            while codeLength > 0 {
                output[Int(op)] = output[Int(op) - Int(codeDist)]
                op += 1
                codeLength -= 1
            }
            code = self.readCodeByTable(litlen)
        }

        while self.bitsbuflen >= 8 {
            self.bitsbuflen -= 8
            self.ip -= 1
        }
        self.op = Int(op)
    }
    
    mutating func expandBufferBlock() -> [UInt8] {

        var buffer = [UInt8](repeating: 0, count: 
	   Int(self.op - RawInflate.MaxBackwardLength)
	);
        let backward: Int = self.op - RawInflate.MaxBackwardLength

        var output: [UInt8] = self.output
        // copy to output buffer
        buffer.replaceSubrange(Int(RawInflate.MaxBackwardLength)..<Int(buffer.count), with: output)


        self.blocks.append(buffer)
        self.totalpos += buffer.count

        // copy to backward buffer
        output.replaceSubrange(backward..<Int(backward+RawInflate.MaxBackwardLength), with: output)

        
        self.op = RawInflate.MaxBackwardLength

        return output
    }
    
    mutating func expandBufferAdaptive(_ optParam: ExpandOption?) -> [UInt8] {
        var buffer: [UInt8]
        var ratio:Int = (self.input.count / self.ip + 1) | 0
        var maxHuffCode: Int
        var newSize: Int
        var maxInflateSize: Int

        let input: [UInt8] = self.input
        let output: [UInt8] = self.output
        
        if let optParam = optParam {
            if let fixRatio = optParam.fixRatio {
                ratio = fixRatio
            }
            if let addRatio = optParam.addRatio {
                ratio += addRatio
            }
        }

        // calculate new buffer size
        if ratio < 2 {
            maxHuffCode = (input.count - self.ip) / Int(((self.currentLitlenTable as! Array<UInt16>)[2]))
            maxInflateSize = (Int(maxHuffCode) / 2 * 258) | 0
            newSize = maxInflateSize < output.count ? output.count + maxInflateSize : output.count << 1
        } else {
            newSize = output.count * ratio
        }

        // buffer expansion
        buffer = [UInt8](repeating: 0, count: newSize)
        buffer.replaceSubrange(0..<output.count, with: output)


        self.output = buffer

        return self.output
    }

    mutating func concatBufferBlock() -> [UInt8] {
        var pos: Int = 0
        let limit: Int = self.totalpos + (self.op - RawInflate.MaxBackwardLength)
        let output: [UInt8] = self.output
        let blocks: [[UInt8]] = self.blocks
        var buffer: [UInt8] = [UInt8](repeating: 0, count: limit)
        
        // Single buffer
        if blocks.count == 0 {
            return Array(output[RawInflate.MaxBackwardLength..<self.op])
        }
        
        // Copy to buffer        
        for i in 0..<blocks.count {
            let block: [UInt8] = blocks[i]
            for j in 0..<block.count {
                buffer[pos] = block[j]
                pos += 1
            }
        }

        // Current buffer
        for i in RawInflate.MaxBackwardLength..<self.op {
            buffer[pos] = output[i]
            pos += 1
        }

        self.blocks = []
        self.buffer = buffer

        return self.buffer ?? Array<UInt8>()
    }

    mutating func concatBufferDynamic() -> [UInt8] {
        var tempBuffer: [UInt8]
        let op: Int = self.op

        
        if self.resize {
            tempBuffer = [UInt8](repeating: 0, count: op)
            tempBuffer.replaceSubrange(0..<op, with: self.output)
        } else {
            return Array(self.output[0...op-1])
        }


        self.buffer = tempBuffer

        return self.buffer ?? Array<UInt8>()
    }
}
