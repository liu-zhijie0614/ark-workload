
class Huffman {
    init() {
        
    }

    static func buildHuffmanTable(_ lengths: [UInt8]) -> ([UInt32], Int, Int) {
        let listSize:Int = lengths.count
        var maxCodeLength:Int = 0
        var minCodeLength:Int = Int.max
        var size: Int
        var table: [UInt32]
        var bitLength:Int = 1
        var code:Int = 0
        var skip:Int = 2
        var reversed: UInt32
        var rtemp: UInt32

        var j: Int
        var value: UInt32
        
        for i in 0..<listSize {
            if lengths[i] > maxCodeLength {
                maxCodeLength = Int(lengths[i])
            }
            if lengths[i] < minCodeLength {
                minCodeLength = Int(lengths[i])
            }
        }
        
        size = 1 << maxCodeLength
        table = [UInt32](repeating: 0, count: size)
        
        while bitLength <= maxCodeLength {
            var i = 0
            while i < listSize {
                if Int(lengths[i]) == bitLength {
                    reversed = 0
                    rtemp = UInt32(code)
                    j = 0
                    
                    while j < bitLength {
                        reversed = (reversed << 1) | (rtemp & 1)
                        rtemp >>= 1
                        j += 1
                    }
                    
                    value = UInt32((bitLength << 16) | i)
                    j = Int(reversed)
                    
                    while j < size {
                        table[j] = value
                        j += skip
                    }
                    
                    code += 1
                }
                i += 1
            }
            
            bitLength += 1
            code <<= 1
            skip <<= 1
        }
        
        return (table, maxCodeLength, minCodeLength)
    }
}
