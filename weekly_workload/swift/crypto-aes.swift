import Glibc

let inDebug = true
func log(_ str: String) {
    if inDebug {
        print(str)
    }
}
func currentTimestamp13() -> Double {
    return Timer().getTime() / 1000
}

/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -  */

/**
 ** AES Cipher function: encrypt 'input' with Rijndael algorithm
 *
 **   takes   byte-array 'input' (16 bytes)
 **          2D byte-array key schedule 'w' (Nr+1 x Nb bytes)
 *
 **   applies Nr rounds (10/12/14) using key schedule w for 'add round key' stage
 *
 **   returns byte-array encrypted value (16 bytes)
 **/
func Cipher(_ input: [Int16], _ w:[[Int16]]) -> [Int16] {    // main Cipher func [§5.1]
    let Nb: Int = 4;               // block size (in words): no of columns in state (fixed at 4 for AES)
    let Nr = w.count / Nb - 1; // no of rounds: 10/12/14 for 128/192/256-bit keys

    var state:[[Int16]] = [
        [Int16](repeating: 0, count: Nb),
        [Int16](repeating: 0, count: Nb),
        [Int16](repeating: 0, count: Nb),
        [Int16](repeating: 0, count: Nb)
    ]   // initialise 4xNb byte-array 'state' with input [§3.4]

    for i in 0..<4*Nb {
        state[i%4][Int(floor(Double(i/4)))] = input[i]
    }

    state = AddRoundKey(state, w, 0, Nb);
    for round in 1..<Nr {
        state = SubBytes(state, Nb);
        state = ShiftRows(state, Nb);
        state = MixColumns(state, Nb);
        state = AddRoundKey(state, w, round, Nb);
    }

    state = SubBytes(state, Nb);
    state = ShiftRows(state, Nb);
    state = AddRoundKey(state, w, Nr, Nb);

    var output = [Int16](repeating: 0, count: 4*Nb)// convert state to 1-d array before returning [§3.4]
    for i in 0..<4*Nb {
        output[i] = state[i%4][Int(floor(Double(i/4)))]
    }
    return output;
}

func SubBytes(_ s1:[[Int16]], _ Nb: Int) -> [[Int16]]{    // apply SBox to state S [§5.1.1]
    var s = s1
    for r in 0..<4 {
        for c in 0..<Nb {
            s[r][c] = Int16(Sbox[Int(s[r][c])])
        }
    }
    return s;
}

func ShiftRows(_ s1:[[Int16]], _ Nb: Int) -> [[Int16]]{    // shift row r of state S left by r bytes [§5.1.2]
    var s = s1
    var t = [Int16](repeating: 0, count: 4);
    for r in 1..<4 {
        for c in 0..<4 {
            t[c] = s[r][(c+r)%Nb] // shift into temp copy
        }
        for c in 0..<4 {
            s[r][c] = t[c] // and copy back
        }
    } // note that this will work for Nb=4,5,6, but not 7,8 (always 4 for AES):
    return s // see fp.gladman.plus.com/cryptography_technology/rijndael/aes.spec.311.pdf
}

func MixColumns(_ s1: [[Int16]], _ Nb: Int) -> [[Int16]] {   // combine bytes of each col of state S [§5.1.3]
    var s = s1
    for c in 0..<4 {
        var a = [Int16](repeating: 0, count: 4)  // 'a' is a copy of the current column from 's'
        var b = [Int16](repeating: 0, count: 4)  // 'b' is a•{02} in GF(2^8)
        for i in 0..<4 {
            a[i] = s[i][c];
            b[i] = ((s[i][c]&0x80) != 0) ? s[i][c]<<1 ^ 0x011b : s[i][c]<<1;
        }
        // a[n] ^ b[n] is a•{03} in GF(2^8)
        s[0][c] = b[0] ^ a[1] ^ b[1] ^ a[2] ^ a[3]; // 2*a0 + 3*a1 + a2 + a3
        s[1][c] = a[0] ^ b[1] ^ a[2] ^ b[2] ^ a[3]; // a0 * 2*a1 + 3*a2 + a3
        s[2][c] = a[0] ^ a[1] ^ b[2] ^ a[3] ^ b[3]; // a0 + a1 + 2*a2 + 3*a3
        s[3][c] = a[0] ^ b[0] ^ a[1] ^ a[2] ^ b[3]; // 3*a0 + a1 + a2 + 2*a3
    }
    return s
}

func AddRoundKey(_ state1: [[Int16]], _ w: [[Int16]], _ rnd: Int, _ Nb: Int) -> [[Int16]] {  // xor Round Key into state S [§5.1.4]
    var state = state1
    for r in 0..<4 {
        for c in 0..<Nb {
            state[r][c] ^= w[rnd*4+c][r]
        }
    }
    return state;
}

func KeyExpansion(_ key1: [Int16]) -> [[Int16]] {  // generate Key Schedule (byte-array Nr+1 x Nb) from Key [§5.2]
    let key = key1
    let Nb = 4;            // block size (in words): no of columns in state (fixed at 4 for AES)
    let Nk = key.count / 4  // key length (in words): 4/6/8 for 128/192/256-bit keys
    let Nr = Nk + 6;       // no of rounds: 10/12/14 for 128/192/256-bit keys

    var w = [[Int16]](repeating: [], count: (Nb*(Nr+1)))
    var temp = [Int16](repeating: 0, count: 4)

    for i in 0..<Nk {
        let r = [key[4*i], key[4*i+1], key[4*i+2], key[4*i+3]]
        w[i] = r
    }
    for i in Nk..<(Nb*(Nr+1)) {
        w[i] = [Int16](repeating: 0, count: 4)
        for t in 0..<4 {
            temp[t] = w[i-1][t]
        }
        if (i % Nk == 0) {
            temp = SubWord(RotWord(temp));
            for t in 0..<4 {
                temp[t] ^= Int16(Rcon[i/Nk][t])
            }
        } else if (Nk > 6 && i%Nk == 4) {
            temp = SubWord(temp);
        }
        for t in 0..<4 {
            w[i][t] = w[i-Nk][t] ^ temp[t]
        }
    }
    return w
}

func SubWord(_ w1: [Int16]) -> [Int16] {    // apply SBox to 4-byte word w
    var w = w1
    for i in 0..<4 {
        w[i] = Int16(Sbox[Int(w[i])])
    }
    return w
}

func RotWord(_ w1:[Int16]) -> [Int16] {// rotate 4-byte word w left by one byte
    var w = w1
    let toMax = 4 - w.count
    if (toMax >= 0) {
        w = w + [Int16](repeating: 0, count: toMax + 1)
    }
    w[4] = w[0];
    for i in 0..<4 {
        w[i] = w[i+1]
    }
    return w
}


// Sbox is pre-computed multiplicative inverse in GF(2^8) used in SubBytes and KeyExpansion [§5.1.1]
let Sbox =  [0x63,0x7c,0x77,0x7b,0xf2,0x6b,0x6f,0xc5,0x30,0x01,0x67,0x2b,0xfe,0xd7,0xab,0x76,
             0xca,0x82,0xc9,0x7d,0xfa,0x59,0x47,0xf0,0xad,0xd4,0xa2,0xaf,0x9c,0xa4,0x72,0xc0,  //Sbox[16-1]
             0xb7,0xfd,0x93,0x26,0x36,0x3f,0xf7,0xcc,0x34,0xa5,0xe5,0xf1,0x71,0xd8,0x31,0x15,
             0x04,0xc7,0x23,0xc3,0x18,0x96,0x05,0x9a,0x07,0x12,0x80,0xe2,0xeb,0x27,0xb2,0x75,  //Sbox[16-3]
             0x09,0x83,0x2c,0x1a,0x1b,0x6e,0x5a,0xa0,0x52,0x3b,0xd6,0xb3,0x29,0xe3,0x2f,0x84,
             0x53,0xd1,0x00,0xed,0x20,0xfc,0xb1,0x5b,0x6a,0xcb,0xbe,0x39,0x4a,0x4c,0x58,0xcf,  //Sbox[16-5]
             0xd0,0xef,0xaa,0xfb,0x43,0x4d,0x33,0x85,0x45,0xf9,0x02,0x7f,0x50,0x3c,0x9f,0xa8,
             0x51,0xa3,0x40,0x8f,0x92,0x9d,0x38,0xf5,0xbc,0xb6,0xda,0x21,0x10,0xff,0xf3,0xd2,  //Sbox[16-7]
             0xcd,0x0c,0x13,0xec,0x5f,0x97,0x44,0x17,0xc4,0xa7,0x7e,0x3d,0x64,0x5d,0x19,0x73,
             0x60,0x81,0x4f,0xdc,0x22,0x2a,0x90,0x88,0x46,0xee,0xb8,0x14,0xde,0x5e,0x0b,0xdb,  //Sbox[16-9]
             0xe0,0x32,0x3a,0x0a,0x49,0x06,0x24,0x5c,0xc2,0xd3,0xac,0x62,0x91,0x95,0xe4,0x79,
             0xe7,0xc8,0x37,0x6d,0x8d,0xd5,0x4e,0xa9,0x6c,0x56,0xf4,0xea,0x65,0x7a,0xae,0x08,  //Sbox[16-11]
             0xba,0x78,0x25,0x2e,0x1c,0xa6,0xb4,0xc6,0xe8,0xdd,0x74,0x1f,0x4b,0xbd,0x8b,0x8a,
             0x70,0x3e,0xb5,0x66,0x48,0x03,0xf6,0x0e,0x61,0x35,0x57,0xb9,0x86,0xc1,0x1d,0x9e,  //Sbox[16-13]
             0xe1,0xf8,0x98,0x11,0x69,0xd9,0x8e,0x94,0x9b,0x1e,0x87,0xe9,0xce,0x55,0x28,0xdf,
             0x8c,0xa1,0x89,0x0d,0xbf,0xe6,0x42,0x68,0x41,0x99,0x2d,0x0f,0xb0,0x54,0xbb,0x16];

// Rcon is Round Constant used for the Key Expansion [1st col is 2^(r-1) in GF(2^8)] [§5.2]
let Rcon = [ [0x00, 0x00, 0x00, 0x00],
             [0x01, 0x00, 0x00, 0x00],  //Rcon[1]
             [0x02, 0x00, 0x00, 0x00],
             [0x04, 0x00, 0x00, 0x00],  //Rcon[3]
             [0x08, 0x00, 0x00, 0x00],
             [0x10, 0x00, 0x00, 0x00],  //Rcon[5]
             [0x20, 0x00, 0x00, 0x00],
             [0x40, 0x00, 0x00, 0x00],  //Rcon[7]
             [0x80, 0x00, 0x00, 0x00],
             [0x1b, 0x00, 0x00, 0x00],  //Rcon[9]
             [0x36, 0x00, 0x00, 0x00] ];



/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -  */

/*
 * Use AES to encrypt 'plaintext' with 'password' using 'nBits' key, in 'Counter' mode of operation
 *                           - see http://csrc.nist.gov/publications/nistpubs/800-38a/sp800-38a.pdf;
 *   for each block;
 *   - outputblock = cipher(counter, key);
 *   - cipherblock = plaintext xor outputblock;
 */
func AESEncryptCtr(_ plaintext: String, _ password: String, _ nBits: Int) -> String {
    if (!(nBits==128 || nBits==192 || nBits==256)) {
        return ""
    } // standard allows 128/192/256 bit keys

    // for this example script, generate the key by applying Cipher to 1st 16/24/32 chars of password;
    // for real-world applications, a more secure approach would be to hash the password e.g. with SHA-1
    let nBytes = nBits/8;  // no bytes in key
    var pwBytes = [Int16](repeating: 0, count: nBytes)
    for i in 0..<nBytes {
        pwBytes[i] = getASCCode(password, i) & 0xff
    }

    var key = Cipher(pwBytes, KeyExpansion(pwBytes));
    key = key + key.prefix(nBytes-16)// key is now 16/24/32 bytes long

    // initialise counter block (NIST SP800-38A §B.2): millisecond time-stamp for nonce in 1st 8 bytes,
    // block counter in 2nd 8 bytes
    let blockSize = 16;  // block size fixed at 16 bytes / 128 bits (Nb=4) for AES
    var counterBlock = [Int16](repeating: 0, count: blockSize)  // block size fixed at 16 bytes / 128 bits (Nb=4) for AES
    let nonce = Int(currentTimestamp13()) // milliseconds since 1-Jan-1970

    // encode nonce in two stages to cater for JavaScript 32-bit limit on bitwise ops
    for i in 0..<4 {
        counterBlock[i] = Int16(Int(Int32(transBigInt32(nonce)) >>> Int32(i*8)) & 0xff)
    }
    for i in 0..<4 {
        counterBlock[i+4] = Int16(Int(Int32(transBigInt32(nonce)/0x100000000) >>> Int32(i*8)) & 0xff)
    }

    // generate key schedule - an expansion of the key into distinct Key Rounds for each round
    let keySchedule = KeyExpansion(key);
    //log("crypto-aes: keySchedule = \(keySchedule)")
    let blockCount = Int(ceil(Double(plaintext.count)/Double(blockSize)))
    var ciphertext = Array<String>(repeating: "", count: blockCount) // ciphertext as array of strings

    for b in 0..<blockCount {
        // set counter (block #) in last 8 bytes of counter block (leaving nonce in 1st 8 bytes)
        // again done in two stages for 32-bit ops
        for c in 0..<4 {
            counterBlock[15-c] = Int16((Int32(b) >>> Int32(c*8)) & 0xff)
        }
        for c in 0..<4 {
            counterBlock[15-c-4] = Int16(Int32(b/0x100000000) >>> Int32(c*8))
        }

        let cipherCntr = Cipher(counterBlock, keySchedule);  // -- encrypt counter block --

        // calculate length of final block:
        let blockLength = b<blockCount-1 ? blockSize : (plaintext.count-1)%blockSize+1;

        var ct = ""
        for i in 0..<blockLength { // -- xor plaintext with ciphered counter byte-by-byte --
            let plaintextByte = getASCCode(plaintext, b*blockSize+i)
            let cipherByte = plaintextByte ^ cipherCntr[i];
            ct += String(Character(UnicodeScalar(Int(cipherByte))!))
        }
        // ct is now ciphertext for this block
        ciphertext[b] = escCtrlChars(ct);  // escape troublesome characters in ciphertext
    }

    // convert the nonce to a string to go on the front of the ciphertext
    var ctrTxt = ""
    for i in 0..<8 {
        ctrTxt += String(Character(UnicodeScalar(Int(counterBlock[i]))!))
    }
    ctrTxt = escCtrlChars(ctrTxt)
    // use '-' to separate blocks, use Array.join to concatenate arrays of strings for efficiency;
    return ctrTxt + "-" + ciphertext.joined(separator: "-")
}

/*
 * Use AES to decrypt 'ciphertext' with 'password' using 'nBits' key, in Counter mode of operation;
 *
 *   for each block;
 *   - outputblock = cipher(counter, key);
 *   - cipherblock = plaintext xor outputblock;
 */
func AESDecryptCtr(_ ciphertext: String, _ password: String, _ nBits: Int) -> String {
    if (!(nBits==128 || nBits==192 || nBits==256)) {
        return ""
    } // standard allows 128/192/256 bit keys

    let nBytes = nBits/8;  // no bytes in key
    var pwBytes = [Int16](repeating: 0, count: nBytes)

    for i in 0..<nBytes {
        pwBytes[i] = getASCCode(password, i) & 0xff
    }

    let pwKeySchedule = KeyExpansion(pwBytes);
    var key = Cipher(pwBytes, pwKeySchedule);
    key = key + key.prefix(nBytes-16)  // key is now 16/24/32 bytes long

    let keySchedule = KeyExpansion(key);
    var ciphertextArray:[Any] = ciphertext.split(separator: "-") // split ciphertext into array of block-length strings

    // recover nonce from 1st element of ciphertext
    let blockSize = 16;  // block size fixed at 16 bytes / 128 bits (Nb=4) for AES
    var counterBlock = [Int16](repeating: 0, count: blockSize);
    let ctrTxt = unescCtrlChars("\(ciphertextArray[0])");
    for i in 0..<8 {
        counterBlock[i] = Int16(getASCCode(ctrTxt, i))
    }

    var plaintext = Array(repeating: "", count: ciphertextArray.count-1);

    for b in 1..<ciphertextArray.count {
        // set counter (block #) in last 8 bytes of counter block (leaving nonce in 1st 8 bytes)
        for c in 0..<4 {
            counterBlock[15-c] = Int16(Int32((b-1)) >>> Int32(c*8)) & 0xff
        }
        for c in 0..<4 {
            counterBlock[15-c-4] = Int16(Int32(Int32((Double(b)/Double(0x100000000) - 1)) >>> Int32(c*8)) & 0xff)
        }

        let cipherCntr = Cipher(counterBlock, keySchedule);  // encrypt counter block

        ciphertextArray[b] = unescCtrlChars("\(ciphertextArray[b])");
        
        var pt = ""
        for i in 0...("\(ciphertextArray[b])".contains("\r\n") ? "\(ciphertextArray[b])".count : "\(ciphertextArray[b])".count - 1 ) {
            // -- xor plaintext with ciphered counter byte-by-byte --
            let ciphertextByte =  getASCCode("\(ciphertextArray[b])", i)
            let plaintextByte = ciphertextByte ^ cipherCntr[i];
            pt += String(Character(UnicodeScalar(Int(plaintextByte))!))
        }
        // pt is now plaintext for this block

        plaintext[b-1] = pt  // b-1 'cos no initial nonce block in plaintext
      }

    return plaintext.joined(separator: "")
}

/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -  */

func escCtrlChars(_ str: String) -> String {  // escape control chars which might cause problems handling ciphertext
    let tmpsArr = [0, 9, 10, 11, 12, 13, 160, 39, 34, 33, 45]
    let s = Array(str).map { c -> String in
        let data = c.unicodeScalars
        if data.count == 2 {
            var str = ""
            let asciiCode1 = c.unicodeScalars.first?.value ?? 0
            let asciiCode2 = c.unicodeScalars.last?.value ?? 0
            if tmpsArr.contains(Int(asciiCode1)) {
                str += "!\(asciiCode1)!"
            }
            if tmpsArr.contains(Int(asciiCode2)) {
                str += "!\(asciiCode2)!"
            }
            return str
        }else {
            let asciiCode = c.unicodeScalars.first?.value ?? 0
            if tmpsArr.contains(Int(asciiCode)) {
                return "!\(asciiCode)!"
            }
        }
        return String(c)
    }.reduce("", {$0+$1})
    return s
}  // \xa0 to cater for bug in Firefox; include '-' to leave it free for use as a block marker

func unescCtrlChars(_ str: String) -> String {  // unescape potentially problematic control characters
    return str.replaceCodeToChar("!\\d\\d?\\d?!")
}

func getASCCode(_ str: String, _ at: Int) -> Int16 {
    
    if str.contains("\r\n") {
        if str.hasPrefix("\r\n") {
            // 开头包含 \r\n
            if at == 0 { return 13; }
            if at == 1 { return 10; }
            if at > 1 {
                let ch = str[str.index(str.startIndex, offsetBy: at-1)]
                return Int16(Unicode.Scalar("\(ch)")?.value ?? 0)
            }
        }else if str.hasSuffix("\r\n") {
            // 结尾包含 \r\n
            if at == str.count - 1 { return 13; }
            if at == str.count { return 10; }
            let ch = str[str.index(str.startIndex, offsetBy: at)]
            return Int16(Unicode.Scalar("\(ch)")?.value ?? 0)
        }
        // /r/n 在字符串的中间
        let tmpStrings = str.split(separator: "\r\n")
        if tmpStrings.count == 2 {
            // 在中间的加这个梳理
            let aStr = tmpStrings[0]
            if at < aStr.count {
                let ch = str[str.index(str.startIndex, offsetBy: at)]
                return Int16(Unicode.Scalar("\(ch)")?.value ?? 0)
            }
            if at == aStr.count - 1 + 1 { return 13 }
            if at == aStr.count - 1 + 2 { return 10 }
            if at > aStr.count - 1 + 2 {
                let bStr = tmpStrings[1]
                let bIndex = at - aStr.count - 2
                let subIndex = bStr[bStr.index(bStr.startIndex, offsetBy: bIndex)]
                return Int16(Unicode.Scalar("\(subIndex)")?.value ?? 0)
            }
        } else {
            
        }
    }
    if at == str.count {
        return 0
    }
    let ch = str[str.index(str.startIndex, offsetBy: at)]
    return Int16(Unicode.Scalar("\(ch)")?.value ?? 0)
}

func transBigInt32(_ bigInt32Num: Int) -> Int {
    var tmp = bigInt32Num
    if (tmp > 2147483647) {
        let max = 4294967295
        tmp = tmp % (max + 1)
        if (tmp > 2147483647){
            tmp = tmp - max - 1
        }
    }else if (tmp < -2147483648){
        let max = 4294967295
        tmp = tmp % (max + 1)
        if (tmp < -2147483648) {
            tmp = tmp + max + 1
        }
    }
    return tmp
}


func run() {
    let plainText = """
    ROMEO: But, soft! what light through yonder window breaks?\n\
    It is the east, and Juliet is the sun.\n\
    Arise, fair sun, and kill the envious moon,\n\
    Who is already sick and pale with grief,\n\
    That thou her maid art far more fair than she:\n\
    Be not her maid, since she is envious;\n\
    Her vestal livery is but sick and green\n\
    And none but fools do wear it; cast it off.\n\
    It is my lady, O, it is my love!\n\
    O, that she knew she were!\n\
    She speaks yet she says nothing: what of that?\n\
    Her eye discourses; I will answer it.\n\
    I am too bold, 'tis not to me she speaks:\n\
    Two of the fairest stars in all the heaven,\n\
    Having some business, do entreat her eyes\n\
    To twinkle in their spheres till they return.\n\
    What if her eyes were there, they in her head?\n\
    The brightness of her cheek would shame those stars,\n\
    As daylight doth a lamp; her eyes in heaven\n\
    Would through the airy region stream so bright\n\
    That birds would sing and think it were not night.\n\
    See, how she leans her cheek upon her hand!\n\
    O, that I were a glove upon that hand,\n\
    That I might touch that cheek!\n\
    JULIET: Ay me!\n\
    ROMEO: She speaks:\n\
    O, speak again, bright angel! for thou art\n\
    As glorious to this night, being o'er my head\n\
    As is a winged messenger of heaven\n\
    Unto the white-upturned wondering eyes\n\
    Of mortals that fall back to gaze on him\n\
    When he bestrides the lazy-pacing clouds\n\
    And sails upon the bosom of the air.
    """
    
    let password = "O Romeo, Romeo! wherefore art thou Romeo?";
    let cipherText = AESEncryptCtr(plainText, password, 256);
    //log("crypto-aes: cipherText \(cipherText)")
    let decryptedText = AESDecryptCtr(cipherText, password, 256);

    if (decryptedText != plainText) {
        fatalError("ERROR: bad result: expected " + plainText + " but got " + decryptedText)
    }
}

class Benchmark {
    /// @Benchmark
    func runIteration() {
       for _ in 0..<8 {
           run()
       }
    }
}


let startTime = currentTimestamp13()
let benchmark = Benchmark()
for i in 0..<20 {
    let startTimeInLoop = currentTimestamp13()
    benchmark.runIteration()
    let endTimeInLoop = currentTimestamp13()
    //log("crypto-aes: ms = \(endTimeInLoop - startTimeInLoop) i = \(i)")
}
let endTime = currentTimestamp13()
print("crypto-aes: ms = \((endTime - startTime))")

infix operator >>> : BitwiseShiftPrecedence
func >>> (lhs: Int32, rhs: Int32) -> Int32 {
    return Int32(bitPattern: UInt32(bitPattern: lhs) >> UInt32(rhs))
}

class Timer {
   private let CLOCK_REALTIME = 0
   private var time_spec = timespec()

   func getTime() -> Double {
       clock_gettime(Int32(CLOCK_REALTIME),&time_spec)
       return Double(time_spec.tv_sec * 1_000_000 + time_spec.tv_nsec / 1_000)
   }
}

extension String {
    func replaceCodeToChar(_ pattern: String) -> String {
        do {
            let mathces = self.matches(of: try Regex(pattern))
            var b = self
            for i in 0..<mathces.count {
                let lastIndex = mathces.count - 1 - i
                let lastRange = mathces[lastIndex].range

                let lastStr = self[lastRange]
                let lastStrNum = Int(lastStr.replacing("!", with: "")) ?? 0
                let lastChar = Character(UnicodeScalar(lastStrNum)!)
                b = String(b[b.startIndex..<lastRange.lowerBound] + String(lastChar) + b[lastRange.upperBound..<b.endIndex])
            }
            return b
        } catch {

        }
        return ""
    }
}

