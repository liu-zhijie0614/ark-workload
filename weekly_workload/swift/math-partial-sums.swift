import Glibc

func partial(_ n: Int) -> Double{
    var a1 = 0.0
    var a2 = 0.0
    var a3 = 0.0
    var a4 = 0.0
    var a5 = 0.0
    var a6 = 0.0
    var a7 = 0.0
    var a8 = 0.0
    var a9 = 0.0
    let twothirds = 2.0/3.0;
    var alt = -1.0;
    var k2 = 0.0
    var k3 = 0.0
    var sk = 0.0
    var ck = 0.0
    for m in 1...n {
        k2 = Double(m*m);
        k3 = k2*Double(m);
        sk = sin(Double(m));
        ck = cos(Double(m));
        alt = -alt;
        
        a1 += pow(twothirds,Double(m-1));
        a2 += pow(Double(m),-0.5);
        a3 += 1.0/(Double(m)*(Double(m)+1.0));
        a4 += 1.0/(k3 * sk*sk);
        a5 += 1.0/(k3 * ck*ck);
        a6 += 1.0/Double(m);
        a7 += 1.0/k2
        a8 += alt/Double(m)
        a9 += alt/(2*Double(m)-1)
    }
    // NOTE: We don't try to validate anything from pow(),  sin() or cos() because those aren't
    // well-specified in ECMAScript;
    return a6 + a7 + a8 + a9;
}

/*
 *  @State
 *  @Tags Jetstream2
 */
class Benchmark {
      func run() {
    	var total = 0.0
        var n = 1024
        while n <= 16384 {
           total = total + partial(n)
           n = n * 2
        }
        let expected = 60.08994194659945;
        if total != expected {
           print("ERROR: bad result: expected \(expected) but got \(total)")
        }
      }
	
        /**
         * @Benchmark
         */
	func runIterationTime() {

	    let start = Timer().getTime()
	    for _ in 0..<80 {
	        self.run()
	    }
	    let end = Timer().getTime()
	    let duration = (end - start) / 1000
	    print("math-partial-sums: ms = \(duration)")
	}
}


Benchmark().runIterationTime()

class Timer {
    private let CLOCK_REALTIME = 0
    private var time_spec = timespec()

    func getTime() -> Double {
        clock_gettime(Int32(CLOCK_REALTIME),&time_spec)
        return Double(time_spec.tv_sec * 1_000_000 + time_spec.tv_nsec / 1_000)
    }
}
