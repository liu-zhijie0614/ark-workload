
import Glibc

func A(_ i: Int,_ j: Int) -> Double {
    let result = (i+j)*(i+j+1)/2+i+1
    return 1.0 / Double(result)
}

func Au(_ u: [Double],_ v: inout [Double]) {
    for i in 0..<u.count {
        var t = 0.0
        for j in 0..<u.count {
            t += A(i, j) * u[j]
        }
        v[i] = t
    }
}

func Atu(_ u: [Double],_ v: inout [Double]) {
    for i in 0..<u.count {
        var t = 0.0
        for j in 0..<u.count {
            t += A(j, i) * u[j]
        }
        v[i] = t
    }
}

func AtAu(_ u:[Double],_ v: inout [Double],_ w: inout [Double]){
    Au(u,&w)
    Atu(w,&v)
}

func spectralnorm(_ n: Int) -> Double {
    var u = [Double]()
    var v = [Double]()
    var w = [Double]()
    var vv = 0.0
    var vBv = 0.0
    
    for _ in 0..<n{
        u.append(1.0)
        v.append(0.0)
        w.append(0.0)
    }
    for _ in 0..<10 {
        AtAu(u,&v,&w)
        AtAu(v,&u,&w)
    }
    
    for i in 0..<n {
        vBv += u[i] * v[i]
        vv += v[i] * v[i]
    }
    return sqrt(Double(vBv/vv))
}

/*
 *  @State
 *  @Tags Jetstream2
 */
class Benchmark {
	func run() {
	    var total = 0.0
	     var n = 6
	     while n <= 48 {
	         total = total + spectralnorm(n)
	         n = n * 2
	     }
	     let expected = 5.086694231303284;
	     if(total != expected){
	         print("ERROR: bad result: expected \(expected) but got \(total)")
	     }
	}
	/**
         * @Benchmark
         */
	func runIterationTime() {

	    let start = Timer().getTime()
	    for _ in 0..<80 {
	        self.run()
	    }
	    let end = Timer().getTime()
	    let duration = (end - start) / 1000
	    print("math-spectral-norm: ms = \(duration)")
	}
}

Benchmark().runIterationTime()

class Timer {
    private let CLOCK_REALTIME = 0
    private var time_spec = timespec()

    func getTime() -> Double {
        clock_gettime(Int32(CLOCK_REALTIME),&time_spec)
        return Double(time_spec.tv_sec * 1_000_000 + time_spec.tv_nsec / 1_000)
    }
}

