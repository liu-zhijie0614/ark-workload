import Glibc

class Timer {
  private let CLOCK_REALTIME = 0
  private var start_timespec = timespec()
  private var end_timespec = timespec()
  private var time_spec = timespec()
  func start() {
        clock_gettime(Int32(CLOCK_REALTIME),&start_timespec)
    }

  func stop() -> Double {
  clock_gettime(Int32(CLOCK_REALTIME),&end_timespec)
  let start_time = Double(start_timespec.tv_sec * 1_000_000 + start_timespec.tv_nsec / 1_000)
  let end_time = Double(end_timespec.tv_sec * 1_000_000 + end_timespec.tv_nsec / 1_000)
  let time = end_time - start_time
  return time / 1_000
}
func getTime() -> Double {
        clock_gettime(Int32(CLOCK_REALTIME),&time_spec)
        return Double(time_spec.tv_sec * 1_000_000 + time_spec.tv_nsec / 1_000)
    }
}

var timer = Timer()
func picColorFromBuffer(e: EData){
  let data = e.data
  if (data.data.isEmpty) {
    print("dingwen input error")
    return
  }
  let msgType: String = data.type
  let readBuffer: [Int] = data.data
  let len: Int = data.len / 4
  switch (msgType) {
    case "pickColor":
      let intBuffer: [Int] = readBuffer
      var r: Int = 0
      var g: Int = 0
      var b: Int = 0
      var a: Int = 0
      for i in 0..<len {
        r += intBuffer[i * 4]
        g += intBuffer[i * 4 + 1]
        b += intBuffer[i * 4 + 2]
        a += intBuffer[i * 4 + 3]
      }
      let r1 = lround(Double(r) / Double(len))
      let g1 = lround(Double(g) / Double(len))
      let b1 = lround(Double(b) / Double(len))
      let a1 = lround(Double(a) / Double(len))
//	  print("Takecolor pinColorFormBuffer color[ARGB] :\t\(a1)\t\(r1)\t\(b1)\t\(g1)");
      break
    default:
      break
  }
}

class Data {
  var type: String;
  var data: [Int];
  var len: Int;
  init (mType: String, mData: [Int], mLen: Int) {
    self.type = mType;
    self.data = mData;
    self.len = mLen;
  }
}

class EData {
  var data: Data;
  init (mData: Data) {
    self.data = mData;
  }
}

var uint8Array: [Int] = [Int](repeating: 0, count: 500*500);
for i in 0..<uint8Array.count {
  uint8Array[i] = i % 256;
}
var data: Data = Data(mType: "pickColor", mData: uint8Array, mLen: 500*500);
var eData: EData = EData(mData:data);
timer.start();
for i in 0..<10{
picColorFromBuffer(e:eData);
}
var time = timer.stop();
print("Takecolor_Obj: \(time)\tms");
