import Glibc

let letters = ["a","b","c","d","e","f","g","h","i","j","k","l","m","n","o","p","q","r","s","t","u","v","w","x","y","z"]

var endResult: String?

func doTest() {
    endResult = ""

    // make up email address
    for k in 0..<4000 {
        let username = makeName(6)
        var email: String?
        ((k%2) == 1) ? (email = username+"@mac.com"): (email = username+"(at)mac.com")

        // validate the email address
        let pattern = "^[a-zA-Z0-9\\-_.]+@[a-zA-Z0-9\\-_]+(\\.?[a-zA-Z0-9\\-_]*)\\.[a-zA-Z]{2,3}$"

        if email!.matches(of: try! Regex(pattern)).count > 0 {
            let r = email! + " appears to be a valid email address."
            addResult(r)
        } else {
            let r = email! + " does NOT appear to be a valid email address."
            addResult(r)
        }
    }

    // make up ZIP codes
    for s in 0..<4000 {
        var zipGood:Bool = true
        var zip = makeNumber(4)
         ((s%2) == 1) ? (zip = zip+"xyz"): (zip = zip+"7")
        // validate the zip code
        for i in 0..<zip.count {
            let ch = zip[zip.index(zip.startIndex, offsetBy: i)]
            let cArr : [Character] = ["0","1","2","3","4","5","6","7","8","9"]
            if (!cArr.contains(ch)) {
                zipGood = false
                let r = zip + " contains letters."
                addResult(r)
            }
        }
        
        if (zipGood && zip.count > 5) {
            zipGood = false
            let r = zip + " is longer than five characters."
            addResult(r)
        }
        
        if (zipGood) {
            let r = zip + " appears to be a valid ZIP code."
            addResult(r)
        }
    }
}

func makeName(_ n: Int) -> String {
   var tmp = ""
    for _ in 0..<n {
        let l  = Int.random(in: 0..<26)
        tmp += letters[l]
    }
   return tmp
}

func makeNumber(_ n: Int) -> String {
   var tmp = ""
   for _ in 0..<n {
        let l = Int.random(in: 0...9)
        tmp += String(l)
   }
   return tmp
}

func addResult(_ r: String) {
    endResult! += "\n" + r
}

/*
* @State
* @Tags Jetstream2
*/
class Benchmark {
    /*
     *@Benchmark
     */
    func runIteration() {
        doTest()
    }
}

class Timer {
    private let CLOCK_REALTIME = 0
    private var time_spec = timespec()

    func getTime() -> Double {
        clock_gettime(Int32(CLOCK_REALTIME),&time_spec)
        return Double(time_spec.tv_sec * 1_000_000 + time_spec.tv_nsec / 1_000) / 1000
    }
}

let start = Timer().getTime()
Benchmark().runIteration()
let end = Timer().getTime()
print("string-validate-input: ms = \(end-start)")
