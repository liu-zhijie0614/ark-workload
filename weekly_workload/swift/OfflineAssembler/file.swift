/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import Glibc

class File {
    var fileName: String
    var data: String
    static var directory = Dictionary<String, File>()
    init(_ fileNames: String, _ datas: String) {
        fileName = fileNames
        data = datas
        if (File.directory[fileName] != nil) {
            print("Creating file named " + "\(fileName)" + " that already exists")
            return
        } else {
            File.directory[fileName] = self
        }
    }
    static func open(_ fileName: String) -> File {
        return File.directory[fileName]!
    }
    func read() -> String {
        return data
    }
}


