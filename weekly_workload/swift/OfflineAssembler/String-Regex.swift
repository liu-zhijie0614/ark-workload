import Glibc

extension String {
    func split(_ separateStr: String) -> [String] {
        var result: [String] = []
        var currentComponent = ""
        for char in self {
            if String(char) == separateStr {
                result.append(currentComponent)
                currentComponent = ""
            } else {
                currentComponent.append(char)
            }
        }
        if !currentComponent.isEmpty {
            result.append(currentComponent)
        }
        return result
    }
    
    func matche(_ regexp: String) -> [String]? {
        guard let regex = try? Regex(regexp) else { return nil }
        let input = self
        guard let matchs = input.firstMatch(of: regex) else { return nil }
        var matchStrs = [String]()
        var idx = 0
        while idx < matchs.count {
            let result = matchs[idx]
            if result.range == nil {continue}
            let subStr = input[result.range!]
            matchStrs.append(String(subStr))
            idx += 1
        }
        return matchStrs
    }
    
    /*
     * Determine whether a string matches a regular rule
     */
    func test(_ str: String) -> Bool {
        return str.starts(with: try! Regex(self))
    }
}

/*
 * Obtains the preceding digit in a string. For example, "123you"-> 123
 */
func parseInt(_ inputString: String) -> Int {
    var numberString = ""
    for char in inputString {
        if char.isNumber {
            // Add to numberString if the character is a number
            numberString.append(char)
        } else {
            break
        }
    }
    
    return Int(numberString) ?? 0
}


