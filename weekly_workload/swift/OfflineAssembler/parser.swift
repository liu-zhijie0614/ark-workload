/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import Glibc

let GlobalAnnotation :String = "global"
let LocalAnnotation : String = "local"


/*
 * Create IncludeFile File Class Splice
 */
class IncludeFile {
    var includeDirs: String = ""
    var fileName: String = ""
    
    init(_ moduleName: String, _ defaultDir: String?) {
        fileName = moduleName  + ".asm"
    }
    
    func toString() -> String {
        return fileName
    }
}

/*
 * Define Token Class
 */
class Token {
    var codeOrigin: CodeOrigin
    var string: String = ""
    init(_ codeOrigi: CodeOrigin, _ strin: String) {
        codeOrigin = codeOrigi
        string = strin
    }
    
    func isEqualTo(_ other: Any) -> Bool {
        if (other is Token) {
            return string == (other as! Token).string
        } else{
            return string == other as! String
        }
    }
    
    func isNotEqualTo(_ other: Any) -> Bool {
        return !isEqualTo(other)
    }
    
    func toString() -> String {
        return "" + string + "\\at" + codeOrigin.toString();
    }
    
    func parseError(_ comment: Any?) {
        print("Parse error: " + "\(String(describing: comment ?? ""))")
    }
}

/*
 * Define Annotation class
 */
class Annotation {
    var codeOrigin: CodeOrigin
    var string: String = ""
    var type: String
    
    init(_ codeOrigi: CodeOrigin, _ typ: String, _ strin: String) {
        codeOrigin = codeOrigi
        type = typ
        string = strin
    }
    
    func isEqualTo(_ other: Any) -> Bool {
        if (other is Token) {
            return string == (other as! Token).string
        } else{
            return string == other as! String
        }
    }
    
    func isNotEqualTo(_ other: Any) -> Bool {
        return !isEqualTo(other)
    }
    
    func toString() -> String {
        return "" + string + "\\at" + codeOrigin.toString();
    }
    
    func parseError(_ comment: Any?) {
        print("Annotation error: " + "\(String(describing: comment ?? ""))")
    }
}

/*
 * Returns a Token Array
 */
func lex(_ strs: String, _ file: SourceFile) -> [Any] {
    // Regex gets a data array containing a character
    func scanRegExp(_ source: String, _ regexp: String) -> [String]? {
        return source.matche(regexp)
    }
    
    // The lexer. Takes a string and returns an array of tokens.
    var result : [Any] = []
    var lineNumber = 1
    var whitespaceFound : Bool = false
    var str = strs
    while(str.count > 0){
        var tokenMatch : [String]?
        var annotation : Any? = nil
        var annotationType : String = ""
        
        if let tokens = scanRegExp(str, "^#([^\n]*)")  {
            // comment, ignore
            tokenMatch = tokens
        } else if let tokens = scanRegExp(str,  "^\\/\\/ ?([^\n]*)") {
            // annotation
            tokenMatch = tokens
            annotation = tokenMatch![0]
            annotationType = whitespaceFound ? LocalAnnotation : GlobalAnnotation
        } else if let tokens = scanRegExp(str, "^\n") {
            /* We've found a '\n'.  Emit the last comment recorded if appropriate:
             * We need to parse annotations regardless of whether the backend does
             * anything with them or not. This is because the C++ backend may make
             * use of this for its cloopDo debugging utility even if
             * enableInstrAnnotations is not enabled.
             */
            tokenMatch = tokens
            
            if (annotation != nil) {
                result.append(Annotation(CodeOrigin(file, lineNumber as Int), annotationType, annotation as! String))
                annotation = nil;
            }
            result.append(Token(CodeOrigin(file, lineNumber as Int), tokenMatch![0]))
            lineNumber += 1;
        } else if let tokens = scanRegExp(str, "^[a-zA-Z]([a-zA-Z0-9_.]*)") {
            tokenMatch = tokens
            result.append(Token(CodeOrigin(file, lineNumber as Int), tokenMatch![0]))
        } else if let tokens = scanRegExp(str, "^\\.([a-zA-Z0-9_]*)") {
            tokenMatch = tokens
            result.append(Token(CodeOrigin(file, lineNumber as Int), tokenMatch![0]))
        } else if let tokens = scanRegExp(str, "^_([a-zA-Z0-9_]*)") {
            tokenMatch = tokens
            result.append(Token(CodeOrigin(file, lineNumber as Int), tokenMatch![0]))
        } else if let tokens = scanRegExp(str, "^([ \t]+)") {
            // whitespace, ignore
            whitespaceFound = true;
            str = String(str.suffix(from: tokens[0].endIndex))
            continue;
        } else if let tokens = scanRegExp(str, "^0x([0-9a-fA-F]+)") {
            tokenMatch = tokens
            result.append(Token(CodeOrigin(file, lineNumber as Int), "\(UInt64(String(tokens[1]), radix: 16)!)"))
        } else if let tokens = scanRegExp(str, "^0([0-7]+)") {
            tokenMatch = tokens
            result.append(Token(CodeOrigin(file, lineNumber as Int), "\(Int(String(tokens[1]), radix: 8)!)"))
        } else if let tokens = scanRegExp(str, "^([0-9]+)") {
            tokenMatch = tokens
            result.append(Token(CodeOrigin(file, lineNumber as Int), tokenMatch![0]))
        } else if let tokens = scanRegExp(str, "^::") {
            // Match with double colons:: Text line at the beginning
            tokenMatch = tokens
            result.append(Token(CodeOrigin(file, lineNumber as Int), tokenMatch![0]))
        } else if let tokens = scanRegExp(str, "^[:,\\(\\)\\[\\]=+-\\~\\|&^*]") {
            // ^[:,\(\)\[\]=\+\-~\|&^*] Matches strings starting with colon, comma, parenthesis, square brackets, equals, plus, minus, tilde, pipe, and, or characters, or asterisks
            tokenMatch = tokens
            result.append(Token(CodeOrigin(file, lineNumber as Int), tokenMatch![0]))
        } else if let tokens = scanRegExp(str, "\"([^\"]*)\"") {
            // Matches strings enclosed in double quotes
            tokenMatch = tokens
            result.append(Token(CodeOrigin(file, lineNumber as Int), tokenMatch![0]))
        } else {
            print("Lexer error at " + "\(CodeOrigin(file, lineNumber as Int))")
        }
        
        whitespaceFound = false
        str = String(str.suffix(from: tokenMatch![0].endIndex))
    }
    
//    if(isDebug) {
//        for i in 0..<result.count {
//            DeBugLog("\(i)" + " line in result: " + "\((result[i] as? Token)?.toString() ?? "")");
//        }
//    }
    return result as [Any]
}

// Token identification.
func isRegister(_ token: Token) -> Bool {
    return registerPattern.test(token.string)
}

func isInstruction(_ token: Token) -> Bool {
    return instructionSet.contains(token.string)
}

func isKeyword(_ token: Token) -> Bool {
    return "^((true)|(false)|(if)|(then)|(else)|(elsif)|(end)|(and)|(or)|(not)|(global)|(macro)|(const)|(constexpr)|(sizeof)|(error)|(include))$".test(token.string)
    || isRegister(token)
    || isInstruction(token)
}

func isIdentifier(_ token: Token) -> Bool {
    return "^[a-zA-Z]([a-zA-Z0-9_.]*)$".test(token.string) && !isKeyword(token)
}

func isLabel(_ token:Any) -> Bool {
    var tokenString: String
    if (token is Token) {
        tokenString = (token as! Token).string
    } else {
        tokenString = token as! String
    }
    // Matches regular expressions that start with an underscore followed by letters, numbers, or underscores
    return "^_([a-zA-Z0-9_]*)$".test(tokenString)
}

func isLocalLabel(_ token:Any) -> Bool {
    var tokenString : String
    if (token is Token) {
        tokenString = (token as! Token).string
    } else {
        tokenString = token as! String;
    }
    return "^\\.([a-zA-Z0-9_]*)$".test(tokenString)
}

func isVariable(_ token:Token) -> Bool {
    return isIdentifier(token) || isRegister(token)
}

func isInteger(_ token:Token) -> Bool {
    return "^[0-9]".test(token.string)
}

func isString(_ token:Token) -> Bool {
    return #"^".*""#.test(token.string)
}


// The parser. Takes an array of tokens and returns an AST. Methods
// other than parse(tokens) are not for public consumption.
class Parser{
    var tokens = Array<Token>()
    var idx: Int = 0
    var annotation: String?
    
    init(_ data: String , _ fileName: SourceFile) {
        tokens = lex(data, fileName) as! [Token]
        idx = 0
        annotation = nil
    }
    
    func parseError(_ comment: Any?) {
        if (tokens[idx] != nil) {
            tokens[idx].parseError(comment as Any)
        } else {
            if (comment != nil) {
                print("Parse error at end of file")
            } else {
                print("Parse error at end of file: " + "\(String(describing: comment))")
            }
        }
    }
    
    func consume(_ regexp: String?) {
        if (regexp != nil) {
            if (!regexp!.test(tokens[idx].string)) {
                parseError(nil)
            }
        } else if (idx != tokens.count) {
            parseError(nil)
        }
        idx += 1
    }
    
    func skipNewLine(){
        while (tokens[idx].isEqualTo("\n")) {
            idx += 1
        }
    }
    
    func parsePredicateAtom() -> Node? {
        if(tokens[idx].isEqualTo("not")) {
            let codeOrigin = tokens[idx].codeOrigin
            idx += 1
            let par = parsePredicateAtom()
            if (par != nil)  {
                return Not(codeOrigin, par!)
            }
        }
        
        if(tokens[idx].isEqualTo("(")) {
            idx += 1
            skipNewLine()
            let result = parsePredicate();
            if(tokens[idx].isNotEqualTo(")")) {
                parseError(nil)
            }
            idx += 1
            return result
        }
        
        if(tokens[idx].isEqualTo("true")) {
            let result = True.instance()
            idx += 1
            return result
        }
        
        if(tokens[idx].isEqualTo("false")) {
            let result = False.instance()
            idx += 1
            return result
        }
        
        if (isIdentifier(tokens[idx])) {
            let result = Setting.forName(tokens[idx].codeOrigin, tokens[idx].string)
            idx += 1
            return result
        }
        parseError(nil)
        return nil
    }
    
    func parsePredicateAnd() -> Node? {
        var result = parsePredicateAtom();
        while (tokens[idx].isEqualTo("and")) {
            let codeOrigin = tokens[idx].codeOrigin
            idx += 1
            skipNewLine();
            let right = parsePredicateAtom()
            if (right != nil) {
                result = And(codeOrigin, result!, right!)
            }
        }
        
        return result
    }
    
    func parsePredicate() -> Node? {
        // some examples of precedence:
        // not a and b -> (not a) and b
        // a and b or c -> (a and b) or c
        // a or b and c -> a or (b and c)
        
        var result = parsePredicateAnd()
        while (tokens[idx].isEqualTo("or")) {
            let codeOrigin = tokens[idx].codeOrigin
            idx += 1
            skipNewLine();
            let right = parsePredicateAnd();
            if (result != nil && right != nil) {
                result = Or(codeOrigin, result!, right!)
            }
        }
        
        return result;
    }
    
    func parseVariable() -> Node{
        var result: Node = Node(nil)
        if (isRegister((tokens[idx]))) {
            if (fprPattern.test(tokens[idx].toString() )) {
                result = FPRegisterID.forName(tokens[idx].codeOrigin, tokens[idx].string)
            } else {
                result = RegisterID.forName(tokens[idx].codeOrigin, tokens[idx].string)
            }
        } else if (isIdentifier(tokens[idx])) {
            result = Variable.forName(tokens[idx].codeOrigin, tokens[idx].string)
        } else {
            parseError(nil)
        }
        
        idx += 1
        return result
    }
    
    // Resolution address
    func parseAddress(_ offset: Node) -> Node{
        if (tokens[idx].isNotEqualTo("[")) {
            parseError(nil)
        }
        
        let codeOrigin = tokens[idx].codeOrigin
        var result : Node?
        
        // Three possibilities:
        // []       -> AbsoluteAddress
        // [a]      -> Address
        // [a,b]    -> BaseIndex with scale = 1
        // [a,b,c]  -> BaseIndex
        
        idx += 1
        if (tokens[idx].isEqualTo("]")) {
            idx += 1
            return AbsoluteAddress(codeOrigin, offset)
        }
        
        let a = parseVariable()
        if (tokens[idx].isEqualTo("]")) {
            result =  Address(codeOrigin, a, offset)
        } else {
            if (tokens[idx].isNotEqualTo(",")) {
                parseError(nil)
            }
            idx += 1
            let b = parseVariable()
            if (tokens[idx].isEqualTo("]")) {
                result = BaseIndex(codeOrigin, a , b , 1, offset )
            } else {
                if (tokens[idx].isNotEqualTo(",")) {
                    parseError(nil)
                }
                idx += 1
                if (!["1", "2", "4", "8"].contains(tokens[idx].string)) {
                    parseError(nil)
                }
                let c =  parseInt(tokens[idx].toString())
                idx += 1
                if (tokens[idx].isNotEqualTo("]")) {
                    parseError(nil)
                }
                result = BaseIndex(codeOrigin, a, b, c, offset)
            }
            
        }
        idx += 1
        return result!
    }
    
    func parseColonColon() -> Dictionary<String, Any> {
        skipNewLine()
        let firstToken = tokens[idx]
        let codeOrigin = tokens[idx].codeOrigin;
        if (!isIdentifier(firstToken)) {
            parseError(nil)
        }
        
        var names : Array = [tokens[idx].string]
        idx += 1
        while (tokens[idx].isEqualTo("::")) {
            idx += 1
            if(!isIdentifier((tokens[idx]))) {
                parseError(nil)
            }
            names.append(tokens[idx].string)
            idx += 1
        }
        if (names.count == 0) {
            firstToken.parseError(nil)
        }
        return ["codeOrigin": codeOrigin, "names": names]
    }
    
    func parseTextInParens() -> Dictionary<String,Any> {
        skipNewLine()
        let codeOrigin = tokens[idx].codeOrigin
        if (tokens[idx].isNotEqualTo("(")){
            print("Missing ( at " + "\(String(describing: codeOrigin))")
        }
        idx += 1
        // need at least one item
        if (tokens[idx].isEqualTo(")")) {
            print("No items in list at" + "\(String(describing: codeOrigin))")
        }
        var numEnclosedParens: Int = 0
        var text = Array<String>()
        while tokens[idx].isNotEqualTo(")") || numEnclosedParens > 0 {
            if (tokens[idx].isEqualTo("(")) {
                numEnclosedParens += 1
            } else if (tokens[idx].isEqualTo(")")) {
                numEnclosedParens -= 1
            }
            
            text.append((tokens[idx].string))
            idx += 1
        }
        idx += 1
        return ["codeOrigin": codeOrigin, "text": text]
    }
    
    func parseExpressionAtom() -> Node? {
        var result: Node?
        skipNewLine()
        if (tokens[idx].isEqualTo("-")) {
            idx += 1
            return NegImmediate((tokens[idx-1].codeOrigin), parseExpressionAtom()!)
        }
        
        if (tokens[idx].isEqualTo("~")) {
            idx += 1
            return BitnotImmediate((tokens[idx-1].codeOrigin), parseExpressionAtom()!)
        }
        
        if (tokens[idx].isEqualTo("(")) {
            idx += 1
            result = parseExpression()
            if (tokens[idx].isNotEqualTo(")")) {
                parseError(nil)
            }
            idx += 1
            return result
        }
        
        if (isInteger((tokens[idx]))) {
            result = Immediate((tokens[idx].codeOrigin), UInt64((tokens[idx].string)) ?? 0)
            idx += 1
            return result
        }
        
        if (isString((tokens[idx]))) {
            result = StringLiteral((tokens[idx].codeOrigin), (tokens[idx].string))
            idx += 1
            return result
        }
        
        if (isIdentifier((tokens[idx]))) {
            let dic = parseColonColon()
            let names = dic["names"] as! Array<String>
            if (names.count > 1) {
                return StructOffset.forField(dic["codeOrigin"] as! CodeOrigin, names[0 ... names.count - 2].joined(separator: "::"), names[names.count - 1])
            }
            return Variable.forName(dic["codeOrigin"] as! CodeOrigin, names[0])
        }
        
        if (isRegister((tokens[idx]))) {
            return parseVariable()
        }
        
        if (tokens[idx].isEqualTo("sizeof")) {
            idx += 1
            let dic = parseColonColon()
            let co = dic["codeOrigin"] as! CodeOrigin
            let names = dic["names"] as! Array<String>
            return Sizeof.forName(co, names.joined(separator: "::"))
        }
        
        if (tokens[idx].isEqualTo("constexpr")) {
            idx += 1
            skipNewLine()
            var codeOrigin: CodeOrigin?
            var text: Array<String>?
            var names: Array<String>?
            var textStr: String = ""
            
            if (tokens[idx].isEqualTo("(")) {
                let dic :Dictionary<String,Any> = parseTextInParens()
                codeOrigin = (dic["codeOrigin"] as! CodeOrigin)
                text = (dic["text"] as! [String])
                textStr = text!.joined(separator: "")
            } else {
                let dic :Dictionary<String,Any> = parseColonColon()
                codeOrigin = (dic["codeOrigin"] as! CodeOrigin)
                names = (dic["names"] as! [String])
                textStr = names!.joined(separator: "::")
            }
            return ConstExpr.forName(codeOrigin: codeOrigin!, text: textStr)
        }
        
        if (isLabel((tokens[idx]))) {
            result = LabelReference((tokens[idx].codeOrigin), Label.forName((tokens[idx].codeOrigin), (tokens[idx].string)))
            idx += 1
            return result;
        }
        
        if (isLocalLabel(tokens[idx] as Any)) {
            result = LocalLabelReference(tokens[idx].codeOrigin, LocalLabel.forName(tokens[idx].codeOrigin, tokens[idx].string))
            idx += 1
            return result;
        }
        parseError(nil)
        return nil
    }
    
    func parseExpressionMul() -> Node {
        skipNewLine()
        var result = parseExpressionAtom()
        while (tokens[idx].isEqualTo("*")) {
            if(tokens[idx].isEqualTo("*")) {
                idx += 1
                result = MulImmediates((tokens[idx-1].codeOrigin), result!, parseExpressionAtom()!)
            } else {
                print("Invalid token " + "\(String(describing: tokens[idx]))" + " in multiply expression")
            }
        }
        return result!
    }
    
    func couldBeExpression() -> Bool{
        return tokens[idx].isEqualTo("-") || tokens[idx].isEqualTo("~") || tokens[idx].isEqualTo("constexpr") || tokens[idx].isEqualTo("sizeof") || isInteger(tokens[idx]) || isString(tokens[idx]) || isVariable(tokens[idx]) || tokens[idx].isEqualTo("(")
    }
    
    func parseExpressionAdd() -> Node {
        skipNewLine()
        var result = parseExpressionMul()
        while (tokens[idx].isEqualTo("+")  || tokens[idx].isEqualTo("-")) {
            if (tokens[idx].isEqualTo("+")){
                idx += 1
                result = AddImmediates(tokens[idx-1].codeOrigin, result, parseExpressionMul())
            } else if (tokens[idx].isEqualTo("-")) {
                idx += 1
                result = SubImmediates(tokens[idx-1].codeOrigin, result , parseExpressionMul())
            } else {
                print("Invalid token " + "\(String(describing: tokens[idx]))" + " in addition expression")
            }
        }
        return result
    }
    
    func parseExpressionAnd() -> Node {
        skipNewLine()
        var result = parseExpressionAdd()
        while (tokens[idx].isEqualTo("&")) {
            idx += 1
            result = AndImmediates(tokens[idx-1].codeOrigin, result , parseExpressionAdd())
        }
        return result
    }
    
    func parseExpression() -> Node {
        skipNewLine();
        var result = parseExpressionAnd()
        while (tokens[idx].isEqualTo("|") || tokens[idx].isEqualTo("^")) {
            if (tokens[idx].isEqualTo("|")) {
                idx += 1
                result = OrImmediates(tokens[idx-1].codeOrigin, result, parseExpressionAnd())
            } else if (tokens[idx].isEqualTo("^")) {
                idx += 1
                result = XorImmediates(tokens[idx-1].codeOrigin, result as! Immediate, parseExpressionAnd() as! Immediate)
            } else {
                print("Invalid token " + "\( String(describing: tokens[idx]))" + " in expression")
            }
        }
        return result
    }
    
    func parseOperand(_ comment: Any) -> Node {
        skipNewLine()
        if (couldBeExpression()) {
            let expr = parseExpression()
            if(tokens[idx].isEqualTo("[")) {
                return parseAddress(expr)
            }
            return expr
        }
        if (tokens[idx].isEqualTo("[")) {
            return parseAddress(Immediate(tokens[idx].codeOrigin, 0))
        }
        if (isLabel(tokens[idx])) {
            let result = LabelReference(tokens[idx].codeOrigin, Label.forName(tokens[idx].codeOrigin, tokens[idx].string))
            idx += 1
            return result
        }
        
        if (isLocalLabel(tokens[idx])) {
            let result = LocalLabelReference(tokens[idx].codeOrigin, LocalLabel.forName(tokens[idx].codeOrigin, tokens[idx].string))
            idx += 1
            return result
        }
        
        parseError(comment)
        return Node(nil)
    }
    
    func parseMacroVariables() -> Array<Variable> {
        skipNewLine()
        consume("^\\($")
        var variables = Array<Variable>()
        while (true) {
            skipNewLine()
            if (tokens[idx].isEqualTo(")")) {
                idx += 1
                break
            } else if(isIdentifier(tokens[idx])) {
                variables.append(Variable.forName(tokens[idx].codeOrigin, tokens[idx].string))
                idx += 1
                skipNewLine()
                if (tokens[idx].isEqualTo(")")) {
                    idx += 1
                    break
                } else if (tokens[idx].isEqualTo(",")) {
                    idx += 1
                } else {
                    parseError(nil)
                }
            } else {
                parseError(nil)
            }
        }
        return variables
    }
    
    func parseSequence(_ final: String?, _ comment: Any?) -> Sequence {
        let firstCodeOrigin = tokens[idx].codeOrigin
        var list: [Node] = []
        while (true) {
            if ((idx == tokens.count && final?.count == 0) || (final!.count > 0 && final!.test(tokens[idx].string))) {
                break
            } else if (tokens[idx] is Annotation) {
                // This is the only place where we can encounter a global
                // annotation, and hence need to be able to distinguish between
                // them.
                // globalAnnotations are the ones that start from column 0. All
                // others are considered localAnnotations.  The only reason to
                // distinguish between them is so that we can format the output
                // nicely as one would expect.
                
                let codeOrigin = tokens[idx].codeOrigin
                list.append(Instruction(codeOrigin, "localAnnotation", [], tokens[idx].string))
                annotation = nil
                idx += 2
            } else if (tokens[idx].isEqualTo("\n")) {
                idx += 1
            } else if (tokens[idx].isEqualTo("const")) {
                idx += 1
                if (!isVariable((tokens[idx]))) {
                    parseError(nil)
                }
                let variable = Variable.forName(tokens[idx].codeOrigin, tokens[idx].string)
                idx += 1
                if (tokens[idx].isNotEqualTo("=")) {
                    parseError(nil)
                }
                idx += 1
                let value = parseOperand("while inside of const " + "\(variable.name)")
                list.append(ConstDecl(tokens[idx].codeOrigin, variable, value))
            } else if (tokens[idx].isEqualTo("error")) {
                list.append(Error(tokens[idx].codeOrigin))
                idx += 1
            } else if (tokens[idx].isEqualTo("if")) {
                var codeOrigin = tokens[idx].codeOrigin
                idx += 1
                skipNewLine()
                var predicate = parsePredicate();
                consume("^((then)|(\n))$")
                skipNewLine()
                var ifThenElse = IfThenElse(codeOrigin, predicate!, parseSequence("^((else)|(end)|(elsif))$", "while inside of \"if " + predicate!.dump() + "\""))
                list.append(ifThenElse)
                while (tokens[idx].isEqualTo("elsif")) {
                    let codeOrigin = tokens[idx].codeOrigin
                    idx += 1
                    skipNewLine()
                    predicate = parsePredicate()
                    consume("^((then)|(\n))$")
                    skipNewLine()
                    if predicate != nil {
                        let elseCase = IfThenElse(codeOrigin, predicate!, parseSequence("^((else)|(end)|(elsif))$", "while inside of \"if " + predicate!.dump() + "\""))
                        ifThenElse.elseCase = elseCase
                        ifThenElse = elseCase
                    }
                }
                if(tokens[idx].isEqualTo("else")) {
                    idx += 1
                    ifThenElse.elseCase = parseSequence("^end$", "while inside of else case for \"if " + predicate!.dump() + "\"")
                    idx += 1
                } else {
                    if (tokens[idx].isNotEqualTo("end")) {
                        parseError(nil)
                    }
                    idx += 1
                }
            } else if (tokens[idx].isEqualTo("macro")) {
                let codeOrigin = tokens[idx].codeOrigin
                idx += 1
                skipNewLine()
                if (!isIdentifier(tokens[idx])) {
                    parseError(nil)
                }
                let name = tokens[idx].string
                idx += 1
                let variables = parseMacroVariables();
                let body = parseSequence("^end$", "while inside of macro " + name as String);
                idx += 1
                list.append(Macro(codeOrigin, name, variables, bod: body))
            } else if(tokens[idx].isEqualTo("global")) {
                let codeOrigin = tokens[idx].codeOrigin
                idx += 1
                skipNewLine()
                let name = tokens[idx].string
                idx += 1
                Label.setAsGlobal(codeOrigin, name)
            } else if (isInstruction(tokens[idx])) {
                let codeOrigin = tokens[idx].codeOrigin
                let name = tokens[idx].string
                idx += 1
                if ((final!.count > 0 && idx == tokens.count) || (final!.count > 0 && final!.test(tokens[idx].toString()))) {
                    // Zero operand instruction, and it's the last one.
                    list.append(Instruction(codeOrigin, name, [] ,annotation))
                    annotation = nil
                    break
                } else if (tokens[idx] is Annotation) {
                    list.append(Instruction(codeOrigin, name, [] ,tokens[idx].string))
                    annotation = nil
                    idx += 2  // Consume the newline as well.
                } else if(tokens[idx].isEqualTo("\n")) {
                    // Zero operand instruction.
                    list.append(Instruction(codeOrigin, name, [], annotation))
                    annotation = nil
                    idx += 1
                } else {
                    // It's definitely an instruction, and it has at least one operand.
                    var operands: [Node] = []
                    var endOfSequence = false
                    while (true) {
                        operands.append(parseOperand("while inside of instruction " + name))
                        if ((final!.count > 0 && idx == tokens.count) || (final!.count > 0 && final!.test(tokens[idx].string))) {
                            // The end of the instruction and of the sequence.
                            endOfSequence = true;
                            break
                        } else if (tokens[idx].isEqualTo(",")) {
                            // Has another operand.
                            idx += 1
                        } else if (tokens[idx] is  Annotation) {
                            annotation = tokens[idx].string;
                            idx += 2; // Consume the newline as well.
                            break;
                        } else if (tokens[idx].isEqualTo("\n")) {
                            // The end of the instruction.
                            idx += 1
                            break;
                        } else {
                            print("Expected a comma, newline, or: " + "\(final)")
                        }
                    }
                    list.append(Instruction(codeOrigin, name, operands , annotation))
                    annotation = nil
                    if (endOfSequence) {
                        break
                    }
                }
            } else if (isIdentifier(tokens[idx])) {
                // Check for potential macro invocation:
                let codeOrigin = tokens[idx].codeOrigin
                let name = tokens[idx].string
                idx += 1
                if (tokens[idx].isEqualTo("(")) {
                    // Macro invocation.
                    idx += 1
                    var operands = Array<Node?>()
                    skipNewLine()
                    if (tokens[idx].isEqualTo(")")) {
                        idx += 1
                    } else {
                        while (true) {
                            skipNewLine()
                            if (tokens[idx].isEqualTo("macro")) {
                                // It's a macro lambda!
                                let codeOriginInner = tokens[idx].codeOrigin
                                idx += 1
                                let variables = parseMacroVariables();
                                let body = parseSequence("^end$", "while inside of anonymous macro passed as argument to " + name)
                                idx += 1
                                operands.append(Macro(codeOriginInner, "", variables, bod: body))
                            } else {
                                operands.append(parseOperand("while inside of macro call to " + name))
                            }
                            
                            skipNewLine()
                            if (tokens[idx].isEqualTo(")")) {
                                idx += 1
                                break
                            } else if (tokens[idx].isEqualTo(",")) {
                                idx += 1
                            } else {
                                print("Unexpected " + "\(String(describing: tokens[idx].string))" + "  while parsing invocation of macro " + "\(name)")
                            }
                        }
                    }
                    
                    if (tokens[idx] is Annotation) {
                        annotation = tokens[idx].string
                        idx += 2
                    }
                    list.append(MacroCall(codeOrigin, name, operands, annotation ?? ""))
                    annotation = nil
                } else {
                    print("Expected after" + "\(name)")
                }
            } else if(isLabel(tokens[idx]) || isLocalLabel(tokens[idx])) {
                let codeOrigin = tokens[idx].codeOrigin
                let name = tokens[idx].string
                idx += 1
                if (tokens[idx].isNotEqualTo(":")){
                    parseError(nil)
                }
                if isLabel(name) {
                    list.append(Label.forName(codeOrigin, name, true))
                } else {
                    list.append(LocalLabel.forName(codeOrigin, name))
                }
                idx += 1
            } else if (tokens[idx].isEqualTo("include")) {
                idx += 1
                if (!isIdentifier(tokens[idx])) {
                    parseError(nil)
                }
                let moduleName = tokens[idx].string
                let fileName = IncludeFile(moduleName, tokens[idx].codeOrigin.fileName()).fileName
                idx += 1
                list.append(parse(fileName))
            } else {
                print("Expecting terminal " + "\(String(describing: final))" + "\(String(describing: comment))")
            }
        }
        return Sequence(firstCodeOrigin, list)
    }
    
    func parseIncludes(_ final: String, _ comment: Any) -> Array<String> {
        let firstCodeOrigin = tokens[idx].codeOrigin
        var fileList = Array<String>()
        fileList.append(tokens[idx].codeOrigin.fileName())
        while (true) {
            if ((idx == tokens.count && final.count > 0) || (final.count > 0 && final.test(tokens[idx].toString()))) {
                break
            } else if (tokens[idx].isEqualTo("include")) {
                idx += 1
                if (!isIdentifier(tokens[idx])) {
                    parseError(nil)
                }
                let moduleName = tokens[idx].string
                let fileName = IncludeFile(moduleName, tokens[idx].codeOrigin.fileName()).fileName
                idx += 1
                fileList.append(fileName)
            } else {
                idx += 1
            }
        }
        
        return fileList
    }
}

func parseData(_ data: String, _ fileName: String) -> Sequence {
    let parser = Parser(data, SourceFile(fileName, nil, nil))
    return parser.parseSequence("", "")
}

func parse(_ fileName: String) -> Sequence {
//    DeBugLog("Load fileName: " + "\(fileName)")
    return parseData(File.open(fileName).read(), fileName)
}

