/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import Glibc

let isDebug: Bool = false
func DeBugLog(_ msg: Any) {
    if isDebug {
        print("\(msg)")
    }
}

/**
 * @State
 * @Tags Jetstream2
 */
class Benchmark {
    var ast: Sequence?
    init() {
        self.ast = nil
    }
    
    /**
     * @Steup
     */
    func setup() {
        InitBytecodes()
        LowLevelInterpreter()
        LowLevelInterpreter64()
        LowLevelInterpreter32_64()
    }
    
    /**
     * @Benchmark
     */
    func runIteration() {
        resetAST()
        ast = parse("LowLevelInterpreter.asm")
    }
    
    func validate() {
        let astDumpedAsLines: Array<String> = ast!.dump().split("\n")
        if (astDumpedAsLines.count != expectedASTDumpedAsLines.count) {
            print("Actual number of lines (" + "\(astDumpedAsLines.count)" + ") differs from expected number of lines(" + "\(expectedASTDumpedAsLines.count)" + ")")
        }
        
        var index = 0
        for line in astDumpedAsLines {
            let expectedLine = expectedASTDumpedAsLines[index]
            if line != expectedLine {
                print("Line #" + "\(index + 1)" + " differs.  Expected: " + "\(expectedLine)" + ",\n got " + "\(line)")
            }
            index += 1
        }
    }
}

func run() {
    let startTime = Timer().getTime()
    
    let benchMark = Benchmark()
    benchMark.setup()
    benchMark.runIteration()
    benchMark.validate()
    let endTime = Timer().getTime()
    let duration = (endTime - startTime) / 1000
    print("OfflineAssembler: ms = \(duration)")
}

class Timer {
    private let CLOCK_REALTIME = 0
    private var time_spec = timespec()
    func getTime() -> Double {
        clock_gettime(Int32(CLOCK_REALTIME),&time_spec)
        return Double(time_spec.tv_sec * 1_000_000 + time_spec.tv_nsec / 1_000)
    }
}



