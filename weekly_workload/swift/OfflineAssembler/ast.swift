/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import Glibc

/*
 * Base utility types for the AST.
 *
 * Valid methods for Node:
 *
 * node.children -> Returns an array of immediate children.  It has been modified
 *     from the original Ruby version to dump directly nearly the original source.
 *
 * node.descendents -> Returns an array of all strict descendants (children
 *     and children of children, transitively).
 *
 */

class SourceFile {
    var fileName: String
    var fileNames = Array<String>()
    var fileNumber: Int
    
    init(_ fileNam: String, _ fileNams: Array<String>?, _ fileNumbe: Int?) {
        fileName = fileNam
        var fileNum = fileNames.firstIndex(of: fileNam)
        if (fileNum == nil) {
            fileNames.append(fileName)
            fileNum = fileNames.count
        } else {
            fileNum! += 1  // File numbers are 1 based
        }
        fileNumber = fileNum!
    }
    
    func name() -> String {
        return fileName
    }
}

/*
 * Define CodeOrigin class for w file path data processing
 */
class CodeOrigin {
    var sourceFile: SourceFile
    var lineNumber: Int
    
    init(_ sourceFil: SourceFile, _ lineNumbe: Int) {
        sourceFile = sourceFil
        lineNumber = lineNumbe
    }
    
    func fileName() -> String {
        return sourceFile.name()
    }
    
    func toString() -> String {
        return fileName() + ":" + String(lineNumber)
    }
}

class Node {
    let codeOrigin: CodeOrigin?
    var isAddress: Bool = false
    var isLabel: Bool = false
    var isImmediate: Bool = false
    var isImmediateOperand: Bool = false
    var isRegister: Bool = false
    
    init(_ codeOrig  : CodeOrigin?) {
        codeOrigin = codeOrig
    }
    
    func codeOriginString() -> String {
        return (codeOrigin != nil) ? codeOrigin!.toString() : ""
    }
    
    func children() -> Array<Node?> {
        return Array<Node?>()
    }
    
    func dump() -> String {
        return codeOriginString()
    }
    
    func value() -> String {
        return ""
    }
}

/*
 * NoChild class, that is, leaf node of tree structure
 */
class NoChildren: Node {
    override func children() -> Array<Node?> {
        return Array<Node?>()
    }
}

func structOffsetKey(_ astStruct: String, _ field: String) -> String {
    return astStruct + "::" + field
}

class StructOffset: NoChildren {
    static var mapping = Dictionary<String, StructOffset>()
    var astStruct: String
    var field: String
    
    init(_ codeOrigin:CodeOrigin, _ astStruc: String, _ fiel: String) {
        astStruct = astStruc
        field = fiel
        super.init(codeOrigin)
        
        isAddress = false
        isLabel = false
        isImmediate = true
        isRegister = false
    }
    
    static func forField(_ codeOrigin: CodeOrigin, _ astStruct: String, _ field: String) -> StructOffset? {
        let key = structOffsetKey(astStruct, field)
        if (StructOffset.mapping[key] == nil) {
            StructOffset.mapping[key] = StructOffset(codeOrigin,  astStruct,  field)
        }
        return StructOffset.mapping[key]
    }
    
    static func resetMappings() {
        StructOffset.mapping = Dictionary()
    }
    
    override func dump() -> String {
        return astStruct + "::" + field
    }
}

class Sizeof: NoChildren {
    static var mapping = Dictionary<String, Sizeof>()
    let astStruct: String
    
    init(_ codeOrigin: CodeOrigin, _ astStruc: String) {
        astStruct = astStruc
        super.init(codeOrigin)
    }
    
    static func forName(_ codeOrigin: CodeOrigin, _ astStruct: String) -> Sizeof {
        if Sizeof.mapping[astStruct] == nil {
            Sizeof.mapping[astStruct] = Sizeof(codeOrigin, astStruct)
        }
        return Sizeof.mapping[astStruct]!
    }
    
    static func resetMappings() {
        Sizeof.mapping = Dictionary()
    }
    
    override func dump() -> String {
        return "sizeof " + astStruct
    }
}

class Immediate: NoChildren {
    let value: UInt64
    
    init(_ codeOrigin: CodeOrigin?, _ valu: UInt64) {
        value = valu
        super.init(codeOrigin)
        
        isAddress = false
        isLabel = false
        isImmediate = true
        isImmediateOperand = true
        isRegister = false
    }
    
    override func dump() -> String {
        return String(value)
    }
    
    func equals(_ other: Immediate) -> Bool {
        return other.value == value
    }
}

class AddImmediates: Node {
    var left: Node?
    var right: Node?
    
    init(_ codeOrigin: CodeOrigin, _ lef: Node?, _ righ: Node?) {
        left = lef
        right = righ
        super.init(codeOrigin)
        
        isAddress = false
        isLabel = false
        isImmediate = true
        isImmediateOperand = true
        isRegister = false
    }
    
    override func children() -> Array<Node?> {
        return [left, right]
    }
    
    override func dump() -> String {
        return "(" + "\(left!.dump())" + " + " + "\(right!.dump())" + ")"
    }
    
    override func value() -> String {
        return left!.value() + right!.value()
    }
}

class SubImmediates: Node {
    let left: Node?
    let right: Node?
    
    init(_ codeOrigin: CodeOrigin, _ lef: Node?, _ righ: Node?) {
        left = lef
        right = righ
        super.init(codeOrigin)
        
        isAddress = false
        isLabel = false
        isImmediate = true
        isImmediateOperand = true
        isRegister = false
    }
    
    override func children() -> Array<Node?> {
        return [left, right]
    }
    
    override func dump() -> String {
        return "(" + "\(left!.dump())" + " - " + "\(right!.dump())" + ")"
    }
    
    override func value() -> String {
        return left!.value() + "-" + right!.value()
    }
}

class MulImmediates: Node {
    let left: Node?
    let right: Node?
    
    init(_ codeOrigin: CodeOrigin, _ lef: Node?, _ righ: Node?) {
        left = lef
        right = righ
        super.init(codeOrigin)
        
        isAddress = false
        isLabel = false
        isImmediate = true
        isImmediateOperand = false
        isRegister = false
    }
    
    override func children() -> Array<Node?> {
        return [left, right]
    }
    
    override func dump() -> String {
        return "(" + "\(left!.dump())" + " * " + "\(right!.dump())" + ")"
    }
}

class NegImmediate: Node {
    let child: Node?
    
    init(_ codeOrigin: CodeOrigin, _ chil: Node?) {
        child = chil
        super.init(codeOrigin)
        
        isAddress = false
        isLabel = false
        isImmediate = true
        isImmediateOperand = false
        isRegister = false
    }
    
    override func children() -> Array<Node?> {
        return [child]
    }
    
    override func dump() -> String {
        return "(-" + child!.dump() + ")"
    }
}

class OrImmediates: Node {
    var left: Node?
    var right: Node?
    
    init(_ codeOrigin: CodeOrigin, _ lef: Node?, _ righ: Node?) {
        left = lef
        right = righ
        super.init(codeOrigin)
        
        isAddress = false
        isLabel = false
        isImmediate = true
        isImmediateOperand = false
        isRegister = false
    }
    
    override func children() -> Array<Node?> {
        return [left, right]
    }
    
    override func dump() -> String {
        return "(" + "\(left!.dump())" + " | " + "\(right!.dump())" + ")"
    }
}

class AndImmediates: Node {
    let left: Node?
    let right: Node?
    
    init(_ codeOrigin: CodeOrigin, _ lef: Node?, _ righ: Node?) {
        left = lef
        right = righ
        super.init(codeOrigin)
        
        isAddress = false
        isLabel = false
        isImmediate = true
        isImmediateOperand = false
        isRegister = false
        
    }
    
    override func children() -> Array<Node?> {
        return [left, right]
    }
    
    override func dump() -> String {
        return "(" + "\(left!.dump())" + " & " + "\(right!.dump())" + ")"
    }
}

class XorImmediates: Node {
    let left: Node?
    let right: Node?
    
    init(_ codeOrigin: CodeOrigin, _ lef: Node?,_ righ: Node?) {
        left = lef
        right = righ
        super.init(codeOrigin)
        
        isAddress = false
        isLabel = false
        isImmediate = true
        isImmediateOperand = false
        isRegister = false
    }
    
    override func children() -> Array<Node?> {
        return [left, right]
    }
    
    override func dump() -> String {
        return "(" + "\(left!.dump())" + " ^ " + "\(right!.dump())" + ")"
    }
}

class BitnotImmediate: Node {
    let child: Node?
    
    init(_ codeOrigin: CodeOrigin,_ chil: Node?) {
        child = chil
        super.init(codeOrigin)
        
        isAddress = false
        isLabel = false
        isImmediate = true
        isImmediateOperand = false
        isRegister = false
    }
    
    override func children() -> Array<Node?> {
        return [child]
    }
    
    override func dump() -> String {
        return "(~" + "\(child!.dump())" + ")"
    }
}

class StringLiteral: NoChildren {
    let value: String
    
    init(_ codeOrigin: CodeOrigin,_ valu: String) {
        value = String(valu.dropFirst().dropLast())
        super.init(codeOrigin)
        
        if !valu.hasPrefix("\"") || !valu.hasSuffix("\"") {
            print("Bad string literal " + "\(value)" + " at " + "\(codeOriginString())")
        }
        
        isAddress = false
        isLabel = false
        isImmediate = false
        isImmediateOperand = false
        isRegister = false
    }
    
    override func dump() -> String {
        return "\"" + "\(value)" + "\""
    }
    
    func equals(other: StringLiteral) -> Bool {
        return other.value == value
    }
}

class RegisterID: NoChildren {
    static var mapping = Dictionary<String, RegisterID>()
    let name: String
    
    init(_ codeOrigin: CodeOrigin,_ nam: String) {
        name = nam
        super.init(codeOrigin)
        
        isAddress = false
        isLabel = false
        isImmediate = false
        isRegister = true
    }
    
    static func forName(_ codeOrigin: CodeOrigin,_ name: String) -> RegisterID {
        if RegisterID.mapping[name] == nil {
            RegisterID.mapping[name] = RegisterID( codeOrigin, name)
        }
        return RegisterID.mapping[name]!
    }
    
    override func dump() -> String {
        return name
    }
}

class FPRegisterID: NoChildren {
    static var mapping = Dictionary<String, FPRegisterID>()
    let name: String
    
    init(_ codeOrigin: CodeOrigin, _ nam: String) {
        name = nam
        super.init(codeOrigin)
        
        isAddress = false
        isLabel = false
        isImmediate = false
        isImmediateOperand = false
        isRegister = true
    }
    
    static func forName(_ codeOrigin: CodeOrigin, _ name: String) -> FPRegisterID {
        if FPRegisterID.mapping[name] == nil {
            FPRegisterID.mapping[name] = FPRegisterID(codeOrigin, name)
        }
        return FPRegisterID.mapping[name]!
    }
    
    override func dump() -> String {
        return name
    }
}

class SpecialRegister: NoChildren {
    let name: String
    
    init(nam: String) {
        name = nam
        super.init(nil)
        
        isAddress = false
        isLabel = false
        isImmediate = false
        isImmediateOperand = false
        isRegister = true
    }
}

class Variable: NoChildren {
    static var mapping = Dictionary<String, Variable>()
    let name: String
    
    init(_ codeOrigin: CodeOrigin, _ nam: String) {
        name = nam
        super.init( codeOrigin)
    }
    
    static func forName(_ codeOrigin: CodeOrigin,_ name: String) -> Variable {
        if Variable.mapping[name] == nil {
            Variable.mapping[name] = Variable( codeOrigin,  name)
        }
        return Variable.mapping[name]!
    }
    
    override func dump() -> String {
        return name
    }
    
    func inspect() -> String {
        return "<variable " + "\(name)" + " at " + "\(String(describing: codeOriginString))"
    }
}

class Address: Node {
    let base: Node
    let offset: Node?
    
    init(_ codeOrigin: CodeOrigin?, _ bas: Node, _ offse: Node?) {
        base = bas
        offset = offse
        super.init(codeOrigin)
        
        if !(base is Variable) && !base.isRegister  {
            print("Bad base for address " + "\(String(describing: base.codeOriginString))" + " at " + "\(codeOrigin?.toString() ?? "")")
        }
        if !(offset is Variable) && !offset!.isImmediate {
            print("Bad offset for address " + "\(String(describing: offset!.codeOriginString))" + " at " + "\(codeOrigin?.toString() ?? "")")
        }
        
        isAddress = true
        isLabel = false
        isImmediate = false
        isImmediateOperand = true
        isRegister = false
    }
    
    override func children() -> Array<Node?> {
        return [base, offset]
    }
    
    override func dump() -> String {
        return offset!.dump() + "[" + "\(base.dump())" + "]"
    }
}

class BaseIndex: Node {
    let base: Node
    let index: Node
    let scale: Int
    let offset: Node?
    
    init(_ codeOrigin: CodeOrigin?, _ bas: Node, _ inde: Node, _ scal: Int, _ offse: Node?) {
        base = bas
        index = inde
        scale = scal
        offset = offse
        super.init(codeOrigin)
        
        if ![1, 2, 4, 8].contains(scale) {
            print("Bad scale " + "\(scale)" + " at " + "\(String(describing: codeOriginString))")
        }
        isAddress = true
        isLabel = false
        isImmediate = false
        isImmediateOperand = false
        isRegister = false
    }
    
    func scaleShift() -> Int? {
        switch scale {
        case 1:
            return 0
        case 2:
            return 1
        case 4:
            return 2
        case 8:
            return 3
        default:
            print("Bad scale " + "\(scale)" + " at " + "\(String(describing: codeOriginString))")
            return nil
        }
    }
    
    override func children() -> Array<Node?> {
        return [base, index, offset]
    }
    
    override func dump() -> String {
        return offset!.dump() + "[" + "\(base.dump())" + ", " + "\(index.dump())" + ", " + "\(scale)" + "]"
    }
}

class AbsoluteAddress: NoChildren {
    var address: Node?
    
    init(_ codeOrigin: CodeOrigin?,_ addres: Node?) {
        address = addres
        super.init(codeOrigin)
        
        isAddress = true
        isLabel = false
        isImmediate = false
        isImmediateOperand = true
        isRegister = false
    }
    
    override func dump() -> String {
        return address!.dump() + "[]"
    }
}

class Instruction: Node {
    let opcode: String
    let operands: Array<Node?>
    let annotation: String?
    
    init(_ codeOrigin: CodeOrigin, _ opcod: String,  _ operand: Array<Node?>, _ annotatio: String? = nil) {
        opcode = opcod
        operands = operand
        annotation = annotatio
        super.init(codeOrigin)
    }
    
    override func children() -> Array<Node?> {
        return Array<Node?>()
    }
    
    override func dump() -> String {
        return "    " + "\(opcode) " + "\(operands.map { $0!.dump() }.joined(separator: ", "))"
    }
}

class Error: NoChildren {
    override func dump() -> String {
        return "    error"
    }
}

class ConstExpr: NoChildren {
    let value: String
    static var mapping = Dictionary<String, ConstExpr>()
    
    init(_ codeOrigin: CodeOrigin, _ valu: String) {
        value = valu
        super.init(codeOrigin)
    }
    
    static func forName(codeOrigin: CodeOrigin, text: String) -> ConstExpr {
        if ConstExpr.mapping[text] == nil {
            ConstExpr.mapping[text] = ConstExpr(codeOrigin, text)
        }
        return ConstExpr.mapping[text]!
    }
    
    static func resetMappings() {
        ConstExpr.mapping = Dictionary()
    }
    
    override func dump() -> String {
        return "constexpr (" + "\(value)" + ")"
    }
    
    func compare(other: ConstExpr) -> Bool {
        return value == other.value
    }
    
    func isImmediates() -> Bool {
        return true
    }
}

class ConstDecl: Node {
    let variable: Variable?
    let valueDecl: Node?
    
    init(_ codeOrigin: CodeOrigin, _ variabl: Variable?, _ valueDec: Node?) {
        variable = variabl
        valueDecl = valueDec
        super.init(codeOrigin)
    }
    
    override func children() -> Array<Node?> {
        return [variable, valueDecl]
    }
    
    override func dump() -> String {
        return "const " + variable!.dump() + " = " + valueDecl!.dump()
    }
}

/*
 * The global variable
 */
var labelMapping = Dictionary<String, Node>()
var referencedExternLabels = Array<Label>()

class Label: NoChildren {
    let name: String
    var extern: Bool = true
    var global: Bool = false
    
    init(_ codeOrigin: CodeOrigin?, _ nam: String) {
        name = nam
        super.init(codeOrigin)
    }
    
    static func forName(_ codeOrigin: CodeOrigin, _ name: String,_ definedInFile: Bool = false) -> Label {
        if labelMapping[name] == nil {
            labelMapping[name] = Label(codeOrigin, name)
        }
        
        if definedInFile {
            (labelMapping[name] as! Label).clearExtern()
        }
        
        return labelMapping[name] as! Label
    }
    
    static func setAsGlobal(_ codeOrigin: CodeOrigin,_ name: String) {
        if labelMapping[name] != nil {
            let label = labelMapping[name] as? Label
            if label!.isGlobal() {
                print("Label: " + "\(name)" + " declared global multiple times")
            }
            label!.setGlobal()
        } else {
            let newLabel = Label(codeOrigin,  name)
            newLabel.setGlobal()
            labelMapping[name] = newLabel
        }
    }
    
    static func resetMappings() {
        labelMapping = Dictionary()
        referencedExternLabels = Array<Label>()
    }
    
    static func resetReferenced() {
        referencedExternLabels = Array<Label>()
    }
    
    func clearExtern() {
        extern = false
    }
    
    func isExtern() -> Bool {
        return extern
    }
    
    func setGlobal() {
        global = true
    }
    
    func isGlobal() -> Bool {
        return global
    }
    
    override func dump() -> String {
        return name + ":"
    }
}

class LocalLabel: NoChildren {
    static var uniqueNameCounter: Int = 0
    var extern: Bool = true
    var name: String
    
    init(_ codeOrigin: CodeOrigin?,_ nam: String) {
        name = nam
        super.init( codeOrigin)
    }
    
    static func forName(_ codeOrigin: CodeOrigin?,_ name: String) -> LocalLabel {
        if labelMapping[name] == nil {
            labelMapping[name] = LocalLabel( codeOrigin,  name)
        }
        return labelMapping[name] as! LocalLabel
    }
    
    func unique(comment: String) -> LocalLabel {
        var newName = "_" + comment
        if labelMapping[newName] != nil {
            newName = "_#" + "\(LocalLabel.uniqueNameCounter)" + "_" + comment
            while labelMapping[newName] != nil {
                LocalLabel.uniqueNameCounter += 1
            }
        }
        return LocalLabel.forName(nil,  newName)
    }
    
    static func resetMappings() {
        LocalLabel.uniqueNameCounter = 0
    }
    
    func cleanName() -> String {
        if name.hasPrefix(".") {
            return "_" + String(name.dropFirst())
        }
        return name
    }
    
    func isGlobal() -> Bool {
        return false
    }
    
    override func dump() -> String {
        return name + ":"
    }
}

class LabelReference: Node {
    var label: Label
    
    init(_ codeOrigin: CodeOrigin,_ labe: Label) {
        label = labe
        super.init(codeOrigin)
        
        isAddress = false
        isLabel = true
        isImmediate = false
        isImmediateOperand = true
    }
    
    override func children() -> Array<Node?> {
        return [label]
    }
    
    func name() -> String {
        return label.name
    }
    
    func isExtern() -> Bool {
        return (labelMapping[name()] as? Label)!.isExtern()
    }
    
    func used() {
        if !referencedExternLabels.contains(where: { $0 === label }) && isExtern() {
            referencedExternLabels.append(label)
        }
    }
    
    override func dump() -> String {
        return label.name
    }
}

class LocalLabelReference: NoChildren {
    var label: LocalLabel
    
    init(_ codeOrigin: CodeOrigin, _ labe: LocalLabel) {
        label = labe
        super.init(codeOrigin)
        
        isAddress = false
        isLabel = true
        isImmediate = false
        isImmediateOperand = true
        
    }
    
    override func children() -> Array<Node?> {
        return [label]
    }
    
    func name() -> String {
        return label.name
    }
    
    override func dump() -> String {
        return label.name
    }
}

class Sequence: Node {
    let list: Array<Node>
    
    init(_ codeOrigin: CodeOrigin, _ lis: Array<Node>) {
        list = lis
        super.init(codeOrigin)
    }
    
    override func children() -> Array<Node?> {
        return list
    }
    
    override func dump() -> String {
        return list.map{$0.dump()}.joined(separator: "\n")
    }
}

class True: NoChildren {
    static func instance() -> True {
        return True(nil)
    }
    
    func value() -> Bool {
        return true
    }
    
    override func dump() -> String {
        return "true"
    }
}

class False: NoChildren {
    static func instance() -> False {
        return False(nil)
    }
    
    func value() -> Bool {
        return false
    }
    
    override func dump() -> String {
        return "false"
    }
}

class Setting: NoChildren {
    static var mapping = Dictionary<String, Setting>()
    var name: String
    
    init(_ codeOrigin: CodeOrigin,_ nam: String) {
        name = nam
        super.init(codeOrigin)
    }
    
    static func forName(_ codeOrigin: CodeOrigin, _ name: String) -> Setting {
        if Setting.mapping[name] == nil {
            Setting.mapping[name] = Setting(codeOrigin, name)
        }
        return Setting.mapping[name]!
    }
    
    static func resetMappings() {
        Setting.mapping = Dictionary()
    }
    
    override func dump() -> String {
        return name
    }
}

class And: Node {
    var left: Node?
    var right: Node
    
    init(_ codeOrigin: CodeOrigin, _ lef: Node?, _ righ: Node) {
        left = lef
        right = righ
        super.init( codeOrigin)
    }
    
    override func children() -> Array<Node?> {
        return [left, right]
    }
    
    override func dump() -> String {
        return "(" + "\(left!.dump())" + " and " + "\(right.dump())" + ")"
    }
}

class Or: Node {
    var left: Node
    var right: Node
    
    init(_ codeOrigin: CodeOrigin, _ lef: Node, _ righ: Node) {
        self.left = lef
        self.right = righ
        super.init( codeOrigin)
    }
    
    override func children() -> Array<Node?> {
        return [left, right]
    }
    
    override func dump() -> String {
        return "(" + "\(left.dump())" + " or " + "\(right.dump())" + ")"
    }
}

class Not: Node {
    var child: Node
    
    init(_ codeOrigin: CodeOrigin, _ chil: Node) {
        child = chil
        super.init( codeOrigin)
    }
    
    override func children() -> Array<Node?> {
        return [child]
    }
    
    override func dump() -> String {
        return "(not" + "\(child.dump())" + ")"
    }
}

class Skip: NoChildren {
    override func dump() -> String {
        return "    skip"
    }
}

class IfThenElse: Node {
    var predicate: Node
    var thenCase: Node
    var elseCase: Node
    
    init(_ codeOrigin: CodeOrigin, _ predicat: Node, _ thenCas: Node) {
        predicate = predicat
        thenCase = thenCas
        elseCase = Skip(codeOrigin)
        super.init(codeOrigin)
    }
    
    override func dump() -> String {
        return "if " + "\(predicate.dump())" + "\n" + "\(thenCase.dump())" + "\nelse\n" + "\(elseCase.dump())" + "\nend"
    }
}

class Macro: Node {
    var name: String
    var variables: Array<Variable>
    var body: Node
    
    init(_ codeOrigin: CodeOrigin,_ nam: String,_ variable: Array<Variable>, bod: Node) {
        name = nam
        variables = variable
        body = bod
        super.init(codeOrigin)
    }
    
    override func dump() -> String {
        return "macro " + "\(name)" + "(" + "\(variables.map { $0.dump() }.joined(separator: ", "))" + ")\n" + "\(body.dump())" + "\nend"
    }
}

class MacroCall: Node {
    var name: String
    var operands: Array<Node?>
    var annotation: String?
    
    init(_ codeOrigin: CodeOrigin, _ nam: String, _ operand: Array<Node?>, _ annotatio: String) {
        name = nam
        operands = operand
        annotation = annotatio
        super.init(codeOrigin)
    }
    
    override func children() -> Array<Node?> {
        return Array<Node?>()
    }
    
    override func dump() -> String {
        return "    " + "\(name)" + "(" + "\(operands.map { $0!.dump() }.joined(separator: ", "))" + ")"
    }
}

/*
 * Reset the static mapping property of the following node classes
 */
func resetAST(){
    StructOffset.resetMappings();
    Sizeof.resetMappings();
    ConstExpr.resetMappings();
    Label.resetMappings();
    LocalLabel.resetMappings();
    Setting.resetMappings();
}










