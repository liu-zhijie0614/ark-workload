/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import Glibc

class Aes{
    var key: [[Int]]
    
    /**
     * Schedule out an AES key for both encryption and decryption.  This
     * is a low-level class.  Use a cipher mode to do bulk encryption.
     *
     * @constructor
     * @param {Array} key The key as an array of 4, 6 or 8 words.
     */
    init(key: [Int]) {
        self.tables = []
        self.key = []
        if (self.tables.count == 0) {
            precompute()
        }
        var i: Int, j: Int, tmp: Int
        var encKey, decKey: [Int]
        let sbox = self.tables[0][4]
        let decTable = self.tables[1]
        let keyLen = key.count
        var rcon: Int = 1
        if (keyLen != 4 && keyLen != 6 && keyLen != 8) {
            fatalError("INVALID: invalid aes block size")
        }
        encKey = Array(key.prefix(key.count))
        decKey = []
        
        i = keyLen
        // schedule encryption keys
        while (i < 4 * keyLen + 28){
            tmp = encKey[i-1]
            // apply sbox
            if (i % keyLen == 0 || (keyLen == 8 && i % keyLen == 4)) {
                tmp = initUInt32(tmp)
                tmp = Int(Int(Int32(sbox[Int(tmp >>> 24)]) << 24) ^
                        sbox[tmp >> 16 & 255] << 16 ^
                        sbox[tmp >> 8 & 255] << 8 ^
                        sbox[tmp & 255])
                // shift rows and add rcon
                if (i%keyLen == 0) {
                    tmp = Int(Int32(tmp) << 8) ^ Int(tmp >>> 24) ^ Int(Int32(rcon) << 24)
                    rcon = (rcon << 1) ^ ((rcon >> 7) * 283)
                }
            }
            var value = encKey[i-keyLen]
            value = initUInt32(value)
            encKey.append(Int(value ^ tmp))
            i += 1
        }
        i = encKey.count
        // schedule decryption keys
        j = 0
        while (i > 0) {
            tmp = encKey[j & 3 != 0 ? i : i - 4]
            tmp = initUInt32(tmp)
            if (i <= 4 || j < 4) {
                decKey.append(tmp)
            } else {
                let value = decTable[0][sbox[Int(tmp >>> 24)]] ^
                            decTable[1][sbox[tmp >> 16 & 255]] ^
                            decTable[2][sbox[tmp >> 8 & 255]] ^
                            decTable[3][sbox[tmp & 255]]
                decKey.append(value)
            }
            j += 1
            i -= 1
        }
        self.key = [encKey, decKey]
    }
    /**
     * The expanded S-box and inverse S-box tables.  These will be computed
     * on the client so that we don't have to send them down the wire.
     *
     * There are two tables, _tables[0] is for encryption and
     * _tables[1] is for decryption.
     *
     * The first 4 sub-tables are the expanded S-box with MixColumns.  The
     * last (_tables[01][4]) is the S-box itself.
     *
     * @private
     */
    var tables:[[[Int]]]
    /**
     * Expand the S-box tables.
     *
     * @private
     */
    func precompute() {
        self.tables = [
          [
            Array(repeating: 0, count: 256),
            Array(repeating: 0, count: 256),
            Array(repeating: 0, count: 256),
            Array(repeating: 0, count: 256),
            Array(repeating: 0, count: 256)
          ],
          [
            Array(repeating: 0, count: 256),
            Array(repeating: 0, count: 256),
            Array(repeating: 0, count: 256),
            Array(repeating: 0, count: 256),
            Array(repeating: 0, count: 256)
          ]
        ]
        var d: [Int] = [Int](0...255)
        var th: [Int] = [Int](0...255)
        var x2: Int = 0
        var x4: Int = 0
        var x8: Int = 0
        var s: Int = 0
        var tEnc: Int = 0
        var tDec: Int = 0
        
        // Compute double and third tables
        for i in 0..<256 {
            d[i] = (i << 1) ^ ((i >> 7) * 283)
            th[d[i] ^ i] = i
        }
        
        var x = 0
        var xInv = 0
        while (self.tables[0][4][x] == 0){
            
            // Compute sbox
            s = xInv ^ (xInv << 1) ^ (xInv << 2) ^ (xInv << 3) ^ (xInv << 4)
            s = s >> 8 ^ s & 255 ^ 99
            self.tables[0][4][x] = s
            self.tables[1][4][s] = x
            
            // Compute MixColumns
            x2 = d[x]
            x4 = d[x2]
            x8 = d[x4]
            tDec = x8 * 0x1010101 ^ x4 * 0x10001 ^ x2 * 0x101 ^ x * 0x1010100
            tEnc = (d[s] * 0x101) ^ (s * 0x1010100)
            
            for i in 0..<4 {
                tEnc = initUInt32(tEnc)
                tDec = initUInt32(tDec)
                tEnc =  Int((Int32(tEnc) << 24) ^ (tEnc >>> 8))
                tDec =  Int((Int32(tDec) << 24) ^ (tDec >>> 8))
                
                self.tables[0][i][x] = tEnc
                self.tables[1][i][s] = tDec
                
            }
            if (x2 != 0) {
                x = x ^ x2
            } else {
                x = x | 1
            }
            
            if (th[xInv] != 0){
                xInv = th[xInv]
            }else{
                xInv = 1
            }
        }
    }
    /**
     * Encrypt an array of 4 big-endian words.
     * @param {Array} data The plaintext.
     * @return {Array} The ciphertext.
     */
    func encrypt(_ data: [Int]) -> [Int] {
        return self.crypt(data, 0)
    }
    /**
     * Decrypt an array of 4 big-endian words.
     * @param {Array} data The ciphertext.
     * @return {Array} The plaintext.
     */
    func decrypt(_ data: [Int]) -> [Int] {
        return self.crypt(data, 1)
    }
    /**
     * Encryption and decryption core.
     * @param {Array} input Four words to be encrypted or decrypted.
     * @param dir The direction, 0 for encrypt and 1 for decrypt.
     * @return {Array} The four encrypted or decrypted words.
     * @private
     */
    func crypt(_ input: [Int],_ dir: Int) -> [Int] {
        if input.count != 4 {
            fatalError("INVALID: invalid aes block size")
        }
        
        let key = self.key[dir]
        var a = input[0] ^ key[0]
        var b = input[dir == 1 ? 3 : 1] ^ key[1]
        var c = input[2] ^ key[2]
        var d = input[dir == 1 ? 1 : 3] ^ key[3]
        var a2: Int, b2: Int, c2: Int
        
        let nInnerRounds = key.count / 4 - 2
        var kIndex = 4
        var out = [Int](repeating: 0, count: 4)
        let table = self.tables[dir]
        
        let t0 = table[0]
        let t1 = table[1]
        let t2 = table[2]
        let t3 = table[3]
        let sbox = table[4]
        
        // Inner rounds.
        for _ in 0..<nInnerRounds {
            a = initUInt32(a)
            b = initUInt32(b)
            c = initUInt32(c)
            d = initUInt32(d)
            a2 = t0[Int(a >>> 24)] ^
                 t1[Int(Int32(b) >> 16 & 255)] ^
                 t2[Int(Int32(c) >> 8 & 255)] ^
                 t3[Int(d & 255)] ^
                 key[kIndex]
            b2 = t0[Int(b >>> 24)] ^
                 t1[Int(Int32(c) >> 16 & 255)] ^
                 t2[Int(Int32(d) >> 8 & 255)] ^
                 t3[Int(a & 255)] ^
                 key[kIndex + 1]
            c2 = t0[Int(c >>> 24)] ^
                 t1[Int(Int32(d) >> 16 & 255)] ^
                 t2[Int(Int32(a) >> 8 & 255)] ^
                 t3[Int(b & 255)] ^
                 key[kIndex + 2]
            d = t0[Int(d >>> 24)] ^
                t1[Int(Int32(a) >> 16 & 255)] ^
                t2[Int(Int32(b) >> 8 & 255)] ^
                t3[Int(c & 255)] ^
                key[kIndex + 3]
            kIndex += 4
            a = a2
            b = b2
            c = c2
        }
        
        for i in 0..<4 {
            var keyIndex = key[kIndex]
            keyIndex = initUInt32(keyIndex)
            kIndex += 1
            out[(dir != 0) ? (3 & -i) : i] = Int(Int32(sbox[Int(a >>> 24)]) << 24) ^
                                             Int(Int32(sbox[Int((Int32(b) >> 16) & 255)]) << 16) ^
                                             Int(Int32(sbox[Int((Int32(c) >> 8) & 255)]) << 8) ^
                                             Int(Int32(sbox[Int(d & 255)])) ^
                                             Int(Int32(keyIndex))
            let a2 = a
            a = b
            b = c
            c = d
            d = a2
        }
        return out
    }
}

/** @fileOverview Arrays of bits, encoded as arrays of Numbers.
 *
 * @author Emily Stark
 * @author Mike Hamburg
 * @author Dan Boneh
 */

/**
 * Arrays of bits, encoded as arrays of Numbers.
 * @namespace
 * @description
 * <p>
 * These objects are the currency accepted by SJCL's crypto functions.
 * </p>
 *
 * <p>
 * Most of our crypto primitives operate on arrays of 4-byte words internally,
 * but many of them can take arguments that are not a multiple of 4 bytes.
 * This library encodes arrays of bits (whose size need not be a multiple of 8
 * bits) as arrays of 32-bit words.  The bits are packed, big-endian, into an
 * array of words, 32 bits at a time.  Since the words are double-precision
 * floating point numbers, they fit some extra data.  We use this (in a private,
 * possibly-changing manner) to encode the number of bits actually  present
 * in the last word of the array.
 * </p>
 *
 * <p>
 * Because bitwise ops clear this out-of-band data, these arrays can be passed
 * to ciphers like AES which want arrays of words.
 * </p>
 */
class BitArray {
    /**
     * Find the length of an array of bits.
     * @param {bitArray} a The array.
     * @return {Number} The length of a, in bits.
     */
    static func bitLength(_ a: [Int]) -> Int {
        let l = a.count
        var x: Int?
        if (l == 0 ){
            return 0
        }
        x = a[l - 1]
        return (l - 1) * 32 + BitArray.getPartial(x!)
    }
    
    /**
     * Get the number of bits used by a partial word.
     * @param {Number} x The partial word.
     * @return {Number} The number of bits used by the partial word.
     */
    static func getPartial(_ x: Int) -> Int {
        return Int(round(Double(x) / 0x10000000000) != 0 ? round(Double(x) / 0x10000000000) : 32)
    }
    
    /**
     * Compare two arrays for equality in a predictable amount of time.
     * @param {bitArray} a The first array.
     * @param {bitArray} b The second array.
     * @return {boolean} true if a == b; false otherwise.
     */
    static func equal(_ a: [Int],_ b: [Int]) -> Bool {
        if BitArray.bitLength(a) != BitArray.bitLength(b) {
            return false
        }
        var x: Int = 0
        for i in 0..<a.count {
            var n = a[i]
            var m = b[i]
            n = initUInt32(n)
            m = initUInt32(m)
            x |= n ^ m
        }
        return x == 0
    }
}

infix operator >>> : BitwiseShiftPrecedence

func >>> (lhs: Int, rhs: Int) -> Int32 {
    return Int32(bitPattern: UInt32(bitPattern: Int32(lhs)) >> UInt32(Int32(rhs)))
}


func initUInt32(_ a: Int) -> Int{
    var tmp = a
    if (tmp > 2147483647) {
        let max = 4294967295
        tmp = tmp % (max + 1)
        if (tmp > 2147483647){
            tmp = tmp - max - 1
        }
    }else if (tmp < -2147483648){
        let max = 4294967295
        tmp = tmp % (max + 1)
        if (tmp < -2147483648) {
            tmp = tmp + max + 1
        }
    }
    return tmp
}

/*
 * Copyright (C) 2018 Apple Inc. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY APPLE INC. AND ITS CONTRIBUTORS ``AS IS''
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL APPLE INC. OR ITS CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

func assert(_ b: Bool){
    if (!b) {
        fatalError("Assertion failed")
    }
}

/*
 * @State
 * @Tags Jetstream2
 */
class Benchmark {
    var dataArr: [[String:[Int]]]
    init() {
        dataArr = getWaypoints()
    }
    /*
     * @Benchmark
     */
    func runIteration() {
        let startTime = Timer().getTime()
        for _ in 0..<3 {
            for i in 0..<dataArr.count{
                let item = dataArr[i]
                let len = 32 * item["key"]!.count
                let aes =  Aes(key: item["key"]!)
                if (i % 500 == 0){
                    // DeBugLog("第\(i + 1)条：加密后数据：\(aes.encrypt(item["pt"]!))")
                    // DeBugLog("第\(i + 1)条：解密后数据：\(aes.decrypt(item["ct"]!))")
                }
                assert(BitArray.equal(aes.encrypt(item["pt"]!), item["ct"]!))
                assert(BitArray.equal(aes.decrypt(item["ct"]!), item["pt"]!))
            }
        }
        let endTime = Timer().getTime()
        print("stanford-crypto-aes: ms = " + "\(endTime - startTime)")
    }
    
}

class Timer {
    private let CLOCK_REALTIME = 0
    private var time_spec = timespec()
    func getTime() -> Double {
        clock_gettime(Int32(CLOCK_REALTIME),&time_spec)
        return Double(time_spec.tv_sec * 1_000_000 + time_spec.tv_nsec / 1_000)/1000
    }
}
let isDebug : Bool = false
func DeBugLog(_ msg : Any , file: String = #file, line: Int = #line, fn: String = #function){
    if isDebug {
        let prefix = "\(msg)"
        print(prefix)
    }
}

func getWaypoints() -> [[String:[Int]]] {
    var filepath=""
    var current_directory:[CChar] = Array(repeating: 0, count: Int(FILENAME_MAX))
    let rs=getcwd(&current_directory, Int(FILENAME_MAX))
    if rs != nil {
        let a = current_directory.compactMap { $0 == 0 ? nil:Character(UnicodeScalar(UInt32($0))!)}
        filepath = String(a) + "/stanford-crypto-aes.json"
    }
    if let file = fopen(filepath, "r") {
        var file_stat = stat()
        stat(filepath,&file_stat)
        var buffer: [UInt8] = Array(repeating: 0, count: Int(file_stat.st_size))
        let bytes_read=fread(&buffer, 1, Int(file_stat.st_size), file)
        if bytes_read > 0 {
            let chars = buffer.compactMap  { c in
                var char = Character(UnicodeScalar(c))
                if char == "\n" {
                    char = Character(" ")
                }
                return char
            }
            
            let content=String(chars)
            do {
                if let result2 = try JsonParser.parse(text: content) {
                    let r=result2 as? [[String:[Int]]] ?? []
                    return r
                } else {
                    return []
                }
            } catch {
                return []
            }
        }
        fclose(file)
    }
    return []
}



Benchmark().runIteration()





/// 类型和数据结构定义
public enum JSON {
    case object([String: JSON])
    case array([JSON])
    case string(String)
    case double(Double)
    case int(Int)
    case bool(Bool)
    case null

    public init(_ value: Int) {
        self = .int(value)
    }

    public init(_ value: Double) {
        self = .double(value)
    }

    public init(_ value: [JSON]) {
        self = .array(value)
    }

    public init(_ value: [String: JSON]) {
        self = .object(value)
    }

    public init(_ value: String) {
        self = .string(value)
    }

    public init(_ value: Bool) {
        self = .bool(value)
    }
}

extension JSON: CustomStringConvertible {
    public var description: String {
        switch self {
        case let .string(v): return "String(\(v))"
        case let .double(v): return "Double(\(v))"
        case let .int(v): return "Int(\(v)"
        case let .bool(v): return "Bool(\(v))"
        case let .array(a): return "Array(\(a.description))"
        case let .object(o): return "Object(\(o.description))"
        case .null: return "Null"
        }
    }
}

extension JSON: Equatable {
    public static func == (lhs: JSON, rhs: JSON) -> Bool {
        switch (lhs, rhs) {
        case let (.string(l), .string(r)): return l == r
        case let (.double(l), .double(r)): return l == r
        case let (.int(l), .int(r)): return l == r
        case let (.bool(l), .bool(r)): return l == r
        case let (.array(l), .array(r)): return l == r
        case let (.object(l), .object(r)): return l == r
        case (.null, .null): return true
        default: return false
        }
    }
    @inlinable public subscript(key: String) -> JSON?{
        switch self {
        case let .object(o): return o[key]
        default: return nil
        }
    }
}

struct ParserError: Error, CustomStringConvertible {
    let message: String
    
    init(msg: String) {
        self.message = msg
    }
    
    var description: String {
        return "ParserError: \(message)"
    }
}

//
//  Parser2.swift
//  json
//
//  Created by mac on 2021/4/14.
//


struct JsonParserError: Error {
    var msg: String
    init(msg: String) {
        self.msg = msg
    }
}

/// 分词 + AST
// [自己动手实现一个简单的JSON解析器](https://segmentfault.com/a/1190000010998941)

// 1. 词法分析：按照构词规则将 JSON 字符串解析成 Token 流
// 2. 语法分析：根据 JSON 文法检查上面 Token 序列所构词的 JSON 结构是否合法

enum JsonToken: Equatable {
    case objBegin // {
    case objEnd   // }
    case arrBegin // [
    case arrEnd   // ]
    case null    // null
    case number(String)   // 1
    case string(String)   // "a"
    case bool(String)     // true false
    case sepColon // :
    case sepComma // ,
    
    static func == (lhs: JsonToken, rhs: JsonToken) -> Bool {
        switch (lhs, rhs) {
        case let (.string(l), .string(r)): return l == r
        case let (.number(l), .number(r)): return l == r
        case let (.bool(l), .bool(r)): return l == r
        case (.null, .null): return true
        case (.objEnd, .objEnd): return true
        case (.objBegin, .objBegin): return true
        case (.arrBegin, .arrBegin): return true
        case (.arrEnd, .arrEnd): return true
        case (.sepComma, .sepComma): return true
        case (.sepColon, .sepComma): return true
        default: return false
        }
    }
    
}

/// 分词
struct JsonTokenizer {
    
    private var input: String
    
    private var currentIndex: String.Index
    
    init(string: String) {
        self.input = string
        self.currentIndex = string.startIndex
    }
    
    /// 当前字符
    private var current: Character? {
        guard currentIndex < input.endIndex else {return nil}
        return input[currentIndex]
    }
    
    /// 移动到下一个且返回其值
    private mutating func peekNext() -> Character? {
        advance()
        return current
    }
    
    /// 移动下标
    private mutating func advance() {
        currentIndex = input.index(after: currentIndex)
    }
    
    private mutating func back() {
        currentIndex = input.index(before: currentIndex)
    }
    
    mutating func nextToken() throws -> JsonToken? {
        scanSpaces()
        guard let ch = current else {
            return nil
        }
        
//      print("nextToken::ch:\(ch)")
        
        switch ch {
        case "{":
            advance()
            return JsonToken.objBegin
        case "}":
            advance()
            return JsonToken.objEnd
        case "[":
            advance()
            return JsonToken.arrBegin
        case "]":
            advance()
            return JsonToken.arrEnd
        case ",":
            advance()
            return JsonToken.sepComma
        case ":":
            advance()
            return JsonToken.sepColon
        case "n":
            let _ = try scanMatch(string: "null")
            advance()
            return JsonToken.null
        case "t":
            let str = try scanMatch(string: "true")
            return JsonToken.bool(str)
        case "f":
            let str = try scanMatch(string: "false")
            return JsonToken.bool(str)
        case "\"":
            let str = try scanString()
            advance()
            return JsonToken.string(str)
        case _ where isNumber(c: ch):
            let str = try scanNumbers()
            return JsonToken.number(str)
        default:
              throw JsonParserError(msg: "无法解析的字符:\(ch) - \(currentIndex)")
        }
    }
    
    mutating func scanString() throws -> String {
        var ret:[Character] = []
        
        repeat {
            guard let ch = peekNext() else {
                throw JsonParserError(msg: "scanString 报错，\(currentIndex) 报错")
            }
            switch ch {
            case "\\": // 处理转义字符
                guard let cn = peekNext(), !isEscape(c: cn) else {
                    throw JsonParserError(msg: "无效的特殊类型的字符")
                }
                ret.append("\\")
                ret.append(cn)
                /// 处理 unicode 编码
                if cn == "u" {
                    try ret.append(contentsOf: scanUnicode())
                }
            case "\"": // 碰到另一个引号，则认为字符串解析结束
                return String(ret)
            case "\r", "\n": // 传入JSON 字符串不允许换行
                throw JsonParserError(msg: "无效的字符\(ch)")
            default:
                ret.append(ch)
            }
        } while (true)
    }
    
    mutating func scanUnicode() throws -> [Character] {
        var ret:[Character] = []
        for _ in 0..<4 {
            if let ch = peekNext(), isHex(c: ch) {
                ret.append(ch)
            } else {
                throw JsonParserError(msg: "unicode 字符不规范\(currentIndex)")
            }
        }
        return ret
    }
    
    mutating func scanNumbers() throws -> String {
        let ind = currentIndex
        while let c = current, isNumber(c: c) {
            advance()
        }
        if currentIndex != ind {
            return String(input[ind..<currentIndex])
        }
        throw JsonParserError(msg: "scanNumbers 出错:\(ind)")
    }
    
    /// 跳过空格
    mutating func scanSpaces() {
        var ch = current
        while ch != nil && ch == " " {
            ch = peekNext()
        }
    }
    
    mutating func scanMatch(string: String) throws -> String {
        return try scanMatch(characters: string.map { $0 })
    }
    
    mutating func scanMatch(characters: [Character]) throws -> String {
        let ind = currentIndex
        var isMatch = true
        for index in (0..<characters.count) {
            if characters[index] != current {
                isMatch = false
                break
            }
            advance()
        }
        if (isMatch) {
            return String(input[ind..<currentIndex])
        }
        throw JsonParserError(msg: "scanUntil 不满足 \(characters)")
    }
    
    func isEscape(c: Character) -> Bool {
        // \" \\ \u \r \n \b \t \f
        return ["\"", "\\", "u", "r", "n", "b", "t", "f"].contains(c)
    }
    
    /// 判断是否是数字字符
    func isNumber(c: Character) -> Bool {
        let chars:[Character: Bool] = ["-": true, "+": true, "e": true, "E": true, ".": true]
        if let b = chars[c], b {
            return true
        }
        
        if(c >= "0" && c <= "9") {
            return true
        }
        
        return false;
    }

    /// 判断是否为 16 进制字符
    func isHex(c: Character) -> Bool {
        return c >= "0" && c <= "9" || c >= "a" && c <= "f" || c >= "A" && c <= "F"
    }
}

// 语法解析
struct JsonParser {
    private var tokenizer: JsonTokenizer
    
    private init(text: String) {
        tokenizer = JsonTokenizer(string: text)
    }
    
    static func parse(text: String) throws -> Any? {
        var parser = JsonParser(text: text)
        return try parser.parse()
    }
    
    
    private mutating func parseElement() throws -> Any? {
        guard let nextToken = try tokenizer.nextToken() else {
            return nil
        }
        
        switch nextToken {
        case .arrBegin:
            return try parserArr()
        case .objBegin:
            return try parserObj()
        case .bool(let b):
            return (b == "true")
        case .null:
            return nil
        case .string(let str):
            return str
        case .number(let n):
            if n.contains("."), let v = Double(n) {
                return v
            } else if let v = Int(n) {
                return v
            } else {
                throw ParserError(msg: "number 转换失败")
            }
        default:
            throw ParserError(msg: "未知 element: \(nextToken)")
        }
    }
    
    private mutating func parserArr() throws -> [Any] {
        var arr: [Any] = []
        repeat {
            guard let ele = try parseElement() else {
                throw ParserError(msg: "parserArr 解析失败")
            }
            arr.append(ele)
            
            guard let next = try tokenizer.nextToken() else {
                throw ParserError(msg: "parserArr 解析失败")
            }
            
            if case JsonToken.arrEnd = next {
                break
            }
            
            if JsonToken.sepComma != next { // ","
                throw ParserError(msg: "parserArr 解析失败")
            }
            
        } while true

        return arr
    }
    
    private mutating func parserObj() throws -> [String: Any] {
        var obj: [String: Any] = [:]

        repeat {
            
            guard let next = try tokenizer.nextToken(), case let .string(key) = next else {
                throw ParserError(msg: "parserObj 错误, key 不存在")
            }
            
            if obj.keys.contains(key) {
                throw ParserError(msg: "parserObj 错误, 已经存在key: \(key)")
            }
            
            guard let comma = try tokenizer.nextToken(), case JsonToken.sepColon = comma else {
                throw ParserError(msg: "parserObj 错误，：不存在")
            }
            
            guard let value = try parseElement() else {
                throw ParserError(msg: "parserObj 错误，值不存在")
            }
            
            obj[key] = value
            
            guard let nex = try tokenizer.nextToken() else {
                throw ParserError(msg: "parserObj 错误， 下一个值不存在")
            }
            
            if case JsonToken.objEnd = nex {
                break
            }
            
            if JsonToken.sepComma != nex {
                throw ParserError(msg: "parserObj 错误，, 不存在")
            }
        } while true

        return obj
    }
    
    private mutating func parse() throws  -> Any? {
        guard let token = try tokenizer.nextToken() else {
            return nil
        }
        switch token {
        case .arrBegin:
            return try parserArr()
        case .objBegin:
            return try parserObj()
        default:
            return nil
        }
    }
    
}
