import Glibc
class Timer {
    private let CLOCK_REALTIME = 0
    private var time_spec = timespec()
    func getTime() -> Double {
        clock_gettime(Int32(CLOCK_REALTIME),&time_spec)
        return Double(time_spec.tv_sec * 1_000_000 + time_spec.tv_nsec / 1_000)/1000
    }
}
// Performance.now is used in latency benchmarks, the fallback is Date.now.
func performanceNow() -> Double{
    return Timer().getTime()
}
func deBugLog(str: String) {
    print(str)
}   

// Configuration.
let kSplayTreeSize = 8000;
let kSplayTreeModifications = 80;
let kSplayTreePayloadDepth = 5;
var splaySampleTimeStart = 0.0;
var splaySamples:[Double] = [];
var seed: UInt64 = 49734321

typealias SetupFunction = () -> Any?
typealias SplayFunction<T> = () -> T

protocol Runner {
    func notifyError(_ name: String,_ error:String)
    func notifyStep(_ name: String)
}

class SuiteData {
    var runs:Int = 0
    var elapsed:Double=0
    init(_ runs: Int, _ elapsed: Double) {
        self.runs = runs
        self.elapsed = elapsed
    }
}
// Simple framework for running the benchmark suites and
// computing a score based on the timing measurements.


// A benchmark has a name (string) and a function that will be run to
// do the performance measurement. The optional setup and tearDown  
// arguments are functions that will be invoked before and after
// running the benchmark, but the running time of these functions will
// not be accounted for in the benchmark score.

class Benchmark {
    var name: String
    var doWarmup: Bool
    var doDeterministic: Bool
    var run: SplayFunction<Void>
    var setup: SplayFunction<Void>?
    var tearDown: SplayFunction<Void>?
    var latencyResult: SplayFunction<[Double]>?
    var minIterations: Int

    init(name: String, doWarmup: Bool = true, doDeterministic: Bool = true, run: @escaping SplayFunction<Void>, setup: SplayFunction<Void>? = nil, tearDown: SplayFunction<Void>? = nil, latencyResult: @escaping SplayFunction<[Double]>, minIterations: Int = 32) {
        self.name = name
        self.doWarmup = doWarmup
        self.doDeterministic = doDeterministic
        self.run = run
        self.setup = setup
        self.tearDown = tearDown
        self.latencyResult = latencyResult
        self.minIterations = minIterations
    }
}

// Benchmark results hold the benchmark and the measured time used to
// run the benchmark. The benchmark score is computed later once a
// full benchmark suite has run to completion. If latency is set to 0
// then there is no latency score for this benchmark.

class BenchmarkResult {
    var benchmark: Benchmark
    var time: Double
    var latency: Double

    init(benchmark: Benchmark, time: Double, latency: Double) {
        self.benchmark = benchmark
        self.time = time
        self.latency = latency
    }

    // Automatically convert results to numbers. Used by the geometric
    // mean computation.
    func valueOf() -> Double {
        return self.time
    }
}

// Suites of benchmarks consist of a name and the set of benchmarks in
// addition to the reference timing that the final score will be based
// on. This way, all scores are relative to a reference run and higher
// scores implies better performance.

class BenchmarkSuite {
    let name: String
    let reference: [Int]
    var benchmarks: [Benchmark]
    var results:[BenchmarkResult] = []
    var runner:Runner?
    // Keep track of all declared benchmark suites.
    static var suites = [BenchmarkSuite]()
    static let version = "9"
    static var scores: [Double] = []
    

    init(name: String, reference: [Int], benchmarks: [Benchmark]) {
        self.name = name
        self.reference = reference
        self.benchmarks = benchmarks
        BenchmarkSuite.suites.append(self)
    }

    // To make the benchmark results predictable, we replace Math.random
    // with a 100% deterministic alternative.
    static func ResetRNG(){
        seed = 49734321;
    }
    static func random() -> Double {
        // Robert Jenkins' 32 bit integer hash function.
        seed = ((seed + 0x7ed55d16) + (seed << 12)) & 0xffffffff
        seed = ((seed ^ 0xc761c23c) ^ (seed >> 19)) & 0xffffffff
        seed = ((seed + 0x165667b1) + (seed << 5)) & 0xffffffff
        seed = ((seed + 0xd3a2646c) ^ (seed << 9)) & 0xffffffff
        seed = ((seed + 0xfd7046c5) + (seed << 3)) & 0xffffffff
        seed = ((seed ^ 0xb55a4f09) ^ (seed >> 16)) & 0xffffffff
        return Double(seed) / Double(0x10000000)
    }

    func RunStep(runner: Runner) -> Any? {
        BenchmarkSuite.ResetRNG()
        self.results=[];
        self.runner=runner;
        let length = self.benchmarks.count;
        var index = 0;
        let suite = self;
        var data:SuiteData? = nil;
        //@Setup
        // Run the setup, the actual benchmark, and the tear down in three
        // separate steps to allow the framework to yield between any of the
        // steps.
        func runNextSetup() -> Any? {
            if index < length {
                suite.benchmarks[index].setup!()
                return runNextBenchmark
            }
            return nil
        }
        // @Benchmark
        func runNextBenchmark() -> Any? {
            data = suite.RunSingleBenchmark(suite.benchmarks[index], data)
            // If data is nil, we're done with this benchmark.
            return (data == nil) ? runNextTearDown : runNextBenchmark()
        }
        // @Teardown
        func runNextTearDown() -> Any? {
            suite.benchmarks[index].tearDown!()
            index += 1
            return runNextSetup
        }

        return runNextSetup();
    }

    // Runs all registered benchmark suites and optionally yields between
    // each individual benchmark to avoid running for too long in the
    // context of browsers. Once done, the final score is reported to the
    // runner.
    static func RunSuites(runner: Runner) {
        var continuation: Any? = nil
        let suites = self.suites
        let length = suites.count
        BenchmarkSuite.scores = [];
        var index = 0
        func runStep() {
            while continuation != nil || index < length {
                if let con = (continuation as? SetupFunction) {
                    continuation = con()
                } else {
                    let suite = suites[index]
                    index += 1
                    continuation = suite.RunStep(runner: runner)
                }

                if continuation != nil {
                    runStep()
                    return
                }
            }
        }
        runStep()
    }

    

    // Runs a single benchmark for at least a second and computes the
    // average time it takes to run a single iteration.
    func RunSingleBenchmark(_ benchmark: Benchmark, _ data: SuiteData? = nil) -> SuiteData? {
        var data=data
        func Measure(_ data:SuiteData? = nil){
            var elapsed: Double = 0
            let start: Double = performanceNow()
            var i = 0
            // Run either for 1 second or for the number of iterations specified
            // by minIterations, depending on the config flag doDeterministic.
            while (benchmark.doDeterministic ? i < benchmark.minIterations : elapsed < 1000) {
                benchmark.run()
                elapsed = performanceNow() - start
                i += 1
            }
            if let data = data {
                data.runs += i
                data.elapsed += elapsed
            }
        }
        // Sets up data in order to skip or not the warmup phase.
        if !benchmark.doWarmup && data == nil {
            data = SuiteData(0, 0)
        }

        if data == nil {
            Measure(nil);
            return SuiteData(0, 0)
        } else {
            Measure(data);
            // If we've run too few iterations, we continue for another second.
            if data!.runs < benchmark.minIterations { return data}
            let usec = Double(data!.elapsed * 1_000) / Double(data!.runs)
            let latencySamples = (benchmark.latencyResult != nil) ? benchmark.latencyResult!() : [0]
            let percentile = 99.5
            let latency = averageAbovePercentile(latencySamples, percentile) * 1_000
            let benchmarkResult=BenchmarkResult(benchmark: benchmark, time: usec, latency: latency);
            notifyStep(benchmarkResult)
            print("\nsplay-latency: ms = \(latency/1000)")
            return nil
        }
    }


    // Computes the average of the worst samples. For example, if percentile is 99, this will report the
    // average of the worst 1% of the samples.
    func averageAbovePercentile(_ numbers: [Double], _ percentile: Double) -> Double {
        var numbersCopy = numbers
        numbersCopy.sort()

        // Now the elements we want are at the end. Keep removing them until the array size shrinks too much.
         // Examples assuming percentile = 99:
         //
         // - numbers.length starts at 100: we will remove just the worst entry and then not remove anymore,
         //   since then numbers.length / originalLength = 0.99.
         //
         // - numbers.length starts at 1000: we will remove the ten worst.
         //
         // - numbers.length starts at 10: we will remove just the worst.
        var numbersWeWant: [Double] = []
        let originalLength = Double(numbersCopy.count)
        while Double(numbersCopy.count) / originalLength > percentile / 100 {
            numbersWeWant.append(numbersCopy.removeLast())
        }
        var sum = 0.0
        for number in numbersWeWant {
            sum += number
        }
        let result = sum / Double(numbersWeWant.count)
        // Do a sanity check.
        if !numbersCopy.isEmpty && result < numbersCopy[numbersCopy.count - 1] {
            return result
        }

        return result
    }

    // Converts a score value to a string with at least three significant
    // digits.
    static func formatScore(_ value: Double) -> String {
        if value > 100 {
            return String(value)
        } else {
            return String(value)
        }
    }

    // Notifies the runner that we're done running a single benchmark in
    // the benchmark suite. This can be useful to report progress.
    func notifyStep(_ result: BenchmarkResult) {
        self.results.append(result)
        if self.runner?.notifyStep != nil {
            self.runner?.notifyStep(result.benchmark.name)
        }
    }

   

    // Notifies the runner that running a benchmark resulted in an error.
    func notifyError(_ error: String) {
        if let runner = self.runner {
            runner.notifyError(self.name, error)
        }
        if let runner = self.runner {
            runner.notifyStep(self.name)
        }
    }
}

/**
 * Constructs a Splay tree.  A splay tree is a self-balancing binary
 * search tree with the additional property that recently accessed
 * elements are quick to access again. It performs basic operations
 * such as insertion, look-up and removal in O(log(n)) amortized time.
 *
 * @constructor
 */
class SplayTree {
    // Pointer to the root node of the tree.
    var root:Node? = nil
    // @return {boolean} Whether the tree is empty.
    func isEmpty() -> Bool {
        return self.root == nil
    }

    /**
     * Inserts a node into the tree with the specified key and value if
     * the tree does not already contain a node with the specified key. If
     * the value is inserted, it becomes the root of the tree.
     *
     * @param {number} key Key to insert into the tree.
     * @param {*} value Value to insert into the tree.
     */
    func insert(key: Double, value: Any) {
        if isEmpty() {
            root = Node(key: key, value: value)
            return
        }
        splay(key)
        if let rootKey = root?.key {
            if key == rootKey {
                return
            }
        }
        let newNode = Node(key: key, value: value)
        if key > root!.key {
            newNode.left = root
            newNode.right = root!.right
            root?.right = nil
        } else {
            newNode.right = root
            newNode.left = root!.left
            root!.left = nil
        }
        root = newNode
    }

    /**
     * Removes a node with the specified key from the tree if the tree
     * contains a node with this key. The removed node is returned. If the
     * key is not found, an exception is thrown.
     *
     * @param {number} key Key to find and remove from the tree.
     * @return {SplayTree.Node} The removed node.
     */
    func remove(key: Double) -> Node {
        splay(key)
        let removed = root
        if root!.left == nil {
            root = root?.right
        } else {
            let right = root?.right
            root = root?.left
            // Splay to make sure that the new root has an empty right child.
            splay(key)
            // Insert the original right child as the right child of the new root.
            root?.right = right
        }
        return removed!
    }

    /**
     * Returns the node having the specified key or null if the tree doesn't contain
     * a node with the specified key.
     *
     * @param {number} key Key to find in the tree.
     * @return {SplayTree.Node} Node having the specified key.
     */
    func find(_ key: Double) -> Node? {
        if isEmpty() {
            return nil
        }
        splay(key)
        return root?.key == key ? root : nil
    }

    /**
     * @return {SplayTree.Node} Node having the maximum key value.
     */
    func findMax(_ opt_startNode: Node? = nil) -> Node? {
        if isEmpty() {
            return nil
        }
        var current = opt_startNode ?? root!
        while let rightChild = current.right {
            current = rightChild
        }
        return current
    }

    /**
     * @return {SplayTree.Node} Node having the maximum key value that
     *     is less than the specified key value.
     */
    func findGreatestLessThan(_ key: Double) -> Node? {
        if root == nil {
            return nil
        }
        splay(key)
        if root!.key < key {
            return root
        } else if let left = root!.left {
            return findMax(left)
        } else {
            return nil
        }
    }

    /**
     * @return {Array<*>} An array containing all the keys of tree's nodes.
     */
    func exportKeys() -> [Double] {
        var result: [Double] = []
        if !isEmpty() {
            root?.traverse_(f: { node in
                result.append(node.key)
            })
        }
        return result
    }

    /**
     * Perform the splay operation for the given key. Moves the node with
     * the given key to the top of the tree.  If no node has the given
     * key, the last node on the search path is moved to the top of the
     * tree. This is the simplified top-down splaying algorithm from:
     * "Self-adjusting Binary Search Trees" by Sleator and Tarjan
     *
     * @param {number} key Key to splay the tree on.
     * @private
     */
    func splay(_ key: Double) {
        if self.isEmpty() {
            return
        }
        // Create a dummy node.  The use of the dummy node is a bit
        // counter-intuitive: The right child of the dummy node will hold
        // the L tree of the algorithm.  The left child of the dummy node
        // will hold the R tree of the algorithm.  Using a dummy node, left
        // and right will always be nodes and we avoid special cases.
        var dummy:Node?, left:Node?, right:Node?
        dummy = Node(key:0,value:0)
        left = dummy
        right = dummy
        var current = self.root!
        while true {
            if key < current.key {
                if current.left == nil {
                    break
                }
                if key < current.left?.key ?? 0 {
                    // Rotate right.
                    let tmp = current.left!
                    current.left = tmp.right
                    tmp.right = current
                    current = tmp
                    if current.left == nil {
                        break
                    }
                }
                // Link right.
                right?.left = current
                right = current
                current = current.left!
            } else if key > current.key {
                if current.right == nil {
                    break
                }
                if key > current.right?.key ?? 0 {
                    // Rotate left.
                    let tmp = current.right!
                    current.right = tmp.left
                    tmp.left = current
                    current = tmp
                    if current.right == nil {
                        break
                    }
                }
                // Link left.
                left?.right = current
                left = current
                current = current.right!
            } else {
                break
            }
        }
        // Assemble.
        left?.right = current.left
        right?.left = current.right
        current.left = dummy?.right
        current.right = dummy?.left
        self.root = current
    }
}

class Node {
    var key: Double
    var value: Any
    var left: Node?
    var right: Node?

    init(key: Double, value: Any) {
        self.key = key
        self.value = value
    }

    func traverse_(f: (Node) -> Any) {
        var current:Node? = self
        while current != nil {
            if let left = current?.left {
                left.traverse_(f: f)
            }
            _ = f(current!)
            current = current?.right
        }
    }

}
class PayloadTreeLastNode {
        var array: Array<Int>
        var string: String
        init(array: Array<Int>, string: String) {
            self.array = array
            self.string = string
        }
}

class PayloadTreeNode {
    var left: Any
    var right: Any
    init(left: Any, right: Any) {
        self.left = left
        self.right = right
    }
}
class SplayLatency: Runner   {
    
    func generateKey() -> Double {
        // The benchmark framework guarantees that Math.random is
        // deterministic; see base.js.
        return BenchmarkSuite.random();
    }
    func SplayLatency() -> [Double]{
        return splaySamples;
    }
    func splayUpdateStats(_ time: Double) {
        let pause = time - splaySampleTimeStart
        splaySampleTimeStart = time
        splaySamples.append(pause)
    }
    
    func generatePayloadTree(_ depth: Int, _ tag: String) -> Any {
        if depth == 0 {
            return PayloadTreeLastNode(array:[0, 1, 2, 3, 4, 5, 6, 7, 8, 9],string:"String for key" + tag + "in leaf node")
        } else {
            return PayloadTreeNode(left:generatePayloadTree(depth - 1, tag) ,right: generatePayloadTree(depth - 1, tag))
        }
    }

    func insertNewNode() -> Double {
        // Insert new node with a unique key.
        var key: Double?
        repeat {
            key = generateKey()
        } while splayTree?.find(key ?? 0) != nil

        let payload = generatePayloadTree(kSplayTreePayloadDepth, String(key ?? 0))
        splayTree?.insert(key: key ?? 0, value: payload)
        return key ?? 0
    }
    func SplayRun() {
        // Replace a few nodes in the splay tree.
        for _ in 0..<kSplayTreeModifications {
            let key = insertNewNode()
            let greatest = splayTree?.findGreatestLessThan(key)
            if greatest == nil {
                 _ = splayTree?.remove(key: key)
            } else {
                 _ = splayTree?.remove(key: greatest!.key)
            }
        }
        splayUpdateStats(performanceNow())
    }
    func SplaySetup() {
        splayTree = SplayTree()
        splaySampleTimeStart = performanceNow()
        for i in 0..<kSplayTreeSize {
            _ = insertNewNode()
            if (i + 1) % 20 == 19 {
                splayUpdateStats(performanceNow())
            }
        }
    }
    func SplayTearDown() {
        // Allow the garbage collector to reclaim the memory
        // used by the splay tree no matter how we exit the
        // tear down function.
        guard let keys = splayTree?.exportKeys() else { return }
        splayTree = nil
        splaySamples = []
       // Verify that the splay tree has the right size.
        let length = keys.count
        if length != kSplayTreeSize {
            return
        }

        // Verify that the splay tree has sorted, unique keys.
        for i in 0..<length - 1 {
            if keys[i] >= keys[i + 1] {
                return
            }
        }
    }
    
    func notifyError(_ name: String,_ error:String){
    }
    func notifyStep(_ name: String){
    }
   
}

let splayLatency=SplayLatency()

 var splayTree: SplayTree? = nil;

 let benchmark=Benchmark(name: "Splay",doWarmup:true,doDeterministic:false, run: splayLatency.SplayRun, setup: splayLatency.SplaySetup, tearDown: splayLatency.SplayTearDown, latencyResult: splayLatency.SplayLatency, minIterations: 32)
 let benchmarkSuite=BenchmarkSuite(name: "Splay", reference: [81491, 2739514], benchmarks: [benchmark])

func start() {
     BenchmarkSuite.RunSuites(runner: splayLatency)
}
start()

