/** ***** BEGIN LICENSE BLOCK *****
 ** Version: MPL 1.1/GPL 2.0/LGPL 2.1
 *
 ** The contents of this file are subject to the Mozilla Public License Version
 ** 1.1 (the "License"); you may not use this file except in compliance with
 ** the License. You may obtain a copy of the License at
 ** http://www.mozilla.org/MPL/
 *
 ** Software distributed under the License is distributed on an "AS IS" basis,
 ** WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 ** for the specific language governing rights and limitations under the
 ** License.
 *
 ** The Original Code is Mozilla XML-RPC Client component.
 *
 ** The Initial Developer of the Original Code is
 ** Digital Creations 2, Inc.
 ** Portions created by the Initial Developer are Copyright (C) 2000
 ** the Initial Developer. All Rights Reserved.
 *
 ** Contributor(s):
 **   Martijn Pieters <mj@digicool.com> (original author)
 **   Samuel Sieb <samuel@sieb.net>
 *
 ** Alternatively, the contents of this file may be used under the terms of
 ** either the GNU General Public License Version 2 or later (the "GPL"), or
 ** the GNU Lesser General Public License Version 2.1 or later (the "LGPL"),
 ** in which case the provisions of the GPL or the LGPL are applicable instead
 ** of those above. If you wish to allow use of your version of this file only
 ** under the terms of either the GPL or the LGPL, and not to allow others to
 ** use your version of this file under the terms of the MPL, indicate your
 ** decision by deleting the provisions above and replace them with the notice
 ** and other provisions required by the GPL or the LGPL. If you do not delete
 ** the provisions above, a recipient may use your version of this file under
 ** the terms of any one of the MPL, the GPL or the LGPL.
 *
 ** ***** END LICENSE BLOCK ***** */


// From: http://lxr.mozilla.org/mozilla/source/extensions/xml-rpc/src/nsXmlRpcClient.js#956

import Glibc

fileprivate let inDebug = false
func log(_ str: String) {
    if inDebug {
        print(str)
    }
}
func currentTimestamp13() -> Double {
    return Timer().getTime() / 1000.0
}

/* Convert data (an array of integers) to a Base64 string. */
let toBase64Table = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/"
let base64Pad = "="

func toBase64(_ data: String) -> String {
    var resultStr = ""
    let length = data.count
    var i = 0
    
    let charArray = Array(data)
    let toBase64TableArray = Array(toBase64Table)

    // Convert every three bytes to 4 ascii characters.
    while (i < length-2) {
        resultStr +=  String(toBase64TableArray[Int(charArray[i].asciiValue!) >> 2])
        resultStr += String(toBase64TableArray[((Int(charArray[i].asciiValue!) & 0x03) << 4) + (Int(charArray[i+1].asciiValue!) >> 4)])
        resultStr += String(toBase64TableArray[((Int(charArray[i+1].asciiValue!) & 0x0f) << 2) + (Int(charArray[i+2].asciiValue!) >> 6)])
        resultStr += String(toBase64TableArray[Int(charArray[i+2].asciiValue!) & 0x3f])
        
        i += 3
    }
    
    // Convert the remaining 1 or 2 bytes, pad out to 4 characters.
    if ((length % 3) > 0) {
    	i = length - (length % 3)
        resultStr += String(toBase64TableArray[Int(charArray[i].asciiValue!) >> 2])
        if((length % 3) == 2) {
            resultStr += String(toBase64TableArray[(Int(charArray[i].asciiValue! & 0x03) << 4) + (Int(charArray[i+1].asciiValue!) >> 4)])
            resultStr += String(toBase64TableArray[(Int(charArray[i+1].asciiValue!) & 0x0f) << 2])
            resultStr += base64Pad
        } else {
            resultStr += String(toBase64TableArray[(Int(charArray[i].asciiValue!) & 0x03) << 4])
            resultStr += base64Pad + base64Pad
        }
    }
    return resultStr
}

/* Convert Base64 data to a string */
let toBinaryTable: [Int] = [
    -1,-1,-1,-1, -1,-1,-1,-1, -1,-1,-1,-1, -1,-1,-1,-1,  //binary1
    -1,-1,-1,-1, -1,-1,-1,-1, -1,-1,-1,-1, -1,-1,-1,-1,
    -1,-1,-1,-1, -1,-1,-1,-1, -1,-1,-1,62, -1,-1,-1,63, //binary2
    52,53,54,55, 56,57,58,59, 60,61,-1,-1, -1, 0,-1,-1,
    -1, 0, 1, 2,  3, 4, 5, 6,  7, 8, 9,10, 11,12,13,14, //binary3
    15,16,17,18, 19,20,21,22, 23,24,25,-1, -1,-1,-1,-1,
    -1,26,27,28, 29,30,31,32, 33,34,35,36, 37,38,39,40, //binary4
    41,42,43,44, 45,46,47,48, 49,50,51,-1, -1,-1,-1,-1
]

func base64ToString(_ data : String) -> String {
    var result = ""
    var leftbits = 0 // number of bits decoded, but yet to be appended
    var leftdata = 0 // bits decoded, but yet to be appended
    
    let charArray = Array(data)
    // Convert one by one.
    for i in 0..<data.count {
        let c = toBinaryTable[Int(charArray[i].asciiValue!) & 0x7f]
        let padding = (Int(charArray[i].asciiValue!) == Character(base64Pad).asciiValue!)
        // Skip illegal characters and whitespace
        if (c == -1) { continue }
        
        // Collect data into leftdata, update bitcount
        leftdata = (leftdata << 6) | c
        leftbits += 6
        
        // If we have 8 or more bits, append 8 bits to the result
        if (leftbits >= 8) {
            leftbits -= 8
            // Append if not padding.
            if (!padding) {
                result += String(Character(UnicodeScalar(((leftdata >> leftbits) & 0xff))!))
            }
            leftdata &= (1 << leftbits) - 1
        }
    }
    
    // If there are any bits left, the base64 string was corrupted
    if (leftbits > 0) {
        fatalError("Corrupted base64 string")
    }
    return result
}

func run() {
        
    var str = ""
    
    for _ in 0..<8192 {
        str += String(Character(UnicodeScalar(( Int(25 * Double.random(in: 0..<1)) + 97 ))!))
    }

    var i = 8192
    while (i < 16384) {
        var base64: String
        base64 = toBase64(str)        
        //log("string-base64: toBase64 \(i) = \(base64)")
        let encoded = base64ToString(base64)
        //log("string-base64: toBase64 \(i) = \(encoded)")
        if (encoded != str) {
            fatalError("bad result: expected \(str) but got \(encoded)")
        }
        // Double the string
        str += str
        i *= 2
    }
}

/*
 * @State
 * @Tags Jetstream2
 */
class Benchmark {
    /*
     *@Benchmark
     */
    func runIteration() {
        for i in 0..<8 {
            let startTimeInLoop = currentTimestamp13()
            run()
            let endTimeInLoop = currentTimestamp13()
            //log("base64: index= \(i) time= \(endTimeInLoop - startTimeInLoop)")
        }
    }
}

let startTime = currentTimestamp13()
let benchmark = Benchmark()
for _ in 0..<20 {
    benchmark.runIteration()
}
let endTime = currentTimestamp13()
print("base64: ms = \(endTime - startTime)")

class Timer {
   private let CLOCK_REALTIME = 0
   private var time_spec = timespec()

   func getTime() -> Double {
       clock_gettime(Int32(CLOCK_REALTIME),&time_spec)
       return Double(time_spec.tv_sec * 1_000_000 + time_spec.tv_nsec / 1_000)
   }
}