import Glibc

func getWaypoints() -> [String:[String:Any]] {
    var filepath=""
    var current_directory:[CChar] = Array(repeating: 0, count: Int(FILENAME_MAX))
    let rs=getcwd(&current_directory, Int(FILENAME_MAX))
    if rs != nil {
        let a = current_directory.compactMap { $0 == 0 ? nil:Character(UnicodeScalar(UInt32($0))!)}
        filepath = String(a) + "/waypoints-json.json"
    }
    if let file = fopen(filepath, "r") {
        var file_stat = stat()
        stat(filepath,&file_stat)
        var buffer: [UInt8] = Array(repeating: 0, count: Int(file_stat.st_size))
        let bytes_read=fread(&buffer, 1, Int(file_stat.st_size), file)
        if bytes_read > 0 {
            let chars = buffer.compactMap  { c in
                var char = Character(UnicodeScalar(c))
                if char == "\n" {
                    char = Character(" ")
                }
                return char
            }
            
            let content=String(chars)
            do {
                if let result2 = try JsonParser.parse(text: content) {
                    let r=result2 as? [String:[String:Any]] ?? [:]
                    return r
                } else {
                    return [:]
                }
            } catch {
                return [:]
            }
        }
        fclose(file)
    }
    return [:]
}

var _faaWaypoints:[String:[String:Any]] = getWaypoints();

