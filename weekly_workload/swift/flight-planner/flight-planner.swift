import Glibc
let TwoPI = Double.pi * 2
let earthRadius = 3440.0 // In nautical miles.
let degreeCharacter: String = "\u{00b0}"
var regExpOptionalUnicodeFlag: String = ""
func debugLog(_ str: String) {
//   print("debug: "+str)
}
func keywords(_ text:String) -> String
{
    if (useUnicode) {
        regExpOptionalUnicodeFlag = "u";
        let str = UnicodeStrings.get(text)
        return str.isEmpty == true ? "" : str
    } else {
        regExpOptionalUnicodeFlag = "";
        return text
    }
}

func status(_ text: String) {
//    debugLog("Status: \(text)")
}

func error(_ text: String) {
//    debugLog("Error: \(text)")
}
class NumberUtils {
    static func toRadians(_ num:Double) -> Double {
        return num * Double.pi / 180
    }
    
    static func toDegrees(_ num:Double) -> Double {
        return num * 180 / Double.pi
    }
}

func distanceFromSpeedAndTime(_ speed: Double, _ time: Time?) -> Double {
    if time != nil {
        return speed * Double(time!.hours())
    }else{
        return 0
    }
}


let LatRE = try? Regex("^([NS\\-])?(90|[0-8]?\\d)(?:( [0-5]?\\d\\.\\d{0,3})'?|(\\.\\d{0,6})|( ([0-5]?\\d)\" ?([0-5]?\\d)'?))?").ignoresCase();

func decimalLatitudeFromString(_ latitudeString: String) -> Double {
    guard let match=latitudeString.match(LatRE) else {
        return 0
    }
    var result = 0.00
    var sign = 1
    
    if match[1] != nil && (match[1]!.uppercased() == "S" || match[1]! == "-") {
        sign = -1
    }
    
    result = Double(match[2] ?? "0") ?? 0
    
    if result != 90 {
        if(match[3] != nil){
            let minutes = Double(match[3]!) ?? 0
    
            result = result + minutes / 60
            
        }else if(match[4] != nil){
            let decimalDegrees = Double(match[4]!) ?? 0
            result = result + decimalDegrees
            
        }else if(match[5] != nil){
            let degrees = Double(match[6] ?? "0") ?? 0
            let minutes = Double(match[7] ?? "0") ?? 0
            result = result + (degrees + minutes/60)/60
            
        }
        
    }
    return result * Double(sign)
}

let LongRE = try? Regex("^([EW\\-]?)(180|(?:1[0-7]|\\d)?\\d)(?:( [0-5]?\\d\\.\\d{0,3})|(\\.\\d{0,6})|( ([0-5]?\\d)\" ?([0-5]?\\d)'?)?)").ignoresCase();

func decimalLongitudeFromString(_ longitudeString: String) -> Double {
    guard let match=longitudeString.match(LongRE) else {
        return 0
    }
    
    var result = 0.0
    var sign = 1
    
    if match[1] != nil && (match[1]!.uppercased() == "W" || match[1]! == "-") {
        sign = -1
    }
    
    result = Double(match[2] ?? "0") ?? 0
    
    if result != 180 {
        if(match[3] != nil){
            let minutes = Double(match[3]!) ?? 0
            result = result + minutes / 60
            
        }else if(match[4] != nil){
            let decimalDegrees = Double(match[4]!) ?? 0
            result = result + decimalDegrees
            
        }else if(match[5] != nil){
            let degrees = Double(match[6] ?? "0") ?? 0
            let minutes = Double(match[7] ?? "0") ?? 0
            result = result + (degrees + minutes/60)/60
            
        }
    }
    
    return result * Double(sign)
}

let TimeRE = try? Regex("^([0-9][0-9]?)(?:\\:([0-5][0-9]))?(?:\\:([0-5][0-9]))?$");

class Time {
    
    var _seconds: Int = 0
    
    init(_ time: Any) {

        if time is String{
            let timeStr = time as! String
            guard let match = timeStr.match(TimeRE) else {
                self._seconds = 0
                return
            }
            if match[3] != nil{
                let hour = Int(match[1] ?? "") ?? 0
                let minute = Int(match[2] ?? "") ?? 0
                let second = Int(match[3]!) ?? 0
                _seconds = (hour * 60 + minute) * 60 + second
            }else if match[2] != nil{
                let minute = Int(match[1] ?? "") ?? 0
                let second = Int(match[2]!) ?? 0
                _seconds = minute * 60 + second
            }else if match[1] != nil{
                _seconds = Int(match[1]!) ?? 0
            }else{
                _seconds = 0
            }
            return
        }
        if(time is Double){
            _seconds = Int(round(time as! Double))
            return
        }
        _seconds = 0
    }
    
    func add(_ otherTime: Time) -> Time {
        return Time(Double(_seconds + otherTime._seconds))
    }
    
    static func differenceBetween(_ time2: Any, _ time1: Any) -> Time {
        let seconds1:Double
        let seconds2:Double
        if time1 is Time{
            seconds1 = Double((time1 as! Time)._seconds)
        }else{
            seconds1 = round((time1 as! Double)/1000)
        }
        if time2 is Time{
            seconds2 = Double((time2 as! Time)._seconds)
        }else{
            seconds2 = round((time2 as! Double)/1000)
        }
        return Time(Double(seconds2 - seconds1))
    }
    
    func seconds() -> Int {
        return _seconds
    }
    
    func minutes() -> Int {
        return _seconds / 60
    }
    
    func hours() -> Int {
        return _seconds / 3600
    }
    
    func toString() -> String {
        var result = ""
        var seconds = _seconds % 60
        if seconds < 0 {
            result = "-"
            seconds = -seconds
        }
        var minutes = _seconds / 60
        let hours = minutes / 60
        minutes = minutes % 60
        
        if hours > 0 {
            result = result + "\(hours)" + ":"
        }
        if minutes < 10 && hours > 0 {
            result = result + "0"
        }
        result = result + "\(minutes)" + ":"
        if seconds < 10 {
            result = result + "0"
        }
        result = result + "\(seconds)"
        
        return result
    }
}

class GeoLocation {
    var latitude: Double
    var longitude: Double
    
    init(_ latitude: Double, _ longitude: Double) {
        self.latitude = latitude
        self.longitude = longitude
    }
    
    func latitudeString() -> String {
        var latitude = self.latitude;
        let latitudeDegrees = floor(latitude)
        let latitudeMinutes = Double(((latitude - latitudeDegrees) * Double(60)).toFixed(3)) ?? 0
        let latitudeMinutesFiller = latitudeMinutes < 10 ? " " : ""
        var latitudePrefix = "N";
        if (latitude < 0) {
          latitude = -latitude;
          latitudePrefix = "S";
        }
        return "\(latitudePrefix)\(latitudeDegrees)\(degreeCharacter)\(latitudeMinutesFiller)\(latitudeMinutes)\'"
    }
    
    func longitudeString() -> String {
        var longitude = self.longitude;
        var longitudePrefix =  "E"
        let longitudeDegrees = floor(longitude)
        let longitudeMinutes = Double(((longitude - longitudeDegrees) * Double(60)).toFixed(3)) ?? 0
        let longitudeMinutesFiller = longitudeMinutes < 10 ? " " : ""
        if (longitude < 0) {
          longitude = -longitude;
          longitudePrefix = "W";
        }
        return "\(longitudePrefix)\(longitudeDegrees)\(degreeCharacter)\(longitudeMinutesFiller)\(longitudeMinutes)\'"
    }
    
    func distanceTo(_ otherLocation: GeoLocation) -> Double {
        let dLat = NumberUtils.toRadians(otherLocation.latitude - self.latitude)
        let dLon = NumberUtils.toRadians(otherLocation.longitude - self.longitude)
        let a = sin(dLat/2) * sin(dLat/2) +
        cos(NumberUtils.toRadians(self.latitude)) * cos(NumberUtils.toRadians(otherLocation.latitude)) *
        sin(dLon/2) * sin(dLon/2)
        let c = 2 * atan2(sqrt(a), sqrt(1 - a))
        return earthRadius * c
    }
    
    func bearingFrom(_ otherLocation: GeoLocation, _ magneticVariation: Double = 0) -> Double {

        let dLon = NumberUtils.toRadians(self.longitude - otherLocation.longitude)
        let thisLatitudeRadians = NumberUtils.toRadians(self.latitude)
        let otherLatitudeRadians = NumberUtils.toRadians(otherLocation.latitude)
        let y = sin(dLon) * cos(NumberUtils.toRadians(self.latitude))
        let x = cos(otherLatitudeRadians) * sin(thisLatitudeRadians) -
        sin(otherLatitudeRadians) * cos(thisLatitudeRadians) * cos(dLon)
        return Double(Int(NumberUtils.toDegrees(atan2(y, x) + Double(720) + magneticVariation))%360)
    }
    
    func bearingTo(_ otherLocation: GeoLocation, _ magneticVariation: Double = 0) -> Double {
        
        
        let dLon = NumberUtils.toRadians(otherLocation.longitude - self.longitude)
        let thisLatitudeRadians = NumberUtils.toRadians(self.latitude)
        let otherLatitudeRadians = NumberUtils.toRadians(otherLocation.latitude)
        let y = sin(dLon) * cos(NumberUtils.toRadians(otherLocation.latitude))
        let x = cos(thisLatitudeRadians) * sin(otherLatitudeRadians) -
        sin(thisLatitudeRadians) * cos(otherLatitudeRadians) * cos(dLon)
        return Double(Int(NumberUtils.toDegrees(atan2(y, x) + Double(720) + magneticVariation))%360)
    }
    
    func locationFrom(_ bearing: Double, _ distance: Double, _ magneticVariation: Double = 0) -> GeoLocation {
        
        
        let bearingRadians = NumberUtils.toRadians(bearing - magneticVariation)
        let thisLatitudeRadians = NumberUtils.toRadians(self.latitude)
        let angularDistance = distance / earthRadius
        let latitudeRadians = asin(sin(thisLatitudeRadians) * cos(angularDistance) +
                                   cos(thisLatitudeRadians) * sin(angularDistance) * cos(bearingRadians))
        let longitudeRadians = NumberUtils.toRadians(self.longitude) +
        atan2(sin(bearingRadians) * sin(angularDistance) * cos(thisLatitudeRadians),
              cos(angularDistance) - sin(thisLatitudeRadians) * sin(latitudeRadians))
        
        return GeoLocation(NumberUtils.toDegrees(latitudeRadians),  NumberUtils.toDegrees(longitudeRadians))
    }
    
    func toString() -> String {
        return "\(latitudeString()), \(longitudeString())"
    }
}

class FaaWaypoints {
    var waypoints: [String:[String: Any]] = [:]
    init() {
    }
    func find(_ waypoint: String) -> [String:Any]? {
        return self.waypoints[waypoint]
    }
}
let faaWaypoints = FaaWaypoints()

class FaaAirways {
    var airways: [String : [String : Any]] = [:]

    init() {
        airways = _faaAirways
    }
    
    func isAirway(_ identifier: String) -> Bool {
        return airways[identifier] != nil
    }
    
    func resolveAirway(_ airwayID: String, _ entryPoint: String, _ exitPoint: String) -> [String] {
        
        guard let airway = airways[airwayID] else { return [] }
        let fixes:[String] = airway["fixes"] as? [String] ?? []
        let entryIndex = fixes.firstIndex(of: entryPoint)
        let exitIndex = fixes.firstIndex(of: exitPoint)
        if(entryIndex == nil || exitIndex == nil){
            return []
        }
        let strid = (entryIndex! <= exitIndex!) ? 1 : -1
        
        var route: [String] = []
        for idx in stride(from: entryIndex!, to: exitIndex!, by: strid) {
            route.append(fixes[idx])
        }
        route.append(fixes[exitIndex!])
        return route
    }
}
let faaAirways = FaaAirways()


class UserWaypoints {
    var waypoints: [String: [String: Any]] = [:]
    
    init() {
        waypoints = [:]
    }
    
    func clear() {
        waypoints = [:]
    }
    
    func find(_ waypoint: String) -> [String: Any]? {
        return waypoints[waypoint]
    }
    
    func update(_ name: String, _ description: String, _ latitude: Any, _ longitude: Any) {
       let decimalLatitude = (latitude is String) ? decimalLatitudeFromString(latitude as! String) : latitude as? Double ?? 0
       let decimalLongitude = (longitude is String) ? decimalLongitudeFromString(longitude as! String) : longitude as? Double ?? 0
        
        waypoints[name.uppercased()] = [
            "name": name,
            "description": description,
            "latitude": decimalLatitude,
            "longitude": decimalLongitude
        ]
    }
}

let userWaypoints =  UserWaypoints()

class EngineConfig {

    var type: String
    private var _fuelFlow: Double
    private var _trueAirspeed: Double
    static var allConfigs: [EngineConfig] = []
    static var allConfigsByType: [String: EngineConfig] = [:]
    static var Taxi:Int = 0
    static var Runup:Int = 1
    static var Takeoff:Int = 2
    static var Climb:Int = 3
    static var Cruise:Int = 4
    static var Pattern:Int = 5
    init(_ type: String?, _ fuelFlow: Double?, _ trueAirspeed: Double?) {
        self.type = type!
        self._fuelFlow = fuelFlow!
        self._trueAirspeed = trueAirspeed!
    }
    
    func trueAirspeed() -> Double {
        return self._trueAirspeed
    }
    
    func fuelFlow() -> Double {
        return self._fuelFlow
    }
    
    static func appendConfig(_ type: String, _ fuelFlow: Double, _ trueAirspeed: Double) {
        if EngineConfig.allConfigsByType[type] != nil {
            error("Duplicate Engine configuration: \(type)")
            return
        }
        
        let newConfig = EngineConfig( type, fuelFlow, trueAirspeed)
        EngineConfig.allConfigs.append(newConfig)
        EngineConfig.allConfigsByType[type] = newConfig
    }
    
    static func getConfig(_ n: Int) -> EngineConfig? {
        if n >= EngineConfig.allConfigs.count {
            return nil
        }
        
        return EngineConfig.allConfigs[n]
    }
}

class Waypoint {
    var name: String
    var type: String
    var description: String
    var latitude: Double
    var longitude: Double
    
    init(_ name: String, _ type: String, _ description: String, _ latitude: Double, _ longitude: Double) {
        self.name = name
        self.type = type
        self.description = description
        self.latitude = latitude
        self.longitude = longitude
    }
}


class Leg {
    var previous: Leg?
    var next: Leg?
    var fix: String
    var location: GeoLocation
    var originalLocation:GeoLocation?
    var course: Double
    var distance: Double
    var trueAirspeed: Double
    var windDirection: Int
    var windSpeed: Double
    var heading: Double
    var estGS: Double
    var startFlightTiming: Bool
    var stopFlightTiming: Bool
    var engineConfig: Int
    var fuelFlow: Double
    var distanceRemaining: Double
    var estimatedTimeEnroute: Time
    var estTimeRemaining: Time
    var estFuel: Double
    var type: String
    var extraTurns: Int = 0
    var climbTime:Time?
    var index: Int
    init(_ fix: String, _ location: GeoLocation?) {
        self.previous = nil
        self.next = nil
        self.fix = fix
        self.location = (location != nil) ? location! : GeoLocation(0, 0)
        self.course = 0
        self.distance = 0
        self.trueAirspeed = 0
        self.windDirection = 0
        self.windSpeed = 0
        self.heading = 0
        self.estGS = 0
        self.startFlightTiming = false
        self.stopFlightTiming = false
        self.engineConfig = EngineConfig.Cruise
        self.fuelFlow = 0
        self.distanceRemaining = 0
        self.estimatedTimeEnroute = Time(0)
        self.estTimeRemaining = Time(0)
        self.estFuel = 0
        self.originalLocation = nil
        self.type = ""
        self.extraTurns = 0
        self.climbTime = Time(0)
        self.index = 0
    }
    
    func fixName() -> String {
        return self.fix
    }
    
    func toString() -> String {
        return self.fix
    }
    
    func setPrevious(_ leg: Leg?) {
        self.previous = leg
    }
    
    func previousLeg() -> Leg? {
        return self.previous
    }
    
    func setNext(_ leg: Leg?) {
        self.next = leg
    }
    
    func nextLeg() -> Leg? {
        return self.next
    }
    
    func setWind(_ windDirection: Int, _ windSpeed: Double) {
        self.windDirection = windDirection
        self.windSpeed = windSpeed
    }
    
    func isSameWind(_ windDirection: Int, _ windSpeed: Double) -> Bool {
        return self.windDirection == windDirection && self.windSpeed == windSpeed
    }
    
    func windToString() -> String {
        if self.windSpeed == 0 {
            return ""
        }
        return "\(self.windDirection == 0 ? 360 : self.windDirection)@\(self.windSpeed)"
    }
    
    func setTrueAirspeed(_ trueAirspeed: Double) {
        self.trueAirspeed = trueAirspeed
    }
    
    func isStandardTrueAirspeed() -> Bool {
        let engineConfig = EngineConfig.getConfig(Int(self.engineConfig))
        if(engineConfig != nil){
            return (self.trueAirspeed == engineConfig!.trueAirspeed())
        }else{
            return false
        }
        
    }
    
    func trueAirspeedToString() -> String {
        return "\(self.trueAirspeed) kts"
    }
    func updateDistanceAndBearing(_ other: GeoLocation) {
        self.distance = self.location.distanceTo( other)
        self.course = round(self.location.bearingFrom( other))
        if self.estGS != 0 {
            let estimatedTimeEnrouteInSeconds = Int(round(self.distance * 3600 / self.estGS))
            self.estimatedTimeEnroute = Time(estimatedTimeEnrouteInSeconds)
        }
        
        if estimatedTimeEnroute.seconds() > 0 {
            self.estFuel = self.fuelFlow * Double(estimatedTimeEnroute.hours())
        }
    }
    func propagateWind() {
        var windDirection = self.windDirection
        let windSpeed = self.windSpeed
        
        windDirection = (windDirection + 360) % 360
        if windDirection == 0 {
            windDirection = 360
        }
        
        var currLeg:Leg? = self
        while currLeg != nil {
            currLeg?.windDirection = windDirection
            currLeg?.windSpeed = windSpeed
            if currLeg?.stopFlightTiming == true {
                break
            }
            currLeg = currLeg?.nextLeg()
        }
    }
    func updateForWind() {
        if self.windSpeed == 0 || self.trueAirspeed == 0 {
            self.heading = self.course
            self.estGS = self.trueAirspeed
            return
        }
        
        let windDirectionRadians = NumberUtils.toRadians(Double(self.windDirection))
        let courseRadians = NumberUtils.toRadians(self.course)
        let swc = (self.windSpeed / self.trueAirspeed) * sin(windDirectionRadians - courseRadians)
        
        if abs(swc) > 1 {
            status( "Wind to strong to fly!")
            return
        }
        
        var headingRadians = courseRadians + asin(swc)
        if headingRadians < 0 {
            headingRadians += TwoPI
        }
        if headingRadians > TwoPI {
            headingRadians -= TwoPI
        }
        let groundSpeed = self.trueAirspeed * sqrt(1 - swc * swc) - self.windSpeed * cos(windDirectionRadians - courseRadians)
        if groundSpeed < 0 {
            status( "Wind to strong to fly!")
            return
        }
        
        self.estGS = groundSpeed
        self.heading = round(NumberUtils.toDegrees(headingRadians))
    }
    
    func calculate() {
        let engineConfig = EngineConfig.getConfig(Int(self.engineConfig))
        
        if self.trueAirspeed == 0 {
            trueAirspeed = engineConfig?.trueAirspeed() ?? 0
        }
        self.fuelFlow = engineConfig?.fuelFlow() ?? 0
        
        self.updateForWind()
    }
    func updateForward() {
        var _previousLeg = self.previousLeg()
        
        var havePrevious = true
        if _previousLeg == nil {
            havePrevious = false
            _previousLeg = self
        }
        let previousLeg = _previousLeg!
        let thisLegType = self.type
        if thisLegType == "Climb" && havePrevious {
            self.location = previousLeg.location
        } else {
            self.updateDistanceAndBearing(previousLeg.location)
            self.updateForWind()
            let nextLeg = self.nextLeg()
            let previousLegType = previousLeg.type 
            if havePrevious {
                if previousLegType == "Climb" {
                    
                    let climbDistance = distanceFromSpeedAndTime(previousLeg.estGS, previousLeg.climbTime)
                    if climbDistance < self.distance {
                        let climbStartLocation = previousLeg.location
                        let climbEndLocation = climbStartLocation.locationFrom(self.course, climbDistance)
                        previousLeg.location = climbEndLocation
                        previousLeg.updateDistanceAndBearing(climbStartLocation)
                        self.estimatedTimeEnroute = Time(0)
                        self.updateDistanceAndBearing(climbEndLocation)
                    } else {
                        status( "Not enough distance to climb in leg #\(previousLeg.index)")
                    }
                } else if (thisLegType == "Left" || thisLegType == "Right") && nextLeg != nil && nextLeg?.location != nil {
                    let standardRateCircumference = self.trueAirspeed / 30
                    let standardRateRadius = standardRateCircumference / TwoPI
                    var offsetInboundBearing = 360 + previousLeg.course + (thisLegType == "Left" ? -90 : 90)
                    offsetInboundBearing = round(Double(Int(offsetInboundBearing + 360) % 360))
                    if previousLeg.originalLocation == nil {
                        previousLeg.originalLocation = previousLeg.location
                    }
                    let previousLocation = previousLeg.originalLocation
                    let inboundLocation = previousLocation!.locationFrom(offsetInboundBearing, standardRateRadius)
                    let bearingToNext = round(nextLeg!.location.bearingFrom( previousLocation!))
                    var offsetOutboundBearing = bearingToNext + (thisLegType == "Left" ? 90 : -90)
                    offsetOutboundBearing = Double((Int(offsetOutboundBearing + 360) % 360))
                    let outboundLocation = previousLocation!.locationFrom(offsetOutboundBearing, standardRateRadius)
                    var turnAngle = thisLegType == "Left" ? (360 + bearingToNext - previousLeg.course) : (360 + previousLeg.course - bearingToNext)
                    turnAngle = Double(Int(turnAngle + 360) % 360)
                    self.estimatedTimeEnroute = Time(Int(round((turnAngle + Double(360 * extraTurns)) / 3)))
                    self.estFuel = self.fuelFlow * Double(self.estimatedTimeEnroute.hours())
                    self.location = outboundLocation
                    self.distance = distanceFromSpeedAndTime(self.trueAirspeed, self.estimatedTimeEnroute)
                    previousLeg.location = inboundLocation
                    let prevPrevLeg = previousLeg.previousLeg()
                    if prevPrevLeg != nil {
                        previousLeg.estimatedTimeEnroute = Time(0)
                        previousLeg.updateDistanceAndBearing(prevPrevLeg!.location)
                    }
                }
            }
        }
    }
    
    func updateBackward() {
        let nextLeg = self.nextLeg()
        var distanceRemaining: Double
        var timeRemaining:Time
        
        if nextLeg != nil {
            distanceRemaining = nextLeg!.distanceRemaining
            timeRemaining = nextLeg!.estTimeRemaining as Time
        }else{
            distanceRemaining = 0
            timeRemaining = Time(0)
        }
        if self.stopFlightTiming || timeRemaining.seconds() > 0 {
            self.distanceRemaining = distanceRemaining + self.distance
            self.estTimeRemaining = timeRemaining.add(self.estimatedTimeEnroute)
        }else {
            self.estTimeRemaining = Time(0)
        }
        
    }
    
}
let RallyLegWithFixRE = try? Regex("^([0-9a-zA-Z\\.]{3,16})\\|(" + keywords("START") + "|" + keywords("TIMING") + ")").ignoresCase();

class RallyLeg: Leg {
   static var startLocation:GeoLocation?
   static var startFix:String = ""
   static var totalTaxiTime:Time?
   static var taxiSegments:Array<Any> = []
    
    init(_ type: String,_ fix: String?, _ location: GeoLocation?,_ engineConfig:Int?) {
        super.init(fix!, location)
        self.type = type
        self.engineConfig = engineConfig ?? 0
    }
    
    override func fixName() -> String {
        return self.type
    }
    
    override func toString() -> String {
        return self.fixName()
    }
    
   static func reset() {
        RallyLeg.startLocation = nil
        RallyLeg.startFix = ""
        RallyLeg.totalTaxiTime = Time(0)
        RallyLeg.taxiSegments = []
    }
    
    static func fixNeeded(_ fix: String) -> String {
        guard let match = fix.match(RallyLegWithFixRE) else {
            return ""
        }
        return String(match[1] ?? "")
    }
    
    static func getLegWithFix(_ waypointText: String, _ fix: String, _ location: GeoLocation) -> Leg? {
        guard let match = waypointText.match(RallyLegWithFixRE) else {
            return nil
        }
      
        let legType = String(match[2] ?? "").uppercased()
        if legType == keywords("START") {
            if RallyLeg.startLocation != nil {
                status( "Trying to create second start leg")
                return nil
            }
            
            RallyLeg.startLocation = location
            RallyLeg.startFix = fix
            RallyLeg.totalTaxiTime = Time(0)
            RallyLeg.taxiSegments = []
            
            return StartLeg( waypointText, fix, location)
        }
        
        if legType == keywords("TIMING") {
            return TimingLeg( waypointText,  fix,  location)
        }
        
        error( "Unhandled Rally Leg type \(legType)")
        return nil
    }
}

class StartLeg: RallyLeg {
    init(_ fixText: String, _ fix: String, _ location: GeoLocation) {
        super.init("Start", fix, location, EngineConfig.Taxi)
    }
    
    override func fixName() -> String {
        return "\(fix)|Start"
    }
}
class TimingLeg: RallyLeg {
   
    
    init(_ fixText: String, _ fix: String, _ location: GeoLocation) {
    
        super.init("Timing", fix, location, EngineConfig.Cruise)
        self.stopFlightTiming=true
    }
    
    override func fixName() -> String {
        return "\(fix)|Timing"
    }
}
let RallyLegNoFixRE = try? Regex(keywords("TAXI") + "|" + keywords("RUNUP") + "|" + keywords("TAKEOFF") + "|" + keywords("CLIMB") + "|" + keywords("PATTERN") + "|" + keywords("LEFT") + "|" + keywords("RIGHT")).ignoresCase();

class RallyLegWithoutFix: RallyLeg {
    
    init(_ type: String, _ CommentsAsFix: String, _ location: GeoLocation?, _ engineConfig: Int?) {
        super.init(type, CommentsAsFix, location, engineConfig)
       
    }
    
    override func setPrevious(_ previous: Leg?) {
        if self.setLocationFromPrevious() && previous != nil {
            self.location = previous!.location
        }
        super.setPrevious(previous!)
    }
    
    func setLocationFromPrevious() -> Bool {
        return false
    }
    
   static func isRallyLegWithoutFix(_ fix: String) -> Bool {
        let barPosition = fix.firstIndex(of: "|")
        let firstPart = barPosition == nil ? fix : String(fix[..<barPosition!])
       return (firstPart.firstMatch(of: RallyLegNoFixRE!)?.range.isEmpty) ?? false
    }
    
  static func getLegNoFix(_ waypointText: String) -> Leg? {
        let barPosition = waypointText.firstIndex(of: "|")
        var firstPart = barPosition == nil ? waypointText : String(waypointText[..<barPosition!])
        firstPart = firstPart.uppercased()
      guard let match = firstPart.match(RallyLegNoFixRE) else {
          return nil
      }
      let legType:String = String(match[0] ?? "")
        if(legType == keywords("TAXI")){
            return TaxiLeg(waypointText)
        }
        if(legType == keywords("RUNUP")){
            return RunupLeg(waypointText)
        }
        if(legType == keywords("TAKEOFF")){
            if RallyLegWithoutFix.startLocation == nil {
                status( "Trying to create a Takeoff leg without start leg")
                return nil
            }
            return TakeoffLeg( waypointText)
        }
        if(legType == keywords("CLIMB")){
            return ClimbLeg(waypointText)
        }
        if(legType == keywords("PATTERN")){
            return PatternLeg( waypointText)
        }
        if(legType == keywords("LEFT") || legType == keywords("RIGHT") ){
            return TurnLeg( waypointText, legType == keywords("RIGHT"))
        }
        error( "Unhandled Rally Leg type \(legType)")
        return nil
    }
}

let TaxiLegRE = try? Regex("^" + keywords("TAXI") + "(?:\\|([0-9][0-9]?(?:\\:[0-5][0-9])?))?$").ignoresCase();

class TaxiLeg: RallyLegWithoutFix {
    init(_ fixText: String) {
        let match = fixText.match(TaxiLegRE)
        super.init("Taxi", "", GeoLocation( -1,  -1), EngineConfig.Taxi)
        var taxiTimeString = "5:00"
        if(match != nil && match![1] != nil){
            taxiTimeString =  String(match![1]!)
        }
        self.estimatedTimeEnroute = Time(taxiTimeString)
    }
    
    override func setLocationFromPrevious() -> Bool {
        return true
    }
    
    override func fixName() -> String {
        return "Taxi|\(estimatedTimeEnroute.toString())"
    }
}

let RunupLegRE = try? Regex("^" + keywords("RUNUP") + "(?:\\|([0-9][0-9]?(?:\\:[0-5][0-9])?))?$").ignoresCase();

class RunupLeg: RallyLegWithoutFix{

    init(_ fixText: String) {
        let match = fixText.match(RunupLegRE)
        super.init("Runup", "", GeoLocation( -1,  -1), EngineConfig.Runup)
        
        var runupTimeString = "30";
        if(match != nil && match![1] != nil){
            runupTimeString = String(match![1]!)
        }
        self.estimatedTimeEnroute = Time(runupTimeString)
        
    

    }
    
    override func setLocationFromPrevious() -> Bool {
        return true
    }
    
    override func fixName() -> String {
        return "Runup|\(estimatedTimeEnroute.toString())"
    }
}
let TakeoffLegRE = try? Regex("^" + keywords("TAKEOFF") + "(?:\\|([0-9][0-9]?(?:\\:[0-5][0-9])?))?(?:\\|([0-9]{1,2}|[0-2][0-9][0-9]|3[0-5][0-9]|360)(?:@)(\\d{1,2}(?:\\.\\d{1,4})?))?$").ignoresCase();

class TakeoffLeg: RallyLegWithoutFix {
    var bearingFromStart: Double = 0
    var distanceFromStart: Double = 0.0
    init(_ fixText: String) {
        let match = fixText.match(TakeoffLegRE)

        var bearingFromStart = 0.0;
        var distanceFromStart = 0.0;
        var takeoffEndLocation = RallyLeg.startLocation
        if match != nil && match![2] != nil && match![3] != nil {
            bearingFromStart = Double((Int(match![2]!)!) % 360)
            distanceFromStart = Double(match![3]!) ?? 0
            takeoffEndLocation = RallyLeg.startLocation?.locationFrom(bearingFromStart, distanceFromStart)
        }
        
        super.init("Takeoff", "", takeoffEndLocation!, EngineConfig.Takeoff)
        self.bearingFromStart = bearingFromStart;
        self.distanceFromStart = distanceFromStart;
        var takeoffTimeString = "2:00"
        if(match != nil && match![1] != nil){
            takeoffTimeString =  String(match![1] ?? "")
        }
        estimatedTimeEnroute = Time(takeoffTimeString)
        self.startFlightTiming = true
    }
    
    override func fixName() -> String {
        var result = "Takeoff"
        
        if estimatedTimeEnroute.seconds() != 120 {
            result += "|" + estimatedTimeEnroute.toString()
        }
        if distanceFromStart > 0 {
            result += "|" + String(bearingFromStart) + "@" + "\(distanceFromStart)"
        }
        
        return result
    }
}

let ClimbLegRE = try? Regex("^" + keywords("CLIMB") + "(?:\\|)(\\d{3,5})(?:\\|([0-9][0-9]?(?:\\:[0-5][0-9])?))$").ignoresCase();

class ClimbLeg: RallyLegWithoutFix {
    var altitude: Int!
    init(_ fixText: String) {
        let match = fixText.match(ClimbLegRE)
        var altitude = 5500
        if(match != nil && match![1] != nil){
            altitude = Int(match![1]!) ?? 0
        }
       
        super.init("Climb", "\(altitude)" + "\"", nil, EngineConfig.Climb)
        
        var timeToClimb = "8:00"
        if(match != nil && match![2] != nil){
            timeToClimb = match![2]!
        }
        
        self.altitude = altitude
        self.estimatedTimeEnroute = Time(timeToClimb)
        self.climbTime = self.estimatedTimeEnroute
    }
    
    override func setLocationFromPrevious() -> Bool {
        return true
    }
    
    override func fixName() -> String {
        return "Climb|\(self.altitude!)|\(self.estimatedTimeEnroute.toString())"
    }
}
let PatternLegRE = try? Regex("^" + keywords("PATTERN") + "(?:\\|([0-9][0-9]?(?:\\:[0-5][0-9])?))$").ignoresCase();

class PatternLeg: RallyLegWithoutFix {
    init(_ fixText: String) {
        super.init("Pattern", "",nil, EngineConfig.Pattern)
        let match = fixText.match(PatternLegRE)
        let patternTimeString = (match != nil && match![1] != nil) ? match![1]! : ""
        self.estimatedTimeEnroute = Time(patternTimeString)
    }
    
    override func setLocationFromPrevious() -> Bool {
        return true
    }
    
    override func fixName() -> String {
        return "Pattern|\(self.estimatedTimeEnroute.toString())"
    }
}


let TurnLegRE = try? Regex("^(" + keywords("LEFT") + "|" + keywords("RIGHT") + ")(?:\\|\\+(\\d))?$").ignoresCase();

class TurnLeg: RallyLegWithoutFix {
    
    init(_ fixText: String, _ isRightTurn: Bool) {
        let match = fixText.match(TurnLegRE)
        var direction = ("Left")
        if(match != nil && match![1] != nil){
            direction = ((match![1]! as String).uppercased() == keywords("LEFT")) ? "Left" : "Right"
        }
        
        let engineConfig = EngineConfig.Cruise
        
        super.init(direction, "", GeoLocation( -1,  -1), engineConfig)

        self.extraTurns = (match != nil && match![2] != nil) ?  Int(match![2]!) ?? 0 : 0
        
    }
    
    override func fixName() -> String {
        var result = self.type
        if self.extraTurns > 0 {
            result = result + "|+" + "\(self.extraTurns)"
        }
        return result
    }
}

let LegModifier = try? Regex("(360|3[0-5][0-9]|[0-2][0-9]{2}|[0-9]{1,2})@([0-9]{1,3})|([1-9][0-9]{1,2}|0)kts").ignoresCase();

class FlightPlan {
    var _name: String
    var _route: String
    var _firstLeg: Leg?
    var _lastLeg: Leg?
    var _legCount: Int
    var _defaultWindDirection: Int
    var _defaultWindSpeed: Int
    var _trueAirspeedOverride: Int
    var _timeToGate: Time?
    var _totalFuel: Double
    var _totalTime: Time
    var _gateTime: Time
    
    init(_ name: String, _ route: String) {
        self._name = name
        self._route = route
        self._firstLeg = nil
        self._lastLeg = nil;
        self._legCount = 0;
        self._defaultWindDirection = 0;
        self._defaultWindSpeed = 0;
        self._trueAirspeedOverride = 0
        self._timeToGate = nil;
        self._totalFuel = 0;
        self._totalTime = Time(0)
        self._gateTime = Time(0)
        RallyLeg.reset() //  Refactor to make this more OO
    }
    
    func clear() {

    }
    
    func appendLeg(_ leg: Leg?) {
        if _firstLeg == nil {
            _firstLeg = leg
        } else if _lastLeg != nil {
            _lastLeg!.setNext(leg)
        }
        
        leg?.setPrevious(_lastLeg)
        leg?.setNext(nil)
        if((self._trueAirspeedOverride) != 0){
            leg?.setTrueAirspeed(Double(self._trueAirspeedOverride))
            self.clearTrueAirspeedOverride()
        }
        if((self._defaultWindSpeed) != 0){
            leg?.setWind(self._defaultWindDirection, Double(self._defaultWindSpeed))
        }
        _lastLeg = leg
        _legCount += 1
    }
    
    func setDefaultWind(_ windDirection: Int, _ windSpeed: Int) {
        self._defaultWindDirection = windDirection
        self._defaultWindSpeed = windSpeed
    }
    
    func clearTrueAirspeedOverride() {
        self._trueAirspeedOverride = 0
    }
    func setTrueAirspeedOverride(_ trueAirspeed:Int) {
        self._trueAirspeedOverride = trueAirspeed
    }
    
    func isLegModifier(_ fix:String?) -> Bool
    {
        return (fix?.firstMatch(of: LegModifier!)?.range.isEmpty) ?? false
    }
    func processLegModifier(_ fix:String?)
    {
        let match = fix?.match(LegModifier)

        if let match{
            if(match[1] != nil && match[2] != nil){
                let windDirection = (Int(match[1]!) ?? 0) % 360
                let windSpeed = Int(match[2]!)
                self.setDefaultWind(windDirection,  windSpeed!)
            }else if(match[3] != nil){
                let trueAirspeed = Int(match[3]!);
                self.setTrueAirspeedOverride(trueAirspeed!)
            }
        }
    }
    func resolveWaypoint(_ waypointText: String) {

        if isLegModifier( waypointText) {
            processLegModifier( waypointText)
        } else if RallyLegWithoutFix.isRallyLegWithoutFix(waypointText) {
            let rallyLeg = RallyLegWithoutFix.getLegNoFix(waypointText)
            if let rallyLeg {
                appendLeg(rallyLeg)
            }
        } else {
    
            var fixName = RallyLeg.fixNeeded(waypointText)
            var isRallyWaypoint = false
            
            if fixName.isEmpty == false {
                isRallyWaypoint = true
            } else {
                fixName = waypointText
            }
            var waypoint = userWaypoints.find(fixName)
            if(waypoint == nil){
                waypoint = faaWaypoints.find(fixName);
            }
            if (waypoint == nil) {
                error( "Couldn't find waypoint \"\(waypointText)\"")
                return
            }
            let location = GeoLocation( waypoint!["latitude"] as! Double,  waypoint!["longitude"] as! Double)
            if isRallyWaypoint {
                let rallyLeg = RallyLeg.getLegWithFix( waypointText, fixName,  location)
                appendLeg(rallyLeg!)
            }else{
                appendLeg(Leg( waypoint?["name"] as? String ?? "", location))
            }
        }
    }
    
    
    func parseRoute() {
        let waypointsToLookup = _route.split(separator: " ");
        var priorWaypoint = "";
        
        for waypointIndex in 0..<waypointsToLookup.count {
            let currentWaypoint = waypointsToLookup[waypointIndex].uppercased()
            if (faaAirways.isAirway(currentWaypoint) && (waypointIndex + 1) < waypointsToLookup.count) {
                let exitWaypointFix = waypointsToLookup[waypointIndex + 1]
                let airwayFixes = faaAirways.resolveAirway(currentWaypoint, priorWaypoint,String(exitWaypointFix))
                var airwayFixIndex = 1
                while airwayFixIndex < airwayFixes.count - 1 {
                    resolveWaypoint(airwayFixes[airwayFixIndex])
                    airwayFixIndex += 1
                }
            }else{
                self.resolveWaypoint(currentWaypoint)
            }
            priorWaypoint = currentWaypoint
        }
    }
    
    func calculate() {
        if(_firstLeg == nil){
            return
        }
        var haveStartTiming = false
        var haveStopTiming = false
        var thisLeg = _firstLeg
        while thisLeg != nil {
            thisLeg?.calculate()
            if((thisLeg?.startFlightTiming) != nil){
                if(haveStartTiming)
                {
                    error("Have duplicate Start timing leg in row " + thisLeg!.toString());
                    haveStartTiming = true
                }
            }
            if((thisLeg?.stopFlightTiming) != nil){
                if(haveStopTiming)
                {
                    error("Have duplicate Timing leg in row " + thisLeg!.toString());
                    haveStopTiming = true
                }
            }
            thisLeg = thisLeg?.nextLeg()
        }
        if(haveStartTiming == false){
            _firstLeg?.startFlightTiming = true
        }
        if(haveStopTiming == false){
            _firstLeg?.stopFlightTiming = true
        }
        thisLeg = _firstLeg
        while thisLeg != nil {
            thisLeg!.updateForward()
            thisLeg = thisLeg!.nextLeg()
        }
        thisLeg = _lastLeg
        while thisLeg != nil {
            thisLeg!.updateBackward()
            thisLeg = thisLeg!.previousLeg()
        }
        thisLeg = _firstLeg
        while thisLeg != nil {
            if (thisLeg?.startFlightTiming == true){
                _gateTime = Time(thisLeg!.estTimeRemaining)
            }
            thisLeg = thisLeg!.nextLeg()
        }
        _totalTime = _firstLeg!.estTimeRemaining
        
    }
    
    func resolvedRoute() -> String {
        var result = ""
        var lastWindDirection: Int = 0
        var lastWindSpeed: Double = 0
        var legIndex: Int = 0
        var currentLeg = _firstLeg
        while  currentLeg != nil {
            if legIndex > 0 {
                result = result + " "
            }
            
            if !currentLeg!.isSameWind(lastWindDirection, lastWindSpeed) {
                result = result + currentLeg!.windToString() + " "
                lastWindDirection = currentLeg!.windDirection
                lastWindSpeed = currentLeg!.windSpeed
            }
            
            if !currentLeg!.isStandardTrueAirspeed() {
                result = result + currentLeg!.trueAirspeedToString() + " "
            }
            
            result = result + currentLeg!.toString()
            currentLeg = currentLeg!.nextLeg()
            legIndex += 1
        }
        
        return result
    }
    func name() -> String{
        return _name
    }
    func totalTime() -> Time{
        return _totalTime
    }
    func gateTime() -> Time{
        return _gateTime
    }
    func toString() -> String {
        var result = ""
        var lastWindDirection = 0
        var lastWindSpeed = 0
        
        var legIndex = 0
        var currentLeg = self._firstLeg
        
        while (currentLeg != nil) {
            if legIndex > 0 {
                result = result + " "
            }
            
            if !currentLeg!.isSameWind(lastWindDirection, Double(lastWindSpeed)) {
                result = result + currentLeg!.windToString() + " "
                lastWindDirection = currentLeg!.windDirection
                lastWindSpeed = Int(currentLeg!.windSpeed)
            }
            
            if !currentLeg!.isStandardTrueAirspeed() {
                result = result + currentLeg!.trueAirspeedToString() + " "
            }
            
            result = result + currentLeg!.toString()
            result = result + "\( currentLeg!.location.toString() ) \(currentLeg!.distance.toFixed(2))nm \(currentLeg!.estGS.toFixed(2))kts \(currentLeg!.estimatedTimeEnroute.toString())  "
            currentLeg = currentLeg!.nextLeg()
            legIndex += 1
        }
        
        result = result + " gate time \(_gateTime._seconds)"
        result = result + " total time \(self._firstLeg != nil ? self._firstLeg!.estTimeRemaining._seconds : 0)"
        
        return result
    }
    
}
func EngineConfigAppendConfig() {
    EngineConfig.appendConfig( "Taxi",  2, 0)
    EngineConfig.appendConfig( "Runup",  8,  0)
    EngineConfig.appendConfig( "Takeoff",  27,  105)
    EngineConfig.appendConfig( "Climb",  22,  125)
    EngineConfig.appendConfig( "Cruise",  15,  142)
    EngineConfig.appendConfig( "Pattern",  11,  95)
}

class ExpectedFlightPlan {
    var name: String
    var route: String
    var expectedRoute: String?
    var expectedTotalTime: Time?
    var expectedGateTime: Time?
    private var flightPlan: FlightPlan
    
    init(_ name: String, _ route: String, _ expectedRoute: String? = nil, _ expectedTotalTime: Time? = nil, _ expectedGateTime: Time? = nil) {
        self.name = name
        self.route = route
        self.expectedRoute = expectedRoute
        self.expectedTotalTime = expectedTotalTime
        self.expectedGateTime = expectedGateTime
        self.flightPlan = FlightPlan(name, route)
    }
    
    func reset() {
        flightPlan = FlightPlan(name, route)
    }
    
    func resolveRoute() {
        flightPlan.parseRoute()
    }
    
    func calculate() {
        flightPlan.calculate()
    }
    func checkExpectations() {
        if let expectedRoute = expectedRoute {
            let computedRoute = flightPlan.resolvedRoute()
            if expectedRoute != computedRoute {
                error("Flight plan \(flightPlan._name) route different than expected (\"\(expectedRoute)\"), got (\"\(computedRoute)\")")
            }
        }
        
        if let expectedTotalTime = expectedTotalTime {
            let computedTotalTime = flightPlan._totalTime
            let deltaTime = abs(Time.differenceBetween( expectedTotalTime, computedTotalTime).seconds())
            if deltaTime > 5 {
                error("Flight plan \(flightPlan._name) total time different than expected (\(expectedTotalTime)), got (\(computedTotalTime))")
            }
        }
        
        if let expectedGateTime = expectedGateTime {
            let computedGateTime = flightPlan._gateTime
            let deltaTime = abs(Time.differenceBetween(expectedGateTime,  computedGateTime).seconds())
            if deltaTime > 5 {
                error("Flight plan \(flightPlan._name) gate time different than expected (\(expectedGateTime)), got (\(computedGateTime))")
            }
        }
    }
}


func setupUserWaypoints(){
    EngineConfigAppendConfig()
    userWaypoints.clear()
    
    userWaypoints.update("Oilcamp", "Oil storage in the middle of no where", "36.68471",  "-120.50277");
    userWaypoints.update("I5.Westshields", "I5 & West Shields", "36.77774", "-120.72426");
    userWaypoints.update("I5.165", "Intersection of I5 and CA165", "36.93022", "-120.84068");
    userWaypoints.update("I5.VOLTA", "I5 & Volta Road", "37.01419", "-120.92878");
    userWaypoints.update("PT.ALPHA", "Intersection of I5 and CA152", "37.05665", "-120.96990");
    userWaypoints.update("Jellysferry", "Jelly's Ferry bridge across Sacramento River", "N40 19.037", "W122 11.359");
    userWaypoints.update("Howie", "RDD Timing Point", "N40 21.893", "W121.8876");
    userWaypoints.update("Hale", "2014 Leg 1 Timing", "N39 33.621", "W119 14.438");
    userWaypoints.update("Winnie", "2014 Leg 2 Timing", "N40 50.499", "W114 12.595");
    userWaypoints.update("WindRiver", "2014 Leg 3 Timing", "N42 43.733", "W108 38.800");
    userWaypoints.update("Buff", "2014 Leg 4 Timing", "N43 59.455", "W103 16.171");
    userWaypoints.update("Omega", "2014 Leg 5 Timing", "N44 53.388", "W95 38.935");
    userWaypoints.update("Paul", "2014 Leg 6 Timing", "N43 22.027", "W89 37.111");
    userWaypoints.update("MicrowaveSt", "2014 Microwave Station", "N40 24.89", "W117 12.37");
    userWaypoints.update("RanchTower", "2014 Ranch/Tower", "N41 6.16", "W115 5.43");
    userWaypoints.update("FremontIsland", "2014 Fremont Island", "N41 10.49", "W112 20.64");
    userWaypoints.update("Tremonton", "2014 Tremonton", "N41 42.86", "W112 11.05");
    userWaypoints.update("RandomPoint", "2014 Random Point", "N42", "W111 03");
    userWaypoints.update("Farson", "2014 Farson", "N42 6.40", "W109 26.95");
    userWaypoints.update("Midwest", "2014 Midwest", "N43 24.49", "W109 16.68");
    userWaypoints.update("Bill", "2014 Bill", "N43 13.96", "W105 15.60");
    userWaypoints.update("MMNHS", "2014 MMNHS","N43 52.67", "W101 57.65");
    userWaypoints.update("Tracks", "2014 Tracks", "N44 21", "W100 22");
    userWaypoints.update("Towers", "2014 Towers", "N43 34.25", "W92 25.64");
    userWaypoints.update("IsletonBridge", "Isleton Bridge", "N38 10.32", "W121 35.62");
    userWaypoints.update("Mystery15", "2015 Mystery", "N38 46.22", "W122 34.25");
    userWaypoints.update("Paskenta", "Paskenta Town", "N39 53.13", "W122 32.36");
    userWaypoints.update("Bonanza", "Bonanza Town", "N42 12.15", "W121 24.53");
    userWaypoints.update("Silverlake", "Silverlake", "N43 07.41", "W121 03.74");
    userWaypoints.update("Millican", "Bend Timing Start", "N43 52.75", "W120 55.13");
    userWaypoints.update("Goering", "Bend Timing", "N44 05.751", "W120 56.834");
    userWaypoints.update("Constantia2", "Our Constantia Wpt", "N39 56.068", "W120 0.831");
    userWaypoints.update("Hallelujah2", "Reno Timing", "N39 46.509", "W120 2.336");
    userWaypoints.update("Redding.Pond", "Pond 6nm North of KRDD", "N40 36", "W122 17");
    userWaypoints.update("Thunderhill", "Thunder Hill Race Track", "N39 32.36", "W122 19.83");
    userWaypoints.update("CascadeHighway", "Cascade Wonderland Highway", "N40 46.63", "W122 19.12");
    userWaypoints.update("Eagleville", "Eagleville closed airport", "N41 18.73", "W120 3.00");
    userWaypoints.update("DuckLakePass", "Saddle near Duck Lake", "N41 3.00", "W120 3.00");
    
}

extension Double{
    func toFixed(_ n:Int) -> String{
        let d = Double(pow(10.0, Float(n)))
        var v = self*d
        v.round(.toNearestOrAwayFromZero)
        return String(v/d)
    }
}
var tempTimesArray:[Double] = []
extension String{
    func match(_ regex:Regex<AnyRegexOutput>?) -> [String?]?{
        guard let regex else { return nil }
        let input = self
        let match = input.matches(of: regex)
        let startTime = Timer().getTime()/1000
        guard let matchs = match.first  else { return  nil }
        var matchStrs = Array<String?>(repeating: nil, count: matchs.count)
        var idx = matchs.count
        while idx > 0 {
            idx -= 1
            let result = matchs[idx]
            if result.range == nil {continue}
            let subStr = input[result.range!]
            matchStrs[idx] = String(subStr.trimmingPrefix(" "))
        }
        let endTime = Timer().getTime()/1000
        tempTimesArray.append(endTime-startTime)
        return matchStrs
    }
}
