

let useUnicode = false


class UnicodeStrings {
    static let instance: UnicodeStrings? = nil
    static var table: [String: String] = [
            "START": "\u{041d}\u{0410}\u{0427}\u{0410}\u{0422}\u{042c}", // НАЧАТЬ
                "TIMING": "\u{0425}\u{0420}\u{041e}\u{041d}\u{041e}\u{041c}\u{0415}\u{0422}\u{0420}\u{0410}\u{0416}", // ХРОНОМЕТРАЖ
                "TAXI": "\u{0420}\u{0423}\u{041b}\u{0415}\u{041d}\u{0418}\u{0415}", // РУЛЕНИЕ
                "RUNUP": "\u{0414}\u{0412}\u{0418}\u{0413}\u{0410}\u{0422}\u{0415}\u{041b}\u{042c}-\u{041d}\u{0410}\u{041a}\u{0410}\u{0422}\u{0410}", // ДВИГАТЕЛЬ НАКАТА
                "TAKEOFF": "\u{0412}\u{0417}\u{041b}\u{0415}\u{0422}", // ВЗЛЕТ
                "CLIMB": "\u{041f}\u{041e}\u{0414}\u{041d}\u{042f}\u{0422}\u{042c}\u{0421}\u{042f}", // ПОДНЯТЬСЯ
                "PATTERN": "\u{041a}\u{0420}\u{0423}\u{0413}", // КРУГ
                "LEFT": "\u{041b}\u{0415}\u{0412}\u{042b}\u{0419}", // ЛЕВЫЙ
                "RIGHT": "\u{041f}\u{0420}\u{0410}\u{0412}\u{042b}\u{0419}" // ПРАВЫЙ
        ]
        
    static func get(_ keyString:String) -> String
    {
        for keyStr in table.keys {
            let keyStrLower = keyStr.lowercased()
            let valueStrLower = table[keyStr]!.lowercased()
            table[keyStrLower] = valueStrLower
            let keyStrTitle = toTitleCase(keyStr)
            let valueStrTitle = toTitleCase(table[keyStr]!)
             table[keyStrTitle] = valueStrTitle
        }
        return table[keyString]!
    }
    
    static func toTitleCase(_ input: String) -> String {
        let words = input.split(separator: " ")
        let titleCasedWords = words.map { word in
            if let firstChar = word.first, firstChar.isLetter {
                return word.lowercased().prefix(1).uppercased() + word.dropFirst().lowercased()
            } else {
                return word.lowercased()
            }
        }
        return titleCasedWords.joined(separator: " ")
    }
}

