
import Glibc
class Timer {
    private let CLOCK_REALTIME = 0
    private var time_spec = timespec()
    func getTime() -> Double {
        clock_gettime(Int32(CLOCK_REALTIME),&time_spec)
//        clock_gettime(clockid_t(UInt32(CLOCK_REALTIME)),&time_spec)
        return Double(time_spec.tv_sec * 1_000_000 + time_spec.tv_nsec / 1_000)
    }
}
@main
struct Benchmark {
    static func main() throws {
        benchmark.runIteration()

    }
    func runIteration(){
        faaWaypoints.waypoints = _faaWaypoints
        let startTime = Timer().getTime()/1000
        for _ in 0..<1000 {
            setupUserWaypoints()
            expectedFlightPlans.forEach { model in
                model.reset()
                model.resolveRoute()
            }
        }
        let endTime = Timer().getTime()/1000
        print("flight_planner: ms = \(endTime-startTime)")
    }

    func validate()
        {
            setupUserWaypoints()
            expectedFlightPlans.forEach { model in
                model.calculate()
                model.checkExpectations()
            }
        }

}
let benchmark = Benchmark()
