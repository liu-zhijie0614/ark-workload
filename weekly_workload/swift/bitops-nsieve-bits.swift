// The Great Computer Language Shootout
//  http://shootout.alioth.debian.org
//
//  Contributed by Ian Osgood

import Glibc

let inDebug = false
func log(_ str: String) {
    if (inDebug) {
        print(str)
    }
}
func currentTimestamp13() -> Double {
   return Timer().getTime() / 1000.0
}

func pad(_ number: Int, _ width: Int) -> String {
    var s = "\(number)"
    let prefixWidth = width - s.count
    if (prefixWidth > 0) {
        for _ in 0..<prefixWidth {
            s = " " + s
        }
    }
    return s;
}

func primes(_ isPrime: inout [Int], _ n: Int) {
    var count = 0
    let m = 10000<<n, size = (m+31)>>5
    for i in 0..<size {
        isPrime[i] = 0xffffffff
    }
    for i in 2..<m {
        if ((isPrime[i>>5] & 1<<(i % 32)) != 0) {
            var j = i+i
            while (j < m) {
                var tmp = transBigInt32(isPrime[j>>5])
                tmp &= transBigInt32(~(1<<(j&31)))
                isPrime[j>>5] = tmp
                j+=i
            }            
            count += 1
        }
    }
}

func sieve() -> [Int] {
    var isPrime = [Int]()
    for i in 4...4 {
        isPrime = Array.init(repeating: 0, count: (((10000<<i)+31)>>5))
        primes(&isPrime, i);
    }
    
    return isPrime;
}

func run() {
    let result = sieve();
    var sum = 0;
    for i in 0..<result.count {
        sum += result[i]
    }
    
    let expected = -1286749544853;
    if sum != expected {
        fatalError("ERROR: bad result: expected \(expected) but got \(sum)")
    }
}

let startTime = currentTimestamp13()
for i in 0..<80 {
    let startTimeInLoop = currentTimestamp13()
    run()
    let endTimeInLoop = currentTimestamp13()
    //log("bitops-nsieve-bits: ms = \((endTimeInLoop - startTimeInLoop)) i= \(i)")
}
let endTime = currentTimestamp13()
print("bitops-nsieve-bits: ms = \(endTime - startTime)")

class Timer {
  private let CLOCK_REALTIME = 0
  private var time_spec = timespec()

  func getTime() -> Double {
      clock_gettime(Int32(CLOCK_REALTIME),&time_spec)
      return Double(time_spec.tv_sec * 1_000_000 + time_spec.tv_nsec / 1_000)
  }
}

func transBigInt32(_ bigInt32Num: Int) -> Int {
    var tmp = bigInt32Num
    if (tmp > 2147483647) {
        let max = 4294967295
        tmp = tmp % (max + 1)
        if (tmp > 2147483647){
            tmp = tmp - max - 1
        }
    }else if (tmp < -2147483648){
        let max = 4294967295
        tmp = tmp % (max + 1)
        if (tmp < -2147483648) {
            tmp = tmp + max + 1
        }
    }
    return tmp
}