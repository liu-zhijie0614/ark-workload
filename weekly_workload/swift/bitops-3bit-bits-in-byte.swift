// Copyright (c) 2004 by Arthur Langereis (arthur_ext at domain xfinitegames, tld com

import Glibc

let inDebug = false
func log(_ str: String) {
    if (inDebug) {
        print(str)
    }
}

func currentTimestamp13() -> Double {
    return Timer().getTime() / 1000
}

var result = 0;

// 1 op = 6 ANDs, 3 SHRs, 3 SHLs, 4 assigns, 2 ADDs
// O(1)
func fast3bitlookup(_ b: Int) -> Int {
    var c1 = 0  // 0b1110 1001 1001 0100;
    var bi3b = 0xe994  // 3 2 2 1  2 1 1 0
    c1 = 3 & (bi3b >> ((b << 1) & 14))
    c1 += 3 & (bi3b >> ((b >> 2) & 14))
    c1 += 3 & (bi3b >> ((b >> 5) & 6))
  return c1
    
  /*
    lir4,0xE994; 9 instructions, no memory access, minimal register dependence, 6 shifts, 2 adds, 1 inline assign
    rlwinmr5,r3,1,28,30;
    rlwinmr6,r3,30,28,30;
    rlwinmr7,r3,27,29,30;
    rlwnmr8,r4,r5,30,31;
    rlwnmr9,r4,r6,30,31;
    rlwnmr10,r4,r7,30,31;
    addr3,r8,r9;
    addr3,r3,r10;
    */
}

func TimeFunc(_ func1:((_ b: Int) -> Int)) -> Int {
    var x = 0, y = 0, t = 0
    var sum = 0;
    for x in 0..<500 {
        for y in 0..<256 {
            sum += func1(y)
        }
    }
    return sum
}

/// @Benchmark
func runTest() {
    let sum = TimeFunc(fast3bitlookup);
    let expected = 512000;
    if sum != expected {
        fatalError("ERROR: bad result: expected \(expected) but got \(sum)")
    }
}
/*
* @State
* @Tags Jetstream2
*/
class Benchmark {
  /*
   *@Benchmark
   */
   func run() {
     let startTime = currentTimestamp13()
     for i in 0..<80 {
	let startTimeInLoop = currentTimestamp13()
	runTest()
	let endTimeInLoop = currentTimestamp13()
	//log("bitops_3bit_bits_in_byte: ms = \((endTimeInLoop - startTimeInLoop)) i= \(i)")
     }
     let endTime = currentTimestamp13()
     print("bitops-3bit-bits-in-byte: ms = \(endTime - startTime)")
   }
}

class Timer {
   private let CLOCK_REALTIME = 0
   private var time_spec = timespec()

   func getTime() -> Double {
       clock_gettime(Int32(CLOCK_REALTIME),&time_spec)
       return Double(time_spec.tv_sec * 1_000_000 + time_spec.tv_nsec / 1_000)
   }
}
Benchmark().run()
