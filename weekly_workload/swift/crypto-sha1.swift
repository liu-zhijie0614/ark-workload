
/**
 ** A JavaScript implementation of the Secure Hash Algorithm, SHA-1, as defined
 ** in FIPS PUB 180-1
 ** Version 2.1a Copyright Paul Johnston 2000 - 2002.
 ** Other contributors: Greg Holt, Andrew Kepert, Ydnar, Lostinet
 ** Distributed under the BSD License
 ** See http://pajhome.org.uk/crypt/md5 for details.
 **/
import Glibc

let inDebug = false
func log(_ str: String) {
    if (inDebug) {
        print(str)
    }
}
func currentTimestamp13() -> Double {
   return Timer().getTime() / 1000.0
}

/*
 * Configurable variables. You may need to tweak these to be compatible with
 * the server-side, but the defaults work in most cases.
 */
let hexcase: Int = 0  /* hex output format. 0 - lowercase; 1 - uppercase        */
let b64pad: String = "" /* base-64 pad character. "=" for strict RFC compliance   */
let chrsz: Int   = 8  /* bits per input character. 8 - ASCII; 16 - Unicode      */

/*
 * These are the funcs you'll usually want to call
 * They take String arguments and return either hex or base-64 encoded Strings
 */
func hex_sha1(_ s: String) -> String {
    return binb2hex(core_sha1(str2binb(s),s.count * chrsz))
}

/*
 * Calculate the SHA-1 of an array of big-endian words, and a bit length
 */
func core_sha1(_ x1: [Int], _ len:Int) -> [Int] {
    var x = x1
    let toMax = max(len >> 5, ((((len + 64) >> 9) << 4) + 15)) - x.count
    if (toMax >= 0) {
        x = x + Array.init(repeating: 0, count: toMax + 1)
    }
    /* append padding */
    x[len >> 5] |= 0x80 << (24 - len % 32)
    x[(((len + 64) >> 9) << 4) + 15] = len
    
    var  w:[Int] = Array.init(repeating: 0, count: 80)
    var a1 =  1732584193
    var b1 = -271733879
    var c1 = -1732584194
    var d1 =  271733878
    var e1 = -1009589776
    
    var i = 0
    while i<x.count {
        let olda = a1
        let oldb = b1
        let oldc = c1
        let oldd = d1
        let olde = e1

        for j in 0..<80 {
            if j < 16 {
                w[j] = x[i + j]
            } else {
                w[j] = rol(w[j-3] ^ w[j-8] ^ w[j-14] ^ w[j-16], 1)
            }
            let t = safe_add(safe_add(rol(a1, 5), sha1_ft(j, b1, c1, d1)),
                             safe_add(safe_add(e1, w[j]), sha1_kt(j)))
            e1 = d1
            d1 = c1
            c1 = rol(b1, 30)
            b1 = a1
            a1 = t
        }

        a1 = safe_add(a1, olda)
        b1 = safe_add(b1, oldb)
        c1 = safe_add(c1, oldc)
        d1 = safe_add(d1, oldd)
        e1 = safe_add(e1, olde)

        i += 16
    }
    return [a1, b1, c1, d1, e1]
}

/*
 * Perform the appropriate triplet combination func for the current
 * iteration
 */
func sha1_ft(_ t:Int, _ b:Int, _ c:Int, _ d:Int) -> Int {
    if t < 20 {
        return (b & c) | ((~b) & d)
    }
    if t < 40 {
        return b ^ c ^ d
    }
    if t < 60 {
        return (b & c) | (b & d) | (c & d)
    }
    return b ^ c ^ d
}

/*
 * Determine the appropriate additive constant for the current iteration
 */
func sha1_kt(_ t:Int) -> Int {
    return (t < 20) ?  1518500249 : (t < 40) ?  1859775393 :
    (t < 60) ? -1894007588 : -899497514
}

/*
 * Add integers, wrapping at 2^32. This uses 16-bit operations internally
 * to work around bugs in some JS interpreters.
 */
func safe_add(_ x:Int, _ y:Int) -> Int
{
    let lsw = (x & 0xFFFF) + (y & 0xFFFF)
    let msw = (x >> 16) + (y >> 16) + (lsw >> 16)
    return Int((Int32(msw) << 16) | (Int32(lsw) & 0xFFFF))
}

/*
 * Bitwise rotate a 32-bit Int to the left.Int32();
 */
func rol(_ num:Int, _ cnt:Int) -> Int
{
    return transBigInt32((num << cnt)) | Int((Int32(transBigInt32(num)) >>> Int32(32 - cnt)))
}

/*
 * Convert an 8-bit or 16-bit String to an array of big-endian words
 * In 8-bit func, characters >255 have their hi-byte silently ignored.
 */
func str2binb(_ str:String) -> [Int] {

    var bin = Array<Int>()
    let mask = (1 << chrsz) - 1    
    let toMax = (((str.count * chrsz - 1)>>5) - bin.count)
    if (toMax >= 0) {
        bin = bin + Array.init(repeating: 0, count: toMax + 1)
    }
    let strArray = Array<Character>(str)
    var i = 0
    for _ in 0..<str.count {
        bin[i>>5] |= (Int(strArray[i/chrsz].asciiValue ?? 0) & mask) << (32 - chrsz - i%32)
        i+=chrsz
    }
    return bin
}

/*
 * Convert an array of big-endian words to a hex String.
 */
func binb2hex(_ binarray:[Int]) -> String {

    let hex_tab = hexcase != 0 ? "0123456789ABCDEF" : "0123456789abcdef"
    var str = ""
    let strArray = Array<Character>(hex_tab)
    for i in 0..<binarray.count * 4 {
        str += String(strArray[(binarray[i>>2] >> ((3 - i%4)*8+4)) & 0xF]) + 
                String(strArray[(binarray[i>>2] >> ((3 - i%4)*8  )) & 0xF])
    }
    return str
}

func run() {
    var plainText:String = """
    Two households, both alike in dignity,\n\
    In fair Verona, where we lay our scene,\n\
    From ancient grudge break to new mutiny,\n\
    Where civil blood makes civil hands unclean.\n\
    From forth the fatal loins of these two foes\n\
    A pair of star-cross'd lovers take their life;\n\
    Whole misadventured piteous overthrows\n\
    Do with their death bury their parents' strife.\n\
    The fearful passage of their death-mark'd love,\n\
    And the continuance of their parents' rage,\n\
    Which, but their children's end, nought could remove,\n\
    Is now the two hours' traffic of our stage;\n\
    The which if you with patient ears attend,\n\
    What here shall miss, our toil shall strive to mend.
    """;
    
    for _ in 0..<4 {
        plainText += plainText
    }
    
    let sha1Output = hex_sha1(plainText)
    //log("crypto-sha1 - sha1Output: \(sha1Output)");
    let expected = "2524d264def74cce2498bf112bedf00e6c0b796d"
    if (sha1Output != expected){
        fatalError("ERROR: bad result: expected " + expected + " but got " + sha1Output)
    }
}

/*
 * @State
 * @Tags Jetstream2
 */
class Benchmark {
  /*
   * @Benchmark
   */
    func runIteration() {
        let startTime = currentTimestamp13()
        for _ in 0..<25 {
            run()
        }
        let endTime = currentTimestamp13()
        //log("crypto-sha1: ms = \(endTime - startTime)")
    }
}

class Timer {
   private let CLOCK_REALTIME = 0
   private var time_spec = timespec()

   func getTime() -> Double {
       clock_gettime(Int32(CLOCK_REALTIME),&time_spec)
       return Double(time_spec.tv_sec * 1_000_000 + time_spec.tv_nsec / 1_000)
   }
}

infix operator >>> : BitwiseShiftPrecedence
func >>> (lhs: Int32, rhs: Int32) -> Int32 {
    return Int32(bitPattern: UInt32(bitPattern: lhs) >> UInt32(rhs))
}
func transBigInt32(_ bigInt32Num: Int) -> Int {
    var tmp = bigInt32Num
    if (tmp > 2147483647) {
        let max = 4294967295
        tmp = tmp % (max + 1)
        if (tmp > 2147483647){
            tmp = tmp - max - 1
        }
    }else if (tmp < -2147483648){
        let max = 4294967295
        tmp = tmp % (max + 1)
        if (tmp < -2147483648) {
            tmp = tmp + max + 1
        }
    }
    return tmp
}

let startTime = currentTimestamp13()
let benchmark = Benchmark()
for i in 0..<20 {
    let startTimeInLoop = currentTimestamp13()
    benchmark.runIteration()
    let endTimeInLoop = currentTimestamp13()
    //log("crypto-sha1: ms = \(endTimeInLoop - startTimeInLoop) i = \(i)")
}
let endTime = currentTimestamp13()
print("crypto-sha1: ms = \((endTime - startTime))")


