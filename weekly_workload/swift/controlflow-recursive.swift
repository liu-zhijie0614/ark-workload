// The Computer Language Shootout
// http://shootout.alioth.debian.org/
// contributed by Isaac Gouy
import Glibc

let inDebug = false
func log(_ str: String) {
    if (inDebug) {
        print(str)
    }
}

func currentTimestamp13() -> Double {
   return Timer().getTime() / 1000.0
}

func ack(_ m: Int, _ n: Int) -> Int {
   if (m==0) { return n+1; }
   if (n==0) { return ack(m-1,1); }
   return ack(m-1, ack(m,n-1) );
}

func fib(_ n: Double) -> Int {
    if (n < 2) { return 1; }
    return fib(n-2) + fib(n-1);

}

func tak(_ x: Int, _ y: Int, _ z: Int) -> Int {
    if (y >= x) { return z; }
    return tak(tak(x-1,y,z), tak(y-1,z,x), tak(z-1,x,y));
}

func run() {
    var result = 0;
    
    for i in 3...5 {
        result += ack(3,i)
        result += fib(17.0+Double(i))
        result += tak(3*i+3,2*i+2,i+1)
    }

    let expected = 57775;
    if (result != expected) {
        fatalError("ERROR: bad result: expected \(expected) but got \(result)")
    }
}
let startTime = currentTimestamp13()
for i in 0..<80 {
    let startTimeInLoop = currentTimestamp13()
    run()
    let endTimeInLoop = currentTimestamp13()
    //log("controlflow-recursive: ms = \((endTimeInLoop - startTimeInLoop)) i= \(i)")
}
let endTime = currentTimestamp13()
print("controlflow-recursive: ms = \(endTime - startTime)")

class Timer {
  private let CLOCK_REALTIME = 0
  private var time_spec = timespec()

  func getTime() -> Double {
      clock_gettime(Int32(CLOCK_REALTIME),&time_spec)
      return Double(time_spec.tv_sec * 1_000_000 + time_spec.tv_nsec / 1_000)
  }
}