import Glibc

/*
* @State
* @Tags Jetstream2
*/
class Morph {
    
    var loops: Int = 15
    var nx: Int = 120
    var nz: Int = 120
    
    
    func morph(_ arr: inout [Double], _ f: Double) {
        let PI2nx = Double.pi * 8 / Double(nx)
        let f30 = -(50 * sin(f * Double.pi * 2))
        for i in 0..<nz {
            for j in 0..<nx {
                let index = 3 * (i * nx + j) + 1
                let x = Double((j - 1)) * PI2nx
                arr[index] = Double(sin(x)) * (-f30)
                if (index % 317 == 1) {
                    //DebugLog("arr index value is " + "\(arr[index])")
                }
            }
        }
    }
    /*
    *@Benchmark
    */
    func runTest() {
        var a: [Double] = Array(repeating: 0, count: nx * nx * 3)
        for index in 0..<(nx * nx * 3) {
            a[index] = 0
        }
        
        for i in 0..<loops {
            morph(&a, Double(i) / Double(loops))
        }
        
        var testOutput: Double = 0
        for i in 0..<nx {
            let index = 3 * (i * nx + 1) + 1
            if index >= a.count {
                break
            }
            testOutput += a[index]
        }
        //DebugLog("testOutput value is \(testOutput)")
                
        var epsilon = 1e-13
        
        if abs(testOutput) >= Double(epsilon) {
            debugPrint("Error: bad test output: expected magnitude below" + "\(epsilon)" + "but got" + "\(testOutput)")
        }
        
    }
    
}

let isDebug : Bool = false
func DebugLog(_ msg : Any , file: String = #file, line: Int = #line, fn: String = #function){
    if isDebug {
        print("\(msg)")
    }
}

class Timer {
    private let CLOCK_REALTIME = 0
    private var time_spec = timespec()

    func getTime() -> Double {
        clock_gettime(Int32(CLOCK_REALTIME),&time_spec)
        return Double(time_spec.tv_sec * 1_000_000 + time_spec.tv_nsec / 1_000)
    }
}

/// @Benchmark
func run() {
    let start = Timer().getTime()
    let morph = Morph()
    for _ in 0..<80 {
        morph.runTest()
    }
    let end = Timer().getTime()
    print("3d-morph: ms = \((end - start) / 1000)")
}

run()

