// The Great Computer Language Shootout
//  http://shootout.alioth.debian.org
//
//  Contributed by Ian Osgood

import Glibc

var last : Double = 42,  ret : Int = 0
let A : Double = 3877, C : Double = 29573, M : Double = 139968

func rand(_ max:Double) -> Double {
    last = Double(Int(last * A + C) % Int(M))
    return max * last / M
}

func makeCumulative(_ table: inout Dictionary <String,Double>) {
    var last : String? = nil
    for (key, value) in table {
        if last != nil {
            table[key] = value + table[last!]!
        }
        last = key;
    }
}

func fastaRepeat(_ m: Int, _ seq:String) {
    var n = m
    var seqi = 0, lenOut = 60;
    while (n>0) {
        if n < lenOut {
          lenOut = n
        }
        if (seqi + lenOut < seq.count) {
            ret += lenOut
            seqi += lenOut
        } else {
            let s = seq.suffix(from: seq.index(seq.startIndex, offsetBy: seqi))
            seqi = lenOut - s.count
            ret += (s + seq.prefix(upTo: seq.index(seq.startIndex, offsetBy: seqi))).count
        }
        n -= lenOut
    }
}

func fastaRandom(_ m: Int, _  table: inout Dictionary <String,Double>) {
    var n = m
    var line = Array<Any?>(repeating: nil, count: 60)
    makeCumulative(&table)
    while (n > 0) {
        if n < line.count {
            line =  Array<Any?>(repeating: nil, count: n)
        };
        for i in 0..<line.count {
            let r = rand(1)
            for (key, value) in table {
                if r < value {
                    line[i] = key
                    break;
                }
            }
        }
        ret += line.count
        n -= line.count
    }
}

/*
 * @State
 * @Tags Jetstream2
 */
class Benchmark {
    /*
     *@Benchmark
     */
    func runIteration() {
        let count : Int = 7;
        
        let ALU : String =
          "GGCCGGGCGCGGTGGCTCACGCCTGTAATCCCAGCACTTTGG" +
          "GAGGCCGAGGCGGGCGGATCACCTGAGGTCAGGAGTTCGAGA" +
          "CCAGCCTGGCCAACATGGTGAAACCCCGTCTCTACTAAAAAT" +
          "ACAAAAATTAGCCGGGCGTGGTGGCGCGCGCCTGTAATCCCA" +
          "GCTACTCGGGAGGCTGAGGCAGGAGAATCGCTTGAACCCGGG" +
          "AGGCGGAGGTTGCAGTGAGCCGAGATCGCGCCACTGCACTCC" +
          "AGCCTGGGCGACAGAGCGAGACTCCGTCTCAAAAA";
        
        var IUB : Dictionary<String, Double> = Dictionary()
        IUB["a"] = 0.27
        IUB["c"] = 0.12
        IUB["g"] = 0.12
        IUB["t"] = 0.27
        IUB["B"] = 0.02
        IUB["D"] = 0.02
        IUB["H"] = 0.02
        IUB["K"] = 0.02
        IUB["M"] = 0.02
        IUB["N"] = 0.02
        IUB["R"] = 0.02
        IUB["S"] = 0.02
        IUB["V"] = 0.02
        IUB["W"] = 0.02
        IUB["Y"] = 0.02
        
        var HomoSap : Dictionary<String, Double> = Dictionary()
        HomoSap["a"] = 0.3029549426680
        HomoSap["c"] = 0.1979883004921
        HomoSap["g"] = 0.1975473066391
        HomoSap["t"] = 0.3015094502008

        fastaRepeat(2*count*100000,  ALU)
        fastaRandom(3*count*1000, &IUB)
        fastaRandom(5*count*1000, &HomoSap)

        let expected = 1456000;
        if (ret != expected) {
            fatalError("ERROR: bad result: expected \(expected) but got \(ret)")
        }
    }
}

class Timer {
    private let CLOCK_REALTIME = 0
    private var time_spec = timespec()
    func getTime() -> Double {
        clock_gettime(Int32(CLOCK_REALTIME),&time_spec)
        return Double(time_spec.tv_sec * 1_000_000 + time_spec.tv_nsec / 1_000)
    }
}

let start = Timer().getTime()
Benchmark().runIteration()
let end = Timer().getTime()
print("string-fasta: ms = \(end-start)")
