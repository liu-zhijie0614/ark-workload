import Glibc

func pad(_ number : Int,_ width : Int) -> String {
    var s = String("\(number)")
    let prefixWidth = width - s.count
    if (prefixWidth > 0) {
        for _ in 1...prefixWidth {
            s = " " + s
        }
    }
    return s
}

func nsieve(_ m : Int, _ isPrime : inout Array<Int> ) -> Int {
    var count : Int
    for i in 2...m {
        isPrime[i] = 1
    }
    count = 0
    
    for i in 2...m {
        if (isPrime[i] == 1) {
            var k = i + i
            while k <= m {
                isPrime[k] = 0
                k += i
            }
            count += 1
        }
    }
    return count
}

func sieve()->Int {
    var sum = 0
    for i in 1...3 {
        
        let m = (1<<i)*10000
        var flags = [Int](repeating: 0, count: m+1)
        sum += nsieve(m, &flags)
    }
    return sum
}


/*
 *@State
 *@Tags Jetstream2
 */
class Benchmark {
    func run() {
        let result = sieve();
        let expected = 14302;
        if (result != expected){
          print("ERROR: bad result: expected  \(expected)  but got \(result)");
        }
    }
    
   /*
    * @Benchmark
    */
    func runIterationTime() {
        let start = Timer().getTime()
        for _ in 0..<80 {
          self.run()
        }
        let end = Timer().getTime()
        let duration = (end - start) / 1000
        print("access-nsieve: ms = \(duration)")
    }
}


class Timer {
    private let CLOCK_REALTIME = 0
    private var time_spec = timespec()

    func getTime() -> Double {
        clock_gettime(Int32(CLOCK_REALTIME),&time_spec)
        return Double(time_spec.tv_sec * 1_000_000 + time_spec.tv_nsec / 1_000)
    }
}

Benchmark().runIterationTime()
