import Glibc
class Timer {
    private let CLOCK_REALTIME = 0
    private var start_timespec = timespec()
    private var end_timespec = timespec()
    private var time_spec = timespec()
    func start() {
        clock_gettime(Int32(CLOCK_REALTIME),&start_timespec)
    }
    
    func stop() -> Double {
        clock_gettime(Int32(CLOCK_REALTIME),&end_timespec)
        let start_time = Double(start_timespec.tv_sec * 1_000_000 + start_timespec.tv_nsec / 1_000)
        let end_time = Double(end_timespec.tv_sec * 1_000_000 + end_timespec.tv_nsec / 1_000)
        let time = end_time - start_time
        return time / 1_000
    }
    func getTime() -> Double {
        clock_gettime(Int32(CLOCK_REALTIME),&time_spec)
        return Double(time_spec.tv_sec * 1_000_000 + time_spec.tv_nsec / 1_000)
    }
}
class Obj {
    var value: Int = 0
    init(value: Int) {
        self.value = value
    }
}

func GenerateFakeRandomObject() -> [Obj] {
    let resource: [Obj] = [Obj](repeating: Obj(value: Int.random(in: 1..<10)), count: 15)
    return resource
}
var global = 0
var arr : [Int] = [12, 43, 56, 76, 89, 54, 45, 32, 35, 47, 46, 44, 21, 37, 84]

func GenerateFakeRandomInteger() -> [Int] {
    let resource: [Int] = [12, 43, 56, 76, 89, 54, 45, 32, 35, 47, 46, 44, 21, 37, 84]
    return resource;
}

class Example {
    // static With parameters
    static func foo(_ resources: [Int], _ i: Int, _ i3: Int, _ resourcesLength: Int) ->Int {
        var i4 = i3;
        if ((resources[i % Int(i3) & (resourcesLength - 1)] & 1) == 0) {
            i4 += 1;
        } else {
            i4 += 2;
        }
        return i4;
    }
    // static Without parameters
    static func parameterlessFoo() -> Obj {
        var res: [Obj] = [Obj](repeating: Obj(value: Int.random(in: 1..<10)), count: 5)
        let resources = GenerateFakeRandomObject()
        for i in 0..<200 {
            res[i % 5] = resources[i % 15]
        }
        return res[1]
    }
    // static Different  parameters
    static func differentFoo(_ resources: [Int], _ i: Int, _ i3: Double, _ resourcesLength: Int) -> Double {
        var i4 = i3;
        if ((resources[i % Int(i3) & (resourcesLength - 1)] & 1) == 0) {
            i4 += 1.1;
        } else {
            i4 += 2.1;
        }
        return i4;
    }
    // static Default  parameters
    static func defaultFoo(_ resources: [Int]! = arr, _ i: Int! = 2, _ i3: Int! = 1, _ resourcesLength: Int! = 15)->Int{
        var i4 = 1;
        if ((resources[i % Int(i3) & (resourcesLength - 1)] & 1) == 0) {
            i4 += 1;
        } else {
            i4 += 2;
        }
        return i4;
    }
    // static Variable  parameters
    static func variableFoo(_ a: Int? = nil, _ b: String? = nil, _ c: Bool? = nil) {
        arr[global] += 1
    }
    // static ...Args  parameters
    static func argsFoo(_ args: Int...) {
        global += 1
    }
}

func RunStaticFunction() -> Int {
    let count = 10000000
    let timer = Timer()
    let resources = GenerateFakeRandomInteger();
    var i3 = 1
    let startTime = timer.getTime()
    let resourcesLength = resources.count
    for i in 0..<count {
        i3 = Example.foo(resources, i, i3, resourcesLength)
    }
    let midTime = timer.getTime()
    for _ in 0..<count {
    }
    let endTime = timer.getTime()
    let time = ((midTime - startTime) - (endTime - midTime)) / 1_000
    print("Static Function Call - RunStaticFunction:\t"+String(time)+"\tms");
    return Int(i3)
}
_ = RunStaticFunction()

func RunParameterlessStaticFunction() -> Obj {
    let count = 10000
    let timer = Timer()
    var i3 = Obj(value:1)
    global = 0
    let startTime = timer.getTime()
    for _ in 0..<count {
        i3 = Example.parameterlessFoo()
    }
    let midTime = timer.getTime()
    for _ in 0..<count {
    }
    let endTime = timer.getTime()
    let time = ((midTime - startTime) - (endTime - midTime)) / 1_000
    print("Static Function Call - RunParameterlessStaticFunction:\t"+String(time)+"\tms");
    return i3
}
_ = RunParameterlessStaticFunction()

func RunNormalDifStaticFunc() -> Int {
    let count = 10000000
    let timer = Timer()
    let resources = GenerateFakeRandomInteger();
    var i3 = 1.1
    let startTime = timer.getTime()
    let resourcesLength = resources.count
    for i in 0..<count {
        i3 = Example.differentFoo(resources, i, i3, resourcesLength)
    }
    let midTime = timer.getTime()
    for _ in 0..<count {
    }
    let endTime = timer.getTime()
    let time = ((midTime - startTime) - (endTime - midTime)) / 1_000
    print("Static Function Call - RunNormalDifStaticFunc:\t"+String(time)+"\tms");
    return Int(i3)
}
_ = RunNormalDifStaticFunc()

func RunNormalVariableFStaticFunc() -> Int {
    let count = 10000000
    let timer = Timer()
    global = 0
    let startTime = timer.getTime()
    for _ in 0..<count {
        Example.variableFoo(1, "2", true)
    }
    let midTime = timer.getTime()
    for _ in 0..<count {
    }
    let endTime = timer.getTime()
    let time = ((midTime - startTime) - (endTime - midTime)) / 1_000
    print("Static Function Call - RunNormalVariableFStaticFunc:\t"+String(time)+"\tms");
    return Int(time)
}
_ = RunNormalVariableFStaticFunc()

func RunNormalDefStaticCall() -> Int {
    let count = 10000000
    let timer = Timer()
    let resources = GenerateFakeRandomInteger();
    var i3 = 1
    let startTime = timer.getTime()
    let resourcesLength = resources.count
    for i in 0..<count {
        i3 = Example.defaultFoo(resources, i, i3, resourcesLength)
    }
    let midTime = timer.getTime()
    for _ in 0..<count {
    }
    let endTime = timer.getTime()
    let time = ((midTime - startTime) - (endTime - midTime)) / 1_000
    print("Static Function Call - RunNormalDefStaticCall:\t"+String(time)+"\tms");
    return Int(i3)
}
_ = RunNormalDefStaticCall()

print("Swift Method Call Is End, global value : \(global)")
