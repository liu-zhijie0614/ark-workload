import Glibc

let count = 1000

class Test{
    
    var arr = Array(repeating: 0, count: count)
    
    init(_ num1: Int,_ num2: Int){
        for i in 0..<count{
            switch (i % 4){
            case 0:
                arr[i] = add(num1, num2)
          break;
            case 1:
                arr[i] = subtract(num1, num2)
          break;
            case 2:
                arr[i] = multiply(num1, num2)
          break;
            case 3:
                arr[i] = divide(num1, num2)
          break;
            default:
                break
            }
            
        }
    }
    func add(_ num1: Int,_ num2: Int)  -> Int{
        return num1 + num2
    }
    func subtract(_ num1: Int,_ num2: Int) -> Int{
        return num1 - num2
    }
    func multiply(_ num1: Int,_ num2: Int) -> Int{
        return num1 * num2
    }
    func divide(_ num1: Int,_ num2: Int) -> Int{
        return num1 / num2
    }
}


class Benchmark{
    
    var test:Test?
    
    func runIteration(){
        let startTime = Timer().getTime()
        for i in 1..<121{
            for j in 1..<501{
                let test = Test(i,j)
                if i == 120 && j == 500{
                    self.test = test
                }
            }
        }
        let endTime = Timer().getTime()
        print("code-first-load: ms = \(endTime - startTime)")
        //DeBugLog("对象创建完成")
        test!.arr[0] = 0
    }
}

class Timer {
    private let CLOCK_REALTIME = 0
    private var time_spec = timespec()
    func getTime() -> Double {
        clock_gettime(Int32(CLOCK_REALTIME),&time_spec)
        return Double(time_spec.tv_sec * 1_000_000 + time_spec.tv_nsec / 1_000)/1000
    }
}
let isDebug : Bool = true
func DeBugLog(_ msg : Any , file: String = #file, line: Int = #line, fn: String = #function){
    if isDebug {
        let prefix = "\(msg)"
        print(prefix)
    }
}
Benchmark().runIteration();
