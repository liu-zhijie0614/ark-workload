/*
 * Copyright (C) 2007 Apple Inc.  All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY APPLE INC. ``AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL APPLE INC. OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
 * OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

import Glibc

let inDebug = false
func log(_ str: String) {
    if (inDebug) {
        print(str)
    }
}
func currentTimestamp13() -> Double {
   return Timer().getTime() / 1000.0
}

func run() {
    var bitwiseAndValue = 4294967296
    for i in 0..<600000 {
        bitwiseAndValue = bitwiseAndValue & i;
        if (inDebug && i % 100000 == 0) {
            //log("bitops-bitwise-and: i = \(i) bitwiseAndValue = \(bitwiseAndValue)")
        }
    }

    let result = bitwiseAndValue
    let expected = 0
    if result != expected {
        fatalError("ERROR: bad result: expected \(expected) but got \(result)")
    }
}

let startTime = currentTimestamp13()
for i in 0..<80 {
    let startTimeInLoop = currentTimestamp13()
    run()
    let endTimeInLoop = currentTimestamp13()
    //log("bitops-bitwise-and: ms = \((endTimeInLoop - startTimeInLoop)) i= \(i)")
}
let endTime = currentTimestamp13()
print("bitops-bitwise-and: ms = \(endTime - startTime)")

class Timer {
  private let CLOCK_REALTIME = 0
  private var time_spec = timespec()

  func getTime() -> Double {
      clock_gettime(Int32(CLOCK_REALTIME),&time_spec)
      return Double(time_spec.tv_sec * 1_000_000 + time_spec.tv_nsec / 1_000)
  }
}
