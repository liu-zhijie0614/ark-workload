import Glibc

var PI = 3.141592653589793
var SOLAR_MASS = 4 * PI * PI
var DAYS_PER_YEAR = 365.24

class Body {
    var x : Double
    var y : Double
    var z : Double
    var vx : Double
    var vy : Double
    var vz : Double
    var mass : Double
    
    init(_ x: Double, _ y: Double, _ z: Double, _ vx: Double, _ vy: Double, _ vz: Double, _ mass: Double) {
        self.x = x
	self.vx = vx
        self.y = y
	self.vy = vy
        self.z = z
        self.vz = vz
        self.mass = mass
    }
    
    func offsetMomentum(_ px : Double, _ py :Double, _ pz : Double) -> Self {
        vx = -px / SOLAR_MASS
        vy = -py / SOLAR_MASS
        vz = -pz / SOLAR_MASS
        return self
    }
    
}

func Jupiter()->Body {
    return Body(
        4.84143144246472090e+00,   //init data x,y,z 
        -1.16032004402742839e+00,
        -1.03622044471123109e-01,
        1.66007664274403694e-03 * DAYS_PER_YEAR,   //init data vx,vy,vz 
        7.69901118419740425e-03 * DAYS_PER_YEAR,
        -6.90460016972063023e-05 * DAYS_PER_YEAR,
        9.54791938424326609e-04 * SOLAR_MASS)
}


func Saturn()->Body {
    return Body(
        8.34336671824457987e+00,   //init data x,y,z 
        4.12479856412430479e+00,
        -4.03523417114321381e-01,
        -2.76742510726862411e-03 * DAYS_PER_YEAR,   //init data vx,vy,vz 
        4.99852801234917238e-03 * DAYS_PER_YEAR,
        2.30417297573763929e-05 * DAYS_PER_YEAR,
        2.85885980666130812e-04 * SOLAR_MASS)
}

func Uranus()->Body {
    return Body(
        1.28943695621391310e+01,   //init data x,y,z 
        -1.51111514016986312e+01,
        -2.23307578892655734e-01,
        2.96460137564761618e-03 * DAYS_PER_YEAR,  //init data vx,vy,vz
        2.37847173959480950e-03 * DAYS_PER_YEAR,
        -2.96589568540237556e-05 * DAYS_PER_YEAR,
        4.36624404335156298e-05 * SOLAR_MASS)
}

func Neptune()->Body {
    return Body(
        1.53796971148509165e+01,   //init data x,y,z 
        -2.59193146099879641e+01,
        1.79258772950371181e-01,
        2.68067772490389322e-03 * DAYS_PER_YEAR,  //init data vx,vy,vz
        1.62824170038242295e-03 * DAYS_PER_YEAR,
        -9.51592254519715870e-05 * DAYS_PER_YEAR,
        5.15138902046611451e-05 * SOLAR_MASS)
}

func Sun() -> Body {
    return  Body(0.0, 0.0, 0.0, 0.0, 0.0, 0.0, SOLAR_MASS)
}


class NBodySystem {
    var bodies: [Body]
    
    init(_ bodies: [Body]) {
        self.bodies = bodies
        var px = 0.0
        var py = 0.0
        var pz = 0.0
        
        let size = bodies.count
        for i in 0..<size {
            let b = bodies[i]
            let m = b.mass
            px += b.vx * m
            py += b.vy * m
            pz += b.vz * m
        }
        
        _ = self.bodies[0].offsetMomentum(px, py, pz)
    }
    
    func advance(by dt: Double) {
        var dx: Double, dy: Double, dz: Double, distance: Double, mag: Double
        let size = self.bodies.count
        
        for i in 0..<size {
            let bodyI = self.bodies[i]
            
            for j in i+1..<size {
                let bodyJ = self.bodies[j]
                dx = bodyI.x - bodyJ.x
                dy = bodyI.y - bodyJ.y
                dz = bodyI.z - bodyJ.z
                
                distance = sqrt(dx*dx + dy*dy + dz*dz)
                mag = dt / (distance * distance * distance)
                
                bodyI.vx -= dx * bodyJ.mass * mag
                bodyI.vy -= dy * bodyJ.mass * mag
                bodyI.vz -= dz * bodyJ.mass * mag
                
                bodyJ.vx += dx * bodyI.mass * mag
                bodyJ.vy += dy * bodyI.mass * mag
                bodyJ.vz += dz * bodyI.mass * mag
            }
        }

        for i in 0..<size {
            let body = self.bodies[i]
            body.x += dt * body.vx
            body.y += dt * body.vy
            body.z += dt * body.vz
        }
    }
    
    func energy()->Double {
        var dx : Double, dy : Double, dz : Double, distance : Double
        var e = 0.0
        let size = self.bodies.count
        
        for i in 0..<size {
            let bodyI = self.bodies[i]
            
            e += 0.5 * bodyI.mass *
            ( bodyI.vx * bodyI.vx
            + bodyI.vy * bodyI.vy
            + bodyI.vz * bodyI.vz )
            
            for j in i+1..<size {
                 let bodyJ = self.bodies[j]
                 dx = bodyI.x - bodyJ.x
                 dy = bodyI.y - bodyJ.y
                 dz = bodyI.z - bodyJ.z

                 distance = sqrt(dx*dx + dy*dy + dz*dz)
                 e -= (bodyI.mass * bodyJ.mass) / distance
              }
        }
        return e
    }
    
}

func run() {
    var ret: Double = 0
    
    var n: Int = 3
    while n <= 24 {
        let planets: [Body] = [Sun(),Jupiter(),Saturn(),Uranus(),Neptune()]
        let bodies = NBodySystem(planets)
        let max = n * 100
        ret += bodies.energy()

        for _ in 0..<max {
            bodies.advance(by: 0.01)
        }
        ret += bodies.energy()
        n *= 2
    }
    
    let expected: Double = -1.3524862408537381
    if ret != expected { print( "ERROR: bad result: expected \(expected) but got \(ret)") }
}


class Timer {
    private let CLOCK_REALTIME = 0
    private var time_spec = timespec()

    func getTime() -> Double {
        clock_gettime(Int32(CLOCK_REALTIME),&time_spec)
        return Double(time_spec.tv_sec * 1_000_000 + time_spec.tv_nsec / 1_000)
    }
}

/// @Benchmark
func runIterationTime() {
    let start = Timer().getTime()
    for _ in 0..<8 {
        run()
    }
    let end = Timer().getTime()
    let duration = (end - start) / 1000
    print("n-body: ms = \(duration)")
}

runIterationTime()
