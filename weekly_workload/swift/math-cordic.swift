/**
 ** Copyright (C) Rich Moore.  All rights reserved.
 *
 ** Redistribution and use in source and binary forms, with or without
 ** modification, are permitted provided that the following conditions
 ** are met:
 ** 1. Redistributions of source code must retain the above copyright
 **    notice, this list of conditions and the following disclaimer.
 ** 2. Redistributions in binary form must reproduce the above copyright
 **    notice, this list of conditions and the following disclaimer in the
 **    documentation and/or other materials provided with the distribution.
 *
 ** THIS SOFTWARE IS PROVIDED BY CONTRIBUTORS ``AS IS'' AND ANY
 ** EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 ** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 ** PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL APPLE INC. OR
 ** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 ** EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 ** PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 ** PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
 ** OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 ** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 ** OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 **/

/////. Start CORDIC
import Glibc

let AG_CONST = 0.6072529350;


func FIXED(_ X: Double) -> Double {
    return X * 65536.0
}

func FLOAT(_ X: Double) -> Double {
    return X / 65536.0
}

func DEG2RAD(_ X: Double) -> Double {
    return 0.017453 * (X)
}

var Angles = [
    FIXED(45.0), FIXED(26.565), FIXED(14.0362), FIXED(7.12502),
    FIXED(3.57633), FIXED(1.78991), FIXED(0.895174), FIXED(0.447614),
    FIXED(0.223811), FIXED(0.111906), FIXED(0.055953),
    FIXED(0.027977)
];

let Target = 28.027;

func cordicsincos(_ Target: Double) -> Double {
    var X: Double
    var Y: Double
    var TargetAngle: Double;
    var CurrAngle: Double;
    
    X = FIXED(AG_CONST);         /* AG_CONST * cos(0) */
    Y = 0;                       /* AG_CONST * sin(0) */
    
    TargetAngle = FIXED(Target);
    CurrAngle = 0;
    
    for step in 0..<12 {
        var NewX: Double;
        if (TargetAngle > CurrAngle) {
            NewX = X - Double((Int(Y) >> step));
            Y = Double(Int(X) >> step) + Y;
            X = NewX;
            CurrAngle += Angles[step];
        } else {
            NewX = X +  Double((Int(Y) >> step));
            Y = -Double((Int(X) >> step)) + Y;
            X = NewX;
            CurrAngle -= Angles[step];
        }
    }
    return FLOAT(X) * FLOAT(Y);
}

///// End CORDIC

var total = 0.0;

func cordic(_ runs: Int ) -> Double {
    let start = Timer().getTime()
    for _ in 0..<runs{
        total = total + cordicsincos(Target)
    }
    let end = Timer().getTime()
    return (end - start)/1000
}

/*
 *  @State
 *  @Tags Jetstream2
 */
class Benchmark {
    func run() {
    	 _ = cordic(25000)
    	 let expected = 10362.570468755888;
   	 if (total != expected){
            print("ERROR: bad result: expected  \(expected)   but got \(total)")
        }
    }
    
    /**
    * @Benchmark
    */
    func runIterationTime() {
    	let start = Timer().getTime()
    	for _ in 0..<80 {
   	    total = 0.0
            self.run()
       }
       let end = Timer().getTime()
       let duration = (end - start) / 1000
       print("math-cordic: ms = \(duration)")
}
}   


Benchmark().runIterationTime()


class Timer {
    private let CLOCK_REALTIME = 0
    private var time_spec = timespec()

    func getTime() -> Double {
        clock_gettime(Int32(CLOCK_REALTIME),&time_spec)
        return Double(time_spec.tv_sec * 1_000_000 + time_spec.tv_nsec / 1_000)
    }
}
