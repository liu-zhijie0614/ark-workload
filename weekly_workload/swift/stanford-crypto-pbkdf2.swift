import Glibc

/**
 * Arrays of bits, encoded as arrays of Numbers.
 * @namespace
 * @description
 * <p>
 * These objects are the currency accepted by SJCL's crypto functions.
 * </p>
 *
 * <p>
 * Most of our crypto primitives operate on arrays of 4-byte words internally,
 * but many of them can take arguments that are not a multiple of 4 bytes.
 * This library encodes arrays of bits (whose size need not be a multiple of 8
 * bits) as arrays of 32-bit words.  The bits are packed, big-endian, into an
 * array of words, 32 bits at a time.  Since the words are double-precision
 * floating point numbers, they fit some extra data.  We use this (in a private,
 * possibly-changing manner) to encode the number of bits actually  present
 * in the last word of the array.
 * </p>
 *
 * <p>
 * Because bitwise ops clear this out-of-band data, these arrays can be passed
 * to ciphers like AES which want arrays of words.
 * </p>
 */
class BitArray {
    
    /**
     * Make a partial word for a bit array.
     * @param {Number} len The number of bits in the word.
     * @param {Number} x The bits.
     * @param {Number} [_end=0] Pass 1 if x has already been shifted to the high side.
     * @return {Number} The partial word.
     */
    static func partial(_ len: Int, _ x: Int, _ end: Bool = false) -> Int {
        if len == 32 {
            return x
        } else {
            let result = (end ? x | 0 : initUInt32(x << (32 - len))) + len * 0x10000000000
            return result
        }
    }
    
    
    /**
     * Concatenate two bit arrays.
     * @param {bitArray} a1 The first array.
     * @param {bitArray} a2 The second array.
     * @return {bitArray} The concatenation of a1 and a2.
     */
    static func concat(_ a1: [Int], _ a2: [Int]) -> [Int] {
        if a1.count == 0 || a2.count == 0 {
            return a1 + a2
        }
        
        let last = a1[a1.count - 1], shift = BitArray.getPartial(last)
        if shift == 32 {
            return a1 + a2
        } else {
            return BitArray.shiftRight(a2, shift, last|0, Array(a1[0..<a1.count-1]))
        }
    }
    
    /**
     * Truncate an array.
     * @param {bitArray} a The array.
     * @param {Number} len The length to truncate to, in bits.
     * @return {bitArray} A new array, truncated to len bits.
     */
    static func clamp(_ a: [Int], _ len: Int) -> [Int] {
        var _a = a
        var _len = len
        if _a.count * 32 < _len { return _a }
        
        let sliceLength = Int(ceil(Double(_len) / 32))
        _a = Array(_a[0..<sliceLength])
        
        let l = _a.count
        _len = _len & 31
        
        if l > 0 && _len != 0 {
            _a[l-1] = BitArray.partial(_len, _a[l-1] & 0x80000000 >> (_len-1), true)
        }
        
        return _a
    }
    
    /**
     * Find the length of an array of bits.
     * @param {bitArray} a The array.
     * @return {Number} The length of a, in bits.
     */
    static func bitLength(_ a: [Int]) -> Int {
        let l = a.count
        var x: Int?
        if (l == 0 ){
            return 0
        }
        x = a[l - 1]
        return (l - 1) * 32 + BitArray.getPartial(x!)
    }
    
    /**
     * Get the number of bits used by a partial word.
     * @param {Number} x The partial word.
     * @return {Number} The number of bits used by the partial word.
     */
    static func getPartial(_ x: Int) -> Int {
        return Int(round(Double(x) / 0x10000000000) != 0 ? round(Double(x) / 0x10000000000) : 32)
    }
    
    /** Shift an array right.
     * @param {bitArray} a The array to shift.
     * @param {Number} shift The number of bits to shift.
     * @param {Number} [carry=0] A byte to carry in
     * @param {bitArray} [out=[]] An array to prepend to the output.
     * @private
     */
    static func shiftRight(_ a: [Int], _ shift: Int, _ carry: Int = 0, _ out: [Int] = []) -> [Int] {
        var last2 = 0
        var shift2: Int
        var _out = out
        var _carry = initUInt32(carry)
        var _shift = shift
        if (_out.count == 0) {
            _out = []
        }
        
        while (_shift >= 32) {
            _out.append(_carry)
            _carry = 0
            _shift -= 32
        }
        
        if (_shift == 0) {
            return _out + a
        }
        
        for i in 0..<a.count {
            _out.append((_carry | Int(initUInt32(a[i]) >>> _shift)))
            _carry = (a[i] << (32 - _shift)) & 0xFFFFFFFF
        }
        
        last2 = a.count == 0 ? 0 : a[a.count - 1]
        shift2 = (BitArray.getPartial(last2) != 0) ? 1 : 0
        _out.append(BitArray.partial(_shift + shift2 & 31, (_shift + shift2 > 32) ? _carry : _out.removeLast(), true))
        return _out
    }
   
}

/**
 * UTF-8 strings
 * @namespace
 */
class Utf8String {


    static func toBits(_ str: String) -> [Int] {
        
        var out:[Int] = [], i: Int = 0, tmp: Int = 0
        let charArr = Array(str)
        
        while i < charArr.count {
            tmp = tmp << 8 | Int(charArr[i].asciiValue!)
            if (i & 3) == 3 {
                out.append(tmp)
                tmp = 0
            }
            i += 1
        }

        if i & 3 != 0 {
            out.append(BitArray.partial(8 * (i & 3), tmp))
        }
        return out
    }
}

/**
 * Hexadecimal
 * @namespace
 */
class SJCLCodecHex {
    /** Convert from a bitArray to a hex string. */
    static func fromBits(_ arr: [Int]) -> String {
        var out = ""
        for i in 0..<arr.count {
            let hexValue = (arr[i] | 0) + 0xF00000000000
            out += String(hexValue, radix: 16).dropFirst(4)
        }
        let str = out.prefix(BitArray.bitLength(arr) / 4)
        return String(str)

    }
    /** Convert from a hex string to a bitArray. */
    static func toBits(_ str: String) -> [Int] {
        var out = [Int]()
        var strHex = str.replacing(try! Regex("\\s|0x"), with: "")
        let len = strHex.count
        strHex = strHex + "00000000"
        let charArr = Array(strHex)
        var i = 0
        while i < strHex.count {
            let hexValue = hexSwitch(Array(charArr[i..<i+8]))
            out.append(initUInt32(hexValue) ^ 0)
            i += 8
        }

        return BitArray.clamp(out, len * 4)
    }
    
    
}

func hexSwitch(_ charArr: Array<Character>) -> Int{
    var i = 0
    var sum = 0
    while i < charArr.count {
        let s = charArr[i]
        var n = 0
        switch s {
        case "A":
            n = 10
            break
        case "B":
            n = 11
            break
        case "C":
            n = 12
            break
        case "D":
            n = 13
            break
        case "E":
            n = 14
            break
        case "F":
            n = 15
            break
        default:
            n = n as Int
            break
        }
        sum = sum + n * Int(pow(Double(16), Double(charArr.count - i - 1)))
        i += 1
    }
    return sum
}

/** @fileOverview Javascript SHA-256 implementation.
 *
 * An older version of this implementation is available in the public
 * domain, but this one is (c) Emily Stark, Mike Hamburg, Dan Boneh,
 * Stanford University 2008-2010 and BSD-licensed for liability
 * reasons.
 *
 * Special thanks to Aldo Cortesi for pointing out several bugs in
 * this code.
 *
 * @author Emily Stark
 * @author Mike Hamburg
 * @author Dan Boneh
 */
class Sha256{
    /**
     * The SHA-256 initialization vector, to be precomputed.
     * @private
     */
    var _init:[Int] = []
    /*
     _init:[0x6a09e667,0xbb67ae85,0x3c6ef372,0xa54ff53a,0x510e527f,0x9b05688c,0x1f83d9ab,0x5be0cd19],
     */
    
    /**
     * The SHA-256 hash key, to be precomputed.
     * @private
     */
    var _key:[Int] = []
    /*
     _key:
     [0x428a2f98, 0x71374491, 0xb5c0fbcf, 0xe9b5dba5, 0x3956c25b, 0x59f111f1, 0x923f82a4, 0xab1c5ed5,
     0xd807aa98, 0x12835b01, 0x243185be, 0x550c7dc3, 0x72be5d74, 0x80deb1fe, 0x9bdc06a7, 0xc19bf174,
     0xe49b69c1, 0xefbe4786, 0x0fc19dc6, 0x240ca1cc, 0x2de92c6f, 0x4a7484aa, 0x5cb0a9dc, 0x76f988da,
     0x983e5152, 0xa831c66d, 0xb00327c8, 0xbf597fc7, 0xc6e00bf3, 0xd5a79147, 0x06ca6351, 0x14292967,
     0x27b70a85, 0x2e1b2138, 0x4d2c6dfc, 0x53380d13, 0x650a7354, 0x766a0abb, 0x81c2c92e, 0x92722c85,
     0xa2bfe8a1, 0xa81a664b, 0xc24b8b70, 0xc76c51a3, 0xd192e819, 0xd6990624, 0xf40e3585, 0x106aa070,
     0x19a4c116, 0x1e376c08, 0x2748774c, 0x34b0bcb5, 0x391c0cb3, 0x4ed8aa4a, 0x5b9cca4f, 0x682e6ff3,
     0x748f82ee, 0x78a5636f, 0x84c87814, 0x8cc70208, 0x90befffa, 0xa4506ceb, 0xbef9a3f7, 0xc67178f2],
     */
    
    /**
     * The hash's block size, in bits.
     * @constant
     */
    var blockSize = 512
    
    var _h:[Int] = []
    
    var _buffer:[Int] = []
    
    var _length = 0
    
    /**
     * Hash a string or an array of words.
     * @static
     * @param {bitArray|String} data the data to hash.
     * @return {bitArray} The hash value, an array of 16 big-endian words.
     */
    static func hash(_ data:Any) -> [Int]{
        return Sha256(nil).update(data).finalize()
    }
    /**
     * Context for a SHA-256 operation in progress.
     * @constructor
     */
    init(_ hash:Sha256? = nil) {
        if self._key.count == 0 {
            _precompute()
        }
        if hash != nil {
            _h = Array(hash!._h)
            _buffer = Array(hash!._buffer)
            _length = hash!._length
        } else {
            reset()
        }
    }
    func frac(_ x: Double) -> Int {
        var a = Int(Double(x - floor(x)) * 0x100000000)
        a = initUInt32(a)
        return a
    }
    /**
     * Function to precompute _init and _key.
     * @private
     */
    private func _precompute(){
        var i = 0, prime = 2, isPrime: Bool
        _init = [Int](repeating: 0, count: 8)
        _key = [Int](repeating: 0, count: 64)
    
        while (i < 64){
            isPrime = true
            var factor = 2
            while (factor * factor <= prime){
                if (prime % factor == 0) {
                    isPrime = false
                    break
                }
                factor += 1
            }
            if (isPrime) {
                if (i < 8) {
                    _init[i] = frac(pow(Double(prime), 1/2))
                }
                _key[i] = frac(pow(Double(prime), 1/3))
                i += 1
            }
            prime += 1
        }
        
    }
    /**
     * Reset the hash state.
     * @return this
     */
    func reset()  -> Self{
        self._h = self._init
        self._buffer = []
        self._length = 0
        return self
    }
    /**
     * Input several words to the hash.
     * @param {bitArray|String} data the data to hash.
     * @return this
     */
    func update(_ datas: Any) -> Self {
        var data:[Int] = []
        if datas is String{
            data = Utf8String.toBits(datas as! String)
        }else if datas is Array<Int>{
            data = datas as! [Int]
        }
        var i:Int = 0
        self._buffer = BitArray.concat(self._buffer, data)
        var b = self._buffer
        let ol = self._length
        self._length = self._length + BitArray.bitLength(data)
        let nl = self._length
        if nl > 9007199254740991{
            fatalError("INVALID Cannot hash more than 2^53 - 1 bits")
        }
        let c = b.map({(item) in initUInt32(item)})
        var j = 0
        i = 512 + ol - ((512 + ol) & 511)
        while i <= nl {
            self._block(Array(c[16 * j..<16 * (j + 1)]))
            j += 1
            i += 512
        }
        b.removeFirst(16*j)
        return self
    }
    
    /**
     * Complete hashing and output the hash value.
     * @return {bitArray} The hash value, an array of 8 big-endian words.
     */
    func finalize() -> [Int] {
        // Round out and push the buffer
        _buffer = BitArray.concat(_buffer, [BitArray.partial(1, 1)])
        
        // Round out the buffer to a multiple of 16 words, less the 2 length words.
        var i = _buffer.count + 2
        while ((i & 15) != 0) {
            _buffer.append(0)
            i += 1
        }
        
        // append the length
        let a = self._length / 0x100000000 | 0
        _buffer.append(a)
        _buffer.append(self._length)
        
        while (!(_buffer.count == 0)) {
            let chunk = Array(_buffer[..<16])
            _buffer.removeFirst(16)
            self._block(chunk)
        }
        self.reset()
        return _h
    }
    
    
    /**
     * Perform one cycle of SHA-256.
     * @param {Uint32Array|bitArray} w one block of words.
     * @private
     */
    private func _block(_ input:[Int]) {
        var w = input
        var tmp, a, b: Int
        let k = self._key
        var h0 = _h[0], h1 = _h[1], h2 = _h[2], h3 = _h[3]
        var h4 = _h[4], h5 = _h[5], h6 = _h[6], h7 = _h[7]
        
        /* Rationale for placement of |0 :
         * If a value can overflow is original 32 bits by a factor of more than a few
         * million (2^23 ish), there is a possibility that it might overflow the
         * 53-bit mantissa and lose precision.
         *
         * To avoid this, we clamp back to 32 bits by |'ing with 0 on any value that
         * propagates around the loop, and on the hash state h[].  I don't believe
         * that the clamps on h4 and on h0 are strictly necessary, but it's close
         * (for h4 anyway), and better safe than sorry.
         *
         * The clamps on h[] are necessary for the output to be correct even in the
         * common case and for short inputs.
         */
        for i in 0..<64 {
            // load up the input word for this round
            if i < 16 {
                tmp = w[i]
            } else {
                a = initUInt32(w[(i + 1) & 15])
                b = initUInt32(w[(i + 14) & 15])
                
                let value1 = Int(a >>> 7) ^ Int(a >>> 18) ^ Int(a >>> 3) ^ a << 25 ^ a << 14
                let value2 = Int(b >>> 17) ^ Int(b >>> 19) ^ Int(b >>> 10) ^ b << 15 ^ b << 13
                let value3 = w[i & 15] + w[(i + 9) & 15]
                
                w[i & 15] = initUInt32(value1 + value2 + value3)
                tmp = w[i & 15]
            }
            h4 = initUInt32(h4)
            
            let value = Int(h4 >>> 6) ^ Int(h4 >>> 11) ^ Int(h4 >>> 25) ^ h4<<26 ^ h4<<21  ^ h4<<7
            tmp = Int(initUInt32(tmp + h7 + value +  (h6 ^ h4 & (h5 ^ h6)) + k[i]))
            // shift register
            h7 = initUInt32(h6)
            h6 = initUInt32(h5)
            h5 = initUInt32(h4)
            h4 = initUInt32(h3 + tmp)
            h3 = initUInt32(h2)
            h2 = initUInt32(h1)
            h1 = initUInt32(h0)
            
            h0 = initUInt32(tmp +  ((h1 & h2) ^ (h3 & (h1 ^ h2))) + (Int(h1 >>> 2) ^ Int(h1 >>> 13) ^ Int(h1 >>> 22) ^ h1 << 30 ^ h1 << 19 ^ h1 << 10))
        }
        _h[0] = initUInt32(_h[0] + h0)
        _h[1] = initUInt32(_h[1] + h1)
        _h[2] = initUInt32(_h[2] + h2)
        _h[3] = initUInt32(_h[3] + h3)
        _h[4] = initUInt32(_h[4] + h4)
        _h[5] = initUInt32(_h[5] + h5)
        _h[6] = initUInt32(_h[6] + h6)
        _h[7] = initUInt32(_h[7] + h7)
        _init = _h
    }
}
/** HMAC with the specified hash function.
 * @constructor
 * @param {bitArray} key the key for HMAC.
 * @param {Object} [Hash=sjcl.hash.sha256] The hash function to use.
 */

class HMAC {
    var hash: Sha256
    var baseHash: [Sha256]
    var resultHash: Sha256
    var exKey:[[Int]] = [[],[]]
    var updated: Bool = false

    init(_ key: [Int] = [], hash: Sha256 = Sha256()) {
        self.hash = hash
        self.baseHash = [Sha256(), Sha256()]
        let bs = hash.blockSize / 32
        self.exKey[0] = [Int](repeating: 0, count: bs)
        self.exKey[1] = [Int](repeating: 0, count: bs)
        var _key = key
        if key.count > bs {
            _key = Sha256.hash(_key)
        }

        for i in 0..<bs {
            if i < key.count {
                self.exKey[0][i] = _key[i] ^ 0x36363636
                self.exKey[1][i] = _key[i] ^ 0x5C5C5C5C
            }else{
                self.exKey[0][i] = 0 ^ 0x36363636
                self.exKey[1][i] = 0 ^ 0x5C5C5C5C
            }

        }

        self.baseHash[0].update(exKey[0])
        self.baseHash[1].update(exKey[1])
        self.resultHash = self.baseHash[0]
    }

    func encrypt(_ data:[Int]) -> [Int] {
        if !updated  {
            self.update(data)
            return digest()
        } else {
            fatalError("encrypt on already updated hmac called!")

        }
    }

    func reset() {
        self.resultHash = Sha256(self.baseHash[0])
        self.updated = false
    }

    func update(_ data: [Int]) {
        self.updated = true
        self.resultHash._buffer = []
        self.resultHash.update(data)
    }

    func digest() -> [Int] {

        let w = self.resultHash.finalize()
        let b = Sha256(baseHash[1])
        b._buffer = []
        let c = b.update(w)

        let finalResult = c.finalize()
        reset()
        return finalResult
    }
}
class Pbkdf2{
    static func pbkdf2(_ password: Any, _ salt: Any, _ count: Int = 10000, _ length: Int? = 0, _ Prff:HMAC? = nil) -> [Int] {

        if length == nil || count < 0 {
            fatalError("invalid params to pbkdf2")
            
        }
        var passwordArr:[Int] = []
        var saltArr:[Int] = []
        if password is String {
            let arr = Utf8String.toBits(password as! String)
            var i = 0
            while i < arr.count {
                let n = arr[i]
                passwordArr.append(initUInt32(n))
                i += 1
            }
        }else{
            let arr = password as! [Int]
            var i = 0
            while i < arr.count {
                let n = arr[i]
                passwordArr.append(initUInt32(n))
                i += 1
            }

        }
        if salt is String {

            let arr = Utf8String.toBits(salt as! String)
            var i = 0
            while i < arr.count {
                let n = arr[i]
                saltArr.append(initUInt32(n))
                i += 1
            }

        }else{
            let arr = salt as! [Int]
            var i = 0
            while i < arr.count {
                let n = arr[i]
                saltArr.append(initUInt32(n))
                i += 1
            }
        }


        var u:[Int],ui:[Int],out:[Int] = []

        var k = 1
        while 32 * out.count < (length == 0 ? 1 : length)! {
            u = HMAC(passwordArr).encrypt(BitArray.concat(saltArr, [k]))
            ui = HMAC(passwordArr).encrypt(BitArray.concat(saltArr, [k]))
    
            for _ in 1..<count {
                
                ui = HMAC(passwordArr).encrypt(ui)
                for j in 0..<ui.count {
                    u[j] ^= ui[j]
                }
            }

            out += u
            k += 1

        }
        if length != 0 {
            out = BitArray.clamp(out, length!)
        }
        //debugLog("pbkdf2-out", out)
        return out
    }
}

class Benchmark {
    // @Benchmark
    func runIteration() {
        let startTime = Timer().getTime()
        for _ in  0..<20{
            let password = "b4db33f";
            let salt = "BA DB EE FB AD BE EF 00";
            let count = 2048;
            let strArr = SJCLCodecHex.toBits(salt)
            //debugLog("strArr", strArr)
            let output = SJCLCodecHex.fromBits(Pbkdf2.pbkdf2(password, strArr, count));
            //debugLog("output", output)
            if (output != "5436c4c0d73e425a521f4a5a13e0fffe5cd957a823f435385fa0a75068a26803"){
                fatalError("Bad output")
            }
        }
        let endTime = Timer().getTime()
        print("stanford-crypto-pbkdf2: ms = " + String(endTime - startTime))
    }
}

fileprivate func debugLog(_ str: String,_ object: Any) {
    let isLog = false
    if isLog {
        print("\(str): \(object)")
    }
}



func initUInt32(_ a: Int) -> Int{
    var tmp = a
    if tmp > Int32.max {
        let max = UInt32.max
        tmp = Int(tmp) % (Int(max) + 1)
        if tmp > Int32.max{
            tmp = tmp - Int(max) - 1
        }
    }else if tmp < Int32.min{
        let max = UInt32.max
        tmp = Int(tmp) % (Int(max) + 1)
        if tmp < Int32.min {
            tmp = tmp + Int(max) + 1
        }
    }
    return tmp
}


Benchmark().runIteration()

class Timer {
    private let CLOCK_REALTIME = 0
    private var time_spec = timespec()
    func getTime() -> Double {
        clock_gettime(Int32(CLOCK_REALTIME),&time_spec)
        return Double(time_spec.tv_sec * 1_000_000 + time_spec.tv_nsec / 1_000)/1000
    }
}

infix operator >>> : BitwiseShiftPrecedence

func >>> (lhs: Int, rhs: Int) -> Int32 {
    return Int32(bitPattern: UInt32(bitPattern: Int32(lhs)) >> UInt32(Int32(rhs)))
}
