import Glibc

class Timer {
    private let CLOCK_REALTIME = 0
    private var start_timespec = timespec()
    private var end_timespec = timespec()
    private var time_spec = timespec()
    func start() {
        clock_gettime(Int32(CLOCK_REALTIME),&start_timespec)
    }
    
    func stop() -> Double {
        clock_gettime(Int32(CLOCK_REALTIME),&end_timespec)
        let start_time = Double(start_timespec.tv_sec * 1_000_000 + start_timespec.tv_nsec / 1_000)
        let end_time = Double(end_timespec.tv_sec * 1_000_000 + end_timespec.tv_nsec / 1_000)
        let time = end_time - start_time
        return time / 1_000
    }
    func getTime() -> Double {
        clock_gettime(Int32(CLOCK_REALTIME),&time_spec)
        return Double(time_spec.tv_sec * 1_000_000 + time_spec.tv_nsec / 1_000)
    }
}

func GenerateFakeRandomInteger() -> [Int] {
    let resource: [Int] = [12, 43, 56, 76, 89, 54, 45, 32, 35, 47, 46, 44, 21, 37, 84]
    return resource;
}

func GenerateRandoms() -> [Int]{
    let result = Array(repeating: Int.random(in: 1..<3), count: 2)
    return result
}
var generaterandoms = GenerateRandoms()

func TimeGenerateRandoms() -> Double{
    let timer = Timer()
    timer.start()
    let tmp = 1
    if generaterandoms[tmp%2] != 0 {

    }
	let time = timer.stop()
    return time
}
class Obj {
    var value: Int = 0
    init(value: Int) {
        self.value = value
    }
}

func GenerateFakeRandomObject() -> [Obj] {
    let resource: [Obj] = [Obj](repeating: Obj(value: Int.random(in: 1..<10)), count: 15)
    return resource
}

var global = 0
var arr : [Int] = [12, 43, 56, 76, 89, 54, 45, 32, 35, 47, 46, 44, 21, 37, 84]
/***************** With parameters *****************/

func Foo(resources: [Int], i: Int, i3: Int, resourcesLength: Int) ->Int {
    var i4 = i3;
    if ((resources[i % Int(i3) & (resourcesLength - 1)] & 1) == 0) {
        i4 += 1;
    } else {
        i4 += 2;
    }
    return i4;
}


/***** Without parameters *****/

func parameterlessFoo() -> Obj {
    var res: [Obj] = [Obj](repeating: Obj(value: Int.random(in: 1..<10)), count: 5)
    let resources = GenerateFakeRandomObject()
    for i in 0..<200 {
        res[i % 5] = resources[i % 15]
    }
    return res[1]
}
/***************************** Default  parameters *****************************/

func defaultFoo(_ resources: [Int]! = arr, _ i: Int! = 2, _ i3: Int! = 1, _ resourcesLength: Int! = 15)->Int{
    var i4 = 1;
    if ((resources[i % Int(i3) & (resourcesLength - 1)] & 1) == 0) {
        i4 += 1;
    } else {
        i4 += 2;
    }
    return i4;
}
/********************* Different  parameters *********************/

func differentFoo(_ resources: [Int], _ i: Int, _ i3: Double, _ resourcesLength: Int) ->Double{
    var i4 = i3;
    if ((resources[i % Int(i3) & (resourcesLength - 1)] & 1) == 0) {
        i4 += 1.1;
    } else {
        i4 += 2.1;
    }
    return i4;
}
/************************* Variable  parameters *************************/

func variableFoo(_ a: Int?, _ b: String?, _ c: Bool?) {
    arr[global] += 1
}
/***************************** ...Args  parameters *****************************/

func argsFoo(_ args: Int...) {
    global += 1
}
/***************** With parameters *****************/
// Normal call
func RunNormalCall() -> Int {
    let count = 10000000
    let resources = GenerateFakeRandomInteger();
    var i3 = 1
    let timer = Timer()
    let startTime = timer.getTime()
    let resourcesLength = resources.count
    for i in 0..<count {
        i3 = Foo(resources: resources, i: i, i3: i3, resourcesLength:resourcesLength)
    }
    let midTime = timer.getTime()
    for _ in 0..<count {
    }
    let endTime = timer.getTime()
    let time = ((midTime - startTime) - (endTime - midTime)) / 1_000
    print("Function Call - RunNormalCall:\t"+String(time)+"\tms");
    return i3
}
_ = RunNormalCall()
/***** Without parameters *****/
// No parameter function call
func RunParameterlessCall() -> Obj {
    let count = 10000
    var i3 = Obj(value:1);
    let timer = Timer()
    global = 0
    let startTime = timer.getTime()
    for _ in 0..<count {
        i3 = parameterlessFoo()
    }
    let midTime = timer.getTime()
    for _ in 0..<count {
    }
    let endTime = timer.getTime()
    let time = ((midTime - startTime) - (endTime - midTime)) / 1_000
    print("Function Call - RunParameterlessCall:\t"+String(time)+"\tms");
    return i3
}
_ = RunParameterlessCall()
/************ Default  parameters *************/
// Normal call
func RunNormalDefCall() -> Int {
    let count = 10000000
    let timer = Timer()
    var i3 = 1
    let resources = GenerateFakeRandomInteger();
    let resourcesLength = resources.count
    global = 0
    let startTime = timer.getTime()
    for i in 0..<count {
        i3 = defaultFoo(resources, i, i3, resourcesLength)
    }
    let midTime = timer.getTime()
    for _ in 0..<count {
    }
    let endTime = timer.getTime()
    let time = ((midTime - startTime) - (endTime - midTime)) / 1_000
    print("Function Call - RunNormalDefCall:\t"+String(time)+"\tms");
    return i3
}
_ = RunNormalDefCall()
/********************* Different  parameters *********************/
// Normal call
func RunNormalDifferentCall() -> Int {
    let count = 10000000
    let timer = Timer()
    let resources = GenerateFakeRandomInteger();
    let resourcesLength = resources.count
    var i3 = 1.1
    global = 0
    let startTime = timer.getTime()
    for i in 0..<count {
        i3 = differentFoo(resources, i, i3, resourcesLength)
    }
    let midTime = timer.getTime()
    for _ in 0..<count {
    }
    let endTime = timer.getTime()
    let time = ((midTime - startTime) - (endTime - midTime)) / 1_000
    print("Function Call - RunNormalDifferentCall:\t"+String(time)+"\tms");
    return Int(time)
}
_ = RunNormalDifferentCall()
/************************* Variable  parameters *************************/
// Normal call
func RunNormalVariableFCall() -> Int {
    let count = 10000000
    let timer = Timer()
    global = 1
    let startTime = timer.getTime()
    for _ in 0..<count {
        variableFoo(1, "1", true)
    }
    let midTime = timer.getTime()
    for _ in 0..<count {
    }
    let endTime = timer.getTime()
    let time = ((midTime - startTime) - (endTime - midTime)) / 1_000
    print("Function Call - RunNormalVariableFCall:\t"+String(time)+"\tms");
    return Int(time)
}
_ = RunNormalVariableFCall()

print("Swift Method Call Is End, global value : \(global)")