import Glibc

func fannkuch(_ n : Int)-> Int{
    var check = 0
    var perm = Array(repeating: 0, count: n)
    var perm1 = Array(repeating: 0, count: n)
    var count = Array(repeating: 0, count: n)
    var maxPerm = Array(repeating: 0, count: n)
    var maxFlipsCount = 0
    let m = n - 1
    
    for i in 0..<n {
        perm1[i] = i
    }
    var r = n
    
    while(true){

        if check < 30 {
            var s = ""
            for i in 0..<n {
                s = s + "\(perm1[i]+1)"
                check += 1
            }
        }
        
        while(r != 1){
            count[r - 1] = r
            r -= 1
            
        }
        
        if (!(perm1[0] == 0 || perm1[m] == m)) {
            for i in 0..<n {
                perm[i] = perm1[i]
            }

            var flipsCount = 0
            var k = 0

             while (!(perm[0] == 0)) {
                k = perm[0]
                let k2 = (k + 1) >> 1
                 for i in 0..<k2 {
                   let temp = perm[i]
                     perm[i] = perm[k - i]
                     perm[k - i] = temp
                }
                flipsCount += 1
             }

             if (flipsCount > maxFlipsCount) {
                maxFlipsCount = flipsCount
                 for i in 0..<n {
                     maxPerm[i] = perm1[i]
                 }
             }
          }
        
        while (true) {
            if r == n {
                return maxFlipsCount
            }
            let perm0 = perm1[0]
             var i = 0
             while (i < r) {
                 let j = i + 1
                perm1[i] = perm1[j]
                i = j
             }
             perm1[r] = perm0

            count[r] = count[r] - 1
            if (count[r] > 0) {
                break
            }
             r += 1
          }
    }
}

func run(){
    let n = 8
    let ret = fannkuch(n)

    let expected = 22
    if (ret != expected){
        print("ERROR: bad result: expected \(expected) but got \(ret)")
    }
}

class Timer {
    private let CLOCK_REALTIME = 0
    private var time_spec = timespec()

    func getTime() -> Double {
        clock_gettime(Int32(CLOCK_REALTIME),&time_spec)
        return Double(time_spec.tv_sec * 1_000_000 + time_spec.tv_nsec / 1_000)
    }
}

/// @Benchmark
func runIterationTime() {
    let start = Timer().getTime()
    for _ in 0..<80 {
        run()
    }
    let end = Timer().getTime()
    let duration = (end - start) / 1000
    print("access-fannkuch: ms = \(duration)")
}

runIterationTime()
