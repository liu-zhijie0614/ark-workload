/*
 * Copyright (C) 2004 Baron Schwartz <baron at sequent dot org>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License for more
 * details.
 */

import Glibc


class MyDate {
    public func timeIntervalSince1970() -> Int {
        return 1167585071
    }
    public func timeStamp() -> Int{
        return 1167585071
    }
    public func milliStamp() -> Int {
        return  1167585071000
    }
    public func getDate() -> Int {
        //Returns a day of the month
        return 1
    }
    public func getTimezoneOffset() -> Int{
        //Returns the time difference between Greenwich Mean Time and local time:
        return  -480
    }
    public func getYear() -> Int{
        //
        return 2007
    }
    public  func getFullYear() -> Int {
        //return a year
        return 2007
    }
    public func getMonth() -> Int{
        //return a month
        return 1
    }
    public func getHours() -> Int{
        //Returns the hour field of the time based on the specified time:
        return 1
    }
    public func getMinutes() -> Int{
        //Returns the minute field of the time based on the specified time:
        return 11
    }
    public  func getSeconds() -> Int {
        //Returns the second field of the time based on the specified time:
        return 11
    }
    public  func getDay() -> Int {
        //Returns the number of a day of the week.
        return 1
    }
    
    public func getTime() -> Int{
        //Returns the number of milliseconds between January 1, 1970:
        let timeInterval = self.timeIntervalSince1970()
        return   timeInterval * 1000
    }
    public  func setTime(_ millisedcond: Int)  {
        //Method to set the Date object in milliseconds.
    }
    
}
class DateClass: MyDate {
    
    var daysInMonth = [31,28,31,30,31,30,31,31,30,31,30,31];
    let monthNames = [
        "January",
        "February",
        "March",
        "April",
        "May",
        "June",
        "July",
        "August",
        "September",
        "October",
        "November",
        "December"];
    let dayNames = [
        "Sunday",
        "Monday",
        "Tuesday",
        "Wednesday",
        "Thursday",
        "Friday",
        "Saturday"];
    init(_ str: String) {
        super.init()
        
    }
    func dateFormat(_ format: String) -> String{
        return createNewFormat(format)
    }
    
    func createNewFormat(_ format: String) -> String {
        var result = ""
        var special = false
        let arr = stringToArray(format)
        for ch in arr {
            if (!special && ch == "\\"){
                special = true
            }else if (special) {
                special = false
                result += ch
            }else  {
                let value = getFormatCode(ch)
                result += value
            }
        }
        return result
    }
    
    func getFormatCode(_ character: String) -> String {
        switch (character) {
        case "d":
            return leftPad(getDate(), 2, "0")
        case "D":
            return substring("\(dayNames[getDay()])", 0, 2)
        case "j":
            return "\(getDate())"
        case "l":
            return dayNames[getDay()]
        case "S":
            return getSuffix()
        case "w":
            return "\(getDay())"
        case "z":
            return "\(getDayOfYear())";
        case "W":
            return "\(getWeekOfYear())";
        case "F":
            return "\(monthNames[getMonth() - 1])"
        case "m":
            return "\(leftPad(getMonth(), 2, "0"))";
        case "M":
            return substring("\(monthNames[getMonth()])", 0, 2)
        case "n":
            return "\(getMonth() + 1)";
        case "t":
            return "\(getDaysInMonth())";
        case "L":
            return "\(isLeapYear() ? 1 : 0))";
        case "Y":
            return "\(getFullYear())";
        case "y":
            return substring("\(getFullYear())", 2, 3)
        case "a":
            return "\(getHours() < 12 ? "am" : "pm")";
        case "A":
            return "\(getHours() < 12 ? "AM" : "PM")";
        case "g":
            return "\((((getHours()%12) != 0) ? getHours() % 12 : 12) )";
        case "G":
            return "\(getHours())";
        case "h":
            return leftPad(((getHours()%12) != 0) ? getHours() % 12 : 12,  2,  "0")
        case "H":
            return leftPad(getHours(), 2, "0");
        case "i":
            return leftPad(getMinutes(), 2, "0")
        case "s":
            return leftPad(getSeconds(),2 , "0");
        case "O":
            return getGMTOffset();
        case "Z":
            return "\(getTimezoneOffset() * -60)";
        default:
            return character;
        }
    }
    func getGMTOffset() -> String {
        return (getTimezoneOffset() > 0 ? "-" : "+")
        + leftPad(Int(floor(Double(getTimezoneOffset() / 60))), 2, "0")
        + leftPad(getTimezoneOffset() % 60,  2, "0");
    }
    
    func getDayOfYear() -> Int {
        var num = 0;
        daysInMonth[1] = isLeapYear() ? 29 : 28;
        for i in 0..<getMonth() {
            num += daysInMonth[i]
        }
        return num + getDate() - 1;
    }
    
    func getWeekOfYear() -> String {
        // Skip to Thursday of this week
        let now = getDayOfYear() + (4 - getDay());
        // Find the first Thursday of the year
        let jan1 =  DateClass("\(self.getFullYear()), 0, 1");
        let then = (7 - jan1.getDay() + 4);
        return leftPad(((now - then) / 7) + 1, 2,"0");
    }
    
    func isLeapYear() -> Bool {
        let year = getFullYear();
        return ((year & 3) == 0 && (((year % 100) != 0) || (year % 400 == 0 && (year != 0))));
    }
    func getDaysInMonth() -> Int {
        daysInMonth[1] = isLeapYear() ? 29 : 28;
        return daysInMonth[getMonth()];
    }
    
    func getSuffix() -> String {
        switch (getDate()) {
        case 1,
            21,
            31:
            return "st";
        case 2,
            22:
            return "nd";
        case 3,
            23:
            return "rd";
        default:
            return "th";
        }
    }
    func leftPad(_ val: Int, _ size: Int, _ ch: String?) -> String {
        var ch = ch
        var result = "\(val)"
        if ch == nil {
            ch = " "
        }
        while (result.count < size) {
            result = ch! + result
        }
        return result
    }

    func stringToArray(_ str: String) -> [String] {
        var arr = [String]()
        for i in str.indices {
            arr.append("\( str[i])")
        }
        return arr
    }
    
    func substring(_ str: String ,_ startOffset: Int, _ endOffset: Int)-> String {
        let startIndex = str.index(str.startIndex, offsetBy: startOffset)
        let endIndex = str.index(str.startIndex, offsetBy: endOffset)
        let substring = str[startIndex...endIndex]
        return String(substring)
    }
}

func run() {
    let date =  DateClass("1/1/2007 01:11:11")
    for _ in 0..<500 {
        let shortFormat = date.dateFormat("Y-m-d")
        let longFormat = date.dateFormat("l, F d, Y g:i:s A")
        date.setTime(date.getTime()  + 84266956)
    }
}

func runIterationTime() {
    let start = Timer().getTime()
    for _ in 0..<80 {
        run()
    }
    let end = Timer().getTime()
    let duration = (end - start) / 1000
    print("date-format-xparb: ms = \(duration)")
}

runIterationTime()

class Timer {
    private let CLOCK_REALTIME = 0
    private var time_spec = timespec()

    func getTime() -> Double {
        clock_gettime(Int32(CLOCK_REALTIME),&time_spec)
        return Double(time_spec.tv_sec * 1_000_000 + time_spec.tv_nsec / 1_000)
    }
}
//interface Patterns  {
//  ISO8601LongPattern:"Y-m-d H:i:s",
//  ISO8601ShortPattern:"Y-m-d",
//  ShortDatePattern: "n/j/Y",
//  LongDatePattern: "l, F d, Y",
//  FullDateTimePattern: "l, F d, Y g:i:s A",
//  MonthDayPattern: "F d",
//  ShortTimePattern: "g:i A",
//  LongTimePattern: "g:i:s A",
//  SortableDateTimePattern: "Y-m-d\\TH:i:s",
//  UniversalSortableDateTimePattern: "Y-m-d H:i:sO",
//  YearMonthPattern: "F, Y"};
