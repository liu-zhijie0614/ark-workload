import Glibc

func run() {
    let start = Timer().now
    for _ in 0 ..< 20 {
        runIteration()
    }
    let end = Timer().now
    print("box2d: ms = \(end - start)")
}

func runIteration() {
    runBox2D()
}

func runBox2D() {
    let world = MakeNewWorld()
    for _ in 0..<20 {
        world.step(1.0/60.0, 10, 3)
//        DeBugLog("position_y:\(world.m_bodyList?.position.y ?? 0.0)")
    }
}

func MakeNewWorld() -> b2World {

    let gravity = b2Vec2(0.0, -10.0)
    let world = b2World(gravity)

    let shape = b2PolygonShape()
    shape.setAsEdge(b2Vec2(-40.0, 0), b2Vec2(40.0, 0))
    shape.radius = 0

    let fd = b2FixtureDef()
    fd.density = 0.0
    fd.shape = shape

    let bd = b2BodyDef()
    bd.type = .staticBody
    let ground = world.createBody(bd)
    ground.createFixture(fd)

    let a = 0.5
    let shape2 = b2PolygonShape()
    shape2.setAsBox(a, a)
    shape2.radius = 0

    var x = b2Vec2(-7.0, 0.75)
    var y = b2Vec2()

    let deltaX = b2Vec2(0.5625, 1)
    let deltaY = b2Vec2(1.125, 0.0)

    for _ in 0..<10 {
        y.set(x.x, x.y)
        for _ in 0..<5 {
            let fd = b2FixtureDef()
            fd.density = 5.0
            fd.shape = shape2
            let bd = b2BodyDef()
            bd.type = .dynamicBody
            bd.position.set(y.x, y.y)
            let body = world.createBody(bd)
            body.createFixture(fd)
            addEqual(&y, deltaY)
        }
        addEqual(&x, deltaX)
    }
    return world
}

let isDebug : Bool = false
func DeBugLog(_ msg : Any , file: String = #file, line: Int = #line, fn: String = #function){
    if isDebug {
        let prefix = "\(msg)"
        print(prefix)
    }
}

class Timer {
    private let CLOCK_REALTIME = 0
    private var time_spec = timespec()
    func getTime() -> Double {
        clock_gettime(Int32(CLOCK_REALTIME),&time_spec)
        return Double(time_spec.tv_sec * 1_000_000 + time_spec.tv_nsec / 1_000)
    }
}
extension Timer {
    var now: Double {
        return getTime()/1000.0
    }
}
