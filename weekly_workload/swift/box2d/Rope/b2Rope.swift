import Glibc

public class b2RopeDef {
    var vertices: [b2Vec2]!
    var count: Int
    var masses: [Double]!
    var gravity: b2Vec2
    var damping: Double
    var k2: Double
    var k3: Double
    init() {
        vertices = nil
        count = 0
        masses = nil
        gravity = b2Vec2()
        damping = 0.1
        k2 = 0.9
        k3 = 0.1
    }
    
}

open class b2Rope {
    var m_count: Int
    var m_ps: [b2Vec2]! = nil
    var m_p0s: [b2Vec2]! = nil
    var m_vs: [b2Vec2]! = nil
    var m_ims: [Double]! = nil
    var m_Ls: [Double]! = nil
    var m_as: [Double]! = nil
    var m_gravity: b2Vec2
    var m_damping: Double
    var m_k2: Double
    var m_k3: Double
    init() {
        m_count = 0
        m_ps = []
        m_p0s = []
        m_vs = []
        m_ims = []
        m_Ls = []
        m_as = []
        m_gravity = b2Vec2(0.0, 0.0)
        m_damping = 0.1
        m_k2 = 1.0
        m_k3 = 0.1
    }
    func initialize(_ def: b2RopeDef) {
        m_count = def.count
        for _ in (0..<m_count) {
            m_ps.append(b2Vec2())
            m_p0s.append(b2Vec2())
            m_vs.append(b2Vec2())
            m_ims.append(Double(0.0))
        }
        for i in 0 ..< m_count {
            m_ps[i] = def.vertices[i]
            m_p0s[i] = def.vertices[i]
            m_vs[i].setZero()
            let m = def.masses[i]
            if m > 0.0 {
                m_ims[i] = 1.0 / m
            } else {
                m_ims[i] = 0.0
            }
        }
        let count2 = m_count - 1
        let count3 = m_count - 2
        for _ in (0..<count2) {
            m_Ls.append(Double(0.0))
        }
        for _ in (0..<count3) {
            m_as.append(Double(0.0))
        }
        for i in 0 ..< count2 {
            let p1 = m_ps[i]
            let p2 = m_ps[i+1]
            m_Ls[i] = b2Distance(p1, p2)
        }
        for i in 0 ..< count3 {
            let p1 = m_ps[i]
            let p2 = m_ps[i + 1]
            let p3 = m_ps[i + 2]
            let d1 = subtract(p2, p1)
            let d2 = subtract(p3, p2)
            let a = b2Cross(d1, d2)
            let b = b2Dot22(d1, d2)
            m_as[i] = b2Atan2(a, b)
        }
        m_gravity = def.gravity
        m_damping = def.damping
        m_k2 = def.k2
        m_k3 = def.k3
    }
    
    func step(_ h: Double, _ iterations: Int) {
        if h == 0.0 {
            return
        }
        let d = exp(-h * m_damping)
        for i in 0 ..< m_count {
            m_p0s[i] = m_ps[i]
            if m_ims[i] > 0.0 {
                addEqual(&m_vs[i], multM(m_gravity, h))
            }
            mulEqual(&m_vs[i], d)
            addEqual(&m_ps[i], multM(m_vs[i], h))
        }
        for _ in 0 ..< iterations {
            solveC2()
            solveC3()
            solveC2()
        }
        let inv_h = 1.0 / h
        for i in 0 ..< m_count {
            m_vs[i] = multM(subtract(m_ps[i], m_p0s[i]), inv_h)
        }
    }
    
    var vertexCount: Int {
        return m_count
    }
    
    var vertices: [b2Vec2] {
        return m_ps
    }
    
    func setAngle(_ angle: Double) {
        let count3 = m_count - 2
        for i in 0 ..< count3 {
            m_as[i] = angle
        }
    }
    
    func solveC2() {
        let count2 = m_count - 1
        for i in 0 ..< count2 {
            var p1 = m_ps[i]
            var p2 = m_ps[i + 1]
            var d = subtract(p2, p1)
            let L = d.normalize()
            let im1 = m_ims[i]
            let im2 = m_ims[i + 1]
            if im1 + im2 == 0.0 {
                continue
            }
            let s1 = im1 / (im1 + im2)
            let s2 = im2 / (im1 + im2)
            subtractEqual(&p1, multM(d, m_k2 * s1 * (m_Ls[i] - L)))
            addEqual(&p2, multM(d, m_k2 * s2 * (m_Ls[i] - L)))
            m_ps[i] = p1
            m_ps[i + 1] = p2
        }
    }
    
    func solveC3() {
        let count3 = m_count - 2
        for i in 0 ..< count3 {
            var p1 = m_ps[i]
            var p2 = m_ps[i + 1]
            var p3 = m_ps[i + 2]
            let m1 = m_ims[i]
            let m2 = m_ims[i + 1]
            let m3 = m_ims[i + 2]
            let d1 = subtract(p2, p1)
            let d2 = subtract(p3, p2)
            let L1sqr = d1.lengthSquared()
            let L2sqr = d2.lengthSquared()
            if L1sqr * L2sqr == 0.0 {
                continue
            }
            let a = b2Cross(d1, d2)
            let b = b2Dot22(d1, d2)
            var angle = b2Atan2(a, b)
            let Jd1 = multM(d1.skew(), (-1.0 / L1sqr))
            let Jd2 = multM(d2.skew(), (1.0 / L2sqr))
            let J1 = minus(Jd1)
            let J2 = subtract(Jd1, Jd2) 
            let J3 = Jd2
            var mass = m1 * b2Dot22(J1, J1) + m2 * b2Dot22(J2, J2) + m3 * b2Dot22(J3, J3)
            if mass == 0.0 {
                continue
            }
            mass = 1.0 / mass
            var C = angle - m_as[i]
            while C > b2_pi {
                angle -= 2 * b2_pi
                C = angle - m_as[i]
            }
            while C < -b2_pi {
                angle += 2.0 * b2_pi
                C = angle - m_as[i]
            }
            let impulse = -m_k3 * mass * C
            addEqual(&p1, multM(J1, m1 * impulse))
            addEqual(&p2, multM(J2, m2 * impulse))
            addEqual(&p3, multM(J3, m3 * impulse))
            m_ps[i] = p1
            m_ps[i + 1] = p2
            m_ps[i + 2] = p3
        }
    }
 
}
