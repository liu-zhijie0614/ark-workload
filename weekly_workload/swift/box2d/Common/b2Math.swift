import Glibc

func b2IsValid(_ x: Double) -> Bool {
    return x.isNaN == false && x.isInfinite == false
}

func b2Sqrt(_ x: Double) -> Double {
    return sqrt(x)
}

func b2Atan2(_ y: Double, _ x: Double) -> Double {
    return atan2(y, x)
}

public class b2Vec2 {
    public var x: Double
    public var y: Double
    public init(_ x_ : Double? = 0, _ y_ : Double? = 0) {
        if (x_ != nil && y_ != nil) {
            x = x_!
            y = y_!
        } else {
            x = 0
            y = 0
        }
    }

    public func setZero() {
        x = 0.0
        y = 0.0
    }
    
    public func set(_ x_ : Double, _ y_ : Double) {
        x = x_
        y = y_
    }
    public func getSubscript(_ index: Int) -> Double {
        if index == 0 {
            return x
        } else {
            return y
        }
    }
    public func setSubscript(_ index: Int, _ newValue: Double) {
        if index == 0 {
            x = newValue
        } else {
            y = newValue
        }
    }
    public func length() -> Double {
        return sqrt(x * x + y * y)
    }
    public func lengthSquared() -> Double {
        return x * x + y * y
    }
    public func normalize() -> Double {
        let length = self.length()
        if length < b2_epsilon {
            return 0.0
        }
        let invLength = 1.0 / length
        x *= invLength
        y *= invLength
        return length
    }
    public func isValid() -> Bool {
        return b2IsValid(x) && b2IsValid(y)
    }
    public func skew() -> b2Vec2 {
        return b2Vec2(-y, x)
    }
}

public func minus(_ v: b2Vec2) -> b2Vec2 {
    let _v = b2Vec2(-v.x, -v.y)
    return _v
}

public func addEqual(_ a: inout b2Vec2, _ b: b2Vec2) {
    a.x += b.x
    a.y += b.y
}

public func subtractEqual(_ a: inout b2Vec2, _ b: b2Vec2) {
    a.x -= b.x
    a.y -= b.y
}

public func mulEqual(_ a: inout b2Vec2, _ b: Double) {
    a.x *= b
    a.y *= b
}

public class b2Vec3 {
    var x : Double
    var y : Double
    var z : Double
    init(_ x_ : Double? = 0, _ y_ : Double? = 0, _ z_ : Double? = 0) {
        if (x_ != nil && y_ != nil && z_ != nil) {
            x = x_!
            y = y_!
            z = z_!
        } else {
            x = 0.0
            y = 0.0
            z = 0.0
        }
    }
    
    func setZero() {
        x = 0.0
        y = 0.0
    }
    
    func set(_ x_: Double, _ y_: Double, _ z_: Double) {
        x = x_
        y = y_
        z = z_
    }
}

public func minus3(_ v: b2Vec3) -> b2Vec3 {
    let _v = b2Vec3(-v.x, -v.y, -v.z)
    return _v
}

public func addEqual3(_ a: inout b2Vec3, _ b: b2Vec3) {
    a.x += b.x
    a.y += b.y
    a.z += b.z
}

public func mulMEqual3(_ a: inout b2Vec3, _ b: Double) {
    a.x *= b
    a.y *= b
    a.z *= b
}

public class b2Mat22 {
    public var ex : b2Vec2
    public var ey : b2Vec2
    public func set_1(_ angle: Double) {
        let c = cos(angle)
        let s = sin(angle)
        ex.set(c, s)
        ey.set(-s, c)
    }
    
    public init(_ c1: b2Vec2? = nil, _ c2: b2Vec2? = nil) {
        if (c1 != nil && c2 != nil) {
            ex = c1!
            ey = c2!
        } else {
            ex = b2Vec2(0.0, 0.0)
            ey = b2Vec2(0.0, 0.0)
        }
    }
    
    public func set(_ c1: b2Vec2, _ c2: b2Vec2) {
        ex = c1
        ey = c2
    }
    public func setIdentity() {
        ex.x = 1.0; ey.x = 0.0
        ex.y = 0.0; ey.y = 1.0
    }
    public func setZero() {
        ex.x = 0.0; ey.x = 0.0
        ex.y = 0.0; ey.y = 0.0
    }
    public func getInverse() -> b2Mat22 {
        let a = ex.x, b = ey.x, c = ex.y, d = ey.y
        let B = b2Mat22()
        var det = a * d - b * c
        if det != 0.0 {
            det = 1.0 / det
        }
        B.ex.x =  det * d;  B.ey.x = -det * b
        B.ex.y = -det * c;  B.ey.y =  det * a
        return B
    }
    public func solve(_ b: b2Vec2) -> b2Vec2 {
        let a11 = ex.x, a12 = ey.x, a21 = ex.y, a22 = ey.y
        var det = a11 * a22 - a12 * a21
        if det != 0.0 {
            det = 1.0 / det
        }
        var x = b2Vec2()
        x.x = det * (a22 * b.x - a12 * b.y)
        x.y = det * (a11 * b.y - a21 * b.x)
        return x
    }
}

public class b2Mat33 {
    public var ex : b2Vec3
    public var ey : b2Vec3
    public var ez : b2Vec3
    public init(_ c1: b2Vec3? = nil, _ c2: b2Vec3? = nil, _ c3: b2Vec3? = nil) {
        if (c1 != nil && c2 != nil && c3 != nil) {
            ex = c1!
            ey = c2!
            ez = c3!
        } else {
            ex = b2Vec3()
            ey = b2Vec3()
            ez = b2Vec3()
        }
    }
    public func setZero() {
        ex.setZero()
        ey.setZero()
        ez.setZero()
    }
    public func solve33(_ b: b2Vec3) -> b2Vec3 {
        var det = b2Dot33(ex, b2Cross33(ey, ez))
        if det != 0.0 {
            det = 1.0 / det
        }
        let x = b2Vec3()
        x.x = det * b2Dot33(b, b2Cross33(ey, ez))
        x.y = det * b2Dot33(ex, b2Cross33(b, ez))
        x.z = det * b2Dot33(ex, b2Cross33(ey, b))
        return x
    }
    public func solve22(_ b: b2Vec2) -> b2Vec2 {
        let a11 = ex.x, a12 = ey.x, a21 = ex.y, a22 = ey.y
        var det = a11 * a22 - a12 * a21
        if det != 0.0 {
            det = 1.0 / det
        }
        var x = b2Vec2()
        x.x = det * (a22 * b.x - a12 * b.y)
        x.y = det * (a11 * b.y - a21 * b.x)
        return x
    }
    public func getInverse22() -> b2Mat33 {
        let a = ex.x, b = ey.x, c = ex.y, d = ey.y
        var det = a * d - b * c
        if det != 0.0 {
            det = 1.0 / det
        }
        let M = b2Mat33()
        M.ex.x =  det * d;    M.ey.x = -det * b; M.ex.z = 0.0
        M.ex.y = -det * c;    M.ey.y =  det * a; M.ey.z = 0.0
        M.ez.x = 0.0; M.ez.y = 0.0; M.ez.z = 0.0
        return M
    }
    
    public func getSymInverse33() -> b2Mat33 {
        var det = b2Dot33(ex, b2Cross33(ey, ez))
        if det != 0.0 {
            det = 1.0 / det
        }
        let a11 = ex.x, a12 = ey.x, a13 = ez.x
        let a22 = ey.y, a23 = ez.y
        let a33 = ez.z
        let M = b2Mat33()
        M.ex.x = det * (a22 * a33 - a23 * a23)
        M.ex.y = det * (a13 * a23 - a12 * a33)
        M.ex.z = det * (a12 * a23 - a13 * a22)
        M.ey.x = M.ex.y
        M.ey.y = det * (a11 * a33 - a13 * a13)
        M.ey.z = det * (a13 * a12 - a11 * a23)
        M.ez.x = M.ex.z
        M.ez.y = M.ey.z
        M.ez.z = det * (a11 * a22 - a12 * a12)
        return M
    }
}

public class b2Rot {
    public var s: Double
    public var c: Double

    public init(_ angle : Double? = nil) {
        if (angle != nil) {
            s = sin(angle!)
            c = cos(angle!)
        } else {
            s = 0.0
            c = 0.0
        }
    }
    
    public func set(_ angle : Double) {
        s = sin(angle)
        c = cos(angle)
    }
    public func setIdentity() {
        s = 0.0
        c = 1.0
    }
    public var angle: Double {
        return b2Atan2(s, c)
    }
    public var xAxis: b2Vec2 {
        return b2Vec2(c, s)
    }
    public var yAxis: b2Vec2 {
        return b2Vec2(-s, c)
    }
}

public class b2Transform  {
    public var p: b2Vec2
    public var q: b2Rot
    public init(_ position: b2Vec2? = nil, _ rotation: b2Rot? = nil) {
        if (position != nil && rotation != nil) {
            p = position!
            q = rotation!
        } else {
            p = b2Vec2()
            q = b2Rot()
        }
    }
    public func setIdentity() {
        p.setZero()
        q.setIdentity()
    }
    public func set(_ position: b2Vec2, _ angle: Double) {
        p = position
        q.set(angle)
    }
}

public class b2Sweep {
    public var localCenter = b2Vec2()
    public var m_c0 = b2Vec2()
    public var c0 : b2Vec2 {
        get {
            return m_c0
        }
        set {
            m_c0 = newValue
        }
    }
    public var c = b2Vec2()
    public var a0: Double = 0
    public var a: Double = 0
    public var alpha0: Double = 0
    public init() {}
    public func getTransform(_ beta: Double) -> b2Transform {
        let xf = b2Transform()
        xf.p = add(multM(c0, (1.0 - beta)), multM(c, beta))
        let angle = (1.0 - beta) * a0 + beta * a
        xf.q.set(angle)
        subtractEqual(&xf.p, b2MulR2(xf.q, localCenter))
        return xf
    }
    public func advance(_ alpha: Double) {
        let beta = (alpha - alpha0) / (1.0 - alpha0)
        addEqual(&c0, multM(subtract(c, c0), beta))
        a0 += beta * (a - a0)
        alpha0 = alpha
    }
    public func normalize() {
        let twoPi = 2.0 * b2_pi
        let d =  twoPi * floor(a0 / twoPi)
        a0 -= d
        a -= d
    }
}

public let b2Vec2_zero = b2Vec2(0.0, 0.0)
public func b2Dot22(_ a : b2Vec2, _ b : b2Vec2) -> Double {
    return a.x * b.x + a.y * b.y
}

public func b2Cross(_ a : b2Vec2, _ b : b2Vec2) -> Double {
    return a.x * b.y - a.y * b.x
}

public func b2Cross12(_ a : b2Vec2, _ s : Double) -> b2Vec2 {
    return b2Vec2(s * a.y, -s * a.x)
}

public func b2Cross21(_ s : Double, _ a : b2Vec2) -> b2Vec2 {
    return b2Vec2(-s * a.y, s * a.x)
}

public func b2Mul22(_ A : b2Mat22, _ v : b2Vec2) -> b2Vec2 {
    return b2Vec2(b2Dot22(v, A.ex), b2Dot22(v, A.ey))
}

public func b2MulTM2(_ A : b2Mat22, _ v : b2Vec2) -> b2Vec2 {
    return b2Vec2(b2Dot22(v, A.ex), b2Dot22(v, A.ey))
}

public func add(_ a: b2Vec2, _ b: b2Vec2) -> b2Vec2 {
    return b2Vec2(a.x + b.x, a.y + b.y)
}

public func subtract(_ a: b2Vec2, _ b: b2Vec2) -> b2Vec2 {
    return b2Vec2(a.x - b.x, a.y - b.y)
}

public func multM(_ a: b2Vec2, _ b: Double) -> b2Vec2 {
    return b2Vec2(a.x * b, a.y * b)
}

public func b2Distance(_ a : b2Vec2, _ b : b2Vec2) -> Double {
    let c = b2Vec2(a.x - b.x, a.y - b.y);
    return c.length()
}

public func b2DistanceSquared(_ a : b2Vec2, _ b: b2Vec2) -> Double {
    let c = b2Vec2(a.x - b.x, a.y - b.y);
    return b2Dot22(c, c)
}

public func multM3 (_ s : Double, _ a : b2Vec3) -> b2Vec3 {
    return b2Vec3(s * a.x, s * a.y, s * a.z)
}

public func add3 (_ a : b2Vec3, _ b : b2Vec3) -> b2Vec3 {
    return b2Vec3(a.x + b.x, a.y + b.y, a.z + b.z)
}

public func subtract3 (_ a : b2Vec3, _ b : b2Vec3) -> b2Vec3 {
    return b2Vec3(a.x - b.x, a.y - b.y, a.z - b.z)
}

public func b2Dot33(_ a : b2Vec3, _ b : b2Vec3) -> Double {
    return a.x * b.x + a.y * b.y + a.z * b.z
}

public func b2Cross33(_ a : b2Vec3, _ b : b2Vec3) -> b2Vec3 {
    return b2Vec3(a.y * b.z - a.z * b.y, a.z * b.x - a.x * b.z, a.x * b.y - a.y * b.x)
}

public func b2Mul(_ A : b2Mat22, _ B : b2Mat22) -> b2Mat22 {
    return b2Mat22(b2Mul22(A, B.ex), b2Mul22(A, B.ey))
}

public func b2MulTMM(_ A : b2Mat22, _ B : b2Mat22) -> b2Mat22 {
    let c1 = b2Vec2(b2Dot22(A.ex, B.ex), b2Dot22(A.ey, B.ex))
    let c2 = b2Vec2(b2Dot22(A.ex, B.ey), b2Dot22(A.ey, B.ey))
    return b2Mat22(c1, c2)
}

public func b2Mul33(_ A : b2Mat33, _ v : b2Vec3) -> b2Vec3 {
    return add3(add3(multM3(v.x,A.ex), multM3(v.y,A.ey)), multM3(v.z,A.ez))
}

public func b2Mul32(_ A : b2Mat33, _ v : b2Vec2) -> b2Vec2 {
    return b2Vec2(A.ex.x * v.x + A.ey.x * v.y, A.ex.y * v.x + A.ey.y * v.y)
}

public func b2MulRR(_ q : b2Rot, _ r : b2Rot) -> b2Rot {
    let qr = b2Rot()
    qr.s = q.s * r.c + q.c * r.s
    qr.c = q.c * r.c - q.s * r.s
    return qr
}

public func b2MulTRR(_ q : b2Rot, _ r : b2Rot) -> b2Rot {
    let qr = b2Rot()
    qr.s = q.c * r.s - q.s * r.c
    qr.c = q.c * r.c + q.s * r.s
    return qr
}

public func b2MulR2(_ q : b2Rot, _ v : b2Vec2) -> b2Vec2 {
    return b2Vec2(q.c * v.x - q.s * v.y, q.s * v.x + q.c * v.y)
}

public func b2MulTR2(_ q : b2Rot, _ v : b2Vec2) -> b2Vec2 {
    return b2Vec2(q.c * v.x + q.s * v.y, -q.s * v.x + q.c * v.y)
}

public func b2MulT2(_ T : b2Transform, _ v : b2Vec2) -> b2Vec2 {
    let x = (T.q.c * v.x - T.q.s * v.y) + T.p.x
    let y = (T.q.s * v.x + T.q.c * v.y) + T.p.y
    return b2Vec2(x, y)
}

public func b2MulTT2(_ T : b2Transform, _ v : b2Vec2) -> b2Vec2 {
    let px = v.x - T.p.x
    let py = v.y - T.p.y
    let x = (T.q.c * px + T.q.s * py)
    let y = (-T.q.s * px + T.q.c * py)
    return b2Vec2(x, y)
}

public func b2MulTT(_ A : b2Transform, _ B : b2Transform) -> b2Transform {
    let C = b2Transform()
    C.q = b2MulRR(A.q, B.q)
    let a = b2MulR2(A.q, B.p)
    C.p.x = a.x + A.p.x
    C.p.y = a.y + A.p.y
    return C
}

public func b2MulT(_ A : b2Transform, _ B : b2Transform) -> b2Transform {
    let C = b2Transform()
    C.q = b2MulTRR(A.q, B.q)
    let a = b2Vec2(B.p.x - A.p.x, B.p.y - A.p.y);
    C.p = b2MulTR2(A.q, a);
    return C
}

public func b2Abs2(_ a : b2Vec2) -> b2Vec2 {
    return b2Vec2(abs(a.x), abs(a.y))
}

public func b2Abs22(_ A : b2Mat22) -> b2Mat22 {
    return b2Mat22(b2Abs2(A.ex), b2Abs2(A.ey))
}

public func b2Min(_ a : b2Vec2, _ b : b2Vec2) -> b2Vec2 {
    return b2Vec2(min(a.x, b.x), min(a.y, b.y))
}

public func b2Max(_ a : b2Vec2, _ b : b2Vec2) -> b2Vec2 {
    return b2Vec2(max(a.x, b.x), max(a.y, b.y))
}

public func b2ClampF(_ a : Double, _ low : Double, _ high : Double) -> Double {
    return max(low, min(a, high))
}

public func b2Clamp(_ a : b2Vec2, _ low : b2Vec2, _ high : b2Vec2) -> b2Vec2 {
    return b2Max(low, b2Min(a, high))
}

public func b2NextPowerOfTwo(_ _x : Int) -> Int {
    var x = _x
    x = x | ((x >> 1))
    x = x | ((x >> 2))
    x = x | ((x >> 4))
    x = x | ((x >> 8))
    x = x | ((x >> 16))
    return x + 1
}

public func b2IsPowerOfTwo(_ x : Int) -> Bool {
    let result = x > 0 && (x & (x - 1)) == 0
    return result
}
