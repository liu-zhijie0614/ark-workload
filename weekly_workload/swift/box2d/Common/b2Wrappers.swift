
public protocol b2QueryWrapper {
    func queryCallback(_ proxyId: Int) -> Bool
}

public protocol b2RayCastWrapper {
    func rayCastCallback(_ input: b2RayCastInput, _ proxyId: Int) -> Double
}

public protocol b2BroadPhaseWrapper {
    func addPair(_ proxyUserDataA: inout b2FixtureProxy, _ proxyUserDataB: inout b2FixtureProxy)
}
