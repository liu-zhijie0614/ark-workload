

public let b2_minFloat = Double.leastNormalMagnitude
public let b2_maxFloat = Double.greatestFiniteMagnitude
public let b2_epsilon = Double.ulpOfOne
public let b2_pi: Double = Double.pi
public let b2_maxManifoldPoints = 2
public let b2_maxPolygonVertices = 8
public let b2_aabbExtension: Double = 0.1
public let b2_aabbMultiplier: Double = 2.0
public let b2_linearSlop: Double = 0.005
public let b2_angularSlop: Double = (2.0 / 180.0 * b2_pi)
public let b2_polygonRadius: Double = (2.0 * b2_linearSlop)
public let b2_maxSubSteps = 8
public let b2_maxTOIContacts = 32
public let b2_velocityThreshold: Double = 1.0
public let b2_maxLinearCorrection: Double = 0.2
public let b2_maxAngularCorrection: Double = (8.0 / 180.0 * b2_pi)
public let b2_maxTranslation: Double = 2.0
public let b2_maxTranslationSquared: Double = (b2_maxTranslation * b2_maxTranslation)
public let b2_maxRotation: Double = (0.5 * b2_pi)
public let b2_maxRotationSquared: Double = (b2_maxRotation * b2_maxRotation)
public let b2_baumgarte: Double = 0.2
public let b2_toiBaugarte: Double = 0.75
public let b2_timeToSleep: Double = 0.5
public let b2_linearSleepTolerance: Double = 0.01
public let b2_angularSleepTolerance: Double = (2.0 / 180.0 * b2_pi)


