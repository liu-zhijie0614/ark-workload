 

public enum b2JointType {
    case unknownJoint
    case revoluteJoint
    case prismaticJoint
    case distanceJoint
    case pulleyJoint
    case mouseJoint
    case gearJoint
    case wheelJoint
    case weldJoint
    case frictionJoint
    case ropeJoint
    case motorJoint
}

public enum b2LimitState {
    case inactiveLimit
    case atLowerLimit
    case atUpperLimit
    case equalLimits
}

open class b2JointEdge {
    init(_ joint: b2Joint) {
        self.joint = joint
    }
    var other: b2Body! = nil
    unowned var joint: b2Joint
    var prev: b2JointEdge? = nil
    var next: b2JointEdge? = nil
}

open class b2JointDef {
    public init() {
        type = b2JointType.unknownJoint
        userData = nil
        bodyA = nil
        bodyB = nil
        collideConnected = false
    }
    open var type: b2JointType
    open var userData: AnyObject?
    open var bodyA: b2Body!
    open var bodyB: b2Body!
    open var collideConnected: Bool
}

open class b2Joint {
    var m_type: b2JointType = b2JointType.unknownJoint
    var m_prev: b2Joint? = nil
    var m_next: b2Joint? = nil
    var m_edgeA : b2JointEdge!
    var m_edgeB : b2JointEdge!
    var m_bodyA: b2Body
    var m_bodyB: b2Body
    var m_index: Int = 0
    var m_islandFlag: Bool = false
    var m_collideConnected: Bool = false
    var m_userData: AnyObject? = nil
    
    open var type: b2JointType {
        return m_type
    }
    open var bodyA: b2Body {
        return m_bodyA
    }
    open var bodyB: b2Body {
        return m_bodyB
    }
    open var anchorA: b2Vec2 {
        fatalError("must override")
    }
    open var anchorB: b2Vec2 {
        fatalError("must override")
    }
    
    init(_ def: b2JointDef) {
        m_type = def.type
        m_prev = nil
        m_next = nil
        m_bodyA = def.bodyA
        m_bodyB = def.bodyB
        m_index = 0
        m_collideConnected = def.collideConnected
        m_islandFlag = false
        m_userData = def.userData
        m_edgeA = b2JointEdge(self)
        m_edgeA.other = nil
        m_edgeA.prev = nil
        m_edgeA.next = nil
        m_edgeB = b2JointEdge(self)
        m_edgeB.other = nil
        m_edgeB.prev = nil
        m_edgeB.next = nil
    }
    
    open func getReactionForce(_ inv_dt: Double) -> b2Vec2 {
        fatalError("must override")
    }
    open func getReactionTorque(_ inv_dt: Double) -> Double {
        fatalError("must override")
    }
    open func getNext() -> b2Joint? {
        return m_next
    }
    open var userData: AnyObject? {
        get {
            return m_userData
        }
        set {
            setUserData(newValue)
        }
    }
    open func setUserData(_ data: AnyObject?) {
        m_userData = data
    }
    open var isActive: Bool {
        return m_bodyA.isActive && m_bodyB.isActive
    }
    open var collideConnected: Bool {
        return m_collideConnected
    }
    open func shiftOrigin(_ newOrigin: b2Vec2) { }
    
    class func create(_ def: b2JointDef) -> b2Joint {
        var joint: b2Joint
        switch def.type {
        case .distanceJoint:
            joint = b2DistanceJoint(def as! b2DistanceJointDef)
            break
        case .mouseJoint:
            joint = b2MouseJoint(def as! b2MouseJointDef)
            break
        case .prismaticJoint:
            joint = b2PrismaticJoint(def as! b2PrismaticJointDef)
            break
        case .revoluteJoint:
            joint = b2RevoluteJoint(def as! b2RevoluteJointDef)
            break
        case .pulleyJoint:
            joint = b2PulleyJoint(def as! b2PulleyJointDef)
            break
        case .gearJoint:
            joint = b2GearJoint(def as! b2GearJointDef)
            break
        case .weldJoint:
            joint = b2WeldJoint(def as! b2WeldJointDef)
            break
        case .frictionJoint:
            joint = b2FrictionJoint(def as! b2FrictionJointDef)
            break
        default:
            fatalError("unknown joint type")
        }
        return joint
    }
    
    class func destroy(_ joint: b2Joint) {
    }
    
    func initVelocityConstraints(_ data: inout b2SolverData) {
        fatalError("must override")
    }
    func solveVelocityConstraints(_ data: inout b2SolverData) {
        fatalError("must override")
    }
    
    func solvePositionConstraints(_ data: inout b2SolverData) -> Bool {
        fatalError("must override")
    }
}

open class b2WeldJointDef : b2JointDef {
   public override init() {
       localAnchorA = b2Vec2(0.0, 0.0)
       localAnchorB = b2Vec2(0.0, 0.0)
       referenceAngle = 0.0
       frequencyHz = 0.0
       dampingRatio = 0.0
       super.init()
       type = b2JointType.weldJoint
   }
   
   open func initialize(_ bodyA: b2Body, _ bodyB: b2Body, _ anchor: b2Vec2) {
       self.bodyA = bodyA
       self.bodyB = bodyB
       self.localAnchorA = bodyA.getLocalPoint(anchor)
       self.localAnchorB = bodyB.getLocalPoint(anchor)
       self.referenceAngle = bodyB.angle - bodyA.angle
   }
   
   open var localAnchorA: b2Vec2
   open var localAnchorB: b2Vec2
   open var referenceAngle: Double
   open var frequencyHz: Double
   open var dampingRatio: Double
}

open class b2WeldJoint : b2Joint {
   
   var m_frequencyHz: Double = 0.0
   var m_dampingRatio: Double = 0.0
   var m_bias: Double = 0.0
   var m_localAnchorA = b2Vec2()
   var m_localAnchorB = b2Vec2()
   var m_referenceAngle: Double = 0.0
   var m_gamma: Double = 0.0
   var m_impulse = b2Vec3()
   var m_indexA: Int = 0
   var m_indexB: Int = 0
   var m_rA = b2Vec2()
   var m_rB = b2Vec2()
   var m_localCenterA = b2Vec2()
   var m_localCenterB = b2Vec2()
   var m_invMassA: Double = 0.0
   var m_invMassB: Double = 0.0
   var m_invIA: Double = 0.0
   var m_invIB: Double = 0.0
   var m_mass = b2Mat33()
   
   open override var anchorA: b2Vec2 {
       return m_bodyA.getWorldPoint(m_localAnchorA)
   }
   open override var anchorB: b2Vec2 {
       return m_bodyB.getWorldPoint(m_localAnchorB)
   }
   
   open override func getReactionForce(_ inv_dt: Double) -> b2Vec2 {
       let P = b2Vec2(m_impulse.x, m_impulse.y)
       return multM(P, inv_dt)
   }
   open override func getReactionTorque(_ inv_dt: Double) -> Double {
       return inv_dt * m_impulse.z
   }
   
   open var localAnchorA: b2Vec2  { return m_localAnchorA }
   
   open var localAnchorB: b2Vec2  { return m_localAnchorB }
   
   open var referenceAngle: Double { return m_referenceAngle }
   
   open func setFrequency(_ hz: Double) { m_frequencyHz = hz }
   open var frequency: Double { return m_frequencyHz }
   open func setDampingRatio(_ ratio: Double) { m_dampingRatio = ratio }
   open var dampingRatio: Double { return m_dampingRatio }
   
   init(_ def: b2WeldJointDef) {
       m_localAnchorA = def.localAnchorA
       m_localAnchorB = def.localAnchorB
       m_referenceAngle = def.referenceAngle
       m_frequencyHz = def.frequencyHz
       m_dampingRatio = def.dampingRatio
       m_impulse = b2Vec3(0.0, 0.0, 0.0)
       super.init(def)
   }
   
   override func initVelocityConstraints(_ data: inout b2SolverData) {
       m_indexA = m_bodyA.m_islandIndex
       m_indexB = m_bodyB.m_islandIndex
       m_localCenterA = m_bodyA.m_sweep.localCenter
       m_localCenterB = m_bodyB.m_sweep.localCenter
       m_invMassA = m_bodyA.m_invMass
       m_invMassB = m_bodyB.m_invMass
       m_invIA = m_bodyA.m_invI
       m_invIB = m_bodyB.m_invI
       let aA = data.positions.get(m_indexA).a
       var vA = data.velocities.get(m_indexA).v
       var wA = data.velocities.get(m_indexA).w
       let aB = data.positions.get(m_indexB).a
       var vB = data.velocities.get(m_indexB).v
       var wB = data.velocities.get(m_indexB).w
       let qA = b2Rot(aA), qB = b2Rot(aB)
       m_rA = b2MulR2(qA, subtract(m_localAnchorA, m_localCenterA))
       m_rB = b2MulR2(qB, subtract(m_localAnchorB, m_localCenterB))
       let mA = m_invMassA, mB = m_invMassB
       let iA = m_invIA, iB = m_invIB
       let K = b2Mat33()
       K.ex.x = mA + mB + m_rA.y * m_rA.y * iA + m_rB.y * m_rB.y * iB
       K.ey.x = -m_rA.y * m_rA.x * iA - m_rB.y * m_rB.x * iB
       K.ez.x = -m_rA.y * iA - m_rB.y * iB
       K.ex.y = K.ey.x
       K.ey.y = mA + mB + m_rA.x * m_rA.x * iA + m_rB.x * m_rB.x * iB
       K.ez.y = m_rA.x * iA + m_rB.x * iB
       K.ex.z = K.ez.x
       K.ey.z = K.ez.y
       K.ez.z = iA + iB
       if m_frequencyHz > 0.0 {
           m_mass = K.getInverse22()
           var invM = iA + iB
           let m = invM > 0.0 ? 1.0 / invM : 0.0
           let C = aB - aA - m_referenceAngle
           let omega = 2.0 * b2_pi * m_frequencyHz
           let d = 2.0 * m * m_dampingRatio * omega
           let k = m * omega * omega
           let h = data.step.dt
           m_gamma = h * (d + h * k)
           m_gamma = m_gamma != 0.0 ? 1.0 / m_gamma : 0.0
           m_bias = C * h * k * m_gamma
           invM += m_gamma
           m_mass.ez.z = invM != 0.0 ? 1.0 / invM : 0.0
       } else {
           m_mass = K.getSymInverse33()
           m_gamma = 0.0
           m_bias = 0.0
       }
       if data.step.warmStarting {
           mulMEqual3(&m_impulse, data.step.dtRatio)
           let P = b2Vec2(m_impulse.x, m_impulse.y)
           subtractEqual(&vA, multM(P, mA))
           wA -= iA * (b2Cross(m_rA, P) + m_impulse.z)
           addEqual(&vB, multM(P, mB))
           wB += iB * (b2Cross(m_rB, P) + m_impulse.z)
       } else {
           m_impulse.setZero()
       }
       data.velocities.get(m_indexA).v = vA
       data.velocities.get(m_indexA).w = wA
       data.velocities.get(m_indexB).v = vB
       data.velocities.get(m_indexB).w = wB
   }
   
   override func solveVelocityConstraints(_ data: inout b2SolverData) {
       var vA = data.velocities.get(m_indexA).v
       var wA = data.velocities.get(m_indexA).w
       var vB = data.velocities.get(m_indexB).v
       var wB = data.velocities.get(m_indexB).w
       let mA = m_invMassA, mB = m_invMassB
       let iA = m_invIA, iB = m_invIB
       if m_frequencyHz > 0.0 {
           let Cdot2 = wB - wA
           let impulse2 = -m_mass.ez.z * (Cdot2 + m_bias + m_gamma * m_impulse.z)
           m_impulse.z += impulse2
           wA -= iA * impulse2
           wB += iB * impulse2
           let Cdot1 = subtract(subtract(add(vB, b2Cross21(wB, m_rB)), vA), b2Cross21(wA, m_rA))
           let impulse1 = minus(b2Mul32(m_mass, Cdot1))
           m_impulse.x += impulse1.x
           m_impulse.y += impulse1.y
           let P = impulse1
           subtractEqual(&vA, multM(P, mA))
           wA -= iA * b2Cross(m_rA, P)
           addEqual(&vB, multM(P, mB))
           wB += iB * b2Cross(m_rB, P)
       } else {
           let Cdot1 = subtract(subtract(add(vB, b2Cross21(wB, m_rB)), vA), b2Cross21(wA, m_rA))
           let Cdot2 = wB - wA
           let Cdot = b2Vec3(Cdot1.x, Cdot1.y, Cdot2)
           let impulse = minus3(b2Mul33(m_mass, Cdot))
           addEqual3(&m_impulse, impulse)
           let P = b2Vec2(impulse.x, impulse.y)
           subtractEqual(&vA, multM(P, mA))
           wA -= iA * (b2Cross(m_rA, P) + impulse.z)
           addEqual(&vB, multM(P, mB))
           wB += iB * (b2Cross(m_rB, P) + impulse.z)
       }
       data.velocities.get(m_indexA).v = vA
       data.velocities.get(m_indexA).w = wA
       data.velocities.get(m_indexB).v = vB
       data.velocities.get(m_indexB).w = wB
   }
   
   override func solvePositionConstraints(_ data: inout b2SolverData) -> Bool {
       var cA = data.positions.get(m_indexA).c
       var aA = data.positions.get(m_indexA).a
       var cB = data.positions.get(m_indexB).c
       var aB = data.positions.get(m_indexB).a
       let qA = b2Rot(aA), qB = b2Rot(aB)
       let mA = m_invMassA, mB = m_invMassB
       let iA = m_invIA, iB = m_invIB
       let rA = b2MulR2(qA, subtract(m_localAnchorA, m_localCenterA))
       let rB = b2MulR2(qB, subtract(m_localAnchorB, m_localCenterB))
       var positionError: Double, angularError: Double
       let K = b2Mat33()
       K.ex.x = mA + mB + rA.y * rA.y * iA + rB.y * rB.y * iB
       K.ey.x = -rA.y * rA.x * iA - rB.y * rB.x * iB
       K.ez.x = -rA.y * iA - rB.y * iB
       K.ex.y = K.ey.x
       K.ey.y = mA + mB + rA.x * rA.x * iA + rB.x * rB.x * iB
       K.ez.y = rA.x * iA + rB.x * iB
       K.ex.z = K.ez.x
       K.ey.z = K.ez.y
       K.ez.z = iA + iB
       if m_frequencyHz > 0.0 {
           let C1 = subtract(subtract(add(cB, rB), cA), rA)
           positionError = C1.length()
           angularError = 0.0
           let P = minus(K.solve22(C1))
           subtractEqual(&cA, multM(P, mA))
           aA -= iA * b2Cross(rA, P)
           addEqual(&cB, multM(P, mB))
           aB += iB * b2Cross(rB, P)
       } else {
           let C1 = subtract(subtract(add(cB, rB), cA), rA)
           let C2 = aB - aA - m_referenceAngle
           positionError = C1.length()
           angularError = abs(C2)
           let C = b2Vec3(C1.x, C1.y, C2)
           let impulse = minus3(K.solve33(C))
           let P = b2Vec2(impulse.x, impulse.y)
           subtractEqual(&cA, multM(P, mA))
           aA -= iA * (b2Cross(rA, P) + impulse.z)
           addEqual(&cB, multM(P, mB))
           aB += iB * (b2Cross(rB, P) + impulse.z)
       }
       data.positions.get(m_indexA).c = cA
       data.positions.get(m_indexA).a = aA
       data.positions.get(m_indexB).c = cB
       data.positions.get(m_indexB).a = aB
       return positionError <= b2_linearSlop && angularError <= b2_angularSlop
   }
}

open class b2RevoluteJointDef : b2JointDef {
   public override init() {
       localAnchorA = b2Vec2(0.0, 0.0)
       localAnchorB = b2Vec2(0.0, 0.0)
       referenceAngle = 0.0
       lowerAngle = 0.0
       upperAngle = 0.0
       maxMotorTorque = 0.0
       motorSpeed = 0.0
       enableLimit = false
       enableMotor = false
       super.init()
       type = b2JointType.revoluteJoint
   }
   
   open func initialize(_ bodyA: b2Body, _ bodyB: b2Body, _ anchor: b2Vec2) {
       self.bodyA = bodyA
       self.bodyB = bodyB
       self.localAnchorA = bodyA.getLocalPoint(anchor)
       self.localAnchorB = bodyB.getLocalPoint(anchor)
       self.referenceAngle = bodyB.angle - bodyA.angle
   }
   
   open var localAnchorA: b2Vec2
   open var localAnchorB: b2Vec2
   open var referenceAngle: Double
   open var maxMotorTorque: Double
   open var motorSpeed: Double
   open var lowerAngle: Double
   open var upperAngle: Double
   open var enableLimit: Bool
   open var enableMotor: Bool
}

open class b2RevoluteJoint : b2Joint {
   open override var anchorA: b2Vec2 {
       return m_bodyA.getWorldPoint(m_localAnchorA)
   }
   open override var anchorB: b2Vec2 {
       return m_bodyB.getWorldPoint(m_localAnchorB)
   }
   
   open var localAnchorA: b2Vec2  { return m_localAnchorA }
   open var localAnchorB: b2Vec2  { return m_localAnchorB }
   open var referenceAngle: Double { return m_referenceAngle }
   
   open var jointAngle: Double {
       let bA = m_bodyA
       let bB = m_bodyB
       return bB.m_sweep.a - bA.m_sweep.a - m_referenceAngle
   }
   
   open var jointSpeed: Double {
       let bA = m_bodyA
       let bB = m_bodyB
       return bB.m_angularVelocity - bA.m_angularVelocity
   }
   
   open var isLimitEnabled: Bool {
       get {
           return m_enableLimit
       }
       set {
           enableLimit(newValue)
       }
   }
   
   open func enableLimit(_ flag: Bool) {
       if flag != m_enableLimit {
           m_bodyA.setAwake(true)
           m_bodyB.setAwake(true)
           m_enableLimit = flag
           m_impulse.z = 0.0
       }
   }
   
   open var lowerLimit: Double {
       return m_lowerAngle
   }
   
   open var upperLimit: Double {
       return m_upperAngle
   }
   
   open func setLimits(_ lower: Double, _ upper: Double) {
       if lower != m_lowerAngle || upper != m_upperAngle {
           m_bodyA.setAwake(true)
           m_bodyB.setAwake(true)
           m_impulse.z = 0.0
           m_lowerAngle = lower
           m_upperAngle = upper
       }
   }
   
   open var isMotorEnabled: Bool {
       return m_enableMotor
   }
   
   open func enableMotor(_ flag: Bool) {
       m_bodyA.setAwake(true)
       m_bodyB.setAwake(true)
       m_enableMotor = flag
   }
   
   open func setMotorSpeed(_ speed: Double) {
       m_bodyA.setAwake(true)
       m_bodyB.setAwake(true)
       m_motorSpeed = speed
   }
   
   open var motorSpeed: Double {
       get {
           return m_motorSpeed
       }
       set {
           setMotorSpeed(newValue)
       }
   }
   
   open func setMaxMotorTorque(_ torque: Double) {
       m_bodyA.setAwake(true)
       m_bodyB.setAwake(true)
       m_maxMotorTorque = torque
   }
   open var maxMotorTorque: Double {
       get {
           return m_maxMotorTorque
       }
       set {
           setMaxMotorTorque(newValue)
       }
   }
   
   open override func getReactionForce(_ inv_dt: Double) -> b2Vec2 {
       let P = b2Vec2(m_impulse.x, m_impulse.y)
       return multM(P, inv_dt)
   }
   
   open override func getReactionTorque(_ inv_dt: Double) -> Double {
       return inv_dt * m_impulse.z
   }
   
   open func getMotorTorque(_ inv_dt: Double) -> Double {
       return inv_dt * m_motorImpulse
   }
   
   init(_ def: b2RevoluteJointDef) {
       m_localAnchorA = def.localAnchorA
       m_localAnchorB = def.localAnchorB
       m_referenceAngle = def.referenceAngle
       m_impulse = b2Vec3(0.0, 0.0, 0.0)
       m_motorImpulse = 0.0
       m_lowerAngle = def.lowerAngle
       m_upperAngle = def.upperAngle
       m_maxMotorTorque = def.maxMotorTorque
       m_motorSpeed = def.motorSpeed
       m_enableLimit = def.enableLimit
       m_enableMotor = def.enableMotor
       m_limitState = b2LimitState.inactiveLimit
       super.init(def)
   }
   
   override func initVelocityConstraints(_ data: inout b2SolverData) {
       m_indexA = m_bodyA.m_islandIndex
       m_indexB = m_bodyB.m_islandIndex
       m_localCenterA = m_bodyA.m_sweep.localCenter
       m_localCenterB = m_bodyB.m_sweep.localCenter
       m_invMassA = m_bodyA.m_invMass
       m_invMassB = m_bodyB.m_invMass
       m_invIA = m_bodyA.m_invI
       m_invIB = m_bodyB.m_invI
       let aA = data.positions.get(m_indexA).a
       var vA = data.velocities.get(m_indexA).v
       var wA = data.velocities.get(m_indexA).w
       let aB = data.positions.get(m_indexB).a
       var vB = data.velocities.get(m_indexB).v
       var wB = data.velocities.get(m_indexB).w
       let qA = b2Rot(aA), qB = b2Rot(aB)
       m_rA = b2MulR2(qA, subtract(m_localAnchorA, m_localCenterA))
       m_rB = b2MulR2(qB, subtract(m_localAnchorB, m_localCenterB))
       let mA = m_invMassA, mB = m_invMassB
       let iA = m_invIA, iB = m_invIB
       let fixedRotation = (iA + iB == 0.0)
       m_mass.ex.x = mA + mB + m_rA.y * m_rA.y * iA + m_rB.y * m_rB.y * iB
       m_mass.ey.x = -m_rA.y * m_rA.x * iA - m_rB.y * m_rB.x * iB
       m_mass.ez.x = -m_rA.y * iA - m_rB.y * iB
       m_mass.ex.y = m_mass.ey.x
       m_mass.ey.y = mA + mB + m_rA.x * m_rA.x * iA + m_rB.x * m_rB.x * iB
       m_mass.ez.y = m_rA.x * iA + m_rB.x * iB
       m_mass.ex.z = m_mass.ez.x
       m_mass.ey.z = m_mass.ez.y
       m_mass.ez.z = iA + iB
       m_motorMass = iA + iB
       if m_motorMass > 0.0 {
           m_motorMass = 1.0 / m_motorMass
       }
       if m_enableMotor == false || fixedRotation {
           m_motorImpulse = 0.0
       }
       if m_enableLimit && fixedRotation == false {
           let jointAngle = aB - aA - m_referenceAngle
           if abs(m_upperAngle - m_lowerAngle) < 2.0 * b2_angularSlop {
               m_limitState = b2LimitState.equalLimits
           } else if jointAngle <= m_lowerAngle {
               if m_limitState != b2LimitState.atLowerLimit {
                   m_impulse.z = 0.0
               }
               m_limitState = b2LimitState.atLowerLimit
           } else if jointAngle >= m_upperAngle {
               if m_limitState != b2LimitState.atUpperLimit {
                   m_impulse.z = 0.0
               }
               m_limitState = b2LimitState.atUpperLimit
           } else {
               m_limitState = b2LimitState.inactiveLimit
               m_impulse.z = 0.0
           }
       } else {
           m_limitState = b2LimitState.inactiveLimit
       }
       if data.step.warmStarting {
           mulMEqual3(&m_impulse, data.step.dtRatio)
           m_motorImpulse *= data.step.dtRatio
           let P = b2Vec2(m_impulse.x, m_impulse.y)
           subtractEqual(&vA, multM(P, mA))
           wA -= iA * (b2Cross(m_rA, P) + m_motorImpulse + m_impulse.z)
           addEqual(&vB, multM(P, mB))
           wB += iB * (b2Cross(m_rB, P) + m_motorImpulse + m_impulse.z)
       } else {
           m_impulse.setZero()
           m_motorImpulse = 0.0
       }
       data.velocities.get(m_indexA).v = vA
       data.velocities.get(m_indexA).w = wA
       data.velocities.get(m_indexB).v = vB
       data.velocities.get(m_indexB).w = wB
   }
   
   override func solveVelocityConstraints(_ data: inout b2SolverData) {
       var vA = data.velocities.get(m_indexA).v
       var wA = data.velocities.get(m_indexA).w
       var vB = data.velocities.get(m_indexB).v
       var wB = data.velocities.get(m_indexB).w
       let mA = m_invMassA, mB = m_invMassB
       let iA = m_invIA, iB = m_invIB
       let fixedRotation = (iA + iB == 0.0)
       if m_enableMotor && m_limitState != b2LimitState.equalLimits && fixedRotation == false {
           let Cdot = wB - wA - m_motorSpeed
           var impulse = -m_motorMass * Cdot
           let oldImpulse = m_motorImpulse
           let maxImpulse = data.step.dt * m_maxMotorTorque
           m_motorImpulse = b2ClampF(m_motorImpulse + impulse, -maxImpulse, maxImpulse)
           impulse = m_motorImpulse - oldImpulse
           wA -= iA * impulse
           wB += iB * impulse
       }
       
       if m_enableLimit && m_limitState != b2LimitState.inactiveLimit && fixedRotation == false {
           let Cdot1 = subtract(subtract(add(vB, b2Cross21(wB, m_rB)), vA), b2Cross21(wA, m_rA))
           let Cdot2 = wB - wA
           let Cdot = b2Vec3(Cdot1.x, Cdot1.y, Cdot2)
           var impulse = minus3(m_mass.solve33(Cdot))
           if m_limitState == b2LimitState.equalLimits {
               addEqual3(&m_impulse, impulse)
           } else if m_limitState == b2LimitState.atLowerLimit {
               let newImpulse = m_impulse.z + impulse.z
               if newImpulse < 0.0 {
                   let rhs = add(minus(Cdot1), multM(b2Vec2(m_mass.ez.x, m_mass.ez.y), m_impulse.z))
                   let reduced = m_mass.solve22(rhs)
                   impulse.x = reduced.x
                   impulse.y = reduced.y
                   impulse.z = -m_impulse.z
                   m_impulse.x += reduced.x
                   m_impulse.y += reduced.y
                   m_impulse.z = 0.0
               } else {
                   addEqual3(&m_impulse, impulse)
               }
           } else if m_limitState == b2LimitState.atUpperLimit {
               let newImpulse = m_impulse.z + impulse.z
               if newImpulse > 0.0 {
                   let mass = b2Vec2(m_mass.ez.x, m_mass.ez.y)
                   let rhs = add(minus(Cdot1), multM(mass, m_impulse.z))
                   let reduced = m_mass.solve22(rhs)
                   impulse.x = reduced.x
                   impulse.y = reduced.y
                   impulse.z = -m_impulse.z
                   m_impulse.x += reduced.x
                   m_impulse.y += reduced.y
                   m_impulse.z = 0.0
               } else {
                   addEqual3(&m_impulse, impulse)
               }
           }
           let P = b2Vec2(impulse.x, impulse.y)
           subtractEqual(&vA, multM(P, mA))
           wA -= iA * (b2Cross(m_rA, P) + impulse.z)
           addEqual(&vB, multM(P, mB))
           wB += iB * (b2Cross(m_rB, P) + impulse.z)
       } else {
           let Cdot = subtract(subtract(add(vB, b2Cross21(wB, m_rB)), vA), b2Cross21(wA, m_rA))
           let impulse = m_mass.solve22(minus(Cdot))
           m_impulse.x += impulse.x
           m_impulse.y += impulse.y
           subtractEqual(&vA, multM(impulse, mA))
           wA -= iA * b2Cross(m_rA, impulse)
           addEqual(&vB, multM(impulse, mB))
           wB += iB * b2Cross(m_rB, impulse)
       }
       data.velocities.get(m_indexA).v = vA
       data.velocities.get(m_indexA).w = wA
       data.velocities.get(m_indexB).v = vB
       data.velocities.get(m_indexB).w = wB
   }
   
   override func solvePositionConstraints(_ data: inout b2SolverData) -> Bool {
       var cA = data.positions.get(m_indexA).c
       var aA = data.positions.get(m_indexA).a
       var cB = data.positions.get(m_indexB).c
       var aB = data.positions.get(m_indexB).a
       var qA = b2Rot(aA), qB = b2Rot(aB)
       var angularError: Double = 0.0
       var positionError: Double = 0.0
       let fixedRotation = (m_invIA + m_invIB == 0.0)
       if m_enableLimit && m_limitState != b2LimitState.inactiveLimit && fixedRotation == false {
           let angle = aB - aA - m_referenceAngle
           var limitImpulse: Double = 0.0
           if m_limitState == b2LimitState.equalLimits {
               let C = b2ClampF(angle - m_lowerAngle, -b2_maxAngularCorrection, b2_maxAngularCorrection)
               limitImpulse = -m_motorMass * C
               angularError = abs(C)
           } else if m_limitState == b2LimitState.atLowerLimit {
               var C = angle - m_lowerAngle
               angularError = -C
               C = b2ClampF(C + b2_angularSlop, -b2_maxAngularCorrection, 0.0)
               limitImpulse = -m_motorMass * C
           } else if m_limitState == b2LimitState.atUpperLimit {
               var C = angle - m_upperAngle
               angularError = C
               C = b2ClampF(C - b2_angularSlop, 0.0, b2_maxAngularCorrection)
               limitImpulse = -m_motorMass * C
           }
           aA -= m_invIA * limitImpulse
           aB += m_invIB * limitImpulse
       }
       qA.set(aA)
       qB.set(aB)
       let rA = b2MulR2(qA, subtract(self.m_localAnchorA, self.m_localCenterA))
       let rB = b2MulR2(qB, subtract(self.m_localAnchorB, self.m_localCenterB))
       let C = subtract(subtract(add(cB, rB), cA), rA)
       positionError = C.length()
       let mA = self.m_invMassA, mB = self.m_invMassB
       let iA = self.m_invIA, iB = self.m_invIB
       var K = b2Mat22()
       K.ex.x = mA + mB + iA * rA.y * rA.y + iB * rB.y * rB.y
       K.ex.y = -iA * rA.x * rA.y - iB * rB.x * rB.y
       K.ey.x = K.ex.y
       K.ey.y = mA + mB + iA * rA.x * rA.x + iB * rB.x * rB.x
       let impulse = minus(K.solve(C))
       subtractEqual(&cA, multM(impulse, mA))
       aA -= iA * b2Cross(rA, impulse)
       addEqual(&cB, multM(impulse, mB))
       aB += iB * b2Cross(rB, impulse)
       data.positions.get(m_indexA).c = cA
       data.positions.get(m_indexA).a = aA
       data.positions.get(m_indexB).c = cB
       data.positions.get(m_indexB).a = aB
       return positionError <= b2_linearSlop && angularError <= b2_angularSlop
   }
   
   var m_localAnchorA: b2Vec2
   var m_localAnchorB: b2Vec2
   var m_impulse: b2Vec3
   var m_motorImpulse: Double
   var m_enableMotor: Bool
   var m_maxMotorTorque: Double
   var m_motorSpeed: Double
   var m_enableLimit: Bool
   var m_referenceAngle: Double
   var m_lowerAngle: Double
   var m_upperAngle: Double
   var m_indexA: Int = 0
   var m_indexB: Int = 0
   var m_rA = b2Vec2()
   var m_rB = b2Vec2()
   var m_localCenterA = b2Vec2()
   var m_localCenterB = b2Vec2()
   var m_invMassA: Double = 0.0
   var m_invMassB: Double = 0.0
   var m_invIA: Double = 0.0
   var m_invIB: Double = 0.0
   var m_mass = b2Mat33()
   var m_motorMass: Double = 0.0
   var m_limitState = b2LimitState.inactiveLimit
}

let b2_minPulleyLength: Double = 2.0
open class b2PulleyJointDef : b2JointDef {
   public override init() {
       groundAnchorA = b2Vec2(-1.0, 1.0)
       groundAnchorB = b2Vec2(1.0, 1.0)
       localAnchorA = b2Vec2(-1.0, 0.0)
       localAnchorB = b2Vec2(1.0, 0.0)
       lengthA = 0.0
       lengthB = 0.0
       ratio = 1.0
       super.init()
       type = b2JointType.pulleyJoint
       collideConnected = true
   }
   
   open func initialize(_ bodyA: b2Body, _ bodyB: b2Body, _ groundAnchorA: b2Vec2, _ groundAnchorB: b2Vec2, _ anchorA: b2Vec2, _ anchorB: b2Vec2, _ ratio: Double) {
       self.bodyA = bodyA
       self.bodyB = bodyB
       self.groundAnchorA = groundAnchorA
       self.groundAnchorB = groundAnchorB
       self.localAnchorA = self.bodyA.getLocalPoint(anchorA)
       self.localAnchorB = self.bodyB.getLocalPoint(anchorB)
       let dA = subtract(anchorA, groundAnchorA)
       self.lengthA = dA.length()
       let dB = subtract(anchorB, groundAnchorB)
       self.lengthB = dB.length()
       self.ratio = ratio
   }
   
   open var groundAnchorA: b2Vec2
   open var groundAnchorB: b2Vec2
   open var localAnchorA: b2Vec2
   open var localAnchorB: b2Vec2
   open var lengthA: Double
   open var lengthB: Double
   open var ratio: Double
}

open class b2PulleyJoint : b2Joint {
   var m_groundAnchorA: b2Vec2
   var m_groundAnchorB: b2Vec2
   var m_lengthA: Double
   var m_lengthB: Double
   var m_localAnchorA: b2Vec2
   var m_localAnchorB: b2Vec2
   var m_constant: Double
   var m_ratio: Double
   var m_impulse: Double
   var m_indexA: Int = 0
   var m_indexB: Int = 0
   var m_uA = b2Vec2()
   var m_uB = b2Vec2()
   var m_rA = b2Vec2()
   var m_rB = b2Vec2()
   var m_localCenterA = b2Vec2()
   var m_localCenterB = b2Vec2()
   var m_invMassA: Double = 0.0
   var m_invMassB: Double = 0.0
   var m_invIA: Double = 0.0
   var m_invIB: Double = 0.0
   var m_mass: Double = 0.0
   
   open override var anchorA: b2Vec2 {
       return m_bodyA.getWorldPoint(m_localAnchorA)
   }
   open override var anchorB: b2Vec2 {
       return m_bodyB.getWorldPoint(m_localAnchorB)
   }
   
   open override func getReactionForce(_ inv_dt: Double) -> b2Vec2 {
       let P = multM(m_uB, m_impulse)
       return multM(P, inv_dt)
   }
   open override func getReactionTorque(_ inv_dt: Double) -> Double {
       return 0.0
   }
   open var groundAnchorA: b2Vec2 {
       return m_groundAnchorA
   }
   open var groundAnchorB: b2Vec2 {
       return m_groundAnchorB
   }
   open var lengthA: Double {
       return m_lengthA
   }
   open var lengthB: Double {
       return m_lengthB
   }
   open var ratio: Double {
       return m_ratio
   }
   open var currentLengthA: Double {
       let p = m_bodyA.getWorldPoint(m_localAnchorA)
       let s = m_groundAnchorA
       let d = subtract(p, s)
       return d.length()
   }
   open var currentLengthB: Double {
       let p = m_bodyB.getWorldPoint(m_localAnchorB)
       let s = m_groundAnchorB
       let d = subtract(p, s)
       return d.length()
   }
   
   open override func shiftOrigin(_ newOrigin: b2Vec2) {
       subtractEqual(&m_groundAnchorA, newOrigin)
       subtractEqual(&m_groundAnchorB, newOrigin)
   }
   
   init(_ def: b2PulleyJointDef) {
       m_groundAnchorA = def.groundAnchorA
       m_groundAnchorB = def.groundAnchorB
       m_localAnchorA = def.localAnchorA
       m_localAnchorB = def.localAnchorB
       m_lengthA = def.lengthA
       m_lengthB = def.lengthB
       m_ratio = def.ratio
       m_constant = def.lengthA + m_ratio * def.lengthB
       m_impulse = 0.0
       super.init(def)
   }
   
   override func initVelocityConstraints(_ data: inout b2SolverData) {
       m_indexA = m_bodyA.m_islandIndex
       m_indexB = m_bodyB.m_islandIndex
       m_localCenterA = m_bodyA.m_sweep.localCenter
       m_localCenterB = m_bodyB.m_sweep.localCenter
       m_invMassA = m_bodyA.m_invMass
       m_invMassB = m_bodyB.m_invMass
       m_invIA = m_bodyA.m_invI
       m_invIB = m_bodyB.m_invI
       let cA = data.positions.get(m_indexA).c
       let aA = data.positions.get(m_indexA).a
       var vA = data.velocities.get(m_indexA).v
       var wA = data.velocities.get(m_indexA).w
       let cB = data.positions.get(m_indexB).c
       let aB = data.positions.get(m_indexB).a
       var vB = data.velocities.get(m_indexB).v
       var wB = data.velocities.get(m_indexB).w
       let qA = b2Rot(aA), qB = b2Rot(aB)
       m_rA = b2MulR2(qA, subtract(m_localAnchorA, m_localCenterA))
       m_rB = b2MulR2(qB, subtract(m_localAnchorB, m_localCenterB))
       m_uA = subtract(add(cA, m_rA), m_groundAnchorA)
       m_uB = subtract(add(cB, m_rB), m_groundAnchorB)
       let lengthA = m_uA.length()
       let lengthB = m_uB.length()
       if lengthA > 10.0 * b2_linearSlop {
           mulEqual(&m_uA, 1.0 / lengthA)
       } else {
           m_uA.setZero()
       }
       if lengthB > 10.0 * b2_linearSlop {
           mulEqual(&m_uB, 1.0 / lengthB)
       } else {
           m_uB.setZero()
       }
       let ruA = b2Cross(m_rA, m_uA)
       let ruB = b2Cross(m_rB, m_uB)
       let mA = m_invMassA + m_invIA * ruA * ruA
       let mB = m_invMassB + m_invIB * ruB * ruB
       m_mass = mA + m_ratio * m_ratio * mB
       if m_mass > 0.0 {
           m_mass = 1.0 / m_mass
       }
       if data.step.warmStarting {
           m_impulse *= data.step.dtRatio
           let PA = multM(m_uA, -(m_impulse))
           let PB = multM(m_uB, (-m_ratio * m_impulse))
           addEqual(&vA, multM(PA, m_invMassA))
           wA += m_invIA * b2Cross(m_rA, PA)
           addEqual(&vB, multM(PB, m_invMassB))
           wB += m_invIB * b2Cross(m_rB, PB)
       } else {
           m_impulse = 0.0
       }
       data.velocities.get(m_indexA).v = vA
       data.velocities.get(m_indexA).w = wA
       data.velocities.get(m_indexB).v = vB
       data.velocities.get(m_indexB).w = wB
   }
   
   override func solveVelocityConstraints(_ data: inout b2SolverData) {
       var vA = data.velocities.get(m_indexA).v
       var wA = data.velocities.get(m_indexA).w
       var vB = data.velocities.get(m_indexB).v
       var wB = data.velocities.get(m_indexB).w
       let vpA = add(vA, b2Cross21(wA, m_rA))
       let vpB = add(vB, b2Cross21(wB, m_rB))
       let Cdot = -b2Dot22(m_uA, vpA) - m_ratio * b2Dot22(m_uB, vpB)
       let impulse = -m_mass * Cdot
       m_impulse += impulse
       let PA = multM(m_uA, -impulse)
       let PB = multM(m_uB, -m_ratio * impulse)
       addEqual(&vA, multM(PA, m_invMassA))
       wA += m_invIA * b2Cross(m_rA, PA)
       addEqual(&vB, multM(PB, m_invMassB))
       wB += m_invIB * b2Cross(m_rB, PB)
       data.velocities.get(m_indexA).v = vA
       data.velocities.get(m_indexA).w = wA
       data.velocities.get(m_indexB).v = vB
       data.velocities.get(m_indexB).w = wB
   }
   
   override func solvePositionConstraints(_ data: inout b2SolverData) -> Bool {
       var cA = data.positions.get(m_indexA).c
       var aA = data.positions.get(m_indexA).a
       var cB = data.positions.get(m_indexB).c
       var aB = data.positions.get(m_indexB).a
       let qA = b2Rot(aA), qB = b2Rot(aB)
       let rA = b2MulR2(qA, subtract(m_localAnchorA, m_localCenterA))
       let rB = b2MulR2(qB, subtract(m_localAnchorB, m_localCenterB))
       var uA = subtract(add(cA, rA), m_groundAnchorA)
       var uB = subtract(add(cB, rB), m_groundAnchorB)
       let lengthA = uA.length()
       let lengthB = uB.length()
       if lengthA > 10.0 * b2_linearSlop {
           mulEqual(&uA, 1.0 / lengthA)
       } else {
           uA.setZero()
       }
       if lengthB > 10.0 * b2_linearSlop {
           mulEqual(&uB, 1.0 / lengthB)
       } else {
           uB.setZero()
       }
       let ruA = b2Cross(rA, uA)
       let ruB = b2Cross(rB, uB)
       let mA = m_invMassA + m_invIA * ruA * ruA
       let mB = m_invMassB + m_invIB * ruB * ruB
       var mass = mA + m_ratio * m_ratio * mB
       if mass > 0.0 {
           mass = 1.0 / mass
       }
       let C = m_constant - lengthA - m_ratio * lengthB
       let linearError = abs(C)
       let impulse = -mass * C
       let PA = multM(uA, -impulse)
       let PB = multM(uB, -m_ratio * impulse)
       addEqual(&cA, multM(PA, m_invMassA))
       aA += m_invIA * b2Cross(rA, PA)
       addEqual(&cB, multM(PB, m_invMassB))
       aB += m_invIB * b2Cross(rB, PB)
       data.positions.get(m_indexA).c = cA
       data.positions.get(m_indexA).a = aA
       data.positions.get(m_indexB).c = cB
       data.positions.get(m_indexB).a = aB
       return linearError < b2_linearSlop
   }
}

open class b2PrismaticJointDef : b2JointDef {
   public override init() {
       localAnchorA = b2Vec2()
       localAnchorB = b2Vec2()
       localAxisA = b2Vec2(1.0, 0.0)
       referenceAngle = 0.0
       enableLimit = false
       lowerTranslation = 0.0
       upperTranslation = 0.0
       enableMotor = false
       maxMotorForce = 0.0
       motorSpeed = 0.0
       super.init()
       type = b2JointType.prismaticJoint
   }
   
   open func initialize(_ bA: b2Body, _ bB: b2Body, _ anchor: b2Vec2, _ axis: b2Vec2) {
       bodyA = bA
       bodyB = bB
       localAnchorA = bodyA.getLocalPoint(anchor)
       localAnchorB = bodyB.getLocalPoint(anchor)
       localAxisA = bodyA.getLocalVector(axis)
       referenceAngle = bodyB.angle - bodyA.angle
   }
   
   open var localAnchorA: b2Vec2
   open var localAnchorB: b2Vec2
   open var localAxisA: b2Vec2
   open var referenceAngle: Double
   open var enableLimit: Bool
   open var lowerTranslation: Double
   open var upperTranslation: Double
   open var enableMotor: Bool
   open var maxMotorForce: Double
   open var motorSpeed: Double
}

open class b2PrismaticJoint : b2Joint {
   open override var anchorA: b2Vec2 {
       return m_bodyA.getWorldPoint(m_localAnchorA)
   }
   open override var anchorB: b2Vec2 {
       return m_bodyB.getWorldPoint(m_localAnchorB)
   }
   open override func getReactionForce(_ inv_dt: Double) -> b2Vec2 {
       return multM(add(multM(m_perp, m_impulse.x), multM(m_axis, (m_motorImpulse + m_impulse.z))), inv_dt)
   }
   open override func getReactionTorque(_ inv_dt: Double) -> Double {
       return inv_dt * m_impulse.y
   }
   open var localAnchorA: b2Vec2  { return m_localAnchorA }
   open var localAnchorB: b2Vec2  { return m_localAnchorB }
   open var localAxisA: b2Vec2 { return m_localXAxisA }
   open var referenceAngle: Double { return m_referenceAngle }
   
   open var jointTranslation: Double {
       let pA = m_bodyA.getWorldPoint(m_localAnchorA)
       let pB = m_bodyB.getWorldPoint(m_localAnchorB)
       let d = subtract(pB, pA)
       let axis = m_bodyA.getWorldVector(m_localXAxisA)
       let translation = b2Dot22(d, axis)
       return translation
   }
   
   open var jointSpeed: Double {
       let bA = m_bodyA
       let bB = m_bodyB
       let rA = b2MulR2(bA.m_xf.q, subtract(m_localAnchorA, bA.m_sweep.localCenter))
       let rB = b2MulR2(bB.m_xf.q, subtract(m_localAnchorB, bB.m_sweep.localCenter))
       let p1 = add(bA.m_sweep.c, rA)
       let p2 = add(bB.m_sweep.c, rB)
       let d = subtract(p2, p1)
       let axis = b2MulR2(bA.m_xf.q, m_localXAxisA)
       let vA = bA.m_linearVelocity
       let vB = bB.m_linearVelocity
       let wA = bA.m_angularVelocity
       let wB = bB.m_angularVelocity
       let speed = b2Dot22(d, b2Cross21(wA, axis)) + b2Dot22(axis, subtract(subtract(add(vB, b2Cross21(wB, rB)), vA), b2Cross21(wA, rA)))
       return speed
   }
   
   open var isLimitEnabled: Bool {
       get {
           return m_enableLimit
       }
       set {
           enableLimit(newValue)
       }
   }
   
   open func enableLimit(_ flag: Bool) {
       if flag != m_enableLimit {
           m_bodyA.setAwake(true)
           m_bodyB.setAwake(true)
           m_enableLimit = flag
           m_impulse.z = 0.0
       }
   }
   
   open var lowerLimit: Double {
       return m_lowerTranslation
   }
   
   open var upperLimit: Double {
       return m_upperTranslation
   }
   
   open func setLimits(_ lower: Double, _ upper: Double) {
       if lower != m_lowerTranslation || upper != m_upperTranslation {
           m_bodyA.setAwake(true)
           m_bodyB.setAwake(true)
           m_lowerTranslation = lower
           m_upperTranslation = upper
           m_impulse.z = 0.0
       }
   }
   
   open var isMotorEnabled: Bool {
       get {
           return m_enableMotor
       }
       set {
           enableMotor(newValue)
       }
   }
   
   open func enableMotor(_ flag: Bool) {
       m_bodyA.setAwake(true)
       m_bodyB.setAwake(true)
       m_enableMotor = flag
   }
   
   func setMotorSpeed(_ speed: Double) {
       m_bodyA.setAwake(true)
       m_bodyB.setAwake(true)
       m_motorSpeed = speed
   }
   
   open var motorSpeed: Double {
       get {
           return m_motorSpeed
       }
       set {
           setMotorSpeed(newValue)
       }
   }
   
   open func setMaxMotorForce(_ force: Double) {
       m_bodyA.setAwake(true)
       m_bodyB.setAwake(true)
       m_maxMotorForce = force
   }
   
   open var maxMotorForce: Double {
       get {
           return m_maxMotorForce
       }
       set {
           setMaxMotorForce(newValue)
       }
   }
   
   open func getMotorForce(_ inv_dt: Double) -> Double {
       return inv_dt * m_motorImpulse
   }
   
   init(_ def: b2PrismaticJointDef) {
       m_localAnchorA = def.localAnchorA
       m_localAnchorB = def.localAnchorB
       m_localXAxisA = def.localAxisA
       m_localXAxisA.normalize()
       m_localYAxisA = b2Cross21(1.0, m_localXAxisA)
       m_referenceAngle = def.referenceAngle
       m_impulse = b2Vec3(0.0, 0.0, 0.0)
       m_motorMass = 0.0
       m_motorImpulse = 0.0
       m_lowerTranslation = def.lowerTranslation
       m_upperTranslation = def.upperTranslation
       m_maxMotorForce = def.maxMotorForce
       m_motorSpeed = def.motorSpeed
       m_enableLimit = def.enableLimit
       m_enableMotor = def.enableMotor
       m_limitState = b2LimitState.inactiveLimit
       m_axis = b2Vec2(0.0, 0.0)
       m_perp = b2Vec2(0.0, 0.0)
       super.init(def)
   }
   
   override func initVelocityConstraints(_ data: inout b2SolverData) {
       m_indexA = m_bodyA.m_islandIndex
       m_indexB = m_bodyB.m_islandIndex
       m_localCenterA = m_bodyA.m_sweep.localCenter
       m_localCenterB = m_bodyB.m_sweep.localCenter
       m_invMassA = m_bodyA.m_invMass
       m_invMassB = m_bodyB.m_invMass
       m_invIA = m_bodyA.m_invI
       m_invIB = m_bodyB.m_invI
       let cA = data.positions.get(m_indexA).c
       let aA = data.positions.get(m_indexA).a
       var vA = data.velocities.get(m_indexA).v
       var wA = data.velocities.get(m_indexA).w
       let cB = data.positions.get(m_indexB).c
       let aB = data.positions.get(m_indexB).a
       var vB = data.velocities.get(m_indexB).v
       var wB = data.velocities.get(m_indexB).w
       let qA = b2Rot(aA), qB = b2Rot(aB)
       let rA = b2MulR2(qA, subtract(m_localAnchorA, m_localCenterA))
       let rB = b2MulR2(qB, subtract(m_localAnchorB, m_localCenterB))
       let d = subtract(add(subtract(cB, cA), rB), rA)
       let mA = m_invMassA, mB = m_invMassB
       let iA = m_invIA, iB = m_invIB
       self.m_axis = b2MulR2(qA, self.m_localXAxisA)
       self.m_a1 = b2Cross(add(d, rA), self.m_axis)
       self.m_a2 = b2Cross(rB, self.m_axis)
       self.m_motorMass = mA + mB + iA * self.m_a1 * self.m_a1 + iB * self.m_a2 * self.m_a2
       if self.m_motorMass > 0.0 {
           self.m_motorMass = 1.0 / self.m_motorMass
       }
       self.m_perp = b2MulR2(qA, self.m_localYAxisA)
       self.m_s1 = b2Cross(add(d, rA), self.m_perp)
       self.m_s2 = b2Cross(rB, self.m_perp)
       let k11 = mA + mB + iA * self.m_s1 * self.m_s1 + iB * self.m_s2 * self.m_s2
       let k12 = iA * self.m_s1 + iB * self.m_s2
       let k13 = iA * self.m_s1 * self.m_a1 + iB * self.m_s2 * self.m_a2
       var k22 = iA + iB
       if k22 == 0.0 {
           k22 = 1.0
       }
       let k23 = iA * self.m_a1 + iB * self.m_a2
       let k33 = mA + mB + iA * self.m_a1 * self.m_a1 + iB * self.m_a2 * self.m_a2
       self.m_K.ex.set(k11, k12, k13)
       self.m_K.ey.set(k12, k22, k23)
       self.m_K.ez.set(k13, k23, k33)
       if m_enableLimit {
           let jointTranslation = b2Dot22(m_axis, d)
           if abs(m_upperTranslation - m_lowerTranslation) < 2.0 * b2_linearSlop {
               m_limitState = b2LimitState.equalLimits
           } else if jointTranslation <= m_lowerTranslation {
               if m_limitState != b2LimitState.atLowerLimit {
                   m_limitState = b2LimitState.atLowerLimit
                   m_impulse.z = 0.0
               }
           } else if jointTranslation >= m_upperTranslation {
               if m_limitState != b2LimitState.atUpperLimit {
                   m_limitState = b2LimitState.atUpperLimit
                   m_impulse.z = 0.0
               }
           } else {
               m_limitState = b2LimitState.inactiveLimit
               m_impulse.z = 0.0
           }
       } else {
           m_limitState = b2LimitState.inactiveLimit
           m_impulse.z = 0.0
       }
       if m_enableMotor == false {
           m_motorImpulse = 0.0
       }
       if data.step.warmStarting {
           mulMEqual3(&m_impulse, data.step.dtRatio)
           m_motorImpulse *= data.step.dtRatio
           let P = add(multM(m_perp, m_impulse.x), multM(m_axis, (m_motorImpulse + m_impulse.z)))
           let LA = m_impulse.x * m_s1 + m_impulse.y + (m_motorImpulse + m_impulse.z) * m_a1
           let LB = m_impulse.x * m_s2 + m_impulse.y + (m_motorImpulse + m_impulse.z) * m_a2
           subtractEqual(&vA, multM(P, mA))
           wA -= iA * LA
           addEqual(&vB, multM(P, mB))
           wB += iB * LB
       } else {
           m_impulse.setZero()
           m_motorImpulse = 0.0
       }
       data.velocities.get(m_indexA).v = vA
       data.velocities.get(m_indexA).w = wA
       data.velocities.get(m_indexB).v = vB
       data.velocities.get(m_indexB).w = wB
   }
   
   override func solveVelocityConstraints(_ data: inout b2SolverData) {
       var vA = data.velocities.get(m_indexA).v
       var wA = data.velocities.get(m_indexA).w
       var vB = data.velocities.get(m_indexB).v
       var wB = data.velocities.get(m_indexB).w
       let mA = m_invMassA, mB = m_invMassB
       let iA = m_invIA, iB = m_invIB
       if m_enableMotor && m_limitState != b2LimitState.equalLimits {
           let Cdot = b2Dot22(m_axis, subtract(vB, vA)) + m_a2 * wB - m_a1 * wA
           var impulse = m_motorMass * (m_motorSpeed - Cdot)
           let oldImpulse = m_motorImpulse
           let maxImpulse = data.step.dt * m_maxMotorForce
           m_motorImpulse = b2ClampF(m_motorImpulse + impulse, -maxImpulse, maxImpulse)
           impulse = m_motorImpulse - oldImpulse
           let P = multM(m_axis, impulse)
           let LA = impulse * m_a1
           let LB = impulse * m_a2
           subtractEqual(&vA, multM(P, mA))
           wA -= iA * LA
           addEqual(&vB, multM(P, mB))
           wB += iB * LB
       }
       var Cdot1 = b2Vec2()
       Cdot1.x = b2Dot22(m_perp, subtract(vB, vA)) + m_s2 * wB - m_s1 * wA
       Cdot1.y = wB - wA
       if m_enableLimit && m_limitState != b2LimitState.inactiveLimit {
           let Cdot2 = b2Dot22(m_axis, subtract(vB, vA)) + m_a2 * wB - m_a1 * wA
           let Cdot = b2Vec3(Cdot1.x, Cdot1.y, Cdot2)
           let f1 = m_impulse
           var df =  m_K.solve33(minus3(Cdot))
           addEqual3(&m_impulse, df)
           if m_limitState == b2LimitState.atLowerLimit {
               m_impulse.z = max(m_impulse.z, 0.0)
           } else if m_limitState == b2LimitState.atUpperLimit {
               m_impulse.z = min(m_impulse.z, 0.0)
           }
           let mkk = b2Vec2(m_K.ez.x, m_K.ez.y);
           let b = subtract(minus(Cdot1), multM(mkk, (m_impulse.z - f1.z)))
           let ffx = b2Vec2(f1.x, f1.y);
           let f2r = add(m_K.solve22(b), ffx)
           m_impulse.x = f2r.x
           m_impulse.y = f2r.y
           df = subtract3(m_impulse, f1)
           let P = add(multM(m_perp, df.x), multM(m_axis, df.z))
           let LA = df.x * m_s1 + df.y + df.z * m_a1
           let LB = df.x * m_s2 + df.y + df.z * m_a2
           subtractEqual(&vA, multM(P, mA))
           wA -= iA * LA
           addEqual(&vB, multM(P, mB))
           wB += iB * LB
       } else {
           let df = m_K.solve22(minus(Cdot1))
           m_impulse.x += df.x
           m_impulse.y += df.y
           let P = multM(m_perp, df.x)
           let LA = df.x * m_s1 + df.y
           let LB = df.x * m_s2 + df.y
           subtractEqual(&vA, multM(P, mA))
           wA -= iA * LA
           addEqual(&vB, multM(P, mB))
           wB += iB * LB
       }
       data.velocities.get(m_indexA).v = vA
       data.velocities.get(m_indexA).w = wA
       data.velocities.get(m_indexB).v = vB
       data.velocities.get(m_indexB).w = wB
   }
   
   override func solvePositionConstraints(_ data: inout b2SolverData) -> Bool {
       var cA = data.positions.get(m_indexA).c
       var aA = data.positions.get(m_indexA).a
       var cB = data.positions.get(m_indexB).c
       var aB = data.positions.get(m_indexB).a
       let qA = b2Rot(aA), qB = b2Rot(aB)
       let mA = m_invMassA, mB = m_invMassB
       let iA = m_invIA, iB = m_invIB
       let rA = b2MulR2(qA, subtract(m_localAnchorA, m_localCenterA))
       let rB = b2MulR2(qB, subtract(m_localAnchorB, m_localCenterB))
       let d = subtract(subtract(add(cB, rB), cA), rA)
       let axis = b2MulR2(qA, m_localXAxisA)
       let a1 = b2Cross(add(d, rA), axis)
       let a2 = b2Cross(rB, axis)
       let perp = b2MulR2(qA, m_localYAxisA)
       let s1 = b2Cross(add(d, rA), perp)
       let s2 = b2Cross(rB, perp)
       var impulse = b2Vec3()
       var C1 = b2Vec2()
       C1.x = b2Dot22(perp, d)
       C1.y = aB - aA - m_referenceAngle
       var linearError = abs(C1.x)
       let angularError = abs(C1.y)
       var active = false
       var C2: Double = 0.0
       if m_enableLimit {
           let translation = b2Dot22(axis, d)
           if abs(m_upperTranslation - m_lowerTranslation) < 2.0 * b2_linearSlop {
               C2 = b2ClampF(translation, -b2_maxLinearCorrection, b2_maxLinearCorrection)
               linearError = max(linearError, abs(translation))
               active = true
           } else if translation <= m_lowerTranslation {
               C2 = b2ClampF(translation - m_lowerTranslation + b2_linearSlop, -b2_maxLinearCorrection, 0.0)
               linearError = max(linearError, m_lowerTranslation - translation)
               active = true
           } else if translation >= m_upperTranslation {
               C2 = b2ClampF(translation - m_upperTranslation - b2_linearSlop, 0.0, b2_maxLinearCorrection)
               linearError = max(linearError, translation - m_upperTranslation)
               active = true
           }
       }
       if active {
           let k11 = mA + mB + iA * s1 * s1 + iB * s2 * s2
           let k12 = iA * s1 + iB * s2
           let k13 = iA * s1 * a1 + iB * s2 * a2
           var k22 = iA + iB
           if k22 == 0.0 {
               k22 = 1.0
           }
           let k23 = iA * a1 + iB * a2
           let k33 = mA + mB + iA * a1 * a1 + iB * a2 * a2
           var K = b2Mat33()
           K.ex.set(k11, k12, k13)
           K.ey.set(k12, k22, k23)
           K.ez.set(k13, k23, k33)
           var C = b2Vec3()
           C.x = C1.x
           C.y = C1.y
           C.z = C2
           impulse = K.solve33(minus3(C))
       } else {
           let k11 = mA + mB + iA * s1 * s1 + iB * s2 * s2
           let k12 = iA * s1 + iB * s2
           var k22 = iA + iB
           if k22 == 0.0 {
               k22 = 1.0
           }
           var K = b2Mat22()
           K.ex.set(k11, k12)
           K.ey.set(k12, k22)
           let impulse1 = K.solve(minus(C1))
           impulse.x = impulse1.x
           impulse.y = impulse1.y
           impulse.z = 0.0
       }
       let P = add(multM(perp, impulse.x), multM(axis, impulse.z))
       let LA = impulse.x * s1 + impulse.y + impulse.z * a1
       let LB = impulse.x * s2 + impulse.y + impulse.z * a2
       subtractEqual(&cA, multM(P, mA))
       aA -= iA * LA
       addEqual(&cB, multM(P, mB))
       aB += iB * LB
       data.positions.get(m_indexA).c = cA
       data.positions.get(m_indexA).a = aA
       data.positions.get(m_indexB).c = cB
       data.positions.get(m_indexB).a = aB
       return linearError <= b2_linearSlop && angularError <= b2_angularSlop
   }
   
   var m_localAnchorA: b2Vec2
   var m_localAnchorB: b2Vec2
   var m_localXAxisA: b2Vec2
   var m_localYAxisA: b2Vec2
   var m_referenceAngle: Double
   var m_impulse: b2Vec3
   var m_motorImpulse: Double
   var m_lowerTranslation: Double
   var m_upperTranslation: Double
   var m_maxMotorForce: Double
   var m_motorSpeed: Double
   var m_enableLimit: Bool
   var m_enableMotor: Bool
   var m_limitState: b2LimitState
   var m_indexA: Int = 0
   var m_indexB: Int = 0
   var m_localCenterA = b2Vec2()
   var m_localCenterB = b2Vec2()
   var m_invMassA: Double = 0.0
   var m_invMassB: Double = 0.0
   var m_invIA: Double = 0.0
   var m_invIB: Double = 0.0
   var m_axis = b2Vec2()
   var m_perp = b2Vec2()
   var m_s1: Double = 0.0
   var m_s2: Double = 0.0
   var m_a1: Double = 0.0
   var m_a2: Double = 0.0
   var m_K = b2Mat33()
   var m_motorMass: Double = 0.0
}

open class b2MouseJointDef : b2JointDef {
   public override init() {
       target = b2Vec2()
       maxForce = 0.0
       frequencyHz = 5.0
       dampingRatio = 0.7
       super.init()
       type = b2JointType.mouseJoint
   }
   
   open var target: b2Vec2
   open var maxForce: Double
   open var frequencyHz: Double
   open var dampingRatio: Double
}

open class b2MouseJoint : b2Joint {
   open override var anchorA: b2Vec2 {
       return m_targetA
   }
   open override var anchorB: b2Vec2 {
       return m_bodyB.getWorldPoint(m_localAnchorB)
   }
   open override func getReactionForce(_ inv_dt: Double) -> b2Vec2 {
       return multM(m_impulse, inv_dt)
   }
   open override func getReactionTorque(_ inv_dt: Double) -> Double {
       return inv_dt * 0.0
   }
   open func setTarget(_ target: b2Vec2) {
       if m_bodyB.isAwake == false {
           m_bodyB.setAwake(true)
       }
       m_targetA = target
   }
   open var target: b2Vec2 {
       get {
           return m_targetA
       }
       set {
           setTarget(newValue)
       }
   }
   open func setMaxForce(_ force: Double) {
       m_maxForce = force
   }
   open var maxForce: Double {
       get {
           return m_maxForce
       }
       set {
           setMaxForce(newValue)
       }
   }
   open func setFrequency(_ hz: Double) {
       m_frequencyHz = hz
   }
   open var frequency: Double {
       get {
           return m_frequencyHz
       }
       set {
           setFrequency(newValue)
       }
   }
   open func setDampingRatio(_ ratio: Double) {
       m_dampingRatio = ratio
   }
   open var dampingRatio: Double {
       get {
           return m_dampingRatio
       }
       set {
           setDampingRatio(newValue)
       }
   }
   open override func shiftOrigin(_ newOrigin: b2Vec2) {
       subtractEqual(&m_targetA, newOrigin)
   }
   
   init(_ def: b2MouseJointDef) {
       m_targetA = def.target
       m_maxForce = def.maxForce
       m_impulse = b2Vec2(0.0, 0.0)
       m_frequencyHz = def.frequencyHz
       m_dampingRatio = def.dampingRatio
       m_beta = 0.0
       m_gamma = 0.0
       super.init(def)
       m_localAnchorB = b2MulTT2(m_bodyB.transform, m_targetA)
   }
   
   override func initVelocityConstraints(_ data: inout b2SolverData) {
       m_indexB = m_bodyB.m_islandIndex
       m_localCenterB = m_bodyB.m_sweep.localCenter
       m_invMassB = m_bodyB.m_invMass
       m_invIB = m_bodyB.m_invI
       let cB = data.positions.get(m_indexB).c
       let aB = data.positions.get(m_indexB).a
       var vB = data.velocities.get(m_indexB).v
       var wB = data.velocities.get(m_indexB).w
       let qB = b2Rot(aB)
       let mass = m_bodyB.mass
       let omega = 2.0 * b2_pi * m_frequencyHz
       let d = 2.0 * mass * m_dampingRatio * omega
       let k = mass * (omega * omega)
       let h = data.step.dt
       m_gamma = h * (d + h * k)
       if m_gamma != 0.0 {
           m_gamma = 1.0 / m_gamma
       }
       m_beta = h * k * m_gamma
       m_rB = b2MulR2(qB, subtract(m_localAnchorB, m_localCenterB))
       var K = b2Mat22()
       K.ex.x = m_invMassB + m_invIB * m_rB.y * m_rB.y + m_gamma
       K.ex.y = -m_invIB * m_rB.x * m_rB.y
       K.ey.x = K.ex.y
       K.ey.y = m_invMassB + m_invIB * m_rB.x * m_rB.x + m_gamma
       m_mass = K.getInverse()
       m_C = subtract(add(cB, m_rB), m_targetA)
       mulEqual(&m_C, m_beta)
       wB *= 0.98
       if data.step.warmStarting {
           mulEqual(&m_impulse, data.step.dtRatio)
           addEqual(&vB, multM(m_impulse, m_invMassB))
           wB += m_invIB * b2Cross(m_rB, m_impulse)
       } else {
           m_impulse.setZero()
       }
       data.velocities.get(m_indexB).v = vB
       data.velocities.get(m_indexB).w = wB
   }
   
   override func solveVelocityConstraints(_ data: inout b2SolverData) {
       var vB = data.velocities.get(m_indexB).v
       var wB = data.velocities.get(m_indexB).w
       let Cdot = add(vB, b2Cross21(wB, m_rB))
       var impulse = b2Mul22(m_mass, minus(add(add(Cdot, m_C), multM(m_impulse, m_gamma))))
       let oldImpulse = m_impulse
       addEqual(&m_impulse, impulse)
       let maxImpulse = data.step.dt * m_maxForce
       if m_impulse.lengthSquared() > maxImpulse * maxImpulse {
           mulEqual(&m_impulse, maxImpulse / m_impulse.length())
       }
       impulse = subtract(m_impulse, oldImpulse)
       addEqual(&vB, multM(impulse, m_invMassB))
       wB += m_invIB * b2Cross(m_rB, impulse)
       data.velocities.get(m_indexB).v = vB
       data.velocities.get(m_indexB).w = wB
   }
   
   override func solvePositionConstraints(_ data: inout b2SolverData) -> Bool {
       return true
   }
   
   var m_localAnchorB: b2Vec2!
   var m_targetA: b2Vec2
   var m_frequencyHz: Double
   var m_dampingRatio: Double
   var m_beta: Double
   var m_impulse: b2Vec2
   var m_maxForce: Double
   var m_gamma: Double
   var m_indexA: Int = 0
   var m_indexB: Int = 0
   var m_rB = b2Vec2()
   var m_localCenterB = b2Vec2()
   var m_invMassB: Double = 0.0
   var m_invIB: Double = 0.0
   var m_mass = b2Mat22()
   var m_C = b2Vec2()
}

open class b2GearJointDef : b2JointDef {
   public override init() {
       joint1 = nil
       joint2 = nil
       ratio = 1.0
       super.init()
       type = b2JointType.gearJoint
   }
   open var joint1: b2Joint! = nil
   open var joint2: b2Joint! = nil
   open var ratio: Double = 1.0
}

open class b2GearJoint : b2Joint {
   open override var anchorA: b2Vec2 {
       return m_bodyA.getWorldPoint(m_localAnchorA)
   }
   open override var anchorB: b2Vec2 {
       return m_bodyB.getWorldPoint(m_localAnchorB)
   }
   
   open override func getReactionForce(_ inv_dt: Double) -> b2Vec2 {
       let P = multM(m_JvAC, m_impulse)
       return multM(P, inv_dt)
   }
   
   open override func getReactionTorque(_ inv_dt: Double) -> Double {
       let L = m_impulse * m_JwA
       return inv_dt * L
   }
   
   open var joint1: b2Joint { return m_joint1 }
   
   open var joint2: b2Joint { return m_joint2 }
   
   open func setRatio(_ ratio: Double) {
       m_ratio = ratio
   }
   
   open var ratio: Double {
       get {
           return m_ratio
       }
       set {
           setRatio(newValue)
       }
   }
   
   init(_ def: b2GearJointDef) {
       m_joint1 = def.joint1
       m_joint2 = def.joint2
       m_typeA = m_joint1.type
       m_typeB = m_joint2.type
       super.init(def)
       var coordinateA: Double, coordinateB: Double
       m_bodyC = m_joint1.bodyA
       m_bodyA = m_joint1.bodyB
       let xfA = m_bodyA.m_xf
       let aA = m_bodyA.m_sweep.a
       let xfC = m_bodyC.m_xf
       let aC = m_bodyC.m_sweep.a
       if m_typeA == b2JointType.revoluteJoint {
           let revolute = def.joint1 as! b2RevoluteJoint
           m_localAnchorC = revolute.m_localAnchorA
           m_localAnchorA = revolute.m_localAnchorB
           m_referenceAngleA = revolute.m_referenceAngle
           m_localAxisC.setZero()
           coordinateA = aA - aC - m_referenceAngleA
       } else {
           let prismatic = def.joint1 as! b2PrismaticJoint
           m_localAnchorC = prismatic.m_localAnchorA
           m_localAnchorA = prismatic.m_localAnchorB
           m_referenceAngleA = prismatic.m_referenceAngle
           m_localAxisC = prismatic.m_localXAxisA
           let pC = m_localAnchorC
           let pA = b2MulTR2(xfC.q, add(b2MulR2(xfA.q, m_localAnchorA), (subtract(xfA.p, xfC.p))))
           coordinateA = b2Dot22(subtract(pA, pC), m_localAxisC)
       }
       m_bodyD = m_joint2.bodyA
       m_bodyB = m_joint2.bodyB
       let xfB = m_bodyB.m_xf
       let aB = m_bodyB.m_sweep.a
       let xfD = m_bodyD.m_xf
       let aD = m_bodyD.m_sweep.a
       if m_typeB == b2JointType.revoluteJoint {
           let revolute = def.joint2 as! b2RevoluteJoint
           m_localAnchorD = revolute.m_localAnchorA
           m_localAnchorB = revolute.m_localAnchorB
           m_referenceAngleB = revolute.m_referenceAngle
           m_localAxisD.setZero()
           coordinateB = aB - aD - m_referenceAngleB
       } else {
           let prismatic = def.joint2 as! b2PrismaticJoint
           m_localAnchorD = prismatic.m_localAnchorA
           m_localAnchorB = prismatic.m_localAnchorB
           m_referenceAngleB = prismatic.m_referenceAngle
           m_localAxisD = prismatic.m_localXAxisA
           let pD = m_localAnchorD
           let pB = b2MulTR2(xfD.q, add(b2MulR2(xfB.q, m_localAnchorB), subtract(xfB.p, xfD.p)))
           coordinateB = b2Dot22(subtract(pB, pD), m_localAxisD)
       }
       m_ratio = def.ratio
       m_constant = coordinateA + m_ratio * coordinateB
       m_impulse = 0.0
   }
   
   override func initVelocityConstraints(_ data: inout b2SolverData) {
       m_indexA = m_bodyA.m_islandIndex
       m_indexB = m_bodyB.m_islandIndex
       m_indexC = m_bodyC.m_islandIndex
       m_indexD = m_bodyD.m_islandIndex
       m_lcA = m_bodyA.m_sweep.localCenter
       m_lcB = m_bodyB.m_sweep.localCenter
       m_lcC = m_bodyC.m_sweep.localCenter
       m_lcD = m_bodyD.m_sweep.localCenter
       m_mA = m_bodyA.m_invMass
       m_mB = m_bodyB.m_invMass
       m_mC = m_bodyC.m_invMass
       m_mD = m_bodyD.m_invMass
       m_iA = m_bodyA.m_invI
       m_iB = m_bodyB.m_invI
       m_iC = m_bodyC.m_invI
       m_iD = m_bodyD.m_invI
       let aA = data.positions.get(m_indexA).a
       var vA = data.velocities.get(m_indexA).v
       var wA = data.velocities.get(m_indexA).w
       let aB = data.positions.get(m_indexB).a
       var vB = data.velocities.get(m_indexB).v
       var wB = data.velocities.get(m_indexB).w
       let aC = data.positions.get(m_indexC).a
       var vC = data.velocities.get(m_indexC).v
       var wC = data.velocities.get(m_indexC).w
       let aD = data.positions.get(m_indexD).a
       var vD = data.velocities.get(m_indexD).v
       var wD = data.velocities.get(m_indexD).w
       let qA = b2Rot(aA), qB = b2Rot(aB), qC = b2Rot(aC), qD = b2Rot(aD)
       m_mass = 0.0
       if m_typeA == b2JointType.revoluteJoint {
           m_JvAC.setZero()
           m_JwA = 1.0
           m_JwC = 1.0
           m_mass += m_iA + m_iC
       } else {
           let u = b2MulR2(qC, m_localAxisC)
           let rC = b2MulR2(qC, subtract(m_localAnchorC, m_lcC))
           let rA = b2MulR2(qA, subtract(m_localAnchorA, m_lcA))
           m_JvAC = u
           m_JwC = b2Cross(rC, u)
           m_JwA = b2Cross(rA, u)
           m_mass += m_mC + m_mA + m_iC * m_JwC * m_JwC + m_iA * m_JwA * m_JwA
       }
       if m_typeB == b2JointType.revoluteJoint {
           m_JvBD.setZero()
           m_JwB = m_ratio
           m_JwD = m_ratio
           m_mass += m_ratio * m_ratio * (m_iB + m_iD)
       } else {
           let u = b2MulR2(qD, m_localAxisD)
           let rD = b2MulR2(qD, subtract(m_localAnchorD, m_lcD))
           let rB = b2MulR2(qB, subtract(m_localAnchorB, m_lcB))
           m_JvBD = multM(u, m_ratio)
           m_JwD = m_ratio * b2Cross(rD, u)
           m_JwB = m_ratio * b2Cross(rB, u)
           let mass1 = m_ratio * m_ratio * (m_mD + m_mB)
           let mass2 = m_iD * m_JwD * m_JwD + m_iB * m_JwB * m_JwB
           let mass = mass1 + mass2
           m_mass += mass
       }
       m_mass = m_mass > 0.0 ? 1.0 / m_mass : 0.0
       if data.step.warmStarting {
           addEqual(&vA, multM(m_JvAC, (m_mA * m_impulse)))
           wA += m_iA * m_impulse * m_JwA
           addEqual(&vB, multM(m_JvBD, (m_mB * m_impulse)))
           wB += m_iB * m_impulse * m_JwB
           subtractEqual(&vC, multM(m_JvAC, (m_mC * m_impulse)))
           wC -= m_iC * m_impulse * m_JwC
           subtractEqual(&vD, multM(m_JvBD, (m_mD * m_impulse)))
           wD -= m_iD * m_impulse * m_JwD
       } else {
           m_impulse = 0.0
       }
       data.velocities.get(m_indexA).v = vA
       data.velocities.get(m_indexA).w = wA
       data.velocities.get(m_indexB).v = vB
       data.velocities.get(m_indexB).w = wB
       data.velocities.get(m_indexC).v = vC
       data.velocities.get(m_indexC).w = wC
       data.velocities.get(m_indexD).v = vD
       data.velocities.get(m_indexD).w = wD
   }
   
   override func solveVelocityConstraints(_ data: inout b2SolverData) {
       var vA = data.velocities.get(m_indexA).v
       var wA = data.velocities.get(m_indexA).w
       var vB = data.velocities.get(m_indexB).v
       var wB = data.velocities.get(m_indexB).w
       var vC = data.velocities.get(m_indexC).v
       var wC = data.velocities.get(m_indexC).w
       var vD = data.velocities.get(m_indexD).v
       var wD = data.velocities.get(m_indexD).w
       var Cdot = b2Dot22(m_JvAC, subtract(vA, vC)) + b2Dot22(m_JvBD, subtract(vB, vD))
       Cdot += (m_JwA * wA - m_JwC * wC) + (m_JwB * wB - m_JwD * wD)
       let impulse = -m_mass * Cdot
       m_impulse += impulse
       addEqual(&vA, multM(m_JvAC, (m_mA * impulse)))
       wA += m_iA * impulse * m_JwA
       addEqual(&vB, multM(m_JvBD, (m_mB * impulse)))
       wB += m_iB * impulse * m_JwB
       subtractEqual(&vC, multM(m_JvAC, (m_mC * impulse)))
       wC -= m_iC * impulse * m_JwC
       subtractEqual(&vD, multM(m_JvBD, (m_mD * impulse)))
       wD -= m_iD * impulse * m_JwD
       data.velocities.get(m_indexA).v = vA
       data.velocities.get(m_indexA).w = wA
       data.velocities.get(m_indexB).v = vB
       data.velocities.get(m_indexB).w = wB
       data.velocities.get(m_indexC).v = vC
       data.velocities.get(m_indexC).w = wC
       data.velocities.get(m_indexD).v = vD
       data.velocities.get(m_indexD).w = wD
   }
   
   override func solvePositionConstraints(_ data: inout b2SolverData) -> Bool {
       var cA = data.positions.get(m_indexA).c
       var aA = data.positions.get(m_indexA).a
       var cB = data.positions.get(m_indexB).c
       var aB = data.positions.get(m_indexB).a
       var cC = data.positions.get(m_indexC).c
       var aC = data.positions.get(m_indexC).a
       var cD = data.positions.get(m_indexD).c
       var aD = data.positions.get(m_indexD).a
       let qA = b2Rot(aA), qB = b2Rot(aB), qC = b2Rot(aC), qD = b2Rot(aD)
       let linearError: Double = 0.0
       var coordinateA: Double = 0, coordinateB: Double = 0
       var JvAC = b2Vec2(), JvBD = b2Vec2()
       var JwA: Double = 0, JwB: Double = 0, JwC: Double = 0, JwD: Double = 0
       var mass: Double = 0.0
       if m_typeA == b2JointType.revoluteJoint {
           JvAC.setZero()
           JwA = 1.0
           JwC = 1.0
           mass += m_iA + m_iC
           coordinateA = aA - aC - m_referenceAngleA
       } else {
           let u = b2MulR2(qC, m_localAxisC)
           let rC = b2MulR2(qC, subtract(m_localAnchorC, m_lcC))
           let rA = b2MulR2(qA, subtract(m_localAnchorA, m_lcA))
           JvAC = u
           JwC = b2Cross(rC, u)
           JwA = b2Cross(rA, u)
           mass += m_mC + m_mA + m_iC * JwC * JwC + m_iA * JwA * JwA
           let pC = subtract(m_localAnchorC, m_lcC)
           let pA = b2MulTR2(qC, add(rA, subtract(cA, cC)))
           coordinateA = b2Dot22(subtract(pA, pC), m_localAxisC)
       }
       if m_typeB == b2JointType.revoluteJoint {
           JvBD.setZero()
           JwB = m_ratio
           JwD = m_ratio
           mass += m_ratio * m_ratio * (m_iB + m_iD)
           coordinateB = aB - aD - m_referenceAngleB
       } else {
           let u = b2MulR2(qD, m_localAxisD)
           let rD = b2MulR2(qD, subtract(m_localAnchorD, m_lcD))
           let rB = b2MulR2(qB, subtract(m_localAnchorB, m_lcB))
           JvBD = multM(u, m_ratio)
           JwD = m_ratio * b2Cross(rD, u)
           JwB = m_ratio * b2Cross(rB, u)
           mass += m_ratio * m_ratio * (m_mD + m_mB) + m_iD * JwD * JwD + m_iB * JwB * JwB
           let pD = subtract(m_localAnchorD, m_lcD)
           let pB = b2MulTR2(qD, add(rB, subtract(cB, cD)))
           coordinateB = b2Dot22(subtract(pB, pD), m_localAxisD)
       }
       let C = (coordinateA + m_ratio * coordinateB) - m_constant
       var impulse: Double = 0.0
       if mass > 0.0 {
           impulse = -C / mass
       }
       addEqual(&cA, multM(JvAC, m_mA * impulse))
       aA += m_iA * impulse * JwA
       addEqual(&cB, multM(JvBD, m_mB * impulse))
       aB += m_iB * impulse * JwB
       subtractEqual(&cC, multM(JvAC, m_mC * impulse))
       aC -= m_iC * impulse * JwC
       subtractEqual(&cD, multM(JvBD, m_mD * impulse))
       aD -= m_iD * impulse * JwD
       data.positions.get(m_indexA).c = cA
       data.positions.get(m_indexA).a = aA
       data.positions.get(m_indexB).c = cB
       data.positions.get(m_indexB).a = aB
       data.positions.get(m_indexC).c = cC
       data.positions.get(m_indexC).a = aC
       data.positions.get(m_indexD).c = cD
       data.positions.get(m_indexD).a = aD
       return linearError < b2_linearSlop
   }
   var m_joint1: b2Joint
   var m_joint2: b2Joint
   var m_typeA: b2JointType
   var m_typeB: b2JointType
   var m_bodyC: b2Body!
   var m_bodyD: b2Body!
   var m_localAnchorA = b2Vec2()
   var m_localAnchorB = b2Vec2()
   var m_localAnchorC = b2Vec2()
   var m_localAnchorD = b2Vec2()
   var m_localAxisC = b2Vec2()
   var m_localAxisD = b2Vec2()
   var m_referenceAngleA: Double = 0.0
   var m_referenceAngleB: Double = 0.0
   var m_constant: Double = 0.0
   var m_ratio: Double = 0.0
   var m_impulse: Double = 0.0
   var m_indexA: Int = 0
   var m_indexB: Int = 0
   var m_indexC: Int = 0
   var m_indexD: Int = 0
   var m_lcA = b2Vec2()
   var m_lcB = b2Vec2()
   var m_lcC = b2Vec2()
   var m_lcD = b2Vec2()
   var m_mA: Double = 0.0
   var m_mB: Double = 0.0
   var m_mC: Double = 0.0
   var m_mD: Double = 0.0
   var m_iA: Double = 0.0
   var m_iB: Double = 0.0
   var m_iC: Double = 0.0
   var m_iD: Double = 0.0
   var m_JvAC = b2Vec2()
   var m_JvBD = b2Vec2()
   var m_JwA: Double = 0.0
   var m_JwB: Double = 0.0
   var m_JwC: Double = 0.0
   var m_JwD: Double = 0.0
   var m_mass: Double = 0.0
}

open class b2FrictionJointDef : b2JointDef {
   public override init() {
       localAnchorA = b2Vec2()
       localAnchorB = b2Vec2()
       maxForce = 0.0
       maxTorque = 0.0
       super.init()
       type = b2JointType.frictionJoint
   }
   
   open func initialize(_ bA: b2Body, _ bB: b2Body, _ anchor: b2Vec2) {
       bodyA = bA
       bodyB = bB
       localAnchorA = bodyA.getLocalPoint(anchor)
       localAnchorB = bodyB.getLocalPoint(anchor)
   }
   
   open var localAnchorA: b2Vec2
   open var localAnchorB: b2Vec2
   open var maxForce: Double
   open var maxTorque: Double
}

open class b2FrictionJoint : b2Joint {
   var m_localAnchorA: b2Vec2
   var m_localAnchorB: b2Vec2
   var m_linearImpulse: b2Vec2
   var m_angularImpulse: Double
   var m_maxForce: Double
   var m_maxTorque: Double
   var m_indexA: Int = 0
   var m_indexB: Int = 0
   var m_rA = b2Vec2()
   var m_rB = b2Vec2()
   var m_localCenterA = b2Vec2()
   var m_localCenterB = b2Vec2()
   var m_invMassA: Double = 0.0
   var m_invMassB: Double = 0.0
   var m_invIA: Double = 0.0
   var m_invIB: Double = 0.0
   var m_linearMass = b2Mat22()
   var m_angularMass: Double = 0.0
   open override var anchorA: b2Vec2 {
       return m_bodyA.getWorldPoint(m_localAnchorA)
   }
   open override var anchorB: b2Vec2 {
       return m_bodyB.getWorldPoint(m_localAnchorB)
   }
   
   open override func getReactionForce(_ inv_dt: Double) -> b2Vec2 {
       return multM(m_linearImpulse, inv_dt)
   }
   
   open override func getReactionTorque(_ inv_dt: Double) -> Double {
       return inv_dt * m_angularImpulse
   }
   
   open var localAnchorA: b2Vec2  {
       return m_localAnchorA
   }
   open var localAnchorB: b2Vec2  {
       return m_localAnchorB
   }
   
   open func setMaxForce(_ force: Double) {
       m_maxForce = force
   }
   
   open var maxForce: Double {
       return m_maxForce
   }
   
   open func setMaxTorque(_ torque: Double) {
       m_maxTorque = torque
   }
   
   open var maxTorque: Double {
       return m_maxTorque
   }

   
   init(_ def: b2FrictionJointDef) {
       m_localAnchorA = def.localAnchorA
       m_localAnchorB = def.localAnchorB
       m_linearImpulse = b2Vec2(0.0, 0.0)
       m_angularImpulse = 0.0
       m_maxForce = def.maxForce
       m_maxTorque = def.maxTorque
       super.init(def)
   }
   
   override func initVelocityConstraints(_ data: inout b2SolverData) {
       m_indexA = m_bodyA.m_islandIndex
       m_indexB = m_bodyB.m_islandIndex
       m_localCenterA = m_bodyA.m_sweep.localCenter
       m_localCenterB = m_bodyB.m_sweep.localCenter
       m_invMassA = m_bodyA.m_invMass
       m_invMassB = m_bodyB.m_invMass
       m_invIA = m_bodyA.m_invI
       m_invIB = m_bodyB.m_invI
       let aA = data.positions.get(m_indexA).a
       var vA = data.velocities.get(m_indexA).v
       var wA = data.velocities.get(m_indexA).w
       let aB = data.positions.get(m_indexB).a
       var vB = data.velocities.get(m_indexB).v
       var wB = data.velocities.get(m_indexB).w
       let qA = b2Rot(aA), qB = b2Rot(aB)
       m_rA = b2MulR2(qA, subtract(m_localAnchorA, m_localCenterA))
       m_rB = b2MulR2(qB, subtract(m_localAnchorB, m_localCenterB))
       let mA = m_invMassA, mB = m_invMassB
       let iA = m_invIA, iB = m_invIB
       let K = b2Mat22()
       K.ex.x = mA + mB + iA * m_rA.y * m_rA.y + iB * m_rB.y * m_rB.y
       K.ex.y = -iA * m_rA.x * m_rA.y - iB * m_rB.x * m_rB.y
       K.ey.x = K.ex.y
       K.ey.y = mA + mB + iA * m_rA.x * m_rA.x + iB * m_rB.x * m_rB.x
       m_linearMass = K.getInverse()
       m_angularMass = iA + iB
       if m_angularMass > 0.0 {
           m_angularMass = 1.0 / m_angularMass
       }
       if data.step.warmStarting {
           mulEqual(&m_linearImpulse, data.step.dtRatio)
           m_angularImpulse *= data.step.dtRatio
           let P = b2Vec2(m_linearImpulse.x, m_linearImpulse.y)
           subtractEqual(&vA, multM(P, mA))
           wA -= iA * (b2Cross(m_rA, P) + m_angularImpulse)
           addEqual(&vB, multM(P, mB))
           wB += iB * (b2Cross(m_rB, P) + m_angularImpulse)
       } else {
           m_linearImpulse.setZero()
           m_angularImpulse = 0.0
       }
       data.velocities.get(m_indexA).v = vA
       data.velocities.get(m_indexA).w = wA
       data.velocities.get(m_indexB).v = vB
       data.velocities.get(m_indexB).w = wB
   }
   
   override func solveVelocityConstraints(_ data: inout b2SolverData) {
       var vA = data.velocities.get(m_indexA).v
       var wA = data.velocities.get(m_indexA).w
       var vB = data.velocities.get(m_indexB).v
       var wB = data.velocities.get(m_indexB).w
       let mA = m_invMassA, mB = m_invMassB
       let iA = m_invIA, iB = m_invIB
       let h = data.step.dt
       let Cdot = wB - wA
       var impulse = -self.m_angularMass * Cdot
       let oldImpulse = self.m_angularImpulse
       let maxImpulse = h * self.m_maxTorque
       self.m_angularImpulse = b2ClampF(self.m_angularImpulse + impulse, -maxImpulse, maxImpulse)
       impulse = self.m_angularImpulse - oldImpulse
       wA -= iA * impulse
       wB += iB * impulse
       let loc_Cdot = subtract(subtract(add(vB, b2Cross21(wB, self.m_rB)), vA), b2Cross21(wA, self.m_rA))
       var loc_impulse = minus(b2Mul22(self.m_linearMass, loc_Cdot))
       let loc_oldImpulse = self.m_linearImpulse
       addEqual(&self.m_linearImpulse, loc_impulse)
       let loc_maxImpulse = h * self.m_maxForce
       if self.m_linearImpulse.lengthSquared() > loc_maxImpulse * loc_maxImpulse {
           self.m_linearImpulse.normalize()
           mulEqual(&self.m_linearImpulse, loc_maxImpulse)
       }
       loc_impulse = subtract(self.m_linearImpulse,loc_oldImpulse)
       subtractEqual(&vA, multM(loc_impulse, mA))
       wA -= iA * b2Cross(self.m_rA, loc_impulse)
       addEqual(&vB, multM(loc_impulse, mB))
       wB += iB * b2Cross(self.m_rB, loc_impulse)
       data.velocities.get(m_indexA).v = vA
       data.velocities.get(m_indexA).w = wA
       data.velocities.get(m_indexB).v = vB
       data.velocities.get(m_indexB).w = wB
   }
   
   override func solvePositionConstraints(_ data: inout b2SolverData) -> Bool {
       return true
   }
}

open class b2DistanceJointDef : b2JointDef {
   init(bodyA: b2Body?, bodyB: b2Body?, anchorA: b2Vec2?, anchorB: b2Vec2?) {
       super.init()
       if (bodyA != nil && bodyB != nil && anchorA != nil && anchorB != nil) {
           initialize(bodyA!, bodyB!, anchorA!, anchorB!)
       } else {
           localAnchorA = b2Vec2(0.0, 0.0)
           localAnchorB = b2Vec2(0.0, 0.0)
           length = 1.0
           frequencyHz = 0.0
           dampingRatio = 0.0
           type = b2JointType.distanceJoint
       }
   }
   
   open func initialize(_ bA: b2Body, _ bB: b2Body, _ anchorA: b2Vec2, _ anchorB: b2Vec2) {
       bodyA = bA
       bodyB = bB
       localAnchorA = bodyA.getLocalPoint(anchorA)
       localAnchorB = bodyB.getLocalPoint(anchorB)
       let d = subtract(anchorB, anchorA)
       length = d.length()
   }
   
   open var localAnchorA = b2Vec2()
   open var localAnchorB = b2Vec2()
   open var length: Double = 1.0
   open var frequencyHz: Double = 0.0
   open var dampingRatio: Double = 0.0
}

open class b2DistanceJoint : b2Joint {
   var m_frequencyHz: Double
   var m_dampingRatio: Double
   var m_bias: Double
   var m_localAnchorA: b2Vec2
   var m_localAnchorB: b2Vec2
   var m_gamma: Double
   var m_impulse: Double
   var m_length: Double
   var m_indexA: Int = 0
   var m_indexB: Int = 0
   var m_u = b2Vec2()
   var m_rA = b2Vec2()
   var m_rB = b2Vec2()
   var m_localCenterA = b2Vec2()
   var m_localCenterB = b2Vec2()
   var m_invMassA : Double = 0.0
   var m_invMassB : Double = 0.0
   var m_invIA : Double = 0.0
   var m_invIB: Double = 0.0
   var m_mass: Double = 0.0
   
   open override var anchorA: b2Vec2 {
       return m_bodyA.getWorldPoint(m_localAnchorA)
   }
   
   open override var anchorB: b2Vec2 {
       return m_bodyB.getWorldPoint(m_localAnchorB)
   }
   
   open override func getReactionForce(_ inv_dt: Double) -> b2Vec2 {
       let F = multM(m_u, (inv_dt * m_impulse))
       return F
   }
   
   open override func getReactionTorque(_ inv_dt: Double) -> Double {
       return 0.0
   }
   open var localAnchorA: b2Vec2  { return m_localAnchorA }
   open var localAnchorB: b2Vec2  { return m_localAnchorB }
   
   open func setLength(_ length: Double) {
       m_length = length
   }
   
   open var length: Double {
       return m_length
   }
   
   open func setFrequency(_ hz: Double) {
       m_frequencyHz = hz
   }
   
   open var frequency: Double {
       return m_frequencyHz
   }
   
   open func setDampingRatio(_ ratio: Double) {
       m_dampingRatio = ratio
   }
   
   open var dampingRatio: Double {
       return m_dampingRatio
   }
   
   init(_ def: b2DistanceJointDef) {
       m_localAnchorA = def.localAnchorA
       m_localAnchorB = def.localAnchorB
       m_length = def.length
       m_frequencyHz = def.frequencyHz
       m_dampingRatio = def.dampingRatio
       m_impulse = 0.0
       m_gamma = 0.0
       m_bias = 0.0
       super.init(def)
   }
   
   override func initVelocityConstraints(_ data: inout b2SolverData) {
       m_indexA = m_bodyA.m_islandIndex
       m_indexB = m_bodyB.m_islandIndex
       m_localCenterA = m_bodyA.m_sweep.localCenter
       m_localCenterB = m_bodyB.m_sweep.localCenter
       m_invMassA = m_bodyA.m_invMass
       m_invMassB = m_bodyB.m_invMass
       m_invIA = m_bodyA.m_invI
       m_invIB = m_bodyB.m_invI
       let cA = data.positions.get(m_indexA).c
       let aA = data.positions.get(m_indexA).a
       var vA = data.velocities.get(m_indexA).v
       var wA = data.velocities.get(m_indexA).w
       let cB = data.positions.get(m_indexB).c
       let aB = data.positions.get(m_indexB).a
       var vB = data.velocities.get(m_indexB).v
       var wB = data.velocities.get(m_indexB).w
       let qA = b2Rot(aA), qB = b2Rot(aB)
       m_rA = b2MulR2(qA, subtract(m_localAnchorA, m_localCenterA))
       m_rB = b2MulR2(qB, subtract(m_localAnchorB, m_localCenterB))
       m_u = subtract(subtract(add(cB, m_rB), cA), m_rA)
       let length = m_u.length()
       if length > b2_linearSlop {
           mulEqual(&m_u, 1.0 / length)
       } else {
           m_u.set(0.0, 0.0)
       }
       let crAu = b2Cross(m_rA, m_u)
       let crBu = b2Cross(m_rB, m_u)
       var invMass = m_invMassA + m_invIA * crAu * crAu + m_invMassB + m_invIB * crBu * crBu
       m_mass = invMass != 0.0 ? 1.0 / invMass : 0.0
       if m_frequencyHz > 0.0 {
           let C = length - m_length
           let omega = 2.0 * b2_pi * m_frequencyHz
           let d = 2.0 * m_mass * m_dampingRatio * omega
           let k = m_mass * omega * omega
           let h = data.step.dt
           m_gamma = h * (d + h * k)
           m_gamma = m_gamma != 0.0 ? 1.0 / m_gamma : 0.0
           m_bias = C * h * k * m_gamma
           invMass += m_gamma
           m_mass = invMass != 0.0 ? 1.0 / invMass : 0.0
       } else {
           m_gamma = 0.0
           m_bias = 0.0
       }
       if data.step.warmStarting {
           m_impulse *= data.step.dtRatio
           let P = multM(m_u, m_impulse)
           subtractEqual(&vA, multM(P, m_invMassA))
           wA -= m_invIA * b2Cross(m_rA, P)
           addEqual(&vB, multM(P, m_invMassB))
           wB += m_invIB * b2Cross(m_rB, P)
       } else {
           m_impulse = 0.0
       }
       data.velocities.get(m_indexA).v = vA
       data.velocities.get(m_indexA).w = wA
       data.velocities.get(m_indexB).v = vB
       data.velocities.get(m_indexB).w = wB
   }
   
   override func solveVelocityConstraints(_ data: inout b2SolverData) {
       var vA = data.velocities.get(m_indexA).v
       var wA = data.velocities.get(m_indexA).w
       var vB = data.velocities.get(m_indexB).v
       var wB = data.velocities.get(m_indexB).w
       let vpA = add(vA, b2Cross21(wA, m_rA))
       let vpB = add(vB, b2Cross21(wB, m_rB))
       let Cdot = b2Dot22(m_u, subtract(vpB, vpA))
       let impulse = -m_mass * (Cdot + m_bias + m_gamma * m_impulse)
       m_impulse += impulse
       let P = multM(m_u, impulse)
       subtractEqual(&vA, multM(P, m_invMassA))
       wA -= m_invIA * b2Cross(m_rA, P)
       addEqual(&vB, multM(P, m_invMassB))
       wB += m_invIB * b2Cross(m_rB, P)
       data.velocities.get(m_indexA).v = vA
       data.velocities.get(m_indexA).w = wA
       data.velocities.get(m_indexB).v = vB
       data.velocities.get(m_indexB).w = wB
   }
   
   override func solvePositionConstraints(_ data: inout b2SolverData) -> Bool {
       if m_frequencyHz > 0.0 {
           return true
       }
       var cA = data.positions.get(m_indexA).c
       var aA = data.positions.get(m_indexA).a
       var cB = data.positions.get(m_indexB).c
       var aB = data.positions.get(m_indexB).a
       let qA = b2Rot(aA), qB = b2Rot(aB)
       let rA = b2MulR2(qA, subtract(m_localAnchorA, m_localCenterA))
       let rB = b2MulR2(qB, subtract(m_localAnchorB, m_localCenterB))
       var u = subtract(subtract(add(cB, rB), cA), rA)
       let length = u.normalize()
       var C = length - m_length
       C = b2ClampF(C, -b2_maxLinearCorrection, b2_maxLinearCorrection)
       let impulse = -m_mass * C
       let P = multM(u, impulse)
       subtractEqual(&cA, multM(P, m_invMassA))
       aA -= m_invIA * b2Cross(rA, P)
       addEqual(&cB, multM(P, m_invMassB))
       aB += m_invIB * b2Cross(rB, P)
       data.positions.get(m_indexA).c = cA
       data.positions.get(m_indexA).a = aA
       data.positions.get(m_indexB).c = cB
       data.positions.get(m_indexB).a = aB
       return abs(C) < b2_linearSlop
   }
}
