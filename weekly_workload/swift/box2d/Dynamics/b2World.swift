

open class b2World {
    var m_island: b2Island! = nil
    var m_TOIIsland: b2Island! = nil
    var m_flags = FlagsW.clearForces
    var m_contactManager = b2ContactManager()
    var m_bodyList: b2Body? = nil
    var m_jointList: b2Joint? = nil
    var m_bodyCount = 0
    var m_jointCount = 0
    var m_gravity: b2Vec2
    var m_allowSleep = true
    var m_destructionListener: b2DestructionListener? = nil
    var m_inv_dt0: Double = 0.0
    var m_warmStarting = true
    var m_continuousPhysics = true
    var m_subStepping = false
    var m_stepComplete = true
    var m_profile = b2Profile()
    public init(_ gravity: b2Vec2) {
        m_destructionListener = nil
        m_bodyList = nil
        m_jointList = nil
        m_bodyCount = 0
        m_jointCount = 0
        m_warmStarting = true
        m_continuousPhysics = true
        m_subStepping = false
        m_stepComplete = true
        m_allowSleep = true
        m_gravity = gravity
        m_flags = FlagsW.clearForces
        m_inv_dt0 = 0.0
        m_contactManager = b2ContactManager()
    }
    
    open func setDestructionListener(_ listener: b2DestructionListener) {
        m_destructionListener = listener
    }
    
    open func setContactFilter(_ filter: b2ContactFilter) {
        m_contactManager.m_contactFilter = filter
    }
    
    open func setContactListener(_ listener: b2ContactListener) {
        m_contactManager.m_contactListener = listener
    }
    
    open func createBody(_ def: b2BodyDef) -> b2Body {
        if isLocked {
            fatalError("world is locked")
        }
        let b = b2Body(def, self)
        b.m_prev = nil
        b.m_next = m_bodyList
        if m_bodyList != nil {
            m_bodyList!.m_prev = b
        }
        m_bodyList = b
        m_bodyCount += 1
        return b
    }
    
    open func destroyBody(_ b: b2Body) {
        if isLocked {
            return
        }
        var je = b.m_jointList
        while je != nil {
            let je0 = je!
            je = je!.next
            if m_destructionListener != nil {
                m_destructionListener!.sayGoodbye(je0.joint)
            }
            b.m_jointList = je
        }
        b.m_jointList = nil
        var ce = b.m_contactList
        while ce != nil {
            let ce0 = ce!
            ce = ce!.next
            m_contactManager.destroy(ce0.contact)
        }
        b.m_contactList = nil
        var f = b.m_fixtureList
        while f != nil {
            let f0 = f!
            f = f!.m_next
            if m_destructionListener != nil {
                m_destructionListener!.sayGoodbye(f0)
            }
            f0.destroyProxies(m_contactManager.m_broadPhase)
            f0.destroy()
            b.m_fixtureList = f
            b.m_fixtureCount -= 1
        }
        b.m_fixtureList = nil
        b.m_fixtureCount = 0
        if b.m_prev != nil {
            b.m_prev!.m_next = b.m_next
        }
        if b.m_next != nil {
            b.m_next!.m_prev = b.m_prev
        }
        if b === m_bodyList {
            m_bodyList = b.m_next
        }
        m_bodyCount -= 1
    }
    
    open func step(_ dt: Double, _ velocityIterations: Int, _ positionIterations: Int) {
        if (m_flags & FlagsW.newFixture) != 0 {
            m_contactManager.findNewContacts()
            m_flags &= ~FlagsW.newFixture
        }
        m_flags |= FlagsW.locked
        let step = b2TimeStep()
        step.dt = dt
        step.velocityIterations    = velocityIterations
        step.positionIterations = positionIterations
        if dt > 0.0 {
            step.inv_dt = 1.0 / dt
        } else {
            step.inv_dt = 0.0
        }
        step.dtRatio = m_inv_dt0 * dt
        step.warmStarting = m_warmStarting
        m_contactManager.collide()
        if m_stepComplete && step.dt > 0.0 {
            solve(step)
        }
        if m_continuousPhysics && step.dt > 0.0 {
            solveTOI(step)
        }
        if step.dt > 0.0 {
            m_inv_dt0 = step.inv_dt
        }
        if (m_flags & FlagsW.clearForces) != 0 {
            clearForces()
        }
        m_flags &= ~FlagsW.locked
    }
    
    open func clearForces() {
        var body = m_bodyList
        while body != nil
        {
            body!.m_force.setZero()
            body!.m_torque = 0.0
            body = body!.getNext()
        }
    }
        
    open func queryAABB(_ callback: b2QueryCallback, _ aabb: b2AABB) {
        let wrapper = b2WorldQueryWrapper()
        wrapper.broadPhase = m_contactManager.m_broadPhase
        wrapper.callback = callback
        m_contactManager.m_broadPhase.query(wrapper, aabb)
    }
    
    open func queryAABB(_ aabb: b2AABB, _ callback: @escaping b2QueryCallbackFunction) {
        queryAABB(b2QueryCallbackProxy(callback), aabb)
    }
    
    open func rayCast(_ callback: b2RayCastCallback, _ point1: b2Vec2, _ point2: b2Vec2) {
        let wrapper = b2WorldRayCastWrapper()
        wrapper.broadPhase = m_contactManager.m_broadPhase
        wrapper.callback = callback
        let input = b2RayCastInput()
        input.maxFraction = 1.0
        input.p1 = point1
        input.p2 = point2
        m_contactManager.m_broadPhase.rayCast(wrapper, input)
    }
    
    open func rayCast(_ point1: b2Vec2, _ point2: b2Vec2, _ callback: @escaping b2RayCastCallbackFunction) {
        rayCast(b2RayCastCallbackProxy(callback), point1, point2)
    }
    
    open func getBodyList() -> b2Body? {
        return m_bodyList
    }
    
    open func getJointList() -> b2Joint? {
        return m_jointList
    }
    
    open func getContactList() -> b2Contact? {
        return m_contactManager.m_contactList
    }
    
    open func setAllowSleeping(_ flag: Bool) {
        if flag == m_allowSleep {
            return
        }
        m_allowSleep = flag
        if m_allowSleep == false {
            var b = m_bodyList
            while b != nil {
                b!.setAwake(true)
                b = b!.m_next
            }
        }
    }
    
    open var allowSleeping: Bool {
        get {
            return m_allowSleep
        }
        set {
            setAllowSleeping(newValue)
        }
    }
    
    open func setWarmStarting(_ flag: Bool) { m_warmStarting = flag }
    open var warmStarting: Bool {
        get {
            return m_warmStarting
        }
        set {
            setWarmStarting(newValue)
        }
    }
    
    open func setContinuousPhysics(_ flag: Bool) { m_continuousPhysics = flag }
    open var continuousPhysics: Bool {
        get {
            return m_continuousPhysics
        }
        set {
            setContinuousPhysics(newValue)
        }
    }
    
    open func setSubStepping(_ flag: Bool) { m_subStepping = flag }
    open var subStepping: Bool {
        get {
            return m_subStepping
        }
        set {
            setSubStepping(newValue)
        }
    }
    
    open var proxyCount: Int {
        return m_contactManager.m_broadPhase.getProxyCount()
    }
    
    open var bodyCount: Int {
        return m_bodyCount
    }
    
    open var jointCount: Int {
        return m_jointCount
    }
    
    open var contactCount: Int {
        return m_contactManager.m_contactCount
    }
    
    open var treeHeight: Int {
        return m_contactManager.m_broadPhase.getTreeHeight()
    }
    
    open var treeBalance: Int {
        return m_contactManager.m_broadPhase.getTreeBalance()
    }
    
    open var treeQuality: Double {
        return m_contactManager.m_broadPhase.getTreeQuality()
    }
    
    open func setGravity(_ gravity: b2Vec2) {
        m_gravity = gravity
    }
    
    open var gravity: b2Vec2 {
        get {
            return m_gravity
        }
        set {
            setGravity(newValue)
        }
    }
    
    open var isLocked: Bool {
        return (m_flags & FlagsW.locked) == FlagsW.locked
    }
    
    open func setAutoClearForces(_ flag: Bool) {
        if flag {
            m_flags |= FlagsW.clearForces
        } else {
            m_flags &= ~FlagsW.clearForces
        }
    }
    
    open var autoClearForces: Bool {
        return (m_flags & FlagsW.clearForces) == FlagsW.clearForces
    }
    
    open func shiftOrigin(_ newOrigin: b2Vec2) {
        if (m_flags & FlagsW.locked) == FlagsW.locked {
            return
        }
        var b = m_bodyList
        while b != nil {
            subtractEqual(&b!.m_xf.p, newOrigin)
            subtractEqual(&b!.m_sweep.c0, newOrigin)
            subtractEqual(&b!.m_sweep.c, newOrigin)
            b = b!.m_next
        }
        var j = m_jointList
        while j != nil {
            j!.shiftOrigin(newOrigin)
            j = j!.m_next
        }
        m_contactManager.m_broadPhase.shiftOrigin(newOrigin)
    }
    
    open var contactManager: b2ContactManager {
        return m_contactManager
    }
    
    open var profile: b2Profile {
        return m_profile
    }
    
    func solve(_ step: b2TimeStep) {
        m_profile.solveInit = 0.0
        m_profile.solveVelocity = 0.0
        m_profile.solvePosition = 0.0
        
        if m_island == nil {
            m_island = b2Island(m_bodyCount,
                                m_contactManager.m_contactCount,
                                m_jointCount,
                                m_contactManager.m_contactListener)
        } else {
            m_island.reset(m_bodyCount,
                           m_contactManager.m_contactCount,
                           m_jointCount,
                           m_contactManager.m_contactListener)
        }
        let island = m_island!
        var b = m_bodyList
        while b != nil {
            b!.m_flags &= ~Flags.islandFlag
            b = b!.m_next
        }
        var c = m_contactManager.m_contactList
        while c != nil {
            c!.m_flags &= ~Flags_C.islandFlag
            c = c!.m_next
        }
        var j = m_jointList
        while j != nil {
            j!.m_islandFlag = false
            j = j!.m_next
        }
        var stack = [b2Body]()
        var seed = m_bodyList
        while seed != nil {
            if (seed!.m_flags & Flags.islandFlag) != 0 {
                seed = seed!.m_next
                continue
            }
            if seed!.isAwake == false || seed!.isActive == false {
                seed = seed!.m_next
                continue
            }
            if seed!.type_Body == b2BodyType.staticBody {
                seed = seed!.m_next
                continue
            }
            island.clear()
            stack.append(seed!)
            seed!.m_flags |= Flags.islandFlag
            while stack.count > 0 {
                let b = stack.removeLast()
                island.addB(b)
                b.setAwake(true)
                if b.type_Body == b2BodyType.staticBody {
                    continue
                }
                var ce = b.m_contactList
                while ce != nil {
                    let contact = ce!.contact
                    if (contact.m_flags & Flags_C.islandFlag) != 0 {
                        ce = ce!.next
                        continue
                    }
                    if contact.isEnabled == false || contact.isTouching == false {
                        ce = ce!.next
                        continue
                    }
                    let sensorA = contact.m_fixtureA.m_isSensor
                    let sensorB = contact.m_fixtureB.m_isSensor
                    if sensorA || sensorB {
                        ce = ce!.next
                        continue
                    }
                    island.addC(contact)
                    contact.m_flags |= Flags_C.islandFlag
                    let other = ce!.other
                    if ((other?.m_flags)! & Flags.islandFlag) != 0 {
                        ce = ce!.next
                        continue
                    }
                    stack.append(other!)
                    other?.m_flags |= Flags.islandFlag
                    ce = ce!.next
                }
                var je = b.m_jointList
                while je != nil {
                    if je!.joint.m_islandFlag == true {
                        je = je!.next
                        continue
                    }
                    let other: b2Body = je!.other
                    if other.isActive == false {
                        je = je!.next
                        continue
                    }
                    
                    island.addJ(je!.joint)
                    je!.joint.m_islandFlag = true
                    
                    if (other.m_flags & Flags.islandFlag) != 0 {
                        je = je!.next
                        continue
                    }
                    stack.append(other)
                    other.m_flags |= Flags.islandFlag
                }
            }
            var profile = b2Profile()
            island.solve(&profile, step, m_gravity, m_allowSleep)
            m_profile.solveInit += profile.solveInit
            m_profile.solveVelocity += profile.solveVelocity
            m_profile.solvePosition += profile.solvePosition
            for i in 0 ..< island.m_bodyCount {
                let b = island.m_bodies[i]
                if b.type_Body == b2BodyType.staticBody {
                    b.m_flags &= ~Flags.islandFlag
                }
            }
            seed = seed!.m_next
        }
        stack.removeAll()
        var loc_b = self.m_bodyList;
        while loc_b != nil {
            if (loc_b!.m_flags & Flags.islandFlag) == 0 {
                loc_b = loc_b!.getNext()
                continue
            }
            if loc_b!.type_Body == b2BodyType.staticBody {
                loc_b = loc_b!.getNext()
                continue
            }
            loc_b!.synchronizeFixtures()
            loc_b = loc_b!.getNext()
        }
        m_contactManager.findNewContacts()
    }
    
    func solveTOI(_ step: b2TimeStep) {
        if m_TOIIsland == nil {
            m_TOIIsland = b2Island(2 * b2_maxTOIContacts, b2_maxTOIContacts, 0, m_contactManager.m_contactListener)
        } else {
            m_TOIIsland.reset(2 * b2_maxTOIContacts, b2_maxTOIContacts, 0, m_contactManager.m_contactListener)
        }
        let island = m_TOIIsland!
        if m_stepComplete {
            var b = m_bodyList
            while b != nil {
                b!.m_flags &= ~Flags.islandFlag
                b!.m_sweep.alpha0 = 0.0
                b = b!.m_next
            }
            var c = m_contactManager.m_contactList
            while c != nil {
                c!.m_flags &= ~(Flags_C.toiFlag | Flags_C.islandFlag)
                c!.m_toiCount = 0
                c!.m_toi = 1.0
                c = c!.m_next
            }
        }
        while true {
            var minContact: b2Contact? = nil
            var minAlpha: Double = 1.0
            var c = m_contactManager.m_contactList
            while c != nil {
                if c!.isEnabled == false {
                    c = c!.m_next
                    continue
                }
                if c!.m_toiCount > b2_maxSubSteps {
                    c = c!.m_next
                    continue
                }
                var alpha: Double = 1.0
                if (c!.m_flags & Flags_C.toiFlag) != 0 {
                    alpha = c!.m_toi
                } else {
                    let fA = c!.fixtureA
                    let fB = c!.fixtureB
                    if fA.isSensor || fB.isSensor {
                        c = c!.m_next
                        continue
                    }
                    let bA = fA.body
                    let bB = fB.body
                    let typeA = bA.m_type
                    let typeB = bB.m_type
                    let activeA = bA.isAwake && typeA != b2BodyType.staticBody
                    let activeB = bB.isAwake && typeB != b2BodyType.staticBody
                    if activeA == false && activeB == false {
                        c = c!.m_next
                        continue
                    }
                    let collideA = bA.isBullet || typeA != b2BodyType.dynamicBody
                    let collideB = bB.isBullet || typeB != b2BodyType.dynamicBody
                    if collideA == false && collideB == false {
                        c = c!.m_next
                        continue
                    }
                    var alpha0 = bA.m_sweep.alpha0
                    if bA.m_sweep.alpha0 < bB.m_sweep.alpha0 {
                        alpha0 = bB.m_sweep.alpha0
                        bA.m_sweep.advance(alpha0)
                    } else if bB.m_sweep.alpha0 < bA.m_sweep.alpha0 {
                        alpha0 = bA.m_sweep.alpha0
                        bB.m_sweep.advance(alpha0)
                    }
                    let indexA = c!.childIndexA
                    let indexB = c!.childIndexB
                    let input = b2TOIInput() 
                    input.proxyA.set(fA.shape, indexA)
                    input.proxyB.set(fB.shape, indexB)
                    input.sweepA = bA.m_sweep
                    input.sweepB = bB.m_sweep
                    input.tMax = 1.0
                    var output = b2TOIOutput()
                    b2TimeOfImpact(&output, input: input)
                    let beta = output.t
                    if output.state == State.touching {
                        alpha = min(alpha0 + (1.0 - alpha0) * beta, 1.0)
                    } else {
                        alpha = 1.0
                    }
                    c!.m_toi = alpha
                    c!.m_flags |= Flags_C.toiFlag
                }
                if alpha < minAlpha {
                    minContact = c
                    minAlpha = alpha
                }
                c = c!.m_next
            }
            if minContact == nil || 1.0 - 10.0 * b2_epsilon < minAlpha {
                m_stepComplete = true
                break
            }
            let fA = minContact!.fixtureA
            let fB = minContact!.fixtureB
            let bA = fA.body
            let bB = fB.body
            let backup1 = bA.m_sweep
            let backup2 = bB.m_sweep
            bA.advance(minAlpha)
            bB.advance(minAlpha)
            minContact!.update(m_contactManager.m_contactListener)
            minContact!.m_flags &= ~Flags_C.toiFlag
            minContact!.m_toiCount += 1
            if minContact!.isEnabled == false || minContact!.isTouching == false {
                minContact!.setEnabled(false)
                bA.m_sweep = backup1
                bB.m_sweep = backup2
                bA.synchronizeTransform()
                bB.synchronizeTransform()
                continue
            }
            bA.setAwake(true)
            bB.setAwake(true)
            island.clear()
            island.addB(bA)
            island.addB(bB)
            island.addC(minContact!)
            bA.m_flags |= Flags.islandFlag
            bB.m_flags |= Flags.islandFlag
            minContact!.m_flags |= Flags_C.islandFlag
            let bodies = [bA, bB]
            for i in 0 ..< 2 {
                let body = bodies[i]
                if body.m_type == b2BodyType.dynamicBody {
                    var ce = body.m_contactList
                    while ce != nil {
                        if island.m_bodyCount == island.m_bodyCapacity {
                            break
                        }
                        if island.m_contactCount == island.m_contactCapacity {
                            break
                        }
                        let contact = ce!.contact
                        if (contact.m_flags & Flags_C.islandFlag) != 0 {
                            ce = ce!.next
                            continue
                        }
                        let other = ce!.other!
                        if other.m_type == b2BodyType.dynamicBody &&
                            body.isBullet == false && other.isBullet == false {
                            ce = ce!.next
                            continue
                        }
                        let sensorA = contact.m_fixtureA.m_isSensor
                        let sensorB = contact.m_fixtureB.m_isSensor
                        if sensorA || sensorB {
                            ce = ce!.next
                            continue
                        }
                        let backup = other.m_sweep
                        if (other.m_flags & Flags.islandFlag) == 0 {
                            other.advance(minAlpha)
                        }
                        contact.update(m_contactManager.m_contactListener)
                        if contact.isEnabled == false {
                            other.m_sweep = backup
                            other.synchronizeTransform()
                            ce = ce!.next
                            continue
                        }
                        if contact.isTouching == false {
                            other.m_sweep = backup
                            other.synchronizeTransform()
                            ce = ce!.next
                            continue
                        }
                        contact.m_flags |= Flags_C.islandFlag
                        island.addC(contact)
                        if (other.m_flags & Flags.islandFlag) != 0 {
                            ce = ce!.next
                            continue
                        }
                        other.m_flags |= Flags.islandFlag
                        if other.m_type != b2BodyType.staticBody {
                            other.setAwake(true)
                        }
                        island.addB(other)
                        ce = ce!.next
                    }
                }
            }
            let subStep = b2TimeStep()
            subStep.dt = (1.0 - minAlpha) * step.dt
            subStep.inv_dt = 1.0 / subStep.dt
            subStep.dtRatio = 1.0
            subStep.positionIterations = 20
            subStep.velocityIterations = step.velocityIterations
            subStep.warmStarting = false
            island.solveTOI(subStep, bA.m_islandIndex, bB.m_islandIndex)
            for i in 0 ..< island.m_bodyCount {
                let body = island.m_bodies[i]
                body.m_flags &= ~Flags.islandFlag
                if body.m_type != b2BodyType.dynamicBody {
                    continue
                }
                body.synchronizeFixtures()
                var ce = body.m_contactList
                while ce != nil {
                    ce!.contact.m_flags &= ~(Flags_C.toiFlag | Flags_C.islandFlag)
                    ce = ce!.next
                }
            }
            m_contactManager.findNewContacts()
            if m_subStepping {
                m_stepComplete = false
                break
            }
        }
    }
}

class FlagsW {
   static let newFixture: Int    = 0x0001
   static let locked: Int        = 0x0002
   static let clearForces: Int   = 0x0004
}

class b2WorldQueryWrapper : b2QueryWrapper {
    var broadPhase: b2BroadPhase! = nil
    var callback: b2QueryCallback! = nil
    func queryCallback(_ proxyId: Int) -> Bool {
        let proxy = broadPhase.getUserData(proxyId)!
        return callback.reportFixture(proxy.fixture)
    }

}

class b2WorldRayCastWrapper : b2RayCastWrapper {
    var broadPhase: b2BroadPhase! = nil
    var callback: b2RayCastCallback! = nil
    func rayCastCallback(_ input: b2RayCastInput, _ proxyId: Int) -> Double {
        let proxy = broadPhase.getUserData(proxyId)!
        let fixture = proxy.fixture
        let index = proxy.childIndex
        var output = b2RayCastOutput()
        let hit = fixture.rayCast(&output, input, index)
        if hit {
            let fraction = output.fraction
            let point = add(multM(input.p1, (1.0 - fraction)), multM(input.p2, fraction))
            return callback.reportFixture(fixture, point, output.normal, fraction)
        }
        return input.maxFraction
    }

}
