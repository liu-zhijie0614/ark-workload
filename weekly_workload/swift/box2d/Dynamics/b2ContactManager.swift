 

var b2_defaultFilter = b2ContactFilter()
var b2_defaultListener = b2DefaultContactListener()

open class b2ContactManager: b2BroadPhaseWrapper {
    
    var m_contactList: b2Contact? = nil
    var m_contactCount: Int = 0
    var m_contactFilter: b2ContactFilter? = b2_defaultFilter
    var m_contactListener: b2ContactListener? = b2_defaultListener
    
    init() {
        m_contactList = nil
        m_contactCount = 0
        m_contactFilter = b2_defaultFilter
        m_contactListener = b2_defaultListener
    }
    
    open func addPair(_ proxyUserDataA: inout b2FixtureProxy, _ proxyUserDataB: inout b2FixtureProxy) {
        let proxyA = proxyUserDataA
        let proxyB = proxyUserDataB
        var fixtureA = proxyA.fixture
        var fixtureB = proxyB.fixture
        var indexA = proxyA.childIndex
        var indexB = proxyB.childIndex
        var bodyA = fixtureA.body
        var bodyB = fixtureB.body
        if bodyA === bodyB {
            return
        }
        var edge = bodyB.getContactList()
        while edge != nil {
            if edge!.other === bodyA {
                let fA = edge!.contact.fixtureA
                let fB = edge!.contact.fixtureB
                let iA = edge!.contact.childIndexA
                let iB = edge!.contact.childIndexB
                if fA === fixtureA && fB === fixtureB && iA == indexA && iB == indexB {
                    return
                }
                if fA === fixtureB && fB === fixtureA && iA == indexB && iB == indexA {
                    return
                }
            }
            edge = edge!.next
        }
        if bodyB.shouldCollide(bodyA) == false {
            return
        }
        if m_contactFilter != nil && m_contactFilter!.shouldCollide(fixtureA, fixtureB) == false {
            return
        }
        let c = b2Contact.create(fixtureA, indexA, fixtureB, indexB)
        if c == nil {
            return
        }
        fixtureA = c!.fixtureA
        fixtureB = c!.fixtureB
        indexA = c!.childIndexA
        indexB = c!.childIndexB
        bodyA = fixtureA.body
        bodyB = fixtureB.body
        c!.m_prev = nil
        c!.m_next = m_contactList
        if m_contactList != nil {
            m_contactList!.m_prev = c
        }
        m_contactList = c
        c!.m_nodeA.contact = c!
        c!.m_nodeA.other = bodyB
        c!.m_nodeA.prev = nil
        c!.m_nodeA.next = bodyA.m_contactList
        if bodyA.m_contactList != nil {
            bodyA.m_contactList!.prev = c!.m_nodeA
        }
        bodyA.m_contactList = c!.m_nodeA
        c!.m_nodeB.contact = c!
        c!.m_nodeB.other = bodyA
        c!.m_nodeB.prev = nil
        c!.m_nodeB.next = bodyB.m_contactList
        if bodyB.m_contactList != nil {
            bodyB.m_contactList!.prev = c!.m_nodeB
        }
        bodyB.m_contactList = c!.m_nodeB
        if fixtureA.isSensor == false && fixtureB.isSensor == false {
            bodyA.setAwake(true)
            bodyB.setAwake(true)
        }
        m_contactCount += 1
    }
    
    func findNewContacts() {
        m_broadPhase.updatePairs(self)
    }
    
    func destroy(_ c: b2Contact) {
        let fixtureA = c.fixtureA
        let fixtureB = c.fixtureB
        let bodyA = fixtureA.body
        let bodyB = fixtureB.body
        if m_contactListener != nil && c.isTouching {
            m_contactListener!.endContact(c)
        }
        if c.m_prev != nil {
            c.m_prev!.m_next = c.m_next
        }
        if c.m_next != nil {
            c.m_next!.m_prev = c.m_prev
        }
        if c === m_contactList {
            m_contactList = c.m_next
        }
        if c.m_nodeA.prev != nil {
            c.m_nodeA.prev!.next = c.m_nodeA.next
        }
        if c.m_nodeA.next != nil {
            c.m_nodeA.next!.prev = c.m_nodeA.prev
        }
        if c.m_nodeA === bodyA.m_contactList {
            bodyA.m_contactList = c.m_nodeA.next
        }
        if c.m_nodeB.prev != nil {
            c.m_nodeB.prev!.next = c.m_nodeB.next
        }
        if c.m_nodeB.next != nil {
            c.m_nodeB.next!.prev = c.m_nodeB.prev
        }
        if c.m_nodeB === bodyB.m_contactList {
            bodyB.m_contactList = c.m_nodeB.next
        }
        b2Contact.destroy(c)
        m_contactCount -= 1
    }
    
    func collide() {
        var c = m_contactList
        while c != nil {
            let fixtureA = c!.fixtureA
            let fixtureB = c!.fixtureB
            let indexA = c!.childIndexA
            let indexB = c!.childIndexB
            let bodyA = fixtureA.body
            let bodyB = fixtureB.body
            if (c!.m_flags & Flags_C.filterFlag) != 0 {
                if bodyB.shouldCollide(bodyA) == false {
                    let cNuke = c!
                    c = cNuke.getNext()
                    destroy(cNuke)
                    continue
                }
                if m_contactFilter != nil && m_contactFilter!.shouldCollide(fixtureA, fixtureB) == false {
                    let cNuke = c!
                    c = cNuke.getNext()
                    destroy(cNuke)
                    continue
                }
                c!.m_flags &= ~Flags_C.filterFlag
            }
            let activeA = bodyA.isAwake && bodyA.m_type != b2BodyType.staticBody
            let activeB = bodyB.isAwake && bodyB.m_type != b2BodyType.staticBody
            if activeA == false && activeB == false {
                c = c!.getNext()
                continue
            }
            let proxyIdA = fixtureA.m_proxies[indexA].proxyId
            let proxyIdB = fixtureB.m_proxies[indexB].proxyId
            let overlap = m_broadPhase.testOverlap(proxyIdA, proxyIdB)
            if overlap == false {
                let cNuke = c!
                c = cNuke.getNext()
                destroy(cNuke)
                continue
            }
            c!.update(m_contactListener!)
            c = c!.getNext()
        }
    }
    
    var m_broadPhase = b2BroadPhase()
    open var broadPhase: b2BroadPhase {
        return m_broadPhase
    }
}


