 

public class b2Profile {
    public var step: Double = 0.0
    public var collide: Double = 0.0
    public var solve: Double = 0.0
    public var solveInit: Double = 0.0
    public var solveVelocity: Double = 0.0
    public var solvePosition: Double = 0.0
    public var broadphase: Double = 0.0
    public var solveTOI: Double = 0.0
    public init() {}
}

public class b2TimeStep {
    var dt: Double = 0.0
    var inv_dt: Double = 0.0
    var dtRatio: Double = 0.0
    var velocityIterations: Int = 0
    var positionIterations: Int = 0
    var warmStarting: Bool = false
}

open class b2Array<T> {
    var array = [T]()

    public init(_ count: Int? = nil, _ repeatedValue: T? = nil) {
        if (count != nil && repeatedValue != nil) {
            for _ in (0..<count!) {
                array.append(repeatedValue!)
            }
        }
    }

    open func append(_ newElement: T) {
        array.append(newElement)
    }
    open func insert(_ newElement: T, atIndex i: Int) {
        array.insert(newElement, at: i)
    }
    open func removeAtIndex(_ index: Int) -> T {
        return array.remove(at: index)
    }
    open func removeLast() {
        array.removeLast()
    }
    open func removeAll(_ keepCapacity: Bool = true) {
        array.removeAll()
    }
    open func get(_ index: Int) -> T {
            return array[index];
        }
    open func set(_ index: Int, _ newValue: T) {
        array[index] = newValue;
    }
    open func clone() -> b2Array {
        let clone = b2Array()
        for e in self.array {
            clone.array.append(e)
        }
        return clone
    }
    open var count : Int {
        get {
            return array.count
        }
    }
}

public class b2Position {
    init(_ c: b2Vec2, _ a: Double) {
        self.c = c
        self.a = a
    }
    var c : b2Vec2
    var a : Double
}

public class b2Velocity {
    init(_ v: b2Vec2, _ w: Double) {
        self.v = v
        self.w = w
    }
    var v : b2Vec2
    var w : Double
}

public class b2SolverData {
    var step = b2TimeStep()
    var positions = b2Array<b2Position>()
    var velocities = b2Array<b2Velocity>()
}


