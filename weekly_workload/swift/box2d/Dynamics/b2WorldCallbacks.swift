 

public protocol b2DestructionListener {
    func sayGoodbye(_ joint: b2Joint)
    func sayGoodbye(_ fixture: b2Fixture)
}

open class b2ContactFilter {
    func shouldCollide(_ fixtureA: b2Fixture, _ fixtureB: b2Fixture) -> Bool {
        let filterA = fixtureA.filterData
        let filterB = fixtureB.filterData
        if filterA.groupIndex == filterB.groupIndex && filterA.groupIndex != 0 {
            return filterA.groupIndex > 0
        }
        let collide = (filterA.maskBits & filterB.categoryBits) != 0 && (filterA.categoryBits & filterB.maskBits) != 0
        return collide
    }
}

public class b2ContactImpulse {
    open var normalImpulses: Array<Double>
    open var tangentImpulses: Array<Double>
    open var count: Int = 0
    init() {
        normalImpulses = []
        tangentImpulses = []
        for _ in 0..<b2_maxManifoldPoints {
            normalImpulses.append(0)
            tangentImpulses.append(0)
        }
    }
}

public protocol b2ContactListener {
    func beginContact(_ contact: b2Contact)
    func endContact(_ contact: b2Contact)
    func preSolve(_ contact: b2Contact, _ oldManifold: b2Manifold)
    func postSolve(_ contact: b2Contact, _ impulse: b2ContactImpulse)
}

open class b2DefaultContactListener : b2ContactListener {
    open func beginContact(_ contact : b2Contact) {}
    open func endContact(_ contact: b2Contact) {}
    open func preSolve(_ contact: b2Contact, _ oldManifold: b2Manifold) {}
    open func postSolve(_ contact: b2Contact, _ impulse: b2ContactImpulse) {}
}

public protocol b2QueryCallback {
    func reportFixture(_ fixture: b2Fixture) -> Bool
}

public typealias b2QueryCallbackFunction = (_ fixture: b2Fixture) -> Bool

class b2QueryCallbackProxy: b2QueryCallback {
    var callback: b2QueryCallbackFunction
    init(_ callback: @escaping b2QueryCallbackFunction) {
        self.callback = callback
    }
    func reportFixture(_ fixture: b2Fixture) -> Bool {
        return self.callback(fixture)
    }
}

public protocol b2RayCastCallback {
    func reportFixture(_ fixture: b2Fixture, _ point: b2Vec2, _ normal: b2Vec2, _ fraction: Double) -> Double
}

public typealias b2RayCastCallbackFunction = (_ fixture: b2Fixture, _ point: b2Vec2, _ normal: b2Vec2, _ fraction: Double) -> Double

class b2RayCastCallbackProxy: b2RayCastCallback {
    var callback: b2RayCastCallbackFunction
    init(_ callback: @escaping b2RayCastCallbackFunction) {
        self.callback = callback
    }
    func reportFixture(_ fixture: b2Fixture, _ point: b2Vec2, _ normal: b2Vec2, _ fraction: Double) -> Double {
        return self.callback(fixture, point, normal, fraction)
    }
}
