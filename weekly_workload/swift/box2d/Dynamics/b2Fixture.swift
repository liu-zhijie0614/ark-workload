 

public class b2Filter {
    public init() {
        categoryBits = 0x0001
        maskBits = 0xFFFF
        groupIndex = 0
    }
    public var categoryBits : Int
    public var maskBits : Int
    public var groupIndex : Int
}

open class b2FixtureDef {
    public init() {
        shape = nil
        userData = nil
        friction = 0.2
        restitution = 0.0
        density = 0.0
        isSensor = false
        filter = b2Filter()
    }
    open var shape: b2Shape!
    open var userData: AnyObject?
    open var friction: Double
    open var restitution: Double
    open var density: Double
    open var isSensor: Bool
    open var filter: b2Filter
}

public class b2FixtureProxy {
    init(_ fixture: b2Fixture) {
        self.fixture = fixture
    }
    var aabb = b2AABB()
    unowned var fixture: b2Fixture
    var childIndex = 0
    var proxyId = 0
}

open class b2Fixture {
    var m_density: Double = 0.0
    var m_next: b2Fixture? = nil
    unowned var m_body: b2Body
    var m_shape: b2Shape! = nil
    var m_friction: Double = 0.0
    var m_restitution: Double = 0.0
    var m_proxies = [b2FixtureProxy]()
    var m_filter = b2Filter()
    var m_isSensor: Bool = false
    var m_userData: AnyObject? = nil
    
    init(_ body: b2Body, _ def: b2FixtureDef) {
        m_userData = def.userData
        m_friction = def.friction
        m_restitution = def.restitution
        m_body = body
        m_next = nil
        m_filter = def.filter
        m_isSensor = def.isSensor
        m_shape = def.shape.clone()
        m_proxies = []
        m_density = def.density
    }
    
    var m_proxyCount: Int {
        return m_proxies.count
    }
    
    open var fixtureType: b2ShapeType {
        return m_shape.type
    }
    
    open var shape: b2Shape {
        return m_shape
    }
    
    open var isSensor: Bool {
        get {
            return m_isSensor
        }
        set {
            setSensor(newValue)
        }
    }
    
    open func setSensor(_ sensor: Bool) {
        if (sensor != m_isSensor) {
            m_body.setAwake(true)
            m_isSensor = sensor
        }
    }
    
    open var filterData: b2Filter {
        get {
            return m_filter
        }
        set {
            setFilterData(newValue)
        }
    }
    
    open func setFilterData(_ filter: b2Filter) {
        m_filter = filter
        refilter()
    }
    
    open func refilter() {
        var edge = m_body.getContactList()
        while (edge != nil) {
            let contact = edge!.contact
            let fixtureA = contact.fixtureA
            let fixtureB = contact.fixtureB
            if (fixtureA === self || fixtureB === self) {
                contact.flagForFiltering()
            }
            edge = edge!.next
        }
        let world = m_body.world
        if (world == nil) {
            return
        }
        let broadPhase = world!.m_contactManager.m_broadPhase
        for i in 0 ..< m_proxyCount {
            broadPhase.touchProxy(m_proxies[i].proxyId)
        }
    }
    
    open var body: b2Body {
        return m_body
    }
    
    open func getNext() -> b2Fixture? {
        if (m_next != nil) {
            return m_next
        }
        return nil
    }
    
    open var userData: AnyObject? {
        get {
            if m_userData != nil {
                return m_userData!
            }
            return nil
        }
        set {
            setUserData(newValue)
        }
    }
    
    open func setUserData(_ data: AnyObject?) {
        m_userData = data
    }
    
    open func testPoint(_ p: b2Vec2) -> Bool {
        return m_shape.testPoint(m_body.transform, p)
    }
    
    open func rayCast(_ output: inout b2RayCastOutput, _ input: b2RayCastInput, _ childIndex: Int) -> Bool {
        return m_shape.rayCast(&output, input, m_body.transform, childIndex)
    }
    
    open var massData: b2MassData {
        return m_shape.computeMass(m_density)
    }
    
    open var density: Double {
        get {
            return m_density
        }
        set {
            setDensity(newValue)
        }
    }
    
    open func setDensity(_ density: Double) {
        m_density = density
    }
    
    open var friction: Double {
        get {
            return m_friction
        }
        set {
            setFriction(newValue)
        }
    }
    
    open func setFriction(_ friction: Double) {
        m_friction = friction
    }
    
    open var restitution: Double {
        get {
            return m_restitution
        }
        set {
            setRestitution(newValue)
        }
    }
    
    open func setRestitution(_ restitution: Double) {
        m_restitution = restitution
    }
    
    open func getAABB(_ childIndex: Int) -> b2AABB {
        return m_proxies[childIndex].aabb
    }

    func destroy() {
        m_proxies.removeAll()
        m_shape = nil
    }
    
    func createProxies(_ broadPhase: b2BroadPhase, _ xf: b2Transform) {
        if m_shape != nil {
            let proxyCount = m_shape.childCount
            for i in 0 ..< proxyCount {
                let proxy = b2FixtureProxy(self)
                m_shape.computeAABB(&proxy.aabb, xf, i)
                proxy.childIndex = i
                proxy.proxyId = broadPhase.createProxy(proxy.aabb, proxy)
                m_proxies.append(proxy)
            }
        }
    }
    
    func destroyProxies(_ broadPhase: b2BroadPhase) {
        for i in 0 ..< m_proxyCount {
            let proxy = m_proxies[i]
            broadPhase.destroyProxy(proxy.proxyId)
        }
        m_proxies.removeAll()
    }
    
    func synchronize(_ broadPhase: b2BroadPhase, _ transform1: b2Transform, _ transform2: b2Transform) {
        if (m_proxyCount == 0) {
            return
        }
        for i in 0 ..< m_proxyCount {
            let proxy = m_proxies[i]
            var aabb1 = b2AABB(), aabb2 = b2AABB()
            m_shape.computeAABB(&aabb1, transform1, proxy.childIndex)
            m_shape.computeAABB(&aabb2, transform2, proxy.childIndex)
            proxy.aabb.combine(aabb1, aabb2)
            let displacement = subtract(transform2.p,transform1.p)
            broadPhase.moveProxy(proxy.proxyId, proxy.aabb, displacement)
        }
    }
    
}
