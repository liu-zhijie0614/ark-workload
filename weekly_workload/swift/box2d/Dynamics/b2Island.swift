 

open class b2Island {
    var m_listener: b2ContactListener?
    var m_bodies: [b2Body]
    var m_contacts: [b2Contact]
    var m_joints: [b2Joint]
    var m_positions: b2Array<b2Position>
    var m_velocities: b2Array<b2Velocity>
    var m_bodyCount: Int { return m_bodies.count }
    var m_jointCount: Int { return m_joints.count }
    var m_contactCount: Int { return m_contacts.count }
    var m_bodyCapacity: Int
    var m_contactCapacity: Int
    var m_jointCapacity: Int
    init(_ bodyCapacity: Int, _ contactCapacity: Int, _ jointCapacity: Int, _ listener: b2ContactListener?) {
        m_bodyCapacity = bodyCapacity
        m_contactCapacity = contactCapacity
        m_jointCapacity     = jointCapacity
        m_listener = listener
        m_bodies = [b2Body]()
        m_contacts = [b2Contact]()
        m_joints = [b2Joint]()
        m_velocities = b2Array<b2Velocity>()
        m_positions = b2Array<b2Position>()
    }
    
    func reset(_ bodyCapacity: Int, _ contactCapacity: Int, _ jointCapacity: Int, _ listener: b2ContactListener?) {
        m_bodyCapacity = bodyCapacity
        m_contactCapacity = contactCapacity
        m_jointCapacity     = jointCapacity
        m_listener = listener
        m_bodies.removeAll()
        m_contacts.removeAll()
        m_joints.removeAll()
        m_velocities.removeAll(true)
        m_positions.removeAll(true)
    }
    
    func clear() {
        m_bodies.removeAll()
        m_contacts.removeAll()
        m_joints.removeAll()
    }
    
    func solve(_ profile: inout b2Profile, _ step: b2TimeStep, _ gravity: b2Vec2, _ allowSleep: Bool) {
        let h = step.dt
        m_positions.removeAll(true)
        m_velocities.removeAll(true)
        for i in 0 ..< m_bodyCount {
            let b = m_bodies[i]
            let c = b.m_sweep.c
            let a = b.m_sweep.a
            var v = b.m_linearVelocity
            var w = b.m_angularVelocity
            b.m_sweep.c0 = b.m_sweep.c
            b.m_sweep.a0 = b.m_sweep.a
            if b.m_type == b2BodyType.dynamicBody {
                addEqual(&v, multM(add(multM(gravity, b.m_gravityScale), multM(b.m_force, b.m_invMass)), h))
                w += h * b.m_invI * b.m_torque
                mulEqual(&v, 1.0 / (1.0 + h * b.m_linearDamping))
                w *= 1.0 / (1.0 + h * b.m_angularDamping)
            }
            m_positions.append(b2Position(c, a))
            m_velocities.append(b2Velocity(v, w))
        }
        var solverData = b2SolverData()
        solverData.step = step
        solverData.positions = m_positions
        solverData.velocities = m_velocities
        var contactSolverDef = b2ContactSolverDef()
        contactSolverDef.step = step
        contactSolverDef.contacts = m_contacts
        contactSolverDef.count = m_contactCount
        contactSolverDef.positions = m_positions
        contactSolverDef.velocities = m_velocities
        let contactSolver = b2ContactSolver(contactSolverDef)
        contactSolver.initializeVelocityConstraints()
        if step.warmStarting {
            contactSolver.warmStart()
        }
        for i in 0 ..< m_jointCount {
            m_joints[i].initVelocityConstraints(&solverData)
        }
        for _ in 0 ..< step.velocityIterations {
            for j in 0 ..< m_jointCount {
                m_joints[j].solveVelocityConstraints(&solverData)
            }
            contactSolver.solveVelocityConstraints()
        }
        contactSolver.storeImpulses()
        for i in 0 ..< m_bodyCount {
            var c = m_positions.get(i).c
            var a = m_positions.get(i).a
            var v = m_velocities.get(i).v
            var w = m_velocities.get(i).w
            let translation = multM(v, h)
            if b2Dot22(translation, translation) > b2_maxTranslationSquared {
                let ratio = b2_maxTranslation / translation.length()
                mulEqual(&v, ratio)
            }
            let rotation = h * w
            if rotation * rotation > b2_maxRotationSquared {
                let ratio = b2_maxRotation / abs(rotation)
                w *= ratio
            }
            c = add(c, multM(v, h))
            a += h * w
            m_positions.get(i).c = c
            m_positions.get(i).a = a
            m_velocities.get(i).v = v
            m_velocities.get(i).w = w
            let b = m_bodies[i]
            b.m_sweep.c0.set(b.m_sweep.c.x, b.m_sweep.c.y)
            b.m_sweep.a0 = b.m_sweep.a
            b.m_sweep.c.x += step.dt * b.m_linearVelocity.x
            b.m_sweep.c.y += step.dt * b.linearVelocity.y
            b.m_sweep.a += step.dt * b.angularVelocity
            b.synchronizeTransform()
        }
        var positionSolved = false
        for _ in 0 ..< step.positionIterations {
            let contactsOkay = contactSolver.solvePositionConstraints()
            var jointsOkay = true
            for i2 in 0 ..< m_jointCount {
                let jointOkay = m_joints[i2].solvePositionConstraints(&solverData)
                jointsOkay = jointsOkay && jointOkay
            }
            if contactsOkay && jointsOkay {
                positionSolved = true
                break
            }
        }
        for i in 0 ..< m_bodyCount {
            let body = m_bodies[i]
            body.m_sweep.c = m_positions.get(i).c
            body.m_sweep.a = m_positions.get(i).a
            body.m_linearVelocity = m_velocities.get(i).v
            body.m_angularVelocity = m_velocities.get(i).w
            body.synchronizeTransform()
        }
        report(contactSolver.m_velocityConstraints)
        if allowSleep {
            var minSleepTime = b2_maxFloat
            let linTolSqr = b2_linearSleepTolerance * b2_linearSleepTolerance
            let angTolSqr = b2_angularSleepTolerance * b2_angularSleepTolerance
            for i in 0 ..< m_bodyCount {
                let b = m_bodies[i]
                if b.type_Body == b2BodyType.staticBody {
                    continue
                }
                if (b.m_flags & Flags.autoSleepFlag) == 0 ||
                    b.m_angularVelocity * b.m_angularVelocity > angTolSqr ||
                    b2Dot22(b.m_linearVelocity, b.m_linearVelocity) > linTolSqr {
                    b.m_sleepTime = 0.0
                    minSleepTime = 0.0
                } else {
                    b.m_sleepTime += h
                    minSleepTime = min(minSleepTime, b.m_sleepTime)
                }
            }
            if minSleepTime >= b2_timeToSleep && positionSolved {
                for i in 0 ..< m_bodyCount {
                    let b = m_bodies[i]
                    b.setAwake(false)
                }
            }
        }
    }
    
    func solveTOI(_ subStep: b2TimeStep, _ toiIndexA: Int, _ toiIndexB: Int) {
        m_positions.removeAll(true)
        m_velocities.removeAll(true)
        for i in 0 ..< m_bodyCount {
            let b = m_bodies[i]
            m_positions.append(b2Position(b.m_sweep.c, b.m_sweep.a))
            m_velocities.append(b2Velocity(b.m_linearVelocity, b.m_angularVelocity))
        }
        let contactSolverDef = b2ContactSolverDef()
        contactSolverDef.contacts = m_contacts
        contactSolverDef.count = m_contactCount
        contactSolverDef.step = subStep
        contactSolverDef.positions = m_positions
        contactSolverDef.velocities = m_velocities
        let contactSolver = b2ContactSolver(contactSolverDef)
        for _ in 0 ..< subStep.positionIterations {
            let contactsOkay = contactSolver.solveTOIPositionConstraints(toiIndexA, toiIndexB)
            if contactsOkay {
                break
            }
        }
        m_bodies[toiIndexA].m_sweep.c0 = m_positions.get(toiIndexA).c
        m_bodies[toiIndexA].m_sweep.a0 = m_positions.get(toiIndexA).a
        m_bodies[toiIndexB].m_sweep.c0 = m_positions.get(toiIndexB).c
        m_bodies[toiIndexB].m_sweep.a0 = m_positions.get(toiIndexB).a
        contactSolver.initializeVelocityConstraints()
        for _ in 0 ..< subStep.velocityIterations {
            contactSolver.solveVelocityConstraints()
        }
        let h = subStep.dt
        for i in 0 ..< m_bodyCount {
            var c = m_positions.get(i).c
            var a = m_positions.get(i).a
            var v = m_velocities.get(i).v
            var w = m_velocities.get(i).w
            let translation = multM(v, h)
            if b2Dot22(translation, translation) > b2_maxTranslationSquared {
                let ratio = b2_maxTranslation / translation.length()
                mulEqual(&v, ratio)
            }
            let rotation = h * w
            if rotation * rotation > b2_maxRotationSquared {
                let ratio = b2_maxRotation / abs(rotation)
                w *= ratio
            }
            addEqual(&c, multM(v, h))
            a += h * w
            m_positions.get(i).c = c
            m_positions.get(i).a = a
            m_velocities.get(i).v = v
            m_velocities.get(i).w = w
            let body = m_bodies[i]
            body.m_sweep.c = c
            body.m_sweep.a = a
            body.m_linearVelocity = v
            body.m_angularVelocity = w
            body.synchronizeTransform()
        }
        report(contactSolver.m_velocityConstraints)
    }
    
    func addB(_ body: b2Body) {
        body.m_islandIndex = m_bodyCount
        m_bodies.append(body)
    }
    
    func addC(_ contact: b2Contact) {
        m_contacts.append(contact)
    }
    
    func addJ(_ joint: b2Joint) {
        m_joints.append(joint)
    }
    
    func report(_ constraints: [b2ContactVelocityConstraint]) {
        if m_listener == nil {
            return
        }
        for i in 0 ..< m_contactCount {
            let c = m_contacts[i]
            let vc = constraints[i]
            let impulse = b2ContactImpulse()
            impulse.count = vc.pointCount
            for j in 0 ..< vc.pointCount {
                impulse.normalImpulses[j] = vc.points[j].normalImpulse
                impulse.tangentImpulses[j] = vc.points[j].tangentImpulse
            }
            m_listener?.postSolve(c, impulse)
        }
    }

}
