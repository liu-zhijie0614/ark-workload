

public enum b2BodyType : Int {
   case staticBody = 0
   case kinematicBody
   case dynamicBody
}

open class b2BodyDef {
    open var type = b2BodyType.staticBody
    open var position = b2Vec2()
    open var angle: Double = 0.0
    open var linearVelocity = b2Vec2()
    open var angularVelocity: Double = 0.0
    open var linearDamping: Double = 0.0
    open var angularDamping: Double = 0.0
    open var allowSleep = true
    open var awake = true
    open var fixedRotation = false
    open var bullet = false
    open var active = true
    open var userData : AnyObject? = nil
    open var gravityScale: Double = 1.0
    public init() {}
}

open class b2Body {
    var m_type: b2BodyType
    var m_flags: Int = 0
    var m_islandIndex = 0
    var m_xf: b2Transform
    var m_sweep: b2Sweep
    var m_linearVelocity: b2Vec2
    var m_angularVelocity: Double
    var m_force: b2Vec2
    var m_torque: Double
    unowned var m_world: b2World
    var m_prev: b2Body? = nil
    var m_next: b2Body? = nil
    var m_fixtureList: b2Fixture? = nil
    var m_fixtureCount: Int = 0
    var m_jointList: b2JointEdge? = nil
    var m_contactList: b2ContactEdge? = nil
    var m_mass: Double
    var m_invMass: Double
    var m_I: Double
    var m_invI: Double
    var m_linearDamping: Double
    var m_angularDamping: Double
    var m_gravityScale: Double
    var m_sleepTime: Double
    var m_userData: AnyObject?
    open func createFixture(_ def: b2FixtureDef) -> b2Fixture {
        if m_world.isLocked == true {
            fatalError("world is locked")
        }
        let fixture = b2Fixture(self, def)
        if (m_flags & Flags.activeFlag) != 0 {
            let broadPhase = m_world.m_contactManager.m_broadPhase
            fixture.createProxies(broadPhase, m_xf)
        }
        fixture.m_next = m_fixtureList
        m_fixtureList = fixture
        m_fixtureCount += 1
        fixture.m_body = self
        if fixture.m_density > 0.0 {
            resetMassData()
        }
        m_world.m_flags |= FlagsW.newFixture
        return fixture
    }
    
    open func createFixture2(_ shape: b2Shape, _ density: Double) -> b2Fixture {
        let def = b2FixtureDef()
        def.shape = shape
        def.density = density
        return createFixture(def)
    }
    
    open func destroyFixture(_ fixture: b2Fixture) {
        if m_world.isLocked == true {
            return
        }
        var found = false
        var prev: b2Fixture? = nil
        var f = m_fixtureList
        while f != nil {
            if f === fixture {
                if prev != nil {
                    prev!.m_next = f!.m_next
                } else {
                    m_fixtureList = f!.m_next
                }
                found = true
            }
            prev = f
            f = f!.m_next
        }
        
        var edge: b2ContactEdge? = m_contactList
        while edge != nil {
            let c = edge!.contact
            edge = edge!.next
            let fixtureA = c.fixtureA
            let fixtureB = c.fixtureB
            if fixture === fixtureA || fixture === fixtureB {
                m_world.m_contactManager.destroy(c)
            }
        }
        if (m_flags & Flags.activeFlag) != 0 {
            let broadPhase = m_world.m_contactManager.m_broadPhase
            fixture.destroyProxies(broadPhase)
        }
        fixture.destroy()
        fixture.m_next = nil
        m_fixtureCount -= 1
        resetMassData()
    }
    
    open func setTransform(_ position: b2Vec2, _ angle: Double) {
        if m_world.isLocked == true {
            return
        }
        m_xf.q.set(angle)
        m_xf.p = position
        m_sweep.c = b2MulT2(m_xf, m_sweep.localCenter)
        m_sweep.a = angle
        m_sweep.c0 = m_sweep.c
        m_sweep.a0 = angle
        let broadPhase = m_world.m_contactManager.m_broadPhase
        var f: b2Fixture? = m_fixtureList
        while f != nil {
            f!.synchronize(broadPhase, m_xf, m_xf)
            f = f!.m_next
        }
    }
    
    open var transform: b2Transform {
        return m_xf
    }
    
    open var position: b2Vec2 {
        return m_xf.p
    }
    
    open var angle: Double {
        return m_sweep.a
    }
    
    open var worldCenter: b2Vec2 {
        return m_sweep.c
    }
    
    open var localCenter: b2Vec2 {
        return m_sweep.localCenter
    }
    
    open func setLinearVelocity(_ v: b2Vec2) {
        if m_type == b2BodyType.staticBody {
            return
        }
        
        if b2Dot22(v, v) > 0.0 {
            setAwake(true)
        }
        
        m_linearVelocity = v
    }
    
    open var linearVelocity: b2Vec2 {
        get {
            return m_linearVelocity
        }
        set {
            setLinearVelocity(newValue)
        }
    }
    
    open func setAngularVelocity(_ omega: Double) {
        if m_type == b2BodyType.staticBody {
            return
        }
        if omega * omega > 0.0 {
            setAwake(true)
        }
        m_angularVelocity = omega
    }
    
    open var angularVelocity: Double {
        get {
            return m_angularVelocity
        }
        set {
            setAngularVelocity(newValue)
        }
    }
    
    open func applyForce(_ force: b2Vec2, _ point: b2Vec2, _ wake: Bool) {
        if m_type != b2BodyType.dynamicBody {
            return
        }
        if wake && (m_flags & Flags.awakeFlag) == 0 {
            setAwake(true)
        }
        if (m_flags & Flags.awakeFlag) != 0 {
            addEqual(&m_force, force)
            m_torque += b2Cross(subtract(point,m_sweep.c), force)
        }
    }
    
    open func applyForceToCenter(_ force: b2Vec2, _ wake: Bool) {
        if m_type != b2BodyType.dynamicBody {
            return
        }
        if wake && (m_flags & Flags.awakeFlag) == 0 {
            setAwake(true)
        }
        if (m_flags & Flags.awakeFlag) != 0 {
            addEqual(&m_force, force)
        }
    }
    
    open func applyTorque(_ torque: Double, _ wake: Bool) {
        if m_type != b2BodyType.dynamicBody {
            return
        }
        if wake && (m_flags & Flags.awakeFlag) == 0 {
            setAwake(true)
        }
        if (m_flags & Flags.awakeFlag) != 0 {
            m_torque += torque
        }
    }
    
    open func applyLinearImpulse(_ impulse: b2Vec2, _ point: b2Vec2, _ wake: Bool) {
        if m_type != b2BodyType.dynamicBody {
            return
        }
        if wake && (m_flags & Flags.awakeFlag) == 0 {
            setAwake(true)
        }
        if (m_flags & Flags.awakeFlag) != 0 {
            addEqual(&m_linearVelocity, multM(impulse, m_invMass))
            m_angularVelocity += m_invI * b2Cross(subtract(point, m_sweep.c), impulse)
        }
    }
    
    open func applyAngularImpulse(_ impulse: Double, _ wake: Bool) {
        if m_type != b2BodyType.dynamicBody {
            return
        }
        if wake && (m_flags & Flags.awakeFlag) == 0 {
            setAwake(true)
        }
        if (m_flags & Flags.awakeFlag) != 0 {
            m_angularVelocity += m_invI * impulse
        }
    }
    open var mass: Double {
        return m_mass
    }
    open var inertia: Double {
        return m_I + m_mass * b2Dot22(m_sweep.localCenter, m_sweep.localCenter)
    }
    open var massData: b2MassData {
        get {
            var data = b2MassData()
            data.mass = m_mass
            data.I = m_I + m_mass * b2Dot22(m_sweep.localCenter, m_sweep.localCenter)
            data.center = m_sweep.localCenter
            return data
        }
        set {
            setMassData(newValue)
        }
    }
    
    open func setMassData(_ massData: b2MassData) {
        if m_world.isLocked == true {
            return
        }
        if m_type != b2BodyType.dynamicBody {
            return
        }
        m_invMass = 0.0
        m_I = 0.0
        m_invI = 0.0
        m_mass = massData.mass
        if m_mass <= 0.0 {
            m_mass = 1.0
        }
        m_invMass = 1.0 / m_mass
        if massData.I > 0.0 && (m_flags & Flags.fixedRotationFlag) == 0 {
            m_I = massData.I - m_mass * b2Dot22(massData.center, massData.center)
            m_invI = 1.0 / m_I
        }
        let oldCenter = m_sweep.c
        m_sweep.localCenter =  massData.center
        m_sweep.c0 = b2MulT2(m_xf, m_sweep.localCenter)
        m_sweep.c = m_sweep.c0
        addEqual(&m_linearVelocity, b2Cross21(m_angularVelocity, subtract(m_sweep.c, oldCenter)))
    }
    
    open func resetMassData() {
        m_mass = 0.0
        m_invMass = 0.0
        m_I = 0.0
        m_invI = 0.0
        m_sweep.localCenter.setZero()
        if m_type == b2BodyType.staticBody || m_type == b2BodyType.kinematicBody {
            m_sweep.c0 = m_xf.p
            m_sweep.c = m_xf.p
            m_sweep.a0 = m_sweep.a
            return
        }
        var localCenter = b2Vec2_zero
        var f: b2Fixture? = m_fixtureList
        while f != nil {
            if f!.m_density == 0.0 {
                f = f!.m_next
                continue
            }
            let massData = f!.massData
            m_mass += massData.mass
            addEqual(&localCenter, multM(massData.center, massData.mass))
            m_I += massData.I
            f = f!.m_next
        }
        if m_mass > 0.0 {
            m_invMass = 1.0 / m_mass
            mulEqual(&localCenter, m_invMass)
        } else {
            m_mass = 1.0
            m_invMass = 1.0
        }
        if m_I > 0.0 && (m_flags & Flags.fixedRotationFlag) == 0 {
            m_I -= m_mass * b2Dot22(localCenter, localCenter)
            m_invI = 1.0 / m_I
        } else {
            m_I = 0.0
            m_invI = 0.0
        }
        let oldCenter = m_sweep.c
        m_sweep.localCenter = localCenter
        m_sweep.c0 = b2MulT2(m_xf, m_sweep.localCenter)
        m_sweep.c = m_sweep.c0
        addEqual(&m_linearVelocity, b2Cross21(m_angularVelocity, subtract(m_sweep.c, oldCenter)))
    }
    
    open func getWorldPoint(_ localPoint: b2Vec2) -> b2Vec2 {
        return b2MulT2(m_xf, localPoint)
    }
    
    open func getWorldVector(_ localVector: b2Vec2) -> b2Vec2 {
        return b2MulR2(m_xf.q, localVector)
    }
    
    open func getLocalPoint(_ worldPoint: b2Vec2) -> b2Vec2 {
        return b2MulTT2(m_xf, worldPoint)
    }
    
    open func getLocalVector(_ worldVector: b2Vec2) -> b2Vec2 {
        return b2MulTR2(m_xf.q, worldVector)
    }
    
    open func getLinearVelocityFromWorldPoint(_ worldPoint: b2Vec2) -> b2Vec2 {
        return add(m_linearVelocity, b2Cross21(m_angularVelocity, subtract(worldPoint, m_sweep.c)))
    }
    
    open func getLinearVelocityFromLocalPoint(_ localPoint: b2Vec2) -> b2Vec2 {
        return getLinearVelocityFromWorldPoint(getWorldPoint(localPoint))
    }
    
    open var linearDamping: Double {
        get {
            return m_linearDamping
        }
        set {
            setLinearDamping(newValue)
        }
    }
    
    open func setLinearDamping(_ linearDamping: Double) {
        m_linearDamping = linearDamping
    }
    
    open var angularDamping: Double {
        get {
            return m_gravityScale
        }
        set {
            setAngularDamping(newValue)
        }
    }
    
    open func setAngularDamping(_ angularDamping: Double) {
        m_angularDamping = angularDamping
    }
    
    open var gravityScale: Double {
        get {
            return m_gravityScale
        }
        set {
            setGravityScale(newValue)
        }
    }
    
    open func setGravityScale(_ scale: Double) {
        m_gravityScale = scale
    }
    
    open func setType(_ type: b2BodyType) {
        if m_world.isLocked == true {
            return
        }
        if m_type == type {
            return
        }
        m_type = type
        resetMassData()
        if m_type == b2BodyType.staticBody {
            m_linearVelocity.setZero()
            m_angularVelocity = 0.0
            m_sweep.a0 = m_sweep.a
            m_sweep.c0 = m_sweep.c
            synchronizeFixtures()
        }
        setAwake(true)
        m_force.setZero()
        m_torque = 0.0
        var ce: b2ContactEdge? = m_contactList
        while ce != nil {
            let ce0 = ce!
            ce = ce!.next
            m_world.m_contactManager.destroy(ce0.contact)
        }
        m_contactList = nil
        let broadPhase = m_world.m_contactManager.m_broadPhase
        var f: b2Fixture? = m_fixtureList
        while f != nil {
            let proxyCount = f!.m_proxyCount
            for i in 0 ..< proxyCount {
                broadPhase.touchProxy(f!.m_proxies[i].proxyId)
            }
            f = f!.m_next
        }
    }
    
    open var type_Body: b2BodyType {
        get {
            return m_type
        }
        set {
            setType(newValue)
        }
    }
    
    open func setBullet(_ flag: Bool) {
        if flag {
            m_flags |= Flags.bulletFlag
        } else {
            m_flags &= ~Flags.bulletFlag
        }
    }
    
    open var isBullet: Bool {
        get {
            return (m_flags & Flags.bulletFlag) == Flags.bulletFlag
        }
        set {
            setBullet(newValue)
        }
    }
    
    open func setSleepingAllowed(_ flag: Bool) {
        if flag {
            m_flags |= Flags.autoSleepFlag
        } else {
            m_flags &= ~Flags.autoSleepFlag
            setAwake(true)
        }
    }
    
    open var isSleepingAllowed: Bool {
        get {
            return (m_flags & Flags.autoSleepFlag) == Flags.autoSleepFlag
        }
        set {
            setSleepingAllowed(newValue)
        }
    }
    
    open func setAwake(_ flag: Bool) {
        if flag {
            if (m_flags & Flags.awakeFlag) == 0 {
                m_flags |= Flags.awakeFlag
                m_sleepTime = 0.0
            }
        } else {
            m_flags &= ~Flags.awakeFlag
            m_sleepTime = 0.0
            m_linearVelocity.setZero()
            m_angularVelocity = 0.0
            m_force.setZero()
            m_torque = 0.0
        }
    }
    
    open var isAwake: Bool {
        return (m_flags & Flags.awakeFlag) == Flags.awakeFlag
    }
    
    open func setActive(_ flag: Bool) {
        if flag == isActive {
            return
        }
        if flag {
            m_flags |= Flags.activeFlag
            let broadPhase = m_world.m_contactManager.m_broadPhase
            var f: b2Fixture? = m_fixtureList
            while f != nil {
                f!.createProxies(broadPhase, m_xf)
                f = f!.m_next
            }
        } else {
            m_flags &= ~Flags.activeFlag
            let broadPhase = m_world.m_contactManager.m_broadPhase
            var f: b2Fixture? = m_fixtureList
            while f != nil {
                f!.destroyProxies(broadPhase)
                if (f!.m_next != nil) {
                    f = f!.m_next
                } else {
                    break
                }
            }
            var ce = m_contactList
            while ce != nil {
                let ce0 = ce!
                ce = ce!.next
                m_world.m_contactManager.destroy(ce0.contact)
            }
            m_contactList = nil
        }
    }
    open var isActive: Bool {
        return (m_flags & Flags.activeFlag) == Flags.activeFlag
    }
    
    open func setFixedRotation(_ flag : Bool) {
        let status = (m_flags & Flags.fixedRotationFlag) == Flags.fixedRotationFlag
        if status == flag {
            return
        }
        if flag {
            m_flags |= Flags.fixedRotationFlag
        } else {
            m_flags &= ~Flags.fixedRotationFlag
        }
        m_angularVelocity = 0.0
        resetMassData()
    }
    
    open var isFixedRotation: Bool {
        return (m_flags & Flags.fixedRotationFlag) == Flags.fixedRotationFlag
    }
    
    open func getFixtureList() -> b2Fixture? {
        return m_fixtureList
    }
    
    open func getJointList() -> b2JointEdge? {
        return m_jointList
    }
    
    open func getContactList() -> b2ContactEdge? {
        return m_contactList
    }
    
    open func getNext() -> b2Body? {
        return m_next
    }
    
    open var userData: AnyObject? {
        get {
            return m_userData
        }
        set {
            setUserData(newValue)
        }
    }
    
    open func setUserData(_ data: AnyObject?) {
        m_userData = data
    }
    
    open var world: b2World? {
        return m_world
    }
    
    init(_ def: b2BodyDef, _ world: b2World) {
        m_flags = 0
        if (def.bullet) {
            m_flags |= Flags.bulletFlag
        }
        if (def.fixedRotation) {
            m_flags |= Flags.fixedRotationFlag
        }
        if (def.allowSleep) {
            m_flags |= Flags.autoSleepFlag
        }
        if (def.awake) {
            m_flags |= Flags.awakeFlag
        }
        if (def.active) {
            m_flags |= Flags.activeFlag
        }
        m_world = world
        m_xf = b2Transform()
        m_xf.p = def.position
        m_xf.q.set(def.angle)
        m_sweep = b2Sweep()
        m_sweep.localCenter = b2Vec2(0.0, 0.0)
        m_sweep.c0 = m_xf.p
        m_sweep.c = m_xf.p
        m_sweep.a0 = def.angle
        m_sweep.a = def.angle
        m_sweep.alpha0 = 0.0
        m_prev = nil
        m_next = nil
        m_linearVelocity = def.linearVelocity
        m_angularVelocity = def.angularVelocity
        m_linearDamping = def.linearDamping
        m_angularDamping = def.angularDamping
        m_gravityScale = def.gravityScale
        m_force = b2Vec2(0.0, 0.0)
        m_torque = 0.0
        m_sleepTime = 0.0
        m_type = def.type
        if m_type == b2BodyType.dynamicBody {
            m_mass = 1.0
            m_invMass = 1.0
        } else {
            m_mass = 0.0
            m_invMass = 0.0
        }
        m_I = 0.0
        m_invI = 0.0
        m_userData = def.userData
        m_fixtureList = nil
        m_fixtureCount = 0
    }
    
    func synchronizeFixtures() {
        var xf1 = b2Transform()
        xf1.q.set(m_sweep.a0)
        xf1.p = subtract(m_sweep.c0, b2MulR2(xf1.q, m_sweep.localCenter))
        let broadPhase = m_world.m_contactManager.m_broadPhase
        var f: b2Fixture? = m_fixtureList
        while f != nil {
            f!.synchronize(broadPhase, xf1, m_xf)
            f = f!.m_next
        }
    }
    
    func synchronizeTransform() {
        m_xf.q.set(m_sweep.a)
        m_xf.p = subtract(m_sweep.c, b2MulR2(m_xf.q, m_sweep.localCenter))
        let tVec = m_sweep.localCenter
        let tMat = b2Mat22()
        tMat.set_1(m_sweep.a)
        m_xf.p.y = m_sweep.c.y - (tMat.ex.y * tVec.x + tMat.ey.y * tVec.y)
    }
    
    func shouldCollide(_ other: b2Body) -> Bool {
        if m_type != b2BodyType.dynamicBody && other.m_type != b2BodyType.dynamicBody {
            return false
        }
        var jn: b2JointEdge? = m_jointList
        while jn != nil {
            if jn!.other === other {
                if jn!.joint.m_collideConnected == false {
                    return false
                }
            }
            jn = jn!.next
        }
        return true
    }
    
    func advance(_ alpha: Double) {
        m_sweep.advance(alpha)
        m_sweep.c = m_sweep.c0
        m_sweep.a = m_sweep.a0
        m_xf.q.set(m_sweep.a)
        m_xf.p = subtract(m_sweep.c, b2MulR2(m_xf.q, m_sweep.localCenter))
    }
    
}

public class Flags {
    static let islandFlag             = 0x0001
    static let awakeFlag              = 0x0002
    static let autoSleepFlag          = 0x0004
    static let bulletFlag             = 0x0008
    static let fixedRotationFlag      = 0x0010
    static let activeFlag             = 0x0020
    static let toiFlag                = 0x0040
}

