import Glibc

public func b2MixFriction(_ friction1 : Double, _ friction2 : Double) -> Double {
    return sqrt(friction1 * friction2)
}

public func b2MixRestitution(_ restitution1 : Double, _ restitution2 : Double) -> Double {
    return restitution1 > restitution2 ? restitution1 : restitution2
}

typealias b2ContactCreateFcn = (_ fixtureA: b2Fixture, _ indexA: Int, _ fixtureB: b2Fixture, _ indexB: Int) -> b2Contact
typealias b2ContactDestroyFcn = (_ contact: b2Contact) -> Void

public class b2ContactRegister {
    var createFcn: b2ContactCreateFcn? = nil
    var destroyFcn: b2ContactDestroyFcn? = nil
    var primary: Bool = false
}

class b2ContactRegisters {
    let rows: b2ShapeType
    let columns: b2ShapeType
    var grid : [b2ContactRegister] = []
    init(_ rows: b2ShapeType, _ columns: b2ShapeType) {
        self.rows = rows
        self.columns = columns
       for _ in 0..<rows.rawValue * columns.rawValue {
            self.grid.append(b2ContactRegister())
        }
    }
    
    func getItem(_ row: b2ShapeType, _ column: b2ShapeType) -> b2ContactRegister {
        return grid[(row.rawValue * columns.rawValue) + column.rawValue]
    }
    
    func setItem(_ row: b2ShapeType, _ column: b2ShapeType,_ value: b2ContactRegister) {
        grid[(row.rawValue * columns.rawValue) + column.rawValue] = value
    }
}

open class b2ContactEdge {
    var other: b2Body!  = nil
    unowned var contact: b2Contact
    var prev: b2ContactEdge? = nil
    var next: b2ContactEdge? = nil
    init(_ contact: b2Contact) {
        self.contact = contact
    }
}

open class b2Contact {
    var m_flags: Int = 0
    var m_prev: b2Contact? = nil
    var m_next: b2Contact? = nil
    var m_nodeA: b2ContactEdge!
    var m_nodeB: b2ContactEdge!
    var m_fixtureA: b2Fixture!
    var m_fixtureB: b2Fixture!
    var m_indexA: Int = 0
    var m_indexB: Int = 0
    var m_manifold = b2Manifold()
    var m_toiCount: Int = 0
    var m_toi: Double = 0
    var m_friction: Double = 0
    var m_restitution: Double = 0
    var m_tangentSpeed: Double = 0
    open var manifold: b2Manifold {
        return m_manifold
    }
    open var worldManifold: b2WorldManifold {
        let bodyA = m_fixtureA.body
        let bodyB = m_fixtureB.body
        let shapeA = m_fixtureA.shape
        let shapeB = m_fixtureB.shape
        let worldManifold = b2WorldManifold()
        worldManifold.initialize(m_manifold,
                                 bodyA.transform, shapeA.m_radius,
                                 bodyB.transform, shapeB.m_radius)
        return worldManifold
    }
    
    open var isTouching: Bool {
        return (m_flags & Flags_C.touchingFlag) == Flags_C.touchingFlag
    }
    
    open func setEnabled(_ flag: Bool) {
        if flag {
            m_flags |= Flags_C.enabledFlag
        } else {
            m_flags &= ~Flags_C.enabledFlag
        }
    }
    
    open var isEnabled: Bool {
        return (m_flags & Flags_C.enabledFlag) == Flags_C.enabledFlag
    }
    
    open func getNext() -> b2Contact? {
        return m_next
    }
    
    open var fixtureA: b2Fixture {
        return m_fixtureA
    }
    
    open var childIndexA: Int {
        return m_indexA
    }
    
    open var fixtureB: b2Fixture {
        return m_fixtureB
    }
    
    open var childIndexB: Int {
        return m_indexB
    }
    
    open var friction: Double {
        get {
            return m_friction
        }
        set {
            m_friction = newValue
        }
    }
    
    open func resetFriction() {
        m_friction = b2MixFriction(m_fixtureA.m_friction, m_fixtureB.m_friction)
    }
    
    open var restitution: Double {
        get {
            return m_restitution
        }
        set {
            m_restitution = newValue
        }
    }
    
    open func resetRestitution() {
        m_restitution = b2MixRestitution(m_fixtureA.m_restitution, m_fixtureB.m_restitution)
    }
    
    open func setTangentSpeed(_ speed: Double) {
        m_tangentSpeed = speed
    }
    
    open var tangentSpeed: Double {
        return m_tangentSpeed
    }
    
    open func evaluate(_ manifold: inout b2Manifold, _ xfA: b2Transform, _ xfB: b2Transform) {
        fatalError("must override")
    }
    
    func flagForFiltering() {
        fatalError("must override")
    }
    
    class func addType(_ createFcn: @escaping b2ContactCreateFcn, _ destoryFcn: @escaping b2ContactDestroyFcn, _ type1: b2ShapeType, _ type2: b2ShapeType) {
        StaticVars.s_registers.getItem(type1, type2).createFcn = createFcn
        StaticVars.s_registers.getItem(type1, type2).destroyFcn = destoryFcn
        StaticVars.s_registers.getItem(type1, type2).primary = true
        if (type1 != type2) {
            StaticVars.s_registers.getItem(type2, type1).createFcn = createFcn
            StaticVars.s_registers.getItem(type2, type1).destroyFcn = destoryFcn
            StaticVars.s_registers.getItem(type2, type1).primary = false
        }
    }
    
    class func initializeRegisters() {
        addType(b2PolygonContact.create, b2PolygonContact.destroy, b2ShapeType.polygon, b2ShapeType.polygon)
    }
    
    class func create(_ fixtureA: b2Fixture, _ indexA: Int, _ fixtureB: b2Fixture, _ indexB: Int) -> b2Contact? {
        if (StaticVars.s_initialized == false) {
            initializeRegisters()
            StaticVars.s_initialized = true
        }
        let type1 = fixtureA.fixtureType
        let type2 = fixtureB.fixtureType
        let createFcn = StaticVars.s_registers.getItem(type1, type2).createFcn
        if (createFcn != nil) {
            if (StaticVars.s_registers.getItem(type1, type2).primary) {
                return createFcn!(fixtureA, indexA, fixtureB, indexB)
            } else {
                return createFcn!(fixtureB, indexB, fixtureA, indexA)
            }
        } else {
            return nil
        }
    }
    
    class func destroy(_ contact: b2Contact) {
        let fixtureA = contact.m_fixtureA
        let fixtureB = contact.m_fixtureB
        if (contact.m_manifold.pointCount > 0 && fixtureA?.isSensor == false && fixtureB?.isSensor == false) {
            fixtureA?.body.setAwake(true)
            fixtureB?.body.setAwake(true)
        }
        let typeA = fixtureA?.fixtureType
        let typeB = fixtureB?.fixtureType
        let destroyFcn = StaticVars.s_registers.getItem(typeA!, typeB!).destroyFcn
        destroyFcn!(contact)
    }
    
    init(_ fixtureA: b2Fixture, _ indexA: Int, _ fixtureB: b2Fixture, _ indexB: Int) {
        m_flags = Flags_C.enabledFlag
        m_fixtureA = fixtureA
        m_fixtureB = fixtureB
        m_indexA = indexA
        m_indexB = indexB
        m_manifold.points.removeAll()
        m_prev = nil
        m_next = nil
        m_nodeA = b2ContactEdge(self)
        m_nodeA.prev = nil
        m_nodeA.next = nil
        m_nodeA.other = nil
        m_nodeB = b2ContactEdge(self)
        m_nodeB.prev = nil
        m_nodeB.next = nil
        m_nodeB.other = nil
        m_toiCount = 0
        m_friction = b2MixFriction(m_fixtureA.m_friction, m_fixtureB.m_friction)
        m_restitution = b2MixRestitution(m_fixtureA.m_restitution, m_fixtureB.m_restitution)
        m_tangentSpeed = 0.0
    }
    
    func update(_ listener: b2ContactListener?) {
        let oldManifold = b2Manifold(m_manifold)
        m_flags |= Flags_C.enabledFlag
        var touching = false
        let wasTouching = (m_flags & Flags_C.touchingFlag) == Flags_C.touchingFlag
        let sensorA = m_fixtureA.isSensor
        let sensorB = m_fixtureB.isSensor
        let sensor = sensorA || sensorB
        let bodyA = m_fixtureA.body
        let bodyB = m_fixtureB.body
        let xfA = bodyA.transform
        let xfB = bodyB.transform
        if (sensor) {
            let shapeA = m_fixtureA.shape
            let shapeB = m_fixtureB.shape
            touching = b2TestOverlap(shapeA, m_indexA,
                                     shapeB, m_indexB,
                                     xfA, xfB)
            m_manifold.points.removeAll()
        } else {
            evaluate(&m_manifold, xfA, xfB)
            touching = m_manifold.pointCount > 0
            for i in 0 ..< m_manifold.pointCount {
                let mp2 = m_manifold.points[i]
                mp2.normalImpulse = 0.0
                mp2.tangentImpulse = 0.0
                let id2 = mp2.id
                for j in 0 ..< oldManifold.pointCount {
                    let mp1 = oldManifold.points[j]
                    if (equalEqual(mp1.id, id2)) {
                        mp2.normalImpulse = mp1.normalImpulse
                        mp2.tangentImpulse = mp1.tangentImpulse
                        break
                    }
                }
            }
            if (touching != wasTouching) {
                bodyA.setAwake(true)
                bodyB.setAwake(true)
            }
        }
    }
}

class StaticVars {
    static var s_registers = b2ContactRegisters(b2ShapeType.typeCount, b2ShapeType.typeCount)
    static var s_initialized : Bool = false
}

public enum Flags_C {
    static let islandFlag      = 0x0001
    static let touchingFlag    = 0x0002
    static let enabledFlag     = 0x0004
    static let filterFlag      = 0x0008
    static let bulletHitFlag   = 0x0010
    static let toiFlag         = 0x0020
}

open class b2PolygonContact : b2Contact {
    override class func create(_ fixtureA: b2Fixture, _ indexA: Int, _ fixtureB: b2Fixture, _ indexB: Int) -> b2Contact {
        return b2PolygonContact(fixtureA, fixtureB)
    }
    override class func destroy(_ contact: b2Contact) {
    }
    
    init(_ fixtureA: b2Fixture, _ fixtureB: b2Fixture) {
        super.init(fixtureA, 0, fixtureB, 0)
    }
    
    override open func evaluate(_ manifold: inout b2Manifold, _ xfA: b2Transform, _ xfB: b2Transform) {
        b2CollidePolygons(&manifold,
                          m_fixtureA.shape as! b2PolygonShape, xfA,
                          m_fixtureB.shape as! b2PolygonShape, xfB)
    }
}

open class b2CircleContact : b2Contact {
    override class func create(_ fixtureA: b2Fixture, _ indexA: Int, _ fixtureB: b2Fixture, _ indexB: Int) -> b2Contact {
        return b2CircleContact(fixtureA, fixtureB)
    }
    
    override class func destroy(_ contact: b2Contact) {
    }
    
    init(_ fixtureA: b2Fixture, _ fixtureB: b2Fixture) {
        super.init(fixtureA, 0, fixtureB, 0)
    }
    
    override open func evaluate(_ manifold: inout b2Manifold, _ xfA: b2Transform, _ xfB: b2Transform) {
        b2CollideCircles(&manifold,
                         m_fixtureA.shape as! b2CircleShape, xfA,
                         m_fixtureB.shape as! b2CircleShape, xfB)
    }
}

open class b2EdgeAndCircleContact : b2Contact {
   override class func create(_ fixtureA: b2Fixture, _ indexA: Int, _ fixtureB: b2Fixture, _ indexB: Int) -> b2Contact {
       return b2EdgeAndCircleContact(fixtureA, fixtureB)
   }
   
   override class func destroy(_ contact: b2Contact) {
   }
   
   init(_ fixtureA: b2Fixture, _ fixtureB: b2Fixture) {
       super.init(fixtureA, 0, fixtureB, 0)
   }
   
   override open func evaluate(_ manifold: inout b2Manifold, _ xfA: b2Transform, _ xfB: b2Transform) {
       b2CollideEdgeAndCircle(&manifold,
                              m_fixtureA.shape as! b2EdgeShape, xfA,
                              m_fixtureB.shape as! b2CircleShape, xfB)
   }
}

open class b2EdgeAndPolygonContact : b2Contact {
   override class func create(_ fixtureA: b2Fixture, _ indexA: Int, _ fixtureB: b2Fixture, _ indexB: Int) -> b2Contact {
       return b2EdgeAndPolygonContact(fixtureA, fixtureB)
   }
   
   override class func destroy(_ contact: b2Contact) {
   }
   
   init(_ fixtureA: b2Fixture, _ fixtureB: b2Fixture) {
       super.init(fixtureA, 0, fixtureB, 0)
   }
   
   override open func evaluate(_ manifold: inout b2Manifold, _ xfA: b2Transform, _ xfB: b2Transform) {
       b2CollideEdgeAndPolygon(
           &manifold,
           m_fixtureA.shape as! b2EdgeShape, xfA,
           m_fixtureB.shape as! b2PolygonShape, xfB)
   }
}

open class b2PolygonAndCircleContact : b2Contact {
   override class func create(_ fixtureA: b2Fixture, _ indexA: Int, _ fixtureB: b2Fixture, _ indexB: Int) -> b2Contact {
       return b2PolygonAndCircleContact(fixtureA, fixtureB)
   }
   override class func destroy(_ contact: b2Contact) {
   }
   
   init(_ fixtureA : b2Fixture, _ fixtureB : b2Fixture) {
       super.init(fixtureA, 0, fixtureB, 0)
   }
   
   override open func evaluate(_ manifold: inout b2Manifold, _ xfA: b2Transform, _ xfB: b2Transform) {
       b2CollidePolygonAndCircle(&manifold,
                                 m_fixtureA.shape as! b2PolygonShape, xfA,
                                 m_fixtureB.shape as! b2CircleShape, xfB)
   }
}

public func equalEqual (_ lhs: b2ContactFeature, _ rhs: b2ContactFeature) -> Bool {
    return lhs.indexA == rhs.indexA && lhs.indexB == rhs.indexB && lhs.typeA == rhs.typeA && lhs.typeB == rhs.typeB
}
