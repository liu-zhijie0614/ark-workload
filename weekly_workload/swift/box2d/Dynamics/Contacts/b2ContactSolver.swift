 
open class b2VelocityConstraintPoint {
    var rA = b2Vec2()
    var rB = b2Vec2()
    var normalImpulse: Double = 0.0
    var tangentImpulse: Double = 0.0
    var normalMass: Double = 0.0
    var tangentMass: Double = 0.0
    var velocityBias: Double = 0.0
}

open class b2ContactVelocityConstraint {
    var points = [b2VelocityConstraintPoint]()
    var normal = b2Vec2()
    var normalMass = b2Mat22()
    var K = b2Mat22()
    var indexA: Int = 0
    var indexB: Int = 0
    var invMassA: Double = 0.0
    var invMassB: Double = 0.0
    var invIA: Double = 0.0
    var invIB: Double = 0.0
    var friction: Double = 0.0
    var restitution: Double = 0.0
    var tangentSpeed: Double = 0.0
    var pointCount: Int = 0
    var contactIndex: Int = 0
}

public class b2ContactSolverDef {
    var step = b2TimeStep()
    var contacts = [b2Contact]()
    var count: Int = 0
    var positions = b2Array<b2Position>()
    var velocities = b2Array<b2Velocity>()
}

open class b2ContactSolver {
    init(_ def : b2ContactSolverDef) {
        m_step = def.step
        m_count = def.count
        m_positionConstraints = [b2ContactPositionConstraint]()
        m_velocityConstraints = [b2ContactVelocityConstraint]()
        m_positions = def.positions
        m_velocities = def.velocities
        m_contacts = def.contacts
        for i in 0 ..< m_count {
            let contact = m_contacts[i]
            let fixtureA = contact.m_fixtureA
            let fixtureB = contact.m_fixtureB
            let shapeA = fixtureA?.shape
            let shapeB = fixtureB?.shape
            let radiusA = shapeA?.m_radius
            let radiusB = shapeB?.m_radius
            let bodyA = fixtureA?.body
            let bodyB = fixtureB?.body
            let manifold = contact.manifold
            let pointCount = manifold.pointCount
            let vc = b2ContactVelocityConstraint()
            vc.friction = contact.m_friction
            vc.restitution = contact.m_restitution
            vc.tangentSpeed = contact.m_tangentSpeed
            vc.indexA = (bodyA?.m_islandIndex)!
            vc.indexB = (bodyB?.m_islandIndex)!
            vc.invMassA = (bodyA?.m_invMass)!
            vc.invMassB = (bodyB?.m_invMass)!
            vc.invIA = (bodyA?.m_invI)!
            vc.invIB = (bodyB?.m_invI)!
            vc.contactIndex = i
            vc.pointCount = pointCount
            vc.K.setZero()
            vc.normalMass.setZero()
            m_velocityConstraints.append(vc)
            let pc = b2ContactPositionConstraint()
            pc.indexA = (bodyA?.m_islandIndex)!
            pc.indexB = (bodyB?.m_islandIndex)!
            pc.invMassA = (bodyA?.m_invMass)!
            pc.invMassB = (bodyB?.m_invMass)!
            pc.localCenterA = (bodyA?.m_sweep.localCenter)!
            pc.localCenterB = (bodyB?.m_sweep.localCenter)!
            pc.invIA = (bodyA?.m_invI)!
            pc.invIB = (bodyB?.m_invI)!
            pc.localNormal = manifold.localNormal
            pc.localPoint = manifold.localPoint
            pc.pointCount = pointCount
            pc.radiusA = radiusA!
            pc.radiusB = radiusB!
            pc.type = manifold.type
            m_positionConstraints.append(pc)
            for j in 0 ..< pointCount {
                let cp = manifold.points[j]
                let vcp = b2VelocityConstraintPoint()
                if (m_step.warmStarting) {
                    vcp.normalImpulse = m_step.dtRatio * cp.normalImpulse
                    vcp.tangentImpulse = m_step.dtRatio * cp.tangentImpulse
                } else {
                    vcp.normalImpulse = 0.0
                    vcp.tangentImpulse = 0.0
                }
                vcp.rA.setZero()
                vcp.rB.setZero()
                vcp.normalMass = 0.0
                vcp.tangentMass = 0.0
                vcp.velocityBias = 0.0
                vc.points.append(vcp)
                pc.localPoints[j] = cp.localPoint
            }
        }
    }
    
    func initializeVelocityConstraints() {
        for i in 0 ..< m_count {
            let vc = m_velocityConstraints[i]
            let pc = m_positionConstraints[i]
            let radiusA = pc.radiusA
            let radiusB = pc.radiusB
            let manifold = m_contacts[vc.contactIndex].manifold
            let indexA = vc.indexA
            let indexB = vc.indexB
            let mA = vc.invMassA
            let mB = vc.invMassB
            let iA = vc.invIA
            let iB = vc.invIB
            let localCenterA = pc.localCenterA
            let localCenterB = pc.localCenterB
            let cA = m_positions.get(indexA).c
            let aA = m_positions.get(indexA).a
            let vA = m_velocities.get(indexA).v
            let wA = m_velocities.get(indexA).w
            let cB = m_positions.get(indexB).c
            let aB = m_positions.get(indexB).a
            let vB = m_velocities.get(indexB).v
            let wB = m_velocities.get(indexB).w
            var xfA = b2Transform(), xfB = b2Transform()
            xfA.q.set(aA)
            xfB.q.set(aB)
            xfA.p = subtract(cA, b2MulR2(xfA.q, localCenterA))
            xfB.p = subtract(cB, b2MulR2(xfB.q, localCenterB))
            let worldManifold = b2WorldManifold()
            worldManifold.initialize(manifold, xfA, radiusA, xfB, radiusB)
            vc.normal = worldManifold.normal
            let pointCount = vc.pointCount
            for j in 0 ..< pointCount {
                let vcp = vc.points[j]
                vcp.rA = subtract(worldManifold.points[j], cA)
                vcp.rB = subtract(worldManifold.points[j], cB)
                let rnA = b2Cross(vcp.rA, vc.normal)
                let rnB = b2Cross(vcp.rB, vc.normal)
                let kNormal = mA + mB + iA * rnA * rnA + iB * rnB * rnB
                vcp.normalMass = kNormal > 0.0 ? 1.0 / kNormal : 0.0
                let tangent = b2Cross12(vc.normal, 1.0)
                let rtA = b2Cross(vcp.rA, tangent)
                let rtB = b2Cross(vcp.rB, tangent)
                let kTangent = mA + mB + iA * rtA * rtA + iB * rtB * rtB
                vcp.tangentMass = kTangent > 0.0 ? 1.0 /  kTangent : 0.0
                vcp.velocityBias = 0.0
                let vRel = b2Dot22(vc.normal, subtract(subtract(add(vB, b2Cross21(wB, vcp.rB)), vA), b2Cross21(wA, vcp.rA)))
                if (vRel < -b2_velocityThreshold) {
                    vcp.velocityBias = -vc.restitution * vRel
                }
            }
            if (vc.pointCount == 2) {
                let vcp1 = vc.points[0]
                let vcp2 = vc.points[1]
                let rn1A = b2Cross(vcp1.rA, vc.normal)
                let rn1B = b2Cross(vcp1.rB, vc.normal)
                let rn2A = b2Cross(vcp2.rA, vc.normal)
                let rn2B = b2Cross(vcp2.rB, vc.normal)
                let k11 = mA + mB + iA * rn1A * rn1A + iB * rn1B * rn1B
                let k22 = mA + mB + iA * rn2A * rn2A + iB * rn2B * rn2B
                let k12 = mA + mB + iA * rn1A * rn2A + iB * rn1B * rn2B
                let k_maxConditionNumber: Double = 1000.0
                if (k11 * k11 < k_maxConditionNumber * (k11 * k22 - k12 * k12)) {
                    vc.K.ex.set(k11, k12)
                    vc.K.ey.set(k12, k22)
                    vc.normalMass = vc.K.getInverse()
                } else {
                    vc.pointCount = 1
                }
            }
        }
    }
    
    func warmStart() {
        for i in 0 ..< m_count {
            let vc = m_velocityConstraints[i]
            let indexA = vc.indexA
            let indexB = vc.indexB
            let mA = vc.invMassA
            let iA = vc.invIA
            let mB = vc.invMassB
            let iB = vc.invIB
            let pointCount = vc.pointCount
            var vA = m_velocities.get(indexA).v
            var wA = m_velocities.get(indexA).w
            var vB = m_velocities.get(indexB).v
            var wB = m_velocities.get(indexB).w
            let normal = vc.normal
            let tangent = b2Cross12(normal, 1.0)
            for j in 0 ..< pointCount {
                let vcp = vc.points[j]
                let P = add(multM(normal, vcp.normalImpulse), multM(tangent, vcp.tangentImpulse))
                wA -= iA * b2Cross(vcp.rA, P)
                subtractEqual(&vA, multM(P, mA));
                wB += iB * b2Cross(vcp.rB, P)
                addEqual(&vB, multM(P, mB))
            }
            m_velocities.get(indexA).v = vA
            m_velocities.get(indexA).w = wA
            m_velocities.get(indexB).v = vB
            m_velocities.get(indexB).w = wB
        }
    }
    
    func solveVelocityConstraints() {
        for i in 0 ..< m_count {
            let vc = m_velocityConstraints[i]
            let indexA = vc.indexA
            let indexB = vc.indexB
            let mA = vc.invMassA
            let iA = vc.invIA
            let mB = vc.invMassB
            let iB = vc.invIB
            let pointCount = vc.pointCount
            var vA = m_velocities.get(indexA).v
            var wA = m_velocities.get(indexA).w
            var vB = m_velocities.get(indexB).v
            var wB = m_velocities.get(indexB).w
            let normal = vc.normal
            let tangent = b2Cross12(normal, 1.0)
            let friction = vc.friction
            for j in 0 ..< pointCount {
                let vcp = vc.points[j]
                let dv = subtract(subtract(add(vB, b2Cross21(wB, vcp.rB)), vA), b2Cross21(wA, vcp.rA))
                let vt = b2Dot22(dv, tangent) - vc.tangentSpeed
                var lambda = vcp.tangentMass * (-vt)
                let maxFriction = friction * vcp.normalImpulse
                let newImpulse = b2ClampF(vcp.tangentImpulse + lambda, -maxFriction, maxFriction)
                lambda = newImpulse - vcp.tangentImpulse
                vcp.tangentImpulse = newImpulse
                let P = multM(tangent, lambda)
                subtractEqual(&vA, multM(P, mA))
                wA -= iA * b2Cross(vcp.rA, P)
                addEqual(&vB, multM(P, mB))
                wB += iB * b2Cross(vcp.rB, P)
            }
            if (vc.pointCount == 1) {
                let vcp = vc.points[0]
                let dv = subtract(subtract(add(vB, b2Cross21(wB, vcp.rB)), vA), b2Cross21(wA, vcp.rA))
                let vn = b2Dot22(dv, normal)
                var lambda = -vcp.normalMass * (vn - vcp.velocityBias)
                let newImpulse = max(vcp.normalImpulse + lambda, 0.0)
                lambda = newImpulse - vcp.normalImpulse
                vcp.normalImpulse = newImpulse
                let P = multM(normal, lambda)
                subtractEqual(&vA, multM(P, mA))
                wA -= iA * b2Cross(vcp.rA, P)
                addEqual(&vB, multM(P, mB))
                wB += iB * b2Cross(vcp.rB, P)
            } else {
                var cp1 = vc.points[0]
                var cp2 = vc.points[1]
                let a = b2Vec2(cp1.normalImpulse, cp2.normalImpulse)
                var dv1 = subtract(subtract(add(vB, b2Cross21(wB, cp1.rB)), vA), b2Cross21(wA, cp1.rA))
                var dv2 = subtract(subtract(add(vB, b2Cross21(wB, cp2.rB)), vA), b2Cross21(wA, cp2.rA))
                var vn1 = b2Dot22(dv1, normal)
                var vn2 = b2Dot22(dv2, normal)
                var b = b2Vec2()
                b.x = vn1 - cp1.velocityBias
                b.y = vn2 - cp2.velocityBias
                subtractEqual(&b, b2Mul22(vc.K, a))
                while (true) {
                    var x = minus(b2Mul22(vc.normalMass, b))
                    if (x.x >= 0.0 && x.y >= 0.0) {
                        let d = subtract(x, a)
                        let P1 = multM(normal, d.x)
                        let P2 = multM(normal, d.y)
                        subtractEqual(&vA, multM((add(P1, P2)), mA))
                        wA -= iA * (b2Cross(cp1.rA, P1) + b2Cross(cp2.rA, P2))
                        addEqual(&vB, multM((add(P1, P2)), mB))
                        wB += iB * (b2Cross(cp1.rB, P1) + b2Cross(cp2.rB, P2))
                        cp1.normalImpulse = x.x
                        cp2.normalImpulse = x.y
                        dv1 = subtract(subtract(add(vB, b2Cross21(wB, cp1.rB)), vA), b2Cross21(wA, cp1.rA))
                        dv2 = subtract(subtract(add(vB, b2Cross21(wB, cp2.rB)), vA), b2Cross21(wA, cp2.rA))
                        vn1 = b2Dot22(dv1, normal)
                        vn2 = b2Dot22(dv2, normal)
                        break
                    }
                    x.x = -cp1.normalMass * b.x
                    x.y = 0.0
                    vn1 = 0.0
                    vn2 = vc.K.ex.y * x.x + b.y
                    if (x.x >= 0.0 && vn2 >= 0.0) {
                        let d = subtract(x, a)
                        let P1 = multM(normal, d.x)
                        let P2 = multM(normal, d.y)
                        subtractEqual(&vA, multM((add(P1, P2)), mA))
                        wA -= iA * (b2Cross(cp1.rA, P1) + b2Cross(cp2.rA, P2))
                        addEqual(&vB, multM((add(P1, P2)), mB))
                        wB += iB * (b2Cross(cp1.rB, P1) + b2Cross(cp2.rB, P2))
                        cp1.normalImpulse = x.x
                        cp2.normalImpulse = x.y
                        dv1 = subtract(subtract(add(vB, b2Cross21(wB, cp1.rB)), vA), b2Cross21(wA, cp1.rA))
                        vn1 = b2Dot22(dv1, normal)
                        break
                    }
                    x.x = 0.0
                    x.y = -cp2.normalMass * b.y
                    vn1 = vc.K.ey.x * x.y + b.x
                    vn2 = 0.0
                    if (x.y >= 0.0 && vn1 >= 0.0) {
                        let d = subtract(x, a)
                        let P1 = multM(normal, d.x)
                        let P2 = multM(normal, d.y)
                        subtractEqual(&vA, multM(add(P1, P2), mA))
                        wA -= iA * (b2Cross(cp1.rA, P1) + b2Cross(cp2.rA, P2))
                        addEqual(&vB, multM(add(P1, P2), mB))
                        wB += iB * (b2Cross(cp1.rB, P1) + b2Cross(cp2.rB, P2))
                        cp1.normalImpulse = x.x
                        cp2.normalImpulse = x.y
                        dv2 = subtract(subtract(add(vB, b2Cross21(wB, cp2.rB)), vA), b2Cross21(wA, cp2.rA))
                        vn2 = b2Dot22(dv2, normal)
                        break
                    }
                    x.x = 0.0
                    x.y = 0.0
                    vn1 = b.x
                    vn2 = b.y
                    if (vn1 >= 0.0 && vn2 >= 0.0) {
                        let d = subtract(x, a)
                        let P1 = multM(normal, d.x)
                        let P2 = multM(normal, d.y)
                        subtractEqual(&vA, multM(add(P1, P2), mA))
                        wA -= iA * (b2Cross(cp1.rA, P1) + b2Cross(cp2.rA, P2))
                        addEqual(&vB, multM(add(P1, P2), mB))
                        wB += iB * (b2Cross(cp1.rB, P1) + b2Cross(cp2.rB, P2))
                        cp1.normalImpulse = x.x
                        cp2.normalImpulse = x.y
                        break
                    }
                    break
                }
            }
            m_velocities.get(indexA).v = vA
            m_velocities.get(indexA).w = wA
            m_velocities.get(indexB).v = vB
            m_velocities.get(indexB).w = wB
        }
    }
    
    func storeImpulses() {
        for i in 0 ..< m_count {
            let vc = m_velocityConstraints[i]
            let manifold = m_contacts[vc.contactIndex].manifold
            for j in 0 ..< vc.pointCount {
                manifold.points[j].normalImpulse = vc.points[j].normalImpulse
                manifold.points[j].tangentImpulse = vc.points[j].tangentImpulse
            }
        }
    }
    
    func solvePositionConstraints() -> Bool {
        var minSeparation: Double = 0.0
        for i in 0 ..< m_count {
            let pc = m_positionConstraints[i]
            let indexA = pc.indexA
            let indexB = pc.indexB
            let localCenterA = pc.localCenterA
            let mA = pc.invMassA
            let iA = pc.invIA
            let localCenterB = pc.localCenterB
            let mB = pc.invMassB
            let iB = pc.invIB
            let pointCount = pc.pointCount
            var cA = m_positions.get(indexA).c
            var aA = m_positions.get(indexA).a
            var cB = m_positions.get(indexB).c
            var aB = m_positions.get(indexB).a
            for j in 0 ..< pointCount {
                let xfA = b2Transform(), xfB = b2Transform()
                xfA.q.set(aA)
                xfB.q.set(aB)
                xfA.p = subtract(cA, b2MulR2(xfA.q, localCenterA))
                xfB.p = subtract(cB, b2MulR2(xfB.q, localCenterB))
                let psm = b2PositionSolverManifold()
                psm.initialize(pc, xfA, xfB, j)
                let normal = psm.normal
                let point = psm.point
                let separation = psm.separation
                let rA = subtract(point, cA)
                let rB = subtract(point, cB)
                minSeparation = min(minSeparation, separation)
                let C = b2ClampF(b2_baumgarte * (separation + b2_linearSlop), -b2_maxLinearCorrection, 0.0)
                let rnA = b2Cross(rA, normal)
                let rnB = b2Cross(rB, normal)
                let K = mA + mB + iA * rnA * rnA + iB * rnB * rnB
                let impulse = K > 0.0 ? -C / K : 0.0
                let P = multM(normal, impulse)
                subtractEqual(&cA, multM(P, mA))
                aA -= iA * b2Cross(rA, P)
                addEqual(&cB, multM(P, mB))
                aB += iB * b2Cross(rB, P)
            }
            m_positions.get(indexA).c = cA
            m_positions.get(indexA).a = aA
            m_positions.get(indexB).c = cB
            m_positions.get(indexB).a = aB
        }
        return minSeparation >= -3.0 * b2_linearSlop
    }
    
    func solveTOIPositionConstraints(_ toiIndexA: Int, _ toiIndexB: Int) -> Bool {
        var minSeparation: Double = 0.0
        for i in 0 ..< m_count {
            let pc = m_positionConstraints[i]
            let indexA = pc.indexA
            let indexB = pc.indexB
            let localCenterA = pc.localCenterA
            let localCenterB = pc.localCenterB
            let pointCount = pc.pointCount
            var mA: Double = 0.0
            var iA: Double = 0.0
            if (indexA == toiIndexA || indexA == toiIndexB) {
                mA = pc.invMassA
                iA = pc.invIA
            }
            var mB: Double = 0.0
            var iB: Double = 0.0
            if (indexB == toiIndexA || indexB == toiIndexB) {
                mB = pc.invMassB
                iB = pc.invIB
            }
            var cA = m_positions.get(indexA).c
            var aA = m_positions.get(indexA).a
            var cB = m_positions.get(indexB).c
            var aB = m_positions.get(indexB).a
            for j in 0 ..< pointCount {
                let xfA = b2Transform()
                let xfB = b2Transform()
                xfA.q.set(aA)
                xfB.q.set(aB)
                xfA.p = subtract(cA, b2MulR2(xfA.q, localCenterA))
                xfB.p = subtract(cB, b2MulR2(xfB.q, localCenterB))
                let psm = b2PositionSolverManifold()
                psm.initialize(pc, xfA, xfB, j)
                let normal = psm.normal
                let point = psm.point
                let separation = psm.separation
                let rA = subtract(point, cA)
                let rB = subtract(point, cB)
                minSeparation = min(minSeparation, separation)
                let C = b2ClampF(b2_toiBaugarte * (separation + b2_linearSlop), -b2_maxLinearCorrection, 0.0)
                let rnA = b2Cross(rA, normal)
                let rnB = b2Cross(rB, normal)
                let K = mA + mB + iA * rnA * rnA + iB * rnB * rnB
                let impulse = K > 0.0 ? -C / K : 0.0
                let P = multM(normal, impulse)
                subtractEqual(&cA, multM(P, mA))
                aA -= iA * b2Cross(rA, P)
                addEqual(&cB, multM(P, mB))
                aB += iB * b2Cross(rB, P)
            }
            m_positions.get(indexA).c = cA
            m_positions.get(indexA).a = aA
            m_positions.get(indexB).c = cB
            m_positions.get(indexB).a = aB
        }
        return minSeparation >= -1.5 * b2_linearSlop
    }
    
    var m_step : b2TimeStep
    var m_positions : b2Array<b2Position>
    var m_velocities : b2Array<b2Velocity>
    var m_positionConstraints : [b2ContactPositionConstraint]
    var m_velocityConstraints : [b2ContactVelocityConstraint]
    var m_contacts : [b2Contact]
    var m_count : Int
}

open class b2ContactPositionConstraint {
    var localPoints :[b2Vec2]
    var localNormal = b2Vec2()
    var localPoint = b2Vec2()
    var indexA = 0
    var indexB = 0
    var invMassA: Double = 0.0
    var invMassB: Double = 0.0
    var localCenterA = b2Vec2()
    var localCenterB = b2Vec2()
    var invIA: Double = 0
    var invIB: Double = 0
    var type: b2ManifoldType = .circles
    var radiusA: Double = 0
    var radiusB : Double = 0
    var pointCount = 0
    
    init() {
        localPoints = []
        for _ in (0..<b2_maxManifoldPoints) {
            localPoints.append(b2Vec2())
        }
    }
}

internal class b2PositionSolverManifold {
    func initialize(_ pc: b2ContactPositionConstraint, _ xfA: b2Transform, _ xfB: b2Transform, _ index: Int) {
        switch (pc.type) {
        case .circles:
            let pointA = b2MulT2(xfA, pc.localPoint)
            let pointB = b2MulT2(xfB, pc.localPoints[0])
            normal = subtract(pointB, pointA)
            normal.normalize()
            point = multM(add(pointA, pointB), 0.5)
            separation = b2Dot22(subtract(pointB, pointA), normal) - pc.radiusA - pc.radiusB
            break
        case .faceA:
            normal = b2MulR2(xfA.q, pc.localNormal)
            let planePoint0 = b2MulT2(xfA, pc.localPoint)
            let clipPoint0 = b2MulT2(xfB, pc.localPoints[index])
            separation = b2Dot22(subtract(clipPoint0, planePoint0), normal) - pc.radiusA - pc.radiusB
            point = clipPoint0
            break
        case .faceB:
            normal = b2MulR2(xfB.q, pc.localNormal)
            let planePoint = b2MulT2(xfB, pc.localPoint)
            let clipPoint = b2MulT2(xfA, pc.localPoints[index])
            separation = b2Dot22(subtract(clipPoint, planePoint), normal) - pc.radiusA - pc.radiusB
            point = clipPoint
            normal = minus(normal)
            break
        default:
            break
        }
    }
    
    var normal = b2Vec2()
    var point = b2Vec2()
    var separation: Double = 0.0
}

