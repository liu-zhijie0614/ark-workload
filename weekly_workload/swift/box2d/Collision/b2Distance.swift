open class b2DistanceProxy {
    open var m_buffer: b2Array<b2Vec2> = b2Array()
    open var m_vertices: b2Array<b2Vec2>? = nil
    open var m_count = 0
    open var m_radius: Double = 0
    public init(_ shape: b2Shape? = nil, _ index: Int? = nil) {
        for _ in (0..<2) {
            m_buffer.append(b2Vec2())
        }
        if ((shape != nil) && (index != nil)) {
            set(shape!, index!)
        }
    }
    
    open func set(_ shape: b2Shape, _ index: Int) {
        switch shape.type {
        case .circle:
            let circle = shape as! b2CircleShape
            m_vertices = circle.m_p_
            m_count = 1
            m_radius = circle.m_radius
            break
        case .polygon:
            let polygon = shape as! b2PolygonShape
            m_vertices = polygon.m_vertices
            m_count = polygon.m_count
            m_radius = polygon.m_radius
            break
        case .edge:
            let edge = shape as! b2EdgeShape
            m_vertices = edge.m_vertices
            m_count = 2
            m_radius = edge.m_radius
            break
        default:
            break
        }
    }
    
    open func getSupport(_ d: b2Vec2) -> Int {
        var bestIndex = 0
        var bestValue = b2Dot22(m_vertices!.get(0), d)
        for i in 1 ..< m_count {
            let value = b2Dot22(m_vertices!.get(i), d)
            if value > bestValue {
                bestIndex = i
                bestValue = value
            }
        }
        return bestIndex
    }
    
    open func getSupportVertex(_ d: b2Vec2) -> b2Vec2 {
        var bestIndex = 0
        var bestValue = b2Dot22(m_vertices!.get(0), d)
        for i in 1 ..< m_count {
            let value = b2Dot22(m_vertices!.get(i), d)
            if value > bestValue {
                bestIndex = i
                bestValue = value
            }
        }
        return m_vertices!.get(bestIndex)
    }
    
    open func getVertexCount() -> Int {
        return m_count
    }
    
    open func getVertex(_ index: Int) -> b2Vec2 {
        return m_vertices!.get(index)
    }
}

class b2SimplexVertex {
    var wA = b2Vec2()
    var wB = b2Vec2()
    var w = b2Vec2()
    var a: Double = 0
    var indexA = 0
    var indexB = 0
    func set(_ other: b2SimplexVertex) {
        self.wA = other.wA
        self.wB = other.wB
        self.w = other.w
        self.a = other.a
        self.indexA = other.indexA
        self.indexB = other.indexB
    }
}

class b2Simplex {
    var m_count = 0
    init() {
        for _ in 0 ..< 3 {
            m_v.append(b2SimplexVertex())
        }
    }
    
    func readCache(_ cache: b2SimplexCache,
                   _ proxyA: b2DistanceProxy, _ transformA: b2Transform,
                   _ proxyB: b2DistanceProxy, _ transformB: b2Transform) {
        m_count = cache.count
        for i in 0 ..< m_count {
            let v = m_v[i]
            v.indexA = cache.indexA[i]
            v.indexB = cache.indexB[i]
            let wALocal = proxyA.getVertex(v.indexA)
            let wBLocal = proxyB.getVertex(v.indexB)
            v.wA = b2MulT2(transformA, wALocal)
            v.wB = b2MulT2(transformB, wBLocal)
            v.w = subtract(v.wB, v.wA)
            v.a = 0.0
        }
        if (m_count > 1) {
            let metric1 = cache.metric
            let metric2 = getMetric()
            if metric2 < 0.5 * metric1 ||
                2.0 * metric1 < metric2 ||
                metric2 < b2_epsilon {
                m_count = 0
            }
        }
        if (m_count == 0) {
            let v = m_v[0]
            v.indexA = 0
            v.indexB = 0
            let wALocal = proxyA.getVertex(0)
            let wBLocal = proxyB.getVertex(0)
            v.wA = b2MulT2(transformA, wALocal)
            v.wB = b2MulT2(transformB, wBLocal)
            v.w = subtract(v.wB, v.wA)
            v.a = 1.0
            m_count = 1
        }
    }
    
    func writeCache(_ cache: inout b2SimplexCache) {
        cache.metric = getMetric()
        cache.count = m_count
        for i in 0 ..< m_count {
            cache.indexA[i] = m_v[i].indexA
            cache.indexB[i] = m_v[i].indexB
        }
    }
    
    func getSearchDirection() -> b2Vec2 {
        switch (m_count) {
        case 1:
            return minus(m_v1.w)
        case 2:
            let e12 = subtract(m_v2.w, m_v1.w)
            let sgn = b2Cross(e12, minus(m_v1.w))
            if (sgn > 0.0) {
                return b2Cross21(1.0, e12)
            } else {
                return b2Cross12(e12, 1.0)
            }
        default:
            return b2Vec2_zero
        }
    }
    
    func getClosestPoint() -> b2Vec2 {
        switch (m_count) {
        case 0:
            return b2Vec2_zero
        case 1:
            return m_v1.w
        case 2:
            return add(multM(m_v1.w, m_v1.a), multM(m_v2.w, m_v2.a))
        case 3:
            return b2Vec2_zero
        default:
            return b2Vec2_zero
        }
    }
    
    func getWitnessPoints() -> (pA: b2Vec2, pB: b2Vec2) {
        var pA = b2Vec2()
        var pB = b2Vec2()
        switch (m_count) {
        case 0:
            fatalError("illegal state")
        case 1:
            pA = m_v1.wA
            pB = m_v1.wB
            break
        case 2:
            pA = add(multM(m_v1.wA, m_v1.a), multM(m_v2.wA, m_v2.a))
            pB = add(multM(m_v1.wB, m_v1.a), multM(m_v2.wB, m_v2.a))
            break
        case 3:
            pA = add(add(multM(m_v1.wA, m_v1.a), multM(m_v2.wA, m_v2.a)), multM(m_v3.wA, m_v3.a));
            break
        default:
            fatalError("illegal state")
        }
        return (pA, pB)
    }
    
    func getMetric() -> Double {
        switch (m_count) {
        case 0:
            return 0.0
        case 1:
            return 0.0
        case 2:
            return b2Distance(m_v1.w, m_v2.w)
        case 3:
            return b2Cross(subtract(m_v2.w, m_v1.w), subtract( m_v3.w, m_v1.w))
        default:
            return 0.0
        }
    }
    
    func solve2() {
        let w1 = m_v1.w
        let w2 = m_v2.w
        let e12 = subtract(w2, w1)
        let d12_2 = -b2Dot22(w1, e12)
        if (d12_2 <= 0.0) {
            m_v1.a = 1.0
            m_count = 1
            return
        }
        let d12_1 = b2Dot22(w2, e12)
        if (d12_1 <= 0.0) {
            m_v2.a = 1.0
            m_count = 1
            m_v1 = m_v2
            return
        }
        let inv_d12 = 1.0 / (d12_1 + d12_2)
        m_v1.a = d12_1 * inv_d12
        m_v2.a = d12_2 * inv_d12
        m_count = 2
    }
    
    func solve3() {
        let w1 = m_v1.w
        let w2 = m_v2.w
        let w3 = m_v3.w
        let e12 = subtract(w2, w1)
        let w1e12 = b2Dot22(w1, e12)
        let w2e12 = b2Dot22(w2, e12)
        let d12_1 = w2e12
        let d12_2 = -w1e12
        let e13 = subtract(w3, w1)
        let w1e13 = b2Dot22(w1, e13)
        let w3e13 = b2Dot22(w3, e13)
        let d13_1 = w3e13
        let d13_2 = -w1e13
        let e23 = subtract(w3,w2)
        let w2e23 = b2Dot22(w2, e23)
        let w3e23 = b2Dot22(w3, e23)
        let d23_1 = w3e23
        let d23_2 = -w2e23
        let n123 = b2Cross(e12, e13)
        let d123_1 = n123 * b2Cross(w2, w3)
        let d123_2 = n123 * b2Cross(w3, w1)
        let d123_3 = n123 * b2Cross(w1, w2)
        if (d12_2 <= 0.0 && d13_2 <= 0.0) {
            m_v1.a = 1.0
            m_count = 1
            return
        }
        if (d12_1 > 0.0 && d12_2 > 0.0 && d123_3 <= 0.0) {
            let inv_d12 = 1.0 / (d12_1 + d12_2)
            m_v1.a = d12_1 * inv_d12
            m_v2.a = d12_2 * inv_d12
            m_count = 2
            return
        }
        if (d13_1 > 0.0 && d13_2 > 0.0 && d123_2 <= 0.0) {
            let inv_d13 = 1.0 / (d13_1 + d13_2)
            m_v1.a = d13_1 * inv_d13
            m_v3.a = d13_2 * inv_d13
            m_count = 2
            m_v2 = m_v3
            return
        }
        if (d12_1 <= 0.0 && d23_2 <= 0.0) {
            m_v2.a = 1.0
            m_count = 1
            m_v1 = m_v2
            return
        }
        if (d13_1 <= 0.0 && d23_1 <= 0.0) {
            m_v3.a = 1.0
            m_count = 1
            m_v1 = m_v3
            return
        }
        if (d23_1 > 0.0 && d23_2 > 0.0 && d123_1 <= 0.0) {
            let inv_d23 = 1.0 / (d23_1 + d23_2)
            m_v2.a = d23_1 * inv_d23
            m_v3.a = d23_2 * inv_d23
            m_count = 2
            m_v1 = m_v3
            return
        }
        let inv_d123 = 1.0 / (d123_1 + d123_2 + d123_3)
        m_v1.a = d123_1 * inv_d123
        m_v2.a = d123_2 * inv_d123
        m_v3.a = d123_3 * inv_d123
        m_count = 3
    }
    
    var m_v = [b2SimplexVertex]()
    var m_v1: b2SimplexVertex {
        get {
            return m_v[0]
        }
        set {
            m_v[0].set(newValue)
        }
    }
    
    var m_v2: b2SimplexVertex {
        get {
            return m_v[1]
        }
        set {
            m_v[1].set(newValue)
        }
    }
    
    var m_v3: b2SimplexVertex {
        get {
            return m_v[2]
        }
        set {
            m_v[2].set(newValue)
        }
    }
}

public class b2SimplexCache {
    public var metric: Double
    public var count: Int
    public var indexA: [Int]
    public var indexB: [Int]
    public init() {
        metric = 0
        count = 0
        indexA = [0, 0, 0]
        indexB = [0, 0, 0]
    }
}

public class b2DistanceInput {
    public var proxyA: b2DistanceProxy
    public var proxyB: b2DistanceProxy
    public var transformA: b2Transform
    public var transformB: b2Transform
    public var useRadii: Bool
    public init() {
        proxyA = b2DistanceProxy()
        proxyB = b2DistanceProxy()
        transformA = b2Transform()
        transformB = b2Transform()
        useRadii = false
    }
}

public class b2DistanceOutput {
    public var pointA: b2Vec2
    public var pointB: b2Vec2
    public var distance: Double
    public var iterations: Int
    public init() {
        pointA = b2Vec2()
        pointB = b2Vec2()
        distance = 0
        iterations = 0
    }
}

public func b2DistanceM(_ output: inout b2DistanceOutput, _ cache: inout b2SimplexCache, _ input: b2DistanceInput) {
    b2_gjkCalls += 1
    let proxyA = input.proxyA
    let proxyB = input.proxyB
    let transformA = input.transformA
    let transformB = input.transformB
    var simplex = b2Simplex()
    simplex.readCache(cache, proxyA, transformA, proxyB, transformB)
    let k_maxIters = 20
    var saveA = [0,0,0], saveB = [0,0,0]
    var saveCount = 0
    var distanceSqr1 = b2_maxFloat
    var distanceSqr2 = distanceSqr1
    var iter = 0
    while iter < k_maxIters {
        saveCount = simplex.m_count
        for i in 0 ..< saveCount {
            saveA[i] = simplex.m_v[i].indexA
            saveB[i] = simplex.m_v[i].indexB
        }
        switch simplex.m_count {
        case 1:
            break
        case 2:
            simplex.solve2()
            break
        case 3:
            simplex.solve3()
            break
        default:
            break
        }
        if (simplex.m_count == 3) {
            break
        }
        let p = simplex.getClosestPoint()
        distanceSqr2 = p.lengthSquared()
        distanceSqr1 = distanceSqr2
        let d = simplex.getSearchDirection()
        if d.lengthSquared() < b2_epsilon * b2_epsilon {
            break
        }
        let vertex = simplex.m_v[simplex.m_count]
        vertex.indexA = proxyA.getSupport(b2MulTR2(transformA.q, minus(d)))
        vertex.wA = b2MulT2(transformA, proxyA.getVertex(vertex.indexA))
        vertex.indexB = proxyB.getSupport(b2MulTR2(transformB.q, d))
        vertex.wB = b2MulT2(transformB, proxyB.getVertex(vertex.indexB))
        vertex.w = subtract(vertex.wB, vertex.wA)
        iter += 1
        b2_gjkIters += 1
        var duplicate = false
        for i in 0 ..< saveCount {
            if vertex.indexA == saveA[i] && vertex.indexB == saveB[i] {
                duplicate = true
                break
            }
        }
        if duplicate {
            break
        }
        simplex.m_count += 1
    }
    b2_gjkMaxIters = max(b2_gjkMaxIters, iter)
    let sget = simplex.getWitnessPoints()
    output.pointA = sget.0
    output.pointB = sget.1
    output.distance = b2Distance(output.pointA, output.pointB)
    output.iterations = iter
    simplex.writeCache(&cache)
    if (input.useRadii) {
        let rA = proxyA.m_radius
        let rB = proxyB.m_radius
        if output.distance > rA + rB && output.distance > b2_epsilon {
            output.distance -= rA + rB
            var normal = subtract(output.pointB, output.pointA)
            normal.normalize()
            addEqual(&output.pointA, multM(normal, rA))
            subtractEqual(&output.pointB, multM(normal, rB))
        } else {
            let p = multM(add(output.pointA, output.pointB), 0.5)
            output.pointA = p
            output.pointB = p
            output.distance = 0.0
        }
    }
    return
}

public var b2_gjkCalls = 0
public var b2_gjkIters = 0
public var b2_gjkMaxIters = 0
