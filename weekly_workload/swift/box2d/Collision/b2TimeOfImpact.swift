 



public class b2TOIInput {
    public init() {}
    public var proxyA = b2DistanceProxy()
    public var proxyB = b2DistanceProxy()
    public var sweepA = b2Sweep()
    public var sweepB = b2Sweep()
    public var tMax: Double = 0.0
}

public enum State {
    case unknown
    case failed
    case overlapped
    case touching
    case separated
}

public class b2TOIOutput {
    public init() {}
    public var state = State.unknown
    public var t: Double = 0
}

public func b2TimeOfImpact(_ output: inout b2TOIOutput, input: b2TOIInput) {
    b2_toiCalls += 1
    output.state = State.unknown
    output.t = input.tMax
    let proxyA = input.proxyA
    let proxyB = input.proxyB
    var sweepA = input.sweepA
    var sweepB = input.sweepB
    sweepA.normalize()
    sweepB.normalize()
    let tMax = input.tMax
    let totalRadius = proxyA.m_radius + proxyB.m_radius
    let target = max(b2_linearSlop, totalRadius - 3.0 * b2_linearSlop)
    let tolerance = 0.25 * b2_linearSlop
    var t1: Double = 0.0
    let k_maxIterations = 20
    var iter = 0
    var cache = b2SimplexCache()
    cache.count = 0
    let distanceInput = b2DistanceInput()
    distanceInput.proxyA = input.proxyA
    distanceInput.proxyB = input.proxyB
    distanceInput.useRadii = false
    while true {
        let xfA = sweepA.getTransform(t1)
        let xfB = sweepB.getTransform(t1)
        distanceInput.transformA = xfA
        distanceInput.transformB = xfB
        var distanceOutput = b2DistanceOutput()
        b2DistanceM(&distanceOutput, &cache, distanceInput)
        if distanceOutput.distance <= 0.0 {
            output.state = State.overlapped
            output.t = 0.0
            break
        }
        if distanceOutput.distance < target + tolerance {
            output.state = State.touching
            output.t = t1
            break
        }
        let fcn = b2SeparationFunction()
        fcn.initialize(cache, proxyA, sweepA, proxyB, sweepB, t1)
        var done = false
        var t2 = tMax
        var pushBackIter = 0
        while true {
            let fms = fcn.findMinSeparation(t2)
            var s2 = fms.0;
            let indexA = fms.1;
            let indexB = fms.2;
            if s2 > target + tolerance {
                output.state = State.separated
                output.t = tMax
                done = true
                break
            }
            if s2 > target - tolerance {
                t1 = t2
                break
            }
            var s1 = fcn.evaluate(indexA, indexB, t1)
            if s1 < target - tolerance {
                output.state = State.failed
                output.t = t1
                done = true
                break
            }
            if s1 <= target + tolerance {
                output.state = State.touching
                output.t = t1
                done = true
                break
            }
            var rootIterCount = 0
            var a1 = t1, a2 = t2
            while true {
                var t: Double
                if ((rootIterCount & 1) != 0) {
                    t = a1 + (target - s1) * (a2 - a1) / (s2 - s1)
                } else {
                    t = 0.5 * (a1 + a2)
                }
                rootIterCount += 1
                b2_toiRootIters += 1
                let s = fcn.evaluate(indexA, indexB, t)
                if (abs(s - target) < tolerance) {
                    t2 = t
                    break
                }
                if (s > target) {
                    a1 = t
                    s1 = s
                } else {
                    a2 = t
                    s2 = s
                }
                if (rootIterCount == 50) {
                    break
                }
            }
            b2_toiMaxRootIters = max(b2_toiMaxRootIters, rootIterCount)
            pushBackIter += 1
            if pushBackIter == b2_maxPolygonVertices {
                break
            }
        }
        iter += 1
        b2_toiIters += 1
        if (done) {
            break
        }
        if (iter == k_maxIterations) {
            output.state = State.failed
            output.t = t1
            break
        }
    }
    b2_toiMaxIters = max(b2_toiMaxIters, iter)
}

public var b2_toiCalls = 0, b2_toiIters = 0, b2_toiMaxIters = 0
public var b2_toiRootIters = 0, b2_toiMaxRootIters = 0

enum TYPE {
    case points
    case faceA
    case faceB
}

private class b2SeparationFunction {
    func initialize(_ cache: b2SimplexCache,
                                       _ proxyA: b2DistanceProxy, _ sweepA: b2Sweep,
                                       _ proxyB: b2DistanceProxy, _ sweepB: b2Sweep,
                                       _ t1: Double) -> Double {
        m_proxyA = proxyA
        m_proxyB = proxyB
        let count = cache.count
        m_sweepA = sweepA
        m_sweepB = sweepB
        let xfA = m_sweepA.getTransform(t1)
        let xfB = m_sweepB.getTransform(t1)
        if count == 1 {
            m_type = TYPE.points
            let localPointA = m_proxyA.getVertex(cache.indexA[0])
            let localPointB = m_proxyB.getVertex(cache.indexB[0])
            let pointA = b2MulT2(xfA, localPointA)
            let pointB = b2MulT2(xfB, localPointB)
            m_axis = subtract(pointB, pointA)
            let s = m_axis.normalize()
            return s
        } else if cache.indexA[0] == cache.indexA[1] {
            m_type = TYPE.faceB
            let localPointB1 = proxyB.getVertex(cache.indexB[0])
            let localPointB2 = proxyB.getVertex(cache.indexB[1])
            m_axis = b2Cross12(subtract(localPointB2, localPointB1), 1.0)
            m_axis.normalize()
            let normal = b2MulR2(xfB.q, m_axis)
            m_localPoint = multM(add(localPointB1, localPointB2), 0.5)
            let pointB = b2MulT2(xfB, m_localPoint)
            let localPointA = proxyA.getVertex(cache.indexA[0])
            let pointA = b2MulT2(xfA, localPointA)
            var s = b2Dot22(subtract(pointA, pointB), normal)
            if s < 0.0 {
                m_axis = minus(m_axis)
                s = -s
            }
            return s
        } else {
            m_type = TYPE.faceA
            let localPointA1 = m_proxyA.getVertex(cache.indexA[0])
            let localPointA2 = m_proxyA.getVertex(cache.indexA[1])
            m_axis = b2Cross12(subtract(localPointA2, localPointA1), 1.0)
            m_axis.normalize()
            let normal = b2MulR2(xfA.q, m_axis)
            m_localPoint = multM(add(localPointA1, localPointA2), 0.5)
            let pointA = b2MulT2(xfA, m_localPoint)
            let localPointB = m_proxyB.getVertex(cache.indexB[0])
            let pointB = b2MulT2(xfB, localPointB)
            var s = b2Dot22(subtract(pointB, pointA), normal)
            if s < 0.0 {
                m_axis = minus(m_axis)
                s = -s
            }
            return s
        }
    }
    
    func findMinSeparation(_ t: Double) -> (separation: Double, indexA: Int, indexB: Int) {
        var indexA: Int
        var indexB: Int
        let xfA = m_sweepA.getTransform(t)
        let xfB = m_sweepB.getTransform(t)
        switch m_type {
        case TYPE.points:
            let axisA1 = b2MulTR2(xfA.q,  m_axis)
            let axisB1 = b2MulTR2(xfB.q, minus(m_axis))
            indexA = m_proxyA.getSupport(axisA1)
            indexB = m_proxyB.getSupport(axisB1)
            let localPointA1 = m_proxyA.getVertex(indexA)
            let localPointB1 = m_proxyB.getVertex(indexB)
            let pointA1 = b2MulT2(xfA, localPointA1)
            let pointB1 = b2MulT2(xfB, localPointB1)
            let separation1 = b2Dot22(subtract(pointB1, pointA1), m_axis)
            return (separation1, indexA, indexB)
        case TYPE.faceA:
            let normal2 = b2MulR2(xfA.q, m_axis)
            let pointA2 = b2MulT2(xfA, m_localPoint)
            let axisB = b2MulTR2(xfB.q, minus(normal2))
            indexA = -1
            indexB = m_proxyB.getSupport(axisB)
            let localPointB = m_proxyB.getVertex(indexB)
            let pointB2 = b2MulT2(xfB, localPointB)
            let separation2 = b2Dot22(subtract(pointB2, pointA2), normal2)
            return (separation2, indexA, indexB)
        case TYPE.faceB:
            let normal = b2MulR2(xfB.q, m_axis)
            let pointB = b2MulT2(xfB, m_localPoint)
            let axisA = b2MulTR2(xfA.q, minus(normal))
            indexB = -1
            indexA = m_proxyA.getSupport(axisA)
            let localPointA = m_proxyA.getVertex(indexA)
            let pointA = b2MulT2(xfA, localPointA)
            let separation = b2Dot22(subtract(pointA, pointB), normal)
            return (separation, indexA, indexB)
        }
    }
    
    func evaluate(_ indexA: Int, _ indexB: Int, _ t: Double) -> Double {
        let xfA = m_sweepA.getTransform(t)
        let xfB = m_sweepB.getTransform(t)
        switch m_type {
        case .points:
            let localPointA1 = m_proxyA.getVertex(indexA)
            let localPointB1 = m_proxyB.getVertex(indexB)
            let pointA1 = b2MulT2(xfA, localPointA1)
            let pointB1 = b2MulT2(xfB, localPointB1)
            let separation1 = b2Dot22(subtract(pointB1, pointA1), m_axis)
            return separation1
        case .faceA:
            let normal2 = b2MulR2(xfA.q, m_axis)
            let pointA2 = b2MulT2(xfA, m_localPoint)
            let localPointB = m_proxyB.getVertex(indexB)
            let pointB2 = b2MulT2(xfB, localPointB)
            let separation2 = b2Dot22(subtract(pointB2, pointA2), normal2)
            return separation2
        case .faceB:
            let normal = b2MulR2(xfB.q, m_axis)
            let pointB = b2MulT2(xfB, m_localPoint)
            let localPointA = m_proxyA.getVertex(indexA)
            let pointA = b2MulT2(xfA, localPointA)
            let separation = b2Dot22(subtract(pointA, pointB), normal)
            return separation
        }
    }
    
    var m_proxyA = b2DistanceProxy()
    var m_proxyB = b2DistanceProxy()
    var m_sweepA = b2Sweep()
    var m_sweepB = b2Sweep()
    var m_type = TYPE.points
    var m_localPoint = b2Vec2()
    var m_axis = b2Vec2()
}


