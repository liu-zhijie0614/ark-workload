
public class b2Pair {
    public var proxyIdA: Int = -1
    public var proxyIdB: Int = -1
    init(_ proxyIdA: Int = -1, _ proxyIdB: Int = -1) {
        self.proxyIdA = proxyIdA
        self.proxyIdB = proxyIdB
    }
}

open class b2BroadPhase : b2QueryWrapper {
    public static let nullProxy = -1
    var m_tree = b2DynamicTree<b2FixtureProxy>()
    var m_proxyCount: Int = 0
    var m_moveBuffer = [Int]()
    var m_pairBuffer = [b2Pair]()
    var m_queryProxyId: Int = 0
    
    public init() {
        m_proxyCount = 0
        m_pairBuffer = []
        m_moveBuffer = []
    }
    
    open func createProxy(_ aabb: b2AABB, _ userData: b2FixtureProxy) -> Int {
        let proxyId = m_tree.createProxy(aabb, userData)
        m_proxyCount += 1
        bufferMove(proxyId)
        return proxyId
    }
    
    open func destroyProxy(_ proxyId: Int) {
        unBufferMove(proxyId)
        m_proxyCount -= 1
        m_tree.destroyProxy(proxyId)
    }
    
    open func moveProxy(_ proxyId: Int, _ aabb: b2AABB, _ displacement: b2Vec2) {
        let buffer = m_tree.moveProxy(proxyId, aabb, displacement)
        if buffer {
            bufferMove(proxyId)
        }
    }
    
    open func touchProxy(_ proxyId: Int) {
        bufferMove(proxyId)
    }
    
    open func getFatAABB(_ proxyId: Int) -> b2AABB {
        return m_tree.getFatAABB(proxyId)
    }
    
    open func getUserData(_ proxyId : Int) -> b2FixtureProxy? {
        return m_tree.getUserData(proxyId)
    }
    
    open func testOverlap(_ proxyIdA : Int, _ proxyIdB : Int) -> Bool {
        let aabbA = m_tree.getFatAABB(proxyIdA)
        let aabbB = m_tree.getFatAABB(proxyIdB)
        return b2TestOverlap2(aabbA, aabbB)
    }
    
    open func getProxyCount() -> Int {
        return m_proxyCount
    }
    
    open func updatePairs<T: b2BroadPhaseWrapper>(_ callback: T) {
        m_pairBuffer.removeAll()
        for i in 0 ..< m_moveBuffer.count {
            m_queryProxyId = m_moveBuffer[i]
            if (m_queryProxyId == b2BroadPhase.nullProxy) {
                continue
            }
            let fatAABB = m_tree.getFatAABB(m_queryProxyId)
            m_tree.query(self, fatAABB)
        }
        m_moveBuffer.removeAll()
        m_pairBuffer.sort {
            if $0.proxyIdA == $1.proxyIdA {
                return $0.proxyIdB < $1.proxyIdB
            } else {
                return $0.proxyIdA < $1.proxyIdA
            }
        }
        
        var i = 0
        while (i < m_pairBuffer.count) {
            let primaryPair = m_pairBuffer[i]
            var userDataA = m_tree.getUserData(primaryPair.proxyIdA)!
            var userDataB = m_tree.getUserData(primaryPair.proxyIdB)!
            
            callback.addPair(&userDataA, &userDataB)
            i += 1
            
            while (i < m_pairBuffer.count) {
                let pair = m_pairBuffer[i]
                if pair.proxyIdA != primaryPair.proxyIdA || 
                    pair.proxyIdB != primaryPair.proxyIdB {
                    break
                }
                i += 1
            }
        }
    }
    
    open func query<T: b2QueryWrapper>(_ callback : T, _ aabb: b2AABB) {
        m_tree.query(callback, aabb)
    }
    
    open func rayCast<T: b2RayCastWrapper>(_ callback: T, _ input: b2RayCastInput) {
        m_tree.rayCast(callback, input)
    }
    
    open func getTreeHeight() -> Int {
        return m_tree.getHeight()
    }
    
    open func getTreeBalance() -> Int {
        return m_tree.getMaxBalance()
    }
    
    open func getTreeQuality() -> Double {
        return m_tree.getAreaRatio()
    }
    
    open func shiftOrigin(_ newOrigin: b2Vec2) {
        m_tree.shiftOrigin(newOrigin)
    }
    
    func bufferMove(_ proxyId: Int) {
        m_moveBuffer.append(proxyId)
    }
    
    func unBufferMove(_ proxyId: Int) {
        for i in 0 ..< m_moveBuffer.count {
            if m_moveBuffer[i] == proxyId {
                m_moveBuffer[i] = b2BroadPhase.nullProxy
            }
        }
    }
    
    open func queryCallback(_ proxyId: Int) -> Bool {
        if proxyId == m_queryProxyId {
            return true
        }
        let pair = b2Pair(min(proxyId, m_queryProxyId),
                          max(proxyId, m_queryProxyId))
        m_pairBuffer.append(pair)
        return true
    }
}
