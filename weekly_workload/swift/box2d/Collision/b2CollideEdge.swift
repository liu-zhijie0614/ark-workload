 

public func b2CollideEdgeAndCircle(_ manifold: inout b2Manifold,
                                   _ edgeA: b2EdgeShape, _ xfA: b2Transform,
                                   _ circleB: b2CircleShape, _ xfB: b2Transform) {
    
    manifold.points.removeAll()
    let Q = b2MulTT2(xfA, b2MulT2(xfB, circleB.m_p))
    
    let A = edgeA.m_vertex1
    let B = edgeA.m_vertex2
    let e = subtract(B, A)
    
    let u = b2Dot22(e, subtract(B, Q))
    let v = b2Dot22(e, subtract(Q, A))
    
    let radius = edgeA.m_radius + circleB.m_radius
    
    var cf = b2ContactFeature()
    cf.indexB = 0
    cf.typeB = b2ContactFeatureType.vertex
    
    if (v <= 0.0) {
        let P = A
        let d = subtract(Q, P) 
        let dd = b2Dot22(d, d)
        if (dd > radius * radius) {
            return
        }
        
        if (edgeA.m_hasVertex0) {
            let A1 = edgeA.m_vertex0
            let B1 = A
            let e1 = subtract(B1, A1)
            let u1 = b2Dot22(e1, subtract(B1, Q))
            
            if (u1 > 0.0) {
                return
            }
        }
        
        cf.indexA = 0
        cf.typeA = b2ContactFeatureType.vertex
        manifold.type = b2ManifoldType.circles
        manifold.localNormal.setZero()
        manifold.localPoint = P
        let cp = b2ManifoldPoint()
        cp.id.setZero()
        cp.id = cf
        cp.localPoint = circleB.m_p
        manifold.points.append(cp)
        return
    }
    if (u <= 0.0) {
        let P = B
        let d = subtract(Q, P)
        let dd = b2Dot22(d, d)
        if (dd > radius * radius) {
            return
        }
        if (edgeA.m_hasVertex3) {
            let B2 = edgeA.m_vertex3
            let A2 = B
            let e2 = subtract(B2, A2)
            let v2 = b2Dot22(e2, subtract(Q, A2))
            if v2 > 0.0 {
                return
            }
        }
        
        cf.indexA = 1
        cf.typeA = b2ContactFeatureType.vertex
        manifold.type = b2ManifoldType.circles
        manifold.localNormal.setZero()
        manifold.localPoint = P
        let cp = b2ManifoldPoint()
        cp.id.setZero()
        cp.id = cf
        cp.localPoint = circleB.m_p
        manifold.points.append(cp)
        return
    }
    
    let den = b2Dot22(e, e)
    let P = multM(add(multM(A, u), multM(B, v)), 1.0 / den)
    let d = subtract(Q, P)
    let dd = b2Dot22(d, d)
    if (dd > radius * radius) {
        return
    }
    var n = b2Vec2(-e.y, e.x)
    if (b2Dot22(n, subtract(Q, A)) < 0.0) {
        n.set(-n.x, -n.y)
    }
    n.normalize()
    
    cf.indexA = 0
    cf.typeA = b2ContactFeatureType.face
    manifold.type = b2ManifoldType.faceA
    manifold.localNormal = n
    manifold.localPoint = A
    let cp = b2ManifoldPoint()
    cp.id.setZero()
    cp.id = cf
    cp.localPoint = circleB.m_p
    manifold.points.append(cp)
    return
}

enum b2EPAxisType {
    case unknown
    case edgeA
    case edgeB
}

class b2EPAxis {
    var type = b2EPAxisType.unknown
    var index = 0
    var separation: Double = 0
}

class b2TempPolygon {
    var vertices = [b2Vec2]()
    var normals = [b2Vec2]()
    var count = 0
    init() {
        for _ in 0..<b2_maxPolygonVertices {
            vertices.append(b2Vec2())
            normals.append(b2Vec2())
        }
    }
}

class b2ReferenceFace {
    var i1 = 0
    var i2 = 0
    var v1 = b2Vec2()
    var v2 = b2Vec2()
    var normal = b2Vec2()
    var sideNormal1 = b2Vec2()
    var sideOffset1: Double = 0
    var sideNormal2 = b2Vec2()
    var sideOffset2: Double = 0
}

class b2EPCollider {
    var m_polygonB = b2TempPolygon()
    var m_xf = b2Transform()
    var m_centroidB = b2Vec2()
    var m_v0 = b2Vec2()
    var m_v1 = b2Vec2()
    var m_v2 = b2Vec2()
    var m_v3 = b2Vec2()
    var m_normal0 = b2Vec2()
    var m_normal1 = b2Vec2()
    var m_normal2 = b2Vec2()
    var m_normal = b2Vec2()
    var m_type1 = VertexType.isolated
    var m_type2 = VertexType.isolated
    var m_lowerLimit = b2Vec2()
    var m_upperLimit = b2Vec2()
    var m_radius: Double = 0
    var m_front = false
    
    func collide(_ edgeA: b2EdgeShape, _ xfA: b2Transform, _ polygonB: b2PolygonShape, _ xfB: b2Transform) -> b2Manifold {
        let manifold = b2Manifold()
        m_xf = b2MulT(xfA, xfB)
        m_centroidB = b2MulT2(m_xf, polygonB.m_centroid)
        m_v0 = edgeA.m_vertex0
        m_v1 = edgeA.m_vertex1
        m_v2 = edgeA.m_vertex2
        m_v3 = edgeA.m_vertex3
        let hasVertex0 = edgeA.m_hasVertex0
        let hasVertex3 = edgeA.m_hasVertex3
        var edge1 = subtract(m_v2, m_v1)
        edge1.normalize()
        m_normal1.set(edge1.y, -edge1.x)
        let offset1 = b2Dot22(m_normal1, subtract(m_centroidB, m_v1))
        var offset0: Double = 0.0, offset2: Double = 0.0
        var convex1 = false, convex2 = false
        if (hasVertex0) {
            var edge0 = subtract(m_v1, m_v0)
            edge0.normalize()
            m_normal0.set(edge0.y, -edge0.x)
            convex1 = b2Cross(edge0, edge1) >= 0.0
            offset0 = b2Dot22(m_normal0, subtract(m_centroidB, m_v0))
        }
        if (hasVertex3) {
            var edge2 = subtract(m_v3, m_v2)
            edge2.normalize()
            m_normal2.set(edge2.y, -edge2.x)
            convex2 = b2Cross(edge1, edge2) > 0.0
            offset2 = b2Dot22(m_normal2, subtract(m_centroidB, m_v2))
        }
        if (hasVertex0 && hasVertex3) {
            if (convex1 && convex2) {
                m_front = offset0 >= 0.0 || offset1 >= 0.0 || offset2 >= 0.0
                if (m_front) {
                    m_normal = m_normal1
                    m_lowerLimit = m_normal0
                    m_upperLimit = m_normal2
                } else {
                    m_normal = minus(m_normal1)
                    m_lowerLimit = minus(m_normal1)
                    m_upperLimit = minus(m_normal1)
                }
            } else if (convex1) {
                m_front = offset0 >= 0.0 || (offset1 >= 0.0 && offset2 >= 0.0)
                if (m_front) {
                    m_normal = m_normal1
                    m_lowerLimit = m_normal0
                    m_upperLimit = m_normal1
                } else {
                    m_normal = minus(m_normal1)
                    m_lowerLimit = minus(m_normal2)
                    m_upperLimit = minus(m_normal1)
                }
            } else if (convex2) {
                m_front = offset2 >= 0.0 || (offset0 >= 0.0 && offset1 >= 0.0)
                if m_front {
                    m_normal = m_normal1
                    m_lowerLimit = m_normal1
                    m_upperLimit = m_normal2
                } else {
                    m_normal = minus(m_normal1)
                    m_lowerLimit = minus(m_normal1)
                    m_upperLimit = minus(m_normal0)
                }
            } else {
                m_front = offset0 >= 0.0 && offset1 >= 0.0 && offset2 >= 0.0
                if (m_front) {
                    m_normal = m_normal1
                    m_lowerLimit = m_normal1
                    m_upperLimit = m_normal1
                } else {
                    m_normal = minus(m_normal1)
                    m_lowerLimit = minus(m_normal2)
                    m_upperLimit = minus(m_normal0)
                }
            }
        } else if (hasVertex0) {
            if (convex1) {
                m_front = offset0 >= 0.0 || offset1 >= 0.0
                if (m_front) {
                    m_normal = m_normal1
                    m_lowerLimit = m_normal0
                    m_upperLimit = minus(m_normal1)
                } else {
                    m_normal = minus(m_normal1)
                    m_lowerLimit = m_normal1
                    m_upperLimit = minus(m_normal1)
                }
            } else {
                m_front = offset0 >= 0.0 && offset1 >= 0.0
                if (m_front) {
                    m_normal = m_normal1
                    m_lowerLimit = m_normal1
                    m_upperLimit = minus(m_normal1)
                } else {
                    m_normal = minus(m_normal1)
                    m_lowerLimit = m_normal1
                    m_upperLimit = minus(m_normal0)
                }
            }
        } else if (hasVertex3) {
            if (convex2) {
                m_front = offset1 >= 0.0 || offset2 >= 0.0
                if (m_front) {
                    m_normal = m_normal1
                    m_lowerLimit = minus(m_normal1)
                    m_upperLimit = m_normal2
                } else {
                    m_normal = minus( m_normal1)
                    m_lowerLimit = minus(m_normal1)
                    m_upperLimit = m_normal1
                }
            } else {
                m_front = offset1 >= 0.0 && offset2 >= 0.0
                if (m_front) {
                    m_normal = m_normal1
                    m_lowerLimit = minus(m_normal1)
                    m_upperLimit = m_normal1
                } else {
                    m_normal = minus(m_normal1)
                    m_lowerLimit = minus(m_normal2)
                    m_upperLimit = m_normal1
                }
            }
        } else {
            m_front = offset1 >= 0.0
            if (m_front) {
                m_normal = m_normal1
                m_lowerLimit = minus(m_normal1)
                m_upperLimit = minus(m_normal1)
            } else {
                m_normal = minus(m_normal1)
                m_lowerLimit = m_normal1
                m_upperLimit = m_normal1
            }
        }
        m_polygonB.count = polygonB.m_count
        for i in 0 ..< polygonB.m_count {
            m_polygonB.vertices[i] = b2MulT2(m_xf, polygonB.m_vertices.get(i))
            m_polygonB.normals[i] = b2MulR2(m_xf.q, polygonB.m_normals.get(i))
        }
        m_radius = 2.0 * b2_polygonRadius
        manifold.points.removeAll()
        let edgeAxis = computeEdgeSeparation()
        if (edgeAxis.type == b2EPAxisType.unknown) {
            return manifold
        }
        if (edgeAxis.separation > m_radius) {
            return manifold
        }
        let polygonAxis = computePolygonSeparation()
        if (polygonAxis.type != b2EPAxisType.unknown && polygonAxis.separation > m_radius) {
            return manifold
        }
        let k_relativeTol:Double = 0.98
        let k_absoluteTol:Double = 0.001
        var primaryAxis: b2EPAxis
        if (polygonAxis.type == b2EPAxisType.unknown) {
            primaryAxis = edgeAxis
        } else if (polygonAxis.separation > k_relativeTol * edgeAxis.separation + k_absoluteTol) {
            primaryAxis = polygonAxis
        } else {
            primaryAxis = edgeAxis
        }
        var ie = [b2ClipVertex]()
        for _ in (0..<2) {
            ie.append(b2ClipVertex())
        }
        let rf = b2ReferenceFace()
        if (primaryAxis.type == b2EPAxisType.edgeA) {
            manifold.type = b2ManifoldType.faceA
            var bestIndex = 0
            var bestValue = b2Dot22(m_normal, m_polygonB.normals[0])
            for i in 1 ..< m_polygonB.count {
                let value = b2Dot22(m_normal, m_polygonB.normals[i])
                if value < bestValue {
                    bestValue = value
                    bestIndex = i
                }
            }
            let i1 = bestIndex
            let i2 = i1 + 1 < m_polygonB.count ? i1 + 1 : 0
            ie[0].v = m_polygonB.vertices[i1]
            ie[0].id.indexA = 0
            ie[0].id.indexB = i1
            ie[0].id.typeA = b2ContactFeatureType.face
            ie[0].id.typeB = b2ContactFeatureType.vertex
            ie[1].v = m_polygonB.vertices[i2]
            ie[1].id.indexA = 0
            ie[1].id.indexB = i2
            ie[1].id.typeA = b2ContactFeatureType.face
            ie[1].id.typeB = b2ContactFeatureType.vertex
            if (m_front) {
                rf.i1 = 0
                rf.i2 = 1
                rf.v1 = m_v1
                rf.v2 = m_v2
                rf.normal = m_normal1
            } else {
                rf.i1 = 1
                rf.i2 = 0
                rf.v1 = m_v2
                rf.v2 = m_v1
                rf.normal = minus(m_normal1)
            }
        } else {
            manifold.type = b2ManifoldType.faceB
            ie[0].v = m_v1
            ie[0].id.indexA = 0
            ie[0].id.indexB = primaryAxis.index
            ie[0].id.typeA = b2ContactFeatureType.vertex
            ie[0].id.typeB = b2ContactFeatureType.face
            ie[1].v = m_v2
            ie[1].id.indexA = 0
            ie[1].id.indexB = primaryAxis.index
            ie[1].id.typeA = b2ContactFeatureType.vertex
            ie[1].id.typeB = b2ContactFeatureType.face
            rf.i1 = primaryAxis.index
            rf.i2 = rf.i1 + 1 < m_polygonB.count ? rf.i1 + 1 : 0
            rf.v1 = m_polygonB.vertices[rf.i1]
            rf.v2 = m_polygonB.vertices[rf.i2]
            rf.normal = m_polygonB.normals[rf.i1]
        }
        rf.sideNormal1.set(rf.normal.y, -rf.normal.x)
        rf.sideNormal2 = minus(rf.sideNormal1)
        rf.sideOffset1 = b2Dot22(rf.sideNormal1, rf.v1)
        rf.sideOffset2 = b2Dot22(rf.sideNormal2, rf.v2)
        let clipPoints1 = b2ClipSegmentToLine(ie, rf.sideNormal1, rf.sideOffset1, rf.i1)
        if (clipPoints1.count < b2_maxManifoldPoints) {
            return manifold
        }
        let clipPoints2 = b2ClipSegmentToLine(clipPoints1, rf.sideNormal2, rf.sideOffset2, rf.i2)
        if (clipPoints2.count < b2_maxManifoldPoints) {
            return manifold
        }
        if (primaryAxis.type == b2EPAxisType.edgeA) {
            manifold.localNormal = rf.normal
            manifold.localPoint = rf.v1
        } else {
            manifold.localNormal = polygonB.m_normals.get(rf.i1)
            manifold.localPoint = polygonB.m_vertices.get(rf.i1)
        }
        var pointCount = 0
        for i in 0 ..< b2_maxManifoldPoints {
            let separation = b2Dot22(rf.normal, subtract(clipPoints2[i].v, rf.v1))
            if (separation <= m_radius) {
                let cp = b2ManifoldPoint()
                if primaryAxis.type == b2EPAxisType.edgeA {
                    cp.localPoint = b2MulTT2(m_xf, clipPoints2[i].v)
                    cp.id = clipPoints2[i].id
                } else {
                    cp.localPoint = clipPoints2[i].v
                    cp.id.typeA = clipPoints2[i].id.typeB
                    cp.id.typeB = clipPoints2[i].id.typeA
                    cp.id.indexA = clipPoints2[i].id.indexB
                    cp.id.indexB = clipPoints2[i].id.indexA
                }
                manifold.points.append(cp)
                pointCount += 1
            }
        }
        return manifold
    }
    
    func computeEdgeSeparation() -> b2EPAxis {
        let axis = b2EPAxis()
        axis.type = b2EPAxisType.edgeA
        axis.index = m_front ? 0 : 1
        axis.separation = b2_maxFloat
        for i in 0 ..< m_polygonB.count {
            let s = b2Dot22(m_normal, subtract(m_polygonB.vertices[i], m_v1))
            if (s < axis.separation) {
                axis.separation = s
            }
        }
        return axis
    }
    
    func computePolygonSeparation() -> b2EPAxis {
        let axis = b2EPAxis()
        axis.type = b2EPAxisType.unknown
        axis.index = -1
        axis.separation = -b2_maxFloat
        let perp = b2Vec2(-m_normal.y, m_normal.x)
        for i in 0 ..< m_polygonB.count {
            let n = minus(m_polygonB.normals[i])
            let s1 = b2Dot22(n, subtract(m_polygonB.vertices[i], m_v1))
            let s2 = b2Dot22(n, subtract(m_polygonB.vertices[i], m_v2))
            let s = min(s1, s2)
            if (s > m_radius) {
                axis.type = b2EPAxisType.edgeB
                axis.index = i
                axis.separation = s
                return axis
            }
            if (b2Dot22(n, perp) >= 0.0) {
                if b2Dot22(subtract(n, m_upperLimit), m_normal) < -b2_angularSlop {
                    continue
                }
            } else {
                if (b2Dot22(subtract(n, m_lowerLimit), m_normal) < -b2_angularSlop) {
                    continue
                }
            }
            if (s > axis.separation) {
                axis.type = b2EPAxisType.edgeB
                axis.index = i
                axis.separation = s
            }
        }
        return axis
    }
}

enum VertexType {
    case isolated
    case concave
    case convex
}

public func b2CollideEdgeAndPolygon(_ manifold: inout b2Manifold,
                                    _ edgeA: b2EdgeShape, _ xfA: b2Transform,
                                    _ polygonB: b2PolygonShape, _ xfB: b2Transform) {
    let collider = b2EPCollider()
    manifold = collider.collide(edgeA, xfA, polygonB, xfB)
}

