 



public let b2_nullNode = -1
open class b2TreeNode<T> {
    open var aabb = b2AABB()
    open var userData: T? = nil
    var parentOrNext: Int = b2_nullNode
    var child1: Int = b2_nullNode
    var child2: Int = b2_nullNode
    var height: Int = -1
    func IsLeaf() -> Bool {
        return child1 == b2_nullNode
    }
}

open class b2DynamicTree<T> {
    var m_root: Int
    var m_nodes: [b2TreeNode<T>]
    var m_nodeCount: Int
    var m_nodeCapacity: Int
    var m_freeList: Int
    var m_insertionCount: Int
    public init() {
        m_root = b2_nullNode
        m_nodeCapacity = 16
        m_nodeCount = 0
        m_nodes = Array<b2TreeNode<T>>()
        for i in 0 ..< m_nodeCapacity - 1 {
            m_nodes.append(b2TreeNode())
            m_nodes[m_nodes.count - 1].parentOrNext = i + 1
            m_nodes[m_nodes.count - 1].height = -1
        }
        m_nodes.append(b2TreeNode())
        m_nodes[m_nodes.count - 1].parentOrNext = b2_nullNode
        m_nodes[m_nodes.count - 1].height = -1
        m_freeList = 0
        m_insertionCount = 0
    }
    
    open func createProxy(_ aabb: b2AABB, _ userData: T?) -> Int {
        let proxyId = allocateNode()
        let r = b2Vec2(b2_aabbExtension, b2_aabbExtension)
        m_nodes[proxyId].aabb.lowerBound = subtract(aabb.lowerBound, r)
        m_nodes[proxyId].aabb.upperBound = add(aabb.upperBound, r)
        m_nodes[proxyId].userData = userData
        m_nodes[proxyId].height = 0
        insertLeaf(proxyId)
        return proxyId
    }
    
    open func destroyProxy(_ proxyId: Int) {
        removeLeaf(proxyId)
        freeNode(proxyId)
    }
    
    open func moveProxy(_ proxyId: Int, _ aabb: b2AABB, _ displacement: b2Vec2) -> Bool {
        if m_nodes[proxyId].aabb.contains(aabb) {
            return false
        }
        removeLeaf(proxyId)
        var b = aabb
        let r = b2Vec2(b2_aabbExtension, b2_aabbExtension)
        b.lowerBound = subtract(b.lowerBound, r)
        b.upperBound = add(b.upperBound, r)
        let d = multM(displacement, b2_aabbMultiplier)
        if (d.x < 0.0) {
            b.lowerBound.x += d.x
        } else {
            b.upperBound.x += d.x
        }
        if (d.y < 0.0) {
            b.lowerBound.y += d.y
        } else {
            b.upperBound.y += d.y
        }
        m_nodes[proxyId].aabb = b
        insertLeaf(proxyId)
        return true
    }
    
    open func getUserData(_ proxyId: Int) -> T? {
        return m_nodes[proxyId].userData
    }
    
    open func getFatAABB(_ proxyId: Int) -> b2AABB {
        return m_nodes[proxyId].aabb
    }
    
    open func query<T: b2QueryWrapper>(_ callback: T, _ aabb: b2AABB) {
        var stack:[Int] = []
        stack.append(m_root)
        while (stack.count > 0) {
            let nodeId = stack.removeLast()
            if (nodeId == b2_nullNode) {
                continue
            }
            let node = m_nodes[nodeId]
            if (b2TestOverlap2(node.aabb, aabb)) {
                if (node.IsLeaf()) {
                    let proceed = callback.queryCallback(nodeId)
                    if (proceed == false) {
                        return
                    }
                } else {
                    stack.append(node.child1)
                    stack.append(node.child2)
                }
            }
        }
    }
    
    open func rayCast<T: b2RayCastWrapper>(_ callback: T, _ input: b2RayCastInput) {
        let p1 = input.p1
        let p2 = input.p2
        var r = subtract(p2, p1)
        r.normalize()
        let v = b2Cross21(1.0, r)
        let abs_v = b2Abs2(v)
        var maxFraction = input.maxFraction
        var segmentAABB = b2AABB()
        let t = add(p1, multM(subtract(p2, p1), maxFraction))
        segmentAABB.lowerBound = b2Min(p1, t)
        segmentAABB.upperBound = b2Max(p1, t)
        var stack:[Int] = []
        stack.append(m_root)
        while (stack.count > 0) {
            let nodeId = stack.removeLast()
            if (nodeId == b2_nullNode) {
                continue
            }
            let node = m_nodes[nodeId]
            if (b2TestOverlap2(node.aabb, segmentAABB)) == false {
                continue
            }
            let c = node.aabb.center
            let h = node.aabb.extents
            let separation = abs(b2Dot22(v, subtract(p1, c))) - b2Dot22(abs_v, h)
            if (separation > 0.0) {
                continue
            }
            if (node.IsLeaf()) {
                var subInput = b2RayCastInput()
                subInput.p1 = input.p1
                subInput.p2 = input.p2
                subInput.maxFraction = maxFraction
                let value = callback.rayCastCallback(subInput, nodeId)
                if (value == 0.0) {
                    return
                }
                if (value > 0.0) {
                    maxFraction = value
                    let t = add(p1, multM(subtract(p2, p1), maxFraction))
                    segmentAABB.lowerBound = b2Min(p1, t)
                    segmentAABB.upperBound = b2Max(p1, t)
                }
            } else {
                stack.append(node.child1)
                stack.append(node.child2)
            }
        }
    }
        
    open func validate() {
        validateStructure(m_root)
        validateMetrics(m_root)
        var freeCount = 0
        var freeIndex = m_freeList
        while (freeIndex != b2_nullNode) {
            freeIndex = m_nodes[freeIndex].parentOrNext
            freeCount += 1
        }
    }
    
    open func getHeight() -> Int {
        if (m_root == b2_nullNode) {
            return 0
        }
        return m_nodes[m_root].height
    }
    
    open func getMaxBalance() -> Int {
        var maxBalance = 0
        for i in 0 ..< m_nodes.count {
            let node = m_nodes[i]
            if (node.height <= 1) {
                continue
            }
            let child1 = node.child1
            let child2 = node.child2
            let balance = abs(m_nodes[child2].height - m_nodes[child1].height)
            maxBalance = max(maxBalance, balance)
        }
        return maxBalance
    }
    
    open func getAreaRatio() -> Double {
        if (m_root == b2_nullNode) {
            return 0.0
        }
        let root = m_nodes[m_root]
        let rootArea = root.aabb.perimeter
        var totalArea: Double = 0.0
        for i in 0 ..< m_nodes.count {
            let node = m_nodes[i]
            if (node.height < 0) {
                continue
            }
            totalArea += node.aabb.perimeter
        }
        return totalArea / rootArea
    }
    
    open func rebuildBottomUp() {
        var nodes :[Int] = []
        for _ in (0..<m_nodes.count) {
            nodes.append(0)
        }
        var count = 0
        for i in 0 ..< m_nodes.count {
            if (m_nodes[i].height < 0) {
                continue
            }
            if (m_nodes[i].IsLeaf()) {
                m_nodes[i].parentOrNext = b2_nullNode
                nodes[count] = i
                count += 1
            } else {
                freeNode(i)
            }
        }
        while (count > 1) {
            var minCost = b2_maxFloat
            var iMin = -1, jMin = -1
            for i in 0 ..< count {
                let aabbi = m_nodes[nodes[i]].aabb
                for j in i + 1 ..< count {
                    let aabbj = m_nodes[nodes[j]].aabb
                    var b = b2AABB()
                    b.combine(aabbi, aabbj)
                    let cost = b.perimeter
                    if (cost < minCost) {
                        iMin = i
                        jMin = j
                        minCost = cost
                    }
                }
            }
            let index1 = nodes[iMin]
            let index2 = nodes[jMin]
            let child1 = m_nodes[index1]
            let child2 = m_nodes[index2]
            let parentIndex = allocateNode()
            let parent = m_nodes[parentIndex]
            parent.child1 = index1
            parent.child2 = index2
            parent.height = 1 + max(child1.height, child2.height)
            parent.aabb.combine(child1.aabb, child2.aabb)
            parent.parentOrNext = b2_nullNode
            child1.parentOrNext = parentIndex
            child2.parentOrNext = parentIndex
            nodes[jMin] = nodes[count-1]
            nodes[iMin] = parentIndex
            count -= 1
        }
        m_root = nodes[0]
        validate()
    }
    
    open func shiftOrigin(_ newOrigin: b2Vec2) {
        for i in 0 ..< m_nodes.count {
            subtractEqual(&m_nodes[i].aabb.lowerBound, newOrigin)
            subtractEqual(&m_nodes[i].aabb.upperBound, newOrigin)
        }
    }
    
    fileprivate func allocateNode() -> Int {
        if (m_freeList == b2_nullNode) {
            let node = b2TreeNode<T>()
            node.parentOrNext = b2_nullNode
            node.height = -1
            m_nodes.append(node)
            m_freeList = m_nodes.count - 1
        }
        let nodeId = m_freeList
        m_freeList = m_nodes[nodeId].parentOrNext
        m_nodes[nodeId].parentOrNext = b2_nullNode
        m_nodes[nodeId].child1 = b2_nullNode
        m_nodes[nodeId].child2 = b2_nullNode
        m_nodes[nodeId].height = 0
        m_nodes[nodeId].userData = nil
        return nodeId
    }
    
    func freeNode(_ nodeId: Int) {
        m_nodes[nodeId].parentOrNext = m_freeList
        m_nodes[nodeId].height = -1
        m_freeList = nodeId
    }
    
    func insertLeaf(_ leaf: Int) {
        m_insertionCount += 1
        if (m_root == b2_nullNode) {
            m_root = leaf
            m_nodes[m_root].parentOrNext = b2_nullNode
            return
        }
        let leafAABB = m_nodes[leaf].aabb
        var index = m_root
        while (m_nodes[index].IsLeaf() == false) {
            let child1 = m_nodes[index].child1
            let child2 = m_nodes[index].child2
            let area = m_nodes[index].aabb.perimeter
            var combinedAABB = b2AABB()
            combinedAABB.combine(m_nodes[index].aabb, leafAABB)
            let combinedArea = combinedAABB.perimeter
            let cost = 2.0 * combinedArea
            let inheritanceCost = 2.0 * (combinedArea - area)
            var cost1: Double
            if (m_nodes[child1].IsLeaf()) {
                var aabb = b2AABB()
                aabb.combine(leafAABB, m_nodes[child1].aabb)
                cost1 = aabb.perimeter + inheritanceCost
            } else {
                var aabb = b2AABB()
                aabb.combine(leafAABB, m_nodes[child1].aabb)
                let oldArea = m_nodes[child1].aabb.perimeter
                let newArea = aabb.perimeter
                cost1 = (newArea - oldArea) + inheritanceCost
            }
            var cost2: Double
            if (m_nodes[child2].IsLeaf()) {
                var aabb = b2AABB()
                aabb.combine(leafAABB, m_nodes[child2].aabb)
                cost2 = aabb.perimeter + inheritanceCost
            } else {
                var aabb = b2AABB()
                aabb.combine(leafAABB, m_nodes[child2].aabb)
                let oldArea = m_nodes[child2].aabb.perimeter
                let newArea = aabb.perimeter
                cost2 = newArea - oldArea + inheritanceCost
            }
            if (cost < cost1 && cost < cost2) {
                break
            }
            if (cost1 < cost2) {
                index = child1
            } else {
                index = child2
            }
        }
        let sibling = index
        let oldParent = m_nodes[sibling].parentOrNext
        let newParent = allocateNode()
        m_nodes[newParent].parentOrNext = oldParent
        m_nodes[newParent].userData = nil
        m_nodes[newParent].aabb.combine(leafAABB, m_nodes[sibling].aabb)
        m_nodes[newParent].height = m_nodes[sibling].height + 1
        if oldParent != b2_nullNode {
            if (m_nodes[oldParent].child1 == sibling) {
                m_nodes[oldParent].child1 = newParent
            } else {
                m_nodes[oldParent].child2 = newParent
            }
            m_nodes[newParent].child1 = sibling
            m_nodes[newParent].child2 = leaf
            m_nodes[sibling].parentOrNext = newParent
            m_nodes[leaf].parentOrNext = newParent
        } else {
            m_nodes[newParent].child1 = sibling
            m_nodes[newParent].child2 = leaf
            m_nodes[sibling].parentOrNext = newParent
            m_nodes[leaf].parentOrNext = newParent
            m_root = newParent
        }
        index = m_nodes[leaf].parentOrNext
        while (index != b2_nullNode) {
            index = balance(index)
            let child1 = m_nodes[index].child1
            let child2 = m_nodes[index].child2
            m_nodes[index].height = 1 + max(m_nodes[child1].height, m_nodes[child2].height)
            m_nodes[index].aabb.combine(m_nodes[child1].aabb, m_nodes[child2].aabb)
            index = m_nodes[index].parentOrNext
        }
    }
    
    func removeLeaf(_ leaf: Int) {
        if (leaf == m_root) {
            m_root = b2_nullNode
            return
        }
        let parent = m_nodes[leaf].parentOrNext
        let grandParent = m_nodes[parent].parentOrNext
        var sibling: Int
        if (m_nodes[parent].child1 == leaf) {
            sibling = m_nodes[parent].child2
        } else {
            sibling = m_nodes[parent].child1
        }
        if (grandParent != b2_nullNode) {
            if (m_nodes[grandParent].child1 == parent) {
                m_nodes[grandParent].child1 = sibling
            } else {
                m_nodes[grandParent].child2 = sibling
            }
            m_nodes[sibling].parentOrNext = grandParent
            freeNode(parent)
            var index = grandParent
            while (index != b2_nullNode) {
                index = balance(index)
                let child1 = m_nodes[index].child1
                let child2 = m_nodes[index].child2
                m_nodes[index].aabb.combine(m_nodes[child1].aabb, m_nodes[child2].aabb)
                m_nodes[index].height = 1 + max(m_nodes[child1].height, m_nodes[child2].height)
                index = m_nodes[index].parentOrNext
            }
        } else {
            m_root = sibling
            m_nodes[sibling].parentOrNext = b2_nullNode
            freeNode(parent)
        }
    }
    
    func balance(_ iA : Int) -> Int {
        let A = m_nodes[iA]
        if (A.IsLeaf() || A.height < 2) {
            return iA
        }
        let iB = A.child1
        let iC = A.child2
        let B = m_nodes[iB]
        let C = m_nodes[iC]
        let balance = C.height - B.height
        if (balance > 1) {
            let iF = C.child1
            let iG = C.child2
            let F = m_nodes[iF]
            let G = m_nodes[iG]
            C.child1 = iA
            C.parentOrNext = A.parentOrNext
            A.parentOrNext = iC
            if (C.parentOrNext != b2_nullNode) {
                if (m_nodes[C.parentOrNext].child1) == iA {
                    m_nodes[C.parentOrNext].child1 = iC
                } else {
                    m_nodes[C.parentOrNext].child2 = iC
                }
            } else {
                m_root = iC
            }
            if (F.height > G.height) {
                C.child2 = iF
                A.child2 = iG
                G.parentOrNext = iA
                A.aabb.combine(B.aabb, G.aabb)
                C.aabb.combine(A.aabb, F.aabb)
                A.height = 1 + max(B.height, G.height)
                C.height = 1 + max(A.height, F.height)
            } else {
                C.child2 = iG
                A.child2 = iF
                F.parentOrNext = iA
                A.aabb.combine(B.aabb, F.aabb)
                C.aabb.combine(A.aabb, G.aabb)
                A.height = 1 + max(B.height, F.height)
                C.height = 1 + max(A.height, G.height)
            }
            return iC
        }
        if (balance < -1) {
            let iD = B.child1
            let iE = B.child2
            let D = m_nodes[iD]
            let E = m_nodes[iE]
            B.child1 = iA
            B.parentOrNext = A.parentOrNext
            A.parentOrNext = iB
            if (B.parentOrNext != b2_nullNode) {
                if (m_nodes[B.parentOrNext].child1 == iA) {
                    m_nodes[B.parentOrNext].child1 = iB
                } else {
                    m_nodes[B.parentOrNext].child2 = iB
                }
            } else {
                m_root = iB
            }
            if (D.height > E.height) {
                B.child2 = iD
                A.child1 = iE
                E.parentOrNext = iA
                A.aabb.combine(C.aabb, E.aabb)
                B.aabb.combine(A.aabb, D.aabb)
                A.height = 1 + max(C.height, E.height)
                B.height = 1 + max(A.height, D.height)
            } else {
                B.child2 = iE
                A.child1 = iD
                D.parentOrNext = iA
                A.aabb.combine(C.aabb, D.aabb)
                B.aabb.combine(A.aabb, E.aabb)
                A.height = 1 + max(C.height, D.height)
                B.height = 1 + max(A.height, E.height)
            }
            return iB
        }
        return iA
    }
    
    func validateStructure(_ index : Int) {
        if (index == b2_nullNode) {
            return
        }
        let node = m_nodes[index]
        let child1 = node.child1
        let child2 = node.child2
        if (node.IsLeaf()) {
            return
        }
        validateStructure(child1)
        validateStructure(child2)
    }
    
    func validateMetrics(_ index : Int) {
        if index == b2_nullNode {
            return
        }
        let node = m_nodes[index]
        let child1 = node.child1
        let child2 = node.child2
        if node.IsLeaf() {
            return
        }
        var aabb = b2AABB()
        aabb.combine(m_nodes[child1].aabb, m_nodes[child2].aabb)
        validateMetrics(child1)
        validateMetrics(child2)
    }
}
