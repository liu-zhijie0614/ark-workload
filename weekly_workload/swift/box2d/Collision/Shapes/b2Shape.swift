



public class b2MassData {
    public var mass: Double = 0
    public var center = b2Vec2()
    public var I: Double = 0
}

public enum b2ShapeType: Int {
    case circle = 0
    case edge = 1
    case polygon = 2
    case chain = 3
    case typeCount = 4
}

open class b2Shape {
    public init() {
        m_type = b2ShapeType.circle
        m_radius = 0
    }
    
    open func clone() -> b2Shape {
        fatalError("must override")
    }
    
    open var type: b2ShapeType {
        return m_type
    }
    
    open var childCount: Int {
        fatalError("must override")
    }
    
    open func testPoint(_ transform: b2Transform, _ point: b2Vec2) -> Bool {
        fatalError("must override")
    }
    
    open func rayCast(_ output: inout b2RayCastOutput, _ input: b2RayCastInput, _ transform: b2Transform, _ childIndex: Int) -> Bool {
        fatalError("must override")
    }
    
    open func computeAABB(_ aabb: inout b2AABB, _ transform: b2Transform, _ childIndex: Int) {
        fatalError("must override")
    }
    
    open func computeMass(_ density: Double) -> b2MassData {
        fatalError("must override")
    }
    
    var m_type: b2ShapeType
    var m_radius: Double
    open var radius: Double {
        get {
            return m_radius
        }
        set {
            m_radius = newValue
        }
    }
}

open class b2CircleShape : b2Shape {
    public override init() {
        super.init()
        m_type = b2ShapeType.circle
        m_radius = 0.0
        m_p = b2Vec2(0.0, 0.0)
    }
    
    open override func clone() -> b2Shape {
        let clone = b2CircleShape()
        clone.m_radius = m_radius
        clone.m_p = m_p
        return clone
    }
    
    open override var childCount: Int {
        return 1
    }
    
    open override func testPoint(_ transform: b2Transform, _ point: b2Vec2) -> Bool {
        let center = add(transform.p, b2MulR2(transform.q, m_p))
        let d = subtract(p, center)
        return b2Dot22(d, d) <= m_radius * m_radius
    }
    
    open override func rayCast(_ output: inout b2RayCastOutput, _ input: b2RayCastInput, _ transform: b2Transform, _ childIndex: Int) -> Bool {
        let position = add(transform.p, b2MulR2(transform.q, m_p))
        let s = subtract(input.p1, position)

        let b = b2Dot22(s, s) - m_radius * m_radius
        let r = subtract(input.p2, input.p1)
        let c =  b2Dot22(s, r)
        let rr = b2Dot22(r, r)
        let sigma = c * c - rr * b
        if (sigma < 0.0 || rr < b2_epsilon) {
            return false
        }
        var a = -(c + b2Sqrt(sigma))
        if (0.0 <= a && a <= input.maxFraction * rr) {
            a /= rr
            output.fraction = a
            output.normal = add(s, multM(r, a))
            output.normal.normalize()
            return true
        }
        return false
    }
    
    open override func computeAABB(_ aabb: inout b2AABB, _ transform: b2Transform, _ childIndex: Int) {
        let p = add(transform.p, b2MulR2(transform.q, m_p))
        aabb.lowerBound.set(p.x - m_radius, p.y - m_radius)
        aabb.upperBound.set(p.x + m_radius, p.y + m_radius)
    }
    
    open override func computeMass(_ density: Double) -> b2MassData {
        let massData = b2MassData()
        massData.mass = density * b2_pi * m_radius * m_radius
        massData.center = m_p
        massData.I = massData.mass * (0.5 * m_radius * m_radius + b2Dot22(m_p, m_p))
        return massData
    }
    
    open func getSupport(_ direction: b2Vec2) -> Int {
        return 0
    }
    
    open func getSupportVertex(_ direction: b2Vec2) -> b2Vec2 {
        return m_p
    }
    
    open var vertexCount: Int {
        return 1
    }
    
    open func vertex(_ index: Int) -> b2Vec2 {
        return m_p
    }
    
    open var p: b2Vec2 {
        get {
            return m_p_.get(0)
        }
        set {
            m_p_.set(0, newValue)
        }
    }
    
    var m_p_ = b2Array<b2Vec2>(1, b2Vec2())
    var m_p: b2Vec2 {
        get {
            return m_p_.get(0)
        }
        set {
            m_p_.set(0, newValue)
        }
    }
}

open class b2EdgeShape : b2Shape {
    public override init() {
        m_vertex0 = b2Vec2(0.0, 0.0)
        m_vertex3 = b2Vec2(0.0, 0.0)
        m_hasVertex0 = false
        m_hasVertex3 = false
        super.init()
        m_type = b2ShapeType.edge
        m_radius = b2_polygonRadius
    }
    
    open func set(_ v1: b2Vec2, _ v2: b2Vec2) {
        m_vertex1 = v1
        m_vertex2 = v2
        m_hasVertex0 = false
        m_hasVertex3 = false
    }
    
    open override func clone() -> b2Shape {
        let clone = b2EdgeShape()
        clone.m_radius = m_radius
        clone.m_vertices = m_vertices.clone()
        clone.m_vertex0 = m_vertex0
        clone.m_vertex3 = m_vertex3
        clone.m_hasVertex0 = m_hasVertex0
        clone.m_hasVertex3 = m_hasVertex3
        return clone
    }
    
    open override var childCount: Int {
        return 1
    }
    
    open override func testPoint(_ transform: b2Transform, _ point: b2Vec2) -> Bool {
        return false
    }
    
    open override func computeAABB(_ aabb: inout b2AABB, _ transform: b2Transform, _ childIndex: Int) {
        let v1 = b2MulT2(transform, m_vertex1)
        let v2 = b2MulT2(transform, m_vertex2)
        let lower = b2Min(v1, v2)
        let upper = b2Max(v1, v2)
        let r = b2Vec2(m_radius, m_radius)
        aabb.lowerBound = subtract(lower, r)
        aabb.upperBound = add(upper, r)
    }
    
    open override func computeMass(_ density: Double) -> b2MassData {
        let massData = b2MassData()
        massData.mass = 0.0
        massData.center = multM(add(m_vertex1, m_vertex2), 0.5)
        massData.I = 0.0
        return massData
    }
    
    open var vertex1 : b2Vec2 {
        get {
            return m_vertices.get(0)
        }
        set {
            m_vertices.set(0, newValue)
        }
    }
    
    open var vertex2 : b2Vec2 {
        get {
            return m_vertices.get(1)
        }
        set {
            m_vertices.set(1, newValue)
        }
    }
    
    open var vertex0: b2Vec2 {
        get {
            return m_vertex0
        }
        set {
            m_vertex0 = newValue
        }
    }
    
    open var vertex3: b2Vec2 {
        get {
            return m_vertex3
        }
        set {
            m_vertex3 = newValue
        }
    }
    
    open var hasVertex0: Bool {
        get {
            return m_hasVertex0
        }
        set {
            m_hasVertex0 = newValue
        }
    }
    
    open var hasVertex3: Bool {
        get {
            return m_hasVertex3
        }
        set {
            m_hasVertex3 = newValue
        }
    }
    
    var m_vertices = b2Array<b2Vec2>(2, b2Vec2())
    var m_vertex1 : b2Vec2 {
        get {
            return m_vertices.get(0)
        }
        set {
            m_vertices.set(0, newValue)
        }
    }
    
    var m_vertex2 : b2Vec2 {
        get {
            return m_vertices.get(1)
        }
        set {
            m_vertices.set(1, newValue)
        }
    }
    
    var m_vertex0 : b2Vec2
    var m_vertex3 : b2Vec2
    var m_hasVertex0 : Bool
    var m_hasVertex3 : Bool
}

open class b2PolygonShape : b2Shape {
    public override init() {
        super.init()
        m_centroid = b2Vec2(0.0, 0.0)
        m_type = b2ShapeType.polygon
        m_radius = b2_polygonRadius
    }
    
    open override func clone() -> b2Shape {
        let clone = b2PolygonShape()
        clone.m_centroid = m_centroid
        clone.m_vertices = m_vertices.clone()
        clone.m_normals = m_normals.clone()
        clone.m_radius = m_radius
        return clone
    }
    
    open override var childCount: Int {
        return 1
    }
    
    open func setAsEdge(_ v1: b2Vec2, _ v2: b2Vec2) {
        m_vertices.append(v1)
        m_vertices.append(v2)
        m_centroid.x = 0.5 * (v1.x + v2.x);
        m_centroid.y = 0.5 * (v1.y + v2.y);
        m_normals.append(crossVF(subtractVV(v2, v1), 1.0))
        var mn0 = m_normals.get(0)
        mn0.normalize()
        m_normals.append(b2Vec2((-m_normals.get(0).x), (-m_normals.get(0).y)))
    }
    
    open func setAsBox(_ hx: Double, _ hy: Double) {
        m_vertices.removeAll()
        m_vertices.append(b2Vec2(-hx, -hy))
        m_vertices.append(b2Vec2( hx, -hy))
        m_vertices.append(b2Vec2( hx,  hy))
        m_vertices.append(b2Vec2(-hx,  hy))
        m_normals.removeAll()
        m_normals.append(b2Vec2( 0.0, -1.0))
        m_normals.append(b2Vec2( 1.0,  0.0))
        m_normals.append(b2Vec2( 0.0,  1.0))
        m_normals.append(b2Vec2(-1.0,  0.0))
        m_centroid.setZero()
    }

    public func subtractVV(_ a: b2Vec2, _ b: b2Vec2) -> b2Vec2 {
        let v = b2Vec2(a.x - b.x, a.y - b.y)
        return v
    }
    
    public func crossVV(_ a: b2Vec2, _ b: b2Vec2) -> Double {
        return a.x * b.y - a.y * b.x
    }
    
    public func crossVF(_ a: b2Vec2, _ s: Double) -> b2Vec2 {
        let v = b2Vec2(s * a.y, (-s * a.x))
        return v
    }
    
    open override func computeAABB(_ aabb: inout b2AABB, _ transform: b2Transform, _ childIndex: Int) {
        var lower = b2MulT2(transform, m_vertices.get(0))
        var upper = lower
        for i in 1 ..< m_count {
            let v = b2MulT2(transform, m_vertices.get(i))
            lower = b2Min(lower, v)
            upper = b2Max(upper, v)
        }
        let r = b2Vec2(m_radius, m_radius)
        aabb.lowerBound = subtract(lower, r)
        aabb.upperBound = add(upper, r)
    }
    
    open override func computeMass(_ density: Double) -> b2MassData {
        var center = b2Vec2(0.0, 0.0)
        var area: Double = 0.0
        var I: Double = 0.0
        var s = b2Vec2(0.0, 0.0)
        for i in 0 ..< m_count {
            addEqual(&s, m_vertices.get(i))
        }
        mulEqual(&s, 1.0 / Double(m_vertices.count))
        let k_inv3: Double = 1.0 / 3.0
        for i in 0 ..< m_count {
            let e1 = subtract(m_vertices.get(i), s)
            let e2 = i + 1 < m_count ? subtract(m_vertices.get(i+1), s) : subtract(m_vertices.get(0),s)
            let D = b2Cross(e1, e2)
            let triangleArea = 0.5 * D
            area += triangleArea
            addEqual(&center, multM(add(e1, e2), k_inv3 * triangleArea))
            let ex1 = e1.x, ey1 = e1.y
            let ex2 = e2.x, ey2 = e2.y
            let intx2 = ex1*ex1 + ex2*ex1 + ex2*ex2
            let inty2 = ey1*ey1 + ey2*ey1 + ey2*ey2
            I += (0.25 * k_inv3 * D) * (intx2 + inty2)
        }
        let massData = b2MassData()
        massData.mass = density * area
        mulEqual(&center, 1.0 / area)
        massData.center = add(center, s)
        massData.I = density * I
        massData.I += massData.mass * (b2Dot22(massData.center, massData.center) - b2Dot22(center, center))
        return massData
    }
    
    open var vertexCount: Int {
        return m_count
    }
    
    open func vertex(_ index : Int) -> b2Vec2 {
        return m_vertices.get(index)
    }
    
    open func validate() -> Bool {
        for i in 0 ..< m_vertices.count {
            let i1 = i
            let i2 = i < m_vertices.count - 1 ? i1 + 1 : 0
            let p = m_vertices.get(i1)
            let e = subtract(m_vertices.get(i2), p)
            for j in 0 ..< m_count {
                if j == i1 || j == i2 {
                    continue
                }
                let v = subtract(m_vertices.get(j), p)
                let c = b2Cross(e, v)
                if c < 0.0 {
                    return false
                }
            }
        }
        return true
    }
    
    open var vertices: b2Array<b2Vec2> {
        return m_vertices
    }
    
    open var count: Int {
        return m_count
    }
    
    var m_centroid = b2Vec2()
    var m_vertices = b2Array<b2Vec2>()
    var m_normals = b2Array<b2Vec2>()
    var m_count: Int {
        return m_vertices.count
    }
}
