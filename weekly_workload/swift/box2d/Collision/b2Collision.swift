public enum b2ContactFeatureType : Int {
    case vertex = 0
    case face = 1
}

public class b2ContactFeature {
    public var indexA = 0
    public var indexB = 0
    public var typeA: b2ContactFeatureType = b2ContactFeatureType.vertex
    public var typeB: b2ContactFeatureType = b2ContactFeatureType.vertex
    public init() {}
    public func setZero() {
        indexA = 0
        indexB = 0
        typeA = b2ContactFeatureType.vertex
        typeB = b2ContactFeatureType.vertex
    }
}

open class b2ManifoldPoint {
    open var localPoint = b2Vec2()
    open var normalImpulse: Double = 0.0
    open var tangentImpulse: Double = 0.0
    open var id = b2ContactFeature()
    
    public init(_ copyFrom: b2ManifoldPoint? = nil) {
        if (copyFrom != nil) {
            self.localPoint = copyFrom!.localPoint
            self.normalImpulse = copyFrom!.normalImpulse
            self.tangentImpulse = copyFrom!.tangentImpulse
            self.id = copyFrom!.id
        }
    }
}

public enum b2ManifoldType {
    case circles
    case faceA
    case faceB
}

open class b2Manifold {
    open var points = [b2ManifoldPoint]()
    open var localNormal = b2Vec2()
    open var localPoint = b2Vec2()
    open var type = b2ManifoldType.circles
    open var pointCount : Int { return points.count }
    public init( _ copyFrom: b2Manifold? = nil) {
        if (copyFrom != nil) {
            self.points = [b2ManifoldPoint]()
            for i in 0..<copyFrom!.points.count {
                self.points.append(b2ManifoldPoint(copyFrom!.points[i]))
            }
            self.localNormal = copyFrom!.localNormal
            self.localPoint = copyFrom!.localPoint
            self.type = copyFrom!.type
        }
    }
}

open class b2WorldManifold {
    open var normal = b2Vec2()
    open var points: [b2Vec2]
    open var separations:[Double]
    public init() {
        points = []
        separations = []
        for _ in (0..<b2_maxManifoldPoints) {
            points.append(b2Vec2(0, 0))
            separations.append(0)
        }
    }
    open func initialize(_ manifold: b2Manifold,
                         _ xfA: b2Transform, _ radiusA: Double,
                         _ xfB: b2Transform, _ radiusB: Double) {
        if (manifold.pointCount == 0) {
            return
        }
        switch manifold.type {
        case .circles:
            normal.set(1.0, 0.0)
            let pointA = b2MulT2(xfA, manifold.localPoint)
            let pointB = b2MulT2(xfB, manifold.points[0].localPoint)
            if b2DistanceSquared(pointA, pointB) > b2_epsilon * b2_epsilon {
                normal = subtract(pointB, pointA)
                normal.normalize()
            }
            let cA = add(pointA, multM(normal, radiusA))
            let cB = subtract(pointB, multM(normal, radiusB))
            points[0] = multM(add(cA, cB), 0.5)
            separations[0] = b2Dot22(subtract(cB, cA), normal)
            break
        case .faceA:
            normal = b2MulR2(xfA.q, manifold.localNormal)
            let planePoint = b2MulT2(xfA, manifold.localPoint)
            for i in 0 ..< manifold.pointCount {
                let clipPoint = b2MulT2(xfB, manifold.points[i].localPoint)
                let cA = add(clipPoint, multM(normal, (radiusA - b2Dot22(subtract(clipPoint, planePoint), normal))))
                let cB = subtract(clipPoint, multM(normal, radiusB))
                points[i] = multM(add(cA, cB), 0.5)
                separations[i] = b2Dot22(subtract(cB, cA), normal)
            }
            break
        case .faceB:
            normal = b2MulR2(xfB.q, manifold.localNormal)
            let planePoint = b2MulT2(xfB, manifold.localPoint)
            for i in 0 ..< manifold.pointCount {
                let clipPoint = b2MulT2(xfA, manifold.points[i].localPoint)
                let cB = add(clipPoint, multM(normal, (radiusB - b2Dot22(subtract(clipPoint, planePoint), normal))))
                let cA = subtract(clipPoint, multM(normal, radiusA))
                points[i] = multM(add(cA, cB), 0.5)
                separations[i] = b2Dot22(subtract(cA, cB), normal)
            }
            normal = minus(normal)
            break
        default:
            break
        }
    }
}

public class b2ClipVertex {
    public init() {}
    public var v = b2Vec2()
    public var id = b2ContactFeature()
}

public class b2RayCastInput {
    public init() {}
    public var p1 = b2Vec2()
    public var p2 = b2Vec2()
    public var maxFraction: Double = 0
}

public class b2RayCastOutput {
    public init() {}
    public var normal = b2Vec2()
    public var fraction: Double = 0
}

public class b2AABB {
    public var lowerBound = b2Vec2()
    public var upperBound = b2Vec2()
    public init(_ lowerBound: b2Vec2? = nil, _ upperBound: b2Vec2? = nil) {
        if (lowerBound != nil && upperBound != nil) {
            self.lowerBound = lowerBound!
            self.upperBound = upperBound!
        }
    }
    
    public var isValid: Bool {
        let d = subtract(upperBound, lowerBound)
        var valid = d.x >= 0.0 && d.y >= 0.0
        valid = valid && lowerBound.isValid() && upperBound.isValid()
        return valid
    }
    
    public var center: b2Vec2 {
        return multM(add(lowerBound, upperBound), 0.5)
    }
    
    public var extents: b2Vec2 {
        return multM((subtract(upperBound, lowerBound)), 0.5)
    }
    
    public var perimeter: Double {
        let wx = upperBound.x - lowerBound.x
        let wy = upperBound.y - lowerBound.y
        return 2.0 * (wx + wy)
    }
    
    public func combine(_ aabb1: b2AABB, _ aabb2: b2AABB) {
        lowerBound = b2Min(aabb1.lowerBound, aabb2.lowerBound)
        upperBound = b2Max(aabb1.upperBound, aabb2.upperBound)
    }
    
    public func contains(_ aabb: b2AABB) -> Bool {
        var result = true
        result = result && lowerBound.x <= aabb.lowerBound.x
        result = result && lowerBound.y <= aabb.lowerBound.y
        result = result && aabb.upperBound.x <= upperBound.x
        result = result && aabb.upperBound.y <= upperBound.y
        return result
    }
    
    public func rayCast(_ input: b2RayCastInput) -> b2RayCastOutput? {
        var tmin = b2_minFloat
        var tmax = b2_maxFloat
        let p = input.p1
        let d = subtract(input.p2, input.p1) 
        let absD = b2Abs2(d)
        var normal = b2Vec2()
        for i in 0 ..< 2 {
            if (absD.getSubscript(i) < b2_epsilon) {
                if p.getSubscript(i) < lowerBound.getSubscript(i) || upperBound.getSubscript(i) < p.getSubscript(i) {
                    return nil
                }
            } else {
                let inv_d = 1.0 / d.getSubscript(i)
                var t1 = (lowerBound.getSubscript(i) - p.getSubscript(i)) * inv_d
                var t2 = (upperBound.getSubscript(i) - p.getSubscript(i)) * inv_d
                var s: Double = -1.0
                if (t1 > t2) {
                    let tt = t1;
                    t1 = t2;
                    t2 = t1;
                    s = 1.0
                }
                if (t1 > tmin) {
                    normal.setZero()
                    normal.setSubscript(i, s)
                    tmin = t1
                }
                tmax = min(tmax, t2)
                if (tmin > tmax) {
                    return nil
                }
            }
        }
        if (tmin < 0.0 || input.maxFraction < tmin) {
            return nil
        }
        var output = b2RayCastOutput()
        output.fraction = tmin
        output.normal = normal
        return output
    }
}

public func b2ClipSegmentToLine(_ vIn: [b2ClipVertex], _ normal: b2Vec2, _ offset: Double, _ vertexIndexA: Int)
-> [b2ClipVertex] {
    var vOut = [b2ClipVertex]()
    let distance0 = b2Dot22(normal, vIn[0].v) - offset
    let distance1 = b2Dot22(normal, vIn[1].v) - offset
    if (distance0 <= 0.0) {
        vOut.append(vIn[0])
    }
    if (distance1 <= 0.0) {
        vOut.append(vIn[1])
    }
    if (distance0 * distance1 < 0.0) {
        let interp = distance0 / (distance0 - distance1)
        var v = b2ClipVertex()
        v.v = add(vIn[0].v, multM((subtract(vIn[1].v, vIn[0].v)), interp))
        v.id.indexA = vertexIndexA
        v.id.indexB = vIn[0].id.indexB
        v.id.typeA = b2ContactFeatureType.vertex
        v.id.typeB = b2ContactFeatureType.face
        vOut.append(v)
    }
    return vOut
}

public func b2TestOverlap(_ shapeA: b2Shape, _ indexA: Int,
                          _ shapeB: b2Shape, _ indexB: Int,
                          _ xfA: b2Transform, _ xfB: b2Transform) -> Bool {
    let input = b2DistanceInput()
    input.proxyA.set(shapeA, indexA)
    input.proxyB.set(shapeB, indexB)
    input.transformA = xfA
    input.transformB = xfB
    input.useRadii = true
    var cache = b2SimplexCache()
    cache.count = 0
    var output = b2DistanceOutput()
    b2DistanceM(&output, &cache, input)
    return output.distance < 10.0 * b2_epsilon
}

public func b2TestOverlap2(_ a: b2AABB, _ b: b2AABB) -> Bool {
    let d1 = subtract(b.lowerBound, a.upperBound)
    let d2 = subtract(a.lowerBound, b.upperBound)
    if d1.x > 0.0 || d1.y > 0.0 {
        return false
    }
    if d2.x > 0.0 || d2.y > 0.0 {
        return false
    }
    return true
}


