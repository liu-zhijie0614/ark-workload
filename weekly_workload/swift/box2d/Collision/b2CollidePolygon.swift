 

func b2FindMaxSeparation(_ poly1: b2PolygonShape, _ xf1: b2Transform,
                         _ poly2: b2PolygonShape, _ xf2: b2Transform) -> (edgeIndex: Int, maxSeparation: Double) {
    let count1 = poly1.m_count
    let count2 = poly2.m_count
    let n1s = poly1.m_normals
    let v1s = poly1.m_vertices
    let v2s = poly2.m_vertices
    let xf = b2MulT(xf2, xf1)
    var bestIndex = 0
    var maxSeparation = -b2_maxFloat
    for i in 0 ..< count1 {
        let n = b2MulR2(xf.q, n1s.get(i))
        let v1 = b2MulT2(xf, v1s.get(i))
        var si = b2_maxFloat
        for j in 0 ..< count2 {
            let sij = b2Dot22(n, subtract(v2s.get(j), v1))
            if sij < si {
                si = sij
            }
        }
        if si > maxSeparation {
            maxSeparation = si
            bestIndex = i
        }
    }
    return (bestIndex, maxSeparation)
}

func b2FindIncidentEdge(_ poly1: b2PolygonShape, _ xf1: b2Transform, _ edge1: Int,
                        _ poly2: b2PolygonShape, _ xf2: b2Transform) -> [b2ClipVertex] {
    let normals1 = poly1.m_normals
    let count2 = poly2.m_count
    let vertices2 = poly2.m_vertices
    let normals2 = poly2.m_normals
    let normal1 = b2MulTR2(xf2.q, b2MulR2(xf1.q, normals1.get(edge1)))
    var index = 0
    var minDot = b2_maxFloat
    for i in 0 ..< count2 {
        let dot = b2Dot22(normal1, normals2.get(i))
        if dot < minDot {
            minDot = dot
            index = i
        }
    }
    let i1 = index
    let i2 = i1 + 1 < count2 ? i1 + 1 : 0
    var c = [b2ClipVertex]()
    for _ in (0..<2) {
        c.append(b2ClipVertex())
    }
    c[0].v = b2MulT2(xf2, vertices2.get(i1))
    c[0].id.indexA = edge1
    c[0].id.indexB = i1
    c[0].id.typeA = b2ContactFeatureType.face
    c[0].id.typeB = b2ContactFeatureType.vertex
    c[1].v = b2MulT2(xf2, vertices2.get(i2))
    c[1].id.indexA = edge1
    c[1].id.indexB = i2
    c[1].id.typeA = b2ContactFeatureType.face
    c[1].id.typeB = b2ContactFeatureType.vertex
    return c
}

public func b2CollidePolygons(
    _ manifold: inout b2Manifold,
    _ polyA: b2PolygonShape, _ xfA: b2Transform,
    _ polyB: b2PolygonShape, _ xfB: b2Transform) {
        
    manifold.points.removeAll()
    let totalRadius = polyA.m_radius + polyB.m_radius
    let fm = b2FindMaxSeparation(polyA,xfA, polyB,xfB)
    let edgeA = fm.0
    let separationA = fm.1
    if (separationA > totalRadius) {
        return
    }
    let fmb = b2FindMaxSeparation(polyB, xfB, polyA, xfA)
    let edgeB = fmb.0
    let separationB = fmb.1
    if (separationB > totalRadius) {
        return
    }
    var poly1: b2PolygonShape
    var poly2: b2PolygonShape
    var xf1: b2Transform, xf2: b2Transform
    var edge1: Int
    var flip: Bool
    let k_tol: Double = 0.1 * b2_linearSlop
    if separationB > separationA + k_tol {
        poly1 = polyB
        poly2 = polyA
        xf1 = xfB
        xf2 = xfA
        edge1 = edgeB
        manifold.type = b2ManifoldType.faceB
        flip = true
    } else {
        poly1 = polyA
        poly2 = polyB
        xf1 = xfA
        xf2 = xfB
        edge1 = edgeA
        manifold.type = b2ManifoldType.faceA
        flip = false
    }
    let incidentEdge = b2FindIncidentEdge(poly1, xf1, edge1,  poly2, xf2)
    let count1 = poly1.m_count
    let vertices1 = poly1.m_vertices
    let iv1 = edge1
    let iv2 = edge1 + 1 < count1 ? edge1 + 1 : 0
        var v11 = vertices1.get(iv1)
        var v12 = vertices1.get(iv2)
    var localTangent = subtract(v12, v11)
    localTangent.normalize()
    let localNormal = b2Cross12(localTangent, 1.0)
    let planePoint = multM(add(v11, v12), 0.5)
    let tangent = b2MulR2(xf1.q, localTangent)
    let normal = b2Cross12(tangent, 1.0)
    v11 = b2MulT2(xf1, v11)
    v12 = b2MulT2(xf1, v12)
    let frontOffset = b2Dot22(normal, v11)
    let sideOffset1 = -b2Dot22(tangent, v11) + totalRadius
    let sideOffset2 = b2Dot22(tangent, v12) + totalRadius
    let clipPoints1 = b2ClipSegmentToLine(incidentEdge, minus(tangent), sideOffset1, iv1)
    if clipPoints1.count < 2 {
        return
    }
    let clipPoints2 = b2ClipSegmentToLine(clipPoints1, tangent, sideOffset2, iv2)
    if clipPoints2.count < 2 {
        return
    }
    manifold.localNormal = localNormal
    manifold.localPoint = planePoint
    var pointCount = 0
    for i in 0 ..< b2_maxManifoldPoints {
        let separation = b2Dot22(normal, clipPoints2[i].v) - frontOffset
        if separation <= totalRadius {
            let cp = b2ManifoldPoint()
            cp.localPoint = b2MulTT2(xf2, clipPoints2[i].v)
            cp.id = clipPoints2[i].id
            if flip {
                let cf = cp.id
                cp.id.indexA = cf.indexB
                cp.id.indexB = cf.indexA
                cp.id.typeA = cf.typeB
                cp.id.typeB = cf.typeA
            }
            manifold.points.append(cp)
            pointCount += 1
        }
    }
    return
}
