 

public func b2CollideCircles(
    _ manifold: inout b2Manifold,
    _ circleA: b2CircleShape, _ xfA: b2Transform,
    _ circleB: b2CircleShape, _ xfB: b2Transform) {
        
    manifold.points.removeAll()
    let pA = b2MulT2(xfA, circleA.m_p)
    let pB = b2MulT2(xfB, circleB.m_p)
    let d = subtract(pB, pA)
    let distSqr = b2Dot22(d, d)
    let rA = circleA.m_radius
    let rB = circleB.m_radius
    let radius = rA + rB
    if distSqr > radius * radius {
        return
    }
    manifold.type = b2ManifoldType.circles
    manifold.localPoint = circleA.m_p
    manifold.localNormal.setZero()
    let cp = b2ManifoldPoint()
    cp.localPoint = circleB.m_p
    cp.id.setZero()
    manifold.points.append(cp)
}

public func b2CollidePolygonAndCircle(
    _ manifold: inout b2Manifold,
    _ polygonA: b2PolygonShape, _ xfA: b2Transform,
    _ circleB: b2CircleShape, _ xfB: b2Transform) {
        
    manifold.points.removeAll()
    let c = b2MulT2(xfB, circleB.m_p)
    let cLocal = b2MulTT2(xfA, c)
    var normalIndex = 0
    var separation = -b2_maxFloat
    let radius = polygonA.m_radius + circleB.m_radius
    let vertexCount = polygonA.m_count
    let vertices = polygonA.m_vertices
    let normals = polygonA.m_normals
    for i in 0 ..< vertexCount {
        let n = subtract(cLocal, vertices.get(i))
        let s = b2Dot22(normals.get(i), n)
        if s > radius {
            return
        }
        if s > separation {
            separation = s
            normalIndex = i
        }
    }
    let vertIndex1 = normalIndex
    let vertIndex2 = vertIndex1 + 1 < vertexCount ? vertIndex1 + 1 : 0
        let v1 = vertices.get(vertIndex1)
        let v2 = vertices.get(vertIndex2)
    if (separation < b2_epsilon) {
        manifold.type = b2ManifoldType.faceA
        manifold.localNormal = normals.get(normalIndex)
        manifold.localPoint = multM((add(v1, v2)), 0.5)
        let cp = b2ManifoldPoint()
        cp.localPoint = circleB.m_p
        cp.id.setZero()
        manifold.points.append(cp)
        return
    }
    let u1 = b2Dot22(subtract(cLocal, v1), subtract(v2, v1))
    let u2 = b2Dot22(subtract(cLocal, v2), subtract(v1, v2))
    if (u1 <= 0.0) {
        if (b2DistanceSquared(cLocal, v1) > radius * radius) {
            return
        }
        manifold.type = b2ManifoldType.faceA
        manifold.localNormal = subtract(cLocal, v1)
        manifold.localNormal.normalize()
        manifold.localPoint = v1
        let cp = b2ManifoldPoint()
        cp.localPoint = circleB.m_p
        cp.id.setZero()
        manifold.points.append(cp)
    } else if (u2 <= 0.0) {
        if b2DistanceSquared(cLocal, v2) > radius * radius {
            return
        }
        manifold.type = b2ManifoldType.faceA
        manifold.localNormal = subtract(cLocal, v2)
        manifold.localNormal.normalize()
        manifold.localPoint = v2
        let cp = b2ManifoldPoint()
        cp.localPoint = circleB.m_p
        cp.id.setZero()
        manifold.points.append(cp)
    } else {
        let faceCenter = multM(add(v1, v2), 0.5)
        let separation = b2Dot22(subtract(cLocal, faceCenter), normals.get(vertIndex1))
        if separation > radius {
            return
        }
        manifold.type = b2ManifoldType.faceA
        manifold.localNormal = normals.get(vertIndex1)
        manifold.localPoint = faceCenter
        let cp = b2ManifoldPoint()
        cp.localPoint = circleB.m_p
        cp.id.setZero()
        manifold.points.append(cp)
    }
    return
}

