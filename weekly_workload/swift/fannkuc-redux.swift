import Glibc

class Timer {
    private let CLOCK_REALTIME = 0
    private var start_timespec = timespec()
    private var end_timespec = timespec()
    private var time_spec = timespec()
    func start() {
        clock_gettime(Int32(CLOCK_REALTIME),&start_timespec)
    }
    
    func stop() -> Double {
        clock_gettime(Int32(CLOCK_REALTIME),&end_timespec)
        let start_time = Double(start_timespec.tv_sec * 1_000_000 + start_timespec.tv_nsec / 1_000)
        let end_time = Double(end_timespec.tv_sec * 1_000_000 + end_timespec.tv_nsec / 1_000)
        let time = end_time - start_time
        return time / 1_000
    }
    func getTime() -> Double {
        clock_gettime(Int32(CLOCK_REALTIME),&time_spec)
        return Double(time_spec.tv_sec * 1_000_000 + time_spec.tv_nsec / 1_000)
    }
}

let timer = Timer()

func fannkuch(_ n: Int) -> Int {
   var perm = Array(repeating: 0, count: n),
      count = Array(repeating: 0, count: n)

   var perm1 = Array(repeating: 0, count: n)
   for j in 0...n-1 { perm1[j] = j }

   var f = 0, i = 0, k = 0, r = 0, flips = 0, nperm = 0, checksum = 0

   r = n
   while r > 0 {
      i = 0
      while r != 1 { count[r-1] = r; r -= 1 }
      while i < n { perm[i] = perm1[i]; i += 1 }

      // Count flips and update max  and checksum
      f = 0
      k = perm[0]
      while k != 0 {
         i = 0
         while 2*i < k {
            let t = perm[i]; perm[i] = perm[k-i]; perm[k-i] = t
            i += 1
         }
         k = perm[0]
         f += 1
      }
      if f>flips { flips = f }
      if (nperm&0x1)==0 { checksum += f } else { checksum -= f }

      // Use incremental change to generate another permutation
      var go = true
      while go {
         if r == n {
            return flips
         }
         let p0 = perm1[0]
         i = 0
         while i < r {
            let j = i+1
            perm1[i] = perm1[j]
            i = j
         }
         perm1[r] = p0

         count[r] -= 1
         if count[r] > 0 { go = false } else { r += 1 }
      }
      nperm += 1
   }
   return flips
}

func runFannkuc_Redux()->Int{
   let n: Int = 8
   timer.start()
   fannkuch(n)
   let time = timer.stop()
   print("Array Access - RunFannkucRedux:\t " + String(time) + " \tms")
   return Int(time)
}
_ = runFannkuc_Redux()