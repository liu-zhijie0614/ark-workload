// Copyright (c) 2004 by Arthur Langereis (arthur_ext at domain xfinitegames, tld com)

import Glibc

let inDebug = false
func log(_ str: String) {
    if (inDebug) {
        print(str)
    }
}

func currentTimestamp13() -> Double {
    return Timer().getTime()/1000
}

var result = 0;

// 1 op = 2 assigns, 16 compare/branches, 8 ANDs, (0-8) ADDs, 8 SHLs
// O(n)
func bitsinbyte(_ b: Int) -> Int {
    var m = 1, c = 0;
    while (m < 0x100) {
        if ((b & m) != 0) {
            c += 1
        }
        m <<= 1
    }
    return c
}

func TimeFunc() -> Int {
    var sum = 0;
    for x in 0..<350 {
        for y in 0..<256 {
            sum += bitsinbyte(y)
        }
    }
    return sum
}

/*
 * @State
 * @Tags Jetstream2
 */
class Benchmark {
    /*
     * @Benchmark
     */
    func run() {
      result = TimeFunc()
      let expected = 358400
      if result != expected {
	fatalError("ERROR: bad result: expected \(expected) but got \(result)")
      }
   }
}

class Timer {
   private let CLOCK_REALTIME = 0
   private var time_spec = timespec()

   func getTime() -> Double {
       clock_gettime(Int32(CLOCK_REALTIME),&time_spec)
       return Double(time_spec.tv_sec * 1_000_000 + time_spec.tv_nsec / 1_000)
   }
}

let startTime = currentTimestamp13()
let benchmark = Benchmark();
for i in 0..<80 {
    let startTimeInLoop = currentTimestamp13()
    benchmark.run()
    let endTimeInLoop = currentTimestamp13()
    //log("bitops_bits_in_byte: ms = \((endTimeInLoop - startTimeInLoop)) i= \(i)")
}
let endTime = currentTimestamp13()
print("bitops-bits-in-byte: ms = \(endTime - startTime)")



