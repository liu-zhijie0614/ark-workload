import Glibc

class Object2 {
    var V: [Double] = Array()
}

class Object1 {
    var LoopCount: Int = 0
    var LoopMax: Int = 50
    var TimeMax: Int = 0
    var TimeAvg: Int = 0
    var TimeMin: Int = 0
    var TimeTemp: Int = 0
    var TimeTotal: Int = 0
    var Init: Bool = false
}

class CreateP {
    var V = [Double]()
    init(_ X: Double, _ Y: Double, _ Z: Double) {
        V = [X, Y, Z, 1]
    }
}

class Q {
    var VArray: [CreateP] = Array(repeating: CreateP(0, 0, 0), count: 9)
    var Edge: [[Double]] = Array()
    var Normal: [[Double]] = Array(repeating: [], count: 6)
    var Line = [Bool]()
    var NumPx: Int = 0
    var LastPx: Int = 0
    
}

class Cube {
    
    func DrawLine(_ from: CreateP, _ to: CreateP) {
        let x1 = from.V[0]
        let x2 = to.V[0]
        let y1 = from.V[1]
        let y2 = to.V[1]
        let dx = abs(x2 - x1)
        let dy = abs(y2 - y1)
        var x = x1
        var y = y1
        var IncX1: Double, IncY1: Double
        var IncX2: Double, IncY2: Double
        var Den: Double
        var Num: Double
        var NumAdd: Double
        var NumPix: Int
        
        if x2 >= x1 {
            IncX1 = 1
            IncX2 = 1
        } else {
            IncX1 = -1
            IncX2 = -1
        }
        
        if y2 >= y1 {
            IncY1 = 1
            IncY2 = 1
        } else {
            IncY1 = -1
            IncY2 = -1
        }
        
        if dx >= dy {
            IncX1 = 0
            IncY2 = 0
            Den = dx
            Num = dx / 2
            NumAdd = dy
            NumPix = Int(dx)
        } else {
            IncX2 = 0
            IncY1 = 0
            Den = dy
            Num = dy / 2
            NumAdd = dx
            NumPix = Int(dy)
        }
        NumPix = q.LastPx + NumPix
        
        for _ in q.LastPx..<NumPix {
            Num += NumAdd
            if Num >= Den {
                x += IncX1
                y += IncY1
            }
            x += IncX2
            y += IncY2
        }
        q.LastPx = NumPix
    }
    
    func CalcCross(_ V0: [Double], _ V1: [Double]) -> [Double] {
        var Cross: [Double] = [0, 0, 0, 0]
        Cross[0] = V0[1]*V1[2] - V0[2]*V1[1]
        Cross[1] = V0[2]*V1[0] - V0[0]*V1[2]
        Cross[2] = V0[0]*V1[1] - V0[1]*V1[0]
        return Cross
    }
    
    func CalcNormal(_ V0: [Double], _ V1: [Double], _ V2: [Double]) -> [Double] {
        var A: [Double] = [0, 0, 0, 0]
        var B: [Double] = [0, 0, 0, 0]
        for i in 0..<3 {
            A[i] = V0[i] - V1[i]
            B[i] = V2[i] - V1[i]
        }
        
        A = CalcCross(A, B)
        let x = A[0]*A[0] + A[1]*A[1] + A[2]*A[2]
        let Length = sqrt(Double(x))
        for i in 0..<3 {
            A[i] = A[i] / Double(Length)
        }
        A[3] = 1
        return A
    }
    
    func MMulti(_ M1: [[Double]], _ M2: [[Double]]) -> [[Double]] {
        var M: [[Double]] = [[0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0]]
        for i in 0..<4 {
            for j in 0..<4 {
                M[i][j] = M1[i][0] * M2[0][j] + M1[i][1] * M2[1][j] + M1[i][2] * M2[2][j] + M1[i][3] * M2[3][j]
            }
        }
        return M
    }
    
    func VMulti(_ M: [[Double]], _ V: [Double]) -> [Double] {
        var Vect: [Double] = [0, 0, 0, 0]
        for i in 0..<4 {
            Vect[i] = M[i][0] * V[0] + M[i][1] * V[1] + M[i][2] * V[2] + M[i][3] * V[3]
        }
        return Vect
    }
    
    func VMulti2(_ M: [[Double]], _ V: [Double]) -> [Double] {
        var Vect: [Double] = [0, 0, 0, 0]
        for i in 0..<4 {
            Vect[i] = M[i][0] * V[0] + M[i][1] * V[1] + M[i][2] * V[2]
        }
        return Vect
    }
    
    func Translate(_ M: [[Double]], _ Dx: Double, _ Dy: Double, _ Dz: Double) -> [[Double]] {
        let T = [
            [1,0,0,Dx],
            [0,1,0,Dy],
            [0,0,1,Dz],
            [0,0,0,1]
            ]
        return MMulti(T, M)
    }
    
    func RotateX(_ M:[[Double]], _ Phi: Double) -> [[Double]] {
        var a = Double(Phi)
        a *= .pi / 180
        let Cos = cos(a)
        let Sin = sin(a)
        let R = [
            [1,0,0,0],
            [0,Cos,-Sin,0],
            [0,Sin,Cos,0],
            [0,0,0,1]
        ]
        return MMulti(R, M)
    }
    
    func RotateY(_ M:[[Double]], _ Phi: Double) -> [[Double]] {
        var a = Phi
        a *= .pi / 180
        let Cos = cos(a)
        let Sin = sin(a)
        let R = [
            [Cos,0,Sin,0],
            [0,1,0,0],
            [-Sin,0,Cos,0],
            [0,0,0,1]
        ]
        return MMulti(R, M)
    }
    
    func RotateZ(_ M:[[Double]], _ Phi: Double) -> [[Double]] {
        var a = Phi
        a *= .pi / 180
        let Cos = cos(a)
        let Sin = sin(a)
        let R = [
            [Cos,-Sin,0,0],
            [Sin,Cos,0,0],
            [0,0,1,0],
            [0,0,0,1]
        ]
        return MMulti(R, M)
    }
    
    func DrawQube() {
        var CurN = [[Double]]()
        q.LastPx = 0
        var i = 5
        while i >= 0 {
            CurN.append(VMulti2(MQube, q.Normal[i]))
            i -= 1
        }
        
        if CurN[0][2] < 0 {
            if !q.Line[0] {
                DrawLine(q.VArray[0], q.VArray[1])
                q.Line[0] = true
            }
            if !q.Line[1] {
                DrawLine(q.VArray[1], q.VArray[2])
                q.Line[1] = true
            }
            if !q.Line[2] {
                DrawLine(q.VArray[2], q.VArray[3])
                q.Line[2] = true
            }
            if !q.Line[3] {
                DrawLine(q.VArray[3], q.VArray[0])
                q.Line[3] = true
            }
        }
        if CurN[1][2] < 0 {
            if !q.Line[2] {
                DrawLine(q.VArray[3], q.VArray[2])
                q.Line[2] = true
            }
            if !q.Line[9] {
                DrawLine(q.VArray[2], q.VArray[6])
                q.Line[9] = true
            }
            if !q.Line[6] {
                DrawLine(q.VArray[6], q.VArray[7])
                q.Line[6] = true
            }
            if !q.Line[10] {
                DrawLine(q.VArray[7], q.VArray[3])
                q.Line[10] = true
            }
        }
        if CurN[2][2] < 0 {
            if !q.Line[4] {
                DrawLine(q.VArray[4], q.VArray[5])
                q.Line[4] = true
            }
            if !q.Line[5] {
                DrawLine(q.VArray[5], q.VArray[6])
                q.Line[5] = true
            }
            if !q.Line[6] {
                DrawLine(q.VArray[6], q.VArray[7])
                q.Line[6] = true
            }
            if !q.Line[7] {
                DrawLine(q.VArray[7], q.VArray[4])
                q.Line[7] = true
            }
        }
        if CurN[3][2] < 0 {
            if !q.Line[4] {
                DrawLine(q.VArray[4], q.VArray[5])
                q.Line[4] = true
            }
            if !q.Line[8] {
                DrawLine(q.VArray[5], q.VArray[1])
                q.Line[8] = true
            }
            if !q.Line[0] {
                DrawLine(q.VArray[1], q.VArray[0])
                q.Line[0] = true
            }
            if !q.Line[11] {
                DrawLine(q.VArray[0], q.VArray[4])
                q.Line[11] = true
            }
        }
        if CurN[4][2] < 0 {
            if !q.Line[11] {
                DrawLine(q.VArray[4], q.VArray[0])
                q.Line[11] = true
            }
            if !q.Line[3] {
                DrawLine(q.VArray[0], q.VArray[3])
                q.Line[3] = true
            }
            if !q.Line[10] {
                DrawLine(q.VArray[3], q.VArray[7])
                q.Line[10] = true
            }
            if !q.Line[7] {
                DrawLine(q.VArray[7], q.VArray[4])
                q.Line[7] = true
            }
        }
        if CurN[5][2] < 0 {
            if !q.Line[8] {
                DrawLine(q.VArray[1], q.VArray[5])
                q.Line[8] = true
            }
            if !q.Line[5] {
                DrawLine(q.VArray[5], q.VArray[6])
                q.Line[5] = true
            }
            if !q.Line[9] {
                DrawLine(q.VArray[6], q.VArray[2])
                q.Line[9] = true
            }
            if !q.Line[1] {
                DrawLine(q.VArray[2], q.VArray[1])
                q.Line[1] = true
            }
        }
        q.Line = [false,false,false,false,false,false,false,false,false,false,false,false]
        q.LastPx = 0
    }
    
    func Loop() {
        if Testing.LoopCount > Testing.LoopMax {
            return
        }
        var TestingStr = String(Testing.LoopCount)
        while TestingStr.count < 3 {
            TestingStr = "0\(TestingStr)"
        }
        MTrans = Translate(I, -q.VArray[8].V[0], -q.VArray[8].V[1], -q.VArray[8].V[2])
        MTrans = RotateX(MTrans, 1)
        MTrans = RotateY(MTrans, 3)
        MTrans = RotateZ(MTrans, 5)
        MTrans = Translate(MTrans, q.VArray[8].V[0], q.VArray[8].V[1], q.VArray[8].V[2])
        MQube = MMulti(MTrans, MQube)
        var i = 8
        while i >= 0 {
            q.VArray[i].V = VMulti(MTrans, q.VArray[i].V)
            i -= 1
        }
        DrawQube()
        Testing.LoopCount += 1
        Loop()
    }
    
    var q = Q()
    // transformation matrix
    var MTrans: [[Double]] = Array()
    // position information of qube
    var MQube: [[Double]] = Array()
    // entity matrix
    var I: [[Double]] = Array()
    var Origin = Object2()
    var Testing = Object1()
    
    var validation: Dictionary<Int, Double> = [
        20: 2889.0000000000045,
        40: 2889.0000000000055,
        80: 2889.000000000005,
        160: 2889.0000000000055
    ]
        
    func creat(_ CubeSize: Double) {
        // init/reset vars
        Origin.V = [150,150,20,1]
        Testing.LoopCount = 0
        Testing.LoopMax = 50
        Testing.TimeMax = 0
        Testing.TimeAvg = 0
        Testing.TimeMin = 0
        Testing.TimeTemp = 0
        Testing.TimeTotal = 0
        Testing.Init = false
        
        // transformation matrix
        MTrans = [
            [1,0,0,0],
            [0,1,0,0],
            [0,0,1,0],
            [0,0,0,1]
        ]
        
        // position information of qube
        MQube = [
            [1,0,0,0],
            [0,1,0,0],
            [0,0,1,0],
            [0,0,0,1]
        ]
        
        // entity matrix
        I = [
            [1,0,0,0],
            [0,1,0,0],
            [0,0,1,0],
            [0,0,0,1]
        ]
        
        // create qube
        q.VArray[0] = CreateP(-CubeSize, -CubeSize, CubeSize)
        q.VArray[1] = CreateP(-CubeSize, CubeSize, CubeSize)
        q.VArray[2] = CreateP(CubeSize, CubeSize, CubeSize)
        q.VArray[3] = CreateP(CubeSize, -CubeSize, CubeSize)
        q.VArray[4] = CreateP(-CubeSize, -CubeSize, -CubeSize)
        q.VArray[5] = CreateP(-CubeSize, CubeSize, -CubeSize)
        q.VArray[6] = CreateP(CubeSize, CubeSize, -CubeSize)
        q.VArray[7] = CreateP(CubeSize, -CubeSize, -CubeSize)
        q.VArray[8] = CreateP(0, 0, 0)
        
        // anti-clockwise edge check
        q.Edge = [[0,1,2],[3,2,6],[7,6,5],[4,5,1],[4,0,3],[1,5,6]]
        for i in 0..<q.Edge.count {
            q.Normal[i] = CalcNormal(q.VArray[Int(q.Edge[i][0])].V,
                                     q.VArray[Int(q.Edge[i][1])].V,
                                     q.VArray[Int(q.Edge[i][2])].V)
        }
        q.Line = [false,false,false,false,false,false,false,false,false,false,false,false]
        q.NumPx = Int(9 * 2 * CubeSize)
        for _ in 0..<q.NumPx {
            _ = CreateP(0, 0, 0)
        }
        
        MTrans = Translate(MTrans, Origin.V[0], Origin.V[1], Origin.V[2])
        MQube = MMulti(MTrans, MQube)
        
        for i in 0..<9 {
            q.VArray[i].V = VMulti(MTrans, q.VArray[i].V)
        }

        DrawQube()
        Testing.Init = true
        Loop()

        //DebugLog("CubeSize is \(Int(CubeSize)), MTrans value is \(MTrans[0][0]) \(MTrans[0][1]) \(MTrans[0][2]) \(MTrans[0][3])")
        //DebugLog("CubeSize is \(Int(CubeSize)), MQube value is \(MQube[0][0]) \(MQube[0][1]) \(MQube[0][2]) \(MQube[0][3])")
        
        // Perform a simple sum-based verification.
        var sum: Double = 0
        for i in 0..<q.VArray.count {
            let vector = q.VArray[i].V
            for j in 0..<vector.count {
                sum += vector[j]
            }
        }
        if sum != validation[Int(CubeSize)] {
            //DebugLog("Error: bad vector sum for CubeSize =  + \(CubeSize) + ; expected  + \(validation[Int(CubeSize)]!) +  but got  + \(sum)")
        }
    }
    
    func runTest() {
        var i = 20
        while i <= 160 {
            creat(Double(i))
            i *= 2
        }
    }
    
    let isDebug : Bool = false
    func DebugLog(_ msg : Any , file: String = #file, line: Int = #line, fn: String = #function){
        if isDebug {
            let prefix = "\(msg)"
            print(prefix)
        }
    }
}

class Timer {
    private let CLOCK_REALTIME = 0
    private var time_spec = timespec()

    func getTime() -> Double {
        clock_gettime(Int32(CLOCK_REALTIME),&time_spec)
        return Double(time_spec.tv_sec * 1_000_000 + time_spec.tv_nsec / 1_000)
    }
}
 /*
  * @State
  * @Tags Jetstream2
 */
class Benchmark {
 /*
  * @Benchmark
 */
  func run() {
    let start = Timer().getTime()
    let cube = Cube()
    for _ in 0..<8 {
        cube.runTest()
    }
    let end = Timer().getTime()
    let duration = (end - start) / 1000
    print("3d-cube: ms = \(duration)")
  }
}

Benchmark().run()

