import Glibc

    /* Intmm, Mm */
let rowsize = 40

    /* global */
var seed: Int = 0  /* converted to long for 16 bit WR*/

var rma: [[Double]] = [[Double]](repeating: [Double](repeating: 0.0, count: rowsize+1), count: rowsize+1),
    rmb: [[Double]] = [[Double]](repeating: [Double](repeating: 0.0, count: rowsize+1), count: rowsize+1),
    rmr: [[Double]] = [[Double]](repeating: [Double](repeating: 0.0, count: rowsize+1), count: rowsize+1)

func Initrand() -> Void {
    seed = 74755   /* constant to long WR*/
}

func Rand() -> Int {
    seed = (seed * 1309 + 13849) & 65535    /* constants to long WR*/
    return seed                             /* typecast back to int WR*/
}

    /* Multiplies two real matrices. */

func rInitmatrix(_ m: inout [[Double]]) {
    var temp: Int
    for i in 1 ... rowsize {
        for j in 1 ... rowsize {
            temp = Rand()
            m[i][j] = (Double(temp) - floor(Double(temp)/120)*120 - 60)/3
        }
    }
}

func rInnerproduct(_ result: inout [[Double]], _ a: [[Double]], _ b: [[Double]], _ row:Int, _ column: Int) {
    /* computes the inner product of A[row,*] and B[*,column] */
    result[row][column] = 0.0
    for i in 1 ... rowsize {
        result[row][column] = result[row][column] + a[row][i] * b[i][column]
    }
}

func Mm(_ run: Int) -> Void {
    Initrand()
    //*`@Setup`
    rInitmatrix(&rma)
    //*`@Setup`
    rInitmatrix(&rmb)
    for i in 1 ... rowsize {
        for j in 1 ... rowsize {
            rInnerproduct(&rmr,rma,rmb,i,j)
        }
    }
    if run < rowsize {
        //printLog("\(rmr[run + 1][run + 1])")
    }
}
///@Generator
class Benchmark {
    ///@Benchmark
    func runIteration() {
        for i in 0 ..< 5000 {
            Mm(i)
        }
    }
}





//以下是测试打印日志相关代码
let isDebug = false
func printLog(_ str: String) -> Void {
    if isDebug {
        print(str)
    }
}
class Timer {
   private let CLOCK_REALTIME = 0
   private var time_spec = timespec()

   func getTime() -> Double {
       clock_gettime(Int32(CLOCK_REALTIME),&time_spec)
       return Double(time_spec.tv_sec * 1_000_000 + time_spec.tv_nsec / 1_000)
   }
}

func startRun(_ times: Int, ben:Benchmark = Benchmark(), time: Timer = Timer()) {
    let start = time.getTime()
    for _ in 0 ..< times {
        ben.runIteration()
    }
    let end = time.getTime()
    print("float-mm: ms = \((end - start) / 1000)")
}
startRun(5)
