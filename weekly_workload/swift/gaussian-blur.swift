//
//  gaussian-blur.swift
//  JetStream2-Swift
//
//  Created by Henry Li on 2023/11/1.
//

import Glibc



class GaussianBlur {
    // Configuration.
    var a0: Double = 0
    var a1: Double = 0
    var a2: Double = 0
    var a3: Double = 0
    var b1: Double = 0
    var b2: Double = 0
    var left_corner: Double = 0
    var right_corner: Double = 0

    func gaussCoef(sigma: Int) -> Array<Double>{
        //debugLog("gaussCoef=====sigma", object: sigma)
        var sigma = Double(sigma)
        if sigma < 0.5 {
            sigma = 0.5
        }
        
        let a = exp(0.726 * 0.726) / sigma,
        g1 = exp(-a),
        g2 = exp(-2 * a),
        k = (1 - g1) * (1 - g1) / (1 + 2 * a * g1 - g2)
        a0 = k
        a1 = k * (a - 1) * g1
        a2 = k * (a + 1) * g1
        a3 = -k * g2
        b1 = 2 * g1
        b2 = -g2
        left_corner = (a0 + a1) / (1 - b1 - b2)
        right_corner = (a2 + a3) / (1 - b1 - b2)
        //debugLog("gaussCoef=====([a0, a1, a2, a3, b1, b2, left_corner, right_corner])", object: ([a0, a1, a2, a3, b1, b2, left_corner, right_corner]))
        return [a0, a1, a2, a3, b1, b2, left_corner, right_corner ]
    }
    
    func convolveRGBA(src:Array<UInt32>, out:inout Array<UInt32>, line:inout Array<Double>, coeff: Array<Double>, width: Int, height: Int) {
        // takes src image and writes the blurred and transposed result into out
        //debugLog("convolveRGBA=====src length",object: src.count)
        //debugLog("convolveRGBA=====out length",object: out.count)
        //debugLog("convolveRGBA=====line length",object: line.count)
        //debugLog("convolveRGBA=====coeff",object: coeff.count)
        //debugLog("convolveRGBA=====width",object: width)
        //debugLog("convolveRGBA=====height",object: height)

        var rgb: Int
        var pre_src_r: Double, pre_src_g: Double, pre_src_b: Double, pre_src_a: Double
        var curr_src_r: Double, curr_src_g: Double, curr_src_b: Double, curr_src_a: Double
        var curr_out_r: Double, curr_out_g: Double, curr_out_b: Double, curr_out_a: Double
        var pre_out_r: Double, pre_out_g: Double, pre_out_b: Double, pre_out_a: Double
        var pre_pre_out_r: Double, pre_pre_out_g: Double, pre_pre_out_b: Double, pre_pre_out_a: Double

        var src_index: Int, out_index: Int, line_index:Int
        var coeff_a0:Double, coeff_a1: Double, coeff_b1: Double, coeff_b2:Double

        for i in 0..<height {
            src_index = i * width
            out_index = i
            line_index = 0

            // left to right;
            rgb = Int(src[src_index])
            pre_src_r = Double(rgb & 0xff)
            pre_src_g = Double((rgb >> 8) & 0xff)
            pre_src_b = Double((rgb >> 16) & 0xff)
            pre_src_a = Double((rgb >> 24) & 0xff)

            pre_pre_out_r = Double(pre_src_r) * coeff[6]
            pre_pre_out_g = Double(pre_src_g) * coeff[6]
            pre_pre_out_b = Double(pre_src_b) * coeff[6]
            pre_pre_out_a = Double(pre_src_a) * coeff[6]

            pre_out_r = pre_pre_out_r
            pre_out_g = pre_pre_out_g
            pre_out_b = pre_pre_out_b
            pre_out_a = pre_pre_out_a

            coeff_a0 = coeff[0]
            coeff_a1 = coeff[1]
            coeff_b1 = coeff[4]
            coeff_b2 = coeff[5]

            for _ in 0..<width {
                rgb = Int(src[src_index])
                curr_src_r = Double(rgb & 0xff)
                curr_src_g = Double((rgb >> 8) & 0xff)
                curr_src_b = Double((rgb >> 16) & 0xff)
                curr_src_a = Double((rgb >> 24) & 0xff)

                curr_out_r = curr_src_r * coeff_a0 + pre_src_r * coeff_a1 + pre_out_r * coeff_b1 + pre_pre_out_r * coeff_b2
                curr_out_g = curr_src_g * coeff_a0 + pre_src_g * coeff_a1 + pre_out_g * coeff_b1 + pre_pre_out_g * coeff_b2
                curr_out_b = curr_src_b * coeff_a0 + pre_src_b * coeff_a1 + pre_out_b * coeff_b1 + pre_pre_out_b * coeff_b2
                curr_out_a = curr_src_a * coeff_a0 + pre_src_a * coeff_a1 + pre_out_a * coeff_b1 + pre_pre_out_a * coeff_b2

                pre_pre_out_r = pre_out_r
                pre_pre_out_g = pre_out_g
                pre_pre_out_b = pre_out_b
                pre_pre_out_a = pre_out_a

                pre_out_r = curr_out_r
                pre_out_g = curr_out_g
                pre_out_b = curr_out_b
                pre_out_a = curr_out_a

                pre_src_r = curr_src_r
                pre_src_g = curr_src_g
                pre_src_b = curr_src_b
                pre_src_a = curr_src_a

                line[line_index] = pre_out_r
                line[line_index + 1] = pre_out_g
                line[line_index + 2] = pre_out_b
                line[line_index + 3] = pre_out_a
                line_index += 4
                src_index += 1
            }
            src_index -= 1
            line_index -= 4
            out_index += height * (width - 1)

            //right to left
            rgb = Int(src[src_index])

            pre_src_r = Double(rgb & 0xff)
            pre_src_g = Double((rgb >> 8) & 0xff)
            pre_src_b = Double((rgb >> 16) & 0xff)
            pre_src_a = Double((rgb >> 24) & 0xff)

            pre_pre_out_r = pre_src_r * coeff[7]
            pre_pre_out_g = pre_src_g * coeff[7]
            pre_pre_out_b = pre_src_b * coeff[7]
            pre_pre_out_a = pre_src_a * coeff[7]

            pre_out_r = pre_pre_out_r
            pre_out_g = pre_pre_out_g
            pre_out_b = pre_pre_out_b
            pre_out_a = pre_pre_out_a;

            curr_src_r = pre_src_r
            curr_src_g = pre_src_g
            curr_src_b = pre_src_b
            curr_src_a = pre_src_a

            coeff_a0 = coeff[2]
            coeff_a1 = coeff[3]

            for _ in 0..<width {
                curr_out_r = curr_src_r * coeff_a0 + pre_src_r * coeff_a1 + pre_out_r * coeff_b1 + pre_pre_out_r * coeff_b2
                curr_out_g = curr_src_g * coeff_a0 + pre_src_g * coeff_a1 + pre_out_g * coeff_b1 + pre_pre_out_g * coeff_b2
                curr_out_b = curr_src_b * coeff_a0 + pre_src_b * coeff_a1 + pre_out_b * coeff_b1 + pre_pre_out_b * coeff_b2
                curr_out_a = curr_src_a * coeff_a0 + pre_src_a * coeff_a1 + pre_out_a * coeff_b1 + pre_pre_out_a * coeff_b2

                pre_pre_out_r = pre_out_r
                pre_pre_out_g = pre_out_g
                pre_pre_out_b = pre_out_b
                pre_pre_out_a = pre_out_a

                pre_out_r = curr_out_r
                pre_out_g = curr_out_g
                pre_out_b = curr_out_b
                pre_out_a = curr_out_a

                pre_src_r = curr_src_r
                pre_src_g = curr_src_g
                pre_src_b = curr_src_b
                pre_src_a = curr_src_a

                rgb = Int(src[src_index])
                curr_src_r = Double(rgb & 0xff)
                curr_src_g = Double((rgb >> 8) & 0xff)
                curr_src_b = Double((rgb >> 16) & 0xff)
                curr_src_a = Double((rgb >> 24) & 0xff)

                rgb = (Int((line[line_index] + pre_out_r)) << 0) + 
		       (Int(line[line_index + 1] + pre_out_g) << 8) + 
		       (Int(line[line_index + 2] + pre_out_b) << 16) + 
		       (Int(line[line_index + 3] + pre_out_a) << 24)

                out[out_index] = UInt32(rgb)

                src_index -= 1
                line_index -= 4
                out_index -= height
            }
        }        
        //debugLog("out length: ", object: out.count)
    }
    
    func blurRGBA(src:Array<UInt32>, width:Int, height:Int, radius:Int) {
        //debugLog("convolveRGBA=====src length",object: src.count)
        //debugLog("convolveRGBA=====width",object: width)
        //debugLog("convolveRGBA=====height",object: height)
        //debugLog("convolveRGBA=====radius",object: radius)
        // Quick exit on zero radius    
        if ((radius == 0)) { return }
      
        // Unify input data type, to keep convolver calls isomorphic
        var src32 = src
        var out = [UInt32](repeating: 0, count: src32.count),
        tmp_line = [Double](repeating: 0, count: max(width, height) * 4)
        let coeff = gaussCoef(sigma: radius)
    
        convolveRGBA(src: src32, out: &out, line: &tmp_line, coeff: coeff, width: width, height: height)
        convolveRGBA(src: out, out: &src32, line: &tmp_line, coeff: coeff, width: height, height: width)
    }           
}

class Benchmark{
    var width = 800
    var height = 450
    var radius = 15
    var buffer: [UInt32] = []
         
    //@Setup
    init(){
        var seedNum:Int = 49734321
        let rand = {() -> () -> Double in
        return {() -> Double in
            seedNum = ((seedNum + 0x7ed55d16) + (seedNum << 12)) & 0xffffffff
            seedNum = ((seedNum ^ 0xc761c23c) ^ (seedNum >>> 19)) & 0xffffffff
            seedNum = ((seedNum + 0x165667b1) + (seedNum  << 5)) & 0xffffffff
            seedNum = ((seedNum + 0xd3a2646c) ^ (seedNum << 9)) & 0xffffffff
            seedNum = ((seedNum + 0xfd7046c5) + (seedNum << 3)) & 0xffffffff
            seedNum = ((seedNum ^ 0xb55a4f09) ^ (seedNum >>> 16)) & 0xffffffff
            return Double(seedNum & 0xfffffff) / Double(0x10000000)
            }
        }()
    
        var bufferArry = [UInt32](repeating: 0, count: width * height)
        for i in 0..<bufferArry.count {
            bufferArry[i] = UInt32(rand())
        }
        self.buffer = bufferArry
    }
    //@Benchmark
    func runIteration() {
        let setupStart = Timer().getTime()
        for _ in  0..<20{
            GaussianBlur().blurRGBA(src: buffer, width: width, height: height, radius: radius)
        }
       
        let setupEnd = Timer().getTime()
        print("gaussian-blur: ms = " + String(setupEnd - setupStart))
    }
    
}

fileprivate func debugLog(_ str: String,object: Any) {
    let isLog = false
    if isLog {
        print("\(str): \(object)")
    }
}

infix operator >>> : BitwiseShiftPrecedence

func >>> (lhs: Int, rhs: Int) -> Int {
    return Int(bitPattern: UInt(bitPattern: lhs) >> UInt(rhs))
}

class Timer {
    private let CLOCK_REALTIME = 0
    private var time_spec = timespec()
    func getTime() -> Double {
        clock_gettime(Int32(CLOCK_REALTIME),&time_spec)
        return Double(time_spec.tv_sec * 1_000_000 + time_spec.tv_nsec / 1_000)/1000
    }
}

Benchmark().runIteration()
