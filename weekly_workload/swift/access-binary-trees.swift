import Glibc

class TreeNode {
    let left : TreeNode?
    let right : TreeNode?
    let item : Double
    
    init(_ left: TreeNode?, _ right: TreeNode?,_ item: Double) {
        self.left = left
        self.right = right
        self.item = item
    }
    
    func itemCheck()->Double {
        if self.left == nil {
            return self.item
        }else {
            return self.item + (self.left?.itemCheck() ?? 0.0) - (self.right?.itemCheck() ?? 0.0)
        }
    }
}

func bottomUpTree(_ item : Double,_ depth : Int) -> TreeNode {
   if depth > 0 {
      return TreeNode(
        bottomUpTree(2*item-1, depth-1),
        bottomUpTree(2*item, depth-1),
        item)
   }else {
       return TreeNode(nil, nil, item)
   }
}

func run() {
    var ret = 0.0

    for n in 4...7 {
        let minDepth = 4
        let maxDepth = max(minDepth + 2, n)
        let stretchDepth = maxDepth + 1
        
        var check = bottomUpTree(0,stretchDepth).itemCheck()
        
        let longLivedTree = bottomUpTree(0,maxDepth)
        var depth = minDepth
        while depth<=maxDepth {
            let iterations = 1 << (maxDepth - depth + minDepth)
            
            check = 0
            for i in 1...iterations {
                check += bottomUpTree(Double(i),depth).itemCheck()
                check += bottomUpTree(-Double(i),depth).itemCheck()
            }
            depth += 2
        }

        ret += longLivedTree.itemCheck()
    }
    
    let expected: Double = -4
    if (ret != expected) {
        print("ERROR: bad result: expected \(expected)  but got \(ret)")
        
    }
}

class Timer {
    private let CLOCK_REALTIME = 0
    private var time_spec = timespec()

    func getTime() -> Double {
        clock_gettime(Int32(CLOCK_REALTIME),&time_spec)
        return Double(time_spec.tv_sec * 1_000_000 + time_spec.tv_nsec / 1_000)
    }
}

/// @Benchmark
func runIterationTime() {
    let start = Timer().getTime()
    for _ in 0..<80 {
        run()
    }
    let end = Timer().getTime()
    let duration = (end - start) / 1000
    print("access-binary-trees: ms = \(duration)")
}

runIterationTime()
