//
//  3d-raytrace.swift
//  Worker
//
//  Created by 史栋 on 2023/11/9.
//

import Glibc

/// 矢量类 存储x,y,z三个浮点数，用来表示一个位置，一个像素点
class Vector {
    
    var values: [Double]
    
    var reflection: Double?
    
    var colour: Vector?
    
    static func zero() -> Vector {
        return Vector(0, 0, 0)
    }
    
    init(_ x: Double,_ y: Double,_ z: Double) {
        values = [x, y, z]
    }
    // log打印使用
    var description: String {
        "[\(scriptString)]"
    }
    // 绘制脚本拼接使用
    var scriptString: String {
        "\(values[0]),\(values[1]),\(values[2])"
    }
}

func createVector(_ x: Double, _ y: Double, _ z: Double) -> Vector{
    Vector(x,y,z)
}

func sqrLengthVector(_ vector: Vector) -> Double{
    return vector.values[0] * vector.values[0] + vector.values[1] * vector.values[1] + vector.values[2] * vector.values[2]
}

func lengthVector(_ vector: Vector) -> Double{
    sqrt(vector.values[0] * vector.values[0] + vector.values[1] * vector.values[1] + vector.values[2] * vector.values[2])
}

func addVector(_ vector: Vector, _ v: Vector) -> Vector {
    vector.values[0] += v.values[0]
    vector.values[1] += v.values[1]
    vector.values[2] += v.values[2]
    return vector
}

func subVector(_ vector: Vector, _ v: Vector) -> Vector {
    vector.values[0] -= v.values[0];
    vector.values[1] -= v.values[1];
    vector.values[2] -= v.values[2];
    return vector;
}

func scaleVector(_ vector: Vector, _ scale: Double) -> Vector{
    vector.values[0] *= scale;
    vector.values[1] *= scale;
    vector.values[2] *= scale;
    return vector;
}

func normaliseVector(_ vector: Vector) -> Vector {
    let len = sqrt(vector.values[0] * vector.values[0] + vector.values[1] * vector.values[1] + vector.values[2] * vector.values[2]);
    vector.values[0] /= len;
    vector.values[1] /= len;
    vector.values[2] /= len;
    return vector;
}

func add(_ v1: Vector, _ v2: Vector) -> Vector {
    Vector(v1.values[0] + v2.values[0], v1.values[1] + v2.values[1], v1.values[2] + v2.values[2]);
}

func sub(_ v1: Vector, _ v2: Vector) -> Vector {
    Vector(v1.values[0] - v2.values[0], v1.values[1] - v2.values[1], v1.values[2] - v2.values[2]);
}

func scalev(_ v1: Vector, _ v2: Vector) -> Vector {
    Vector(v1.values[0] * v2.values[0], v1.values[1] * v2.values[1], v1.values[2] * v2.values[2]);
}

func dot(_ v1: Vector, _ v2: Vector) -> Double {
    v1.values[0] * v2.values[0] + v1.values[1] * v2.values[1] + v1.values[2] * v2.values[2];
}

func scale(_ v: Vector,_ scale: Double) -> Vector{
    Vector(v.values[0] * scale, v.values[1] * scale, v.values[2] * scale)
}

func cross(_ v1: Vector, _ v2: Vector) -> Vector {
    Vector(v1.values[1] * v2.values[2] - v1.values[2] * v2.values[1],
           v1.values[2] * v2.values[0] - v1.values[0] * v2.values[2],
           v1.values[0] * v2.values[1] - v1.values[1] * v2.values[0])
    
}

func normalise(_ v: Vector) -> Vector{
    let len = lengthVector(v)
    return Vector(v.values[0] / len, v.values[1] / len, v.values[2] / len)
}

func transformMatrix(_ matrix: [Double],_ v: Vector) -> Vector {
    let vals = matrix
    let x  = vals[0] * v.values[0] + vals[1] * v.values[1] + vals[2] * v.values[2] + vals[3]
    let y  = vals[4] * v.values[0] + vals[5] * v.values[1] + vals[6] * v.values[2] + vals[7]
    let z  = vals[8] * v.values[0] + vals[9] * v.values[1] + vals[10] * v.values[2] + vals[11]
    return Vector(x, y, z)
}

func invertMatrix(_ matrix: inout [Double]) -> [Double] {
    var temp = Array<Double>(repeating: 0, count: 16)
    let tx = -matrix[3];
    let ty = -matrix[7];
    let tz = -matrix[11];
    for h in 0..<3 {
        for v in 0..<3 {
            temp[h + v * 4] = matrix[v + h * 4]
        }
    }
    for i in 0..<11 {
        matrix[i] = temp[i]
    }
    matrix[3] = tx * matrix[0] + ty * matrix[1] + tz * matrix[2]
    matrix[7] = tx * matrix[4] + ty * matrix[5] + tz * matrix[6]
    matrix[11] = tx * matrix[8] + ty * matrix[9] + tz * matrix[10]
    return matrix
}

typealias ShaderCallback = (_ triangle: Triangle, _ v1: Vector, _ v2: Vector) -> Vector

/// 三角形
class Triangle {
    
    var axis: Int
    
    var normal: Vector
    
    var nu: Double
    
    var nv: Double
    
    var nd: Double
    
    var eu: Double
    
    var ev: Double
    
    var nu1: Double
    
    var nv1: Double
    
    var nu2: Double
    
    var nv2: Double
    
    var material: Vector
    
    var shader: ShaderCallback?
    
    init(_ p1: Vector, _ p2: Vector, _ p3: Vector) {
        let edge1 = sub(p3, p1);
        let edge2 = sub(p2, p1);
        let normal = cross(edge1, edge2);
        if (abs(normal.values[0]) > abs(normal.values[1])) {
            if (abs(normal.values[0]) > abs(normal.values[2])) {
                axis = 0
            }
            else {
                axis = 2
            }
        } else {
            if (abs(normal.values[1]) > abs(normal.values[2])) {
                axis = 1
            }
            else {
                axis = 2
            }
        }
        let u = (axis + 1) % 3
        let v = (axis + 2) % 3
        let u1 = edge1.values[u]
        let v1 = edge1.values[v]
        let u2 = edge2.values[u]
        let v2 = edge2.values[v]
        self.normal = normalise(normal)
        nu = normal.values[u] / normal.values[axis]
        nv = normal.values[v] / normal.values[axis]
        nd = dot(normal, p1) / normal.values[axis]
        let det = u1 * v2 - v1 * u2
        eu = p1.values[u];
        ev = p1.values[v];
        nu1 = u1 / det;
        nv1 = -v1 / det;
        nu2 = v2 / det;
        nv2 = -u2 / det;
        material = Vector(0.7, 0.7, 0.7)
    }
    
    func intersect(_ orig: Vector, _ dir: Vector, _ near: Double?, _ far: Double?) -> Double? {
        let u = (axis + 1) % 3
        let v = (axis + 2) % 3
        let d = dir.values[axis] + nu * dir.values[u] + nv * dir.values[v]
        let t = (nd - orig.values[axis] - nu * orig.values[u] - nv * orig.values[v]) / d;
        if let near = near, let far = far {
            if t < near || t > far {
                return nil
            }
        }
        let Pu = orig.values[u] + t * dir.values[u] - eu;
        let Pv = orig.values[v] + t * dir.values[v] - ev;
        let a2 = Pv * nu1 + Pu * nv1;
        if (a2 < 0) {
            return nil
        }
        let a3 = Pu * nu2 + Pv * nv2;
        if (a3 < 0) {
            return nil
        }
        if ((a2 + a3) > 1) {
            return nil
        }
        return t
    }
    
}


/// 场景
class Scene {
    
    var triangles: [Triangle]
    
    var lights: [Vector]
    
    var ambient: Vector
    
    var background: Vector
    
    init(_ a_triangles: [Triangle]) {
        triangles = a_triangles
        lights = [.zero(), .zero(), .zero()]
        ambient = Vector.zero()
        background = Vector(0.8,0.8,1)
    }
    
    func intersect(_ origin: Vector, _ dir: Vector, _ near: Double?, _ far: Double?) -> Vector {
        var closest: Triangle?
        var rfar = far
        for i in 0..<triangles.count {
            let triangle = triangles[i];
            let d = triangle.intersect(origin, dir, near, rfar)
            if d == nil {
                continue
            }
            rfar = d
            closest = triangle
        }
        guard let closest = closest,let rfar = rfar else {
            return Vector(background.values[0],background.values[1],background.values[2])
        }
        var normal = closest.normal;
        let hit = add(origin, scale(dir, rfar));
        if dot(dir, normal) > 0 {
            normal = Vector(-normal.values[0], -normal.values[1], -normal.values[2])
        }
        
        var colour: Vector!
        if let shader = closest.shader {
            colour = shader(closest, hit, dir)
        } else {
            colour = closest.material
        }
        
        var reflected: Vector?
        if colour.reflection != nil {
            if (colour.reflection! > 0.001) {
                let reflection = addVector(scale(normal, -2 * dot(dir, normal)), dir)
                reflected = intersect(hit, reflection, 0.0001, 1000000)
                if colour.reflection! >= 0.999999 {
                    return reflected!
                }
            }
        }
        
        var l = Vector(ambient.values[0], ambient.values[1], ambient.values[2])
        for  i in 0..<lights.count {
            let light = lights[i]
            let toLight = sub(light, hit)
            var distance = lengthVector(toLight)
            _ = scaleVector(toLight, 1.0/distance)
            distance -= 0.0001
            if blocked(hit, toLight, distance) {
                continue
            }
            let nl = dot(normal, toLight)
            if nl > 0 {
                _ = addVector(l, scale(light.colour!, nl))
            }
        }
        l = scalev(l, colour);
        if let reflected = reflected {
            l = addVector(scaleVector(l, 1 - colour.reflection!), scaleVector(reflected, colour.reflection!))
        }
        return l
    }
    
    func blocked(_ O: Vector, _ D: Vector, _ far: Double) -> Bool{
        let near: Double = 0.0001;
        for i in 0..<triangles.count {
            let triangle = triangles[i];
            let d = triangle.intersect(O, D, near, far);
            if let dd = d {
                if dd > far || dd < near {
                    continue
                }
            } else {
                continue
            }
            return true;
        }
        return false;
    }
    
}

/// 光线
class Ray {
    
    var origin: Vector

    var dir: Vector
    
    init(_ origin: Vector, _ dir: Vector) {
        self.origin = origin
        self.dir = dir
    }
    
    static func zero() -> Ray {
        return Ray(.zero(), .zero())
    }
    
}

/// 相机
class Camera {
    
    var directions: [Vector]
    
    var origin: Vector
    
    var rays: [Ray]
    
    init(_ origin: Vector, _ lookat: Vector, _ up: Vector) {
        let zaxis = normaliseVector(subVector(lookat, origin))
        let xaxis = normaliseVector(cross(up, zaxis))
        let yaxis = normaliseVector(cross(xaxis, subVector(Vector(0,0,0), zaxis)))
        var m = Array<Double>(repeating: 0, count: 16)
        m[0] = xaxis.values[0]
        m[1] = xaxis.values[1]
        m[2] = xaxis.values[2]
        m[4] = yaxis.values[0]
        m[5] = yaxis.values[1]
        m[6] = yaxis.values[2]
        m[8] = zaxis.values[0]
        m[9] = zaxis.values[1]
        m[10] = zaxis.values[2]
        _ = invertMatrix(&m)
        m[3] = 0
        m[7] = 0
        m[11] = 0
        self.origin = origin
        directions = Array(repeating: .zero(), count: 4)
        directions[0] = normalise(Vector(-0.7,  0.7, 1))
        directions[1] = normalise(Vector( 0.7,  0.7, 1))
        directions[2] = normalise(Vector( 0.7, -0.7, 1))
        directions[3] = normalise(Vector(-0.7, -0.7, 1))
        directions[0] = transformMatrix(m, directions[0]);
        directions[1] = transformMatrix(m, directions[1]);
        directions[2] = transformMatrix(m, directions[2]);
        directions[3] = transformMatrix(m, directions[3]);
        rays = []
    }
    
    /// 生成光线
    func generateRayPair(_ y: Double) -> [Ray]{
        let dir1 = addVector(scale(directions[0], y), scale(directions[3], 1-y))
        let ray1 = Ray(origin, dir1)
        let dir2 = addVector(scale(directions[1], y), scale(directions[2], 1-y))
        let ray2 = Ray(origin, dir2)
        rays = [ray1, ray2]
        return rays
    }
    
    func render(_ scene: Scene, _ pixels: inout [[Vector]], _ width: Int, _ height: Int) {
        renderRows(self, scene, &pixels, width, height, 0, height)
    }
    
}

func renderRows(_ camera: Camera, _ scene: Scene, _ pixels: inout [[Vector]], _ width: Int, _ height: Int, _ starty: Int, _ stopy: Int) {
    var pointNum = 1
    for y in starty..<stopy {
        let rays = camera.generateRayPair(Double(y) / Double(height));
        for x in 0..<width {
            let xp = Double(x) / Double(width)
            let origin = addVector(scale(rays[0].origin, xp), scale(rays[1].origin, 1 - xp))
            let dir = normaliseVector(addVector(scale(rays[0].dir, xp), scale(rays[1].dir, 1 - xp)))
            let l = scene.intersect(origin, dir, nil, nil)
            pixels[y][x] = l
            if DEBUG {
                print("point \(pointNum): \(l.description)")
                pointNum += 1
            }
        }
    }
}



/// 构建场景
func raytraceScene() -> [[Vector]]
{
    /// 创建矢量，通过矢量构建三角形
    let tfl = createVector(-10,  10, -10)
    let tfr = createVector( 10,  10, -10)
    let tbl = createVector(-10,  10,  10)
    let tbr = createVector( 10,  10,  10)
    let bfl = createVector(-10, -10, -10)
    let bfr = createVector( 10, -10, -10)
    let bbl = createVector(-10, -10,  10)
    let bbr = createVector( 10, -10,  10)
    var triangles = [
        Triangle(tfl, tfr, bfr),
        Triangle(tfl, bfr, bfl),
        Triangle(tbl, tbr, bbr),
        Triangle(tbl, bbr, bbl),
        Triangle(tbl, tfl, bbl),
        Triangle(tfl, bfl, bbl),
        Triangle(tbr, tfr, bbr),
        Triangle(tfr, bfr, bbr),
        Triangle(tbl, tbr, tfr),
        Triangle(tbl, tfr, tfl),
        Triangle(bbl, bbr, bfr),
        Triangle(bbl, bfr, bfl)
    ]
    let floorShader =  { (tri: Triangle, pos: Vector, view: Vector) -> Vector in
        let green = createVector(0.0, 0.4, 0.0);
        let grey = createVector(0.4, 0.4, 0.4);
        grey.reflection = 1.0;
        let x = ((pos.values[0] / 32.0).truncatingRemainder(dividingBy: 2.0) + 2).truncatingRemainder(dividingBy: 2.0)
        let z = ((pos.values[2] / 32.0 + 0.3).truncatingRemainder(dividingBy: 2.0) + 2).truncatingRemainder(dividingBy: 2.0)
        if (x < 1.0) != (z < 1.0) {
            return grey
        } else {
            return green
        }
    }
    let ffl = createVector(-1000, -30, -1000)
    let ffr = createVector( 1000, -30, -1000)
    let fbl = createVector(-1000, -30,  1000)
    let fbr = createVector( 1000, -30,  1000)
    let tr13 = Triangle(fbl, fbr, ffr)
    tr13.shader = floorShader
    triangles.append(tr13)
    let tr14 = Triangle(fbl, ffr, ffl)
    tr14.shader = floorShader
    triangles.append(tr14)
    /// 通过三角形数组构建场景
    let _scene = Scene(triangles)
  /// 设置场景灯光1
    _scene.lights[0] = createVector(20, 38, -22)
    _scene.lights[0].colour = createVector(0.7, 0.3, 0.3)
  /// 设置场景灯光2
    _scene.lights[1] = createVector(-23, 40, 17)
    _scene.lights[1].colour = createVector(0.7, 0.3, 0.3)
  /// 设置场景灯光3
    _scene.lights[2] = createVector(23, 20, 17)
    _scene.lights[2].colour = createVector(0.7, 0.7, 0.7)
    /// 设置背景
    _scene.ambient = createVector(0.1, 0.1, 0.1);
    let size = 30;
    /// 初始化像素数组
    var pixels: [[Vector]] = Array(repeating: Array(repeating: .zero(), count: size), count: size)
    /// 初始化相机
    let _camera = Camera(createVector(-40, 40, 40), createVector(0, 0, 0), createVector(0, 1, 0))
    /// 执行3d-raytrace算法，生成像素数组
    _camera.render(_scene, &pixels, size, size)
    return pixels
}

/// 使用像素数组生成绘制图像的js脚本字符串
func arrayToCanvasCommands(_ pixels: [[Vector]]) -> String
{
    var str = "<canvas id=\"renderCanvas\" width=\"30px\" height=\"30px\"></canvas><script>\nvar pixels = ["
    let size = 30;
    for  y in 0..<size {
        str += "[";
        for x in 0..<size {
            str += "[\(pixels[y][x].scriptString)],"
        }
        str += "],"
    }
    str += """
];\n    var canva = document.getElementById("renderCanvas").getContext("2d");\n\
    \n\
    \n\
    var size = 30;\n\
    canva.fillStyle = "red";\n\
    canva.fillRect(0, 0, size, size);\n\
    canva.scale(1, -1);\n\
    canva.translate(0, -size);\n\
    \n\
    if (!canva.setFillColor)\n\
        canva.setFillColor = function(r, g, b, a) {\n\
        this.fillStyle = "rgb("+[Math.floor(r * 255), Math.floor(g * 255), Math.floor(b * 255)]+")";\n\
    }\n\
    \n\
    for (var y = 0; y < size; y++) {\n\
        for (var x = 0; x < size; x++) {\n\
            var l = pixels[y][x];\n\
            canva.setFillColor(l[0], l[1], l[2], 1);\n\
            canva.fillRect(x, y, 1, 1);\n\
        }\n\
    }</script>
"""
    return str;
}


/********** 测试部分 *******/

/// 是否输出日志
let DEBUG = false
/// 是否输出脚本
let showRenderJs = false

/*
 * @State
 * @Tags Jetstream2
 */
class RayTraceBranchMark {
    
    var runCount = 80
  /*
   * @Benchmark
   */
    func run() {
        let startTime = Timer().getTime()
        for _ in 0..<runCount {
            let result = arrayToCanvasCommands(raytraceScene())
            if DEBUG && showRenderJs {
                print(result)
            }
        }
        let endTime = Timer().getTime()
        print("3d-raytrace: ms = \((endTime - startTime) / 1000.0)")
    }
    
}

RayTraceBranchMark().run()

class Timer {
    
    private let CLOCK_REALTIME = 0
    
    private var time_spec = timespec()

    func getTime() -> Double {
        
        clock_gettime(Int32(CLOCK_REALTIME),&time_spec)
        return Double(time_spec.tv_sec * 1_000_000 + time_spec.tv_nsec / 1_000)
    }
}
