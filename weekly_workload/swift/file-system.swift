/**
 ** Copyright (C) 2018 Apple Inc. All rights reserved.
 **
 **Redistribution and use in source and binary forms, with or without
 ** modification, are permitted provided that the following conditions
 ** are met:
 ** 1. Redistributions of source code must retain the above copyright
 **    notice, this list of conditions and the following disclaimer.
 ** 2. Redistributions in binary form must reproduce the above copyright
 **    notice, this list of conditions and the following disclaimer in the
 **    documentation and/or other materials provided with the distribution.
 **
 ** THIS SOFTWARE IS PROVIDED BY APPLE INC. AND ITS CONTRIBUTORS ``AS IS''
 ** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 ** THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 ** PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL APPLE INC. OR ITS CONTRIBUTORS
 ** BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 ** CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 ** SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 ** INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 ** CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 ** ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 ** THE POSSIBILITY OF SUCH DAMAGE.
**/
import Glibc

var seedNum: Int = 49734321
func resetSeed() {
    seedNum = 49734321
}

let random = {() -> () -> Double in
    return {() -> Double in
        // Robert Jenkins' 32 bit integer hash function.
        seedNum = ((seedNum + 0x7ed55d16) + (seedNum << 12))  & 0xffffffff
        seedNum = ((seedNum ^ 0xc761c23c) ^ (seedNum >>> 19)) & 0xffffffff
        seedNum = ((seedNum + 0x165667b1) + (seedNum << 5))   & 0xffffffff
        seedNum = ((seedNum + 0xd3a2646c) ^ (seedNum << 9))   & 0xffffffff
        seedNum = ((seedNum + 0xfd7046c5) + (seedNum << 3))   & 0xffffffff
        seedNum = ((seedNum ^ 0xb55a4f09) ^ (seedNum >>> 16)) & 0xffffffff
        return Double(seedNum & 0xfffffff) / Double(0x10000000)
    }
}()

func randomFileContents(_ bytes: Int = (Int(random() * 128) >>> 0) + 2056) -> [UInt8] {
    var result = [UInt8](repeating: 0, count: bytes)
    for i in 0 ..< bytes {
        result[i] = (UInt8(Int(random() * Double(255)) >>> 0))
    }
    return result
}

class File {
    private var _data: [UInt8]
    init(_ dataView: [UInt8]) {
        _data = dataView
    }
    var data: [UInt8] {
        set { _data = newValue }
        get { return _data }
    }
    func swapByteOrder() {
        for i in 0 ..< data.count {
            data[i] = data[data.count-1-i]
        } 
    }
}

class Directory {
    var structure: Dictionary<String, Any>
    init() {
        structure = Dictionary<String, Any>()
    }
    
    func addFile(_ name: String, _ file: File) async -> File {
        let entryFile = structure[name]
        if (entryFile != nil) {
            if (entryFile is File){
                fatalError("Can't replace file with file;")
            }
            if (entryFile is Directory){
                fatalError("Can't replace a file with a new directory;")
            }
            fatalError("Should not reach this code;")
        }
        structure[name] = file
        return file
    }
    
    func addDirectory(_ name: String, _ directory: Directory = Directory()) async -> Directory {
        let entryFile = structure[name]
        if (entryFile != nil) {
            if (entryFile is File){
                fatalError("Can't replace file with directory;")
            }
            if (entryFile is Directory){
                fatalError("Can't replace directory with new directory;")
            }
            fatalError("Should not reach this code;")
        }
        structure[name] = directory
        return directory
    }
    
    func ls() async -> [(name: String, entry: Any, isDirectory: Bool)] {
        var result: [(name: String, entry: Any, isDirectory: Bool)] = [(name:"",entry:Directory(),isDirectory:false)]
        result.removeFirst()
        for item in structure {
            result.append((name:item.0,entry:item.1,isDirectory:(item.1 is Directory)))
        }
        return result
    }
    
    func forEachFile() async -> [(name: String, entry: Any, isDirectory: Bool)] {
        var result: [(name: String, entry: Any, isDirectory: Bool)] = [(name:"",entry:Directory(),isDirectory:false)]
        result.removeFirst()
        for item in await ls() {
            if !item.2 {
                result.append(item)
            }
        }
        return result
    }
    
    func forEachFileRecursively() async -> [(name: String, entry: Any, isDirectory: Bool)] {
        var result: [(name: String, entry: Any, isDirectory: Bool)] = [(name:"",entry:Directory(),isDirectory:false)]
        result.removeFirst()
        for item in await ls() {
            if item.2 {
                for file in await (item.entry as! Directory).forEachFileRecursively() {
                    result.append(file)
                }
            } else {
                result.append(item)
            }
        }
        return result
    }
    
    func forEachDirectoryRecursively() async -> [(name: String, entry: Any, isDirectory: Bool)] {
        var result: [(name: String, entry: Any, isDirectory: Bool)] = [(name:"",entry:Directory(),isDirectory:false)]
        result.removeFirst()
        for item in await ls() {
            if !item.2 {
                continue
            }
            for dirItem in await (item.1 as! Directory).forEachDirectoryRecursively() {
                result.append(dirItem)
            }
            result.append(item)
        }
        return result
    }
    
    func fileCount() async -> Int {
        var count = 0
        for item in await ls() {
            if !item.2 {
                count += 1
            }
        }
        return count
    }
    
    func rm(_ name:String) async -> Bool {
        return structure.removeValue(forKey: name) != nil
    }
}

func setupDirectory() async -> Directory {
    let fs = Directory()
    var dirs = [fs]
    
    for index in 0 ..< 250 {
        let dir = dirs[index]
        for i in 0 ..< 8 {
            if (dirs.count < 250 && random() >= 0.3) {
                dirs.append(await dir.addDirectory("dir-\(i)"))
            }
        }
    }
    
    for dir in dirs {
        for i in 0 ..< 5 {
            if random() >= 0.6 {
                await dir.addFile("file-\(i)", File(randomFileContents()))
            }
        }
    }
    
    return fs
}

 /*
  * @State
  * @Tags Jetstream2
  */
class Benchmark {
    /*
     *@Benchmark
     */
    func runIteration() async { 
        resetSeed()
        let fs = await setupDirectory()
        if isDebug {
            let dirs = await fs.forEachDirectoryRecursively()
            let files = await fs.forEachFileRecursively()
            //printLog("根节点fs下所有dir的数量====="+String(dirs.count))
            //printLog("根节点fs下所有dir下的file的数量====="+String(files.count))
        }
        for file in await fs.forEachFileRecursively() {
            (file.1 as! File).swapByteOrder()
        }
        for item in await fs.forEachDirectoryRecursively() {
            let dir = item.1 as! Directory
            if await dir.fileCount() > 3 {
                if isDebug {
                    let deles = await dir.fileCount()
                    //printLog("")
                    //printLog("dir对象删除的file数量====="+String(deles))
                }
                for name in await dir.forEachFile() {
                    let result = await dir.rm(name.0)
                    if !result {
                        fatalError("rm should have returned true")
                    }
                }
            }
        }
    }
} 

infix operator >>> : BitwiseShiftPrecedence
func >>> (lhs:Int,rhs:Int) -> Int {
    if lhs >= 0 {
        return lhs >> rhs
    }else{
        return (Int.max + lhs + 1) >> rhs | (1 << (63 - rhs))
    }
}
class Timer {
   private let CLOCK_REALTIME = 0
   private var time_spec = timespec()

   func getTime() -> Double {
       clock_gettime(Int32(CLOCK_REALTIME),&time_spec)
       return Double(time_spec.tv_sec * 1_000_000 + time_spec.tv_nsec / 1_000)
   }
}
 
//以下是测试打印日志相关代码
let isDebug = false
func printLog(_ str: String) -> Void {
    print(str)
}

func addLoop(_ times: Int, ben:Benchmark = Benchmark()) async {
    let start = Timer().getTime()
    for _ in 0 ..< times {
        await ben.runIteration()
    }
    let end = Timer().getTime()
    print("file-system: ms = \((end - start) / 1000)")
}

await addLoop(10)
