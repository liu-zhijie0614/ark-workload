import Glibc

class Timer {
  private let CLOCK_REALTIME = 0
  private var start_timespec = timespec()
  private var end_timespec = timespec()
  private var time_spec = timespec()
  func start() {
        clock_gettime(Int32(CLOCK_REALTIME),&start_timespec)
    }

  func stop() -> Double {
  clock_gettime(Int32(CLOCK_REALTIME),&end_timespec)
  let start_time = Double(start_timespec.tv_sec * 1_000_000 + start_timespec.tv_nsec / 1_000)
  let end_time = Double(end_timespec.tv_sec * 1_000_000 + end_timespec.tv_nsec / 1_000)
  let time = end_time - start_time
  return time / 1_000
}
func getTime() -> Double {
        clock_gettime(Int32(CLOCK_REALTIME),&time_spec)
        return Double(time_spec.tv_sec * 1_000_000 + time_spec.tv_nsec / 1_000)
    }
}

var timer = Timer();
var cnt = 0;
timer.start();
for i in 0..<10000 {
  for j in 0..<10000 {
    cnt = cnt + i + j;
  }
}
var time = timer.stop();
//print(cnt);
//print(time);
print("test_for_plus1w:\t"+String(time)+"\tms");