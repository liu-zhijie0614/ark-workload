declare function print(a:any,b?:any):string

//***********************  ArkTS 获取时间戳  **************************
declare interface ArkTools {
  timeInUs(args: number): number;
}
function currentTimestamp16():number {
  return ArkTools.timeInUs()
}
// var start = ArkTools.timeInUs()
var start = currentTimestamp16()
function foo() {
  var x:number = 0;
  for (var i:number = 0 ; i < 30000000; i++) {
    x = (i + 0.5) * (i + 0.5);
  }
  return x
}
for (let i:number=0;i<10;i++){
  var result = foo()
}
// var end = ArkTools.timeInUs()
var end = currentTimestamp16()
print("Forlooper_Obj: " + ((end - start)/1000) + "\tms");