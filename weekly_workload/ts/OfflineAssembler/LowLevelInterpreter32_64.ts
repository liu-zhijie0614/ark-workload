/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { File } from './file';

('use strict');

export default function lowLevelInterpreter3264(): void {
  let source = `
# Copyright (C) 2011-2017 Apple Inc. All rights reserved.\n\#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:\n\# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the distribution.\n\#
# THIS SOFTWARE IS PROVIDED BY APPLE INC. AND ITS CONTRIBUTORS \`\`AS IS''
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
# THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
# PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL APPLE INC. OR ITS CONTRIBUTORS
# BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, \
OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF\n\# THE POSSIBILITY OF SUCH DAMAGE.
\n\
# Utilities
macro dispatch(advance)
    addp advance * 4, PC
    jmp [PC]
end\n\
macro dispatchBranchWithOffset(pcOffset)
    lshifti 2, pcOffset
    addp pcOffset, PC
    jmp [PC]
end\n\
macro dispatchBranch(pcOffset)
    loadi pcOffset, t0
    dispatchBranchWithOffset(t0)
end\n\
macro dispatchAfterCall()
    loadi ArgumentCount + TagOffset[cfr], PC
    loadi 4[PC], t3
    storei r1, TagOffset[cfr, t3, 8]\n\    storei r0, PayloadOffset[cfr, t3, 8]
    valueProfile(r1, r0, 4 * (CallOpCodeSize - 1), t3)
    dispatch(CallOpCodeSize)
end\n\
macro cCall2(function)
    if ARM or ARMv7 or ARMv7_TRADITIONAL or MIPS
        call function
    elsif X86 or X86_WIN
        subp 8, sp\n\        push a1
        push a0
        call function
        addp 16, sp
    elsif C_LOOP\n\        cloopCallSlowPath function, a0, a1
    else
        error
    end
end\n\
macro cCall2Void(function)
    if C_LOOP
        cloopCallSlowPathVoid function, a0, a1
    else
        cCall2(function)\n\    end
end\n\
macro cCall4(function)
    if ARM or ARMv7 or ARMv7_TRADITIONAL or MIPS
        call function
    elsif X86 or X86_WIN
        push a3\n\        push a2
        push a1
        push a0
        call function
        addp 16, sp
    elsif C_LOOP\n\        error
    else
        error
    end
end\n\
macro callSlowPath(slowPath)
    move cfr, a0
    move PC, a1
    cCall2(slowPath)\n\    move r0, PC
end

macro doVMEntry(makeCall)
    functionPrologue()
    pushCalleeSaves()\n\
    # x86 needs to load arguments from the stack
    if X86 or X86_WIN
        loadp 16[cfr], a2
        loadp 12[cfr], a1
        loadp 8[cfr], a0\n\    end

    const entry = a0
    const vm = a1
    const protoCallFrame = a2\n\
    # We are using t3, t4 and t5 as temporaries through the function.
    # Since we have the guarantee that tX != aY when X != Y, we are safe from
    # aliasing problems with our arguments.\n\
    if ARMv7
        vmEntryRecord(cfr, t3)
        move t3, sp
    else\n\        vmEntryRecord(cfr, sp)
    end\n\
    storep vm, VMEntryRecord::m_vm[sp]
    loadp VM::topCallFrame[vm], t4
    storep t4, VMEntryRecord::m_prevTopCallFrame[sp]
    loadp VM::topVMEntryFrame[vm], t4
    storep t4, VMEntryRecord::m_prevTopVMEntryFrame[sp]\n\
    # Align stack pointer
    if X86_WIN or MIPS
        addp CallFrameAlignSlots * SlotSize, sp, t3
        andp ~StackAlignmentMask, t3\n\        subp t3, CallFrameAlignSlots * SlotSize, sp
    elsif ARM or ARMv7 or ARMv7_TRADITIONAL
        addp CallFrameAlignSlots * SlotSize, sp, t3
        clrbp t3, StackAlignmentMask, t3
        if ARMv7
            subp t3, CallFrameAlignSlots * SlotSize, t3\n\            move t3, sp
        else
            subp t3, CallFrameAlignSlots * SlotSize, sp
        end
    end\n\
    loadi ProtoCallFrame::paddedArgCount[protoCallFrame], t4
    addp CallFrameHeaderSlots, t4, t4
    lshiftp 3, t4
    subp sp, t4, t3
    bpa t3, sp, .throwStackOverflow\n\
    # Ensure that we have enough additional stack capacity for the incoming args,
    # and the frame for the JS code we're executing. We need to do this check
    # before we start copying the args from the protoCallFrame below.
    if C_LOOP\n\        bpaeq t3, VM::m_cloopStackLimit[vm], .stackHeightOK
    else
        bpaeq t3, VM::m_softStackLimit[vm], .stackHeightOK
    end\n\
    if C_LOOP
        move entry, t4
        move vm, t5
        cloopCallSlowPath _llint_stack_check_at_vm_entry, vm, t3
        bpeq t0, 0, .stackCheckFailed\n\        move t4, entry
        move t5, vm
        jmp .stackHeightOK\n\
.stackCheckFailed:
        move t4, entry
        move t5, vm
    end\n\
.throwStackOverflow:
    subp 8, sp # Align stack for cCall2() to make a call.
    move vm, a0
    move protoCallFrame, a1
    cCall2(_llint_throw_stack_overflow_error)\n\
    if ARMv7
        vmEntryRecord(cfr, t3)\n\        move t3, sp
    else
        vmEntryRecord(cfr, sp)
    end\n\
    loadp VMEntryRecord::m_vm[sp], t5
    loadp VMEntryRecord::m_prevTopCallFrame[sp], t4
    storep t4, VM::topCallFrame[t5]
    loadp VMEntryRecord::m_prevTopVMEntryFrame[sp], t4
    storep t4, VM::topVMEntryFrame[t5]\n\
    if ARMv7
        subp cfr, CalleeRegisterSaveSize, t5\n\        move t5, sp
    else
        subp cfr, CalleeRegisterSaveSize, sp
    end\n\
    popCalleeSaves()
    functionEpilogue()
    ret\n\
.stackHeightOK:
    move t3, sp
    move 4, t3\n\
.copyHeaderLoop:
    subi 1, t3\n\    loadi TagOffset[protoCallFrame, t3, 8], t5
    storei t5, TagOffset + CodeBlock[sp, t3, 8]
    loadi PayloadOffset[protoCallFrame, t3, 8], t5
    storei t5, PayloadOffset + CodeBlock[sp, t3, 8]
    btinz t3, .copyHeaderLoop\n\
    loadi PayloadOffset + ProtoCallFrame::argCountAndCodeOriginValue[protoCallFrame], t4
    subi 1, t4
    loadi ProtoCallFrame::paddedArgCount[protoCallFrame], t5
    subi 1, t5\n\
    bieq t4, t5, .copyArgs
.fillExtraArgsLoop:
    subi 1, t5\n\    storei UndefinedTag, ThisArgumentOffset + 8 + TagOffset[sp, t5, 8]
    storei 0, ThisArgumentOffset + 8 + PayloadOffset[sp, t5, 8]
    bineq t4, t5, .fillExtraArgsLoop\n\
.copyArgs:
    loadp ProtoCallFrame::args[protoCallFrame], t3

.copyArgsLoop:
    btiz t4, .copyArgsDone\n\    subi 1, t4
    loadi TagOffset[t3, t4, 8], t5
    storei t5, ThisArgumentOffset + 8 + TagOffset[sp, t4, 8]
    loadi PayloadOffset[t3, t4, 8], t5
    storei t5, ThisArgumentOffset + 8 + PayloadOffset[sp, t4, 8]
    jmp .copyArgsLoop\n\
.copyArgsDone:
    storep sp, VM::topCallFrame[vm]
    storep cfr, VM::topVMEntryFrame[vm]

    makeCall(entry, t3, t4)\n\
    if ARMv7
        vmEntryRecord(cfr, t3)\n\        move t3, sp
    else
        vmEntryRecord(cfr, sp)
    end\n\
    loadp VMEntryRecord::m_vm[sp], t5
    loadp VMEntryRecord::m_prevTopCallFrame[sp], t4
    storep t4, VM::topCallFrame[t5]
    loadp VMEntryRecord::m_prevTopVMEntryFrame[sp], t4
    storep t4, VM::topVMEntryFrame[t5]\n\
    if ARMv7
        subp cfr, CalleeRegisterSaveSize, t5\n\        move t5, sp
    else
        subp cfr, CalleeRegisterSaveSize, sp
    end\n\
    popCalleeSaves()
    functionEpilogue()
    ret
end\n\
macro makeJavaScriptCall(entry, temp, unused)
    addp CallerFrameAndPCSize, sp
    checkStackPointerAlignment(temp, 0xbad0dc02)
    if C_LOOP\n\        cloopCallJSFunction entry
    else
        call entry
    end
    checkStackPointerAlignment(temp, 0xbad0dc03)
    subp CallerFrameAndPCSize, sp\n\end

macro makeHostFunctionCall(entry, temp1, temp2)
    move entry, temp1
    storep cfr, [sp]
    if C_LOOP
        move sp, a0\n\        storep lr, PtrSize[sp]
        cloopCallNative temp1
    elsif X86 or X86_WIN
        # Put callee frame pointer on stack as arg0, also put it in ecx for "fastcall" targets
        move 0, temp2
        move temp2, 4[sp] # put 0 in ReturnPC\n\        move sp, a0 # a0 is ecx
        push temp2 # Push dummy arg1
        push a0
        call temp1\n\        addp 8, sp
    else
        move sp, a0
        call temp1
    end
end\n\
_handleUncaughtException:
    loadp Callee + PayloadOffset[cfr], t3
    andp MarkedBlockMask, t3
    loadp MarkedBlock::m_vm[t3], t3
    restoreCalleeSavesFromVMEntryFrameCalleeSavesBuffer(t3, t0)
    loadp VM::callFrameForCatch[t3], cfr\n\    storep 0, VM::callFrameForCatch[t3]

    loadp CallerFrame[cfr], cfr

    if ARMv7
        vmEntryRecord(cfr, t3)\n\        move t3, sp
    else
        vmEntryRecord(cfr, sp)
    end\n\
    loadp VMEntryRecord::m_vm[sp], t3
    loadp VMEntryRecord::m_prevTopCallFrame[sp], t5
    storep t5, VM::topCallFrame[t3]
    loadp VMEntryRecord::m_prevTopVMEntryFrame[sp], t5
    storep t5, VM::topVMEntryFrame[t3]\n\
    if ARMv7
        subp cfr, CalleeRegisterSaveSize, t3\n\        move t3, sp
    else
        subp cfr, CalleeRegisterSaveSize, sp
    end\n\
    popCalleeSaves()
    functionEpilogue()
    ret\n\
macro doReturnFromHostFunction(extraStackSpace)
    functionEpilogue(extraStackSpace)
    ret
end\n\
# Debugging operation if you'd like to print an operand in the instruction stream. fromWhere
# should be an immediate integer - any integer you like; use it to identify the place you're
# debugging from. operand should likewise be an immediate, and should identify the operand
# in the instruction stream you'd like to print out.
macro traceOperand(fromWhere, operand)\n\    move fromWhere, a2
    move operand, a3
    move cfr, a0
    move PC, a1
    cCall4(_llint_trace_operand)
    move r0, PC\n\    move r1, cfr
end\n\
# Debugging operation if you'd like to print the value of an operand in the instruction
# stream. Same as traceOperand(), but assumes that the operand is a register, and prints its
# value.
macro traceValue(fromWhere, operand)
    move fromWhere, a2\n\    move operand, a3
    move cfr, a0
    move PC, a1
    cCall4(_llint_trace_value)
    move r0, PC\n\    move r1, cfr
end\n\
# Call a slowPath for call opcodes.
macro callCallSlowPath(slowPath, action)
    storep PC, ArgumentCount + TagOffset[cfr]
    move cfr, a0\n\    move PC, a1
    cCall2(slowPath)
    action(r0, r1)
end\n\
macro callTrapHandler(throwHandler)
    storei PC, ArgumentCount + TagOffset[cfr]
    move cfr, a0\n\    move PC, a1
    cCall2(_llint_slow_path_handle_traps)
    btpnz r0, throwHandler
    loadi ArgumentCount + TagOffset[cfr], PC
end\n\
macro checkSwitchToJITForLoop()
    checkSwitchToJIT(
        1,\n\        macro ()
            storei PC, ArgumentCount + TagOffset[cfr]
            move cfr, a0
            move PC, a1
            cCall2(_llint_loop_osr)
            btpz r0, .recover\n\            move r1, sp
            jmp r0
        .recover:
            loadi ArgumentCount + TagOffset[cfr], PC
        end)
end\n\
macro loadVariable(operand, index, tag, payload)
    loadisFromInstruction(operand, index)
    loadi TagOffset[cfr, index, 8], tag
    loadi PayloadOffset[cfr, index, 8], payload
end\n\
# Index, tag, and payload must be different registers. Index is not
# changed.
macro loadConstantOrVariable(index, tag, payload)
    bigteq index, FirstConstantRegisterIndex, .constant
    loadi TagOffset[cfr, index, 8], tag\n\    loadi PayloadOffset[cfr, index, 8], payload
    jmp .done
.constant:
    loadp CodeBlock[cfr], payload\n\    loadp CodeBlock::m_constantRegisters + VectorBufferOffset[payload], payload
    # There is a bit of evil here: if the index contains a value >= FirstConstantRegisterIndex,
    # then value << 3 will be equal to (value - FirstConstantRegisterIndex) << 3.
    loadp TagOffset[payload, index, 8], tag
    loadp PayloadOffset[payload, index, 8], payload
.done:\n\end

macro loadConstantOrVariableTag(index, tag)
    bigteq index, FirstConstantRegisterIndex, .constant
    loadi TagOffset[cfr, index, 8], tag\n\    jmp .done
.constant:
    loadp CodeBlock[cfr], tag\n\    loadp CodeBlock::m_constantRegisters + VectorBufferOffset[tag], tag
    # There is a bit of evil here: if the index contains a value >= FirstConstantRegisterIndex,
    # then value << 3 will be equal to (value - FirstConstantRegisterIndex) << 3.
    loadp TagOffset[tag, index, 8], tag
.done:
end\n\
# Index and payload may be the same register. Index may be clobbered.
macro loadConstantOrVariable2Reg(index, tag, payload)
    bigteq index, FirstConstantRegisterIndex, .constant
    loadi TagOffset[cfr, index, 8], tag\n\    loadi PayloadOffset[cfr, index, 8], payload
    jmp .done
.constant:
    loadp CodeBlock[cfr], tag\n\    loadp CodeBlock::m_constantRegisters + VectorBufferOffset[tag], tag
    # There is a bit of evil here: if the index contains a value >= FirstConstantRegisterIndex,
    # then value << 3 will be equal to (value - FirstConstantRegisterIndex) << 3.
    lshifti 3, index\n\    addp index, tag
    loadp PayloadOffset[tag], payload
    loadp TagOffset[tag], tag
.done:
end\n\
macro loadConstantOrVariablePayloadTagCustom(index, tagCheck, payload)
    bigteq index, FirstConstantRegisterIndex, .constant
    tagCheck(TagOffset[cfr, index, 8])
    loadi PayloadOffset[cfr, index, 8], payload\n\    jmp .done
.constant:
    loadp CodeBlock[cfr], payload\n\    loadp CodeBlock::m_constantRegisters + VectorBufferOffset[payload], payload
    # There is a bit of evil here: if the index contains a value >= FirstConstantRegisterIndex,
    # then value << 3 will be equal to (value - FirstConstantRegisterIndex) << 3.
    tagCheck(TagOffset[payload, index, 8])
    loadp PayloadOffset[payload, index, 8], payload
.done:\n\end

# Index and payload must be different registers. Index is not mutated. Use
# this if you know what the tag of the variable should be. Doing the tag
# test as part of loading the variable reduces register use, but may not
# be faster than doing loadConstantOrVariable followed by a branch on the\n\# tag.
macro loadConstantOrVariablePayload(index, expectedTag, payload, slow)
    loadConstantOrVariablePayloadTagCustom(
        index,
        macro (actualTag) bineq actualTag, expectedTag, slow end,\n\       payload)
end

macro loadConstantOrVariablePayloadUnchecked(index, payload)
    loadConstantOrVariablePayloadTagCustom(\n\        index,
        macro (actualTag) end,
        payload)
end\n\
macro writeBarrierOnOperand(cellOperand)
    loadisFromInstruction(cellOperand, t1)
    loadConstantOrVariablePayload(t1, CellTag, t2, .writeBarrierDone)
    skipIfIsRememberedOrInEden(
        t2, \n\        macro()
            push cfr, PC
            # We make two extra slots because cCall2 will poke.
            subp 8, sp
            move t2, a1 # t2 can be a0 on x86
            move cfr, a0\n\            cCall2Void(_llint_write_barrier_slow)
            addp 8, sp
            pop PC, cfr
        end)
.writeBarrierDone:
end\n\
macro writeBarrierOnOperands(cellOperand, valueOperand)
    loadisFromInstruction(valueOperand, t1)
    loadConstantOrVariableTag(t1, t0)
    bineq t0, CellTag, .writeBarrierDone\n\
    writeBarrierOnOperand(cellOperand)
.writeBarrierDone:
end\n\
macro writeBarrierOnGlobal(valueOperand, loadHelper)
    loadisFromInstruction(valueOperand, t1)
    loadConstantOrVariableTag(t1, t0)
    bineq t0, CellTag, .writeBarrierDone\n\
    loadHelper(t3)

    skipIfIsRememberedOrInEden(
        t3,\n\        macro()
            push cfr, PC
            # We make two extra slots because cCall2 will poke.
            subp 8, sp
            move cfr, a0\n\            move t3, a1
            cCall2Void(_llint_write_barrier_slow)
            addp 8, sp
            pop PC, cfr\n\        end)
.writeBarrierDone:
end

macro writeBarrierOnGlobalObject(valueOperand)
    writeBarrierOnGlobal(valueOperand,\n\        macro(registerToStoreGlobal)
            loadp CodeBlock[cfr], registerToStoreGlobal
            loadp CodeBlock::m_globalObject[registerToStoreGlobal], registerToStoreGlobal
        end)
end\n\
macro writeBarrierOnGlobalLexicalEnvironment(valueOperand)
    writeBarrierOnGlobal(valueOperand,\n\        macro(registerToStoreGlobal)
            loadp CodeBlock[cfr], registerToStoreGlobal
            loadp CodeBlock::m_globalObject[registerToStoreGlobal], registerToStoreGlobal
            loadp JSGlobalObject::m_globalLexicalEnvironment[registerToStoreGlobal], registerToStoreGlobal
        end)
end\n\
macro valueProfile(tag, payload, operand, scratch)
    loadp operand[PC], scratch
    storei tag, ValueProfile::m_buckets + TagOffset[scratch]
    storei payload, ValueProfile::m_buckets + PayloadOffset[scratch]
end\n\

# Entrypoints into the interpreter\n\
# Expects that CodeBlock is in t1, which is what prologue() leaves behind.
macro functionArityCheck(doneLabel, slowPath)
    loadi PayloadOffset + ArgumentCount[cfr], t0
    biaeq t0, CodeBlock::m_numParameters[t1], doneLabel
    move cfr, a0\n\    move PC, a1
    cCall2(slowPath)   # This slowPath has a simple protocol: t0 = 0 => no error, t0 != 0 => error
    btiz r0, .noError
    move r1, cfr   # r1 contains caller frame
    jmp _llint_throw_from_slow_path_trampoline\n\
.noError:
    # r1 points to ArityCheckData.
    loadp CommonSlowPaths::ArityCheckData::thunkToCall[r1], t3
    btpz t3, .proceedInline\n\
    loadp CommonSlowPaths::ArityCheckData::paddedStackSpace[r1], a0
    call t3
    if ASSERT_ENABLED
        loadp ReturnPC[cfr], t0\n\        loadp [t0], t0
    end
    jmp .continue\n\
.proceedInline:
    loadi CommonSlowPaths::ArityCheckData::paddedStackSpace[r1], t1
    btiz t1, .continue
    loadi PayloadOffset + ArgumentCount[cfr], t2
    addi CallFrameHeaderSlots, t2\n\
    // Check if there are some unaligned slots we can use
    move t1, t3
    andi StackAlignmentSlots - 1, t3\n\    btiz t3, .noExtraSlot
.fillExtraSlots:
    move 0, t0
    storei t0, PayloadOffset[cfr, t2, 8]
    move UndefinedTag, t0
    storei t0, TagOffset[cfr, t2, 8]\n\    addi 1, t2
    bsubinz 1, t3, .fillExtraSlots
    andi ~(StackAlignmentSlots - 1), t1
    btiz t1, .continue\n\
.noExtraSlot:
    // Move frame up t1 slots
    negi t1
    move cfr, t3\n\    move t1, t0
    lshiftp 3, t0
    addp t0, cfr
    addp t0, sp
.copyLoop:
    loadi PayloadOffset[t3], t0\n\    storei t0, PayloadOffset[t3, t1, 8]
    loadi TagOffset[t3], t0
    storei t0, TagOffset[t3, t1, 8]
    addp 8, t3
    bsubinz 1, t2, .copyLoop\n\
    // Fill new slots with JSUndefined
    move t1, t2
.fillLoop:
    move 0, t0\n\    storei t0, PayloadOffset[t3, t1, 8]
    move UndefinedTag, t0
    storei t0, TagOffset[t3, t1, 8]
    addp 8, t3
    baddinz 1, t2, .fillLoop\n\
.continue:
    # Reload CodeBlock and PC, since the slow_path clobbered it.
    loadp CodeBlock[cfr], t1
    loadp CodeBlock::m_instructions[t1], PC
    jmp doneLabel\n\end

macro branchIfException(label)
    loadp Callee + PayloadOffset[cfr], t3
    andp MarkedBlockMask, t3\n\    loadp MarkedBlock::m_vm[t3], t3
    btiz VM::m_exception[t3], .noException
    jmp label
.noException:
end
\n\
# Instruction implementations

_llint_op_enter:
    traceExecution()\n\    checkStackPointerAlignment(t2, 0xdead00e1)
    loadp CodeBlock[cfr], t2                // t2<CodeBlock> = cfr.CodeBlock
    loadi CodeBlock::m_numVars[t2], t2      // t2<size_t> = t2<CodeBlock>.m_numVars
    btiz t2, .opEnterDone
    move UndefinedTag, t0
    move 0, t1\n\    negi t2
.opEnterLoop:
    storei t0, TagOffset[cfr, t2, 8]
    storei t1, PayloadOffset[cfr, t2, 8]
    addi 1, t2\n\    btinz t2, .opEnterLoop
.opEnterDone:
    callOpcodeSlowPath(_slow_path_enter)
    dispatch(constexpr op_enter_length)
\n\
_llint_op_get_argument:
    traceExecution()
    loadisFromInstruction(1, t1)
    loadisFromInstruction(2, t2)\n\    loadi PayloadOffset + ArgumentCount[cfr], t0
    bilteq t0, t2, .opGetArgumentOutOfBounds
    loadi ThisArgumentOffset + TagOffset[cfr, t2, 8], t0
    loadi ThisArgumentOffset + PayloadOffset[cfr, t2, 8], t3
    storei t0, TagOffset[cfr, t1, 8]\n\    storei t3, PayloadOffset[cfr, t1, 8]
    valueProfile(t0, t3, 12, t1)
    dispatch(constexpr op_get_argument_length)\n\
.opGetArgumentOutOfBounds:
    storei UndefinedTag, TagOffset[cfr, t1, 8]
    storei 0, PayloadOffset[cfr, t1, 8]
    valueProfile(UndefinedTag, 0, 12, t1)
    dispatch(constexpr op_get_argument_length)\n\

_llint_op_argument_count:
    traceExecution()
    loadisFromInstruction(1, t2)\n\    loadi PayloadOffset + ArgumentCount[cfr], t0
    subi 1, t0
    move Int32Tag, t1
    storei t1, TagOffset[cfr, t2, 8]
    storei t0, PayloadOffset[cfr, t2, 8]
    dispatch(constexpr op_argument_count_length)\n\

_llint_op_get_scope:
    traceExecution()
    loadi Callee + PayloadOffset[cfr], t0
    loadi JSCallee::m_scope[t0], t0\n\    loadisFromInstruction(1, t1)
    storei CellTag, TagOffset[cfr, t1, 8]
    storei t0, PayloadOffset[cfr, t1, 8]
    dispatch(constexpr op_get_scope_length)\n\

_llint_op_to_this:
    traceExecution()
    loadi 4[PC], t0
    bineq TagOffset[cfr, t0, 8], CellTag, .opToThisSlow\n\    loadi PayloadOffset[cfr, t0, 8], t0
    bbneq JSCell::m_type[t0], FinalObjectType, .opToThisSlow
    loadpFromInstruction(2, t2)
    bpneq JSCell::m_structureID[t0], t2, .opToThisSlow
    dispatch(constexpr op_to_this_length)\n\
.opToThisSlow:
    callOpcodeSlowPath(_slow_path_to_this)
    dispatch(constexpr op_to_this_length)
\n\
_llint_op_check_tdz:
    traceExecution()
    loadisFromInstruction(1, t0)
    loadConstantOrVariableTag(t0, t1)
    bineq t1, EmptyValueTag, .opNotTDZ\n\    callOpcodeSlowPath(_slow_path_throw_tdz_error)

.opNotTDZ:
    dispatch(constexpr op_check_tdz_length)
\n\
_llint_op_mov:
    traceExecution()
    loadi 8[PC], t1\n\    loadi 4[PC], t0
    loadConstantOrVariable(t1, t2, t3)
    storei t2, TagOffset[cfr, t0, 8]
    storei t3, PayloadOffset[cfr, t0, 8]
    dispatch(constexpr op_mov_length)
\n\
_llint_op_not:
    traceExecution()
    loadi 8[PC], t0\n\    loadi 4[PC], t1
    loadConstantOrVariable(t0, t2, t3)
    bineq t2, BooleanTag, .opNotSlow
    xori 1, t3
    storei t2, TagOffset[cfr, t1, 8]\n\    storei t3, PayloadOffset[cfr, t1, 8]
    dispatch(constexpr op_not_length)

.opNotSlow:
    callOpcodeSlowPath(_slow_path_not)
    dispatch(constexpr op_not_length)\n\

_llint_op_eq:
    traceExecution()
    loadi 12[PC], t2
    loadi 8[PC], t0\n\    loadConstantOrVariable(t2, t3, t1)
    loadConstantOrVariable2Reg(t0, t2, t0)
    bineq t2, t3, .opEqSlow
    bieq t2, CellTag, .opEqSlow
    bib t2, LowestTag, .opEqSlow
    loadi 4[PC], t2\n\    cieq t0, t1, t0
    storei BooleanTag, TagOffset[cfr, t2, 8]
    storei t0, PayloadOffset[cfr, t2, 8]
    dispatch(constexpr op_eq_length)\n\
.opEqSlow:
    callOpcodeSlowPath(_slow_path_eq)
    dispatch(constexpr op_eq_length)
\n\
_llint_op_eq_null:
    traceExecution()
    loadi 8[PC], t0
    loadi 4[PC], t3
    assertNotConstant(t0)\n\    loadi TagOffset[cfr, t0, 8], t1
    loadi PayloadOffset[cfr, t0, 8], t0
    bineq t1, CellTag, .opEqNullImmediate
    btbnz JSCell::m_flags[t0], MasqueradesAsUndefined, .opEqNullMasqueradesAsUndefined
    move 0, t1\n\    jmp .opEqNullNotImmediate
.opEqNullMasqueradesAsUndefined:
    loadp JSCell::m_structureID[t0], t1
    loadp CodeBlock[cfr], t0
    loadp CodeBlock::m_globalObject[t0], t0
    cpeq Structure::m_globalObject[t1], t0, t1\n\    jmp .opEqNullNotImmediate
.opEqNullImmediate:
    cieq t1, NullTag, t2
    cieq t1, UndefinedTag, t1\n\    ori t2, t1
.opEqNullNotImmediate:
    storei BooleanTag, TagOffset[cfr, t3, 8]
    storei t1, PayloadOffset[cfr, t3, 8]
    dispatch(constexpr op_eq_null_length)
\n\
_llint_op_neq:
    traceExecution()
    loadi 12[PC], t2
    loadi 8[PC], t0
    loadConstantOrVariable(t2, t3, t1)\n\    loadConstantOrVariable2Reg(t0, t2, t0)
    bineq t2, t3, .opNeqSlow
    bieq t2, CellTag, .opNeqSlow
    bib t2, LowestTag, .opNeqSlow
    loadi 4[PC], t2\n\    cineq t0, t1, t0
    storei BooleanTag, TagOffset[cfr, t2, 8]
    storei t0, PayloadOffset[cfr, t2, 8]
    dispatch(constexpr op_neq_length)\n\
.opNeqSlow:
    callOpcodeSlowPath(_slow_path_neq)
    dispatch(constexpr op_neq_length)
\n\
_llint_op_neq_null:
    traceExecution()
    loadi 8[PC], t0
    loadi 4[PC], t3\n\    assertNotConstant(t0)
    loadi TagOffset[cfr, t0, 8], t1
    loadi PayloadOffset[cfr, t0, 8], t0
    bineq t1, CellTag, .opNeqNullImmediate
    btbnz JSCell::m_flags[t0], MasqueradesAsUndefined, .opNeqNullMasqueradesAsUndefined
    move 1, t1\n\    jmp .opNeqNullNotImmediate
.opNeqNullMasqueradesAsUndefined:
    loadp JSCell::m_structureID[t0], t1
    loadp CodeBlock[cfr], t0
    loadp CodeBlock::m_globalObject[t0], t0
    cpneq Structure::m_globalObject[t1], t0, t1\n\    jmp .opNeqNullNotImmediate
.opNeqNullImmediate:
    cineq t1, NullTag, t2
    cineq t1, UndefinedTag, t1\n\    andi t2, t1
.opNeqNullNotImmediate:
    storei BooleanTag, TagOffset[cfr, t3, 8]
    storei t1, PayloadOffset[cfr, t3, 8]
    dispatch(constexpr op_neq_null_length)\n\

macro strictEq(equalityOperation, slowPath)
    loadi 12[PC], t2\n\    loadi 8[PC], t0
    loadConstantOrVariable(t2, t3, t1)
    loadConstantOrVariable2Reg(t0, t2, t0)
    bineq t2, t3, .slow\n\    bib t2, LowestTag, .slow
    bineq t2, CellTag, .notStringOrSymbol
    bbaeq JSCell::m_type[t0], ObjectType, .notStringOrSymbol
    bbb JSCell::m_type[t1], ObjectType, .slow
.notStringOrSymbol:
    loadi 4[PC], t2\n\    equalityOperation(t0, t1, t0)
    storei BooleanTag, TagOffset[cfr, t2, 8]
    storei t0, PayloadOffset[cfr, t2, 8]
    dispatch(4)\n\
.slow:
    callOpcodeSlowPath(slowPath)
    dispatch(4)
end\n\
_llint_op_stricteq:
    traceExecution()
    strictEq(macro (left, right, result) cieq left, right, result end, _slow_path_stricteq)
\n\
_llint_op_nstricteq:
    traceExecution()
    strictEq(macro (left, right, result) cineq left, right, result end, _slow_path_nstricteq)
\n\
_llint_op_inc:
    traceExecution()
    loadi 4[PC], t0
    bineq TagOffset[cfr, t0, 8], Int32Tag, .opIncSlow
    loadi PayloadOffset[cfr, t0, 8], t1\n\    baddio 1, t1, .opIncSlow
    storei t1, PayloadOffset[cfr, t0, 8]
    dispatch(constexpr op_inc_length)\n\
.opIncSlow:
    callOpcodeSlowPath(_slow_path_inc)
    dispatch(constexpr op_inc_length)
\n\
_llint_op_dec:
    traceExecution()
    loadi 4[PC], t0
    bineq TagOffset[cfr, t0, 8], Int32Tag, .opDecSlow
    loadi PayloadOffset[cfr, t0, 8], t1
    bsubio 1, t1, .opDecSlow\n\    storei t1, PayloadOffset[cfr, t0, 8]
    dispatch(constexpr op_dec_length)

.opDecSlow:
    callOpcodeSlowPath(_slow_path_dec)
    dispatch(constexpr op_dec_length)\n\

_llint_op_to_number:
    traceExecution()
    loadi 8[PC], t0\n\    loadi 4[PC], t1
    loadConstantOrVariable(t0, t2, t3)
    bieq t2, Int32Tag, .opToNumberIsInt
    biaeq t2, LowestTag, .opToNumberSlow
.opToNumberIsInt:
    storei t2, TagOffset[cfr, t1, 8]\n\    storei t3, PayloadOffset[cfr, t1, 8]
    valueProfile(t2, t3, 12, t1)
    dispatch(constexpr op_to_number_length)\n\
.opToNumberSlow:
    callOpcodeSlowPath(_slow_path_to_number)
    dispatch(constexpr op_to_number_length)
\n\
_llint_op_to_string:
    traceExecution()
    loadi 8[PC], t0\n\    loadi 4[PC], t1
    loadConstantOrVariable(t0, t2, t3)
    bineq t2, CellTag, .opToStringSlow
    bbneq JSCell::m_type[t3], StringType, .opToStringSlow
.opToStringIsString:
    storei t2, TagOffset[cfr, t1, 8]\n\    storei t3, PayloadOffset[cfr, t1, 8]
    dispatch(constexpr op_to_string_length)

.opToStringSlow:
    callOpcodeSlowPath(_slow_path_to_string)
    dispatch(constexpr op_to_string_length)\n\

_llint_op_negate:
    traceExecution()
    loadi 8[PC], t0\n\    loadi 4[PC], t3
    loadConstantOrVariable(t0, t1, t2)
    loadisFromInstruction(3, t0)
    bineq t1, Int32Tag, .opNegateSrcNotInt
    btiz t2, 0x7fffffff, .opNegateSlow
    negi t2\n\    ori ArithProfileInt, t0
    storei Int32Tag, TagOffset[cfr, t3, 8]
    storeisToInstruction(t0, 3)
    storei t2, PayloadOffset[cfr, t3, 8]\n\    dispatch(constexpr op_negate_length)
.opNegateSrcNotInt:
    bia t1, LowestTag, .opNegateSlow
    xori 0x80000000, t1
    ori ArithProfileNumber, t0
    storei t2, PayloadOffset[cfr, t3, 8]\n\    storeisToInstruction(t0, 3)
    storei t1, TagOffset[cfr, t3, 8]
    dispatch(constexpr op_negate_length)

.opNegateSlow:
    callOpcodeSlowPath(_slow_path_negate)
    dispatch(constexpr op_negate_length)\n\

macro binaryOpCustomStore(integerOperationAndStore, doubleOperation, slowPath)
    loadi 12[PC], t2\n\    loadi 8[PC], t0
    loadConstantOrVariable(t2, t3, t1)
    loadConstantOrVariable2Reg(t0, t2, t0)
    bineq t2, Int32Tag, .op1NotInt\n\    bineq t3, Int32Tag, .op2NotInt
    loadisFromInstruction(4, t5)
    ori ArithProfileIntInt, t5
    storeisToInstruction(t5, 4)
    loadi 4[PC], t2\n\    integerOperationAndStore(t3, t1, t0, .slow, t2)
    dispatch(5)

.op1NotInt:
    # First operand is definitely not an int, the second operand could be anything.
    bia t2, LowestTag, .slow\n\    bib t3, LowestTag, .op1NotIntOp2Double
    bineq t3, Int32Tag, .slow
    loadisFromInstruction(4, t5)
    ori ArithProfileNumberInt, t5
    storeisToInstruction(t5, 4)
    ci2d t1, ft1\n\    jmp .op1NotIntReady
.op1NotIntOp2Double:
    fii2d t1, t3, ft1
    loadisFromInstruction(4, t5)
    ori ArithProfileNumberNumber, t5\n\    storeisToInstruction(t5, 4)
.op1NotIntReady:
    loadi 4[PC], t1
    fii2d t0, t2, ft0
    doubleOperation(ft1, ft0)
    stored ft0, [cfr, t1, 8]\n\    dispatch(5)

.op2NotInt:
    # First operand is definitely an int, the second operand is definitely not.
    loadi 4[PC], t2\n\    bia t3, LowestTag, .slow
    loadisFromInstruction(4, t5)
    ori ArithProfileIntNumber, t5
    storeisToInstruction(t5, 4)
    ci2d t0, ft0\n\    fii2d t1, t3, ft1
    doubleOperation(ft1, ft0)
    stored ft0, [cfr, t2, 8]
    dispatch(5)\n\
.slow:
    callOpcodeSlowPath(slowPath)
    dispatch(5)
end\n\
macro binaryOp(integerOperation, doubleOperation, slowPath)
    binaryOpCustomStore(\n\        macro (int32Tag, left, right, slow, index)
            integerOperation(left, right, slow)
            storei int32Tag, TagOffset[cfr, index, 8]
            storei right, PayloadOffset[cfr, index, 8]
        end,\n\        doubleOperation, slowPath)
end

_llint_op_add:
    traceExecution()\n\    binaryOp(
        macro (left, right, slow) baddio left, right, slow end,
        macro (left, right) addd left, right end,
        _slow_path_add)
\n\
_llint_op_mul:
    traceExecution()
    binaryOpCustomStore(\n\        macro (int32Tag, left, right, slow, index)
            const scratch = int32Tag   # We know that we can reuse the int32Tag register since it has a constant.
            move right, scratch
            bmulio left, scratch, slow
            btinz scratch, .done
            bilt left, 0, slow\n\            bilt right, 0, slow
        .done:
            storei Int32Tag, TagOffset[cfr, index, 8]
            storei scratch, PayloadOffset[cfr, index, 8]
        end,
        macro (left, right) muld left, right end,\n\        _slow_path_mul)


_llint_op_sub:
    traceExecution()\n\    binaryOp(
        macro (left, right, slow) bsubio left, right, slow end,
        macro (left, right) subd left, right end,
        _slow_path_sub)
\n\
_llint_op_div:
    traceExecution()
    binaryOpCustomStore(
        macro (int32Tag, left, right, slow, index)
            ci2d left, ft0\n\            ci2d right, ft1
            divd ft0, ft1
            bcd2i ft1, right, .notInt
            storei int32Tag, TagOffset[cfr, index, 8]
            storei right, PayloadOffset[cfr, index, 8]\n\            jmp .done
        .notInt:
            stored ft1, [cfr, index, 8]
        .done:
        end,
        macro (left, right) divd left, right end,\n\        _slow_path_div)


macro bitOp(operation, slowPath, advance)
    loadi 12[PC], t2\n\    loadi 8[PC], t0
    loadConstantOrVariable(t2, t3, t1)
    loadConstantOrVariable2Reg(t0, t2, t0)
    bineq t3, Int32Tag, .slow
    bineq t2, Int32Tag, .slow
    loadi 4[PC], t2\n\    operation(t1, t0)
    storei t3, TagOffset[cfr, t2, 8]
    storei t0, PayloadOffset[cfr, t2, 8]
    dispatch(advance)\n\
.slow:
    callOpcodeSlowPath(slowPath)
    dispatch(advance)
end\n\
_llint_op_lshift:
    traceExecution()
    bitOp(
        macro (left, right) lshifti left, right end,
        _slow_path_lshift,\n\        constexpr op_lshift_length)


_llint_op_rshift:
    traceExecution()\n\    bitOp(
        macro (left, right) rshifti left, right end,
        _slow_path_rshift,
        constexpr op_rshift_length)
\n\
_llint_op_urshift:
    traceExecution()
    bitOp(
        macro (left, right) urshifti left, right end,
        _slow_path_urshift,\n\        constexpr op_urshift_length)


_llint_op_unsigned:
    traceExecution()
    loadi 4[PC], t0\n\    loadi 8[PC], t1
    loadConstantOrVariablePayload(t1, Int32Tag, t2, .opUnsignedSlow)
    bilt t2, 0, .opUnsignedSlow
    storei t2, PayloadOffset[cfr, t0, 8]
    storei Int32Tag, TagOffset[cfr, t0, 8]\n\    dispatch(constexpr op_unsigned_length)
.opUnsignedSlow:
    callOpcodeSlowPath(_slow_path_unsigned)
    dispatch(constexpr op_unsigned_length)
\n\
_llint_op_bitand:
    traceExecution()
    bitOp(
        macro (left, right) andi left, right end,
        _slow_path_bitand,\n\        constexpr op_bitand_length)


_llint_op_bitxor:
    traceExecution()\n\    bitOp(
        macro (left, right) xori left, right end,
        _slow_path_bitxor,
        constexpr op_bitxor_length)\n\

_llint_op_bitor:
    traceExecution()
    bitOp(
        macro (left, right) ori left, right end,\n\        _slow_path_bitor,
        constexpr op_bitor_length)


_llint_op_overrides_has_instance:
    traceExecution()\n\
    loadisFromInstruction(1, t3)
    storei BooleanTag, TagOffset[cfr, t3, 8]\n\
    # First check if hasInstanceValue is the one on Function.prototype[Symbol.hasInstance]
    loadisFromInstruction(3, t0)
    loadConstantOrVariablePayload(t0, CellTag, t2, .opOverrideshasInstanceValueNotCell)
    loadConstantOrVariable(t0, t1, t2)
    bineq t1, CellTag, .opOverrideshasInstanceValueNotCell\n\
    # We don't need hasInstanceValue's tag register anymore.
    loadp CodeBlock[cfr], t1
    loadp CodeBlock::m_globalObject[t1], t1
    loadp JSGlobalObject::m_functionProtoHasInstanceSymbolFunction[t1], t1
    bineq t1, t2, .opOverrideshasInstanceValueNotDefault\n\
    # We know the constructor is a cell.
    loadisFromInstruction(2, t0)
    loadConstantOrVariablePayloadUnchecked(t0, t1)
    tbz JSCell::m_flags[t1], ImplementsDefaultHasInstance, t0
    storei t0, PayloadOffset[cfr, t3, 8]\n\    dispatch(constexpr op_overrides_has_instance_length)

.opOverrideshasInstanceValueNotCell:
.opOverrideshasInstanceValueNotDefault:
    storei 1, PayloadOffset[cfr, t3, 8]
    dispatch(constexpr op_overrides_has_instance_length)\n\
_llint_op_instanceof_custom:
    traceExecution()
    callOpcodeSlowPath(_llint_slow_path_instanceof_custom)
    dispatch(constexpr op_instanceof_custom_length)\n\

_llint_op_is_empty:
    traceExecution()
    loadi 8[PC], t1\n\    loadi 4[PC], t0
    loadConstantOrVariable(t1, t2, t3)
    cieq t2, EmptyValueTag, t3
    storei BooleanTag, TagOffset[cfr, t0, 8]
    storei t3, PayloadOffset[cfr, t0, 8]
    dispatch(constexpr op_is_empty_length)\n\

_llint_op_is_undefined:
    traceExecution()
    loadi 8[PC], t1\n\    loadi 4[PC], t0
    loadConstantOrVariable(t1, t2, t3)
    storei BooleanTag, TagOffset[cfr, t0, 8]
    bieq t2, CellTag, .opIsUndefinedCell
    cieq t2, UndefinedTag, t3\n\    storei t3, PayloadOffset[cfr, t0, 8]
    dispatch(constexpr op_is_undefined_length)
.opIsUndefinedCell:
    btbnz JSCell::m_flags[t3], MasqueradesAsUndefined, .opIsUndefinedMasqueradesAsUndefined
    move 0, t1\n\    storei t1, PayloadOffset[cfr, t0, 8]
    dispatch(constexpr op_is_undefined_length)
.opIsUndefinedMasqueradesAsUndefined:
    loadp JSCell::m_structureID[t3], t1
    loadp CodeBlock[cfr], t3\n\    loadp CodeBlock::m_globalObject[t3], t3
    cpeq Structure::m_globalObject[t1], t3, t1
    storei t1, PayloadOffset[cfr, t0, 8]
    dispatch(constexpr op_is_undefined_length)
\n\
_llint_op_is_boolean:
    traceExecution()
    loadi 8[PC], t1
    loadi 4[PC], t2
    loadConstantOrVariableTag(t1, t0)\n\    cieq t0, BooleanTag, t0
    storei BooleanTag, TagOffset[cfr, t2, 8]
    storei t0, PayloadOffset[cfr, t2, 8]
    dispatch(constexpr op_is_boolean_length)
\n\
_llint_op_is_number:
    traceExecution()
    loadi 8[PC], t1
    loadi 4[PC], t2
    loadConstantOrVariableTag(t1, t0)\n\    storei BooleanTag, TagOffset[cfr, t2, 8]
    addi 1, t0
    cib t0, LowestTag + 1, t1
    storei t1, PayloadOffset[cfr, t2, 8]
    dispatch(constexpr op_is_number_length)
\n\
_llint_op_is_cell_with_type:
    traceExecution()
    loadi 8[PC], t1
    loadi 4[PC], t2
    loadConstantOrVariable(t1, t0, t3)\n\    storei BooleanTag, TagOffset[cfr, t2, 8]
    bineq t0, CellTag, .notCellCase
    loadi 12[PC], t0
    cbeq JSCell::m_type[t3], t0, t1
    storei t1, PayloadOffset[cfr, t2, 8]\n\    dispatch(constexpr op_is_cell_with_type_length)
.notCellCase:
    storep 0, PayloadOffset[cfr, t2, 8]
    dispatch(constexpr op_is_cell_with_type_length)
\n\
_llint_op_is_object:
    traceExecution()
    loadi 8[PC], t1
    loadi 4[PC], t2\n\    loadConstantOrVariable(t1, t0, t3)
    storei BooleanTag, TagOffset[cfr, t2, 8]
    bineq t0, CellTag, .opIsObjectNotCell
    cbaeq JSCell::m_type[t3], ObjectType, t1
    storei t1, PayloadOffset[cfr, t2, 8]\n\    dispatch(constexpr op_is_object_length)
.opIsObjectNotCell:
    storep 0, PayloadOffset[cfr, t2, 8]
    dispatch(constexpr op_is_object_length)
\n\
macro loadPropertyAtVariableOffsetKnownNotInline(propertyOffset, objectAndStorage, tag, payload)
    assert(macro (ok) bigteq propertyOffset, firstOutOfLineOffset, ok end)\n\    negi propertyOffset
    loadp JSObject::m_butterfly[objectAndStorage], objectAndStorage
    loadi TagOffset + (firstOutOfLineOffset - 2) * 8[objectAndStorage, propertyOffset, 8], tag
    loadi PayloadOffset + (firstOutOfLineOffset - 2) * 8[objectAndStorage, propertyOffset, 8], payload
end\n\
macro loadPropertyAtVariableOffset(propertyOffset, objectAndStorage, tag, payload)
    bilt propertyOffset, firstOutOfLineOffset, .isInline
    loadp JSObject::m_butterfly[objectAndStorage], objectAndStorage
    negi propertyOffset\n\    jmp .ready
.isInline:
    addp sizeof JSObject - (firstOutOfLineOffset - 2) * 8, objectAndStorage
.ready:
    loadi TagOffset + (firstOutOfLineOffset - 2) * 8[objectAndStorage, propertyOffset, 8], tag
    loadi PayloadOffset + (firstOutOfLineOffset - 2) * 8[objectAndStorage, propertyOffset, 8], payload\n\end

macro storePropertyAtVariableOffset(propertyOffsetAsInt, objectAndStorage, tag, payload)
    bilt propertyOffsetAsInt, firstOutOfLineOffset, .isInline
    loadp JSObject::m_butterfly[objectAndStorage], objectAndStorage
    negi propertyOffsetAsInt\n\    jmp .ready
.isInline:
    addp sizeof JSObject - (firstOutOfLineOffset - 2) * 8, objectAndStorage
.ready:
    storei tag, TagOffset + (firstOutOfLineOffset - 2) * 8[objectAndStorage, propertyOffsetAsInt, 8]
    storei payload, PayloadOffset + (firstOutOfLineOffset - 2) * 8[objectAndStorage, propertyOffsetAsInt, 8]\n\end


# We only do monomorphic get_by_id caching for now, and we do not modify the
# opcode for own properties. We also allow for the cache to change anytime it fails,
# since ping-ponging is free.\
At best we get lucky and the get_by_id will continue
# to take fast path on the new cache. At worst we take slow path, which is what
# we would have been doing anyway. For prototype/unset properties, we will attempt to
# convert opcode into a get_by_id_proto_load/get_by_id_unset, respectively, after an
# execution counter hits zero.\n\
_llint_op_get_by_id:
    traceExecution()
    loadi 8[PC], t0
    loadi 16[PC], t1
    loadConstantOrVariablePayload(t0, CellTag, t3, .opGetByIdSlow)\n\    loadi 20[PC], t2
    bineq JSCell::m_structureID[t3], t1, .opGetByIdSlow
    loadPropertyAtVariableOffset(t2, t3, t0, t1)
    loadi 4[PC], t2
    storei t0, TagOffset[cfr, t2, 8]\n\    storei t1, PayloadOffset[cfr, t2, 8]
    valueProfile(t0, t1, 32, t2)
    dispatch(constexpr op_get_by_id_length)\n\
.opGetByIdSlow:
    callOpcodeSlowPath(_llint_slow_path_get_by_id)
    dispatch(constexpr op_get_by_id_length)
\n\
_llint_op_get_by_id_proto_load:
    traceExecution()
    loadi 8[PC], t0
    loadi 16[PC], t1
    loadConstantOrVariablePayload(t0, CellTag, t3, .opGetByIdProtoSlow)\n\    loadi 20[PC], t2
    bineq JSCell::m_structureID[t3], t1, .opGetByIdProtoSlow
    loadpFromInstruction(6, t3)
    loadPropertyAtVariableOffset(t2, t3, t0, t1)
    loadi 4[PC], t2\n\    storei t0, TagOffset[cfr, t2, 8]
    storei t1, PayloadOffset[cfr, t2, 8]
    valueProfile(t0, t1, 32, t2)
    dispatch(constexpr op_get_by_id_proto_load_length)\n\
.opGetByIdProtoSlow:
    callOpcodeSlowPath(_llint_slow_path_get_by_id)
    dispatch(constexpr op_get_by_id_proto_load_length)
\n\
_llint_op_get_by_id_unset:
    traceExecution()
    loadi 8[PC], t0
    loadi 16[PC], t1\n\    loadConstantOrVariablePayload(t0, CellTag, t3, .opGetByIdUnsetSlow)
    bineq JSCell::m_structureID[t3], t1, .opGetByIdUnsetSlow
    loadi 4[PC], t2
    storei UndefinedTag, TagOffset[cfr, t2, 8]
    storei 0, PayloadOffset[cfr, t2, 8]\n\    valueProfile(UndefinedTag, 0, 32, t2)
    dispatch(constexpr op_get_by_id_unset_length)

.opGetByIdUnsetSlow:
    callOpcodeSlowPath(_llint_slow_path_get_by_id)
    dispatch(constexpr op_get_by_id_unset_length)\n\

_llint_op_get_array_length:
    traceExecution()
    loadi 8[PC], t0\n\    loadp 16[PC], t1
    loadConstantOrVariablePayload(t0, CellTag, t3, .opGetArrayLengthSlow)
    move t3, t2
    arrayProfile(t2, t1, t0)
    btiz t2, IsArray, .opGetArrayLengthSlow
    btiz t2, IndexingShapeMask, .opGetArrayLengthSlow\n\    loadi 4[PC], t1
    loadp JSObject::m_butterfly[t3], t0
    loadi -sizeof IndexingHeader + IndexingHeader::u.lengths.publicLength[t0], t0
    bilt t0, 0, .opGetArrayLengthSlow
    valueProfile(Int32Tag, t0, 32, t2)
    storep t0, PayloadOffset[cfr, t1, 8]\n\    storep Int32Tag, TagOffset[cfr, t1, 8]
    dispatch(constexpr op_get_array_length_length)

.opGetArrayLengthSlow:
    callOpcodeSlowPath(_llint_slow_path_get_by_id)
    dispatch(constexpr op_get_array_length_length)\n\

_llint_op_put_by_id:
    traceExecution()
    writeBarrierOnOperands(1, 3)\n\    loadi 4[PC], t3
    loadConstantOrVariablePayload(t3, CellTag, t0, .opPutByIdSlow)
    loadi JSCell::m_structureID[t0], t2
    bineq t2, 16[PC], .opPutByIdSlow\n\
    # At this point, we have:
    # t2 -> currentStructureID
    # t0 -> object base
    # We will lose currentStructureID in the shenanigans below.\n\
    loadi 12[PC], t1
    loadConstantOrVariable(t1, t2, t3)
    loadi 32[PC], t1\n\
    # At this point, we have:
    # t0 -> object base
    # t1 -> put by id flags
    # t2 -> value tag
    # t3 -> value payload\n\
    btinz t1, PutByIdPrimaryTypeMask, .opPutByIdTypeCheckObjectWithStructureOrOther

    # We have one of the non-structure type checks. Find out which one.
    andi PutByIdSecondaryTypeMask, t1
    bilt t1, PutByIdSecondaryTypeString, .opPutByIdTypeCheckLessThanString\n\
    # We are one of the following: String, Symbol, Object, ObjectOrOther, Top
    bilt t1, PutByIdSecondaryTypeObjectOrOther, .opPutByIdTypeCheckLessThanObjectOrOther

    # We are either ObjectOrOther or Top.
    bieq t1, PutByIdSecondaryTypeTop, .opPutByIdDoneCheckingTypes\n\
    # Check if we are ObjectOrOther.
    bieq t2, CellTag, .opPutByIdTypeCheckObject
.opPutByIdTypeCheckOther:
    bieq t2, NullTag, .opPutByIdDoneCheckingTypes
    bieq t2, UndefinedTag, .opPutByIdDoneCheckingTypes\n\    jmp .opPutByIdSlow

.opPutByIdTypeCheckLessThanObjectOrOther:
    # We are either String, Symbol or Object.
    bineq t2, CellTag, .opPutByIdSlow\n\    bieq t1, PutByIdSecondaryTypeObject, .opPutByIdTypeCheckObject
    bieq t1, PutByIdSecondaryTypeSymbol, .opPutByIdTypeCheckSymbol
    bbeq JSCell::m_type[t3], StringType, .opPutByIdDoneCheckingTypes
    jmp .opPutByIdSlow
.opPutByIdTypeCheckObject:
    bbaeq JSCell::m_type[t3], ObjectType, .opPutByIdDoneCheckingTypes\n\    jmp .opPutByIdSlow
.opPutByIdTypeCheckSymbol:
    bbeq JSCell::m_type[t3], SymbolType, .opPutByIdDoneCheckingTypes
    jmp .opPutByIdSlow\n\
.opPutByIdTypeCheckLessThanString:
    # We are one of the following: Bottom, Boolean, Other, Int32, Number.
    bilt t1, PutByIdSecondaryTypeInt32, .opPutByIdTypeCheckLessThanInt32\n\
    # We are either Int32 or Number.
    bieq t1, PutByIdSecondaryTypeNumber, .opPutByIdTypeCheckNumber

    bieq t2, Int32Tag, .opPutByIdDoneCheckingTypes
    jmp .opPutByIdSlow\n\
.opPutByIdTypeCheckNumber:
    bib t2, LowestTag + 1, .opPutByIdDoneCheckingTypes
    jmp .opPutByIdSlow\n\
.opPutByIdTypeCheckLessThanInt32:
    # We are one of the following: Bottom, Boolean, Other
    bineq t1, PutByIdSecondaryTypeBoolean, .opPutByIdTypeCheckBottomOrOther
    bieq t2, BooleanTag, .opPutByIdDoneCheckingTypes
    jmp .opPutByIdSlow\n\
.opPutByIdTypeCheckBottomOrOther:
    bieq t1, PutByIdSecondaryTypeOther, .opPutByIdTypeCheckOther
    jmp .opPutByIdSlow\n\
.opPutByIdTypeCheckObjectWithStructureOrOther:
    bieq t2, CellTag, .opPutByIdTypeCheckObjectWithStructure
    btinz t1, PutByIdPrimaryTypeObjectWithStructureOrOther, .opPutByIdTypeCheckOther
    jmp .opPutByIdSlow\n\
.opPutByIdTypeCheckObjectWithStructure:
    andi PutByIdSecondaryTypeMask, t1
    bineq t1, JSCell::m_structureID[t3], .opPutByIdSlow\n\
.opPutByIdDoneCheckingTypes:
    loadi 24[PC], t1

    btiz t1, .opPutByIdNotTransition\n\
    # This is the transition case. t1 holds the new Structure*. If we have a chain, we need to
    # check it. t0 is the base. We may clobber t1 to use it as scratch.
    loadp 28[PC], t3
    btpz t3, .opPutByIdTransitionDirect\n\
    loadi 16[PC], t2 # Need old structure again.
    loadp StructureChain::m_vector[t3], t3
    assert(macro (ok) btpnz t3, ok end)\n\
    loadp Structure::m_prototype[t2], t2
    btpz t2, .opPutByIdTransitionChainDone
.opPutByIdTransitionChainLoop:
    loadp [t3], t1\n\    bpneq t1, JSCell::m_structureID[t2], .opPutByIdSlow
    addp 4, t3
    loadp Structure::m_prototype[t1], t2
    btpnz t2, .opPutByIdTransitionChainLoop\n\
.opPutByIdTransitionChainDone:
    loadi 24[PC], t1\n\
.opPutByIdTransitionDirect:
    storei t1, JSCell::m_structureID[t0]
    loadi 12[PC], t1
    loadConstantOrVariable(t1, t2, t3)\n\    loadi 20[PC], t1
    storePropertyAtVariableOffset(t1, t0, t2, t3)
    writeBarrierOnOperand(1)
    dispatch(constexpr op_put_by_id_length)\n\
.opPutByIdNotTransition:
    # The only thing live right now is t0, which holds the base.
    loadi 12[PC], t1
    loadConstantOrVariable(t1, t2, t3)
    loadi 20[PC], t1\n\    storePropertyAtVariableOffset(t1, t0, t2, t3)
    dispatch(constexpr op_put_by_id_length)

.opPutByIdSlow:
    callOpcodeSlowPath(_llint_slow_path_put_by_id)
    dispatch(constexpr op_put_by_id_length)\n\

_llint_op_get_by_val:
    traceExecution()
    loadi 8[PC], t2
    loadConstantOrVariablePayload(t2, CellTag, t0, .opGetByValSlow)\n\    move t0, t2
    loadp 16[PC], t3
    arrayProfile(t2, t3, t1)
    loadi 12[PC], t3
    loadConstantOrVariablePayload(t3, Int32Tag, t1, .opGetByValSlow)
    loadp JSObject::m_butterfly[t0], t3\n\    andi IndexingShapeMask, t2
    bieq t2, Int32Shape, .opGetByValIsContiguous
    bineq t2, ContiguousShape, .opGetByValNotContiguous
.opGetByValIsContiguous:\n\
    biaeq t1, -sizeof IndexingHeader + IndexingHeader::u.lengths.publicLength[t3], .opGetByValOutOfBounds
    loadi TagOffset[t3, t1, 8], t2
    loadi PayloadOffset[t3, t1, 8], t1
    jmp .opGetByValDone\n\
.opGetByValNotContiguous:
    bineq t2, DoubleShape, .opGetByValNotDouble
    biaeq t1, -sizeof IndexingHeader + IndexingHeader::u.lengths.publicLength[t3], .opGetByValOutOfBounds
    loadd [t3, t1, 8], ft0
    bdnequn ft0, ft0, .opGetByValSlow\n\    # FIXME: This could be massively optimized.
    fd2ii ft0, t1, t2
    loadi 4[PC], t0
    jmp .opGetByValNotEmpty\n\
.opGetByValNotDouble:
    subi ArrayStorageShape, t2
    bia t2, SlowPutArrayStorageShape - ArrayStorageShape, .opGetByValSlow
    biaeq t1, -sizeof IndexingHeader + IndexingHeader::u.lengths.vectorLength[t3], \
    .opGetByValOutOfBounds
    loadi ArrayStorage::m_vector + TagOffset[t3, t1, 8], t2
    loadi ArrayStorage::m_vector + PayloadOffset[t3, t1, 8], t1

.opGetByValDone:
    loadi 4[PC], t0\n\    bieq t2, EmptyValueTag, .opGetByValOutOfBounds
.opGetByValNotEmpty:
    storei t2, TagOffset[cfr, t0, 8]
    storei t1, PayloadOffset[cfr, t0, 8]
    valueProfile(t2, t1, 20, t0)
    dispatch(constexpr op_get_by_val_length)\n\
.opGetByValOutOfBounds:
    loadpFromInstruction(4, t0)
    storeb 1, ArrayProfile::m_outOfBounds[t0]
.opGetByValSlow:
    callOpcodeSlowPath(_llint_slow_path_get_by_val)\n\    dispatch(constexpr op_get_by_val_length)
\n\
macro contiguousPutByVal(storeCallback)
    biaeq t3, -sizeof IndexingHeader + IndexingHeader::u.lengths.publicLength[t0], .outOfBounds
.storeResult:
    loadi 12[PC], t2
    storeCallback(t2, t1, t0, t3)\n\    dispatch(5)

.outOfBounds:
    biaeq t3, -sizeof IndexingHeader + IndexingHeader::u.lengths.vectorLength[t0], .opPutByValOutOfBounds
    loadp 16[PC], t2\n\    storeb 1, ArrayProfile::m_mayStoreToHole[t2]
    addi 1, t3, t2
    storei t2, -sizeof IndexingHeader + IndexingHeader::u.lengths.publicLength[t0]
    jmp .storeResult
end\n\
macro putByVal(slowPath)
    traceExecution()
    writeBarrierOnOperands(1, 3)
    loadi 4[PC], t0\n\    loadConstantOrVariablePayload(t0, CellTag, t1, .opPutByValSlow)
    move t1, t2
    loadp 16[PC], t3
    arrayProfile(t2, t3, t0)
    loadi 8[PC], t0\n\    loadConstantOrVariablePayload(t0, Int32Tag, t3, .opPutByValSlow)
    loadp JSObject::m_butterfly[t1], t0
    andi IndexingShapeMask, t2
    bineq t2, Int32Shape, .opPutByValNotInt32
    contiguousPutByVal(\n\        macro (operand, scratch, base, index)
            loadConstantOrVariablePayload(operand, Int32Tag, scratch, .opPutByValSlow)
            storei Int32Tag, TagOffset[base, index, 8]
            storei scratch, PayloadOffset[base, index, 8]
        end)\n\
.opPutByValNotInt32:
    bineq t2, DoubleShape, .opPutByValNotDouble
    contiguousPutByVal(
        macro (operand, scratch, base, index)\n\            const tag = scratch
            const payload = operand
            loadConstantOrVariable2Reg(operand, tag, payload)
            bineq tag, Int32Tag, .notInt
            ci2d payload, ft0\n\            jmp .ready
        .notInt:
            fii2d payload, tag, ft0
            bdnequn ft0, ft0, .opPutByValSlow
        .ready:
            stored ft0, [base, index, 8]\n\        end)

.opPutByValNotDouble:
    bineq t2, ContiguousShape, .opPutByValNotContiguous
    contiguousPutByVal(\n\        macro (operand, scratch, base, index)
            const tag = scratch
            const payload = operand
            loadConstantOrVariable2Reg(operand, tag, payload)
            storei tag, TagOffset[base, index, 8]
            storei payload, PayloadOffset[base, index, 8]\n\        end)

.opPutByValNotContiguous:
    bineq t2, ArrayStorageShape, .opPutByValSlow
    biaeq t3, -sizeof IndexingHeader + IndexingHeader::u.lengths.vectorLength[t0], .opPutByValOutOfBounds
    bieq ArrayStorage::m_vector + TagOffset[t0, t3, 8], \
    EmptyValueTag, .opPutByValArrayStorageEmpty
.opPutByValArrayStorageStoreResult:
    loadi 12[PC], t2
    loadConstantOrVariable2Reg(t2, t1, t2)
    storei t1, ArrayStorage::m_vector + TagOffset[t0, t3, 8]
    storei t2, ArrayStorage::m_vector + PayloadOffset[t0, t3, 8]\n\    dispatch(5)

.opPutByValArrayStorageEmpty:
    loadp 16[PC], t1
    storeb 1, ArrayProfile::m_mayStoreToHole[t1]\n\    addi 1, ArrayStorage::m_numValuesInVector[t0]
    bib t3, -sizeof IndexingHeader + IndexingHeader::u.lengths.publicLength[t0], .opPutByValArrayStorageStoreResult
    addi 1, t3, t1
    storei t1, -sizeof IndexingHeader + IndexingHeader::u.lengths.publicLength[t0]
    jmp .opPutByValArrayStorageStoreResult\n\
.opPutByValOutOfBounds:
    loadpFromInstruction(4, t0)
    storeb 1, ArrayProfile::m_outOfBounds[t0]
.opPutByValSlow:
    callOpcodeSlowPath(slowPath)\n\    dispatch(5)
end

_llint_op_put_by_val:
    putByVal(_llint_slow_path_put_by_val)\n\
_llint_op_put_by_val_direct:
    putByVal(_llint_slow_path_put_by_val_direct)\n\
_llint_op_jmp:
    traceExecution()
    dispatchBranch(4[PC])
\n\
macro jumpTrueOrFalse(conditionOp, slow)
    loadi 4[PC], t1
    loadConstantOrVariablePayload(t1, BooleanTag, t0, .slow)
    conditionOp(t0, .target)
    dispatch(3)\n\
.target:
    dispatchBranch(8[PC])\n\
.slow:
    callOpcodeSlowPath(slow)
    dispatch(0)
end
\n\
macro equalNull(cellHandler, immediateHandler)
    loadi 4[PC], t0
    assertNotConstant(t0)
    loadi TagOffset[cfr, t0, 8], t1
    loadi PayloadOffset[cfr, t0, 8], t0\n\    bineq t1, CellTag, .immediate
    loadp JSCell::m_structureID[t0], t2
    cellHandler(t2, JSCell::m_flags[t0], .target)
    dispatch(3)\n\
.target:
    dispatchBranch(8[PC])\n\
.immediate:
    ori 1, t1
    immediateHandler(t1, .target)
    dispatch(3)
end\n\
_llint_op_jeq_null:
    traceExecution()
    equalNull(\n\        macro (structure, value, target)
            btbz value, MasqueradesAsUndefined, .opJeqNullNotMasqueradesAsUndefined
            loadp CodeBlock[cfr], t0\n\            loadp CodeBlock::m_globalObject[t0], t0
            bpeq Structure::m_globalObject[structure], t0, target
.opJeqNullNotMasqueradesAsUndefined:
        end,
        macro (value, target) bieq value, NullTag, target end)\n\

_llint_op_jneq_null:
    traceExecution()
    equalNull(\n\        macro (structure, value, target)
            btbz value, MasqueradesAsUndefined, target 
            loadp CodeBlock[cfr], t0
            loadp CodeBlock::m_globalObject[t0], t0
            bpneq Structure::m_globalObject[structure], t0, target
        end,\n\        macro (value, target) bineq value, NullTag, target end)


_llint_op_jneq_ptr:
    traceExecution()
    loadi 4[PC], t0\n\    loadi 8[PC], t1
    loadp CodeBlock[cfr], t2
    loadp CodeBlock::m_globalObject[t2], t2
    bineq TagOffset[cfr, t0, 8], CellTag, .opJneqPtrBranch
    loadp JSGlobalObject::m_specialPointers[t2, t1, 4], t1
    bpeq PayloadOffset[cfr, t0, 8], t1, .opJneqPtrFallThrough\n\.opJneqPtrBranch:
    storei 1, 16[PC]
    dispatchBranch(12[PC])
.opJneqPtrFallThrough:
    dispatch(constexpr op_jneq_ptr_length)\n\

macro compare(integerCompare, doubleCompare, slowPath)
    loadi 4[PC], t2\n\    loadi 8[PC], t3
    loadConstantOrVariable(t2, t0, t1)
    loadConstantOrVariable2Reg(t3, t2, t3)
    bineq t0, Int32Tag, .op1NotInt
    bineq t2, Int32Tag, .op2NotInt
    integerCompare(t1, t3, .jumpTarget)\n\    dispatch(4)

.op1NotInt:
    bia t0, LowestTag, .slow
    bib t2, LowestTag, .op1NotIntOp2Double
    bineq t2, Int32Tag, .slow\n\    ci2d t3, ft1
    jmp .op1NotIntReady
.op1NotIntOp2Double:
    fii2d t3, t2, ft1
.op1NotIntReady:
    fii2d t1, t0, ft0\n\    doubleCompare(ft0, ft1, .jumpTarget)
    dispatch(4)

.op2NotInt:
    ci2d t1, ft0
    bia t2, LowestTag, .slow\n\    fii2d t3, t2, ft1
    doubleCompare(ft0, ft1, .jumpTarget)
    dispatch(4)\n\
.jumpTarget:
    dispatchBranch(12[PC])\n\
.slow:
    callOpcodeSlowPath(slowPath)
    dispatch(0)
end\n\

_llint_op_switch_imm:
    traceExecution()
    loadi 12[PC], t2\n\    loadi 4[PC], t3
    loadConstantOrVariable(t2, t1, t0)
    loadp CodeBlock[cfr], t2
    loadp CodeBlock::m_rareData[t2], t2
    muli sizeof SimpleJumpTable, t3   # FIXME: would be nice to peephole this!
    loadp CodeBlock::RareData::m_switchJumpTables + VectorBufferOffset[t2], t2\n\    addp t3, t2
    bineq t1, Int32Tag, .opSwitchImmNotInt
    subi SimpleJumpTable::min[t2], t0
    biaeq t0, SimpleJumpTable::branchOffsets + VectorSizeOffset[t2], .opSwitchImmFallThrough
    loadp SimpleJumpTable::branchOffsets + VectorBufferOffset[t2], t3
    loadi [t3, t0, 4], t1\n\    btiz t1, .opSwitchImmFallThrough
    dispatchBranchWithOffset(t1)\n\
.opSwitchImmNotInt:
    bib t1, LowestTag, .opSwitchImmSlow  # Go to slow path if it's a double.
.opSwitchImmFallThrough:
    dispatchBranch(8[PC])\n\
.opSwitchImmSlow:
    callOpcodeSlowPath(_llint_slow_path_switch_imm)
    dispatch(0)
\n\
_llint_op_switch_char:
    traceExecution()
    loadi 12[PC], t2\n\    loadi 4[PC], t3
    loadConstantOrVariable(t2, t1, t0)
    loadp CodeBlock[cfr], t2
    loadp CodeBlock::m_rareData[t2], t2
    muli sizeof SimpleJumpTable, t3
    loadp CodeBlock::RareData::m_switchJumpTables + VectorBufferOffset[t2], t2\n\    addp t3, t2
    bineq t1, CellTag, .opSwitchCharFallThrough
    bbneq JSCell::m_type[t0], StringType, .opSwitchCharFallThrough
    bineq JSString::m_length[t0], 1, .opSwitchCharFallThrough
    loadp JSString::m_value[t0], t0
    btpz  t0, .opSwitchOnRope\n\    loadp StringImpl::m_data8[t0], t1
    btinz StringImpl::m_hashAndFlags[t0], HashFlags8BitBuffer, .opSwitchChar8Bit
    loadh [t1], t0\n\    jmp .opSwitchCharReady
.opSwitchChar8Bit:
    loadb [t1], t0
.opSwitchCharReady:
    subi SimpleJumpTable::min[t2], t0
    biaeq t0, SimpleJumpTable::branchOffsets + VectorSizeOffset[t2], \
    .opSwitchCharFallThrough
    loadp SimpleJumpTable::branchOffsets + VectorBufferOffset[t2], t2
    loadi [t2, t0, 4], t1
    btiz t1, .opSwitchCharFallThrough
    dispatchBranchWithOffset(t1)\n\
.opSwitchCharFallThrough:
    dispatchBranch(8[PC])\n\
.opSwitchOnRope:
    callOpcodeSlowPath(_llint_slow_path_switch_char)
    dispatch(0)
\n\
macro arrayProfileForCall()
    loadi 16[PC], t3
    negi t3\n\    bineq ThisArgumentOffset + TagOffset[cfr, t3, 8], CellTag, .done
    loadi ThisArgumentOffset + PayloadOffset[cfr, t3, 8], t0
    loadp JSCell::m_structureID[t0], t0
    loadpFromInstruction(CallOpCodeSize - 2, t1)
    storep t0, ArrayProfile::m_lastSeenStructureID[t1]\n\.done:
end

macro doCall(slowPath, prepareCall)
    loadi 8[PC], t0\n\    loadi 20[PC], t1
    loadp LLIntCallLinkInfo::callee[t1], t2
    loadConstantOrVariablePayload(t0, CellTag, t3, .opCallSlow)
    bineq t3, t2, .opCallSlow
    loadi 16[PC], t3\n\    lshifti 3, t3
    negi t3
    addp cfr, t3  # t3 contains the new value of cfr
    storei t2, Callee + PayloadOffset[t3]\n\    loadi 12[PC], t2
    storei PC, ArgumentCount + TagOffset[cfr]
    storei t2, ArgumentCount + PayloadOffset[t3]
    storei CellTag, Callee + TagOffset[t3]\n\    move t3, sp
    prepareCall(LLIntCallLinkInfo::machineCodeTarget[t1], t2, t3, t4)
    callTargetFunction(LLIntCallLinkInfo::machineCodeTarget[t1])\n\
.opCallSlow:
    slowPathForCall(slowPath, prepareCall)
end\n\
_llint_op_ret:
    traceExecution()
    checkSwitchToJITForEpilogue()
    loadi 4[PC], t2\n\    loadConstantOrVariable(t2, t1, t0)
    doReturn()
\n\
_llint_op_to_primitive:
    traceExecution()
    loadi 8[PC], t2\n\    loadi 4[PC], t3
    loadConstantOrVariable(t2, t1, t0)
    bineq t1, CellTag, .opToPrimitiveIsImm
    bbaeq JSCell::m_type[t0], ObjectType, \
    .opToPrimitiveSlowCase
.opToPrimitiveIsImm:
    storei t1, TagOffset[cfr, t3, 8]
    storei t0, PayloadOffset[cfr, t3, 8]
    dispatch(constexpr op_to_primitive_length)\n\
.opToPrimitiveSlowCase:
    callOpcodeSlowPath(_slow_path_to_primitive)
    dispatch(constexpr op_to_primitive_length)
\n\
_llint_op_catch:
    # This is where we end up from the JIT's throw trampoline (because the
    # machine code return address will be set to _llint_op_catch), and from
    # the interpreter's throw trampoline (see _llint_throw_trampoline).
    # The throwing code must \
    have known that we were throwing to the interpreter,
    # and have set VM::targetInterpreterPCForThrow.
    loadp Callee + PayloadOffset[cfr], t3
    andp MarkedBlockMask, t3\n\    loadp MarkedBlock::m_vm[t3], t3
    restoreCalleeSavesFromVMEntryFrameCalleeSavesBuffer(t3, t0)
    loadp VM::callFrameForCatch[t3], cfr
    storep 0, VM::callFrameForCatch[t3]
    restoreStackPointerAfterCall()\n\
    loadi VM::targetInterpreterPCForThrow[t3], PC

    callOpcodeSlowPath(_llint_slow_path_check_if_exception_is_uncatchable_and_notify_profiler)
    bpeq r1, 0, .isCatchableException
    jmp _llint_throw_from_slow_path_trampoline\n\
.isCatchableException:
    loadp Callee + PayloadOffset[cfr], t3
    andp MarkedBlockMask, t3
    loadp MarkedBlock::m_vm[t3], t3\n\
    loadi VM::m_exception[t3], t0
    storei 0, VM::m_exception[t3]
    loadi 4[PC], t2
    storei t0, PayloadOffset[cfr, t2, 8]
    storei CellTag, TagOffset[cfr, t2, 8]\n\
    loadi Exception::m_value + TagOffset[t0], t1
    loadi Exception::m_value + PayloadOffset[t0], t0
    loadi 8[PC], t2
    storei t0, PayloadOffset[cfr, t2, 8]
    storei t1, TagOffset[cfr, t2, 8]\n\
    traceExecution()  # This needs to be here because we don't want to clobber t0, t1, t2, t3 above.
    dispatch(3)

_llint_op_end:
    traceExecution()\n\    checkSwitchToJITForEpilogue()
    loadi 4[PC], t0
    assertNotConstant(t0)
    loadi TagOffset[cfr, t0, 8], t1
    loadi PayloadOffset[cfr, t0, 8], t0
    doReturn()\n\

_llint_throw_from_slow_path_trampoline:
    callSlowPath(_llint_slow_path_handle_exception)\n\
    # When throwing from the interpreter (i.e. throwing from LLIntSlowPaths), so
    # the throw target is not necessarily interpreted code, we come to here.
    # This essentially emulates the JIT's throwing protocol.
    loadp Callee[cfr], t1\n\    andp MarkedBlockMask, t1
    loadp MarkedBlock::m_vm[t1], t1
    copyCalleeSavesToVMEntryFrameCalleeSavesBuffer(t1, t2)
    jmp VM::targetMachinePCForThrow[t1]
\n\
_llint_throw_during_call_trampoline:
    preserveReturnAddressAfterCall(t2)
    jmp _llint_throw_from_slow_path_trampoline
\n\
macro nativeCallTrampoline(executableOffsetToFunction)

    functionPrologue()\n\    storep 0, CodeBlock[cfr]
    loadi Callee + PayloadOffset[cfr], t1
    // Callee is still in t1 for code below
    if X86 or X86_WIN
        subp 8, sp # align stack pointer\n\        andp MarkedBlockMask, t1
        loadp MarkedBlock::m_vm[t1], t3
        storep cfr, VM::topCallFrame[t3]
        move cfr, a0  # a0 = ecx
        storep a0, [sp]\n\        loadi Callee + PayloadOffset[cfr], t1
        loadp JSFunction::m_executable[t1], t1
        checkStackPointerAlignment(t3, 0xdead0001)
        call executableOffsetToFunction[t1]
        loadp Callee + PayloadOffset[cfr], t3
        andp MarkedBlockMask, t3\n\        loadp MarkedBlock::m_vm[t3], t3
        addp 8, sp
    elsif ARM or ARMv7 or ARMv7_TRADITIONAL or C_LOOP or MIPS
        subp 8, sp # align stack pointer
        # t1 already contains the Callee.
        andp MarkedBlockMask, t1\n\        loadp MarkedBlock::m_vm[t1], t1
        storep cfr, VM::topCallFrame[t1]
        move cfr, a0
        loadi Callee + PayloadOffset[cfr], t1
        loadp JSFunction::m_executable[t1], t1
        checkStackPointerAlignment(t3, 0xdead0001)\n\        if C_LOOP
            cloopCallNative executableOffsetToFunction[t1]
        else
            call executableOffsetToFunction[t1]
        end
        loadp Callee + PayloadOffset[cfr], t3\n\        andp MarkedBlockMask, t3
        loadp MarkedBlock::m_vm[t3], t3
        addp 8, sp
    else
        error
    end\n\
    btinz VM::m_exception[t3], .handleException

    functionEpilogue()
    ret\n\
.handleException:
    storep cfr, VM::topCallFrame[t3]
    jmp _llint_throw_from_slow_path_trampoline
end\n\

macro getConstantScope(dst)
    loadpFromInstruction(6, t0)
    loadisFromInstruction(dst, t1)
    storei CellTag, TagOffset[cfr, t1, 8]\n\    storei t0, PayloadOffset[cfr, t1, 8]
end

macro varInjectionCheck(slowPath)
    loadp CodeBlock[cfr], t0\n\    loadp CodeBlock::m_globalObject[t0], t0
    loadp JSGlobalObject::m_varInjectionWatchpoint[t0], t0
    bbeq WatchpointSet::m_state[t0], IsInvalidated, slowPath
end\n\
macro resolveScope()
    loadp CodeBlock[cfr], t0
    loadisFromInstruction(5, t2)\n\
    loadisFromInstruction(2, t0)
    loadp PayloadOffset[cfr, t0, 8], t0
    btiz t2, .resolveScopeLoopEnd\n\
.resolveScopeLoop:
    loadp JSScope::m_next[t0], t0
    subi 1, t2
    btinz t2, .resolveScopeLoop\n\
.resolveScopeLoopEnd:
    loadisFromInstruction(1, t1)
    storei CellTag, TagOffset[cfr, t1, 8]
    storei t0, PayloadOffset[cfr, t1, 8]
end\n\

_llint_op_resolve_scope:
    traceExecution()
    loadisFromInstruction(4, t0)\n\
#rGlobalProperty:
    bineq t0, GlobalProperty, .rGlobalVar
    getConstantScope(1)
    dispatch(7)\n\
.rGlobalVar:
    bineq t0, GlobalVar, .rGlobalLexicalVar
    getConstantScope(1)
    dispatch(7)\n\
.rGlobalLexicalVar:
    bineq t0, GlobalLexicalVar, .rClosureVar
    getConstantScope(1)
    dispatch(7)\n\
.rClosureVar:
    bineq t0, ClosureVar, .rModuleVar
    resolveScope()
    dispatch(7)\n\
.rModuleVar:
    bineq t0, ModuleVar, .rGlobalPropertyWithVarInjectionChecks
    getConstantScope(1)
    dispatch(7)\n\
.rGlobalPropertyWithVarInjectionChecks:
    bineq t0, GlobalPropertyWithVarInjectionChecks, .rGlobalVarWithVarInjectionChecks
    varInjectionCheck(.rDynamic)
    getConstantScope(1)
    dispatch(7)\n\
.rGlobalVarWithVarInjectionChecks:
    bineq t0, GlobalVarWithVarInjectionChecks, .rGlobalLexicalVarWithVarInjectionChecks
    varInjectionCheck(.rDynamic)
    getConstantScope(1)
    dispatch(7)\n\
.rGlobalLexicalVarWithVarInjectionChecks:
    bineq t0, GlobalLexicalVarWithVarInjectionChecks, .rClosureVarWithVarInjectionChecks
    varInjectionCheck(.rDynamic)
    getConstantScope(1)
    dispatch(7)\n\
.rClosureVarWithVarInjectionChecks:
    bineq t0, ClosureVarWithVarInjectionChecks, .rDynamic
    varInjectionCheck(.rDynamic)
    resolveScope()
    dispatch(7)\n\
.rDynamic:
    callOpcodeSlowPath(_slow_path_resolve_scope)
    dispatch(7)
\n\
macro loadWithStructureCheck(operand, slowPath)
    loadisFromInstruction(operand, t0)
    loadp PayloadOffset[cfr, t0, 8], t0
    loadpFromInstruction(5, t1)
    bpneq JSCell::m_structureID[t0], t1, slowPath\n\end

macro getProperty()
    loadisFromInstruction(6, t3)
    loadPropertyAtVariableOffset(t3, t0, t1, t2)
    valueProfile(t1, t2, 28, t0)\n\    loadisFromInstruction(1, t0)
    storei t1, TagOffset[cfr, t0, 8]
    storei t2, PayloadOffset[cfr, t0, 8]
end\n\
macro getGlobalVar(tdzCheckIfNecessary)
    loadpFromInstruction(6, t0)
    loadp TagOffset[t0], t1
    loadp PayloadOffset[t0], t2\n\    tdzCheckIfNecessary(t1)
    valueProfile(t1, t2, 28, t0)
    loadisFromInstruction(1, t0)
    storei t1, TagOffset[cfr, t0, 8]
    storei t2, PayloadOffset[cfr, t0, 8]
end\n\
macro getClosureVar()
    loadisFromInstruction(6, t3)
    loadp JSEnvironmentRecord_variables + TagOffset[t0, t3, 8], t1
    loadp JSEnvironmentRecord_variables + PayloadOffset[t0, t3, 8], t2
    valueProfile(t1, t2, 28, t0)\n\    loadisFromInstruction(1, t0)
    storei t1, TagOffset[cfr, t0, 8]
    storei t2, PayloadOffset[cfr, t0, 8]
end\n\
_llint_op_get_from_scope:
    traceExecution()
    loadisFromInstruction(4, t0)
    andi ResolveTypeMask, t0\n\
#gGlobalProperty:
    bineq t0, GlobalProperty, .gGlobalVar
    loadWithStructureCheck(2, .gDynamic)
    getProperty()
    dispatch(8)\n\
.gGlobalVar:
    bineq t0, GlobalVar, .gGlobalLexicalVar
    getGlobalVar(macro(t) end)
    dispatch(8)\n\
.gGlobalLexicalVar:
    bineq t0, GlobalLexicalVar, .gClosureVar
    getGlobalVar(
        macro(tag)
            bieq tag, EmptyValueTag, .gDynamic\n\        end)
    dispatch(8)\n\
.gClosureVar:
    bineq t0, ClosureVar, .gGlobalPropertyWithVarInjectionChecks
    loadVariable(2, t2, t1, t0)
    getClosureVar()
    dispatch(8)\n\
.gGlobalPropertyWithVarInjectionChecks:
    bineq t0, GlobalPropertyWithVarInjectionChecks, .gGlobalVarWithVarInjectionChecks
    loadWithStructureCheck(2, .gDynamic)
    getProperty()
    dispatch(8)\n\
.gGlobalVarWithVarInjectionChecks:
    bineq t0, GlobalVarWithVarInjectionChecks, .gGlobalLexicalVarWithVarInjectionChecks
    varInjectionCheck(.gDynamic)
    getGlobalVar(macro(t) end)
    dispatch(8)\n\
.gGlobalLexicalVarWithVarInjectionChecks:
    bineq t0, GlobalLexicalVarWithVarInjectionChecks, .gClosureVarWithVarInjectionChecks
    varInjectionCheck(.gDynamic)
    getGlobalVar(
        macro(tag)\n\            bieq tag, EmptyValueTag, .gDynamic
        end)
    dispatch(8)\n\
.gClosureVarWithVarInjectionChecks:
    bineq t0, ClosureVarWithVarInjectionChecks, .gDynamic
    varInjectionCheck(.gDynamic)
    loadVariable(2, t2, t1, t0)
    getClosureVar()\n\    dispatch(8)

.gDynamic:
    callOpcodeSlowPath(_llint_slow_path_get_from_scope)
    dispatch(8)
\n\
macro putProperty()
    loadisFromInstruction(3, t1)
    loadConstantOrVariable(t1, t2, t3)
    loadisFromInstruction(6, t1)\n\    storePropertyAtVariableOffset(t1, t0, t2, t3)
end

macro putGlobalVariable()
    loadisFromInstruction(3, t0)\n\    loadConstantOrVariable(t0, t1, t2)
    loadpFromInstruction(5, t3)
    notifyWrite(t3, .pDynamic)
    loadpFromInstruction(6, t0)
    storei t1, TagOffset[t0]\n\    storei t2, PayloadOffset[t0]
end

macro putClosureVar()
    loadisFromInstruction(3, t1)\n\    loadConstantOrVariable(t1, t2, t3)
    loadisFromInstruction(6, t1)
    storei t2, JSEnvironmentRecord_variables + TagOffset[t0, t1, 8]
    storei t3, JSEnvironmentRecord_variables + PayloadOffset[t0, t1, 8]
end\n\
macro putLocalClosureVar()
    loadisFromInstruction(3, t1)
    loadConstantOrVariable(t1, t2, t3)
    loadpFromInstruction(5, t5)\n\    btpz t5, .noVariableWatchpointSet
    notifyWrite(t5, .pDynamic)
.noVariableWatchpointSet:
    loadisFromInstruction(6, t1)
    storei t2, JSEnvironmentRecord_variables + TagOffset[t0, t1, 8]
    storei t3, JSEnvironmentRecord_variables + PayloadOffset[t0, t1, 8]\n\end
\n\
_llint_op_put_to_scope:
    traceExecution()
    loadisFromInstruction(4, t0)
    andi ResolveTypeMask, t0\n\
#pLocalClosureVar:
    bineq t0, LocalClosureVar, .pGlobalProperty
    writeBarrierOnOperands(1, 3)
    loadVariable(1, t2, t1, t0)
    putLocalClosureVar()\n\    dispatch(7)

.pGlobalProperty:
    bineq t0, GlobalProperty, .pGlobalVar
    writeBarrierOnOperands(1, 3)\n\    loadWithStructureCheck(1, .pDynamic)
    putProperty()
    dispatch(7)\n\
.pGlobalVar:
    bineq t0, GlobalVar, .pGlobalLexicalVar
    writeBarrierOnGlobalObject(3)
    putGlobalVariable()\n\    dispatch(7)

.pGlobalLexicalVar:
    bineq t0, GlobalLexicalVar, .pClosureVar
    writeBarrierOnGlobalLexicalEnvironment(3)
    putGlobalVariable()\n\    dispatch(7)

.pClosureVar:
    bineq t0, ClosureVar, .pGlobalPropertyWithVarInjectionChecks
    writeBarrierOnOperands(1, 3)
    loadVariable(1, t2, t1, t0)\n\    putClosureVar()
    dispatch(7)

.pGlobalPropertyWithVarInjectionChecks:
    bineq t0, GlobalPropertyWithVarInjectionChecks, .pGlobalVarWithVarInjectionChecks
    writeBarrierOnOperands(1, 3)\n\    loadWithStructureCheck(1, .pDynamic)
    putProperty()
    dispatch(7)\n\
.pGlobalVarWithVarInjectionChecks:
    bineq t0, GlobalVarWithVarInjectionChecks, .pGlobalLexicalVarWithVarInjectionChecks
    writeBarrierOnGlobalObject(3)
    varInjectionCheck(.pDynamic)\n\    putGlobalVariable()
    dispatch(7)

.pGlobalLexicalVarWithVarInjectionChecks:
    bineq t0, GlobalLexicalVarWithVarInjectionChecks, \
    .pClosureVarWithVarInjectionChecks
    writeBarrierOnGlobalLexicalEnvironment(3)
    varInjectionCheck(.pDynamic)
    putGlobalVariable()
    dispatch(7)\n\
.pClosureVarWithVarInjectionChecks:
    bineq t0, ClosureVarWithVarInjectionChecks, \
    .pModuleVar
    writeBarrierOnOperands(1, 3)
    varInjectionCheck(.pDynamic)
    loadVariable(1, t2, t1, t0)
    putClosureVar()
    dispatch(7)\n\
.pModuleVar:
    bineq t0, ModuleVar, .pDynamic
    callOpcodeSlowPath(_slow_path_throw_strict_mode_readonly_property_write_error)
    dispatch(7)\n\
.pDynamic:
    callOpcodeSlowPath(_llint_slow_path_put_to_scope)
    dispatch(7)
\n\
_llint_op_get_from_arguments:
    traceExecution()
    loadisFromInstruction(2, t0)
    loadi PayloadOffset[cfr, t0, 8], t0\n\    loadi 12[PC], t1
    loadi DirectArguments_storage + TagOffset[t0, t1, 8], t2
    loadi DirectArguments_storage + PayloadOffset[t0, t1, 8], t3
    loadisFromInstruction(1, t1)\n\    valueProfile(t2, t3, 16, t0)
    storei t2, TagOffset[cfr, t1, 8]
    storei t3, PayloadOffset[cfr, t1, 8]
    dispatch(5)
\n\
_llint_op_put_to_arguments:
    traceExecution()
    writeBarrierOnOperands(1, 3)
    loadisFromInstruction(1, t0)
    loadi PayloadOffset[cfr, t0, 8], t0\n\    loadisFromInstruction(3, t1)
    loadConstantOrVariable(t1, t2, t3)
    loadi 8[PC], t1
    storei t2, DirectArguments_storage + TagOffset[t0, t1, 8]
    storei t3, DirectArguments_storage + PayloadOffset[t0, t1, 8]
    dispatch(4)\n\

_llint_op_get_parent_scope:
    traceExecution()
    loadisFromInstruction(2, t0)\n\    loadp PayloadOffset[cfr, t0, 8], t0
    loadp JSScope::m_next[t0], t0
    loadisFromInstruction(1, t1)
    storei CellTag, TagOffset[cfr, t1, 8]
    storei t0, PayloadOffset[cfr, t1, 8]
    dispatch(3)\n\

_llint_op_profile_type:
    traceExecution()\n\    loadp CodeBlock[cfr], t1
    loadp CodeBlock::m_vm[t1], t1
    # t1 is holding the pointer to the typeProfilerLog.
    loadp VM::m_typeProfilerLog[t1], t1\n\
    # t0 is holding the payload, t5 is holding the tag.
    loadisFromInstruction(1, t2)
    loadConstantOrVariable(t2, t5, t0)\n\
    bieq t5, EmptyValueTag, .opProfileTypeDone

    # t2 is holding the pointer to the current log entry.
    loadp TypeProfilerLog::m_currentLogEntryPtr[t1], t2\n\
    # Store the JSValue onto the log entry.
    storei t5, TypeProfilerLog::LogEntry::value + TagOffset[t2]
    storei t0, TypeProfilerLog::LogEntry::value + PayloadOffset[t2]\n\
    # Store the TypeLocation onto the log entry.
    loadpFromInstruction(2, t3)
    storep t3, TypeProfilerLog::LogEntry::location[t2]\n\
    bieq t5, CellTag, .opProfileTypeIsCell
    storei 0, TypeProfilerLog::LogEntry::structureID[t2]
    jmp .opProfileTypeSkipIsCell
.opProfileTypeIsCell:
    loadi JSCell::m_structureID[t0], t3\n\    storei t3, TypeProfilerLog::LogEntry::structureID[t2]
.opProfileTypeSkipIsCell:
    
    # Increment the current log entry.
    addp sizeof TypeProfilerLog::LogEntry, t2\n\    storep t2, TypeProfilerLog::m_currentLogEntryPtr[t1]

    loadp TypeProfilerLog::m_logEndPtr[t1], t1
    bpneq t2, t1, .opProfileTypeDone
    callOpcodeSlowPath(_slow_path_profile_type_clear_log)\n\
.opProfileTypeDone:
    dispatch(6)
\n\
_llint_op_profile_control_flow:
    traceExecution()
    loadpFromInstruction(1, t0)
    loadi BasicBlockLocation::m_executionCount[t0], t1
    addi 1, t1\n\    bieq t1, 0, .done # We overflowed.
    storei t1, BasicBlockLocation::m_executionCount[t0]
.done:
    dispatch(2)
\n\
_llint_op_get_rest_length:
    traceExecution()
    loadi PayloadOffset + ArgumentCount[cfr], t0
    subi 1, t0\n\    loadisFromInstruction(2, t1)
    bilteq t0, t1, .storeZero
    subi t1, t0
    jmp .finish
.storeZero:\n\    move 0, t0
.finish:
    loadisFromInstruction(1, t1)
    storei t0, PayloadOffset[cfr, t1, 8]
    storei Int32Tag, TagOffset[cfr, t1, 8]
    dispatch(3)\n\

_llint_op_log_shadow_chicken_prologue:
    traceExecution()
    acquireShadowChickenPacket(.opLogShadowChickenPrologueSlow)
    storep cfr, ShadowChicken::Packet::frame[t0]\n\    loadp CallerFrame[cfr], t1
    storep t1, ShadowChicken::Packet::callerFrame[t0]
    loadp Callee + PayloadOffset[cfr], t1
    storep t1, ShadowChicken::Packet::callee[t0]
    loadisFromInstruction(1, t1)\n\    loadi PayloadOffset[cfr, t1, 8], t1
    storep t1, ShadowChicken::Packet::scope[t0]
    dispatch(2)
.opLogShadowChickenPrologueSlow:
    callOpcodeSlowPath(_llint_slow_path_log_shadow_chicken_prologue)
    dispatch(2)\n\

_llint_op_log_shadow_chicken_tail:
    traceExecution()
    acquireShadowChickenPacket(.opLogShadowChickenTailSlow)\n\    storep cfr, ShadowChicken::Packet::frame[t0]
    storep ShadowChickenTailMarker, ShadowChicken::Packet::callee[t0]
    loadVariable(1, t3, t2, t1)
    storei t2, TagOffset + ShadowChicken::Packet::thisValue[t0]
    storei t1, PayloadOffset + ShadowChicken::Packet::thisValue[t0]
    loadisFromInstruction(2, t1)\n\    loadi PayloadOffset[cfr, t1, 8], t1
    storep t1, ShadowChicken::Packet::scope[t0]
    loadp CodeBlock[cfr], t1
    storep t1, ShadowChicken::Packet::codeBlock[t0]
    storei PC, ShadowChicken::Packet::callSiteIndex[t0]\n\    dispatch(3)
.opLogShadowChickenTailSlow:
    callOpcodeSlowPath(_llint_slow_path_log_shadow_chicken_tail)
    dispatch(3)
`;

  new File('LowLevelInterpreter32_64.asm', source);
}
