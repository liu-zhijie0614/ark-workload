/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

export let float_2: number = 2.0;
export let angle_180: number = 180.0;
export let half: number = 0.5;
export let float_8: number = 8.0;
export let float_10: number = 10.0;
export let int_4: number = 4;
export let int_16: number = 16;
export let float_3: number = 3.0;
export let float_025: number = 0.25;

export let b2_minFloat = Number.MIN_VALUE;
export let b2_maxFloat = Number.MAX_SAFE_INTEGER;
export let b2_epsilon = Number.EPSILON;
export let b2_pi: number = Math.PI;
export let b2_maxManifoldPoints = 2;
export let b2_maxPolygonVertices = 8;
export let b2_aabbExtension: number = 0.1;
export let b2_aabbMultiplier: number = 2.0;
export let b2_linearSlop: number = 0.005;
export let b2_angularSlop: number = (float_2 / angle_180) * b2_pi;
export let b2_polygonRadius: number = float_2 * b2_linearSlop;
export let b2_maxSubSteps = 8;
export let b2_maxTOIContacts = 32;
export let b2_velocityThreshold: number = 1.0;
export let b2_maxLinearCorrection: number = 0.2;
export let b2_maxAngularCorrection: number = (float_8 / angle_180) * b2_pi;
export let b2_maxTranslation: number = 2.0;
export let b2_maxTranslationSquared: number = b2_maxTranslation * b2_maxTranslation;
export let b2_maxRotation: number = half * b2_pi;
export let b2_maxRotationSquared: number = b2_maxRotation * b2_maxRotation;
export let b2_baumgarte: number = 0.2;
export let b2_toiBaugarte: number = 0.75;
export let b2_timeToSleep: number = 0.5;
export let b2_linearSleepTolerance: number = 0.01;
export let b2_angularSleepTolerance: number = (float_2 / angle_180) * b2_pi;

export let hex_1: number = 0x0001;
export let hex_2: number = 0x0002;
export let hex_4: number = 0x0004;
export let hex_8: number = 0x0008;
export let hex_10: number = 0x0010;
export let hex_20: number = 0x0020;
export let hex_40: number = 0x0040;
