/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { replace16NumberCount } from './bitstream';
export class PopObject {
}
const Number_2 = 2;
const Number_4 = 4;
export class Heap {
    constructor(length) {
        this.buffer = replace16NumberCount(length * Number_2);
        this.length = 0;
    }
    getParent(index) {
        return (((index - Number_2) / Number_4) | 0) * Number_2;
    }
    getChild(index) {
        return Number_2 * index + Number_2;
    }
    push(index, value) {
        let current = this.length;
        let parent;
        let heap = this.buffer;
        let swap;
        heap[this.length] = value;
        heap[this.length + 1] = index;
        this.length += 2;
        while (current > 0) {
            parent = this.getParent(current);
            if (heap[current] > heap[parent]) {
                swap = heap[current];
                heap[current] = heap[parent];
                heap[parent] = swap;
                swap = heap[current + 1];
                heap[current + 1] = heap[parent + 1];
                heap[parent + 1] = swap;
                current = parent;
            }
            else {
                break;
            }
        }
        return this.length;
    }
    pop() {
        let index = 0;
        let value = 0;
        let swap = 0;
        let current = 0;
        let parent = 0;
        let heap = this.buffer;
        value = heap[0];
        index = heap[1];
        this.length -= 2;
        heap[0] = heap[this.length];
        heap[1] = heap[this.length + 1];
        parent = 0;
        while (true) {
            current = this.getChild(parent);
            if (current >= this.length) {
                break;
            }
            if (current + Number_2 < this.length && heap[current + Number_2] > heap[current]) {
                current += Number_2;
            }
            if (heap[current] > heap[parent]) {
                swap = heap[parent];
                heap[parent] = heap[current];
                heap[current] = swap;
                swap = heap[parent + 1];
                heap[parent + 1] = heap[current + 1];
                heap[current + 1] = swap;
            }
            else {
                break;
            }
            parent = current;
        }
        return { index: index, value: value, length: this.length };
    }
}
