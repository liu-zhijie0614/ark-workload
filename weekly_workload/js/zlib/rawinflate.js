/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { Huffman } from './huffman';
import { replace8NumberCount } from './bitstream';
const Number_2 = 2;
const Number_3 = 3;
const Number_4 = 4;
const Number_5 = 5;
const Number_7 = 7;
const Number_8 = 8;
const Number_9 = 9;
const Number_11 = 11;
const Number_16 = 16;
const Number_17 = 17;
const Number_18 = 18;
const Number_30 = 30;
const Number_143 = 143;
const Number_255 = 255;
const Number_256 = 256;
const Number_257 = 257;
const Number_258 = 258;
const Number_279 = 279;
const Number_288 = 288;
const Number_32768 = 32768;
var BufferType;
(function (BufferType) {
    BufferType[BufferType["BLOCK"] = 0] = "BLOCK";
    BufferType[BufferType["ADAPTIVE"] = 1] = "ADAPTIVE";
})(BufferType || (BufferType = {}));
/*
 *@Generator
 */
export class RawInflate {
    constructor(input, opt_params) {
        this.currentLitlenTable = null;
        this.buffer = null;
        this.bfinal = false;
        this.bufferType = BufferType.ADAPTIVE;
        this.resize = false;
        this.blocks = [];
        this.bufferSize = RawInflate.ZLIB_RAW_INFLATE_BUFFER_SIZE;
        this.totalpos = 0;
        this.ip = 0;
        this.bitsbuf = 0;
        this.bitsbuflen = 0;
        this.input = new Uint8Array(input);
        this.bfinal = false;
        this.bufferType = BufferType.ADAPTIVE;
        this.resize = false;
        // option parameters
        if (opt_params != null) {
            if (opt_params.index) {
                this.ip = opt_params.index;
            }
            if (opt_params.bufferSize) {
                this.bufferSize = opt_params.bufferSize;
            }
            if (opt_params.bufferType) {
                this.bufferType = opt_params.bufferType;
            }
            if (opt_params.resize) {
                this.resize = opt_params.resize;
            }
        }
        // initialize
        switch (this.bufferType) {
            case BufferType.BLOCK:
                this.op = RawInflate.MaxBackwardLength;
                this.output = replace8NumberCount(RawInflate.MaxBackwardLength + this.bufferSize + RawInflate.MaxCopyLength);
                break;
            case BufferType.ADAPTIVE:
                this.op = 0;
                this.output = replace8NumberCount(this.bufferSize);
                break;
            default:
                throw new Error('invalid inflate mode');
        }
    }
    decompress() {
        while (!this.bfinal) {
            this.parseBlock();
        }
        switch (this.bufferType) {
            case BufferType.BLOCK:
                return this.concatBufferBlock();
            case BufferType.ADAPTIVE:
                return this.concatBufferDynamic();
            default:
                throw new Error('invalid inflate mode');
        }
    }
    parseBlock() {
        let hdr = this.readBits(Number_3);
        // BFINAL
        if ((hdr & 0x1) != 0) {
            this.bfinal = true;
        }
        // BTYPE
        hdr >>>= 1;
        switch (hdr) {
            // uncompressed
            case 0:
                this.parseUncompressedBlock();
                break;
            // fixed huffman
            case 1:
                this.parseFixedHuffmanBlock();
                break;
            // dynamic huffman
            case Number_2:
                this.parseDynamicHuffmanBlock();
                break;
            // reserved or other
            default:
                throw new Error(`unknown BTYPE: ${hdr}`);
        }
    }
    readBits(length) {
        let bitsbuf = this.bitsbuf;
        let bitsbuflen = this.bitsbuflen;
        let input = this.input;
        let ip = this.ip;
        /** @type {number} */
        let inputLength = input.length;
        /** @type {number} input and output byte. */
        let octet;
        // input byte
        if (ip + ((length - bitsbuflen + Number_7) >> Number_3) >= inputLength) {
            throw new Error('input buffer is broken');
        }
        // not enough buffer
        while (bitsbuflen < length) {
            bitsbuf |= input[ip] << bitsbuflen;
            ip += 1;
            bitsbuflen += Number_8;
        }
        // output byte
        octet = bitsbuf & ((1 << length) - 1);
        bitsbuf >>>= length;
        bitsbuflen -= length;
        this.bitsbuf = bitsbuf;
        this.bitsbuflen = bitsbuflen;
        this.ip = ip;
        return octet;
    }
    readCodeByTable(table) {
        let bitsbuf = this.bitsbuf;
        let bitsbuflen = this.bitsbuflen;
        let input = this.input;
        let ip = this.ip;
        let inputLength = input.length;
        let codeTable = table[0];
        let maxCodeLength = table[1];
        let codeWithLength;
        let codeLength;
        // not enough buffer
        while (bitsbuflen < maxCodeLength) {
            if (ip >= inputLength) {
                break;
            }
            bitsbuf |= input[ip] << bitsbuflen;
            ip += 1;
            bitsbuflen += Number_8;
        }
        // read max length
        codeWithLength = codeTable[bitsbuf & ((1 << maxCodeLength) - 1)];
        codeLength = codeWithLength >>> Number_16;
        if (codeLength > bitsbuflen) {
            throw new Error(`invalid code length: ${codeLength}`);
        }
        bitsbuf >>= codeLength;
        bitsbuflen -= codeLength;
        ip += 1;
        this.bitsbuf = bitsbuf;
        this.bitsbuflen = bitsbuflen;
        this.ip = ip;
        return codeWithLength & 0xffff;
    }
    parseUncompressedBlock() {
        let input = this.input;
        let ip = this.ip;
        let output = this.output;
        let op = this.op;
        let inputLength = input.length;
        let len;
        let nlen;
        let olength = output.length;
        let preCopy;
        // skip buffered header bits
        this.bitsbuf = 0;
        this.bitsbuflen = 0;
        // len
        if (ip + 1 >= inputLength) {
            throw new Error('invalid uncompressed block header: LEN');
        }
        len = input[ip] | (input[ip + 1] << Number_8);
        ip += Number_2;
        // nlen
        if (ip + 1 >= inputLength) {
            throw new Error('invalid uncompressed block header: NLEN');
        }
        nlen = input[ip] | (input[ip + 1] << Number_8);
        ip += Number_2;
        // check len & nlen
        if (len === ~nlen) {
            throw new Error('invalid uncompressed block header: length verify');
        }
        // check size
        if (ip + len > input.length) {
            throw new Error('input buffer is broken');
        }
        // expand buffer
        switch (this.bufferType) {
            case BufferType.BLOCK:
                // pre copy
                while (op + len > output.length) {
                    preCopy = olength - op;
                    len -= preCopy;
                    output.set(input.subarray(ip, ip + preCopy), op);
                    op += preCopy;
                    ip += preCopy;
                    this.op = op;
                    output = this.expandBufferBlock();
                    op = this.op;
                }
                break;
            case BufferType.ADAPTIVE:
                while (op + len > output.length) {
                    output = this.expandBufferAdaptive({ fixRatio: 2 });
                }
                break;
            default:
                throw new Error('invalid inflate mode');
        }
        // copy
        output.set(input.subarray(ip, ip + len), op);
        op += len;
        ip += len;
        this.ip = ip;
        this.op = op;
        this.output = output;
    }
    parseFixedHuffmanBlock() {
        switch (this.bufferType) {
            case BufferType.ADAPTIVE:
                this.decodeHuffmanAdaptive(RawInflate.FixedLiteralLengthTable, RawInflate.FixedDistanceTable);
                break;
            case BufferType.BLOCK:
                this.decodeHuffmanBlock(RawInflate.FixedLiteralLengthTable, RawInflate.FixedDistanceTable);
                break;
            default:
                throw new Error('invalid inflate mode');
        }
    }
    parseDynamicHuffmanBlock() {
        let hlit = this.readBits(Number_5) + Number_257;
        let hdist = this.readBits(Number_5) + 1;
        let hclen = this.readBits(Number_4) + Number_4;
        let codeLengths = replace8NumberCount(RawInflate.order.length);
        let codeLengthsTable;
        let litlenTable;
        let distTable;
        let lengthTable;
        let code;
        let prev = null;
        let repeats;
        // decode code lengths
        for (let i = 0; i < hclen; ++i) {
            codeLengths[RawInflate.order[i]] = this.readBits(Number_3);
        }
        // decode length table
        codeLengthsTable = RawInflate.buildHuffmanTable(codeLengths);
        lengthTable = replace8NumberCount(hlit + hdist);
        let i = 0;
        while (i < hlit + hdist) {
            code = this.readCodeByTable(codeLengthsTable);
            switch (code) {
                case Number_16:
                    repeats = Number_3 + this.readBits(Number_2);
                    while (repeats > 0) {
                        lengthTable[i] = prev !== null && prev !== void 0 ? prev : 0;
                        i += 1;
                        repeats -= 1;
                    }
                    break;
                case Number_17:
                    repeats = Number_3 + this.readBits(Number_3);
                    while (repeats > 0) {
                        lengthTable[i] = 0;
                        i += 1;
                        repeats -= 1;
                    }
                    prev = 0;
                    break;
                case Number_18:
                    repeats = Number_11 + this.readBits(Number_7);
                    while (repeats > 0) {
                        lengthTable[i] = 0;
                        repeats -= 1;
                        i += 1;
                    }
                    prev = 0;
                    break;
                default:
                    lengthTable[i] = code;
                    prev = code;
                    i += 1;
                    break;
            }
        }
        litlenTable = RawInflate.buildHuffmanTable(lengthTable.subarray(0, hlit));
        distTable = RawInflate.buildHuffmanTable(lengthTable.subarray(hlit));
        switch (this.bufferType) {
            case BufferType.ADAPTIVE:
                this.decodeHuffmanAdaptive(litlenTable, distTable);
                break;
            case BufferType.BLOCK:
                this.decodeHuffmanBlock(litlenTable, distTable);
                break;
            default:
                throw new Error('invalid inflate mode');
        }
    }
    decodeHuffmanBlock(litlen, dist) {
        let output = this.output;
        let op = this.op;
        this.currentLitlenTable = litlen;
        let olength = output.length - RawInflate.MaxCopyLength;
        let code;
        let ti;
        let codeDist;
        let codeLength;
        let lengthCodeTable = RawInflate.LengthCodeTable;
        let lengthExtraTable = RawInflate.LengthExtraTable;
        let distCodeTable = RawInflate.DistCodeTable;
        let distExtraTable = RawInflate.DistExtraTable;
        code = this.readCodeByTable(litlen);
        while (code !== Number_256) {
            // literal
            if (code < Number_256) {
                if (op >= olength) {
                    this.op = op;
                    output = this.expandBufferBlock();
                    op = this.op;
                }
                output[op] = code;
                op += 1;
                continue;
            }
            // length code
            ti = code - Number_257;
            codeLength = lengthCodeTable[ti];
            if (lengthExtraTable[ti] > 0) {
                codeLength += this.readBits(lengthExtraTable[ti]);
            }
            // dist code
            code = this.readCodeByTable(dist);
            codeDist = distCodeTable[code];
            if (distExtraTable[code] > 0) {
                codeDist += this.readBits(distExtraTable[code]);
            }
            // lz77 decode
            if (op >= olength) {
                this.op = op;
                output = this.expandBufferBlock();
                op = this.op;
            }
            while (codeLength > 0) {
                output[op] = output[op - codeDist];
                op += 1;
                codeLength -= 1;
            }
            code = this.readCodeByTable(litlen);
        }
        while (this.bitsbuflen >= Number_8) {
            this.bitsbuflen -= 8;
            this.ip -= 1;
        }
        this.op = op;
    }
    decodeHuffmanAdaptive(litlen, dist) {
        let output = this.output;
        let op = this.op;
        this.currentLitlenTable = litlen;
        let olength = output.length;
        let code;
        let ti;
        let codeDist;
        let codeLength;
        let lengthCodeTable = RawInflate.LengthCodeTable;
        let lengthExtraTable = RawInflate.LengthExtraTable;
        let distCodeTable = RawInflate.DistCodeTable;
        let distExtraTable = RawInflate.DistExtraTable;
        code = this.readCodeByTable(litlen);
        while (code !== Number_256) {
            // literal
            if (code < Number_256) {
                if (op >= olength) {
                    output = this.expandBufferAdaptive();
                    olength = output.length;
                }
                output[op] = code;
                op += 1;
                continue;
            }
            // length code
            ti = code - Number_257;
            codeLength = lengthCodeTable[ti];
            if (lengthExtraTable[ti] > 0) {
                codeLength += this.readBits(lengthExtraTable[ti]);
            }
            // dist code
            code = this.readCodeByTable(dist);
            codeDist = distCodeTable[code];
            if (distExtraTable[code] > 0) {
                codeDist += this.readBits(distExtraTable[code]);
            }
            // lz77 decode
            if (op + codeLength > olength) {
                output = this.expandBufferAdaptive();
                olength = output.length;
            }
            while (codeLength > 0) {
                output[op] = output[op - codeDist];
                op += 1;
                codeLength -= 1;
            }
            code = this.readCodeByTable(litlen);
        }
        while (this.bitsbuflen >= Number_8) {
            this.bitsbuflen -= 8;
            this.ip -= 1;
        }
        this.op = op;
    }
    expandBufferBlock() {
        let buffer = replace8NumberCount(this.op - RawInflate.MaxBackwardLength);
        let backward = this.op - RawInflate.MaxBackwardLength;
        let output = this.output;
        // copy to output buffer
        buffer.set(output.subarray(RawInflate.MaxBackwardLength, buffer.length));
        this.blocks.push(buffer);
        this.totalpos += buffer.length;
        // copy to backward buffer
        output.set(output.subarray(backward, backward + RawInflate.MaxBackwardLength));
        this.op = RawInflate.MaxBackwardLength;
        return output;
    }
    expandBufferAdaptive(opt_param) {
        let buffer;
        let ratio = (this.input.length / this.ip + 1) | 0;
        let maxHuffCode;
        let newSize;
        let maxInflateSize;
        let input = this.input;
        let output = this.output;
        if (opt_param != null) {
            if (opt_param.fixRatio) {
                ratio = opt_param.fixRatio;
            }
            if (opt_param.addRatio) {
                ratio += opt_param.addRatio;
            }
        }
        // calculate new buffer size
        if (ratio < Number_2) {
            maxHuffCode = (input.length - this.ip) / this.currentLitlenTable[Number_2];
            maxInflateSize = ((maxHuffCode / Number_2) * Number_258) | 0;
            newSize = maxInflateSize < output.length ? output.length + maxInflateSize : output.length << 1;
        }
        else {
            newSize = output.length * ratio;
        }
        // buffer expantion
        buffer = replace8NumberCount(newSize);
        buffer.set(output);
        this.output = buffer;
        return this.output;
    }
    concatBufferBlock() {
        var _a;
        let pos = 0;
        let limit = this.totalpos + (this.op - RawInflate.MaxBackwardLength);
        let output = this.output;
        let blocks = this.blocks;
        let buffer = replace8NumberCount(limit);
        // single buffer
        if (blocks.length === 0) {
            return this.output.subarray(RawInflate.MaxBackwardLength, this.op);
        }
        // copy to buffer
        for (let i = 0; i < blocks.length; ++i) {
            let block = blocks[i];
            for (let j = 0; j < block.length; ++j) {
                buffer[pos] = block[j];
                pos += 1;
            }
        }
        // current buffer
        for (let i = RawInflate.MaxBackwardLength; i < this.op; ++i) {
            buffer[pos] = output[i];
            pos += 1;
        }
        this.blocks = [];
        this.buffer = buffer;
        return (_a = this.buffer) !== null && _a !== void 0 ? _a : new Uint8Array(0);
    }
    concatBufferDynamic() {
        var _a;
        let buffer;
        let op = this.op;
        if (this.resize) {
            buffer = replace8NumberCount(op);
            buffer.set(this.output.subarray(0, op));
        }
        else {
            buffer = this.output.subarray(0, op);
        }
        this.buffer = buffer;
        return (_a = this.buffer) !== null && _a !== void 0 ? _a : new Uint8Array(0);
    }
}
RawInflate.ZLIB_RAW_INFLATE_BUFFER_SIZE = 0x8000;
RawInflate.buildHuffmanTable = Huffman.buildHuffmanTable;
RawInflate.MaxBackwardLength = Number_32768;
RawInflate.MaxCopyLength = Number_258;
RawInflate.order = (() => {
    const table = [16, 17, 18, 0, 8, 7, 9, 6, 10, 5, 11, 4, 12, 3, 13, 2, 14, 1, 15];
    return new Uint16Array(table);
})();
RawInflate.LengthCodeTable = ((table) => {
    return new Uint16Array(table);
})([
    0x0003, 0x0004, 0x0005, 0x0006, 0x0007, 0x0008, 0x0009, 0x000a, 0x000b, 0x000d, 0x000f, 0x0011, 0x0013, 0x0017,
    0x001b, 0x001f, 0x0023, 0x002b, 0x0033, 0x003b, 0x0043, 0x0053, 0x0063, 0x0073, 0x0083, 0x00a3, 0x00c3, 0x00e3,
    0x0102, 0x0102, 0x0102
]);
RawInflate.LengthExtraTable = ((table) => {
    return new Uint8Array(table);
})([0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 2, 2, 2, 2, 3, 3, 3, 3, 4, 4, 4, 4, 5, 5, 5, 5, 0, 0, 0]);
RawInflate.DistCodeTable = ((table) => {
    return new Uint16Array(table);
})([
    0x0001, 0x0002, 0x0003, 0x0004, 0x0005, 0x0007, 0x0009, 0x000d, 0x0011, 0x0019, 0x0021, 0x0031, 0x0041, 0x0061,
    0x0081, 0x00c1, 0x0101, 0x0181, 0x0201, 0x0301, 0x0401, 0x0601, 0x0801, 0x0c01, 0x1001, 0x1801, 0x2001, 0x3001,
    0x4001, 0x6001
]);
RawInflate.DistExtraTable = (() => {
    const table = [0, 0, 0, 0, 1, 1, 2, 2, 3, 3, 4, 4, 5, 5, 6, 6, 7, 7, 8, 8, 9, 9, 10, 10, 11, 11, 12, 12, 13, 13];
    return new Uint8Array(table);
})();
RawInflate.FixedLiteralLengthTable = (() => {
    let lengths = new Uint8Array(replace8NumberCount(Number_288));
    for (let i = 0; i < lengths.length; ++i) {
        lengths[i] = i <= Number_143 ? Number_8 : i <= Number_255 ? Number_9 : i <= Number_279 ? Number_7 : Number_8;
    }
    return RawInflate.buildHuffmanTable(lengths);
})();
RawInflate.FixedDistanceTable = (() => {
    let lengths = new Uint8Array(replace8NumberCount(Number_30));
    for (let i = 0; i < lengths.length; ++i) {
        lengths[i] = 5;
    }
    return RawInflate.buildHuffmanTable(lengths);
})();
