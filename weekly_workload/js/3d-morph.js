"use strict";
/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
const NX_NUMBER = 120;
const LOOP_COUNT = 15;
const NUM_EIGHT = 8;
const NUM_TWO = 2;
const NUM_THREE = 3;
const MAGNIFICATION = 50;
const TIME_UNIT = 1000;
const TEST_COUNT = 80;
class Morph {
    constructor() {
        this.loops = LOOP_COUNT;
        this.nx = NX_NUMBER;
        this.nz = NX_NUMBER;
    }
    morph(arr, f) {
        let PI2nx = (Math.PI * NUM_EIGHT) / this.nx;
        let f30 = -(MAGNIFICATION * Math.sin(f * Math.PI * NUM_TWO));
        for (let i = 0; i < this.nz; i++) {
            for (let j = 0; j < this.nx; j++) {
                let index = NUM_THREE * (i * this.nx + j) + 1;
                let x = (j - 1) * PI2nx;
                arr[index] = Math.sin(x) * -f30;
            }
        }
    }
    runTest() {
        let a = new Float64Array(this.nx * this.nx * NUM_THREE);
        for (let index = 0; index < this.nx * this.nx * NUM_THREE; index++) {
            a[index] = 0;
        }
        for (let i = 0; i < this.loops; i++) {
            this.morph(a, i / this.loops);
        }
        let testOutput = 0;
        for (let i = 0; i < this.nx; i++) {
            let index = NUM_THREE * (i * this.nx + 1) + 1;
            if (index >= a.length) {
                break;
            }
            testOutput += a[index];
        }
        //debugLog("testOutput value is" + testOutput)
        let epsilon = 1e-13;
        if (Math.abs(testOutput) >= epsilon) {
            //debugLog("Error: bad test output: expected magnitude below" + epsilon + "but got" + testOutput)
        }
    }
}
let isdebug = false;
function debugLog(msg) {
    if (isdebug) {
        console.log(msg);
    }
}
/*
 * @State
 * @Tags Jetstream2
 */
class Benchmark {
    /*
     * @Benchmark
     */
    run() {
        let start = Date.now();
        let morph = new Morph();
        for (let i = 0; i < TEST_COUNT; i++) {
            morph.runTest();
        }
        let end = Date.now();
        console.log('3d-morph: ms = ' + (end - start));
    }
}
new Benchmark().run();
