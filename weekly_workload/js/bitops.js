"use strict";
function RunBitOp1() {
    let res = 0;
    let resources = new Int32Array([12, 43, 56, 76, 89, 54, 45, 32, 35, 47, 46, 44, 21, 37, 84]);
    let start = Date.now();
    let resourcesLength = resources.length - 1;
    for (let i = 0; i < 6000000; i++) {
        let bi3b = 0xE994;
        let b = resources[(i & res) & resourcesLength];
        res += 3 & (bi3b >> ((b << 1) & 14));
        res += 3 & (bi3b >> ((b >> 2) & 14));
        res += 3 & (bi3b >> ((b >> 5) & 6));
    }
    let end = Date.now();
    console.log(res);
    let time = (end - start) ;
    console.log("Numerical Calculation - RunBitOp1:\t" + String(time) + "\tms");
    return time;
}
RunBitOp1();

function RunBitOp2() {
    let res = 0;
    let start = Date.now();
    for (let i = 0; i < 6000000; i++) {
        let b = i;
        let m = 1, c = 0;
        while (m < 0x100) {
            if ((b & m) != 0) {
                c += 1;
            }
            m <<= 1;
        }
        res += c;
    }
    let end = Date.now();
    console.log(res);
    let time = (end - start) ;
    console.log("Numerical Calculation - RunBitOp2:\t" + String(time) + "\tms");
    return time;
}
RunBitOp2();
function RunBitOp3() {
    let res = 0;
    let start = Date.now();
    for (let y = 0; y < 6000000; y++) {
        let x = y;
        let r = 0;
        while (x != 0) {
            x &= x - 1;
            ++r;
        }
        res += r;
    }
    let end = Date.now();
    console.log(res);
    let time = (end - start) ;
    console.log("Numerical Calculation - RunBitOp3:\t" + String(time) + "\tms");
    return time;
}
RunBitOp3();
function RunBitOp4() {
    let results = new Int32Array([3, 53, 76, 37, 82, 23, 66, 17, 82, 43, 77, 93, 28, 24, 85]);
    let resources = new Int32Array([12, 43, 56, 76, 89, 54, 45, 32, 35, 47, 46, 44, 21, 37, 84]);
    let res = 0;
    let start = Date.now();
    let resultsLength = results.length - 1;
    let resourcesLength = resources.length - 1;
    for (let i = 0; i < 6000000; i++) {
        res |= ~(1 << (resources[i & resourcesLength] ^ 31));
    }
    let end = Date.now();
    console.log(res);
    let time = (end - start) ;
    console.log("Numerical Calculation - RunBitOp4:\t" + String(time) + "\tms");
    return time;
}
RunBitOp4();
