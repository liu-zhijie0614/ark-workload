/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { File } from './file';
import { registerPattern, fprPattern } from './registers';
import { instructionSet } from './instructions';
import { Node, StructOffset, Immediate, Sizeof, AddImmediates, SubImmediates, MulImmediates, NegImmediate, OrImmediates, AndImmediates, XorImmediates, BitnotImmediate, RegisterID, FPRegisterID, Variable, Address, BaseIndex, AbsoluteAddress, Instruction, Error, ConstExpr, ConstDecl, Label, LocalLabel, LabelReference, LocalLabelReference, Sequence, True, False, Setting, And, Or, Not, StringLiteral, IfThenElse, Macro, MacroCall, SourceFile, CodeOrigin } from './ast';
let debug = false;
function DeBugLog(msg) {
    if (debug) {
        console.log(msg);
    }
}
let GlobalAnnotation = 'global';
let LocalAnnotation = 'local';
/*
 * Create IncludeFile File Class Splice
 */
export class IncludeFile extends Node {
    constructor(moduleName, defaultDir) {
        super(null);
        this.includeDirs = '';
        this.fileName = '';
        this.fileName = moduleName + '.asm';
    }
    toString() {
        return this.fileName;
    }
}
/*
 * Define Token Class
 */
export class Token {
    constructor(codeOrigi, string) {
        this.codeOrigin = codeOrigi;
        this.string = string;
    }
    isEqualTo(other) {
        if (other instanceof Token) {
            return this.string === other.string;
        }
        else {
            return this.string === other;
        }
    }
    isNotEqualTo(other) {
        return !this.isEqualTo(other);
    }
    toString() {
        return '' + this.string + '\\at ' + this.codeOrigin.toString();
    }
    parseError(comment) {
        console.log('Parse error:' + comment);
    }
}
/*
 * Define Annotation class
 */
export class Annotation {
    constructor(codeOrigi, typ, strin) {
        this.string = '';
        this.codeOrigin = codeOrigi;
        this.type = typ;
        this.string = strin;
    }
    isEqualTo(other) {
        if (other instanceof Token) {
            return this.string === other.string;
        }
        else {
            return this.string === other;
        }
    }
    isNotEqualTo(other) {
        return !this.isEqualTo(other);
    }
    toString() {
        return '' + this.string + 'at' + this.codeOrigin.toString();
    }
    parseError(comment) {
        console.log('Annotation error:' + comment);
    }
}
/*
 * Returns a Token Array
 */
function lex(strs, file) {
    // Regex gets a data array containing a character
    let scanRegExp = (source, regexp) => {
        return source.match(regexp);
    };
    // The lexer. Takes a string and returns an array of tokens.
    let result = [];
    let lineNumber = 1;
    let whitespaceFound = false;
    let str = strs;
    while (str.length) {
        let tokenMatch;
        let annotation = null;
        let annotationType = '';
        if ((tokenMatch = scanRegExp(str, '^#([^\n]*)'))) {
            // comment, ignore
            tokenMatch = tokenMatch;
        }
        else if ((tokenMatch = scanRegExp(str, '^// ?([^\n]*)'))) {
            // annotation
            tokenMatch = tokenMatch;
            annotation = tokenMatch[0];
            annotationType = whitespaceFound
                ? LocalAnnotation
                : GlobalAnnotation;
        }
        else if ((tokenMatch = scanRegExp(str, '^\n'))) {
            /* We've found a '\n'.  Emit the last comment recorded if appropriate:
             * We need to parse annotations regardless of whether the backend does
             * anything with them or not. This is because the C++ backend may make
             * use of this for its cloopDo debugging utility even if
             * enableInstrAnnotations is not enabled.
             */
            tokenMatch = tokenMatch;
            if (annotation) {
                result.push(new Annotation(new CodeOrigin(file, lineNumber), annotationType, annotation));
                annotation = null;
            }
            result.push(new Token(new CodeOrigin(file, lineNumber), tokenMatch[0]));
            lineNumber += 1;
        }
        else if ((tokenMatch = scanRegExp(str, '^[a-zA-Z]([a-zA-Z0-9_.]*)'))) {
            tokenMatch = tokenMatch;
            result.push(new Token(new CodeOrigin(file, lineNumber), tokenMatch[0]));
        }
        else if ((tokenMatch = scanRegExp(str, '^\\.([a-zA-Z0-9_]*)'))) {
            tokenMatch = tokenMatch;
            result.push(new Token(new CodeOrigin(file, lineNumber), tokenMatch[0]));
        }
        else if ((tokenMatch = scanRegExp(str, '^_([a-zA-Z0-9_]*)'))) {
            tokenMatch = tokenMatch;
            result.push(new Token(new CodeOrigin(file, lineNumber), tokenMatch[0]));
        }
        else if ((tokenMatch = scanRegExp(str, '^([ \t]+)'))) {
            // whitespace, ignore
            whitespaceFound = true;
            str = str.slice(tokenMatch[0].length);
            continue;
        }
        else if ((tokenMatch = scanRegExp(str, '^0x([0-9a-fA-F]+)'))) {
            tokenMatch = tokenMatch;
            result.push(new Token(new CodeOrigin(file, lineNumber), Number.parseInt(tokenMatch[1], 16).toString()));
        }
        else if ((tokenMatch = scanRegExp(str, '^0([0-7]+)'))) {
            tokenMatch = tokenMatch;
            result.push(new Token(new CodeOrigin(file, lineNumber), Number.parseInt(tokenMatch[1], 8).toString()));
        }
        else if ((tokenMatch = scanRegExp(str, '^([0-9]+)'))) {
            tokenMatch = tokenMatch;
            result.push(new Token(new CodeOrigin(file, lineNumber), tokenMatch[0]));
        }
        else if ((tokenMatch = scanRegExp(str, '^::'))) {
            // Match with double colons:: Text line at the beginning
            tokenMatch = tokenMatch;
            result.push(new Token(new CodeOrigin(file, lineNumber), tokenMatch[0]));
        }
        else if ((tokenMatch = scanRegExp(str, '^[:,\\(\\)\\[\\]=\\+\\-~\\|&^*]'))) {
            // ^[:,\(\)\[\]=\+\-~\|&^*] Matches strings starting with colon, comma, parenthesis, square brackets, equals, plus, minus, tilde, pipe, and, or characters, or asterisks
            tokenMatch = tokenMatch;
            result.push(new Token(new CodeOrigin(file, lineNumber), tokenMatch[0]));
        }
        else if ((tokenMatch = scanRegExp(str, '^".*"'))) {
            // Matches strings enclosed in double quotes
            tokenMatch = tokenMatch;
            result.push(new Token(new CodeOrigin(file, lineNumber), tokenMatch[0]));
        }
        else {
            console.log(`Lexer error at ${new CodeOrigin(file, lineNumber)}`);
        }
        whitespaceFound = false;
        str = str.slice(tokenMatch[0].length);
    }
    // if (debug) {
    //   for (let i = 0; i < result.length; i++) {
    // DeBugLog(i + ' line in result: ' + result[i].toString());
    //   }
    // }
    return result;
}
// Token identification.
function isRegister(token) {
    return registerPattern.test(token.string);
}
function isInstruction(token) {
    return instructionSet.has(token.string);
}
function isKeyword(token) {
    return (RegExp('^((true)|(false)|(if)|(then)|(else)|(elsif)|(end)|(and)|(or)|(not)|(global)|(macro)|(const)|(constexpr)|(sizeof)|(error)|(include))$').test(token.string) ||
        isRegister(token) ||
        isInstruction(token));
}
function isIdentifier(token) {
    return (RegExp('^[a-zA-Z]([a-zA-Z0-9_.]*)$').test(token.string) &&
        !isKeyword(token));
}
function isLabel(token) {
    let tokenString;
    if (token instanceof Token) {
        tokenString = token.string;
    }
    else {
        tokenString = token;
    }
    // Matches regular expressions that start with an underscore followed by letters, numbers, or underscores
    return RegExp('^_([a-zA-Z0-9_]*)$').test(tokenString);
}
function isLocalLabel(token) {
    let tokenString;
    if (token instanceof Token) {
        tokenString = token.string;
    }
    else {
        tokenString = token;
    }
    return RegExp('^\\.([a-zA-Z0-9_]*)$').test(tokenString);
}
function isVariable(token) {
    return isIdentifier(token) || isRegister(token);
}
function isInteger(token) {
    return RegExp('^[0-9]').test(token.string);
}
function isString(token) {
    return RegExp('^".*"').test(token.string);
}
// The parser. Takes an array of tokens and returns an AST. Methods
// other than parse(tokens) are not for public consumption.
export class Parser {
    constructor(data, fileName) {
        this.tokens = Array();
        this.idx = 0;
        this.tokens = lex(data, fileName);
        this.idx = 0;
        this.annotation = null;
    }
    parseError(comment) {
        if (this.tokens[this.idx] != null) {
            this.tokens[this.idx].parseError(comment);
        }
        else {
            if (comment != null) {
                console.log('Parse error at end of file');
            }
            else {
                console.log('Parse error at end of file: ' + comment);
            }
        }
    }
    consume(regexp) {
        if (regexp) {
            if (!RegExp(regexp).test(this.tokens[this.idx].string)) {
                this.parseError(null);
            }
        }
        else if (this.idx != this.tokens.length) {
            this.parseError(null);
        }
        this.idx += 1;
    }
    skipNewLine() {
        while (this.tokens[this.idx].isEqualTo('\n')) {
            this.idx += 1;
        }
    }
    parsePredicateAtom() {
        if (this.tokens[this.idx].isEqualTo('not')) {
            let codeOrigin = this.tokens[this.idx].codeOrigin;
            this.idx += 1;
            let par = this.parsePredicateAtom();
            if (par) {
                return new Not(codeOrigin, par);
            }
        }
        if (this.tokens[this.idx].isEqualTo('(')) {
            this.idx += 1;
            this.skipNewLine();
            let result = this.parsePredicate();
            if (this.tokens[this.idx].isNotEqualTo(')')) {
                this.parseError(null);
            }
            this.idx += 1;
            return result;
        }
        if (this.tokens[this.idx].isEqualTo('true')) {
            let result = True.instance();
            this.idx += 1;
            return result;
        }
        if (this.tokens[this.idx].isEqualTo('false')) {
            let result = False.instance();
            this.idx += 1;
            return result;
        }
        if (isIdentifier(this.tokens[this.idx])) {
            let result = Setting.forName(this.tokens[this.idx].codeOrigin, this.tokens[this.idx].string);
            this.idx += 1;
            return result;
        }
        this.parseError(null);
        return null;
    }
    parsePredicateAnd() {
        let result = this.parsePredicateAtom();
        while (this.tokens[this.idx].isEqualTo('and')) {
            let codeOrigin = this.tokens[this.idx].codeOrigin;
            this.idx += 1;
            this.skipNewLine();
            let right = this.parsePredicateAtom();
            if (right != null) {
                result = new And(codeOrigin, result, right);
            }
        }
        return result;
    }
    parsePredicate() {
        // some examples of precedence:
        // not a and b -> (not a) and b
        // a and b or c -> (a and b) or c
        // a or b and c -> a or (b and c)
        let result = this.parsePredicateAnd();
        while (this.tokens[this.idx].isEqualTo('or')) {
            let codeOrigin = this.tokens[this.idx].codeOrigin;
            this.idx += 1;
            this.skipNewLine();
            let right = this.parsePredicateAnd();
            if (result != null && right != null) {
                result = new Or(codeOrigin, result, right);
            }
        }
        return result;
    }
    parseVariable() {
        let result = new Node(null);
        if (isRegister(this.tokens[this.idx])) {
            if (fprPattern.test(this.tokens[this.idx].toString())) {
                result = FPRegisterID.forName(this.tokens[this.idx].codeOrigin, this.tokens[this.idx].string);
            }
            else {
                result = RegisterID.forName(this.tokens[this.idx].codeOrigin, this.tokens[this.idx].string);
            }
        }
        else if (isIdentifier(this.tokens[this.idx])) {
            result = Variable.forName(this.tokens[this.idx].codeOrigin, this.tokens[this.idx].string);
        }
        else {
            this.parseError(null);
        }
        this.idx += 1;
        return result;
    }
    // Resolution address
    parseAddress(offset) {
        if (this.tokens[this.idx].isNotEqualTo('[')) {
            this.parseError(null);
        }
        let codeOrigin = this.tokens[this.idx].codeOrigin;
        let result;
        // Three possibilities:
        // []       -> AbsoluteAddress
        // [a]      -> Address
        // [a,b]    -> BaseIndex with scale = 1
        // [a,b,c]  -> BaseIndex
        this.idx += 1;
        if (this.tokens[this.idx].isEqualTo(']')) {
            this.idx += 1;
            return new AbsoluteAddress(codeOrigin, offset);
        }
        let a = this.parseVariable();
        if (this.tokens[this.idx].isEqualTo(']')) {
            result = new Address(codeOrigin, a, offset);
        }
        else {
            if (this.tokens[this.idx].isNotEqualTo(',')) {
                this.parseError(null);
            }
            this.idx += 1;
            let b = this.parseVariable();
            if (this.tokens[this.idx].isEqualTo(']')) {
                result = new BaseIndex(codeOrigin, a, b, 1, offset);
            }
            else {
                if (this.tokens[this.idx].isNotEqualTo(',')) {
                    this.parseError(null);
                }
                this.idx += 1;
                if (!['1', '2', '4', '8'].includes(this.tokens[this.idx].string)) {
                    this.parseError(null);
                }
                let c = Number.parseInt(this.tokens[this.idx].string);
                this.idx += 1;
                if (this.tokens[this.idx].isNotEqualTo(']')) {
                    this.parseError(null);
                }
                result = new BaseIndex(codeOrigin, a, b, c, offset);
            }
        }
        this.idx += 1;
        return result;
    }
    parseColonColon() {
        this.skipNewLine();
        let firstToken = this.tokens[this.idx];
        let codeOrigin = this.tokens[this.idx].codeOrigin;
        if (isIdentifier(firstToken) == false) {
            this.parseError(null);
        }
        let names = [this.tokens[this.idx].string];
        this.idx += 1;
        while (this.tokens[this.idx].isEqualTo('::')) {
            this.idx += 1;
            if (isIdentifier(this.tokens[this.idx]) == false) {
                this.parseError(null);
            }
            names.push(this.tokens[this.idx].string);
            this.idx += 1;
        }
        if (names.length == 0) {
            firstToken.parseError(null);
        }
        return new Map([
            ['codeOrigin', codeOrigin],
            ['names', names]
        ]);
    }
    parseTextInParens() {
        this.skipNewLine();
        let codeOrigin = this.tokens[this.idx].codeOrigin;
        if (this.tokens[this.idx].isNotEqualTo('(')) {
            console.log('Missing "(" at ' + codeOrigin);
        }
        this.idx += 1;
        // need at least one item
        if (this.tokens[this.idx].isEqualTo(')')) {
            console.log('No items in list at ' + codeOrigin);
        }
        let numEnclosedParens = 0;
        let text = Array();
        while (this.tokens[this.idx].isNotEqualTo(')') ||
            numEnclosedParens > 0) {
            if (this.tokens[this.idx].isEqualTo('(')) {
                numEnclosedParens += 1;
            }
            else if (this.tokens[this.idx].isEqualTo(')')) {
                numEnclosedParens -= 1;
            }
            text.push(this.tokens[this.idx].string);
            this.idx += 1;
        }
        this.idx += 1;
        return new Map([
            ['codeOrigin', codeOrigin],
            ['text', text]
        ]);
    }
    parseExpressionAtom() {
        let result;
        this.skipNewLine();
        if (this.tokens[this.idx].isEqualTo('-')) {
            this.idx += 1;
            return new NegImmediate(this.tokens[this.idx - 1].codeOrigin, this.parseExpressionAtom());
        }
        if (this.tokens[this.idx].isEqualTo('~')) {
            this.idx += 1;
            return new BitnotImmediate(this.tokens[this.idx - 1].codeOrigin, this.parseExpressionAtom());
        }
        if (this.tokens[this.idx].isEqualTo('(')) {
            this.idx += 1;
            result = this.parseExpression();
            if (this.tokens[this.idx].isNotEqualTo(')')) {
                this.parseError(null);
            }
            this.idx += 1;
            return result;
        }
        if (isInteger(this.tokens[this.idx])) {
            result = new Immediate(this.tokens[this.idx].codeOrigin, Number(this.tokens[this.idx].string));
            this.idx += 1;
            return result;
        }
        if (isString(this.tokens[this.idx])) {
            result = new StringLiteral(this.tokens[this.idx].codeOrigin, this.tokens[this.idx].string);
            this.idx += 1;
            return result;
        }
        if (isIdentifier(this.tokens[this.idx])) {
            let dic = this.parseColonColon();
            let names = dic.get('names');
            if (names.length > 1) {
                return StructOffset.forField(dic['codeOrigin'], names.slice(0, -1).join('::'), names[names.length - 1]);
            }
            return Variable.forName(dic['codeOrigin'], names[0]);
        }
        if (isRegister(this.tokens[this.idx])) {
            return this.parseVariable();
        }
        if (this.tokens[this.idx].isEqualTo('sizeof')) {
            this.idx += 1;
            let dic = this.parseColonColon();
            let co = dic.get('codeOrigin');
            let names = dic.get('names');
            return Sizeof.forName(co, names.join('::'));
        }
        if (this.tokens[this.idx].isEqualTo('constexpr')) {
            this.idx += 1;
            this.skipNewLine();
            let codeOrigin;
            let text;
            let names;
            let textStr = '';
            if (this.tokens[this.idx].isEqualTo('(')) {
                let dic = this.parseTextInParens();
                codeOrigin = dic.get('codeOrigin');
                text = dic.get('text');
                textStr = text.join('');
            }
            else {
                let dic = this.parseColonColon();
                codeOrigin = dic.get('codeOrigin');
                names = dic.get('names');
                textStr = names.join('::');
            }
            return ConstExpr.forName(codeOrigin, textStr);
        }
        if (isLabel(this.tokens[this.idx])) {
            result = new LabelReference(this.tokens[this.idx].codeOrigin, Label.forName(this.tokens[this.idx].codeOrigin, this.tokens[this.idx].string));
            this.idx += 1;
            return result;
        }
        if (isLocalLabel(this.tokens[this.idx])) {
            result = new LocalLabelReference(this.tokens[this.idx].codeOrigin, LocalLabel.forName(this.tokens[this.idx].codeOrigin, this.tokens[this.idx].string));
            this.idx += 1;
            return result;
        }
        this.parseError(null);
        return null;
    }
    parseExpressionMul() {
        this.skipNewLine();
        let result = this.parseExpressionAtom();
        while (this.tokens[this.idx].isEqualTo('*')) {
            if (this.tokens[this.idx].isEqualTo('*')) {
                this.idx += 1;
                result = new MulImmediates(this.tokens[this.idx - 1].codeOrigin, result, this.parseExpressionAtom());
            }
            else {
                console.log('Invalid token ' +
                    this.tokens[this.idx] +
                    ' in multiply expression');
            }
        }
        return result;
    }
    couldBeExpression() {
        return (this.tokens[this.idx].isEqualTo('-') ||
            this.tokens[this.idx].isEqualTo('~') ||
            this.tokens[this.idx].isEqualTo('constexpr') ||
            this.tokens[this.idx].isEqualTo('sizeof') ||
            isInteger(this.tokens[this.idx]) ||
            isString(this.tokens[this.idx]) ||
            isVariable(this.tokens[this.idx]) ||
            this.tokens[this.idx].isEqualTo('('));
    }
    parseExpressionAdd() {
        this.skipNewLine();
        let result = this.parseExpressionMul();
        while (this.tokens[this.idx].isEqualTo('+') ||
            this.tokens[this.idx].isEqualTo('-')) {
            if (this.tokens[this.idx].isEqualTo('+') == true) {
                this.idx += 1;
                result = new AddImmediates(this.tokens[this.idx - 1].codeOrigin, result, this.parseExpressionMul());
            }
            else if (this.tokens[this.idx].isEqualTo('-')) {
                this.idx += 1;
                result = new SubImmediates(this.tokens[this.idx - 1].codeOrigin, result, this.parseExpressionMul());
            }
            else {
                console.log('Invalid token ' +
                    this.tokens[this.idx] +
                    ' in addition expression');
            }
        }
        return result;
    }
    parseExpressionAnd() {
        this.skipNewLine();
        let result = this.parseExpressionAdd();
        while (this.tokens[this.idx].isEqualTo('&')) {
            this.idx += 1;
            result = new AndImmediates(this.tokens[this.idx - 1].codeOrigin, result, this.parseExpressionAdd());
        }
        return result;
    }
    parseExpression() {
        this.skipNewLine();
        let result = this.parseExpressionAnd();
        while (this.tokens[this.idx].isEqualTo('|') ||
            this.tokens[this.idx].isEqualTo('^')) {
            if (this.tokens[this.idx].isEqualTo('|')) {
                this.idx += 1;
                result = new OrImmediates(this.tokens[this.idx - 1].codeOrigin, result, this.parseExpressionAnd());
            }
            else if (this.tokens[this.idx].isEqualTo('^')) {
                this.idx += 1;
                result = new XorImmediates(this.tokens[this.idx - 1].codeOrigin, result, this.parseExpressionAnd());
            }
            else {
                console.log('Invalid token ' + this.tokens[this.idx] + ' in expression');
            }
        }
        return result;
    }
    parseOperand(comment) {
        this.skipNewLine();
        if (this.couldBeExpression()) {
            let expr = this.parseExpression();
            if (this.tokens[this.idx].isEqualTo('[')) {
                return this.parseAddress(expr);
            }
            return expr;
        }
        if (this.tokens[this.idx].isEqualTo('[')) {
            return this.parseAddress(new Immediate(this.tokens[this.idx].codeOrigin, 0));
        }
        if (isLabel(this.tokens[this.idx])) {
            let result = new LabelReference(this.tokens[this.idx].codeOrigin, Label.forName(this.tokens[this.idx].codeOrigin, this.tokens[this.idx].string));
            this.idx += 1;
            return result;
        }
        if (isLocalLabel(this.tokens[this.idx])) {
            let result = new LocalLabelReference(this.tokens[this.idx].codeOrigin, LocalLabel.forName(this.tokens[this.idx].codeOrigin, this.tokens[this.idx].string));
            this.idx += 1;
            return result;
        }
        this.parseError(comment);
        return new Node(null);
    }
    parseMacroVariables() {
        this.skipNewLine();
        this.consume('^\\($');
        let variables = Array();
        while (true) {
            this.skipNewLine();
            if (this.tokens[this.idx].isEqualTo(')')) {
                this.idx += 1;
                break;
            }
            else if (isIdentifier(this.tokens[this.idx])) {
                variables.push(Variable.forName(this.tokens[this.idx].codeOrigin, this.tokens[this.idx].string));
                this.idx += 1;
                this.skipNewLine();
                if (this.tokens[this.idx].isEqualTo(')')) {
                    this.idx += 1;
                    break;
                }
                else if (this.tokens[this.idx].isEqualTo(',') == true) {
                    this.idx += 1;
                }
                else {
                    this.parseError(null);
                }
            }
            else {
                this.parseError(null);
            }
        }
        return variables;
    }
    parseSequence(final, comment) {
        let firstCodeOrigin = this.tokens[this.idx].codeOrigin;
        let list = Array();
        while (true) {
            if ((this.idx == this.tokens.length && !final) ||
                (final && RegExp(final).test(this.tokens[this.idx].string))) {
                break;
            }
            else if (this.tokens[this.idx] instanceof Annotation) {
                // This is the only place where we can encounter a global
                // annotation, and hence need to be able to distinguish between
                // them.
                // globalAnnotations are the ones that start from column 0. All
                // others are considered localAnnotations.  The only reason to
                // distinguish between them is so that we can format the output
                // nicely as one would expect.
                let codeOrigin = this.tokens[this.idx].codeOrigin;
                list.push(new Instruction(codeOrigin, 'localAnnotation', [], this.tokens[this.idx].string));
                this.annotation = null;
                this.idx += 2;
            }
            else if (this.tokens[this.idx].isEqualTo('\n')) {
                this.idx += 1;
            }
            else if (this.tokens[this.idx].isEqualTo('const')) {
                this.idx += 1;
                if (!isVariable(this.tokens[this.idx])) {
                    this.parseError(null);
                }
                let variable = Variable.forName(this.tokens[this.idx].codeOrigin, this.tokens[this.idx].string);
                this.idx += 1;
                if (this.tokens[this.idx].isNotEqualTo('=')) {
                    this.parseError(null);
                }
                this.idx += 1;
                let value = this.parseOperand('while inside of const' + variable.name);
                list.push(new ConstDecl(this.tokens[this.idx].codeOrigin, variable, value));
            }
            else if (this.tokens[this.idx].isEqualTo('error')) {
                list.push(new Error(this.tokens[this.idx].codeOrigin));
                this.idx += 1;
            }
            else if (this.tokens[this.idx].isEqualTo('if') == true) {
                let codeOrigin = this.tokens[this.idx].codeOrigin;
                this.idx += 1;
                this.skipNewLine();
                let predicate = this.parsePredicate();
                this.consume('^((then)|(\n))$');
                this.skipNewLine();
                let ifThenElse = new IfThenElse(codeOrigin, predicate, this.parseSequence('^((else)|(end)|(elsif))$', 'while inside of "if ' + predicate.dump() + '"'));
                list.push(ifThenElse);
                while (this.tokens[this.idx].isEqualTo('elsif')) {
                    let codeOrigin = this.tokens[this.idx].codeOrigin;
                    this.idx += 1;
                    this.skipNewLine();
                    predicate = this.parsePredicate();
                    this.consume('^((then)|(\n))$');
                    this.skipNewLine();
                    if (predicate != null) {
                        let elseCase = new IfThenElse(codeOrigin, predicate, this.parseSequence('^((else)|(end)|(elsif))$', 'while inside of "if ' + predicate.dump() + '"'));
                        ifThenElse.elseCase = elseCase;
                        ifThenElse = elseCase;
                    }
                }
                if (this.tokens[this.idx].isEqualTo('else')) {
                    this.idx += 1;
                    ifThenElse.elseCase = this.parseSequence('^end$', 'while inside of else case for "if ' + predicate.dump() + '"');
                    this.idx += 1;
                }
                else {
                    if (this.tokens[this.idx].isNotEqualTo('end')) {
                        this.parseError(null);
                    }
                    this.idx += 1;
                }
            }
            else if (this.tokens[this.idx].isEqualTo('macro')) {
                let codeOrigin = this.tokens[this.idx].codeOrigin;
                this.idx += 1;
                this.skipNewLine();
                if (!isIdentifier(this.tokens[this.idx])) {
                    this.parseError(null);
                }
                let name = this.tokens[this.idx].string;
                this.idx += 1;
                let variables = this.parseMacroVariables();
                let body = this.parseSequence('^end$', ('while inside of macro ' + name));
                this.idx += 1;
                list.push(new Macro(codeOrigin, name, variables, body));
            }
            else if (this.tokens[this.idx].isEqualTo('global')) {
                let codeOrigin = this.tokens[this.idx].codeOrigin;
                this.idx += 1;
                this.skipNewLine();
                let name = this.tokens[this.idx].string;
                this.idx += 1;
                Label.setAsGlobal(codeOrigin, name);
            }
            else if (isInstruction(this.tokens[this.idx])) {
                let codeOrigin = this.tokens[this.idx].codeOrigin;
                let name = this.tokens[this.idx].string;
                this.idx += 1;
                if ((!final && this.idx == this.tokens.length) ||
                    (final && RegExp(final).test(this.tokens[this.idx].toString()))) {
                    // Zero operand instruction, and it's the last one.
                    list.push(new Instruction(codeOrigin, name, [], this.annotation));
                    this.annotation = null;
                    break;
                }
                else if (this.tokens[this.idx] instanceof Annotation) {
                    list.push(new Instruction(codeOrigin, name, [], this.tokens[this.idx].string));
                    this.annotation = null;
                    this.idx += 2; // Consume the newline as well.
                }
                else if (this.tokens[this.idx].isEqualTo('\n')) {
                    // Zero operand instruction.
                    list.push(new Instruction(codeOrigin, name, [], this.annotation));
                    this.annotation = null;
                    this.idx += 1;
                }
                else {
                    // It's definitely an instruction, and it has at least one operand.
                    let operands = Array();
                    let endOfSequence = false;
                    while (true) {
                        operands.push(this.parseOperand('while inside of instruction ' + name));
                        if ((!final && this.idx == this.tokens.length) ||
                            (final && RegExp(final).test(this.tokens[this.idx].string))) {
                            // The end of the instruction and of the sequence.
                            endOfSequence = true;
                            break;
                        }
                        else if (this.tokens[this.idx].isEqualTo(',')) {
                            // Has another operand.
                            this.idx += 1;
                        }
                        else if (this.tokens[this.idx] instanceof Annotation) {
                            this.annotation = this.tokens[this.idx].string;
                            this.idx += 2; // Consume the newline as well.
                            break;
                        }
                        else if (this.tokens[this.idx].isEqualTo('\n')) {
                            // The end of the instruction.
                            this.idx += 1;
                            break;
                        }
                        else {
                            console.log('Expected a comma, newline, or ' +
                                final +
                                ' after ' +
                                operands[operands.length - 1].dump());
                        }
                    }
                    list.push(new Instruction(codeOrigin, name, operands, this.annotation));
                    this.annotation = null;
                    if (endOfSequence) {
                        break;
                    }
                }
            }
            else if (isIdentifier(this.tokens[this.idx])) {
                // Check for potential macro invocation:
                let codeOrigin = this.tokens[this.idx].codeOrigin;
                let name = this.tokens[this.idx].string;
                this.idx += 1;
                if (this.tokens[this.idx].isEqualTo('(')) {
                    // Macro invocation.
                    this.idx += 1;
                    let operands = Array();
                    this.skipNewLine();
                    if (this.tokens[this.idx].isEqualTo(')')) {
                        this.idx += 1;
                    }
                    else {
                        while (true) {
                            this.skipNewLine();
                            if (this.tokens[this.idx].isEqualTo('macro')) {
                                // It's a macro lambda!
                                let codeOriginInner = this.tokens[this.idx].codeOrigin;
                                this.idx += 1;
                                let variables = this.parseMacroVariables();
                                let body = this.parseSequence('^end$', 'while inside of anonymous macro passed as argument to ' +
                                    name);
                                this.idx += 1;
                                operands.push(new Macro(codeOriginInner, '', variables, body));
                            }
                            else {
                                operands.push(this.parseOperand('while inside of macro call to ' + name));
                            }
                            this.skipNewLine();
                            if (this.tokens[this.idx].isEqualTo(')')) {
                                this.idx += 1;
                                break;
                            }
                            else if (this.tokens[this.idx].isEqualTo(',')) {
                                this.idx += 1;
                            }
                            else {
                                console.log('Unexpected ' +
                                    this.tokens[this.idx].string +
                                    ' while parsing invocation of macro ' +
                                    name);
                            }
                        }
                    }
                    if (this.tokens[this.idx] instanceof Annotation) {
                        this.annotation = this.tokens[this.idx].string;
                        this.idx += 2;
                    }
                    list.push(new MacroCall(codeOrigin, name, operands, this.annotation));
                    this.annotation = null;
                }
                else {
                    console.log('Expected "(" after ' + name);
                }
            }
            else if (isLabel(this.tokens[this.idx]) ||
                isLocalLabel(this.tokens[this.idx])) {
                let codeOrigin = this.tokens[this.idx].codeOrigin;
                let name = this.tokens[this.idx].string;
                this.idx += 1;
                if (this.tokens[this.idx].isNotEqualTo(':')) {
                    this.parseError(null);
                }
                if (isLabel(name)) {
                    list.push(Label.forName(codeOrigin, name, true));
                }
                else {
                    list.push(LocalLabel.forName(codeOrigin, name));
                }
                this.idx += 1;
            }
            else if (this.tokens[this.idx].isEqualTo('include')) {
                this.idx += 1;
                if (!isIdentifier(this.tokens[this.idx])) {
                    this.parseError(null);
                }
                let moduleName = this.tokens[this.idx].string;
                let fileName = new IncludeFile(moduleName, this.tokens[this.idx].codeOrigin.fileName()).fileName;
                this.idx += 1;
                list.push(parse(fileName));
            }
            else {
                console.log('Expecting terminal ' + final + ' ' + comment);
            }
        }
        return new Sequence(firstCodeOrigin, list);
    }
    parseIncludes(final, comment) {
        let firstCodeOrigin = this.tokens[this.idx].codeOrigin;
        let fileList = Array();
        fileList.push(this.tokens[this.idx].codeOrigin.fileName());
        while (true) {
            if ((this.idx == this.tokens.length && !final) ||
                (final && final.test(this.tokens[this.idx].toString()))) {
                break;
            }
            else if (this.tokens[this.idx].isEqualTo('include')) {
                this.idx += 1;
                if (!isIdentifier(this.tokens[this.idx])) {
                    this.parseError(null);
                }
                let moduleName = this.tokens[this.idx].string;
                let fileName = new IncludeFile(moduleName, this.tokens[this.idx].codeOrigin.fileName()).fileName;
                this.idx += 1;
                fileList.push(fileName);
            }
            else {
                this.idx += 1;
            }
        }
        return fileList;
    }
}
function parseData(data, fileName) {
    let parser = new Parser(data, new SourceFile(fileName, null, null));
    return parser.parseSequence(null, '');
}
export function parse(fileName) {
    // DeBugLog("Load fileName: "+ fileName)
    return parseData(File.open(fileName).read(), fileName);
}
