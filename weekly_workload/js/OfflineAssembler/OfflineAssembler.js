/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { resetAST } from './ast';
import { parse } from './parser';
import LowLevelInterpreter from './LowLevelInterpreter';
import LowLevelInterpreter32_64 from './LowLevelInterpreter32_64';
import LowLevelInterpreter64 from './LowLevelInterpreter64';
import InitBytecodes from './InitBytecodes';
import { expectedASTDumpedAsLines } from './expected';
('use strict');
const ONE_THOUSAND = 1000;
/**
 * @State
 */
class Benchmark {
    constructor() {
        this.ast = null;
    }
    /**
     * @Setup
     */
    setup() {
        InitBytecodes();
        LowLevelInterpreter();
        LowLevelInterpreter64();
        LowLevelInterpreter32_64();
    }
    /**
     * @Benchmark
     */
    runIteration() {
        resetAST();
        this.ast = parse('LowLevelInterpreter.asm');
    }
    validate() {
        let astDumpedAsLines = this.ast.dump().split('\n');
        if (astDumpedAsLines.length != expectedASTDumpedAsLines.length) {
            throw new Error('Actual number of lines (' +
                astDumpedAsLines.length +
                ') differs from expected number of lines(' +
                expectedASTDumpedAsLines.length +
                ')');
        }
        let index = 0;
        for (let line of astDumpedAsLines) {
            let expectedLine = expectedASTDumpedAsLines[index];
            if (line != expectedLine) {
                throw new Error('Line #' +
                    (index + 1) +
                    ' differs.  Expected: "' +
                    expectedLine +
                    '", got "' +
                    line +
                    '"');
            }
            index += 1;
        }
    }
}
function run() {
    let startTime = Date.now();
    let benchmark = new Benchmark();
    benchmark.setup();
    benchmark.runIteration();
    benchmark.validate();
    let endTime = Date.now();
    console.log('OfflineAssembler: ms = ', endTime - startTime);
}
run();
