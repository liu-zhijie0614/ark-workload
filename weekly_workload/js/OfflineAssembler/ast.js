/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
const TWO = 2;
const THREE = 3;
const FOUR = 4;
const EIGHT = 8;
export class SourceFile {
    constructor(fileNam, fileNams, fileNumbe) {
        this.fileNames = Array();
        this.fileName = fileNam;
        let fileNum = this.fileNames.indexOf(fileNam);
        if (fileNum == null) {
            this.fileNames.push(this.fileName);
            fileNum = this.fileNames.length;
        }
        else {
            fileNum += 1; // File numbers are 1 based
        }
        this.fileNumber = fileNum;
    }
    name() {
        return this.fileName;
    }
}
/*
 * Define CodeOrigin class for w file path data processing
 */
export class CodeOrigin {
    constructor(sourceFil, lineNumbe) {
        this.sourceFile = sourceFil;
        this.lineNumber = lineNumbe;
    }
    fileName() {
        return this.sourceFile.name();
    }
    toString() {
        return this.fileName() + ':' + String(this.lineNumber);
    }
}
export class Node {
    constructor(codeOrig) {
        this.isAddress = false;
        this.isLabel = false;
        this.isImmediate = false;
        this.isImmediateOperand = false;
        this.isRegister = false;
        this.codeOrigin = codeOrig;
    }
    codeOriginString() {
        return this.codeOrigin != null ? this.codeOrigin.toString() : '';
    }
    children() {
        return new Array();
    }
    dump() {
        return this.codeOriginString();
    }
    value() {
        return '';
    }
}
/*
 * NoChild class, that is, leaf node of tree structure
 */
export class NoChildren extends Node {
    children() {
        return new Array();
    }
}
function structOffsetKey(astStruct, field) {
    return astStruct + '::' + field;
}
export class StructOffset extends NoChildren {
    constructor(codeOrigin, astStruc, fiel) {
        super(codeOrigin);
        this.astStruct = astStruc;
        this.field = fiel;
        this.isAddress = false;
        this.isLabel = false;
        this.isImmediate = true;
        this.isRegister = false;
    }
    static forField(codeOrigin, astStruct, field) {
        let key = structOffsetKey(astStruct, field);
        if (StructOffset.mapping.get(key) == null) {
            StructOffset.mapping.set(key, new StructOffset(codeOrigin, astStruct, field));
        }
        return StructOffset.mapping.get(key);
    }
    static resetMappings() {
        StructOffset.mapping = new Map();
    }
    dump() {
        return this.astStruct + '::' + this.field;
    }
}
StructOffset.mapping = new Map();
export class Sizeof extends NoChildren {
    constructor(codeOrigin, astStruc) {
        super(codeOrigin);
        this.astStruct = astStruc;
    }
    static forName(codeOrigin, astStruct) {
        if (Sizeof.mapping.get(astStruct) == null) {
            Sizeof.mapping.set(astStruct, new Sizeof(codeOrigin, astStruct));
        }
        return Sizeof.mapping.get(astStruct);
    }
    static resetMappings() {
        Sizeof.mapping = new Map();
    }
    dump() {
        return 'sizeof ' + this.astStruct;
    }
}
Sizeof.mapping = new Map();
export class Immediate extends NoChildren {
    constructor(codeOrigin, valu) {
        super(codeOrigin);
        this.values = valu;
        this.isAddress = false;
        this.isLabel = false;
        this.isImmediate = true;
        this.isImmediateOperand = true;
        this.isRegister = false;
    }
    dump() {
        return this.values.toString();
    }
    equals(other) {
        return other.values == this.values;
    }
}
export class AddImmediates extends Node {
    constructor(codeOrigin, lef, righ) {
        super(codeOrigin);
        this.left = lef;
        this.right = righ;
        this.isAddress = false;
        this.isLabel = false;
        this.isImmediate = true;
        this.isImmediateOperand = true;
        this.isRegister = false;
    }
    children() {
        return [this.left, this.right];
    }
    dump() {
        return '(' + this.left.dump() + ' + ' + this.right.dump() + ')';
    }
    value() {
        return this.left.value() + this.right.value();
    }
}
export class SubImmediates extends Node {
    constructor(codeOrigin, lef, righ) {
        super(codeOrigin);
        this.left = lef;
        this.right = righ;
        this.isAddress = false;
        this.isLabel = false;
        this.isImmediate = true;
        this.isImmediateOperand = true;
        this.isRegister = false;
    }
    children() {
        return [this.left, this.right];
    }
    dump() {
        return '(' + this.left.dump() + ' - ' + this.right.dump() + ')';
    }
    value() {
        return this.left.value() + '-' + this.right.value();
    }
}
export class MulImmediates extends Node {
    constructor(codeOrigin, lef, righ) {
        super(codeOrigin);
        this.left = lef;
        this.right = righ;
        this.isAddress = false;
        this.isLabel = false;
        this.isImmediate = true;
        this.isImmediateOperand = false;
        this.isRegister = false;
    }
    children() {
        return [this.left, this.right];
    }
    dump() {
        return '(' + this.left.dump() + ' * ' + this.right.dump() + ')';
    }
}
export class NegImmediate extends Node {
    constructor(codeOrigin, chil) {
        super(codeOrigin);
        this.child = chil;
        this.isAddress = false;
        this.isLabel = false;
        this.isImmediate = true;
        this.isImmediateOperand = false;
        this.isRegister = false;
    }
    children() {
        return [this.child];
    }
    dump() {
        return '(-' + this.child.dump() + ')';
    }
}
export class OrImmediates extends Node {
    constructor(codeOrigin, lef, righ) {
        super(codeOrigin);
        this.left = lef;
        this.right = righ;
        this.isAddress = false;
        this.isLabel = false;
        this.isImmediate = true;
        this.isImmediateOperand = false;
        this.isRegister = false;
    }
    children() {
        return [this.left, this.right];
    }
    dump() {
        return '(' + this.left.dump() + ' | ' + this.right.dump() + ')';
    }
}
export class AndImmediates extends Node {
    constructor(codeOrigin, lef, righ) {
        super(codeOrigin);
        this.left = lef;
        this.right = righ;
        this.isAddress = false;
        this.isLabel = false;
        this.isImmediate = true;
        this.isImmediateOperand = false;
        this.isRegister = false;
    }
    children() {
        return [this.left, this.right];
    }
    dump() {
        return '(' + this.left.dump() + ' & ' + this.right.dump() + ')';
    }
}
export class XorImmediates extends Node {
    constructor(codeOrigin, lef, righ) {
        super(codeOrigin);
        this.left = lef;
        this.right = righ;
        this.isAddress = false;
        this.isLabel = false;
        this.isImmediate = true;
        this.isImmediateOperand = false;
        this.isRegister = false;
    }
    children() {
        return [this.left, this.right];
    }
    dump() {
        return '(' + this.left.dump() + ' ^ ' + this.right.dump() + ')';
    }
}
export class BitnotImmediate extends Node {
    constructor(codeOrigin, chil) {
        super(codeOrigin);
        this.child = chil;
        this.isAddress = false;
        this.isLabel = false;
        this.isImmediate = true;
        this.isImmediateOperand = false;
        this.isRegister = false;
    }
    children() {
        return [this.child];
    }
    dump() {
        return '(~' + this.child.dump() + ')';
    }
}
export class StringLiteral extends NoChildren {
    constructor(codeOrigin, valu) {
        super(codeOrigin);
        this.values = valu.slice(1, -1);
        if (!valu.startsWith('"') || !valu.endsWith('"')) {
            console.log('Bad string literal ' +
                this.values +
                ' at ' +
                this.codeOriginString());
        }
        this.isAddress = false;
        this.isLabel = false;
        this.isImmediate = false;
        this.isImmediateOperand = false;
        this.isRegister = false;
    }
    dump() {
        return '"' + this.values + '"';
    }
    equals(other) {
        return other.values == this.values;
    }
}
export class RegisterID extends NoChildren {
    constructor(codeOrigin, nam) {
        super(codeOrigin);
        this.name = nam;
        this.isAddress = false;
        this.isLabel = false;
        this.isImmediate = false;
        this.isRegister = true;
    }
    static forName(codeOrigin, name) {
        if (!RegisterID.mapping.get(name)) {
            RegisterID.mapping.set(name, new RegisterID(codeOrigin, name));
        }
        return RegisterID.mapping.get(name);
    }
    dump() {
        return this.name;
    }
}
RegisterID.mapping = new Map();
export class FPRegisterID extends NoChildren {
    constructor(codeOrigin, nam) {
        super(codeOrigin);
        this.name = nam;
        this.isAddress = false;
        this.isLabel = false;
        this.isImmediate = false;
        this.isImmediateOperand = false;
        this.isRegister = true;
    }
    static forName(codeOrigin, name) {
        if (FPRegisterID.mapping.get(name) == null) {
            FPRegisterID.mapping.set(name, new FPRegisterID(codeOrigin, name));
        }
        return FPRegisterID.mapping.get(name);
    }
    dump() {
        return this.name;
    }
}
FPRegisterID.mapping = new Map();
export class SpecialRegister extends NoChildren {
    constructor(nam) {
        super(null);
        this.name = nam;
        this.isAddress = false;
        this.isLabel = false;
        this.isImmediate = false;
        this.isImmediateOperand = false;
        this.isRegister = true;
    }
}
export class Variable extends NoChildren {
    constructor(codeOrigin, nam) {
        super(codeOrigin);
        this.name = nam;
    }
    static forName(codeOrigin, name) {
        if (Variable.mapping.get(name) == undefined) {
            Variable.mapping.set(name, new Variable(codeOrigin, name));
        }
        return Variable.mapping.get(name);
    }
    dump() {
        return this.name;
    }
    inspect() {
        return '<variable ' + this.name + ' at ' + this.codeOriginString();
    }
}
Variable.mapping = new Map();
export class Address extends Node {
    constructor(codeOrigin, bas, offse) {
        super(codeOrigin);
        this.base = bas;
        this.offset = offse;
        if (!(this.base instanceof Variable) && !this.base.isRegister) {
            console.log('Bad base for address ' +
                this.base.codeOriginString() +
                ' at ' +
                codeOrigin.toString());
        }
        if (!(this.offset instanceof Variable) && !this.offset.isImmediate) {
            console.log('Bad offset for address ' +
                this.offset.codeOriginString() +
                ' at ' +
                codeOrigin.toString());
        }
        this.isAddress = true;
        this.isLabel = false;
        this.isImmediate = false;
        this.isImmediateOperand = true;
        this.isRegister = false;
    }
    children() {
        return [this.base, this.offset];
    }
    dump() {
        return this.offset.dump() + '[' + this.base.dump() + ']';
    }
}
export class BaseIndex extends Node {
    constructor(codeOrigin, bas, inde, scal, offse) {
        super(codeOrigin);
        this.base = bas;
        this.index = inde;
        this.scale = scal;
        this.offset = offse;
        if (![1, TWO, FOUR, EIGHT].includes(this.scale)) {
            console.log('Bad scale ' + this.scale + ' at ' + this.codeOriginString());
        }
        this.isAddress = true;
        this.isLabel = false;
        this.isImmediate = false;
        this.isImmediateOperand = false;
        this.isRegister = false;
    }
    scaleShift() {
        switch (this.scale) {
            case 1:
                return 0;
            case TWO:
                return 1;
            case FOUR:
                return TWO;
            case EIGHT:
                return THREE;
            default:
                console.log('Bad scale ' + this.scale + ' at ' + this.codeOriginString());
                return null;
        }
    }
    children() {
        return [this.base, this.index, this.offset];
    }
    dump() {
        return (this.offset.dump() +
            '[' +
            this.base.dump() +
            ', ' +
            this.index.dump() +
            ', ' +
            this.scale +
            ']');
    }
}
export class AbsoluteAddress extends NoChildren {
    constructor(codeOrigin, addres) {
        super(codeOrigin);
        this.address = addres;
        this.isAddress = true;
        this.isLabel = false;
        this.isImmediate = false;
        this.isImmediateOperand = true;
        this.isRegister = false;
    }
    dump() {
        return this.address.dump() + '[]';
    }
}
export class Instruction extends Node {
    constructor(codeOrigin, opcod, operand, annotatio) {
        super(codeOrigin);
        this.opcode = opcod;
        this.operands = operand;
        this.annotation = annotatio;
    }
    children() {
        return Array();
    }
    dump() {
        return ('    ' +
            this.opcode +
            ' ' +
            this.operands
                .map((v) => {
                return v.dump();
            })
                .join(', '));
    }
}
export class Error extends NoChildren {
    dump() {
        return '    error';
    }
}
export class ConstExpr extends NoChildren {
    constructor(codeOrigin, valu) {
        super(codeOrigin);
        this.values = valu;
    }
    static forName(codeOrigin, text) {
        if (ConstExpr.mapping.get(text) == null) {
            ConstExpr.mapping.set(text, new ConstExpr(codeOrigin, text));
        }
        return ConstExpr.mapping.get(text);
    }
    static resetMappings() {
        ConstExpr.mapping = new Map();
    }
    dump() {
        return 'constexpr (' + this.values + ')';
    }
    compare(other) {
        return this.values == other.values;
    }
    isImmediates() {
        return true;
    }
}
ConstExpr.mapping = new Map();
export class ConstDecl extends Node {
    constructor(codeOrigin, variabl, valueDec) {
        super(codeOrigin);
        this.variable = variabl;
        this.valueDecl = valueDec;
    }
    children() {
        return [this.variable, this.valueDecl];
    }
    dump() {
        return ('const ' + this.variable.dump() + ' = ' + this.valueDecl.dump());
    }
}
/*
 * The global variable
 */
let labelMapping = new Map();
let referencedExternLabels = Array();
export class Label extends NoChildren {
    constructor(codeOrigin, nam) {
        super(codeOrigin);
        this.extern = true;
        this.global = false;
        this.name = nam;
    }
    static forName(codeOrigin, name, definedInFile = false) {
        if (labelMapping[name] == null) {
            labelMapping[name] = new Label(codeOrigin, name);
        }
        if (definedInFile) {
            labelMapping[name].clearExtern();
        }
        return labelMapping[name];
    }
    static setAsGlobal(codeOrigin, name) {
        if (labelMapping[name]) {
            let label = labelMapping[name];
            if (label.isGlobal()) {
                console.log('Label: ' + name + ' declared global multiple times');
            }
            label.setGlobal();
        }
        else {
            let newLabel = new Label(codeOrigin, name);
            newLabel.setGlobal();
            labelMapping[name] = newLabel;
        }
    }
    static resetMappings() {
        labelMapping = new Map();
        referencedExternLabels = Array();
    }
    static resetReferenced() {
        referencedExternLabels = Array();
    }
    clearExtern() {
        this.extern = false;
    }
    isExtern() {
        return this.extern;
    }
    setGlobal() {
        this.global = true;
    }
    isGlobal() {
        return this.global;
    }
    dump() {
        return this.name + ':';
    }
}
export class LocalLabel extends NoChildren {
    constructor(codeOrigin, nam) {
        super(codeOrigin);
        this.extern = true;
        this.name = nam;
    }
    static forName(codeOrigin, name) {
        if (!labelMapping[name]) {
            labelMapping[name] = new LocalLabel(codeOrigin, name);
        }
        return labelMapping[name];
    }
    unique(comment) {
        let newName = '_' + comment;
        if (labelMapping[newName]) {
            newName = '_#' + LocalLabel.uniqueNameCounter + '_' + comment;
            while (labelMapping[newName] != null) {
                LocalLabel.uniqueNameCounter++;
            }
        }
        return LocalLabel.forName(null, newName);
    }
    static resetMappings() {
        LocalLabel.uniqueNameCounter = 0;
    }
    cleanName() {
        if (this.name.startsWith('.')) {
            return '_' + this.name.slice(1);
        }
        return this.name;
    }
    isGlobal() {
        return false;
    }
    dump() {
        return this.name + ':';
    }
}
LocalLabel.uniqueNameCounter = 0;
export class LabelReference extends Node {
    constructor(codeOrigin, labe) {
        super(codeOrigin);
        this.label = labe;
        this.isAddress = false;
        this.isLabel = true;
        this.isImmediate = false;
        this.isImmediateOperand = true;
    }
    children() {
        return [this.label];
    }
    name() {
        return this.label.name;
    }
    isExtern() {
        return labelMapping[this.name()].isExtern();
    }
    used() {
        if (!referencedExternLabels.includes(this.label) && this.isExtern()) {
            referencedExternLabels.push(this.label);
        }
    }
    dump() {
        return this.label.name;
    }
}
export class LocalLabelReference extends NoChildren {
    constructor(codeOrigin, labe) {
        super(codeOrigin);
        this.label = labe;
        this.isAddress = false;
        this.isLabel = true;
        this.isImmediate = false;
        this.isImmediateOperand = true;
    }
    children() {
        return [this.label];
    }
    name() {
        return this.label.name;
    }
    dump() {
        return this.label.name;
    }
}
export class Sequence extends Node {
    constructor(codeOrigin, lis) {
        super(codeOrigin);
        this.list = lis;
    }
    children() {
        return this.list;
    }
    dump() {
        return ('' +
            this.list
                .map((v) => {
                return v.dump();
            })
                .join('\n'));
    }
}
export class True extends NoChildren {
    static instance() {
        return new True(null);
    }
    values() {
        return true;
    }
    dump() {
        return 'true';
    }
}
export class False extends NoChildren {
    static instance() {
        return new False(null);
    }
    values() {
        return false;
    }
    dump() {
        return 'false';
    }
}
export class Setting extends NoChildren {
    constructor(codeOrigin, nam) {
        super(codeOrigin);
        this.name = nam;
    }
    static forName(codeOrigin, name) {
        if (Setting.mapping.get(name) == null) {
            Setting.mapping.set(name, new Setting(codeOrigin, name));
        }
        return Setting.mapping.get(name);
    }
    static resetMappings() {
        Setting.mapping = new Map();
    }
    dump() {
        return this.name;
    }
}
Setting.mapping = new Map();
export class And extends Node {
    constructor(codeOrigin, lef, righ) {
        super(codeOrigin);
        this.left = lef;
        this.right = righ;
    }
    children() {
        return [this.left, this.right];
    }
    dump() {
        return '(' + this.left.dump() + ' and ' + this.right.dump() + ')';
    }
}
export class Or extends Node {
    constructor(codeOrigin, lef, righ) {
        super(codeOrigin);
        this.left = lef;
        this.right = righ;
    }
    children() {
        return [this.left, this.right];
    }
    dump() {
        return '(' + this.left.dump() + ' or ' + this.right.dump() + ')';
    }
}
export class Not extends Node {
    constructor(codeOrigin, chil) {
        super(codeOrigin);
        this.child = chil;
    }
    children() {
        return [this.child];
    }
    dump() {
        return '(not' + this.child.dump() + ')';
    }
}
export class Skip extends NoChildren {
    dump() {
        return '    skip';
    }
}
export class IfThenElse extends Node {
    constructor(codeOrigin, predicat, thenCas) {
        super(codeOrigin);
        this.predicate = predicat;
        this.thenCase = thenCas;
        this.elseCase = new Skip(codeOrigin);
    }
    dump() {
        return ('if ' +
            this.predicate.dump() +
            '\n' +
            this.thenCase.dump() +
            '\nelse\n' +
            this.elseCase.dump() +
            '\nend');
    }
}
export class Macro extends Node {
    constructor(codeOrigin, nam, variable, bod) {
        super(codeOrigin);
        this.name = nam;
        this.variables = variable;
        this.body = bod;
    }
    dump() {
        return ('macro ' +
            this.name +
            '(' +
            this.variables
                .map((v) => {
                return v.dump();
            })
                .join(', ') +
            ')\n' +
            this.body.dump() +
            '\nend');
    }
}
export class MacroCall extends Node {
    constructor(codeOrigin, nam, operand, annotatio) {
        super(codeOrigin);
        this.name = nam;
        this.operands = operand;
        this.annotation = annotatio;
    }
    children() {
        return Array();
    }
    dump() {
        return ('    ' +
            this.name +
            '(' +
            this.operands
                .map((v) => {
                return v.dump();
            })
                .join(', ') +
            ')');
    }
}
/*
 * Reset the static mapping property of the following node classes
 */
export function resetAST() {
    StructOffset.resetMappings();
    Sizeof.resetMappings();
    ConstExpr.resetMappings();
    Label.resetMappings();
    LocalLabel.resetMappings();
    Setting.resetMappings();
}
let debug = false;
function DeBugLog(msg) {
    if (debug) {
        console.log(msg);
    }
}
