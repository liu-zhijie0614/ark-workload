"use strict";
/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
const NUM_1000 = 1000;
const NUM_2 = 2;
const NUM_3 = 3;
const NUM_4 = 4;
const NUM_121 = 121;
const NUM_501 = 501;
const NUM_120 = 120;
const NUM_500 = 500;
class Test {
    constructor(num1, num2) {
        this.arr = new Int32Array(NUM_1000);
        for (let i = 0; i < NUM_1000; i++) {
            switch (i % NUM_4) {
                case 0:
                    this.arr[i] = this.add(num1, num2);
                    break;
                case 1:
                    this.arr[i] = this.subtract(num1, num2);
                    break;
                case NUM_2:
                    this.arr[i] = this.multiply(num1, num2);
                    break;
                case NUM_3:
                    this.arr[i] = this.divide(num1, num2);
                    break;
                default:
                    break;
            }
        }
    }
    add(num1, num2) {
        return num1 + num2;
    }
    subtract(num1, num2) {
        return num1 - num2;
    }
    multiply(num1, num2) {
        return num1 * num2;
    }
    divide(num1, num2) {
        return num1 / num2;
    }
}
class Benchmark {
    /*
     * @Benchmark
     */
    runIteration() {
        let startTime = Date.now();
        for (let i = 1; i < NUM_121; i++) {
            for (let j = 1; j < NUM_501; j++) {
                let test = new Test(i, j);
                if (i === NUM_120 && j === NUM_500) {
                    this.test = test;
                }
            }
        }
        let endTime = Date.now();
        console.log(`code-first-load: ms = ${endTime - startTime}`);
        //deBugLog("对象创建完成");
        this.test.arr[0] = 0;
    }
}
let isDebug = false;
function deBugLog(msg) {
    if (isDebug) {
        console.log(msg);
    }
}
new Benchmark().runIteration();
