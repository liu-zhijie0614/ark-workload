"use strict";
const input = 250000;
class AminoAcid {
    constructor(prob, sym) {
        this.prob = 0.0;
        this.sym = 0;
        this.prob = prob;
        this.sym = sym;
    }
}
const IM = 139968;
const IA = 3877;
const IC = 29573;
const SEED = 42;
let n = input;
const width = 60;
const iub = [
    new AminoAcid(0.27, 97),
    new AminoAcid(0.12, 99),
    new AminoAcid(0.12, 103),
    new AminoAcid(0.27, 116),
    new AminoAcid(0.02, 66),
    new AminoAcid(0.02, 68),
    new AminoAcid(0.02, 72),
    new AminoAcid(0.02, 75),
    new AminoAcid(0.02, 77),
    new AminoAcid(0.02, 78),
    new AminoAcid(0.02, 82),
    new AminoAcid(0.02, 83),
    new AminoAcid(0.02, 86),
    new AminoAcid(0.02, 87),
    new AminoAcid(0.02, 89), // "Y"),
];
function binarySearch(rnd, arr) {
    let low = 0;
    let high = arr.length - 1;
    for (; low <= high;) {
        let middle = (low + high) >>> 1;
        if (arr[middle].prob >= rnd) {
            high = middle - 1;
        }
        else {
            low = middle + 1;
        }
    }
    return arr[high + 1].sym;
}
function accumulateProbabilities(acid) {
    for (let i = 1; i < acid.length; i++) {
        acid[i].prob += acid[i - 1].prob;
    }
}
function randomFasta(buffer, acid, n) {
    let cnt = n;
    accumulateProbabilities(acid);
    let pos = 0;
    let seed = SEED;
    while (cnt > 0) {
        let m = cnt > width ? width : cnt;
        let f = 1.0 / IM;
        let myrand = seed;
        for (let i = 0; i < m; i++) {
            myrand = (myrand * IA + IC) % IM;
            let r = myrand * f;
            buffer[pos] = binarySearch(r, acid);
            pos++;
            if (pos === buffer.length) {
                pos = 0;
            }
        }
        seed = myrand;
        buffer[pos] = 10;
        pos++;
        if (pos === buffer.length) {
            pos = 0;
        }
        cnt -= m;
    }
}
function RunFastaRandom1() {
    const bufferSize = 256 * 1024;
    let buffer = new Array();
    for (let i = 0; i < bufferSize; i++) {
        buffer.push(10);
    }
    let start = Date.now();
    randomFasta(buffer, iub, 3 * n);
    let end = Date.now();
    let time = (end - start) ;
    console.log("Array Access - RunFastaRandom1:\t" + String(time) + "\tms");
    return time;
}
RunFastaRandom1();
