"use strict";
/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/////. Start CORDIC
const FI_CONST = 65536.0;
const DEG_CONST = 0.017453;
const AG_CONST = 0.607252935;
const ANGLE_1_CONST = 45.0;
const ANGLE_2_CONST = 26.565;
const ANGLE_3_CONST = 14.0362;
const ANGLE_4_CONST = 7.12502;
const ANGLE_5_CONST = 3.57633;
const ANGLE_6_CONST = 1.78991;
const ANGLE_7_CONST = 0.895174;
const ANGLE_8_CONST = 0.447614;
const ANGLE_9_CONST = 0.223811;
const ANGLE_10_CONST = 0.111906;
const ANGLE_11_CONST = 0.055953;
const ANGLE_12_CONST = 0.027977;
const STEP_CONST = 12;
const CORDIC_CONST = 25000;
const NUM_1000_CONST = 1000;
const NUM_TIME_LOOP1_CONST = 80;
function FIXED(X) {
    return X * FI_CONST;
}
function FLOAT(X) {
    return X / FI_CONST;
}
function DEG2RAD(X) {
    return DEG_CONST * X;
}
let Angles = [
    FIXED(ANGLE_1_CONST),
    FIXED(ANGLE_2_CONST),
    FIXED(ANGLE_3_CONST),
    FIXED(ANGLE_4_CONST),
    FIXED(ANGLE_5_CONST),
    FIXED(ANGLE_6_CONST),
    FIXED(ANGLE_7_CONST),
    FIXED(ANGLE_8_CONST),
    FIXED(ANGLE_9_CONST),
    FIXED(ANGLE_10_CONST),
    FIXED(ANGLE_11_CONST),
    FIXED(ANGLE_12_CONST)
];
let Target = 28.027;
function cordicsincos(Target) {
    let x;
    let y;
    let TargetAngle;
    let CurrAngle;
    x = FIXED(AG_CONST); /* AG_CONST * cos(0) */
    y = 0; /* AG_CONST * sin(0) */
    TargetAngle = FIXED(Target);
    CurrAngle = 0;
    for (let Step = 0; Step < STEP_CONST; Step++) {
        let NewX;
        if (TargetAngle > CurrAngle) {
            NewX = x - (y >> Step);
            y = (x >> Step) + y;
            x = NewX;
            CurrAngle += Angles[Step];
        }
        else {
            NewX = x + (y >> Step);
            y = -(x >> Step) + y;
            x = NewX;
            CurrAngle -= Angles[Step];
        }
    }
    return FLOAT(x) * FLOAT(y);
}
///// End CORDIC
let total = 0.0;
function cordic(runs) {
    let start = new Date().getTime();
    for (let i = 0; i < runs; i++) {
        total += cordicsincos(Target);
    }
    let end = new Date().getTime();
    return end - start;
}
/**
 * @State
 */
class Benchmark {
    run() {
        cordic(CORDIC_CONST);
        let expected = 10362.570468755888;
        if (total != expected) {
            console.log('ERROR: bad result: expected  (expected)   but got (total)');
        }
    }
    /**
     * @Benchmark
     */
    runIterationTime() {
        let start = Date.now();
        for (let i = 0; i < NUM_TIME_LOOP1_CONST; i++) {
            total = 0.0;
            this.run();
        }
        let end = Date.now();
        console.log('math-cordic: ms = ' + (end - start));
    }
}
new Benchmark().runIterationTime();
