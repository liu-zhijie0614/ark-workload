"use strict";
function picColorFromBuffer(e) {
    let data = e.data;
    if (!data || !data.data) {
        console.log('dingwen input error');
        return;
    }
    let msgType = data.type;
    let readBuffer = data.data;
    let len = data.len / 4;
    switch (msgType) {
        case 'pickColor':
            let intBuffer = new Uint8Array(readBuffer);
            let r = 0;
            let g = 0;
            let b = 0;
            let a = 0;
            for (var i = 0; i < len; i++) {
                r += intBuffer[i * 4];
                g += intBuffer[i * 4 + 1];
                b += intBuffer[i * 4 + 2];
                a += intBuffer[i * 4 + 3];
            }
            r = Math.round(r / len);
            g = Math.round(g / len);
            b = Math.round(b / len);
            a = Math.round(a / len);
            //    console.log("Takecolor pinColorFormBuffer color[ARGB] :\t" + a + "\t" + r + "\t" + g + "\t" + b);
            break;
        default:
            break;
    }
}
class Data {
    constructor(mType, mData, mLen) {
        this.type = mType;
        this.data = mData;
        this.len = mLen;
    }
}
class EData {
    constructor(mData) {
        this.data = mData;
    }
}
let readBuffer = new ArrayBuffer(500 * 500);
let uint8Array = new Uint8Array(readBuffer);
for (let i = 0; i < uint8Array.length; i++) {
    uint8Array[i] = i % 256;
}
let data = new Data('pickColor', readBuffer, 500 * 500);
let eData = new EData(data);
const start = Date.now();
for (let i = 0; i < 10; i++) {
    picColorFromBuffer(eData);
}
const end = Date.now();
console.log("Takecolor_Obj: " + (end - start) + "\tms");
