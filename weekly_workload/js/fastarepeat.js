"use strict";
const input = 250000;
let n = input;
const width = 60;
const aluString = "GGCCGGGCGCGGTGGCTCACGCCTGTAATCCCAGCACTTTGG" +
    "GAGGCCGAGGCGGGCGGATCACCTGAGGTCAGGAGTTCGAGA" +
    "CCAGCCTGGCCAACATGGTGAAACCCCGTCTCTACTAAAAAT" +
    "ACAAAAATTAGCCGGGCGTGGTGGCGCGCGCCTGTAATCCCA" +
    "GCTACTCGGGAGGCTGAGGCAGGAGAATCGCTTGAACCCGGG" +
    "AGGCGGAGGTTGCAGTGAGCCGAGATCGCGCCACTGCACTCC" +
    "AGCCTGGGCGACAGAGCGAGACTCCGTCTCAAAAA";
let aluLen = aluString.length - 1;
let alu = new Int32Array(aluLen);
for (let i = 0; i < aluLen; i++) {
    alu[i] = aluString[i].charCodeAt(0);
}
function repeatFasta(geneLength, buffer, gene2) {
    let pos = 0;
    let rpos = 0;
    let cnt = 500000;
    let lwidth = width;
    while (cnt > 0) {
        if (pos + lwidth > buffer.length) {
            pos = 0;
        }
        if (rpos + lwidth > geneLength) {
            rpos = rpos % geneLength;
        }
        if (cnt < lwidth) {
            lwidth = cnt;
        }
        for (let i = 0; i < lwidth; i++) {
            buffer[pos + i] = gene2[rpos + i];
        }
        buffer[pos + lwidth] = 10;
        pos += lwidth + 1;
        rpos += lwidth;
        cnt -= lwidth;
    }
    if (pos > 0 && pos < buffer.length) {
        buffer[pos] = 10;
    }
    else if (pos === buffer.length) {
        buffer[0] = 10;
    }
    let result = 0;
    for (let i = 0; i < buffer.length; i++) {
        result += buffer[i];
    }
    return result;
}
function RunFastaRepeat() {
    const bufferSize = 256 * 1024;
    let buffer = new Array();
    for (let i = 0; i < bufferSize; i++) {
        buffer.push(10);
    }
    let gene2 = new Int32Array(2 * aluLen);
    for (let i = 0; i < gene2.length; i++) {
        gene2[i] = alu[i % aluLen];
    }
    let res = 0;
    let start = Date.now();
    for (let i = 0; i < 500; i++) {
        res += repeatFasta(aluLen, buffer, gene2);
    }
    let end = Date.now();
    let time = (end - start) ;
    console.log(res);
    console.log("Array Access - RunFastaRepeat:\t" + String(time) + "\tms");
    return time;
}
RunFastaRepeat();
