"use strict";
const input = 8;
function fannkuch(n) {
    let perm = new Int32Array(n);
    let count = new Int32Array(n);
    let perm1 = new Int32Array(n);
    for (let i = 0; i < n; i++) {
        perm[i] = 0;
        count[i] = 0;
        perm1[i] = 0;
    }
    for (let j = 0; j < (n - 1); j++) {
        perm1[j] = j;
    }
    let f = 0;
    let i = 0;
    let k = 0;
    let r = n;
    let flips = 0;
    let nperm = 0;
    let checksum = 0;
    while (r > 0) {
        i = 0;
        while (r != 1) {
            count[r - 1] = r;
            r -= 1;
        }
        while (i < n) {
            perm[i] = perm1[i];
            i += 1;
        }
        f = 0;
        k = perm[0];
        while (k != 0) {
            i = 0;
            while (2 * i < k) {
                const t = perm[i];
                perm[i] = perm[k - i];
                perm[k - i] = t;
                i += 1;
            }
            k = perm[0];
            f += 1;
        }
        if (f > flips) {
            flips = f;
        }
        if ((nperm & 0x1) == 0) {
            checksum += f;
        }
        else {
            checksum -= f;
        }
        let go = true;
        while (go) {
            if (r == n) {
                return flips;
            }
            const p0 = perm1[0];
            i = 0;
            while (i < r) {
                const j = i + 1;
                perm1[i] = perm1[j];
                i = j;
            }
            perm1[r] = p0;
            count[r] -= 1;
            if (count[r] > 0) {
                go = false;
            }
            else {
                r += 1;
            }
        }
        nperm += 1;
    }
    return flips;
}
function RunFannkucRedux() {
    let n = input;
    let res = 0;
    let start = Date.now();
    res = fannkuch(n);
    let end = Date.now();
    let time = (end - start) ;
    console.log("Array Access - RunFannkucRedux:\t" + String(time) + "\tms");
    return time;
}
RunFannkucRedux();
