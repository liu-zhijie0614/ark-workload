"use strict";
class Obj {
    constructor(value) {
        this.value = 0;
        this.value = value;
    }
}
;
function GenerateFakeRandomObject() {
    let resource = new Array(15).fill(new Obj(0));
    for (let i = 0; i < 15; i++) {
        let random = Math.random() * (10) + 1;
        resource[i] = new Obj(random);
    }
    return resource;
}
let global_value = 0;
function GenerateFakeRandomInteger() {
    let resource = new Int32Array([12, 43, 56, 76, 89, 54, 45, 32, 35, 47, 46, 44, 21, 37, 84]);
    return resource;
}
let arr = GenerateFakeRandomInteger();
function Foo(a, b, c) {
    arr[global_value] += 1;
}
function CallFoo(f, a, b, c) {
    f(a, b, c);
}
function RunFunctionPtr() {
    let count = 10000000;
    global_value = 0;
    let startTime = Date.now();
    for (let i = 0; i < count; i++) {
        CallFoo(Foo, 1, 2, i);
    }
    let midTime = Date.now();
    for (let i = 0; i < count; i++) {
    }
    let endTime = Date.now();
    let time = ((midTime - startTime) - (endTime - midTime)) ;
    console.log("Function Call - RunFunctionPtr:\t" + String(time) + "\tms");
    return time;
}
RunFunctionPtr();
function DefaultFoo(resources = arr, i = 1, i3 = 1, resourcesLength = 1) {
    if ((resources[i % i3 & (resourcesLength - 1)] & 1) == 0) {
        i3 += 1;
    }
    else {
        i3 += 2;
    }
    return i3;
}
function CallDefaultFoo(f, resources = arr, i = 1, i3 = 1, resourcesLength = 1) {
    return f(resources, i, i3, resourcesLength);
}
function RunDefaultfunctionPtr() {
    let count = 10000000;
    global_value = 0;
    let i3 = 1;
    let resources = GenerateFakeRandomInteger();
    let resourcesLength = resources.length;
    let startTime = Date.now();
    for (let i = 0; i < count; i++) {
        i3 = CallDefaultFoo(DefaultFoo, resources, i, i3, resourcesLength);
    }
    let midTime = Date.now();
    for (let i = 0; i < count; i++) {
    }
    let endTime = Date.now();
    let time = ((midTime - startTime) - (endTime - midTime)) ;
    console.log("Function Call - RunDefaultfunctionPtr:\t" + String(time) + "\tms");
    return time;
}
RunDefaultfunctionPtr();
function DifferentFoo(a, b, c) {
    arr[global_value] += 1;
}
function CallDifferentFoo(f, a, b, c) {
    f(a, b, c);
}
function RunDifferentFunctionPtr() {
    let count = 10000000;
    global_value = 0;
    let startTime = Date.now();
    for (let i = 0; i < count; i++) {
        CallDifferentFoo(DifferentFoo, 1, "1", true);
    }
    let midTime = Date.now();
    for (let i = 0; i < count; i++) {
    }
    let endTime = Date.now();
    let time = ((midTime - startTime) - (endTime - midTime)) ;
    console.log("Function Call - RunDifferentFunctionPtr:\t" + String(time) + "\tms");
    return time;
}
RunDifferentFunctionPtr();
function VariableFoo(a, b, c) {
    arr[global_value] += 1;
}
function CallVariableFoo(f, a, b, c) {
    f(a, b, c);
}
function RunVariableFunctionPtr() {
    let count = 10000000;
    global_value = 0;
    let startTime = Date.now();
    for (let i = 0; i < count; i++) {
        CallVariableFoo(VariableFoo, 1, "1", true);
    }
    let midTime = Date.now();
    for (let i = 0; i < count; i++) {
    }
    let endTime = Date.now();
    let time = ((midTime - startTime) - (endTime - midTime)) ;
    console.log("Function Call - RunVariableFunctionPtr:\t" + String(time) + "\tms");
    return time;
}
RunVariableFunctionPtr();
console.log("Ts Method Call Is End, global_value value: \t" + String(global_value));
