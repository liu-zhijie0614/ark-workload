"use strict";
function FIXED(X) {
    return X * 65536.0;
}
function FLOAT(X) {
    return X / 65536.0;
}
let AG_CONST = 0.6072529350;
function cordicsincos(Target, Angles) {
    let X = 0.0;
    let Y = 0.0;
    let TargetAngle = 0.0;
    let CurrAngle = 0.0;
    let Step = 0;
    X = FIXED(AG_CONST);
    TargetAngle = FIXED(Target);
    CurrAngle = 0.0;
    for (Step = 0; Step < 12; Step++) {
        let NewX = 0.0;
        if (TargetAngle > CurrAngle) {
            NewX = X - (Y >> Step);
            Y = (X >> Step) + Y;
            X = NewX;
            CurrAngle += Angles[Step];
        }
        else {
            NewX = X + (Y >> Step);
            Y = -(X >> Step) + Y;
            X = NewX;
            CurrAngle -= Angles[Step];
        }
    }
    return FLOAT(X) * FLOAT(Y);
}
function RunCordic() {
    let res = 0.0;
    let Target = 28.027;
    let input = 150000;
    let Angles = new Float64Array([
    FIXED(45.0), FIXED(26.565), FIXED(14.0362), FIXED(7.12502),
    FIXED(3.57633), FIXED(1.78991), FIXED(0.895174), FIXED(0.447614),
    FIXED(0.223811), FIXED(0.111906), FIXED(0.055953),
    FIXED(0.027977)
    ]);
    let start = Date.now();
    for (let i = 0; i < input; i++) {
        res += cordicsincos(Target + i, Angles);
    }
    let end = Date.now();
    let time = (end - start) ;
    console.log(res);
    console.log("Numerical Calculation - RunCordic:\t" + String(time) + "\tms");
    return time;
}
RunCordic();