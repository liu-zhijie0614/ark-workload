"use strict";
function Mandelbrot() {
    const w = 1000;
    const h = w;
    let bit_num = 0;
    let i = 0;
    let byte_acc = 0;
    const iter = 50;
    const limit = 2.0;
    let Zr = 0;
    let Zi = 0;
    let Tr = 0;
    let Ti = 0;
    let res = 0;
    const limit2 = limit * limit;
    for (let y = 0; y < h; y++) {
        const Ci = 2.0 * y / h - 1.0;
        for (let x = 0; x < w; x++) {
            Zr = 0.0;
            Zi = 0.0;
            Tr = 0.0;
            Ti = 0.0;
            const Cr = 2.0 * x / w - 1.5;
            i = 0;
            while (i < iter && (Tr + Ti <= limit2)) {
                i += 1;
                Zi = 2.0 * Zr * Zi + Ci;
                Zr = Tr - Ti + Cr;
                Tr = Zr * Zr;
                Ti = Zi * Zi;
            }
            byte_acc <<= 1;
            if ((Tr + Ti) <= (limit2)) {
                byte_acc |= 0x01;
            }
            bit_num += 1;
            if (bit_num == 8) {
                res += byte_acc;
                byte_acc = 0;
                bit_num = 0;
            }
            else if (x == (w - 1)) {
                byte_acc <<= (8 - w % 8);
                res += byte_acc;
                byte_acc = 0;
                bit_num = 0;
            }
        }
    }
    return res;
}
function RunMandelbrot() {
    let start = Date.now();
    let res = Mandelbrot();
    let end = Date.now();
    let time = (end - start) ;
    console.log(res);
    console.log("Array Access - RunMandelbrot:\t" + String(time) + "\tms");
    return time;
}
RunMandelbrot();
