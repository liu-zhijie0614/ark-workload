"use strict";
class Obj {
    constructor(value) {
        this.value = 0;
        this.value = value;
    }
}
;
function GenerateFakeRandomObject() {
    let resource = new Array(15).fill(new Obj(0));
    for (let i = 0; i < 15; i++) {
        let random = Math.random() * (10) + 1;
        resource[i] = new Obj(random);
    }
    return resource;
}
let global_value = 0;
function GenerateFakeRandomInteger() {
    let resource = new Int32Array([12, 43, 56, 76, 89, 54, 45, 32, 35, 47, 46, 44, 21, 37, 84]);
    return resource;
}
let arr = GenerateFakeRandomInteger();
function ParameterlessFoo() {
    let res = [new Obj(0), new Obj(0), new Obj(0), new Obj(0), new Obj(0)];
    let resources = GenerateFakeRandomObject();
    for (let i = 0; i < 200; i++) {
        res[i % 5] = resources[i % 15];
    }
    return res[1];
}
function CallParameterlessFoo(f) {
    return f();
}
function RunParameterlessFunctionPtr() {
    let count = 10000;
    global_value = 0;
    let i3 = new Obj(1);
    let startTime = Date.now();
    for (let i = 0; i < count; i++) {
        i3 = CallParameterlessFoo(ParameterlessFoo);
    }
    let midTime = Date.now();
    for (let i = 0; i < count; i++) {
    }
    let endTime = Date.now();
    let time = ((midTime - startTime) - (endTime - midTime)) ;
    console.log("Function Call - RunParameterlessFunctionPtr:\t" + String(time) + "\tms");
    return time;
}
RunParameterlessFunctionPtr();
console.log("Ts Method Call Is End, global_value value: \t" + String(global_value));
