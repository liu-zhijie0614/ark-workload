"use strict";
/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
let last = 42;
let ret = 0;
let A = 3877;
let C = 29573;
let M = 139968;
const TOBASE_NUMBER_60 = 60;
const TOBASE_NUMBER_0_27 = 0.27;
const TOBASE_NUMBER_0_12 = 0.12;
const TOBASE_NUMBER_0_02 = 0.02;
const TOBASE_NUMBER_0_3029549426680 = 0.302954942668;
const TOBASE_NUMBER_0_1979883004921 = 0.1979883004921;
const TOBASE_NUMBER_0_1975473066391 = 0.1975473066391;
const TOBASE_NUMBER_0_3015094502008 = 0.3015094502008;
const TOBASE_NUMBER_100000 = 100000;
const TOBASE_NUMBER_3 = 3;
const TOBASE_NUMBER_2 = 2;
const TOBASE_NUMBER_5 = 5;
const TOBASE_NUMBER_1000 = 1000;
function rand(max) {
    last = (last * A + C) % M;
    return (max * last) / M;
}
function makeCumulative(table) {
    let last = null;
    for (let key of table.keys()) {
        if (last) {
            table.set(key, (table.get(key) || 0) + (table.get(last) || 0));
        }
        last = key;
    }
}
function fastaRepeat(m, seq) {
    let n = m;
    let seqi = 0;
    let lenOut = 60;
    while (n > 0) {
        if (n < lenOut) {
            lenOut = n;
        }
        if (seqi + lenOut < seq.length) {
            ret += lenOut;
            seqi += lenOut;
        }
        else {
            let s = seq.substring(seqi);
            seqi = lenOut - s.length;
            ret += (s + seq.substring(0, seqi)).length;
        }
        n -= lenOut;
    }
}
function fastaRandom(m, table) {
    let n = m;
    let line = new Array(TOBASE_NUMBER_60);
    makeCumulative(table);
    while (n > 0) {
        if (n < line.length) {
            line = new Array(n);
        }
        for (let i = 0; i < line.length; i++) {
            let r = rand(1);
            for (let item of table) {
                if (r < item[1]) {
                    line[i] = item[0];
                    break;
                }
            }
        }
        ret += line.length;
        n -= line.length;
    }
}
class BenchMark {
    /*
     *  @Benchmark
     */
    runIteration() {
        const count = 7;
        let alu = 'GGCCGGGCGCGGTGGCTCACGCCTGTAATCCCAGCACTTTGG' + //string 1
            'GAGGCCGAGGCGGGCGGATCACCTGAGGTCAGGAGTTCGAGA' + //string 2
            'CCAGCCTGGCCAACATGGTGAAACCCCGTCTCTACTAAAAAT' + //string 3
            'ACAAAAATTAGCCGGGCGTGGTGGCGCGCGCCTGTAATCCCA' + //string 4
            'GCTACTCGGGAGGCTGAGGCAGGAGAATCGCTTGAACCCGGG' + //string 5
            'AGGCGGAGGTTGCAGTGAGCCGAGATCGCGCCACTGCACTCC' + //string 6
            'AGCCTGGGCGACAGAGCGAGACTCCGTCTCAAAAA';
        let iub = new Map();
        iub.set('a', TOBASE_NUMBER_0_27);
        iub.set('c', TOBASE_NUMBER_0_12);
        iub.set('g', TOBASE_NUMBER_0_12);
        iub.set('t', TOBASE_NUMBER_0_27);
        iub.set('B', TOBASE_NUMBER_0_02);
        iub.set('D', TOBASE_NUMBER_0_02);
        iub.set('H', TOBASE_NUMBER_0_02);
        iub.set('K', TOBASE_NUMBER_0_02);
        iub.set('M', TOBASE_NUMBER_0_02);
        iub.set('N', TOBASE_NUMBER_0_02);
        iub.set('R', TOBASE_NUMBER_0_02);
        iub.set('S', TOBASE_NUMBER_0_02);
        iub.set('V', TOBASE_NUMBER_0_02);
        iub.set('W', TOBASE_NUMBER_0_02);
        iub.set('Y', TOBASE_NUMBER_0_02);
        let homoSap = new Map();
        homoSap.set('a', TOBASE_NUMBER_0_3029549426680);
        homoSap.set('c', TOBASE_NUMBER_0_1979883004921);
        homoSap.set('g', TOBASE_NUMBER_0_1975473066391);
        homoSap.set('t', TOBASE_NUMBER_0_3015094502008);
        fastaRepeat(TOBASE_NUMBER_2 * count * TOBASE_NUMBER_100000, alu);
        fastaRandom(TOBASE_NUMBER_3 * count * TOBASE_NUMBER_1000, iub);
        fastaRandom(TOBASE_NUMBER_5 * count * TOBASE_NUMBER_1000, homoSap);
        let expected = 1456000;
        if (ret != expected) {
            throw new Error('ERROR: bad result: expected ' + expected + ' but got ' + ret);
        }
    }
}
let start = Date.now();
new BenchMark().runIteration();
let end = Date.now();
console.log(`string-fasta: ms = ${end - start}`);
