"use strict";
function currentTimestamp16() {
    return Date.now();
}
// var start = Date.now()
var start = currentTimestamp16();
function foo() {
    var x = 0;
    for (var i = 0; i < 30000000; i++) {
        x = (i + 0.5) * (i + 0.5);
    }
    return x;
}
for (let i = 0; i < 10; i++) {
    var result = foo();
}
// var end = Date.now()
var end = currentTimestamp16();
console.log("Forlooper_Obj: " + (end - start)  + "\tms");
