"use strict";
let input = 3000000;
function GenerateFloatInput() {
    return new Float64Array([1.0, 2.0]);
}
function GenerateBitOpsInput() {
    return new Int32Array([2, 3]);
}
function GenerateIntegerInput() {
    return new Int32Array([2, 4]);
}
function GenerateFakeRandomInteger() {
    let resource = new Int32Array([12, 43, 56, 76, 89, 54, 45, 32, 35, 47, 46, 44, 21, 37, 84]);
    return resource;
}
function GenerateFakeRandomFloat() {
    let resource = new Float64Array([12.2, 43.5, 56.2, 76.6, 89.7, 54.9, 45.2, 32.5, 35.6, 47.2, 46.6, 44.3, 21.2, 37.6, 84.57]);
    return resource;
}
// 浮点计算
function FloatNumAddition() {
    let count = 3000000;
    let floatInputs = GenerateFloatInput();
    let f1 = 1.0; // floatInputs[0];
    let f2 = 2.0; // floatInputs[1];
    let f3 = 1.0;
    let resources = GenerateFakeRandomInteger();
    let start = Date.now();
    let resourcesLength = resources.length;
    for (let i = 0; i < count; i++) {
        if ((resources[i % f3 & (resourcesLength - 1)] & 1) == 0) { // 2100ms
            f3 += f1;
        }
        else {
            f3 += f2;
        }
    }
    let end = Date.now();
    console.log("FloatNumAddition" + f3);
    let time = (end - start) ;
    console.log("Numerical Calculation - FloatNumAddition:\t" + String(time) + "\tms");
    return time;
}
FloatNumAddition();
function FloatNumSubstraction() {
    let count = 3000000;
    let floatInputs = GenerateFloatInput();
    let f1 = 1.0; // floatInputs[0];
    let f2 = 2.0; // floatInputs[1];
    let f3 = 13000000.0;
    let resources = GenerateFakeRandomInteger();
    let start = Date.now();
    let resourcesLength = resources.length;
    for (let i = 0; i < count; i++) {
        if ((resources[i % f3 & (resourcesLength - 1)] & 1) == 0) {
            f3 -= f1;
        }
        else {
            f3 -= f2;
        }
    }
    let end = Date.now();
    console.log("FloatNumSubstraction" + f3);
    let time = (end - start) ;
    console.log("Numerical Calculation - FloatNumSubstraction:\t" + String(time) + "\tms");
    return time;
}
FloatNumSubstraction();
function FloatNumProduction() {
    let count = 3000000;
    let floatInputs = GenerateFloatInput();
    let f1 = 1.0; // floatInputs[0];
    let f2 = 2.0; // floatInputs[1];
    let f3 = 1.0;
    let resources = GenerateFakeRandomInteger();
    let start = Date.now();
    let resourcesLength = resources.length;
    for (let i = 0; i < count; i++) {
        if ((resources[i & (resourcesLength - 1)] & 1) == 0) {
            f3 += f3 * f1;
        }
        else {
            f3 += f3 * f2;
        }
    }
    let end = Date.now();
    console.log("FloatNumProduction" + f3);
    let time = (end - start) ;
    console.log("Numerical Calculation - FloatNumProduction:\t" + String(time) + "\tms");
    return time;
}
FloatNumProduction();
function FloatNumDivision() {
    let count = 3000000;
    let f3 = 1.0;
    let resources = GenerateFakeRandomInteger();
    let inputs = GenerateFakeRandomFloat();
    let start = Date.now();
    let resourcesLength = resources.length;
    let inputsLength = inputs.length;
    for (let i = 0; i < count; i++) {
        if ((resources[i & (resourcesLength - 1)] & 1) == 0) {
            f3 += f3 / inputs[i & (inputsLength - 1)] / 0.1;
        }
        else {
            f3 += f3 / inputs[(i + resources[i & (resourcesLength - 1)]) & (inputsLength - 1)] / 0.1;
        }
    }
    let end = Date.now();
    console.log("FloatNumDivision" + f3);
    let time = (end - start) ;
    console.log("Numerical Calculation - FloatNumDivision:\t" + String(time) + "\tms");
    return time;
}
FloatNumDivision();
// 位运算
function BitOpsAND() {
    let count = 30000000;
    let bitInputs = GenerateBitOpsInput();
    let b1 = 2; // bitInputs[0]; // 10
    let b2 = 3; // bitInputs[1]; // 11
    let b3 = 1;
    let resources = GenerateFakeRandomInteger();
    let start = Date.now();
    ;
    let resourcesLength = resources.length;
    for (let i = 0; i < count; i++) {
        if ((resources[i % b3 & (resourcesLength - 1)] & 1) == 0) {
            b3 = (b3 & b1) + 1;
        }
        else {
            b3 = (b3 & b2) + 1;
        }
    }
    let end = Date.now();
    console.log("BitOpsAND" + b3);
    let time = (end - start) ;
    console.log("Numerical Calculation - BitOpsAND:\t" + String(time) + "\tms");
    return time;
}
BitOpsAND();
function BitOpsOR() {
    let count = 30000000;
    let b1 = 2; // bitInputs[0]; // 10
    let b2 = 3; // bitInputs[1]; // 11
    let b3 = 1;
    let resources = GenerateFakeRandomInteger();
    let start = Date.now();
    let resourcesLength = resources.length;
    for (let i = 0; i < count; i++) {
        if ((resources[i % b3 & (resourcesLength - 1)] & 1) == 0) {
            b3 = (b3 | b1) + 1;
        }
        else {
            b3 = (b3 | b2) + 1;
        }
    }
    let end = Date.now();
    console.log("BitOpsOR" + b3);
    let time = (end - start) ;
    console.log("Numerical Calculation - BitOpsOR:\t" + String(time) + "\tms");
    return time;
}
BitOpsOR();
function BitOpsXOR() {
    let count = 30000000;
    let b3 = 1;
    let resources = GenerateFakeRandomInteger();
    let start = Date.now();
    let resourcesLength = resources.length;
    for (let i = 0; i < count; i++) {
        if (b3 <= 0) {
            b3 = 1;
        }
        if ((resources[i % b3 & (resourcesLength - 1)] & 1) == 0) {
            b3 = b3 ^ resources[i & (resourcesLength - 1)];
        }
        else {
            b3 = b3 ^ resources[(i + 5) & (resourcesLength - 1)];
        }
    }
    let end = Date.now();
    console.log("BitOpsXOR" + b3);
    let time = (end - start) ;
    console.log("Numerical Calculation - BitOpsXOR:\t" + String(time) + "\tms");
    return time;
}
BitOpsXOR();
function BitOpsNOT() {
    let count = 30000000;
    let b1 = -2;
    let b2 = -3;
    let b3 = 1;
    let resources = GenerateFakeRandomInteger();
    let start = Date.now();
    let resourcesLength = resources.length;
    for (let i = 0; i < count; i++) {
        if ((resources[i % b3 & (resourcesLength - 1)] & 1) == 0) {
            b3 += ~b1;
        }
        else {
            b3 += ~b2;
        }
    }
    let end = Date.now();
    console.log("BitOpsNOT" + b3);
    let time = (end - start) ;
    console.log("Numerical Calculation - BitOpsNOT:\t" + String(time) + "\tms");
    return time;
}
BitOpsNOT();
function BitOpsShiftLeft() {
    let count = 30000000;
    let bitInputs = GenerateBitOpsInput();
    let b1 = 2; // bitInputs[0]; // 10
    let b3 = 1;
    let resources = GenerateFakeRandomInteger();
    let start = Date.now();
    let resourcesLength = resources.length;
    for (let i = 0; i < count; i++) {
        if ((resources[i % b3 & (resourcesLength - 1)] & 1) == 0) {
            b3 += b1 << 1;
        }
        else {
            b3 += b1 << 2;
        }
    }
    let end = Date.now();
    console.log("BitOpsShiftLeft" + b3);
    let time = (end - start) ;
    console.log("Numerical Calculation - BitOpsShiftLeft:\t" + String(time) + "\tms");
    return time;
}
BitOpsShiftLeft();
function BitOpsShiftRight() {
    let count = 30000000;
    let bitInputs = GenerateBitOpsInput();
    let b1 = 2; // bitInputs[0]; // 10
    let b3 = 1;
    let resources = GenerateFakeRandomInteger();
    let start = Date.now();
    let resourcesLength = resources.length;
    for (let i = 0; i < count; i++) {
        if ((resources[i % b3 & (resourcesLength - 1)] & 1) == 0) {
            b3 += b1 >> 1;
        }
        else {
            b3 += b1 >> 2;
        }
    }
    let end = Date.now();
    console.log("BitOpsShiftRight" + b3);
    let time = (end - start) ;
    console.log("Numerical Calculation - BitOpsShiftRight:\t" + String(time) + "\tms");
    return time;
}
BitOpsShiftRight();
// 整数计算
function IntegerNumAddition() {
    let count = 30000000;
    let IntegerInputs = GenerateIntegerInput();
    let resources = GenerateFakeRandomInteger();
    let i1 = 2; // IntegerInputs[0];
    let i2 = 4; // IntegerInputs[1];
    let i3 = resources[0];
    let start = Date.now();
    let resourcesLength = resources.length;
    for (let i = 0; i < count; i++) {
        if ((resources[i % i3 & (resourcesLength - 1)] & 1) == 0) { // 1.07ms
            i3 += i1;
        }
        else {
            i3 += i2;
        }
    }
    let end = Date.now();
    let time = (end - start) ;
    console.log("Numerical Calculation - IntegerNumAddition:\t" + String(time) + "\tms");
    return time;
}
IntegerNumAddition();
function IntegerNumSubstraction() {
    let count = 30000000;
    let IntegerInputs = GenerateIntegerInput();
    let i1 = 2; // IntegerInputs[0];
    let i2 = 4; // IntegerInputs[1];
    let i3 = 130000000;
    let resources = GenerateFakeRandomInteger();
    let start = Date.now();
    let resourcesLength = resources.length;
    for (let i = 0; i < count; i++) {
        if ((resources[i % i3 & (resourcesLength - 1)] & 1) == 0) { // 1.07ms
            i3 -= i1;
        }
        else {
            i3 -= i2;
        }
    }
    let end = Date.now();
    console.log("IntegerNumSubstraction" + i3);
    let time = (end - start) ;
    console.log("Numerical Calculation - IntegerNumSubstraction:\t" + String(time) + "\tms");
    return time;
}
IntegerNumSubstraction();
function IntegerNumProduction() {
    let count = 30000000;
    let i3 = 1;
    let resources = GenerateFakeRandomInteger();
    let start = Date.now();
    let resourcesLength = resources.length - 1;
    for (let i = 0; i < count; i++) {
        if ((resources[i % i3 & resourcesLength] & 1) == 0) { // ark:21ms node:4ms
            i3 *= 3; // resources[i & resourcesLength]
        }
        else {
            i3 *= 2; // resources[(i + 5) & resourcesLength]
        }
        if (i3 > 10000000) {
            i3 = 1;
        }
    }
    let end = Date.now();
    console.log("IntegerNumProduction" + i3);
    let time = (end - start) ;
    console.log("Numerical Calculation - IntegerNumProduction:\t" + String(time) + "\tms");
    return time;
}
IntegerNumProduction();
function IntegerNumDivision() {
    let count = 30000000; // input;
    let i1 = 2; // IntegerInputs[0];
    let i2 = 4; // IntegerInputs[1];
    let i3 = 32768;
    let resources = GenerateFakeRandomInteger();
    let start = Date.now();
    let resourcesLength = resources.length;
    for (let i = 0; i < count; i++) {
        if (i3 <= 4) {
            i3 = 32768;
        }
        if ((resources[i % i3 & (resourcesLength - 1)] & 1) == 0) {
            i3 = i3 / i1;
        }
        else {
            i3 = i3 / i2;
        }
    }
    let end = Date.now();
    console.log("IntegerNumDivision" + i3);
    let time = (end - start) ;
    console.log("Numerical Calculation - IntegerNumDivision:\t" + String(time) + "\tms");
    return time;
}
IntegerNumDivision();
// 浮点数比较
function FloatNumComparision() {
    let count = 3000000; // input;
    let resources = GenerateFakeRandomFloat();
    let res = 1;
    let start = Date.now();
    let resourcesLength = resources.length;
    for (let i = 0; i < count; i++) {
        let next_index = i + 5;
        let next_res = res + 1;
        if (resources[i % res & (resourcesLength - 1)] > resources[next_index % next_res & (resourcesLength - 1)]) {
            res += 1;
        }
        else {
            res += 2;
        }
    }
    let end = Date.now();
    console.log("FloatNumComparision" + res);
    let time = (end - start) ;
    console.log("Numerical Calculation - FloatNumComparision:\t" + String(time) + "\tms");
    return time;
}
FloatNumComparision();
// String运算
function StringCalculation() {
    let count = 3000000 ;
    let str1 = "h";
    let res = "11";
    let resources = GenerateFakeRandomInteger();
    let start = Date.now();
    for (let i = 0; i < count; i++) {
        if (resources[i % res.length % 15] > resources[(i + resources[i % 15]) % res.length % 15]) {
            res += str1 + i;
        }
        else {
            res += str1;
        }
    }
    let end = Date.now();
    console.log(res.length);
    let time = (end - start) ;
    console.log("Numerical Calculation - StringCalculation:\t" + String(time) + "\tms");
    return time;
}
StringCalculation();
