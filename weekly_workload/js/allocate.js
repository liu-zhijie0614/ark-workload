"use strict";
class Obj {
    constructor(num) {
        this.num = 0;
        this.num = num;
    }
}
function AllocateObj() {
    let count = 100000;
    let data = [new Obj(1), new Obj(1), new Obj(1), new Obj(1), new Obj(1)]; // new Array();
    let resources = new Int32Array([12, 43, 56, 76, 89, 54, 45, 32, 35, 47, 46, 44, 21, 37, 84]);
    var start = Date.now();
    let resourcesLength = resources.length - 1;
    for (let i = 0; i < count; i++) {
        if ((resources[i & resourcesLength] & 1) == 0) {
            data[i & 4] = new Obj(i);
        }
        else {
            data[i & 4] = new Obj(i - 5);
        }
    }
    var end = Date.now();
    let res = 0;
    for (let i = 0; i < data.length; i++) {
        res += data[i].num;
    }
    console.log(res);
    let time = (end - start) ;
    console.log("Allocate Obj :\t" + time + "\tms");
}
AllocateObj();
