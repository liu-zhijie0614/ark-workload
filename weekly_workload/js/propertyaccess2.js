"use strict";
// 多态
class Base {
    constructor(num) {
        this.baseNum = 1;
        this.baseNum = num;
    }
    compute() {
        return this.baseNum;
    }
}
class DeriveDouble extends Base {
    constructor(num) {
        super(num);
    }
    compute() {
        return this.baseNum * 2;
    }
}
class DerivedTripple extends Base {
    constructor(num) {
        super(num);
    }
    compute() {
        return this.baseNum * 3;
    }
}
function Polymorphism() {
    let count = 100000;
    var result = new Array();
    var result2 = new Array();
    var result3 = new Array();
    for (let i = 0; i < count; i++) {
        result.push(i);
        result2.push(new DeriveDouble(i));
        result3.push(new DerivedTripple(i));
    }
    let start = Date.now();
    for (let i = 0; i < count; i++) {
        if (result[i] == i) {
            result2[i].baseNum = result2[i].compute();
            result3[i].baseNum = result3[i].compute();
        }
    }
    let end = Date.now();
    var res = true;
    for (let i = 0; i < count; i++) {
        if (result2[i].baseNum != i * 2 || result3[i].baseNum != i * 3) {
            res = false;
        }
    }
    if (!res) {
        console.log("result is wrong");
    }
    let time = (end - start) ;
    console.log("Property Access - Polymorphism:\t" + String(time) + "\tms");
    return time;
}
Polymorphism();
// let runner9 = new BenchmarkRunner("Property Access - Polymorphism", Polymorphism);
// runner9.run();
// 单态
class Square {
    constructor(length, width) {
        this.length = 0;
        this.width = 0;
        this.length = length;
        this.width = width;
    }
}
function GenerateFakeRandomSquare() {
    var resource = new Array(10);
    for (let i = 0; i < 10; i++) {
        let random1 = Math.random() * (10) + 1;
        let random2 = Math.random() * (10) + 1;
        resource[i] = new Square(random1, random2);
    }
    return resource;
}
function SingleICClass() {
    let container = GenerateFakeRandomSquare();
    let count = 1000000;
    let arraySize = 3;
    let length_res = [0, 0, 0];
    let width_res = [0, 0, 0];
    let start = Date.now();
    for (let i = 0; i < count; i++) {
        for (let j = 0; j < container.length; j++) {
            let thisBox = container[j];
            length_res[i % arraySize] += thisBox.length;
            width_res[i % arraySize] += thisBox.width;
        }
    }
    let end = Date.now();
    let res = 0;
    for (let j = 0; j < arraySize; j++) {
        res += length_res[j] + width_res[j];
    }
    console.log(res);
    let time = (end - start) ;
    console.log("Property Access - SingleICClass:\t" + String(time) + "\tms");
    return time;
}
SingleICClass();
// let runner2 = new BenchmarkRunner("Property Access - SingleICClass", SingleICClass);
// runner2.run();
// 不可扩展属性
class Person {
    constructor(name, age) {
        this.name = "";
        this.age = 0;
        this.name = name;
        this.age = age;
    }
    equal(cmp) {
        return this.name == cmp.name && this.age == cmp.age;
    }
}
class Student extends Person {
    constructor(name, age, university) {
        super(name, age);
        this.university = "";
        this.university = university;
    }
}
function GenerateFakeRandomPersons() {
    let person1 = new Person("John", Math.random() * (50) + 1);
    let person2 = new Person("John", Math.random() * (50) + 1);
    let person3 = new Person("John", Math.random() * (50) + 1);
    let resources = [person1, person2, person3];
    return resources;
}
function NoneExtension() {
    let person1 = new Person("John", 12);
    let person2 = new Person("John", 13);
    let person3 = new Person("John", 14);
    let resourcesPerson = [person1, person2, person3];
    let student1 = new Student("John", 21, "UNY");
    let count = 100000;
    let arraySize = 3;
    let res = new Int32Array([0, 0, 0]);
    let test = person1;
    let start = Date.now();
    for (let i = 0; i < count; i++) {
        // if (resourcesPerson[i % resourcesPerson.length].name == "John" && resourcesPerson[i % resourcesPerson.length].age <= 20) {
        //     if (student1.university == "UNY") {
        //         res[i % arraySize] += 1;
        //     }
        // } else {
        //     if (student1.age == 21) {
        //         res[i % arraySize] += 2;
        //     }
        // }
        let a = resourcesPerson[i % arraySize];
        if (a.equal(person1)) {
            let b = resourcesPerson[(i + 1) % arraySize];
            if (b.equal(person2)) {
                let c = resourcesPerson[(i + 2) % arraySize];
                if (c.equal(person3)) {
                    test = c;
                }
            }
        }
        if (i % arraySize == 0 && test.equal(person3)) {
            res[i % 3] += 1;
            test = person1;
        }
        if (i % arraySize == 0 && test.equal(person1)) {
            res[i % 3] += 1;
            test = person2;
        }
    }
    let end = Date.now();
    let sum = 0;
    for (let j = 0; j < arraySize; j++) {
        sum += res[j];
    }
    console.log(sum);
    let time = (end - start) ;
    console.log("Property Access - NoneExtension:\t" + String(time) + "\tms");
    return time;
}
NoneExtension();
// Getter & Setter
class MyObject {
    constructor(num, age) {
        this.num = num;
        this.age = age;
    }
    get Getter() {
        return this.num;
    }
    set Setter(n) {
        this.age = n;
    }
}
function GenerateNumArray(count) {
    let numArray = new Array(count);
    for (let i = 0; i < count; i++) {
        numArray[i] = i + 1;
    }
    return numArray;
}
function GenerateResultArray(count) {
    let result = new Array(count);
    for (let i = 0; i < count; i++) {
        result[i] = new MyObject(i, i);
    }
    return result;
}
function GetterSetterTest() {
    let count = 10000000;
    let result = GenerateResultArray(10);
    let numArray = GenerateNumArray(10);
    let start = Date.now();
    let resLen = result.length;
    for (let i = 0; i < count; i++) {
        let index = i % resLen;
        let ret = result[index];
        if (ret.Getter == numArray[index]) {
            ret.Setter = index;
        }
    }
    let end = Date.now();
    let time = (end - start) ;
    console.log("Property Access - GetterSetterTest:\t" + String(time) + "\tms");
    return result;
}
let result = GetterSetterTest();
let numArray = GenerateNumArray(result.length);
var sum = 0;
var res = true;
for (let i = 0; i < result.length; i++) {
    sum += result[i].num;
    if (result[i].age != numArray[i]) {
        res = false;
        break;
    }
}
console.log(res);
console.log(sum);
class Grades {
    constructor(math, english, physics, chemistry) {
        this.math = 0.0;
        this.english = 0.0;
        this.physics = 0.0;
        this.chemistry = 0.0;
        this.math = math;
        this.english = english;
        this.physics = physics;
        this.chemistry = chemistry;
    }
}
function GenerateFakeRandomGrades() {
    let resource = new Array(10);
    for (let i = 0; i < 10; i++) {
        let random1 = Math.random() * (100.0) + 1.0;
        let random2 = Math.random() * (100.0) + 1.0;
        let random3 = Math.random() * (100.0) + 1.0;
        let random4 = Math.random() * (100.0) + 1.0;
        resource[i] = new Grades(random1, random2, random3, random4);
    }
    return resource;
}
function NumberObject() {
    let count = 1000000;
    let arraySize = 2;
    let myGrades = GenerateFakeRandomGrades();
    let res = new Float64Array([0, 0, 0]);
    let start = Date.now();
    let gradesLength = myGrades.length - 1;
    for (let i = 0; i < count; i++) {
        let ret = res[i & arraySize];
        let grades = myGrades[i & gradesLength];
        ret += grades.chemistry;
        ret += grades.english;
        ret += grades.math;
        ret += grades.physics;
        res[i & arraySize] = ret;
    }
    let end = Date.now();
    let sum = 0;
    for (let j = 0; j < arraySize; j++) {
        sum += res[j];
    }
    console.log(sum);
    let time = (end - start) ;
    console.log("Property Access - NumberObject:\t" + String(time) + "\tms");
    return time;
}
NumberObject();
