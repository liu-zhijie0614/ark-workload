/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { B2Vec2, b2MulT2, b2DistanceSquared, b2Dot22, b2MulR2, b2Min, b2Max, b2Abs2, add, subtract, multM, minus } from '../Common/b2Math';
import { B2DistanceInput, B2SimplexCache, B2DistanceOutput, b2DistanceM } from './b2Distance';
import { b2_maxManifoldPoints, b2_minFloat, b2_maxFloat, b2_epsilon } from '../Common/b2Settings';
import { float_2 } from '../Common/b2Settings';
import { half } from '../Common/b2Settings';
import { float_10 } from '../Common/b2Settings';
var B2ContactFeatureType;
(function (B2ContactFeatureType) {
    B2ContactFeatureType[B2ContactFeatureType["vertex"] = 0] = "vertex";
    B2ContactFeatureType[B2ContactFeatureType["face"] = 1] = "face";
})(B2ContactFeatureType || (B2ContactFeatureType = {}));
export class B2ContactFeature {
    constructor() {
        this.indexA = 0;
        this.indexB = 0;
        this.typeA = B2ContactFeatureType.vertex;
        this.typeB = B2ContactFeatureType.vertex;
    }
    setZero() {
        this.indexA = 0;
        this.indexB = 0;
        this.typeA = B2ContactFeatureType.vertex;
        this.typeB = B2ContactFeatureType.vertex;
    }
}
export class B2ManifoldPoint {
    constructor(copyFrom = null) {
        this.localPoint = new B2Vec2();
        this.normalImpulse = 0.0;
        this.tangentImpulse = 0.0;
        this.id = new B2ContactFeature();
        if (copyFrom != null) {
            this.localPoint = copyFrom.localPoint;
            this.normalImpulse = copyFrom.normalImpulse;
            this.tangentImpulse = copyFrom.tangentImpulse;
            this.id = copyFrom.id;
        }
    }
}
var B2ManifoldType;
(function (B2ManifoldType) {
    B2ManifoldType[B2ManifoldType["circles"] = 0] = "circles";
    B2ManifoldType[B2ManifoldType["faceA"] = 1] = "faceA";
    B2ManifoldType[B2ManifoldType["faceB"] = 2] = "faceB";
})(B2ManifoldType || (B2ManifoldType = {}));
export class B2Manifold {
    get pointCount() {
        return this.points.length;
    }
    constructor(copyFrom = null) {
        this.points = [];
        this.localNormal = new B2Vec2();
        this.localPoint = new B2Vec2();
        this.type = B2ManifoldType.circles;
        if (copyFrom != null) {
            this.points = [];
            for (let i = 0; i < copyFrom.points.length; i++) {
                this.points.push(new B2ManifoldPoint(copyFrom.points[i]));
            }
            this.localNormal = copyFrom.localNormal;
            this.localPoint = copyFrom.localPoint;
            this.type = copyFrom.type;
        }
    }
}
export class B2WorldManifold {
    constructor() {
        this.normal = new B2Vec2();
        this.points = [];
        this.separations = [];
        for (let i = 0; i < b2_maxManifoldPoints; i++) {
            this.points.push(new B2Vec2(0, 0));
            this.separations.push(0);
        }
    }
    initialize(manifold, xfA, radiusA, xfB, radiusB) {
        if (manifold.pointCount == 0) {
            return;
        }
        switch (manifold.type) {
            case B2ManifoldType.circles:
                this.normal.set(1.0, 0.0);
                let pointA = b2MulT2(xfA, manifold.localPoint);
                let pointB = b2MulT2(xfB, manifold.points[0].localPoint);
                if (b2DistanceSquared(pointA, pointB) > b2_epsilon * b2_epsilon) {
                    this.normal = subtract(pointB, pointA);
                    this.normal.normalize();
                }
                let cA = add(pointA, multM(this.normal, radiusA));
                let cB = subtract(pointB, multM(this.normal, radiusB));
                this.points[0] = multM(add(cA, cB), half);
                this.separations[0] = b2Dot22(subtract(cB, cA), this.normal);
                break;
            case B2ManifoldType.faceA:
                this.normal = b2MulR2(xfA.q, manifold.localNormal);
                let planePointA = b2MulT2(xfA, manifold.localPoint);
                for (let i = 0; i < manifold.pointCount; i++) {
                    let clipPoint = b2MulT2(xfB, manifold.points[i].localPoint);
                    let cA = add(clipPoint, multM(this.normal, radiusA - b2Dot22(subtract(clipPoint, planePointA), this.normal)));
                    let cB = subtract(clipPoint, multM(this.normal, radiusB));
                    this.points[i] = multM(add(cA, cB), half);
                    this.separations[i] = b2Dot22(subtract(cB, cA), this.normal);
                }
                break;
            case B2ManifoldType.faceB:
                this.normal = b2MulR2(xfB.q, manifold.localNormal);
                let planePointB = b2MulT2(xfB, manifold.localPoint);
                for (let i = 0; i < manifold.pointCount; i++) {
                    let clipPoint = b2MulT2(xfA, manifold.points[i].localPoint);
                    let cB = add(clipPoint, multM(this.normal, radiusB - b2Dot22(subtract(clipPoint, planePointB), this.normal)));
                    let cA = subtract(clipPoint, multM(this.normal, radiusB));
                    this.points[i] = multM(add(cA, cB), half);
                    this.separations[i] = b2Dot22(subtract(cA, cB), this.normal);
                }
                this.normal = minus(this.normal);
                break;
            default:
                break;
        }
    }
}
export class B2ClipVertex {
    constructor() {
        this.v = new B2Vec2();
        this.id = new B2ContactFeature();
    }
}
export class B2RayCastInput {
    constructor() {
        this.p1 = new B2Vec2();
        this.p2 = new B2Vec2();
        this.maxFraction = 0;
    }
}
export class B2RayCastOutput {
    constructor() {
        this.normal = new B2Vec2();
        this.fraction = 0;
    }
}
export class B2AABB {
    constructor(lowerBound = null, upperBound = null) {
        this.lowerBound = new B2Vec2();
        this.upperBound = new B2Vec2();
        if (lowerBound != null && upperBound != null) {
            this.lowerBound = lowerBound;
            this.upperBound = upperBound;
        }
    }
    isValid() {
        let d = subtract(this.upperBound, this.lowerBound);
        let valid = d.x >= 0.0 && d.y >= 0.0;
        valid = valid && this.lowerBound.isValid() && this.upperBound.isValid();
        return valid;
    }
    center() {
        return multM(add(this.lowerBound, this.upperBound), half);
    }
    extents() {
        return multM(subtract(this.upperBound, this.lowerBound), half);
    }
    get perimeter() {
        let wx = this.upperBound.x - this.lowerBound.x;
        let wy = this.upperBound.y - this.lowerBound.y;
        return float_2 * (wx + wy);
    }
    combine(aabb1, aabb2) {
        this.lowerBound = b2Min(aabb1.lowerBound, aabb2.lowerBound);
        this.upperBound = b2Max(aabb1.upperBound, aabb2.upperBound);
    }
    contains(aabb) {
        let result = true;
        result = result && this.lowerBound.x <= aabb.lowerBound.x;
        result = result && this.lowerBound.y <= aabb.lowerBound.y;
        result = result && aabb.upperBound.x <= this.upperBound.x;
        result = result && aabb.upperBound.y <= this.upperBound.y;
        return result;
    }
    rayCast(input) {
        let tmin = b2_minFloat;
        let tmax = b2_maxFloat;
        let p = input.p1;
        let d = subtract(input.p2, input.p1);
        let absD = b2Abs2(d);
        let normal = new B2Vec2();
        for (let i = 0; i < float_2; i++) {
            if (absD.getSubscript(i) < b2_epsilon) {
                if (p.getSubscript(i) < this.lowerBound.getSubscript(i) ||
                    this.upperBound.getSubscript(i) < p.getSubscript(i)) {
                    return null;
                }
            }
            else {
                let inv_d = 1.0 / d.getSubscript(i);
                let t1 = (this.lowerBound.getSubscript(i) - p.getSubscript(i)) * inv_d;
                let t2 = (this.upperBound.getSubscript(i) - p.getSubscript(i)) * inv_d;
                let s = -1.0;
                if (t1 > t2) {
                    t1 = t2;
                    t2 = t1;
                    s = 1.0;
                }
                if (t1 > tmin) {
                    normal.setZero();
                    normal.setSubscript(i, s);
                    tmin = t1;
                }
                tmax = Math.min(tmax, t2);
                if (tmin > tmax) {
                    return null;
                }
            }
        }
        if (tmin < 0.0 || input.maxFraction < tmin) {
            return null;
        }
        let output = new B2RayCastOutput();
        output.fraction = tmin;
        output.normal = normal;
        return output;
    }
}
function b2ClipSegmentToLine(vIn, normal, offset, vertexIndexA) {
    let vOut = [];
    let distance0 = b2Dot22(normal, vIn[0].v) - offset;
    let distance1 = b2Dot22(normal, vIn[1].v) - offset;
    if (distance0 <= 0.0) {
        vOut.push(vIn[0]);
    }
    if (distance1 <= 0.0) {
        vOut.push(vIn[1]);
    }
    if (distance0 * distance1 < 0.0) {
        let interp = distance0 / (distance0 - distance1);
        let v = new B2ClipVertex();
        v.v = add(vIn[0].v, multM(subtract(vIn[1].v, vIn[0].v), interp));
        v.id.indexA = vertexIndexA;
        v.id.indexB = vIn[0].id.indexB;
        v.id.typeA = B2ContactFeatureType.vertex;
        v.id.typeB = B2ContactFeatureType.face;
        vOut.push(v);
    }
    return vOut;
}
function b2TestOverlap(shapeA, indexA, shapeB, indexB, xfA, xfB) {
    let input = new B2DistanceInput();
    input.proxyA.set(shapeA, indexA);
    input.proxyB.set(shapeB, indexB);
    input.transformA = xfA;
    input.transformB = xfB;
    input.useRadii = true;
    let cache = new B2SimplexCache();
    cache.count = 0;
    let output = new B2DistanceOutput();
    b2DistanceM(output, cache, input);
    return output.distance < float_10 * b2_epsilon;
}
function b2TestOverlap2(a, b) {
    let d1 = subtract(b.lowerBound, a.upperBound);
    let d2 = subtract(a.lowerBound, b.upperBound);
    if (d1.x > 0.0 || d1.y > 0.0) {
        return false;
    }
    if (d2.x > 0.0 || d2.y > 0.0) {
        return false;
    }
    return true;
}
export { B2ManifoldType, b2TestOverlap, b2ClipSegmentToLine, b2TestOverlap2, B2ContactFeatureType };
