/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { b2TestOverlap2 } from './b2Collision';
import { B2DynamicTree } from './b2DynamicTree';
export class B2Pair {
    constructor(proxyIdA = -1, proxyIdB = -1) {
        this.proxyIdA = -1;
        this.proxyIdB = -1;
        this.proxyIdA = proxyIdA;
        this.proxyIdB = proxyIdB;
    }
}
export class B2BroadPhase {
    constructor() {
        this.m_tree = new B2DynamicTree();
        this.m_proxyCount = 0;
        this.m_moveBuffer = [];
        this.m_pairBuffer = [];
        this.m_queryProxyId = 0;
        this.m_proxyCount = 0;
        this.m_pairBuffer = [];
        this.m_moveBuffer = [];
    }
    createProxy(aabb, userData) {
        let proxyId = this.m_tree.createProxy(aabb, userData);
        this.m_proxyCount += 1;
        this.bufferMove(proxyId);
        return proxyId;
    }
    destroyProxy(proxyId) {
        this.unBufferMove(proxyId);
        this.m_proxyCount -= 1;
        this.m_tree.destroyProxy(proxyId);
    }
    moveProxy(proxyId, aabb, displacement) {
        let buffer = this.m_tree.moveProxy(proxyId, aabb, displacement);
        if (buffer) {
            this.bufferMove(proxyId);
        }
    }
    touchProxy(proxyId) {
        this.bufferMove(proxyId);
    }
    getFatAABB(proxyId) {
        return this.m_tree.getFatAABB(proxyId);
    }
    getUserData(proxyId) {
        return this.m_tree.getUserData(proxyId);
    }
    testOverlap(proxyIdA, proxyIdB) {
        let aabbA = this.m_tree.getFatAABB(proxyIdA);
        let aabbB = this.m_tree.getFatAABB(proxyIdB);
        return b2TestOverlap2(aabbA, aabbB);
    }
    getProxyCount() {
        return this.m_proxyCount;
    }
    updatePairs(callback) {
        this.m_pairBuffer.splice(0, this.m_pairBuffer.length);
        for (let i = 0; i < this.m_moveBuffer.length; i++) {
            this.m_queryProxyId = this.m_moveBuffer[i];
            if (this.m_queryProxyId == B2BroadPhase.nullProxy) {
                continue;
            }
            let fatAABB = this.m_tree.getFatAABB(this.m_queryProxyId);
            this.m_tree.query(this, fatAABB);
        }
        this.m_moveBuffer.splice(0, this.m_moveBuffer.length);
        this.m_pairBuffer.sort((a, b) => {
            if (a.proxyIdA == b.proxyIdA) {
                return a.proxyIdB - b.proxyIdB;
            }
            else {
                return a.proxyIdA - b.proxyIdA;
            }
        });
        let i = 0;
        while (i < this.m_pairBuffer.length) {
            let primaryPair = this.m_pairBuffer[i];
            let userDataA = this.m_tree.getUserData(primaryPair.proxyIdA);
            let userDataB = this.m_tree.getUserData(primaryPair.proxyIdB);
            callback.addPair(userDataA, userDataB);
            i += 1;
            while (i < this.m_pairBuffer.length) {
                let pair = this.m_pairBuffer[i];
                if (pair.proxyIdA != primaryPair.proxyIdA || pair.proxyIdB != primaryPair.proxyIdB) {
                    break;
                }
                i += 1;
            }
        }
    }
    query(callback, aabb) {
        this.m_tree.query(callback, aabb);
    }
    rayCast(callback, input) {
        this.m_tree.rayCast(callback, input);
    }
    getTreeHeight() {
        return this.m_tree.getHeight();
    }
    getTreeBalance() {
        return this.m_tree.getMaxBalance();
    }
    getTreeQuality() {
        return this.m_tree.getAreaRatio();
    }
    shiftOrigin(newOrigin) {
        this.m_tree.shiftOrigin(newOrigin);
    }
    bufferMove(proxyId) {
        this.m_moveBuffer.push(proxyId);
    }
    unBufferMove(proxyId) {
        for (let i = 0; i < this.m_moveBuffer.length; i++) {
            if (this.m_moveBuffer[i] == proxyId) {
                this.m_moveBuffer[i] = B2BroadPhase.nullProxy;
            }
        }
    }
    queryCallback(proxyId) {
        if (proxyId == this.m_queryProxyId) {
            return true;
        }
        let pair = new B2Pair(Math.min(proxyId, this.m_queryProxyId), Math.max(proxyId, this.m_queryProxyId));
        this.m_pairBuffer.push(pair);
        return true;
    }
}
B2BroadPhase.nullProxy = -1;
