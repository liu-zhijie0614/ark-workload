/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { B2Vec2, add, b2MulR2, subtract, b2Dot22, b2Sqrt, multM, b2MulT2, b2Min, b2Max, addEqual, mulEqual, b2Cross } from '../../Common/b2Math';
import { float_2 } from '../../Common/b2Settings';
import { half } from '../../Common/b2Settings';
import { float_025 } from '../../Common/b2Settings';
import { float_3 } from '../../Common/b2Settings';
import { int_4 } from '../../Common/b2Settings';
import { b2_epsilon, b2_pi, b2_polygonRadius } from '../../Common/b2Settings';
import { B2Array } from '../../Dynamics/b2TimeStep';
export class B2MassData {
    constructor() {
        this.mass = 0;
        this.center = new B2Vec2();
        this.I = 0;
    }
}
var B2ShapeType;
(function (B2ShapeType) {
    B2ShapeType[B2ShapeType["circle"] = 0] = "circle";
    B2ShapeType[B2ShapeType["edge"] = 1] = "edge";
    B2ShapeType[B2ShapeType["polygon"] = float_2] = "polygon";
    B2ShapeType[B2ShapeType["chain"] = float_3] = "chain";
    B2ShapeType[B2ShapeType["typeCount"] = int_4] = "typeCount";
})(B2ShapeType || (B2ShapeType = {}));
export class B2Shape {
    constructor() {
        this.m_type = B2ShapeType.circle;
        this.m_radius = 0;
    }
    clone() {
        throw new Error('must override');
    }
    type() {
        return this.m_type;
    }
    childCount() {
        throw new Error('must override');
    }
    testPoint(transform, point) {
        throw new Error('must override');
    }
    rayCast(output, input, transform, childIndex) {
        throw new Error('must override');
    }
    computeAABB(aabb, transform, childIndex) {
        throw new Error('must override');
    }
    computeMass(density) {
        throw new Error('must override');
    }
    get radius() {
        return this.m_radius;
    }
    set radius(newValue) {
        this.m_radius = newValue;
    }
}
export class B2CircleShape extends B2Shape {
    constructor() {
        super();
        this.m_p_ = new B2Array(1, new B2Vec2());
        this.m_type = B2ShapeType.circle;
        this.m_radius = 0.0;
        this.m_p = new B2Vec2(0.0, 0.0);
    }
    clone() {
        let clone = new B2CircleShape();
        clone.m_radius = this.m_radius;
        clone.m_p = this.m_p;
        return clone;
    }
    childCount() {
        return 1;
    }
    testPoint(transform, point) {
        let center = add(transform.p, b2MulR2(transform.q, this.m_p));
        let d = subtract(this.p, center);
        return b2Dot22(d, d) <= this.m_radius * this.m_radius;
    }
    rayCast(output, input, transform, childIndex) {
        let position = add(transform.p, b2MulR2(transform.q, this.m_p));
        let s = subtract(input.p1, position);
        let b = b2Dot22(s, s) - this.m_radius * this.m_radius;
        let r = subtract(input.p2, input.p1);
        let c = b2Dot22(s, r);
        let rr = b2Dot22(r, r);
        let sigma = c * c - rr * b;
        if (sigma < 0.0 || rr < b2_epsilon) {
            return false;
        }
        let a = -(c + b2Sqrt(sigma));
        if (0.0 <= a && a <= input.maxFraction * rr) {
            a /= rr;
            output.fraction = a;
            output.normal = add(s, multM(r, a));
            output.normal.normalize();
            return true;
        }
        return false;
    }
    computeAABB(aabb, transform, childIndex) {
        let p = add(transform.p, b2MulR2(transform.q, this.m_p));
        aabb.lowerBound.set(p.x - this.m_radius, p.y - this.m_radius);
        aabb.upperBound.set(p.x + this.m_radius, p.y + this.m_radius);
    }
    computeMass(density) {
        let massData = new B2MassData();
        massData.mass = density * b2_pi * this.m_radius * this.m_radius;
        massData.center = this.m_p;
        massData.I = massData.mass * (half * this.m_radius * this.m_radius + b2Dot22(this.m_p, this.m_p));
        return massData;
    }
    getSupport(direction) {
        return 0;
    }
    getSupportVertex(direction) {
        return this.m_p;
    }
    vertexCount() {
        return 1;
    }
    vertex(index) {
        return this.m_p;
    }
    get p() {
        return this.m_p_.get(0);
    }
    set p(newValue) {
        this.m_p_.set(0, newValue);
    }
    get m_p() {
        return this.m_p_.get(0);
    }
    set m_p(newValue) {
        this.m_p_.set(0, newValue);
    }
}
export class B2EdgeShape extends B2Shape {
    constructor() {
        super();
        this.m_vertices = new B2Array(float_2, new B2Vec2());
        this.m_vertex0 = new B2Vec2(0.0, 0.0);
        this.m_vertex3 = new B2Vec2(0.0, 0.0);
        this.m_hasVertex0 = false;
        this.m_hasVertex3 = false;
        this.m_type = B2ShapeType.edge;
        this.m_radius = b2_polygonRadius;
    }
    set(v1, v2) {
        this.m_vertex1 = v1;
        this.m_vertex2 = v2;
        this.m_hasVertex0 = false;
        this.m_hasVertex3 = false;
    }
    clone() {
        let clone = new B2EdgeShape();
        clone.m_radius = this.m_radius;
        clone.m_vertices = this.m_vertices.clone();
        clone.m_vertex0 = this.m_vertex0;
        clone.m_vertex3 = this.m_vertex3;
        clone.m_hasVertex0 = this.m_hasVertex0;
        clone.m_hasVertex3 = this.m_hasVertex3;
        return clone;
    }
    childCount() {
        return 1;
    }
    testPoint(transform, point) {
        return false;
    }
    computeAABB(aabb, transform, childIndex) {
        let v1 = b2MulT2(transform, this.m_vertex1);
        let v2 = b2MulT2(transform, this.m_vertex2);
        let lower = b2Min(v1, v2);
        let upper = b2Max(v1, v2);
        let r = new B2Vec2(this.m_radius, this.m_radius);
        aabb.lowerBound = subtract(lower, r);
        aabb.upperBound = add(upper, r);
    }
    computeMass(density) {
        let massData = new B2MassData();
        massData.mass = 0.0;
        massData.center = multM(add(this.m_vertex1, this.m_vertex2), half);
        massData.I = 0.0;
        return massData;
    }
    get vertex1() {
        return this.m_vertices.get(0);
    }
    set vertex1(newValue) {
        this.m_vertices.set(0, newValue);
    }
    get vertex2() {
        return this.m_vertices.get(1);
    }
    set vertex2(newValue) {
        this.m_vertices.set(1, newValue);
    }
    get vertex0() {
        return this.m_vertex0;
    }
    set vertex0(newValue) {
        this.m_vertex0 = newValue;
    }
    get vertex3() {
        return this.m_vertex3;
    }
    set vertex3(newValue) {
        this.m_vertex3 = newValue;
    }
    get hasVertex0() {
        return this.m_hasVertex0;
    }
    set hasVertex0(newValue) {
        this.m_hasVertex0 = newValue;
    }
    get hasVertex3() {
        return this.m_hasVertex3;
    }
    set hasVertex3(newValue) {
        this.m_hasVertex3 = newValue;
    }
    get m_vertex1() {
        return this.m_vertices.get(0);
    }
    set m_vertex1(newValue) {
        this.m_vertices.set(0, newValue);
    }
    get m_vertex2() {
        return this.m_vertices.get(1);
    }
    set m_vertex2(newValue) {
        this.m_vertices.set(1, newValue);
    }
}
export class B2PolygonShape extends B2Shape {
    constructor() {
        super();
        this.m_centroid = new B2Vec2();
        this.m_vertices = new B2Array();
        this.m_normals = new B2Array();
        this.m_centroid = new B2Vec2(0.0, 0.0);
        this.m_type = B2ShapeType.polygon;
        this.m_radius = b2_polygonRadius;
    }
    clone() {
        let clone = new B2PolygonShape();
        clone.m_centroid = this.m_centroid;
        clone.m_vertices = this.m_vertices.clone();
        clone.m_normals = this.m_normals.clone();
        clone.m_radius = this.m_radius;
        return clone;
    }
    childCount() {
        return 1;
    }
    setAsEdge(v1, v2) {
        this.m_vertices.append(v1);
        this.m_vertices.append(v2);
        this.m_centroid.x = half * (v1.x + v2.x);
        this.m_centroid.y = half * (v1.y + v2.y);
        this.m_normals.append(this.crossVF(this.subtractVV(v2, v1), 1.0));
        let mn0 = this.m_normals.get(0);
        mn0.normalize();
        this.m_normals.append(new B2Vec2(-this.m_normals.get(0).x, -this.m_normals.get(0).y));
    }
    setAsBox(hx, hy) {
        this.m_vertices.removeAll();
        this.m_vertices.append(new B2Vec2(-hx, -hy));
        this.m_vertices.append(new B2Vec2(hx, -hy));
        this.m_vertices.append(new B2Vec2(hx, hy));
        this.m_vertices.append(new B2Vec2(-hx, hy));
        this.m_normals.removeAll();
        this.m_normals.append(new B2Vec2(0.0, -1.0));
        this.m_normals.append(new B2Vec2(1.0, 0.0));
        this.m_normals.append(new B2Vec2(0.0, 1.0));
        this.m_normals.append(new B2Vec2(-1.0, 0.0));
        this.m_centroid.setZero();
    }
    subtractVV(a, b) {
        let v = new B2Vec2(a.x - b.x, a.y - b.y);
        return v;
    }
    crossVV(a, b) {
        return a.x * b.y - a.y * b.x;
    }
    crossVF(a, s) {
        let v = new B2Vec2(s * a.y, -s * a.x);
        return v;
    }
    computeAABB(aabb, transform, childIndex) {
        let lower = b2MulT2(transform, this.m_vertices.get(0));
        let upper = lower;
        for (let i = 1; i < this.m_count; i++) {
            let v = b2MulT2(transform, this.m_vertices.get(i));
            lower = b2Min(lower, v);
            upper = b2Max(upper, v);
        }
        let r = new B2Vec2(this.m_radius, this.m_radius);
        aabb.lowerBound = subtract(lower, r);
        aabb.upperBound = add(r, upper);
    }
    computeMass(density) {
        let center = new B2Vec2(0.0, 0.0);
        let area = 0.0;
        let I = 0.0;
        let s = new B2Vec2(0.0, 0.0);
        for (let i = 0; i < this.m_count; i++) {
            addEqual(s, this.m_vertices.get(i));
        }
        mulEqual(s, 1.0 / this.m_vertices.count);
        let k_inv3 = 1.0 / float_3;
        for (let i = 0; i < this.m_count; i++) {
            let e1 = subtract(this.m_vertices.get(i), s);
            let e2 = i + 1 < this.m_count ? subtract(this.m_vertices.get(i + 1), s) : subtract(this.m_vertices.get(0), s);
            let D = b2Cross(e1, e2);
            let triangleArea = half * D;
            area += triangleArea;
            addEqual(center, multM(add(e1, e2), k_inv3 * triangleArea));
            let ex1 = e1.x, ey1 = e1.y;
            let ex2 = e2.x, ey2 = e2.y;
            let intx2 = ex1 * ex1 + ex2 * ex1 + ex2 * ex2;
            let inty2 = ey1 * ey1 + ey2 * ey1 + ey2 * ey2;
            I += float_025 * k_inv3 * D * (intx2 + inty2);
        }
        let massData = new B2MassData();
        massData.mass = density * area;
        mulEqual(center, 1.0 / area);
        massData.center = add(center, s);
        massData.I = density * I;
        massData.I += massData.mass * (b2Dot22(massData.center, massData.center) - b2Dot22(center, center));
        return massData;
    }
    vertexCount() {
        return this.m_count;
    }
    vertex(index) {
        return this.m_vertices.get(index);
    }
    validate() {
        for (let i = 0; i < this.m_vertices.count; i++) {
            let i1 = i;
            let i2 = i < this.m_vertices.count - 1 ? i1 + 1 : 0;
            let p = this.m_vertices.get(i1);
            let e = subtract(this.m_vertices.get(i2), p);
            for (let j = 0; j < this.m_count; j++) {
                if (j == i1 || j == i2) {
                    continue;
                }
                let v = subtract(this.m_vertices.get(j), p);
                let c = b2Cross(e, v);
                if (c < 0.0) {
                    return false;
                }
            }
        }
        return true;
    }
    get vertices() {
        return this.m_vertices;
    }
    get count() {
        return this.m_count;
    }
    get m_count() {
        return this.m_vertices.count;
    }
}
export { B2ShapeType };
