/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { b2_maxManifoldPoints } from '../Common/b2Settings';
export class B2ContactFilter {
    shouldCollide(fixtureA, fixtureB) {
        let filterA = fixtureA.filterData;
        let filterB = fixtureB.filterData;
        if (filterA.groupIndex == filterB.groupIndex && filterA.groupIndex != 0) {
            return filterA.groupIndex > 0;
        }
        let collide = (filterA.maskBits & filterB.categoryBits) != 0 && (filterA.categoryBits & filterB.maskBits) != 0;
        return collide;
    }
}
export class B2ContactImpulse {
    constructor() {
        this.count = 0;
        this.normalImpulses = [];
        this.tangentImpulses = [];
        for (let i = 0; i < b2_maxManifoldPoints; i++) {
            this.normalImpulses.push(0);
            this.tangentImpulses.push(0);
        }
    }
}
export class B2DefaultContactListener {
    beginContact(contact) { }
    endContact(contact) { }
    preSolve(contact, oldManifold) { }
    postSolve(contact, impulse) { }
}
export class B2QueryCallbackProxy {
    constructor(callback) {
        this.callback = callback;
    }
    reportFixture(fixture) {
        return this.callback(fixture);
    }
}
export class B2RayCastCallbackProxy {
    constructor(callback) {
        this.callback = callback;
    }
    reportFixture(fixture, point, normal, fraction) {
        return this.callback(fixture, point, normal, fraction);
    }
}
