/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
export class B2Profile {
    constructor() {
        this.step = 0.0;
        this.collide = 0.0;
        this.solve = 0.0;
        this.solveInit = 0.0;
        this.solveVelocity = 0.0;
        this.solvePosition = 0.0;
        this.broadPhase = 0.0;
        this.solveTOI = 0.0;
    }
}
export class B2TimeStep {
    constructor() {
        this.dt = 0.0;
        this.inv_dt = 0.0;
        this.dtRatio = 0.0;
        this.velocityIterations = 0;
        this.positionIterations = 0;
        this.warmStarting = false;
    }
}
export class B2Array {
    constructor(count = null, repeatedValue = null) {
        this.array = [];
        if (count != null && repeatedValue != null) {
            for (let i = 0; i < count; i++) {
                this.array.push(repeatedValue);
            }
        }
    }
    append(newElement) {
        this.array.push(newElement);
    }
    insert(newElement, i) {
        this.array.splice(i, 0, newElement);
    }
    removeAtIndex(index) {
        return this.array.splice(index, 1);
    }
    removeLast() {
        this.array.pop();
    }
    removeAll(keepCapacity = true) {
        this.array.splice(0, this.array.length);
    }
    get(index) {
        return this.array[index];
    }
    set(index, newValue) {
        this.array[index] = newValue;
    }
    clone() {
        let clone = new B2Array();
        for (let e of this.array) {
            clone.array.push(e);
        }
        return clone;
    }
    get count() {
        return this.array.length;
    }
}
export class B2Position {
    constructor(c, a) {
        this.c = c;
        this.a = a;
    }
}
export class B2Velocity {
    constructor(v, w) {
        this.v = v;
        this.w = w;
    }
}
export class B2SolverData {
    constructor() {
        this.step = new B2TimeStep();
        this.positions = new B2Array();
        this.velocities = new B2Array();
    }
}
