/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { B2Vec2, b2MulTT2, b2MulTR2, B2Mat22, B2Transform, b2Vec2_zero, subtract, multM, add, b2Cross, b2Cross21, b2MulT2, b2MulR2, B2Sweep, b2Dot22, addEqual, mulEqual } from '../Common/b2Math';
import { B2FixtureDef, B2Fixture } from './b2Fixture';
import { B2MassData } from '../Collision/Shapes/b2Shape';
import { FlagsW } from './b2World';
import { hex_1, hex_10, hex_2, hex_20, hex_4, hex_40, hex_8 } from '../Common/b2Settings';
var B2BodyType;
(function (B2BodyType) {
    B2BodyType[B2BodyType["staticBody"] = 0] = "staticBody";
    B2BodyType[B2BodyType["kinematicBody"] = 1] = "kinematicBody";
    B2BodyType[B2BodyType["dynamicBody"] = 2] = "dynamicBody";
})(B2BodyType || (B2BodyType = {}));
export class B2BodyDef {
    constructor() {
        this.type = B2BodyType.staticBody;
        this.position = new B2Vec2();
        this.angle = 0.0;
        this.linearVelocity = new B2Vec2();
        this.angularVelocity = 0.0;
        this.linearDamping = 0.0;
        this.angularDamping = 0.0;
        this.allowSleep = true;
        this.awake = true;
        this.fixedRotation = false;
        this.bullet = false;
        this.active = true;
        this.userData = null;
        this.gravityScale = 1.0;
    }
}
export class B2Body {
    createFixture(def) {
        if (this.m_world.isLocked == true) {
            throw new Error('world is locked');
        }
        let fixture = new B2Fixture(this, def);
        if ((this.m_flags & Flags.activeFlag) != 0) {
            let broadPhase = this.m_world.m_contactManager.m_broadPhase;
            fixture.createProxies(broadPhase, this.m_xf);
        }
        fixture.m_next = this.m_fixtureList;
        this.m_fixtureList = fixture;
        this.m_fixtureCount += 1;
        fixture.m_body = this;
        if (fixture.m_density > 0.0) {
            this.resetMassData();
        }
        this.m_world.m_flags |= FlagsW.newFixture;
        return fixture;
    }
    createFixture2(shape, density) {
        let def = new B2FixtureDef();
        def.shape = shape;
        def.density = density;
        return this.createFixture(def);
    }
    destroyFixture(fixture) {
        if (this.m_world.isLocked == true) {
            return;
        }
        let found = false;
        let prev = null;
        let f = this.m_fixtureList;
        while (f != null) {
            if (f === fixture) {
                if (prev != null) {
                    prev.m_next = f.m_next;
                }
                else {
                    this.m_fixtureList = f.m_next;
                }
                found = true;
            }
            prev = f;
            f = f.m_next;
        }
        let edge = this.m_contactList;
        while (edge != null) {
            let c = edge.contact;
            edge = edge.next;
            let fixtureA = c.fixtureA;
            let fixtureB = c.fixtureB;
            if (fixture === fixtureA || fixture === fixtureB) {
                this.m_world.m_contactManager.destroy(c);
            }
        }
        if ((this.m_flags & Flags.activeFlag) != 0) {
            let broadPhase = this.m_world.m_contactManager.m_broadPhase;
            fixture.destroyProxies(broadPhase);
        }
        fixture.destroy();
        fixture.m_next = null;
        this.m_fixtureCount -= 1;
        this.resetMassData();
    }
    setTransform(position, angle) {
        if (this.m_world.isLocked == true) {
            return;
        }
        this.m_xf.q.set(angle);
        this.m_xf.p = position;
        this.m_sweep.c = b2MulT2(this.m_xf, this.m_sweep.localCenter);
        this.m_sweep.a = angle;
        this.m_sweep.c0 = this.m_sweep.c;
        this.m_sweep.a0 = angle;
        let broadPhase = this.m_world.m_contactManager.m_broadPhase;
        let f = this.m_fixtureList;
        while (f != null) {
            f.synchronize(broadPhase, this.m_xf, this.m_xf);
            f = f.m_next;
        }
    }
    get transform() {
        return this.m_xf;
    }
    get position() {
        return this.m_xf.p;
    }
    get angle() {
        return this.m_sweep.a;
    }
    get worldCenter() {
        return this.m_sweep.c;
    }
    get localCenter() {
        return this.m_sweep.localCenter;
    }
    setLinearVelocity(v) {
        if (this.m_type == B2BodyType.staticBody) {
            return;
        }
        if (b2Dot22(v, v) > 0.0) {
            this.setAwake(true);
        }
        this.m_linearVelocity = v;
    }
    get linearVelocity() {
        return this.m_linearVelocity;
    }
    set linearVelocity(newValue) {
        this.setLinearVelocity(newValue);
    }
    setAngularVelocity(omega) {
        if (this.m_type == B2BodyType.staticBody) {
            return;
        }
        if (omega * omega > 0.0) {
            this.setAwake(true);
        }
        this.m_angularVelocity = omega;
    }
    get angularVelocity() {
        return this.m_angularVelocity;
    }
    set angularVelocity(newValue) {
        this.setAngularVelocity(newValue);
    }
    applyForce(force, point, wake) {
        if (this.m_type != B2BodyType.dynamicBody) {
            return;
        }
        if (wake && (this.m_flags & Flags.awakeFlag) == 0) {
            this.setAwake(true);
        }
        if ((this.m_flags & Flags.awakeFlag) != 0) {
            addEqual(this.m_force, force);
            this.m_torque += b2Cross(subtract(point, this.m_sweep.c), force);
        }
    }
    applyForceToCenter(force, wake) {
        if (this.m_type != B2BodyType.dynamicBody) {
            return;
        }
        if (wake && (this.m_flags & Flags.awakeFlag) == 0) {
            this.setAwake(true);
        }
        if ((this.m_flags & Flags.awakeFlag) != 0) {
            addEqual(this.m_force, force);
        }
    }
    applyTorque(torque, wake) {
        if (this.m_type != B2BodyType.dynamicBody) {
            return;
        }
        if (wake && (this.m_flags & Flags.awakeFlag) == 0) {
            this.setAwake(true);
        }
        if ((this.m_flags & Flags.awakeFlag) != 0) {
            this.m_torque += torque;
        }
    }
    applyLinearImpulse(impulse, point, wake) {
        if (this.m_type != B2BodyType.dynamicBody) {
            return;
        }
        if (wake && (this.m_flags & Flags.awakeFlag) == 0) {
            this.setAwake(true);
        }
        if ((this.m_flags & Flags.awakeFlag) != 0) {
            addEqual(this.m_linearVelocity, multM(impulse, this.m_invMass));
            this.m_angularVelocity += this.m_invI * b2Cross(subtract(point, this.m_sweep.c), impulse);
        }
    }
    applyAngularImpulse(impulse, wake) {
        if (this.m_type != B2BodyType.dynamicBody) {
            return;
        }
        if (wake && (this.m_flags & Flags.awakeFlag) == 0) {
            this.setAwake(true);
        }
        if ((this.m_flags & Flags.awakeFlag) != 0) {
            this.m_angularVelocity += this.m_invI * impulse;
        }
    }
    get mass() {
        return this.m_mass;
    }
    get inertia() {
        return this.m_I + this.m_mass * b2Dot22(this.m_sweep.localCenter, this.m_sweep.localCenter);
    }
    get massData() {
        let data = new B2MassData();
        data.mass = this.m_mass;
        data.I = this.m_I + this.m_mass * b2Dot22(this.m_sweep.localCenter, this.m_sweep.localCenter);
        data.center = this.m_sweep.localCenter;
        return data;
    }
    set massData(newValue) {
        this.setMassData(newValue);
    }
    setMassData(massData) {
        if (this.m_world.isLocked == true) {
            return;
        }
        if (this.m_type != B2BodyType.dynamicBody) {
            return;
        }
        this.m_invMass = 0.0;
        this.m_I = 0.0;
        this.m_invI = 0.0;
        this.m_mass = massData.mass;
        if (this.m_mass <= 0.0) {
            this.m_mass = 1.0;
        }
        this.m_invMass = 1.0 / this.m_mass;
        if (massData.I > 0.0 && (this.m_flags & Flags.fixedRotationFlag) == 0) {
            this.m_I = massData.I - this.m_mass * b2Dot22(massData.center, massData.center);
            this.m_invI = 1.0 / this.m_I;
        }
        let oldCenter = this.m_sweep.c;
        this.m_sweep.localCenter = massData.center;
        this.m_sweep.c0 = b2MulT2(this.m_xf, this.m_sweep.localCenter);
        this.m_sweep.c = this.m_sweep.c0;
        addEqual(this.m_linearVelocity, b2Cross21(this.m_angularVelocity, subtract(this.m_sweep.c, oldCenter)));
    }
    resetMassData() {
        this.m_mass = 0.0;
        this.m_invMass = 0.0;
        this.m_I = 0.0;
        this.m_invI = 0.0;
        this.m_sweep.localCenter.setZero();
        if (this.m_type == B2BodyType.staticBody || this.m_type == B2BodyType.kinematicBody) {
            this.m_sweep.c0 = this.m_xf.p;
            this.m_sweep.c = this.m_xf.p;
            this.m_sweep.a0 = this.m_sweep.a;
            return;
        }
        let localCenter = b2Vec2_zero;
        let f = this.m_fixtureList;
        while (f != null) {
            if (f.m_density == 0.0) {
                f = f.m_next;
                continue;
            }
            let massData = f.massData;
            this.m_mass += massData.mass;
            addEqual(localCenter, multM(massData.center, massData.mass));
            this.m_I += massData.I;
            f = f.m_next;
        }
        if (this.m_mass > 0.0) {
            this.m_invMass = 1.0 / this.m_mass;
            mulEqual(localCenter, this.m_invMass);
        }
        else {
            this.m_mass = 1.0;
            this.m_invMass = 1.0;
        }
        if (this.m_I > 0.0 && (this.m_flags & Flags.fixedRotationFlag) == 0) {
            this.m_I -= this.m_mass * b2Dot22(localCenter, localCenter);
            this.m_invI = 1.0 / this.m_I;
        }
        else {
            this.m_I = 0.0;
            this.m_invI = 0.0;
        }
        let oldCenter = this.m_sweep.c;
        this.m_sweep.localCenter = localCenter;
        this.m_sweep.c0 = b2MulT2(this.m_xf, this.m_sweep.localCenter);
        this.m_sweep.c = this.m_sweep.c0;
        addEqual(this.m_linearVelocity, b2Cross21(this.m_angularVelocity, subtract(this.m_sweep.c, oldCenter)));
    }
    getWorldPoint(localPoint) {
        return b2MulT2(this.m_xf, localPoint);
    }
    getWorldVector(localVector) {
        return b2MulR2(this.m_xf.q, localVector);
    }
    getLocalPoint(worldPoint) {
        return b2MulTT2(this.m_xf, worldPoint);
    }
    getLocalVector(worldVector) {
        return b2MulTR2(this.m_xf.q, worldVector);
    }
    getLinearVelocityFromWorldPoint(worldPoint) {
        return add(this.m_linearVelocity, b2Cross21(this.m_angularVelocity, subtract(worldPoint, this.m_sweep.c)));
    }
    getLinearVelocityFromLocalPoint(localPoint) {
        return this.getLinearVelocityFromWorldPoint(this.getWorldPoint(localPoint));
    }
    get linearDamping() {
        return this.m_linearDamping;
    }
    set linearDamping(newValue) {
        this.setLinearDamping(newValue);
    }
    setLinearDamping(linearDamping) {
        this.m_linearDamping = linearDamping;
    }
    get angularDamping() {
        return this.m_gravityScale;
    }
    set angularDamping(newValue) {
        this.setAngularDamping(newValue);
    }
    setAngularDamping(angularDamping) {
        this.m_angularDamping = angularDamping;
    }
    get gravityScale() {
        return this.m_gravityScale;
    }
    set gravityScale(newValue) {
        this.setGravityScale(newValue);
    }
    setGravityScale(scale) {
        this.m_gravityScale = scale;
    }
    setType(type) {
        if (this.m_world.isLocked == true) {
            return;
        }
        if (this.m_type == type) {
            return;
        }
        this.m_type = type;
        this.resetMassData();
        if (this.m_type == B2BodyType.staticBody) {
            this.m_linearVelocity.setZero();
            this.m_angularVelocity = 0.0;
            this.m_sweep.a0 = this.m_sweep.a;
            this.m_sweep.c0 = this.m_sweep.c;
            this.synchronizeFixtures();
        }
        this.setAwake(true);
        this.m_force.setZero();
        this.m_torque = 0.0;
        let ce = this.m_contactList;
        while (ce != null) {
            let ce0 = ce;
            ce = ce.next;
            this.m_world.m_contactManager.destroy(ce0.contact);
        }
        this.m_contactList = null;
        let broadPhase = this.m_world.m_contactManager.m_broadPhase;
        let f = this.m_fixtureList;
        while (f != null) {
            let proxyCount = f.m_proxyCount;
            for (let i = 0; i < proxyCount; i++) {
                broadPhase.touchProxy(f.m_proxies[i].proxyId);
            }
            f = f.m_next;
        }
    }
    get type_Body() {
        return this.m_type;
    }
    set type_Body(newValue) {
        this.setType(newValue);
    }
    setBullet(flag) {
        if (flag) {
            this.m_flags |= Flags.bulletFlag;
        }
        else {
            this.m_flags &= ~Flags.bulletFlag;
        }
    }
    get isBullet() {
        return (this.m_flags & Flags.bulletFlag) == Flags.bulletFlag;
    }
    set isBullet(newValue) {
        this.setBullet(newValue);
    }
    setSleepingAllowed(flag) {
        if (flag) {
            this.m_flags |= Flags.autoSleepFlag;
        }
        else {
            this.m_flags &= ~Flags.autoSleepFlag;
            this.setAwake(true);
        }
    }
    get isSleepingAllowed() {
        return (this.m_flags & Flags.autoSleepFlag) == Flags.autoSleepFlag;
    }
    set isSleepingAllowed(newValue) {
        this.setSleepingAllowed(newValue);
    }
    setAwake(flag) {
        if (flag) {
            if ((this.m_flags & Flags.awakeFlag) == 0) {
                this.m_flags |= Flags.awakeFlag;
                this.m_sleepTime = 0.0;
            }
        }
        else {
            this.m_flags &= ~Flags.awakeFlag;
            this.m_sleepTime = 0.0;
            this.m_linearVelocity.setZero();
            this.m_angularVelocity = 0.0;
            this.m_force.setZero();
            this.m_torque = 0.0;
        }
    }
    get isAwake() {
        return (this.m_flags & Flags.awakeFlag) == Flags.awakeFlag;
    }
    setActive(flag) {
        if (flag == this.isActive) {
            return;
        }
        if (flag) {
            this.m_flags |= Flags.activeFlag;
            let broadPhase = this.m_world.m_contactManager.m_broadPhase;
            let f = this.m_fixtureList;
            while (f != null) {
                f.createProxies(broadPhase, this.m_xf);
                f = f.m_next;
            }
        }
        else {
            this.m_flags &= ~Flags.activeFlag;
            let broadPhase = this.m_world.m_contactManager.m_broadPhase;
            let f = this.m_fixtureList;
            while (f != null) {
                f.destroyProxies(broadPhase);
                if (f.m_next != null) {
                    f = f.m_next;
                }
                else {
                    break;
                }
            }
            let ce = this.m_contactList;
            while (ce != null) {
                let ce0 = ce;
                ce = ce.next;
                this.m_world.m_contactManager.destroy(ce0.contact);
            }
            this.m_contactList = null;
        }
    }
    get isActive() {
        return (this.m_flags & Flags.activeFlag) == Flags.activeFlag;
    }
    setFixedRotation(flag) {
        let status = (this.m_flags & Flags.fixedRotationFlag) == Flags.fixedRotationFlag;
        if (status == flag) {
            return;
        }
        if (flag) {
            this.m_flags |= Flags.fixedRotationFlag;
        }
        else {
            this.m_flags &= ~Flags.fixedRotationFlag;
        }
        this.m_angularVelocity = 0.0;
        this.resetMassData();
    }
    isFixedRotation() {
        return (this.m_flags & Flags.fixedRotationFlag) == Flags.fixedRotationFlag;
    }
    getFixtureList() {
        return this.m_fixtureList;
    }
    getJointList() {
        return this.m_jointList;
    }
    getContactList() {
        return this.m_contactList;
    }
    getNext() {
        return this.m_next;
    }
    get userData() {
        return this.m_userData;
    }
    set userData(newValue) {
        this.setUserData(newValue);
    }
    setUserData(data) {
        this.m_userData = data;
    }
    get world() {
        return this.m_world;
    }
    constructor(def, world) {
        this.m_flags = 0;
        this.m_islandIndex = 0;
        this.m_prev = null;
        this.m_next = null;
        this.m_fixtureList = null;
        this.m_fixtureCount = 0;
        this.m_jointList = null;
        this.m_contactList = null;
        this.m_flags = 0;
        if (def.bullet) {
            this.m_flags |= Flags.bulletFlag;
        }
        if (def.fixedRotation) {
            this.m_flags |= Flags.fixedRotationFlag;
        }
        if (def.allowSleep) {
            this.m_flags |= Flags.autoSleepFlag;
        }
        if (def.awake) {
            this.m_flags |= Flags.awakeFlag;
        }
        if (def.active) {
            this.m_flags |= Flags.activeFlag;
        }
        this.m_world = world;
        this.m_xf = new B2Transform();
        this.m_xf.p = def.position;
        this.m_xf.q.set(def.angle);
        this.m_sweep = new B2Sweep();
        this.m_sweep.localCenter = new B2Vec2(0.0, 0.0);
        this.m_sweep.c0 = this.m_xf.p;
        this.m_sweep.c = this.m_xf.p;
        this.m_sweep.a0 = def.angle;
        this.m_sweep.a = def.angle;
        this.m_sweep.alpha0 = 0.0;
        this.m_prev = null;
        this.m_next = null;
        this.m_linearVelocity = def.linearVelocity;
        this.m_angularVelocity = def.angularVelocity;
        this.m_linearDamping = def.linearDamping;
        this.m_angularDamping = def.angularDamping;
        this.m_gravityScale = def.gravityScale;
        this.m_force = new B2Vec2(0.0, 0.0);
        this.m_torque = 0.0;
        this.m_sleepTime = 0.0;
        this.m_type = def.type;
        if (this.m_type == B2BodyType.dynamicBody) {
            this.m_mass = 1.0;
            this.m_invMass = 1.0;
        }
        else {
            this.m_mass = 0.0;
            this.m_invMass = 0.0;
        }
        this.m_I = 0.0;
        this.m_invI = 0.0;
        this.m_userData = def.userData;
        this.m_fixtureList = null;
        this.m_fixtureCount = 0;
    }
    synchronizeFixtures() {
        let xf1 = new B2Transform();
        xf1.q.set(this.m_sweep.a0);
        xf1.p = subtract(this.m_sweep.c0, b2MulR2(xf1.q, this.m_sweep.localCenter));
        let broadPhase = this.m_world.m_contactManager.m_broadPhase;
        let f = this.m_fixtureList;
        while (f != null) {
            f.synchronize(broadPhase, xf1, this.m_xf);
            f = f.m_next;
        }
    }
    synchronizeTransform() {
        this.m_xf.q.set(this.m_sweep.a);
        this.m_xf.p = subtract(this.m_sweep.c, b2MulR2(this.m_xf.q, this.m_sweep.localCenter));
        let tVec = this.m_sweep.localCenter;
        let tMat = new B2Mat22();
        tMat.set_1(this.m_sweep.a);
        this.m_xf.p.y = this.m_sweep.c.y - (tMat.ex.y * tVec.x + tMat.ey.y * tVec.y);
    }
    shouldCollide(other) {
        if (this.m_type != B2BodyType.dynamicBody && other.m_type != B2BodyType.dynamicBody) {
            return false;
        }
        let jn = this.m_jointList;
        while (jn != null) {
            if (jn.other === other) {
                if (jn.joint.m_collideConnected == false) {
                    return false;
                }
            }
            jn = jn.next;
        }
        return true;
    }
    advance(alpha) {
        this.m_sweep.advance(alpha);
        this.m_sweep.c = this.m_sweep.c0;
        this.m_sweep.a = this.m_sweep.a0;
        this.m_xf.q.set(this.m_sweep.a);
        this.m_xf.p = subtract(this.m_sweep.c, b2MulR2(this.m_xf.q, this.m_sweep.localCenter));
    }
}
export class Flags {
}
Flags.islandFlag = hex_1;
Flags.awakeFlag = hex_2;
Flags.autoSleepFlag = hex_4;
Flags.bulletFlag = hex_8;
Flags.fixedRotationFlag = hex_10;
Flags.activeFlag = hex_20;
Flags.toiFlag = hex_40;
export { B2BodyType };
