/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { B2ShapeType } from '../../Collision/Shapes/b2Shape';
import { B2WorldManifold, B2Manifold, b2TestOverlap } from '../../Collision/b2Collision';
import { b2CollidePolygons } from '../../Collision/b2CollidePolygon';
import { b2CollideCircles, b2CollidePolygonAndCircle } from '../../Collision/b2CollideCircles';
import { b2CollideEdgeAndCircle, b2CollideEdgeAndPolygon } from '../../Collision/b2CollideEdge';
import { hex_10, hex_2, hex_20, hex_4, hex_8 } from '../../Common/b2Settings';
function b2MixFriction(friction1, friction2) {
    return Math.sqrt(friction1 * friction2);
}
function b2MixRestitution(restitution1, restitution2) {
    return restitution1 > restitution2 ? restitution1 : restitution2;
}
export class B2ContactRegister {
    constructor() {
        this.createFcn = null;
        this.destroyFcn = null;
        this.primary = false;
    }
}
export class B2ContactRegisters {
    constructor(rows, columns) {
        this.grid = [];
        this.rows = rows;
        this.columns = columns;
        for (let i = 0; i < rows * columns; i++) {
            this.grid.push(new B2ContactRegister());
        }
    }
    getItem(row, column) {
        return this.grid[row * this.columns + column];
    }
    setItem(row, column, value) {
        this.grid[row * this.columns + column] = value;
    }
}
export class B2ContactEdge {
    constructor(contact) {
        this.other = null;
        this.prev = null;
        this.next = null;
        this.contact = contact;
    }
}
export class B2Contact {
    get manifold() {
        return this.m_manifold;
    }
    get worldManifold() {
        let bodyA = this.m_fixtureA.body;
        let bodyB = this.m_fixtureB.body;
        let shapeA = this.m_fixtureA.shape;
        let shapeB = this.m_fixtureB.shape;
        let worldManifold = new B2WorldManifold();
        worldManifold.initialize(this.m_manifold, bodyA.transform, shapeA.m_radius, bodyB.transform, shapeB.m_radius);
        return worldManifold;
    }
    get isTouching() {
        return (this.m_flags & Flags_C.touchingFlag) == Flags_C.touchingFlag;
    }
    setEnabled(flag) {
        if (flag) {
            this.m_flags |= Flags_C.enabledFlag;
        }
        else {
            this.m_flags &= ~Flags_C.enabledFlag;
        }
    }
    get isEnabled() {
        return (this.m_flags & Flags_C.enabledFlag) == Flags_C.enabledFlag;
    }
    getNext() {
        return this.m_next;
    }
    get fixtureA() {
        return this.m_fixtureA;
    }
    get childIndexA() {
        return this.m_indexA;
    }
    get fixtureB() {
        return this.m_fixtureB;
    }
    get childIndexB() {
        return this.m_indexB;
    }
    get friction() {
        return this.m_friction;
    }
    set setFriction(friction) {
        this.m_friction = friction;
    }
    resetFriction() {
        this.m_friction = b2MixFriction(this.m_fixtureA.m_friction, this.m_fixtureB.m_friction);
    }
    get restitution() {
        return this.m_restitution;
    }
    set setRestitution(restitution) {
        this.m_restitution = restitution;
    }
    resetRestitution() {
        this.m_restitution = b2MixRestitution(this.m_fixtureA.m_restitution, this.m_fixtureB.m_restitution);
    }
    setTangentSpeed(speed) {
        this.m_tangentSpeed = speed;
    }
    get tangentSpeed() {
        return this.m_tangentSpeed;
    }
    evaluate(manifold, xfA, xfB) {
        throw new Error('must override');
    }
    flagForFiltering() {
        throw new Error('must override');
    }
    static addType(createFcn, destroyFcn, type1, type2) {
        StaticVars.s_registers.getItem(type1, type2).createFcn = createFcn;
        StaticVars.s_registers.getItem(type1, type2).destroyFcn = destroyFcn;
        StaticVars.s_registers.getItem(type1, type2).primary = true;
        if (type1 != type2) {
            StaticVars.s_registers.getItem(type2, type1).createFcn = createFcn;
            StaticVars.s_registers.getItem(type2, type1).destroyFcn = destroyFcn;
            StaticVars.s_registers.getItem(type2, type1).primary = false;
        }
    }
    static initializeRegisters() {
        B2Contact.addType(B2PolygonContact.create, B2PolygonContact.destroy, B2ShapeType.polygon, B2ShapeType.polygon);
    }
    static create(fixtureA, indexA, fixtureB, indexB) {
        if (StaticVars.s_initialized == false) {
            B2Contact.initializeRegisters();
            StaticVars.s_initialized = true;
        }
        let type1 = fixtureA.fixtureType;
        let type2 = fixtureB.fixtureType;
        let createFcn = StaticVars.s_registers.getItem(type1, type2).createFcn;
        if (createFcn != null) {
            if (StaticVars.s_registers.getItem(type1, type2).primary) {
                return createFcn(fixtureA, indexA, fixtureB, indexB);
            }
            else {
                return createFcn(fixtureB, indexB, fixtureA, indexA);
            }
        }
        else {
            return null;
        }
    }
    static destroy(contact) {
        let fixtureA = contact.m_fixtureA;
        let fixtureB = contact.m_fixtureB;
        if (contact.m_manifold.pointCount > 0 && (fixtureA === null || fixtureA === void 0 ? void 0 : fixtureA.isSensor) == false && (fixtureB === null || fixtureB === void 0 ? void 0 : fixtureB.isSensor) == false) {
            fixtureA === null || fixtureA === void 0 ? void 0 : fixtureA.body.setAwake(true);
            fixtureB === null || fixtureB === void 0 ? void 0 : fixtureB.body.setAwake(true);
        }
        let typeA = fixtureA === null || fixtureA === void 0 ? void 0 : fixtureA.fixtureType;
        let typeB = fixtureB === null || fixtureB === void 0 ? void 0 : fixtureB.fixtureType;
        let destroyFcn = StaticVars.s_registers.getItem(typeA, typeB).destroyFcn;
        destroyFcn(contact);
    }
    constructor(fixtureA, indexA, fixtureB, indexB) {
        this.m_flags = 0;
        this.m_prev = null;
        this.m_next = null;
        this.m_indexA = 0;
        this.m_indexB = 0;
        this.m_manifold = new B2Manifold();
        this.m_toiCount = 0;
        this.m_toi = 0;
        this.m_friction = 0;
        this.m_restitution = 0;
        this.m_tangentSpeed = 0;
        this.m_flags = Flags_C.enabledFlag;
        this.m_fixtureA = fixtureA;
        this.m_fixtureB = fixtureB;
        this.m_indexA = indexA;
        this.m_indexB = indexB;
        this.m_manifold.points.splice(0, this.m_manifold.points.length);
        this.m_prev = null;
        this.m_next = null;
        this.m_nodeA = new B2ContactEdge(this);
        this.m_nodeA.prev = null;
        this.m_nodeA.next = null;
        this.m_nodeA.other = null;
        this.m_nodeB = new B2ContactEdge(this);
        this.m_nodeB.prev = null;
        this.m_nodeB.next = null;
        this.m_nodeB.other = null;
        this.m_toiCount = 0;
        this.m_friction = b2MixFriction(this.m_fixtureA.m_friction, this.m_fixtureB.m_friction);
        this.m_restitution = b2MixRestitution(this.m_fixtureA.m_restitution, this.m_fixtureB.m_restitution);
        this.m_tangentSpeed = 0.0;
    }
    update(listener) {
        let oldManifold = new B2Manifold(this.m_manifold);
        this.m_flags |= Flags_C.enabledFlag;
        let touching = false;
        let wasTouching = (this.m_flags & Flags_C.touchingFlag) == Flags_C.touchingFlag;
        let sensorA = this.m_fixtureA.isSensor;
        let sensorB = this.m_fixtureB.isSensor;
        let sensor = sensorA || sensorB;
        let bodyA = this.m_fixtureA.body;
        let bodyB = this.m_fixtureB.body;
        let xfA = bodyA.transform;
        let xfB = bodyB.transform;
        if (sensor) {
            let shapeA = this.m_fixtureA.shape;
            let shapeB = this.m_fixtureB.shape;
            touching = b2TestOverlap(shapeA, this.m_indexA, shapeB, this.m_indexB, xfA, xfB);
            this.m_manifold.points.splice(0, this.m_manifold.points.length);
        }
        else {
            this.evaluate(this.m_manifold, xfA, xfB);
            touching = this.m_manifold.pointCount > 0;
            for (let i = 0; i < this.m_manifold.pointCount; i++) {
                let mp2 = this.m_manifold.points[i];
                mp2.normalImpulse = 0.0;
                mp2.tangentImpulse = 0.0;
                let id2 = mp2.id;
                for (let j = 0; j < oldManifold.pointCount; j++) {
                    let mp1 = oldManifold.points[j];
                    if (equalEqual(mp1.id, id2)) {
                        mp2.normalImpulse = mp1.normalImpulse;
                        mp2.tangentImpulse = mp1.tangentImpulse;
                        break;
                    }
                }
            }
            if (touching != wasTouching) {
                bodyA.setAwake(true);
                bodyB.setAwake(true);
            }
        }
    }
}
export class StaticVars {
}
StaticVars.s_registers = new B2ContactRegisters(B2ShapeType.typeCount, B2ShapeType.typeCount);
StaticVars.s_initialized = false;
var Flags_C;
(function (Flags_C) {
    Flags_C[Flags_C["islandFlag"] = 1] = "islandFlag";
    Flags_C[Flags_C["touchingFlag"] = hex_2] = "touchingFlag";
    Flags_C[Flags_C["enabledFlag"] = hex_4] = "enabledFlag";
    Flags_C[Flags_C["filterFlag"] = hex_8] = "filterFlag";
    Flags_C[Flags_C["bulletHitFlag"] = hex_10] = "bulletHitFlag";
    Flags_C[Flags_C["toiFlag"] = hex_20] = "toiFlag";
})(Flags_C || (Flags_C = {}));
export class B2PolygonContact extends B2Contact {
    static create(fixtureA, indexA, fixtureB, indexB) {
        return new B2PolygonContact(fixtureA, fixtureB);
    }
    static destroy(contact) { }
    constructor(fixtureA, fixtureB) {
        super(fixtureA, 0, fixtureB, 0);
    }
    evaluate(manifold, xfA, xfB) {
        b2CollidePolygons(manifold, this.m_fixtureA.shape, xfA, this.m_fixtureB.shape, xfB);
    }
}
export class B2CircleContact extends B2Contact {
    static create(fixtureA, indexA, fixtureB, indexB) {
        return new B2CircleContact(fixtureA, fixtureB);
    }
    static destroy(contact) { }
    constructor(fixtureA, fixtureB) {
        super(fixtureA, 0, fixtureB, 0);
    }
    evaluate(manifold, xfA, xfB) {
        b2CollideCircles(manifold, this.m_fixtureA.shape, xfA, this.m_fixtureB.shape, xfB);
    }
}
export class B2EdgeAndCircleContact extends B2Contact {
    static create(fixtureA, indexA, fixtureB, indexB) {
        return new B2EdgeAndCircleContact(fixtureA, fixtureB);
    }
    static destroy(contact) { }
    constructor(fixtureA, fixtureB) {
        super(fixtureA, 0, fixtureB, 0);
    }
    evaluate(manifold, xfA, xfB) {
        b2CollideEdgeAndCircle(manifold, this.m_fixtureA.shape, xfA, this.m_fixtureB.shape, xfB);
    }
}
export class B2EdgeAndPolygonContact extends B2Contact {
    static create(fixtureA, indexA, fixtureB, indexB) {
        return new B2EdgeAndPolygonContact(fixtureA, fixtureB);
    }
    static destroy(contact) { }
    constructor(fixtureA, fixtureB) {
        super(fixtureA, 0, fixtureB, 0);
    }
    evaluate(manifold, xfA, xfB) {
        b2CollideEdgeAndPolygon(manifold, this.m_fixtureA.shape, xfA, this.m_fixtureB.shape, xfB);
    }
}
export class B2PolygonAndCircleContact extends B2Contact {
    static create(fixtureA, indexA, fixtureB, indexB) {
        return new B2PolygonAndCircleContact(fixtureA, fixtureB);
    }
    static destroy(contact) { }
    constructor(fixtureA, fixtureB) {
        super(fixtureA, 0, fixtureB, 0);
    }
    evaluate(manifold, xfA, xfB) {
        b2CollidePolygonAndCircle(manifold, this.m_fixtureA.shape, xfA, this.m_fixtureB.shape, xfB);
    }
}
export function equalEqual(lhs, rhs) {
    return lhs.indexA == rhs.indexA && lhs.indexB == rhs.indexB && lhs.typeA == rhs.typeA && lhs.typeB == rhs.typeB;
}
export { Flags_C };
