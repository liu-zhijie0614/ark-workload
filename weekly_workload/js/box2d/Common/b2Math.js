/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { int_4, b2_epsilon, b2_pi, float_2, float_8, int_16 } from './b2Settings';
function b2IsValid(x) {
    return Number.isNaN(x) == false && Number.isFinite(x) == false;
}
function b2Sqrt(x) {
    return Math.sqrt(x);
}
function b2Atan2(y, x) {
    return Math.atan2(y, x);
}
export class B2Vec2 {
    constructor(x_ = 0, y_ = 0) {
        if (x_ != null && y_ != null) {
            this.x = x_;
            this.y = y_;
        }
        else {
            this.x = 0;
            this.y = 0;
        }
    }
    setZero() {
        this.x = 0.0;
        this.y = 0.0;
    }
    set(x_, y_) {
        this.x = x_;
        this.y = y_;
    }
    getSubscript(index) {
        if (index == 0) {
            return this.x;
        }
        else {
            return this.y;
        }
    }
    setSubscript(index, newValue) {
        if (index == 0) {
            this.x = newValue;
        }
        else {
            this.y = newValue;
        }
    }
    length() {
        return Math.sqrt(this.x * this.x + this.y * this.y);
    }
    lengthSquared() {
        return this.x * this.x + this.y * this.y;
    }
    normalize() {
        let length = this.length();
        if (length < b2_epsilon) {
            return 0.0;
        }
        let invLength = 1.0 / length;
        this.x *= invLength;
        this.y *= invLength;
        return length;
    }
    isValid() {
        return b2IsValid(this.x) && b2IsValid(this.y);
    }
    skew() {
        return new B2Vec2(-this.y, this.x);
    }
}
export function minus(v) {
    let _v = new B2Vec2(-v.x, -v.y);
    return _v;
}
export function addEqual(a, b) {
    a.x += b.x;
    a.y += b.y;
}
export function subtractEqual(a, b) {
    a.x -= b.x;
    a.y -= b.y;
}
export function mulEqual(a, b) {
    a.x *= b;
    a.y *= b;
}
export class B2Vec3 {
    constructor(x_ = 0, y_ = 0, z_ = 0) {
        if (x_ != null && y_ != null && z_ != null) {
            this.x = x_;
            this.y = y_;
            this.z = z_;
        }
        else {
            this.x = 0.0;
            this.y = 0.0;
            this.z = 0.0;
        }
    }
    setZero() {
        this.x = 0.0;
        this.y = 0.0;
    }
    set(x_, y_, z_) {
        this.x = x_;
        this.y = y_;
        this.z = z_;
    }
}
export function minus3(v) {
    let _v = new B2Vec3(-v.x, -v.y, -v.z);
    return _v;
}
export function addEqual3(a, b) {
    a.x += b.x;
    a.y += b.y;
    a.z += b.z;
}
export function mulMEqual3(a, b) {
    a.x *= b;
    a.y *= b;
    a.z *= b;
}
export class B2Mat22 {
    set_1(angle) {
        let c = Math.cos(angle);
        let s = Math.sin(angle);
        this.ex.set(c, s);
        this.ey.set(-s, c);
    }
    constructor(c1 = null, c2 = null) {
        if (c1 != null && c2 != null) {
            this.ex = c1;
            this.ey = c2;
        }
        else {
            this.ex = new B2Vec2(0.0, 0.0);
            this.ey = new B2Vec2(0.0, 0.0);
        }
    }
    set(c1, c2) {
        this.ex = c1;
        this.ey = c2;
    }
    setIdentity() {
        this.ex.x = 1.0;
        this.ey.x = 0.0;
        this.ex.y = 0.0;
        this.ey.y = 1.0;
    }
    setZero() {
        this.ex.x = 0.0;
        this.ey.x = 0.0;
        this.ex.y = 0.0;
        this.ey.y = 0.0;
    }
    getInverse() {
        let a = this.ex.x, b = this.ey.x, c = this.ex.y, d = this.ey.y;
        let B = new B2Mat22();
        let det = a * d - b * c;
        if (det != 0.0) {
            det = 1.0 / det;
        }
        B.ex.x = det * d;
        B.ey.x = -det * b;
        B.ex.y = -det * c;
        B.ey.y = det * a;
        return B;
    }
    solve(b) {
        let a11 = this.ex.x, a12 = this.ey.x, a21 = this.ex.y, a22 = this.ey.y;
        let det = a11 * a22 - a12 * a21;
        if (det != 0.0) {
            det = 1.0 / det;
        }
        let x = new B2Vec2();
        x.x = det * (a22 * b.x - a12 * b.y);
        x.y = det * (a11 * b.y - a21 * b.x);
        return x;
    }
}
export class B2Mat33 {
    constructor(c1 = null, c2 = null, c3 = null) {
        if (c1 != null && c2 != null && c3 != null) {
            this.ex = c1;
            this.ey = c2;
            this.ez = c3;
        }
        else {
            this.ex = new B2Vec3();
            this.ey = new B2Vec3();
            this.ez = new B2Vec3();
        }
    }
    setZero() {
        this.ex.setZero();
        this.ey.setZero();
        this.ez.setZero();
    }
    solve33(b) {
        let det = b2Dot33(this.ex, b2Cross33(this.ey, this.ez));
        if (det != 0.0) {
            det = 1.0 / det;
        }
        let x = new B2Vec3();
        x.x = det * b2Dot33(b, b2Cross33(this.ey, this.ez));
        x.y = det * b2Dot33(this.ex, b2Cross33(b, this.ez));
        x.z = det * b2Dot33(this.ex, b2Cross33(this.ey, b));
        return x;
    }
    solve22(b) {
        let a11 = this.ex.x, a12 = this.ey.x, a21 = this.ex.y, a22 = this.ey.y;
        let det = a11 * a22 - a12 * a21;
        if (det != 0.0) {
            det = 1.0 / det;
        }
        let x = new B2Vec2();
        x.x = det * (a22 * b.x - a12 * b.y);
        x.y = det * (a11 * b.y - a21 * b.x);
        return x;
    }
    getInverse22() {
        let a = this.ex.x, b = this.ey.x, c = this.ex.y, d = this.ey.y;
        let det = a * d - b * c;
        if (det != 0.0) {
            det = 1.0 / det;
        }
        let M = new B2Mat33();
        M.ex.x = det * d;
        M.ey.x = -det * b;
        M.ex.z = 0.0;
        M.ex.y = -det * c;
        M.ey.y = det * a;
        M.ey.z = 0.0;
        M.ez.x = 0.0;
        M.ez.y = 0.0;
        M.ez.z = 0.0;
        return M;
    }
    getSymInverse33() {
        let det = b2Dot33(this.ex, b2Cross33(this.ey, this.ez));
        if (det != 0.0) {
            det = 1.0 / det;
        }
        let a11 = this.ex.x, a12 = this.ey.x, a13 = this.ez.x;
        let a22 = this.ey.y, a23 = this.ez.y;
        let a33 = this.ez.z;
        let M = new B2Mat33();
        M.ex.x = det * (a22 * a33 - a23 * a23);
        M.ex.y = det * (a13 * a23 - a12 * a33);
        M.ex.z = det * (a12 * a23 - a13 * a22);
        M.ey.x = M.ex.y;
        M.ey.y = det * (a11 * a33 - a13 * a13);
        M.ey.z = det * (a13 * a12 - a11 * a23);
        M.ez.x = M.ex.z;
        M.ez.y = M.ey.z;
        M.ez.z = det * (a11 * a22 - a12 * a12);
        return M;
    }
}
export class B2Rot {
    constructor(angle = null) {
        if (angle != null) {
            this.s = Math.sin(angle);
            this.c = Math.cos(angle);
        }
        else {
            this.s = 0.0;
            this.c = 0.0;
        }
    }
    set(angle) {
        this.s = Math.sin(angle);
        this.c = Math.cos(angle);
    }
    setIdentity() {
        this.s = 0.0;
        this.c = 1.0;
    }
    angle() {
        return b2Atan2(this.s, this.c);
    }
    xAxis() {
        return new B2Vec2(this.c, this.s);
    }
    yAxis() {
        return new B2Vec2(-this.s, this.c);
    }
}
export class B2Transform {
    constructor(position = null, rotation = null) {
        if (position != null && rotation != null) {
            this.p = position;
            this.q = rotation;
        }
        else {
            this.p = new B2Vec2();
            this.q = new B2Rot();
        }
    }
    setIdentity() {
        this.p.setZero();
        this.q.setIdentity();
    }
    set(position, angle) {
        this.p = position;
        this.q.set(angle);
    }
}
export class B2Sweep {
    get c0() {
        return this.m_c0;
    }
    set c0(value) {
        this.m_c0 = value;
    }
    constructor() {
        this.localCenter = new B2Vec2();
        this.m_c0 = new B2Vec2();
        this.c = new B2Vec2();
        this.a0 = 0;
        this.a = 0;
        this.alpha0 = 0;
    }
    getTransform(beta) {
        let xf = new B2Transform();
        xf.p = add(multM(this.c0, 1.0 - beta), multM(this.c, beta));
        let angle = (1.0 - beta) * this.a0 + beta * this.a;
        xf.q.set(angle);
        subtractEqual(xf.p, b2MulR2(xf.q, this.localCenter));
        return xf;
    }
    advance(alpha) {
        let beta = (alpha - this.alpha0) / (1.0 - this.alpha0);
        addEqual(this.c0, multM(subtract(this.c, this.c0), beta));
        this.a0 += beta * (this.a - this.a0);
        this.alpha0 = alpha;
    }
    normalize() {
        let twoPi = float_2 * b2_pi;
        let d = twoPi * Math.floor(this.a0 / twoPi);
        this.a0 -= d;
        this.a -= d;
    }
}
let b2Vec2_zero = new B2Vec2(0.0, 0.0);
function b2Dot22(a, b) {
    return a.x * b.x + a.y * b.y;
}
function b2Cross(a, b) {
    return a.x * b.y - a.y * b.x;
}
function b2Cross12(a, s) {
    return new B2Vec2(s * a.y, -s * a.x);
}
function b2Cross21(s, a) {
    return new B2Vec2(-s * a.y, s * a.x);
}
function b2Mul22(A, v) {
    return new B2Vec2(b2Dot22(v, A.ex), b2Dot22(v, A.ey));
}
function b2MulTM2(A, v) {
    return new B2Vec2(b2Dot22(v, A.ex), b2Dot22(v, A.ey));
}
function add(a, b) {
    return new B2Vec2(a.x + b.x, a.y + b.y);
}
function subtract(a, b) {
    return new B2Vec2(a.x - b.x, a.y - b.y);
}
function multM(a, b) {
    return new B2Vec2(a.x * b, a.y * b);
}
function b2Distance(a, b) {
    let c = new B2Vec2(a.x - b.x, a.y - b.y);
    return c.length();
}
function b2DistanceSquared(a, b) {
    let c = new B2Vec2(a.x - b.x, a.y - b.y);
    return b2Dot22(c, c);
}
function multM3(s, a) {
    return new B2Vec3(s * a.x, s * a.y, s * a.z);
}
function add3(a, b) {
    return new B2Vec3(a.x + b.x, a.y + b.y, a.z + b.z);
}
function subtract3(a, b) {
    return new B2Vec3(a.x - b.x, a.y - b.y, a.z - b.z);
}
function b2Dot33(a, b) {
    return a.x * b.x + a.y * b.y + a.z * b.z;
}
function b2Cross33(a, b) {
    return new B2Vec3(a.y * b.z - a.z * b.y, a.z * b.x - a.x * b.z, a.x * b.y - a.y * b.x);
}
function b2Mul(A, B) {
    return new B2Mat22(b2Mul22(A, B.ex), b2Mul22(A, B.ey));
}
function b2MulTMM(A, B) {
    let c1 = new B2Vec2(b2Dot22(A.ex, B.ex), b2Dot22(A.ey, B.ex));
    let c2 = new B2Vec2(b2Dot22(A.ex, B.ey), b2Dot22(A.ey, B.ey));
    return new B2Mat22(c1, c2);
}
function b2Mul33(A, v) {
    return add3(add3(multM3(v.x, A.ex), multM3(v.y, A.ey)), multM3(v.z, A.ez));
}
function b2Mul32(A, v) {
    return new B2Vec2(A.ex.x * v.x + A.ey.x * v.y, A.ex.y * v.x + A.ey.y * v.y);
}
function b2MulRR(q, r) {
    let qr = new B2Rot();
    qr.s = q.s * r.c + q.c * r.s;
    qr.c = q.c * r.c - q.s * r.s;
    return qr;
}
function b2MulTRR(q, r) {
    let qr = new B2Rot();
    qr.s = q.c * r.s - q.s * r.c;
    qr.c = q.c * r.c + q.s * r.s;
    return qr;
}
function b2MulR2(q, v) {
    return new B2Vec2(q.c * v.x - q.s * v.y, q.s * v.x + q.c * v.y);
}
function b2MulTR2(q, v) {
    return new B2Vec2(q.c * v.x + q.s * v.y, -q.s * v.x + q.c * v.y);
}
function b2MulT2(T, v) {
    let x = T.q.c * v.x - T.q.s * v.y + T.p.x;
    let y = T.q.s * v.x + T.q.c * v.y + T.p.y;
    return new B2Vec2(x, y);
}
function b2MulTT2(T, v) {
    let px = v.x - T.p.x;
    let py = v.y - T.p.y;
    let x = T.q.c * px + T.q.s * py;
    let y = -T.q.s * px + T.q.c * py;
    return new B2Vec2(x, y);
}
function b2MulTT(A, B) {
    let C = new B2Transform();
    C.q = b2MulRR(A.q, B.q);
    let a = b2MulR2(A.q, B.p);
    C.p.x = a.x + A.p.x;
    C.p.y = a.y + A.p.y;
    return C;
}
function b2MulT(A, B) {
    let C = new B2Transform();
    C.q = b2MulTRR(A.q, B.q);
    let a = new B2Vec2(B.p.x - A.p.x, B.p.y - A.p.y);
    C.p = b2MulTR2(A.q, a);
    return C;
}
function b2Abs2(a) {
    return new B2Vec2(Math.abs(a.x), Math.abs(a.y));
}
function b2Abs22(A) {
    return new B2Mat22(b2Abs2(A.ex), b2Abs2(A.ey));
}
function b2Min(a, b) {
    return new B2Vec2(Math.min(a.x, b.x), Math.min(a.y, b.y));
}
function b2Max(a, b) {
    return new B2Vec2(Math.max(a.x, b.x), Math.max(a.y, b.y));
}
function b2ClampF(a, low, high) {
    return Math.max(low, Math.min(a, high));
}
function b2Clamp(a, low, high) {
    return b2Max(low, b2Min(a, high));
}
export function b2NextPowerOfTwo(_x) {
    let x = _x;
    x = x | (x >> 1);
    x = x | (x >> float_2);
    x = x | (x >> int_4);
    x = x | (x >> float_8);
    x = x | (x >> int_16);
    return x + 1;
}
function b2IsPowerOfTwo(x) {
    let result = x > 0 && (x & (x - 1)) == 0;
    return result;
}
export { b2MulT, add, add3, b2Mul32, b2Mul33, subtract, multM, b2MulTT2, multM3, subtract3, b2MulTM2 };
export { b2Vec2_zero, b2DistanceSquared, b2Min, b2Max, b2Abs2, b2ClampF, b2Mul22, b2Atan2 };
export { b2Mul, b2MulR2, b2Dot22, b2Sqrt, b2Cross, b2MulT2, b2Distance, b2MulTR2, b2Cross21, b2Cross12 };
