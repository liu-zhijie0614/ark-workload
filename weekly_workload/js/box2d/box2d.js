/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { B2PolygonShape } from './Collision/Shapes/b2Shape';
import { addEqual, B2Vec2 } from './Common/b2Math';
import { B2BodyDef, B2BodyType } from './Dynamics/b2Body';
import { B2FixtureDef } from './Dynamics/b2Fixture';
import { B2World } from './Dynamics/b2World';
/*
 * @State
 */
class Benchmark {
    /*
     * @Benchmark
     */
    benchmarkRun() {
        let loop_120 = 20;
        let multiple_1000 = 1000.0;
        let start = Date.now();
        for (let i = 0; i < loop_120; i++) {
            this.runIteration();
        }
        let end = Date.now();
        console.log('box2d: ms = ' + (end - start));
    }
    runIteration() {
        this.runBox2D();
    }
    runBox2D() {
        let times = 60;
        let second = 1;
        let fps = second / times;
        let loop_20 = 20;
        let velocityIterations = 10;
        let positionIterations = 3;
        let world = this.makeNewWorld();
        for (let i = 0; i < loop_20; i++) {
            world.step(fps, velocityIterations, positionIterations);
            // log('position_y = :' + world.m_bodyList?.position.y);
        }
    }
    /*
     * @Setup
     */
    makeNewWorld() {
        let loop_5 = 5;
        let loop_10 = 10;
        let density = 5.0;
        let x_x = -7.0;
        let x_y = 0.75;
        let gravity_y = -10.0;
        let zero = 0.0;
        let edge_v1_x = -40.0;
        let edge_v2_x = 40.0;
        let deltaX_x = 0.5625;
        let deltaX_y = 1;
        let deltaY_x = 1.125;
        let gravity = new B2Vec2(zero, gravity_y);
        let world = new B2World(gravity);
        let shape = new B2PolygonShape();
        shape.setAsEdge(new B2Vec2(edge_v1_x, zero), new B2Vec2(edge_v2_x, zero));
        shape.radius = 0;
        let fd = new B2FixtureDef();
        fd.density = 0.0;
        fd.shape = shape;
        let bd = new B2BodyDef();
        bd.type = B2BodyType.staticBody;
        let ground = world.createBody(bd);
        ground.createFixture(fd);
        let a = 0.5;
        let shape2 = new B2PolygonShape();
        shape2.setAsBox(a, a);
        shape2.radius = 0.0;
        let x = new B2Vec2(x_x, x_y);
        let y = new B2Vec2();
        let deltaX = new B2Vec2(deltaX_x, deltaX_y);
        let deltaY = new B2Vec2(deltaY_x, zero);
        for (let i = 0; i < loop_10; i++) {
            y.set(x.x, x.y);
            for (let j = 0; j < loop_5; j++) {
                let fd = new B2FixtureDef();
                fd.density = density;
                fd.shape = shape2;
                let bd = new B2BodyDef();
                bd.type = B2BodyType.dynamicBody;
                bd.position.set(y.x, y.y);
                let body = world.createBody(bd);
                body.createFixture(fd);
                addEqual(y, deltaY);
            }
            addEqual(x, deltaX);
        }
        return world;
    }
}
new Benchmark().benchmarkRun();
let debug = false;
function log(msg) {
    if (debug) {
        console.log(msg);
    }
}
