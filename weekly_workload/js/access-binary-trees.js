"use strict";
/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
const COMMON_NUMBER_TWO = 2;
const CURRENT_LOOP_COUNT_NUMBER_TWO = 7;
const MAX_LOOP_COUNT = 80;
const MS_CONVERSION_RATIO = 1000;
class TreeNode {
    constructor(left, right, item) {
        this.left = left;
        this.right = right;
        this.item = item;
    }
    itemCheck() {
        var _a, _b;
        if (this.left == null) {
            return this.item;
        }
        else {
            return (this.item +
                ((_a = this.left.itemCheck()) !== null && _a !== void 0 ? _a : 0.0) -
                ((_b = this.right.itemCheck()) !== null && _b !== void 0 ? _b : 0.0));
        }
    }
}
function bottomUpTree(item, depth) {
    if (depth > 0) {
        return new TreeNode(bottomUpTree(COMMON_NUMBER_TWO * item - 1, depth - 1), bottomUpTree(COMMON_NUMBER_TWO * item, depth - 1), item);
    }
    else {
        return new TreeNode(null, null, item);
    }
}
/*
 *@State
 */
class Benchmark {
    run() {
        let ret = 0.0;
        for (let n = 4; n <= CURRENT_LOOP_COUNT_NUMBER_TWO; n++) {
            let minDepth = 4;
            let maxDepth = Math.max(minDepth + COMMON_NUMBER_TWO, n);
            let stretchDepth = maxDepth + 1;
            let check = bottomUpTree(0, stretchDepth).itemCheck();
            let longLivedTree = bottomUpTree(0, maxDepth);
            for (let depth = minDepth; depth < maxDepth; depth += COMMON_NUMBER_TWO) {
                let iterations = 1 << (maxDepth - depth + minDepth);
                check = 0;
                for (let i = 1; i <= iterations; i++) {
                    check += bottomUpTree(i, depth).itemCheck();
                    check += bottomUpTree(-i, depth).itemCheck();
                }
            }
            ret += longLivedTree.itemCheck();
        }
        let expected = -4;
        if (ret != expected) {
            console.log('ERROR: bad result: expected' + expected + ' but got' + ret);
        }
    }
    /*
     * @Benchmark
     */
    runIterationTime() {
        let start = Date.now();
        for (let i = 0; i < MAX_LOOP_COUNT; i++) {
            this.run();
        }
        let end = Date.now();
        console.log('access-binary-trees: ms = ' + (end - start));
    }
}
new Benchmark().runIterationTime();
