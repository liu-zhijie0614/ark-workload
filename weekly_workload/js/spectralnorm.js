"use strict";
const input = 500;
function approximate(u, v, buffer) {
    for (let count = 0; count < 10; count++) {
        multiplyAv(u, buffer);
        multiplyAtv(buffer, v);
        multiplyAv(v, buffer);
        multiplyAtv(buffer, u);
    }
    let vLen = v.length;
    let vBv = 0.0, vv = 0.0;
    for (let i = 0; i < vLen; i++) {
        vBv += u[i] * v[i];
        vv += v[i] * v[i];
    }
    return Math.sqrt(vBv / vv);
}
function multiplyAv(v, av) {
    let vLen = v.length;
    let avLen = av.length;
    for (let i = 0; i < avLen; i++) {
        av[i] = 0.0;
        for (let j = 0; j < vLen; j++) {
            const ij = i + j;
            let ret = 1.0 / (ij * (ij + 1) / 2 + i + 1);
            av[i] += ret * v[j];
        }
    }
}
function multiplyAtv(v, atv) {
    let vLen = v.length;
    let atvLen = atv.length;
    for (let i = 0; i < atvLen; i++) {
        atv[i] = 0;
        for (let j = 0; j < vLen; j++) {
            const ij = i + j;
            let ret = 1.0 / (ij * (ij + 1) / 2 + j + 1);
            atv[i] += ret * v[j];
        }
    }
}
function RunSpectralNorm() {
    const n = input;
    const u = Array(n).fill(1.0);
    const v = Array(n).fill(0.0);
    const buffer = Array(n).fill(0.0);
    let uF = new Float64Array(u);
    let vF = new Float64Array(v);
    let bufferF = new Float64Array(buffer);
    let res = 0.0;
    const start = Date.now();
    res = approximate(uF, vF, bufferF);
    const end = Date.now();
    // ASSERT_FLOAT_EQ(res, 1.2742241159529055);
    let time = (end - start) ;
    console.log("Array Access - RunSpectralNorm:\t" + String(time) + "\tms");
    return time;
}
RunSpectralNorm();
// let runner = new BenchmarkRunner("Array Access - RunSpectralNorm", RunSpectralNorm);
// runner.run();
