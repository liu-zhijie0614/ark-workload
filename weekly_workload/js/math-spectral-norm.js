"use strict";
/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
const NUM_TWO_CONST = 2;
const NUM_LOOP_CONST = 10;
const NUM_WHILE_CONST = 48;
const NUM_STIME_CONST = 1000;
const NUM_TIME_LOOP_CONST = 80;
function a(i, j) {
    let result = ((i + j) * (i + j + 1)) / NUM_TWO_CONST + i + 1;
    return 1.0 / result;
}
function au(u, v) {
    for (let i = 0; i < u.length; ++i) {
        let t = 0;
        for (let j = 0; j < u.length; ++j) {
            t += a(i, j) * u[j];
        }
        v[i] = t;
    }
}
function atu(u, v) {
    for (let i = 0; i < u.length; ++i) {
        let t = 0;
        for (let j = 0; j < u.length; ++j) {
            t += a(j, i) * u[j];
        }
        v[i] = t;
    }
}
function atAu(u, v, w) {
    au(u, w);
    atu(w, v);
}
function spectralnorm(n) {
    let i;
    let u = [];
    let v = [];
    let w = [];
    let vv = 0;
    let vBv = 0;
    for (i = 0; i < n; ++i) {
        u.push(1.0);
        v.push(0.0);
        w.push(0.0);
    }
    for (i = 0; i < NUM_LOOP_CONST; ++i) {
        atAu(u, v, w);
        atAu(v, u, w);
    }
    for (i = 0; i < n; ++i) {
        vBv += u[i] * v[i];
        vv += v[i] * v[i];
    }
    return Math.sqrt(vBv / vv);
}
/**
 * @State
 */
class Benchmark {
    run() {
        let total = 0;
        let n = 6;
        while (n <= NUM_WHILE_CONST) {
            total = total + spectralnorm(n);
            n = n * NUM_TWO_CONST;
        }
        let expected = 5.086694231303284;
        if (total != expected) {
            console.log('ERROR: bad result: expected ' + expected + ' but got ' + total);
        }
    }
    /**
     * @Benchmark
     */
    runIterationTime() {
        let start = Date.now();
        for (let i = 0; i < NUM_TIME_LOOP_CONST; i++) {
            this.run();
        }
        let end = Date.now();
        console.log('math-spectral-norm: ms = ' + (end - start));
    }
}
new Benchmark().runIterationTime();
