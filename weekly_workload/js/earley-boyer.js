"use strict";
/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
class sc_Pair {
    constructor(car, cdr) {
        this.car = car;
        this.cdr = cdr;
    }
}
class SC_NSNull {
    constructor() { }
}
let sc_const_4_nboyer = new sc_Pair("\u1E9Cimplies", new sc_Pair(new sc_Pair("\u1E9Cand", new sc_Pair(new sc_Pair("\u1E9Cimplies", new sc_Pair("\u1E9Cx", new sc_Pair("\u1E9Cy", new SC_NSNull()))), new sc_Pair(new sc_Pair("\u1E9Cand", new sc_Pair(new sc_Pair("\u1E9Cimplies", new sc_Pair("\u1E9Cy", new sc_Pair("\u1E9Cz", new SC_NSNull()))), new sc_Pair(new sc_Pair("\u1E9Cand", new sc_Pair(new sc_Pair("\u1E9Cimplies", new sc_Pair("\u1E9Cz", new sc_Pair("\u1E9Cu", new SC_NSNull()))), new sc_Pair(new sc_Pair("\u1E9Cimplies", new sc_Pair("\u1E9Cu", new sc_Pair("\u1E9Cw", new SC_NSNull()))), new SC_NSNull()))), new SC_NSNull()))), new SC_NSNull()))), new sc_Pair(new sc_Pair("\u1E9Cimplies", new sc_Pair("\u1E9Cx", new sc_Pair("\u1E9Cw", new SC_NSNull()))), new SC_NSNull())));
let sc_const_3_nboyer = sc_list(new sc_Pair("\u1E9Cequal", new sc_Pair(new sc_Pair("\u1E9Ccompile", new sc_Pair("\u1E9Cform", new SC_NSNull())), new sc_Pair(new sc_Pair("\u1E9Creverse", new sc_Pair(new sc_Pair("\u1E9Ccodegen", new sc_Pair(new sc_Pair("\u1E9Coptimize", new sc_Pair("\u1E9Cform", new SC_NSNull())), new sc_Pair(new sc_Pair("\u1E9Cnil", new SC_NSNull()), new SC_NSNull()))), new SC_NSNull())), new SC_NSNull()))), new sc_Pair("\u1E9Cequal", new sc_Pair(new sc_Pair("\u1E9Ceqp", new sc_Pair("\u1E9Cx", new sc_Pair("\u1E9Cy", new SC_NSNull()))), new sc_Pair(new sc_Pair("\u1E9Cequal", new sc_Pair(new sc_Pair("\u1E9Cfix", new sc_Pair("\u1E9Cx", new SC_NSNull())), new sc_Pair(new sc_Pair("\u1E9Cfix", new sc_Pair("\u1E9Cy", new SC_NSNull())), new SC_NSNull()))), new SC_NSNull()))), new sc_Pair("\u1E9Cequal", new sc_Pair(new sc_Pair("\u1E9Cgreaterp", new sc_Pair("\u1E9Cx", new sc_Pair("\u1E9Cy", new SC_NSNull()))), new sc_Pair(new sc_Pair("\u1E9Clessp", new sc_Pair("\u1E9Cy", new sc_Pair("\u1E9Cx", new SC_NSNull()))), new SC_NSNull()))), new sc_Pair("\u1E9Cequal", new sc_Pair(new sc_Pair("\u1E9Clesseqp", new sc_Pair("\u1E9Cx", new sc_Pair("\u1E9Cy", new SC_NSNull()))), new sc_Pair(new sc_Pair("\u1E9Cnot", new sc_Pair(new sc_Pair("\u1E9Clessp", new sc_Pair("\u1E9Cy", new sc_Pair("\u1E9Cx", new SC_NSNull()))), new SC_NSNull())), new SC_NSNull()))), new sc_Pair("\u1E9Cequal", new sc_Pair(new sc_Pair("\u1E9Cgreatereqp", new sc_Pair("\u1E9Cx", new sc_Pair("\u1E9Cy", new SC_NSNull()))), new sc_Pair(new sc_Pair("\u1E9Cnot", new sc_Pair(new sc_Pair("\u1E9Clessp", new sc_Pair("\u1E9Cx", new sc_Pair("\u1E9Cy", new SC_NSNull()))), new SC_NSNull())), new SC_NSNull()))), new sc_Pair("\u1E9Cequal", new sc_Pair(new sc_Pair("\u1E9Cboolean", new sc_Pair("\u1E9Cx", new SC_NSNull())), new sc_Pair(new sc_Pair("\u1E9Cor", new sc_Pair(new sc_Pair("\u1E9Cequal", new sc_Pair("\u1E9Cx", new sc_Pair(new sc_Pair("\u1E9Ct", new SC_NSNull()), new SC_NSNull()))), new sc_Pair(new sc_Pair("\u1E9Cequal", new sc_Pair("\u1E9Cx", new sc_Pair(new sc_Pair("\u1E9Cf", new SC_NSNull()), new SC_NSNull()))), new SC_NSNull()))), new SC_NSNull()))), new sc_Pair("\u1E9Cequal", new sc_Pair(new sc_Pair("\u1E9Ciff", new sc_Pair("\u1E9Cx", new sc_Pair("\u1E9Cy", new SC_NSNull()))), new sc_Pair(new sc_Pair("\u1E9Cand", new sc_Pair(new sc_Pair("\u1E9Cimplies", new sc_Pair("\u1E9Cx", new sc_Pair("\u1E9Cy", new SC_NSNull()))), new sc_Pair(new sc_Pair("\u1E9Cimplies", new sc_Pair("\u1E9Cy", new sc_Pair("\u1E9Cx", new SC_NSNull()))), new SC_NSNull()))), new SC_NSNull()))), new sc_Pair("\u1E9Cequal", new sc_Pair(new sc_Pair("\u1E9Ceven1", new sc_Pair("\u1E9Cx", new SC_NSNull())), new sc_Pair(new sc_Pair("\u1E9Cif", new sc_Pair(new sc_Pair("\u1E9Czerop", new sc_Pair("\u1E9Cx", new SC_NSNull())), new sc_Pair(new sc_Pair("\u1E9Ct", new SC_NSNull()), new sc_Pair(new sc_Pair("\u1E9Codd", new sc_Pair(new sc_Pair("\u1E9Csub1", new sc_Pair("\u1E9Cx", new SC_NSNull())), new SC_NSNull())), new SC_NSNull())))), new SC_NSNull()))), new sc_Pair("\u1E9Cequal", new sc_Pair(new sc_Pair("\u1E9Ccountps-", new sc_Pair("\u1E9Cl", new sc_Pair("\u1E9Cpred", new SC_NSNull()))), new sc_Pair(new sc_Pair("\u1E9Ccountps-loop", new sc_Pair("\u1E9Cl", new sc_Pair("\u1E9Cpred", new sc_Pair(new sc_Pair("\u1E9Czero", new SC_NSNull()), new SC_NSNull())))), new SC_NSNull()))), new sc_Pair("\u1E9Cequal", new sc_Pair(new sc_Pair("\u1E9Cfact-", new sc_Pair("\u1E9Ci", new SC_NSNull())), new sc_Pair(new sc_Pair("\u1E9Cfact-loop", new sc_Pair("\u1E9Ci", new sc_Pair(1, new SC_NSNull()))), new SC_NSNull()))), new sc_Pair("\u1E9Cequal", new sc_Pair(new sc_Pair("\u1E9Creverse-", new sc_Pair("\u1E9Cx", new SC_NSNull())), new sc_Pair(new sc_Pair("\u1E9Creverse-loop", new sc_Pair("\u1E9Cx", new sc_Pair(new sc_Pair("\u1E9Cnil", new SC_NSNull()), new SC_NSNull()))), new SC_NSNull()))), new sc_Pair("\u1E9Cequal", new sc_Pair(new sc_Pair("\u1E9Cdivides", new sc_Pair("\u1E9Cx", new sc_Pair("\u1E9Cy", new SC_NSNull()))), new sc_Pair(new sc_Pair("\u1E9Czerop", new sc_Pair(new sc_Pair("\u1E9Cremainder", new sc_Pair("\u1E9Cy", new sc_Pair("\u1E9Cx", new SC_NSNull()))), new SC_NSNull())), new SC_NSNull()))), new sc_Pair("\u1E9Cequal", new sc_Pair(new sc_Pair("\u1E9Cassume-true", new sc_Pair("\u1E9Cvar", new sc_Pair("\u1E9Calist", new SC_NSNull()))), new sc_Pair(new sc_Pair("\u1E9Ccons", new sc_Pair(new sc_Pair("\u1E9Ccons", new sc_Pair("\u1E9Cvar", new sc_Pair(new sc_Pair("\u1E9Ct", new SC_NSNull()), new SC_NSNull()))), new sc_Pair("\u1E9Calist", new SC_NSNull()))), new SC_NSNull()))), new sc_Pair("\u1E9Cequal", new sc_Pair(new sc_Pair("\u1E9Cassume-false", new sc_Pair("\u1E9Cvar", new sc_Pair("\u1E9Calist", new SC_NSNull()))), new sc_Pair(new sc_Pair("\u1E9Ccons", new sc_Pair(new sc_Pair("\u1E9Ccons", new sc_Pair("\u1E9Cvar", new sc_Pair(new sc_Pair("\u1E9Cf", new SC_NSNull()), new SC_NSNull()))), new sc_Pair("\u1E9Calist", new SC_NSNull()))), new SC_NSNull()))), new sc_Pair("\u1E9Cequal", new sc_Pair(new sc_Pair("\u1E9Ctautology-checker", new sc_Pair("\u1E9Cx", new SC_NSNull())), new sc_Pair(new sc_Pair("\u1E9Ctautologyp", new sc_Pair(new sc_Pair("\u1E9Cnormalize", new sc_Pair("\u1E9Cx", new SC_NSNull())), new sc_Pair(new sc_Pair("\u1E9Cnil", new SC_NSNull()), new SC_NSNull()))), new SC_NSNull()))), new sc_Pair("\u1E9Cequal", new sc_Pair(new sc_Pair("\u1E9Cfalsify", new sc_Pair("\u1E9Cx", new SC_NSNull())), new sc_Pair(new sc_Pair("\u1E9Cfalsify1", new sc_Pair(new sc_Pair("\u1E9Cnormalize", new sc_Pair("\u1E9Cx", new SC_NSNull())), new sc_Pair(new sc_Pair("\u1E9Cnil", new SC_NSNull()), new SC_NSNull()))), new SC_NSNull()))), new sc_Pair("\u1E9Cequal", new sc_Pair(new sc_Pair("\u1E9Cprime", new sc_Pair("\u1E9Cx", new SC_NSNull())), new sc_Pair(new sc_Pair("\u1E9Cand", new sc_Pair(new sc_Pair("\u1E9Cnot", new sc_Pair(new sc_Pair("\u1E9Czerop", new sc_Pair("\u1E9Cx", new SC_NSNull())), new SC_NSNull())), new sc_Pair(new sc_Pair("\u1E9Cnot", new sc_Pair(new sc_Pair("\u1E9Cequal", new sc_Pair("\u1E9Cx", new sc_Pair(new sc_Pair("\u1E9Cadd1", new sc_Pair(new sc_Pair("\u1E9Czero", new SC_NSNull()), new SC_NSNull())), new SC_NSNull()))), new SC_NSNull())), new sc_Pair(new sc_Pair("\u1E9Cprime1", new sc_Pair("\u1E9Cx", new sc_Pair(new sc_Pair("\u1E9Csub1", new sc_Pair("\u1E9Cx", new SC_NSNull())), new SC_NSNull()))), new SC_NSNull())))), new SC_NSNull()))), new sc_Pair("\u1E9Cequal", new sc_Pair(new sc_Pair("\u1E9Cand", new sc_Pair("\u1E9Cp", new sc_Pair("\u1E9Cq", new SC_NSNull()))), new sc_Pair(new sc_Pair("\u1E9Cif", new sc_Pair("\u1E9Cp", new sc_Pair(new sc_Pair("\u1E9Cif", new sc_Pair("\u1E9Cq", new sc_Pair(new sc_Pair("\u1E9Ct", new SC_NSNull()), new sc_Pair(new sc_Pair("\u1E9Cf", new SC_NSNull()), new SC_NSNull())))), new sc_Pair(new sc_Pair("\u1E9Cf", new SC_NSNull()), new SC_NSNull())))), new SC_NSNull()))), new sc_Pair("\u1E9Cequal", new sc_Pair(new sc_Pair("\u1E9Cor", new sc_Pair("\u1E9Cp", new sc_Pair("\u1E9Cq", new SC_NSNull()))), new sc_Pair(new sc_Pair("\u1E9Cif", new sc_Pair("\u1E9Cp", new sc_Pair(new sc_Pair("\u1E9Ct", new SC_NSNull()), new sc_Pair(new sc_Pair("\u1E9Cif", new sc_Pair("\u1E9Cq", new sc_Pair(new sc_Pair("\u1E9Ct", new SC_NSNull()), new sc_Pair(new sc_Pair("\u1E9Cf", new SC_NSNull()), new SC_NSNull())))), new SC_NSNull())))), new SC_NSNull()))), new sc_Pair("\u1E9Cequal", new sc_Pair(new sc_Pair("\u1E9Cnot", new sc_Pair("\u1E9Cp", new SC_NSNull())), new sc_Pair(new sc_Pair("\u1E9Cif", new sc_Pair("\u1E9Cp", new sc_Pair(new sc_Pair("\u1E9Cf", new SC_NSNull()), new sc_Pair(new sc_Pair("\u1E9Ct", new SC_NSNull()), new SC_NSNull())))), new SC_NSNull()))), new sc_Pair("\u1E9Cequal", new sc_Pair(new sc_Pair("\u1E9Cimplies", new sc_Pair("\u1E9Cp", new sc_Pair("\u1E9Cq", new SC_NSNull()))), new sc_Pair(new sc_Pair("\u1E9Cif", new sc_Pair("\u1E9Cp", new sc_Pair(new sc_Pair("\u1E9Cif", new sc_Pair("\u1E9Cq", new sc_Pair(new sc_Pair("\u1E9Ct", new SC_NSNull()), new sc_Pair(new sc_Pair("\u1E9Cf", new SC_NSNull()), new SC_NSNull())))), new sc_Pair(new sc_Pair("\u1E9Ct", new SC_NSNull()), new SC_NSNull())))), new SC_NSNull()))), new sc_Pair("\u1E9Cequal", new sc_Pair(new sc_Pair("\u1E9Cfix", new sc_Pair("\u1E9Cx", new SC_NSNull())), new sc_Pair(new sc_Pair("\u1E9Cif", new sc_Pair(new sc_Pair("\u1E9Cnumberp", new sc_Pair("\u1E9Cx", new SC_NSNull())), new sc_Pair("\u1E9Cx", new sc_Pair(new sc_Pair("\u1E9Czero", new SC_NSNull()), new SC_NSNull())))), new SC_NSNull()))), new sc_Pair("\u1E9Cequal", new sc_Pair(new sc_Pair("\u1E9Cif", new sc_Pair(new sc_Pair("\u1E9Cif", new sc_Pair("\u1E9Ca", new sc_Pair("\u1E9Cb", new sc_Pair("\u1E9Cc", new SC_NSNull())))), new sc_Pair("\u1E9Cd", new sc_Pair("\u1E9Ce", new SC_NSNull())))), new sc_Pair(new sc_Pair("\u1E9Cif", new sc_Pair("\u1E9Ca", new sc_Pair(new sc_Pair("\u1E9Cif", new sc_Pair("\u1E9Cb", new sc_Pair("\u1E9Cd", new sc_Pair("\u1E9Ce", new SC_NSNull())))), new sc_Pair(new sc_Pair("\u1E9Cif", new sc_Pair("\u1E9Cc", new sc_Pair("\u1E9Cd", new sc_Pair("\u1E9Ce", new SC_NSNull())))), new SC_NSNull())))), new SC_NSNull()))), new sc_Pair("\u1E9Cequal", new sc_Pair(new sc_Pair("\u1E9Czerop", new sc_Pair("\u1E9Cx", new SC_NSNull())), new sc_Pair(new sc_Pair("\u1E9Cor", new sc_Pair(new sc_Pair("\u1E9Cequal", new sc_Pair("\u1E9Cx", new sc_Pair(new sc_Pair("\u1E9Czero", new SC_NSNull()), new SC_NSNull()))), new sc_Pair(new sc_Pair("\u1E9Cnot", new sc_Pair(new sc_Pair("\u1E9Cnumberp", new sc_Pair("\u1E9Cx", new SC_NSNull())), new SC_NSNull())), new SC_NSNull()))), new SC_NSNull()))), new sc_Pair("\u1E9Cequal", new sc_Pair(new sc_Pair("\u1E9Cplus", new sc_Pair(new sc_Pair("\u1E9Cplus", new sc_Pair("\u1E9Cx", new sc_Pair("\u1E9Cy", new SC_NSNull()))), new sc_Pair("\u1E9Cz", new SC_NSNull()))), new sc_Pair(new sc_Pair("\u1E9Cplus", new sc_Pair("\u1E9Cx", new sc_Pair(new sc_Pair("\u1E9Cplus", new sc_Pair("\u1E9Cy", new sc_Pair("\u1E9Cz", new SC_NSNull()))), new SC_NSNull()))), new SC_NSNull()))), new sc_Pair("\u1E9Cequal", new sc_Pair(new sc_Pair("\u1E9Cequal", new sc_Pair(new sc_Pair("\u1E9Cplus", new sc_Pair("\u1E9Ca", new sc_Pair("\u1E9Cb", new SC_NSNull()))), new sc_Pair(new sc_Pair("\u1E9Czero", new SC_NSNull()), new SC_NSNull()))), new sc_Pair(new sc_Pair("\u1E9Cand", new sc_Pair(new sc_Pair("\u1E9Czerop", new sc_Pair("\u1E9Ca", new SC_NSNull())), new sc_Pair(new sc_Pair("\u1E9Czerop", new sc_Pair("\u1E9Cb", new SC_NSNull())), new SC_NSNull()))), new SC_NSNull()))), new sc_Pair("\u1E9Cequal", new sc_Pair(new sc_Pair("\u1E9Cdifference", new sc_Pair("\u1E9Cx", new sc_Pair("\u1E9Cx", new SC_NSNull()))), new sc_Pair(new sc_Pair("\u1E9Czero", new SC_NSNull()), new SC_NSNull()))), new sc_Pair("\u1E9Cequal", new sc_Pair(new sc_Pair("\u1E9Cequal", new sc_Pair(new sc_Pair("\u1E9Cplus", new sc_Pair("\u1E9Ca", new sc_Pair("\u1E9Cb", new SC_NSNull()))), new sc_Pair(new sc_Pair("\u1E9Cplus", new sc_Pair("\u1E9Ca", new sc_Pair("\u1E9Cc", new SC_NSNull()))), new SC_NSNull()))), new sc_Pair(new sc_Pair("\u1E9Cequal", new sc_Pair(new sc_Pair("\u1E9Cfix", new sc_Pair("\u1E9Cb", new SC_NSNull())), new sc_Pair(new sc_Pair("\u1E9Cfix", new sc_Pair("\u1E9Cc", new SC_NSNull())), new SC_NSNull()))), new SC_NSNull()))), new sc_Pair("\u1E9Cequal", new sc_Pair(new sc_Pair("\u1E9Cequal", new sc_Pair(new sc_Pair("\u1E9Czero", new SC_NSNull()), new sc_Pair(new sc_Pair("\u1E9Cdifference", new sc_Pair("\u1E9Cx", new sc_Pair("\u1E9Cy", new SC_NSNull()))), new SC_NSNull()))), new sc_Pair(new sc_Pair("\u1E9Cnot", new sc_Pair(new sc_Pair("\u1E9Clessp", new sc_Pair("\u1E9Cy", new sc_Pair("\u1E9Cx", new SC_NSNull()))), new SC_NSNull())), new SC_NSNull()))), new sc_Pair("\u1E9Cequal", new sc_Pair(new sc_Pair("\u1E9Cequal", new sc_Pair("\u1E9Cx", new sc_Pair(new sc_Pair("\u1E9Cdifference", new sc_Pair("\u1E9Cx", new sc_Pair("\u1E9Cy", new SC_NSNull()))), new SC_NSNull()))), new sc_Pair(new sc_Pair("\u1E9Cand", new sc_Pair(new sc_Pair("\u1E9Cnumberp", new sc_Pair("\u1E9Cx", new SC_NSNull())), new sc_Pair(new sc_Pair("\u1E9Cor", new sc_Pair(new sc_Pair("\u1E9Cequal", new sc_Pair("\u1E9Cx", new sc_Pair(new sc_Pair("\u1E9Czero", new SC_NSNull()), new SC_NSNull()))), new sc_Pair(new sc_Pair("\u1E9Czerop", new sc_Pair("\u1E9Cy", new SC_NSNull())), new SC_NSNull()))), new SC_NSNull()))), new SC_NSNull()))), new sc_Pair("\u1E9Cequal", new sc_Pair(new sc_Pair("\u1E9Cmeaning", new sc_Pair(new sc_Pair("\u1E9Cplus-tree", new sc_Pair(new sc_Pair("\u1E9Cappend", new sc_Pair("\u1E9Cx", new sc_Pair("\u1E9Cy", new SC_NSNull()))), new SC_NSNull())), new sc_Pair("\u1E9Ca", new SC_NSNull()))), new sc_Pair(new sc_Pair("\u1E9Cplus", new sc_Pair(new sc_Pair("\u1E9Cmeaning", new sc_Pair(new sc_Pair("\u1E9Cplus-tree", new sc_Pair("\u1E9Cx", new SC_NSNull())), new sc_Pair("\u1E9Ca", new SC_NSNull()))), new sc_Pair(new sc_Pair("\u1E9Cmeaning", new sc_Pair(new sc_Pair("\u1E9Cplus-tree", new sc_Pair("\u1E9Cy", new SC_NSNull())), new sc_Pair("\u1E9Ca", new SC_NSNull()))), new SC_NSNull()))), new SC_NSNull()))), new sc_Pair("\u1E9Cequal", new sc_Pair(new sc_Pair("\u1E9Cmeaning", new sc_Pair(new sc_Pair("\u1E9Cplus-tree", new sc_Pair(new sc_Pair("\u1E9Cplus-fringe", new sc_Pair("\u1E9Cx", new SC_NSNull())), new SC_NSNull())), new sc_Pair("\u1E9Ca", new SC_NSNull()))), new sc_Pair(new sc_Pair("\u1E9Cfix", new sc_Pair(new sc_Pair("\u1E9Cmeaning", new sc_Pair("\u1E9Cx", new sc_Pair("\u1E9Ca", new SC_NSNull()))), new SC_NSNull())), new SC_NSNull()))), new sc_Pair("\u1E9Cequal", new sc_Pair(new sc_Pair("\u1E9Cappend", new sc_Pair(new sc_Pair("\u1E9Cappend", new sc_Pair("\u1E9Cx", new sc_Pair("\u1E9Cy", new SC_NSNull()))), new sc_Pair("\u1E9Cz", new SC_NSNull()))), new sc_Pair(new sc_Pair("\u1E9Cappend", new sc_Pair("\u1E9Cx", new sc_Pair(new sc_Pair("\u1E9Cappend", new sc_Pair("\u1E9Cy", new sc_Pair("\u1E9Cz", new SC_NSNull()))), new SC_NSNull()))), new SC_NSNull()))), new sc_Pair("\u1E9Cequal", new sc_Pair(new sc_Pair("\u1E9Creverse", new sc_Pair(new sc_Pair("\u1E9Cappend", new sc_Pair("\u1E9Ca", new sc_Pair("\u1E9Cb", new SC_NSNull()))), new SC_NSNull())), new sc_Pair(new sc_Pair("\u1E9Cappend", new sc_Pair(new sc_Pair("\u1E9Creverse", new sc_Pair("\u1E9Cb", new SC_NSNull())), new sc_Pair(new sc_Pair("\u1E9Creverse", new sc_Pair("\u1E9Ca", new SC_NSNull())), new SC_NSNull()))), new SC_NSNull()))), new sc_Pair("\u1E9Cequal", new sc_Pair(new sc_Pair("\u1E9Ctimes", new sc_Pair("\u1E9Cx", new sc_Pair(new sc_Pair("\u1E9Cplus", new sc_Pair("\u1E9Cy", new sc_Pair("\u1E9Cz", new SC_NSNull()))), new SC_NSNull()))), new sc_Pair(new sc_Pair("\u1E9Cplus", new sc_Pair(new sc_Pair("\u1E9Ctimes", new sc_Pair("\u1E9Cx", new sc_Pair("\u1E9Cy", new SC_NSNull()))), new sc_Pair(new sc_Pair("\u1E9Ctimes", new sc_Pair("\u1E9Cx", new sc_Pair("\u1E9Cz", new SC_NSNull()))), new SC_NSNull()))), new SC_NSNull()))), new sc_Pair("\u1E9Cequal", new sc_Pair(new sc_Pair("\u1E9Ctimes", new sc_Pair(new sc_Pair("\u1E9Ctimes", new sc_Pair("\u1E9Cx", new sc_Pair("\u1E9Cy", new SC_NSNull()))), new sc_Pair("\u1E9Cz", new SC_NSNull()))), new sc_Pair(new sc_Pair("\u1E9Ctimes", new sc_Pair("\u1E9Cx", new sc_Pair(new sc_Pair("\u1E9Ctimes", new sc_Pair("\u1E9Cy", new sc_Pair("\u1E9Cz", new SC_NSNull()))), new SC_NSNull()))), new SC_NSNull()))), new sc_Pair("\u1E9Cequal", new sc_Pair(new sc_Pair("\u1E9Cequal", new sc_Pair(new sc_Pair("\u1E9Ctimes", new sc_Pair("\u1E9Cx", new sc_Pair("\u1E9Cy", new SC_NSNull()))), new sc_Pair(new sc_Pair("\u1E9Czero", new SC_NSNull()), new SC_NSNull()))), new sc_Pair(new sc_Pair("\u1E9Cor", new sc_Pair(new sc_Pair("\u1E9Czerop", new sc_Pair("\u1E9Cx", new SC_NSNull())), new sc_Pair(new sc_Pair("\u1E9Czerop", new sc_Pair("\u1E9Cy", new SC_NSNull())), new SC_NSNull()))), new SC_NSNull()))), new sc_Pair("\u1E9Cequal", new sc_Pair(new sc_Pair("\u1E9Cexec", new sc_Pair(new sc_Pair("\u1E9Cappend", new sc_Pair("\u1E9Cx", new sc_Pair("\u1E9Cy", new SC_NSNull()))), new sc_Pair("\u1E9Cpds", new sc_Pair("\u1E9Cenvrn", new SC_NSNull())))), new sc_Pair(new sc_Pair("\u1E9Cexec", new sc_Pair("\u1E9Cy", new sc_Pair(new sc_Pair("\u1E9Cexec", new sc_Pair("\u1E9Cx", new sc_Pair("\u1E9Cpds", new sc_Pair("\u1E9Cenvrn", new SC_NSNull())))), new sc_Pair("\u1E9Cenvrn", new SC_NSNull())))), new SC_NSNull()))), new sc_Pair("\u1E9Cequal", new sc_Pair(new sc_Pair("\u1E9Cmc-flatten", new sc_Pair("\u1E9Cx", new sc_Pair("\u1E9Cy", new SC_NSNull()))), new sc_Pair(new sc_Pair("\u1E9Cappend", new sc_Pair(new sc_Pair("\u1E9Cflatten", new sc_Pair("\u1E9Cx", new SC_NSNull())), new sc_Pair("\u1E9Cy", new SC_NSNull()))), new SC_NSNull()))), new sc_Pair("\u1E9Cequal", new sc_Pair(new sc_Pair("\u1E9Cmember", new sc_Pair("\u1E9Cx", new sc_Pair(new sc_Pair("\u1E9Cappend", new sc_Pair("\u1E9Ca", new sc_Pair("\u1E9Cb", new SC_NSNull()))), new SC_NSNull()))), new sc_Pair(new sc_Pair("\u1E9Cor", new sc_Pair(new sc_Pair("\u1E9Cmember", new sc_Pair("\u1E9Cx", new sc_Pair("\u1E9Ca", new SC_NSNull()))), new sc_Pair(new sc_Pair("\u1E9Cmember", new sc_Pair("\u1E9Cx", new sc_Pair("\u1E9Cb", new SC_NSNull()))), new SC_NSNull()))), new SC_NSNull()))), new sc_Pair("\u1E9Cequal", new sc_Pair(new sc_Pair("\u1E9Cmember", new sc_Pair("\u1E9Cx", new sc_Pair(new sc_Pair("\u1E9Creverse", new sc_Pair("\u1E9Cy", new SC_NSNull())), new SC_NSNull()))), new sc_Pair(new sc_Pair("\u1E9Cmember", new sc_Pair("\u1E9Cx", new sc_Pair("\u1E9Cy", new SC_NSNull()))), new SC_NSNull()))), new sc_Pair("\u1E9Cequal", new sc_Pair(new sc_Pair("\u1E9Clength", new sc_Pair(new sc_Pair("\u1E9Creverse", new sc_Pair("\u1E9Cx", new SC_NSNull())), new SC_NSNull())), new sc_Pair(new sc_Pair("\u1E9Clength", new sc_Pair("\u1E9Cx", new SC_NSNull())), new SC_NSNull()))), new sc_Pair("\u1E9Cequal", new sc_Pair(new sc_Pair("\u1E9Cmember", new sc_Pair("\u1E9Ca", new sc_Pair(new sc_Pair("\u1E9Cintersect", new sc_Pair("\u1E9Cb", new sc_Pair("\u1E9Cc", new SC_NSNull()))), new SC_NSNull()))), new sc_Pair(new sc_Pair("\u1E9Cand", new sc_Pair(new sc_Pair("\u1E9Cmember", new sc_Pair("\u1E9Ca", new sc_Pair("\u1E9Cb", new SC_NSNull()))), new sc_Pair(new sc_Pair("\u1E9Cmember", new sc_Pair("\u1E9Ca", new sc_Pair("\u1E9Cc", new SC_NSNull()))), new SC_NSNull()))), new SC_NSNull()))), new sc_Pair("\u1E9Cequal", new sc_Pair(new sc_Pair("\u1E9Cnth", new sc_Pair(new sc_Pair("\u1E9Czero", new SC_NSNull()), new sc_Pair("\u1E9Ci", new SC_NSNull()))), new sc_Pair(new sc_Pair("\u1E9Czero", new SC_NSNull()), new SC_NSNull()))), new sc_Pair("\u1E9Cequal", new sc_Pair(new sc_Pair("\u1E9Cexp", new sc_Pair("\u1E9Ci", new sc_Pair(new sc_Pair("\u1E9Cplus", new sc_Pair("\u1E9Cj", new sc_Pair("\u1E9Ck", new SC_NSNull()))), new SC_NSNull()))), new sc_Pair(new sc_Pair("\u1E9Ctimes", new sc_Pair(new sc_Pair("\u1E9Cexp", new sc_Pair("\u1E9Ci", new sc_Pair("\u1E9Cj", new SC_NSNull()))), new sc_Pair(new sc_Pair("\u1E9Cexp", new sc_Pair("\u1E9Ci", new sc_Pair("\u1E9Ck", new SC_NSNull()))), new SC_NSNull()))), new SC_NSNull()))), new sc_Pair("\u1E9Cequal", new sc_Pair(new sc_Pair("\u1E9Cexp", new sc_Pair("\u1E9Ci", new sc_Pair(new sc_Pair("\u1E9Ctimes", new sc_Pair("\u1E9Cj", new sc_Pair("\u1E9Ck", new SC_NSNull()))), new SC_NSNull()))), new sc_Pair(new sc_Pair("\u1E9Cexp", new sc_Pair(new sc_Pair("\u1E9Cexp", new sc_Pair("\u1E9Ci", new sc_Pair("\u1E9Cj", new SC_NSNull()))), new sc_Pair("\u1E9Ck", new SC_NSNull()))), new SC_NSNull()))), new sc_Pair("\u1E9Cequal", new sc_Pair(new sc_Pair("\u1E9Creverse-loop", new sc_Pair("\u1E9Cx", new sc_Pair("\u1E9Cy", new SC_NSNull()))), new sc_Pair(new sc_Pair("\u1E9Cappend", new sc_Pair(new sc_Pair("\u1E9Creverse", new sc_Pair("\u1E9Cx", new SC_NSNull())), new sc_Pair("\u1E9Cy", new SC_NSNull()))), new SC_NSNull()))), new sc_Pair("\u1E9Cequal", new sc_Pair(new sc_Pair("\u1E9Creverse-loop", new sc_Pair("\u1E9Cx", new sc_Pair(new sc_Pair("\u1E9Cnil", new SC_NSNull()), new SC_NSNull()))), new sc_Pair(new sc_Pair("\u1E9Creverse", new sc_Pair("\u1E9Cx", new SC_NSNull())), new SC_NSNull()))), new sc_Pair("\u1E9Cequal", new sc_Pair(new sc_Pair("\u1E9Ccount-list", new sc_Pair("\u1E9Cz", new sc_Pair(new sc_Pair("\u1E9Csort-lp", new sc_Pair("\u1E9Cx", new sc_Pair("\u1E9Cy", new SC_NSNull()))), new SC_NSNull()))), new sc_Pair(new sc_Pair("\u1E9Cplus", new sc_Pair(new sc_Pair("\u1E9Ccount-list", new sc_Pair("\u1E9Cz", new sc_Pair("\u1E9Cx", new SC_NSNull()))), new sc_Pair(new sc_Pair("\u1E9Ccount-list", new sc_Pair("\u1E9Cz", new sc_Pair("\u1E9Cy", new SC_NSNull()))), new SC_NSNull()))), new SC_NSNull()))), new sc_Pair("\u1E9Cequal", new sc_Pair(new sc_Pair("\u1E9Cequal", new sc_Pair(new sc_Pair("\u1E9Cappend", new sc_Pair("\u1E9Ca", new sc_Pair("\u1E9Cb", new SC_NSNull()))), new sc_Pair(new sc_Pair("\u1E9Cappend", new sc_Pair("\u1E9Ca", new sc_Pair("\u1E9Cc", new SC_NSNull()))), new SC_NSNull()))), new sc_Pair(new sc_Pair("\u1E9Cequal", new sc_Pair("\u1E9Cb", new sc_Pair("\u1E9Cc", new SC_NSNull()))), new SC_NSNull()))), new sc_Pair("\u1E9Cequal", new sc_Pair(new sc_Pair("\u1E9Cplus", new sc_Pair(new sc_Pair("\u1E9Cremainder", new sc_Pair("\u1E9Cx", new sc_Pair("\u1E9Cy", new SC_NSNull()))), new sc_Pair(new sc_Pair("\u1E9Ctimes", new sc_Pair("\u1E9Cy", new sc_Pair(new sc_Pair("\u1E9Cquotient", new sc_Pair("\u1E9Cx", new sc_Pair("\u1E9Cy", new SC_NSNull()))), new SC_NSNull()))), new SC_NSNull()))), new sc_Pair(new sc_Pair("\u1E9Cfix", new sc_Pair("\u1E9Cx", new SC_NSNull())), new SC_NSNull()))), new sc_Pair("\u1E9Cequal", new sc_Pair(new sc_Pair("\u1E9Cpower-eval", new sc_Pair(new sc_Pair("\u1E9Cbig-plus1", new sc_Pair("\u1E9Cl", new sc_Pair("\u1E9Ci", new sc_Pair("\u1E9Cbase", new SC_NSNull())))), new sc_Pair("\u1E9Cbase", new SC_NSNull()))), new sc_Pair(new sc_Pair("\u1E9Cplus", new sc_Pair(new sc_Pair("\u1E9Cpower-eval", new sc_Pair("\u1E9Cl", new sc_Pair("\u1E9Cbase", new SC_NSNull()))), new sc_Pair("\u1E9Ci", new SC_NSNull()))), new SC_NSNull()))), new sc_Pair("\u1E9Cequal", new sc_Pair(new sc_Pair("\u1E9Cpower-eval", new sc_Pair(new sc_Pair("\u1E9Cbig-plus", new sc_Pair("\u1E9Cx", new sc_Pair("\u1E9Cy", new sc_Pair("\u1E9Ci", new sc_Pair("\u1E9Cbase", new SC_NSNull()))))), new sc_Pair("\u1E9Cbase", new SC_NSNull()))), new sc_Pair(new sc_Pair("\u1E9Cplus", new sc_Pair("\u1E9Ci", new sc_Pair(new sc_Pair("\u1E9Cplus", new sc_Pair(new sc_Pair("\u1E9Cpower-eval", new sc_Pair("\u1E9Cx", new sc_Pair("\u1E9Cbase", new SC_NSNull()))), new sc_Pair(new sc_Pair("\u1E9Cpower-eval", new sc_Pair("\u1E9Cy", new sc_Pair("\u1E9Cbase", new SC_NSNull()))), new SC_NSNull()))), new SC_NSNull()))), new SC_NSNull()))), new sc_Pair("\u1E9Cequal", new sc_Pair(new sc_Pair("\u1E9Cremainder", new sc_Pair("\u1E9Cy", new sc_Pair(1, new SC_NSNull()))), new sc_Pair(new sc_Pair("\u1E9Czero", new SC_NSNull()), new SC_NSNull()))), new sc_Pair("\u1E9Cequal", new sc_Pair(new sc_Pair("\u1E9Clessp", new sc_Pair(new sc_Pair("\u1E9Cremainder", new sc_Pair("\u1E9Cx", new sc_Pair("\u1E9Cy", new SC_NSNull()))), new sc_Pair("\u1E9Cy", new SC_NSNull()))), new sc_Pair(new sc_Pair("\u1E9Cnot", new sc_Pair(new sc_Pair("\u1E9Czerop", new sc_Pair("\u1E9Cy", new SC_NSNull())), new SC_NSNull())), new SC_NSNull()))), new sc_Pair("\u1E9Cequal", new sc_Pair(new sc_Pair("\u1E9Cremainder", new sc_Pair("\u1E9Cx", new sc_Pair("\u1E9Cx", new SC_NSNull()))), new sc_Pair(new sc_Pair("\u1E9Czero", new SC_NSNull()), new SC_NSNull()))), new sc_Pair("\u1E9Cequal", new sc_Pair(new sc_Pair("\u1E9Clessp", new sc_Pair(new sc_Pair("\u1E9Cquotient", new sc_Pair("\u1E9Ci", new sc_Pair("\u1E9Cj", new SC_NSNull()))), new sc_Pair("\u1E9Ci", new SC_NSNull()))), new sc_Pair(new sc_Pair("\u1E9Cand", new sc_Pair(new sc_Pair("\u1E9Cnot", new sc_Pair(new sc_Pair("\u1E9Czerop", new sc_Pair("\u1E9Ci", new SC_NSNull())), new SC_NSNull())), new sc_Pair(new sc_Pair("\u1E9Cor", new sc_Pair(new sc_Pair("\u1E9Czerop", new sc_Pair("\u1E9Cj", new SC_NSNull())), new sc_Pair(new sc_Pair("\u1E9Cnot", new sc_Pair(new sc_Pair("\u1E9Cequal", new sc_Pair("\u1E9Cj", new sc_Pair(1, new SC_NSNull()))), new SC_NSNull())), new SC_NSNull()))), new SC_NSNull()))), new SC_NSNull()))), new sc_Pair("\u1E9Cequal", new sc_Pair(new sc_Pair("\u1E9Clessp", new sc_Pair(new sc_Pair("\u1E9Cremainder", new sc_Pair("\u1E9Cx", new sc_Pair("\u1E9Cy", new SC_NSNull()))), new sc_Pair("\u1E9Cx", new SC_NSNull()))), new sc_Pair(new sc_Pair("\u1E9Cand", new sc_Pair(new sc_Pair("\u1E9Cnot", new sc_Pair(new sc_Pair("\u1E9Czerop", new sc_Pair("\u1E9Cy", new SC_NSNull())), new SC_NSNull())), new sc_Pair(new sc_Pair("\u1E9Cnot", new sc_Pair(new sc_Pair("\u1E9Czerop", new sc_Pair("\u1E9Cx", new SC_NSNull())), new SC_NSNull())), new sc_Pair(new sc_Pair("\u1E9Cnot", new sc_Pair(new sc_Pair("\u1E9Clessp", new sc_Pair("\u1E9Cx", new sc_Pair("\u1E9Cy", new SC_NSNull()))), new SC_NSNull())), new SC_NSNull())))), new SC_NSNull()))), new sc_Pair("\u1E9Cequal", new sc_Pair(new sc_Pair("\u1E9Cpower-eval", new sc_Pair(new sc_Pair("\u1E9Cpower-rep", new sc_Pair("\u1E9Ci", new sc_Pair("\u1E9Cbase", new SC_NSNull()))), new sc_Pair("\u1E9Cbase", new SC_NSNull()))), new sc_Pair(new sc_Pair("\u1E9Cfix", new sc_Pair("\u1E9Ci", new SC_NSNull())), new SC_NSNull()))), new sc_Pair("\u1E9Cequal", new sc_Pair(new sc_Pair("\u1E9Cpower-eval", new sc_Pair(new sc_Pair("\u1E9Cbig-plus", new sc_Pair(new sc_Pair("\u1E9Cpower-rep", new sc_Pair("\u1E9Ci", new sc_Pair("\u1E9Cbase", new SC_NSNull()))), new sc_Pair(new sc_Pair("\u1E9Cpower-rep", new sc_Pair("\u1E9Cj", new sc_Pair("\u1E9Cbase", new SC_NSNull()))), new sc_Pair(new sc_Pair("\u1E9Czero", new SC_NSNull()), new sc_Pair("\u1E9Cbase", new SC_NSNull()))))), new sc_Pair("\u1E9Cbase", new SC_NSNull()))), new sc_Pair(new sc_Pair("\u1E9Cplus", new sc_Pair("\u1E9Ci", new sc_Pair("\u1E9Cj", new SC_NSNull()))), new SC_NSNull()))), new sc_Pair("\u1E9Cequal", new sc_Pair(new sc_Pair("\u1E9Cgcd", new sc_Pair("\u1E9Cx", new sc_Pair("\u1E9Cy", new SC_NSNull()))), new sc_Pair(new sc_Pair("\u1E9Cgcd", new sc_Pair("\u1E9Cy", new sc_Pair("\u1E9Cx", new SC_NSNull()))), new SC_NSNull()))), new sc_Pair("\u1E9Cequal", new sc_Pair(new sc_Pair("\u1E9Cnth", new sc_Pair(new sc_Pair("\u1E9Cappend", new sc_Pair("\u1E9Ca", new sc_Pair("\u1E9Cb", new SC_NSNull()))), new sc_Pair("\u1E9Ci", new SC_NSNull()))), new sc_Pair(new sc_Pair("\u1E9Cappend", new sc_Pair(new sc_Pair("\u1E9Cnth", new sc_Pair("\u1E9Ca", new sc_Pair("\u1E9Ci", new SC_NSNull()))), new sc_Pair(new sc_Pair("\u1E9Cnth", new sc_Pair("\u1E9Cb", new sc_Pair(new sc_Pair("\u1E9Cdifference", new sc_Pair("\u1E9Ci", new sc_Pair(new sc_Pair("\u1E9Clength", new sc_Pair("\u1E9Ca", new SC_NSNull())), new SC_NSNull()))), new SC_NSNull()))), new SC_NSNull()))), new SC_NSNull()))), new sc_Pair("\u1E9Cequal", new sc_Pair(new sc_Pair("\u1E9Cdifference", new sc_Pair(new sc_Pair("\u1E9Cplus", new sc_Pair("\u1E9Cx", new sc_Pair("\u1E9Cy", new SC_NSNull()))), new sc_Pair("\u1E9Cx", new SC_NSNull()))), new sc_Pair(new sc_Pair("\u1E9Cfix", new sc_Pair("\u1E9Cy", new SC_NSNull())), new SC_NSNull()))), new sc_Pair("\u1E9Cequal", new sc_Pair(new sc_Pair("\u1E9Cdifference", new sc_Pair(new sc_Pair("\u1E9Cplus", new sc_Pair("\u1E9Cy", new sc_Pair("\u1E9Cx", new SC_NSNull()))), new sc_Pair("\u1E9Cx", new SC_NSNull()))), new sc_Pair(new sc_Pair("\u1E9Cfix", new sc_Pair("\u1E9Cy", new SC_NSNull())), new SC_NSNull()))), new sc_Pair("\u1E9Cequal", new sc_Pair(new sc_Pair("\u1E9Cdifference", new sc_Pair(new sc_Pair("\u1E9Cplus", new sc_Pair("\u1E9Cx", new sc_Pair("\u1E9Cy", new SC_NSNull()))), new sc_Pair(new sc_Pair("\u1E9Cplus", new sc_Pair("\u1E9Cx", new sc_Pair("\u1E9Cz", new SC_NSNull()))), new SC_NSNull()))), new sc_Pair(new sc_Pair("\u1E9Cdifference", new sc_Pair("\u1E9Cy", new sc_Pair("\u1E9Cz", new SC_NSNull()))), new SC_NSNull()))), new sc_Pair("\u1E9Cequal", new sc_Pair(new sc_Pair("\u1E9Ctimes", new sc_Pair("\u1E9Cx", new sc_Pair(new sc_Pair("\u1E9Cdifference", new sc_Pair("\u1E9Cc", new sc_Pair("\u1E9Cw", new SC_NSNull()))), new SC_NSNull()))), new sc_Pair(new sc_Pair("\u1E9Cdifference", new sc_Pair(new sc_Pair("\u1E9Ctimes", new sc_Pair("\u1E9Cc", new sc_Pair("\u1E9Cx", new SC_NSNull()))), new sc_Pair(new sc_Pair("\u1E9Ctimes", new sc_Pair("\u1E9Cw", new sc_Pair("\u1E9Cx", new SC_NSNull()))), new SC_NSNull()))), new SC_NSNull()))), new sc_Pair("\u1E9Cequal", new sc_Pair(new sc_Pair("\u1E9Cremainder", new sc_Pair(new sc_Pair("\u1E9Ctimes", new sc_Pair("\u1E9Cx", new sc_Pair("\u1E9Cz", new SC_NSNull()))), new sc_Pair("\u1E9Cz", new SC_NSNull()))), new sc_Pair(new sc_Pair("\u1E9Czero", new SC_NSNull()), new SC_NSNull()))), new sc_Pair("\u1E9Cequal", new sc_Pair(new sc_Pair("\u1E9Cdifference", new sc_Pair(new sc_Pair("\u1E9Cplus", new sc_Pair("\u1E9Cb", new sc_Pair(new sc_Pair("\u1E9Cplus", new sc_Pair("\u1E9Ca", new sc_Pair("\u1E9Cc", new SC_NSNull()))), new SC_NSNull()))), new sc_Pair("\u1E9Ca", new SC_NSNull()))), new sc_Pair(new sc_Pair("\u1E9Cplus", new sc_Pair("\u1E9Cb", new sc_Pair("\u1E9Cc", new SC_NSNull()))), new SC_NSNull()))), new sc_Pair("\u1E9Cequal", new sc_Pair(new sc_Pair("\u1E9Cdifference", new sc_Pair(new sc_Pair("\u1E9Cadd1", new sc_Pair(new sc_Pair("\u1E9Cplus", new sc_Pair("\u1E9Cy", new sc_Pair("\u1E9Cz", new SC_NSNull()))), new SC_NSNull())), new sc_Pair("\u1E9Cz", new SC_NSNull()))), new sc_Pair(new sc_Pair("\u1E9Cadd1", new sc_Pair("\u1E9Cy", new SC_NSNull())), new SC_NSNull()))), new sc_Pair("\u1E9Cequal", new sc_Pair(new sc_Pair("\u1E9Clessp", new sc_Pair(new sc_Pair("\u1E9Cplus", new sc_Pair("\u1E9Cx", new sc_Pair("\u1E9Cy", new SC_NSNull()))), new sc_Pair(new sc_Pair("\u1E9Cplus", new sc_Pair("\u1E9Cx", new sc_Pair("\u1E9Cz", new SC_NSNull()))), new SC_NSNull()))), new sc_Pair(new sc_Pair("\u1E9Clessp", new sc_Pair("\u1E9Cy", new sc_Pair("\u1E9Cz", new SC_NSNull()))), new SC_NSNull()))), new sc_Pair("\u1E9Cequal", new sc_Pair(new sc_Pair("\u1E9Clessp", new sc_Pair(new sc_Pair("\u1E9Ctimes", new sc_Pair("\u1E9Cx", new sc_Pair("\u1E9Cz", new SC_NSNull()))), new sc_Pair(new sc_Pair("\u1E9Ctimes", new sc_Pair("\u1E9Cy", new sc_Pair("\u1E9Cz", new SC_NSNull()))), new SC_NSNull()))), new sc_Pair(new sc_Pair("\u1E9Cand", new sc_Pair(new sc_Pair("\u1E9Cnot", new sc_Pair(new sc_Pair("\u1E9Czerop", new sc_Pair("\u1E9Cz", new SC_NSNull())), new SC_NSNull())), new sc_Pair(new sc_Pair("\u1E9Clessp", new sc_Pair("\u1E9Cx", new sc_Pair("\u1E9Cy", new SC_NSNull()))), new SC_NSNull()))), new SC_NSNull()))), new sc_Pair("\u1E9Cequal", new sc_Pair(new sc_Pair("\u1E9Clessp", new sc_Pair("\u1E9Cy", new sc_Pair(new sc_Pair("\u1E9Cplus", new sc_Pair("\u1E9Cx", new sc_Pair("\u1E9Cy", new SC_NSNull()))), new SC_NSNull()))), new sc_Pair(new sc_Pair("\u1E9Cnot", new sc_Pair(new sc_Pair("\u1E9Czerop", new sc_Pair("\u1E9Cx", new SC_NSNull())), new SC_NSNull())), new SC_NSNull()))), new sc_Pair("\u1E9Cequal", new sc_Pair(new sc_Pair("\u1E9Cgcd", new sc_Pair(new sc_Pair("\u1E9Ctimes", new sc_Pair("\u1E9Cx", new sc_Pair("\u1E9Cz", new SC_NSNull()))), new sc_Pair(new sc_Pair("\u1E9Ctimes", new sc_Pair("\u1E9Cy", new sc_Pair("\u1E9Cz", new SC_NSNull()))), new SC_NSNull()))), new sc_Pair(new sc_Pair("\u1E9Ctimes", new sc_Pair("\u1E9Cz", new sc_Pair(new sc_Pair("\u1E9Cgcd", new sc_Pair("\u1E9Cx", new sc_Pair("\u1E9Cy", new SC_NSNull()))), new SC_NSNull()))), new SC_NSNull()))), new sc_Pair("\u1E9Cequal", new sc_Pair(new sc_Pair("\u1E9Cvalue", new sc_Pair(new sc_Pair("\u1E9Cnormalize", new sc_Pair("\u1E9Cx", new SC_NSNull())), new sc_Pair("\u1E9Ca", new SC_NSNull()))), new sc_Pair(new sc_Pair("\u1E9Cvalue", new sc_Pair("\u1E9Cx", new sc_Pair("\u1E9Ca", new SC_NSNull()))), new SC_NSNull()))), new sc_Pair("\u1E9Cequal", new sc_Pair(new sc_Pair("\u1E9Cequal", new sc_Pair(new sc_Pair("\u1E9Cflatten", new sc_Pair("\u1E9Cx", new SC_NSNull())), new sc_Pair(new sc_Pair("\u1E9Ccons", new sc_Pair("\u1E9Cy", new sc_Pair(new sc_Pair("\u1E9Cnil", new SC_NSNull()), new SC_NSNull()))), new SC_NSNull()))), new sc_Pair(new sc_Pair("\u1E9Cand", new sc_Pair(new sc_Pair("\u1E9Cnlistp", new sc_Pair("\u1E9Cx", new SC_NSNull())), new sc_Pair(new sc_Pair("\u1E9Cequal", new sc_Pair("\u1E9Cx", new sc_Pair("\u1E9Cy", new SC_NSNull()))), new SC_NSNull()))), new SC_NSNull()))), new sc_Pair("\u1E9Cequal", new sc_Pair(new sc_Pair("\u1E9Clistp", new sc_Pair(new sc_Pair("\u1E9Cgopher", new sc_Pair("\u1E9Cx", new SC_NSNull())), new SC_NSNull())), new sc_Pair(new sc_Pair("\u1E9Clistp", new sc_Pair("\u1E9Cx", new SC_NSNull())), new SC_NSNull()))), new sc_Pair("\u1E9Cequal", new sc_Pair(new sc_Pair("\u1E9Csamefringe", new sc_Pair("\u1E9Cx", new sc_Pair("\u1E9Cy", new SC_NSNull()))), new sc_Pair(new sc_Pair("\u1E9Cequal", new sc_Pair(new sc_Pair("\u1E9Cflatten", new sc_Pair("\u1E9Cx", new SC_NSNull())), new sc_Pair(new sc_Pair("\u1E9Cflatten", new sc_Pair("\u1E9Cy", new SC_NSNull())), new SC_NSNull()))), new SC_NSNull()))), new sc_Pair("\u1E9Cequal", new sc_Pair(new sc_Pair("\u1E9Cequal", new sc_Pair(new sc_Pair("\u1E9Cgreatest-factor", new sc_Pair("\u1E9Cx", new sc_Pair("\u1E9Cy", new SC_NSNull()))), new sc_Pair(new sc_Pair("\u1E9Czero", new SC_NSNull()), new SC_NSNull()))), new sc_Pair(new sc_Pair("\u1E9Cand", new sc_Pair(new sc_Pair("\u1E9Cor", new sc_Pair(new sc_Pair("\u1E9Czerop", new sc_Pair("\u1E9Cy", new SC_NSNull())), new sc_Pair(new sc_Pair("\u1E9Cequal", new sc_Pair("\u1E9Cy", new sc_Pair(1, new SC_NSNull()))), new SC_NSNull()))), new sc_Pair(new sc_Pair("\u1E9Cequal", new sc_Pair("\u1E9Cx", new sc_Pair(new sc_Pair("\u1E9Czero", new SC_NSNull()), new SC_NSNull()))), new SC_NSNull()))), new SC_NSNull()))), new sc_Pair("\u1E9Cequal", new sc_Pair(new sc_Pair("\u1E9Cequal", new sc_Pair(new sc_Pair("\u1E9Cgreatest-factor", new sc_Pair("\u1E9Cx", new sc_Pair("\u1E9Cy", new SC_NSNull()))), new sc_Pair(1, new SC_NSNull()))), new sc_Pair(new sc_Pair("\u1E9Cequal", new sc_Pair("\u1E9Cx", new sc_Pair(1, new SC_NSNull()))), new SC_NSNull()))), new sc_Pair("\u1E9Cequal", new sc_Pair(new sc_Pair("\u1E9Cnumberp", new sc_Pair(new sc_Pair("\u1E9Cgreatest-factor", new sc_Pair("\u1E9Cx", new sc_Pair("\u1E9Cy", new SC_NSNull()))), new SC_NSNull())), new sc_Pair(new sc_Pair("\u1E9Cnot", new sc_Pair(new sc_Pair("\u1E9Cand", new sc_Pair(new sc_Pair("\u1E9Cor", new sc_Pair(new sc_Pair("\u1E9Czerop", new sc_Pair("\u1E9Cy", new SC_NSNull())), new sc_Pair(new sc_Pair("\u1E9Cequal", new sc_Pair("\u1E9Cy", new sc_Pair(1, new SC_NSNull()))), new SC_NSNull()))), new sc_Pair(new sc_Pair("\u1E9Cnot", new sc_Pair(new sc_Pair("\u1E9Cnumberp", new sc_Pair("\u1E9Cx", new SC_NSNull())), new SC_NSNull())), new SC_NSNull()))), new SC_NSNull())), new SC_NSNull()))), new sc_Pair("\u1E9Cequal", new sc_Pair(new sc_Pair("\u1E9Ctimes-list", new sc_Pair(new sc_Pair("\u1E9Cappend", new sc_Pair("\u1E9Cx", new sc_Pair("\u1E9Cy", new SC_NSNull()))), new SC_NSNull())), new sc_Pair(new sc_Pair("\u1E9Ctimes", new sc_Pair(new sc_Pair("\u1E9Ctimes-list", new sc_Pair("\u1E9Cx", new SC_NSNull())), new sc_Pair(new sc_Pair("\u1E9Ctimes-list", new sc_Pair("\u1E9Cy", new SC_NSNull())), new SC_NSNull()))), new SC_NSNull()))), new sc_Pair("\u1E9Cequal", new sc_Pair(new sc_Pair("\u1E9Cprime-list", new sc_Pair(new sc_Pair("\u1E9Cappend", new sc_Pair("\u1E9Cx", new sc_Pair("\u1E9Cy", new SC_NSNull()))), new SC_NSNull())), new sc_Pair(new sc_Pair("\u1E9Cand", new sc_Pair(new sc_Pair("\u1E9Cprime-list", new sc_Pair("\u1E9Cx", new SC_NSNull())), new sc_Pair(new sc_Pair("\u1E9Cprime-list", new sc_Pair("\u1E9Cy", new SC_NSNull())), new SC_NSNull()))), new SC_NSNull()))), new sc_Pair("\u1E9Cequal", new sc_Pair(new sc_Pair("\u1E9Cequal", new sc_Pair("\u1E9Cz", new sc_Pair(new sc_Pair("\u1E9Ctimes", new sc_Pair("\u1E9Cw", new sc_Pair("\u1E9Cz", new SC_NSNull()))), new SC_NSNull()))), new sc_Pair(new sc_Pair("\u1E9Cand", new sc_Pair(new sc_Pair("\u1E9Cnumberp", new sc_Pair("\u1E9Cz", new SC_NSNull())), new sc_Pair(new sc_Pair("\u1E9Cor", new sc_Pair(new sc_Pair("\u1E9Cequal", new sc_Pair("\u1E9Cz", new sc_Pair(new sc_Pair("\u1E9Czero", new SC_NSNull()), new SC_NSNull()))), new sc_Pair(new sc_Pair("\u1E9Cequal", new sc_Pair("\u1E9Cw", new sc_Pair(1, new SC_NSNull()))), new SC_NSNull()))), new SC_NSNull()))), new SC_NSNull()))), new sc_Pair("\u1E9Cequal", new sc_Pair(new sc_Pair("\u1E9Cgreatereqp", new sc_Pair("\u1E9Cx", new sc_Pair("\u1E9Cy", new SC_NSNull()))), new sc_Pair(new sc_Pair("\u1E9Cnot", new sc_Pair(new sc_Pair("\u1E9Clessp", new sc_Pair("\u1E9Cx", new sc_Pair("\u1E9Cy", new SC_NSNull()))), new SC_NSNull())), new SC_NSNull()))), new sc_Pair("\u1E9Cequal", new sc_Pair(new sc_Pair("\u1E9Cequal", new sc_Pair("\u1E9Cx", new sc_Pair(new sc_Pair("\u1E9Ctimes", new sc_Pair("\u1E9Cx", new sc_Pair("\u1E9Cy", new SC_NSNull()))), new SC_NSNull()))), new sc_Pair(new sc_Pair("\u1E9Cor", new sc_Pair(new sc_Pair("\u1E9Cequal", new sc_Pair("\u1E9Cx", new sc_Pair(new sc_Pair("\u1E9Czero", new SC_NSNull()), new SC_NSNull()))), new sc_Pair(new sc_Pair("\u1E9Cand", new sc_Pair(new sc_Pair("\u1E9Cnumberp", new sc_Pair("\u1E9Cx", new SC_NSNull())), new sc_Pair(new sc_Pair("\u1E9Cequal", new sc_Pair("\u1E9Cy", new sc_Pair(1, new SC_NSNull()))), new SC_NSNull()))), new SC_NSNull()))), new SC_NSNull()))), new sc_Pair("\u1E9Cequal", new sc_Pair(new sc_Pair("\u1E9Cremainder", new sc_Pair(new sc_Pair("\u1E9Ctimes", new sc_Pair("\u1E9Cy", new sc_Pair("\u1E9Cx", new SC_NSNull()))), new sc_Pair("\u1E9Cy", new SC_NSNull()))), new sc_Pair(new sc_Pair("\u1E9Czero", new SC_NSNull()), new SC_NSNull()))), new sc_Pair("\u1E9Cequal", new sc_Pair(new sc_Pair("\u1E9Cequal", new sc_Pair(new sc_Pair("\u1E9Ctimes", new sc_Pair("\u1E9Ca", new sc_Pair("\u1E9Cb", new SC_NSNull()))), new sc_Pair(1, new SC_NSNull()))), new sc_Pair(sc_list("\u1E9Cand", new sc_Pair("\u1E9Cnot", new sc_Pair(new sc_Pair("\u1E9Cequal", new sc_Pair("\u1E9Ca", new sc_Pair(new sc_Pair("\u1E9Czero", new SC_NSNull()), new SC_NSNull()))), new SC_NSNull())), new sc_Pair("\u1E9Cnot", new sc_Pair(new sc_Pair("\u1E9Cequal", new sc_Pair("\u1E9Cb", new sc_Pair(new sc_Pair("\u1E9Czero", new SC_NSNull()), new SC_NSNull()))), new SC_NSNull())), new sc_Pair("\u1E9Cnumberp", new sc_Pair("\u1E9Ca", new SC_NSNull())), new sc_Pair("\u1E9Cnumberp", new sc_Pair("\u1E9Cb", new SC_NSNull())), new sc_Pair("\u1E9Cequal", new sc_Pair(new sc_Pair("\u1E9Csub1", new sc_Pair("\u1E9Ca", new SC_NSNull())), new sc_Pair(new sc_Pair("\u1E9Czero", new SC_NSNull()), new SC_NSNull()))), new sc_Pair("\u1E9Cequal", new sc_Pair(new sc_Pair("\u1E9Csub1", new sc_Pair("\u1E9Cb", new SC_NSNull())), new sc_Pair(new sc_Pair("\u1E9Czero", new SC_NSNull()), new SC_NSNull())))), new SC_NSNull()))), new sc_Pair("\u1E9Cequal", new sc_Pair(new sc_Pair("\u1E9Clessp", new sc_Pair(new sc_Pair("\u1E9Clength", new sc_Pair(new sc_Pair("\u1E9Cdelete", new sc_Pair("\u1E9Cx", new sc_Pair("\u1E9Cl", new SC_NSNull()))), new SC_NSNull())), new sc_Pair(new sc_Pair("\u1E9Clength", new sc_Pair("\u1E9Cl", new SC_NSNull())), new SC_NSNull()))), new sc_Pair(new sc_Pair("\u1E9Cmember", new sc_Pair("\u1E9Cx", new sc_Pair("\u1E9Cl", new SC_NSNull()))), new SC_NSNull()))), new sc_Pair("\u1E9Cequal", new sc_Pair(new sc_Pair("\u1E9Csort2", new sc_Pair(new sc_Pair("\u1E9Cdelete", new sc_Pair("\u1E9Cx", new sc_Pair("\u1E9Cl", new SC_NSNull()))), new SC_NSNull())), new sc_Pair(new sc_Pair("\u1E9Cdelete", new sc_Pair("\u1E9Cx", new sc_Pair(new sc_Pair("\u1E9Csort2", new sc_Pair("\u1E9Cl", new SC_NSNull())), new SC_NSNull()))), new SC_NSNull()))), new sc_Pair("\u1E9Cequal", new sc_Pair(new sc_Pair("\u1E9Cdsort", new sc_Pair("\u1E9Cx", new SC_NSNull())), new sc_Pair(new sc_Pair("\u1E9Csort2", new sc_Pair("\u1E9Cx", new SC_NSNull())), new SC_NSNull()))), new sc_Pair("\u1E9Cequal", new sc_Pair(new sc_Pair("\u1E9Clength", new sc_Pair(new sc_Pair("\u1E9Ccons", new sc_Pair("\u1E9Cx1", new sc_Pair(new sc_Pair("\u1E9Ccons", new sc_Pair("\u1E9Cx2", new sc_Pair(new sc_Pair("\u1E9Ccons", new sc_Pair("\u1E9Cx3", new sc_Pair(new sc_Pair("\u1E9Ccons", new sc_Pair("\u1E9Cx4", new sc_Pair(new sc_Pair("\u1E9Ccons", new sc_Pair("\u1E9Cx5", new sc_Pair(new sc_Pair("\u1E9Ccons", new sc_Pair("\u1E9Cx6", new sc_Pair("\u1E9Cx7", new SC_NSNull()))), new SC_NSNull()))), new SC_NSNull()))), new SC_NSNull()))), new SC_NSNull()))), new SC_NSNull()))), new SC_NSNull())), new sc_Pair(new sc_Pair("\u1E9Cplus", new sc_Pair(6, new sc_Pair(new sc_Pair("\u1E9Clength", new sc_Pair("\u1E9Cx7", new SC_NSNull())), new SC_NSNull()))), new SC_NSNull()))), new sc_Pair("\u1E9Cequal", new sc_Pair(new sc_Pair("\u1E9Cdifference", new sc_Pair(new sc_Pair("\u1E9Cadd1", new sc_Pair(new sc_Pair("\u1E9Cadd1", new sc_Pair("\u1E9Cx", new SC_NSNull())), new SC_NSNull())), new sc_Pair(2, new SC_NSNull()))), new sc_Pair(new sc_Pair("\u1E9Cfix", new sc_Pair("\u1E9Cx", new SC_NSNull())), new SC_NSNull()))), new sc_Pair("\u1E9Cequal", new sc_Pair(new sc_Pair("\u1E9Cquotient", new sc_Pair(new sc_Pair("\u1E9Cplus", new sc_Pair("\u1E9Cx", new sc_Pair(new sc_Pair("\u1E9Cplus", new sc_Pair("\u1E9Cx", new sc_Pair("\u1E9Cy", new SC_NSNull()))), new SC_NSNull()))), new sc_Pair(2, new SC_NSNull()))), new sc_Pair(new sc_Pair("\u1E9Cplus", new sc_Pair("\u1E9Cx", new sc_Pair(new sc_Pair("\u1E9Cquotient", new sc_Pair("\u1E9Cy", new sc_Pair(2, new SC_NSNull()))), new SC_NSNull()))), new SC_NSNull()))), new sc_Pair("\u1E9Cequal", new sc_Pair(new sc_Pair("\u1E9Csigma", new sc_Pair(new sc_Pair("\u1E9Czero", new SC_NSNull()), new sc_Pair("\u1E9Ci", new SC_NSNull()))), new sc_Pair(new sc_Pair("\u1E9Cquotient", new sc_Pair(new sc_Pair("\u1E9Ctimes", new sc_Pair("\u1E9Ci", new sc_Pair(new sc_Pair("\u1E9Cadd1", new sc_Pair("\u1E9Ci", new SC_NSNull())), new SC_NSNull()))), new sc_Pair(2, new SC_NSNull()))), new SC_NSNull()))), new sc_Pair("\u1E9Cequal", new sc_Pair(new sc_Pair("\u1E9Cplus", new sc_Pair("\u1E9Cx", new sc_Pair(new sc_Pair("\u1E9Cadd1", new sc_Pair("\u1E9Cy", new SC_NSNull())), new SC_NSNull()))), new sc_Pair(new sc_Pair("\u1E9Cif", new sc_Pair(new sc_Pair("\u1E9Cnumberp", new sc_Pair("\u1E9Cy", new SC_NSNull())), new sc_Pair(new sc_Pair("\u1E9Cadd1", new sc_Pair(new sc_Pair("\u1E9Cplus", new sc_Pair("\u1E9Cx", new sc_Pair("\u1E9Cy", new SC_NSNull()))), new SC_NSNull())), new sc_Pair(new sc_Pair("\u1E9Cadd1", new sc_Pair("\u1E9Cx", new SC_NSNull())), new SC_NSNull())))), new SC_NSNull()))), new sc_Pair("\u1E9Cequal", new sc_Pair(new sc_Pair("\u1E9Cequal", new sc_Pair(new sc_Pair("\u1E9Cdifference", new sc_Pair("\u1E9Cx", new sc_Pair("\u1E9Cy", new SC_NSNull()))), new sc_Pair(new sc_Pair("\u1E9Cdifference", new sc_Pair("\u1E9Cz", new sc_Pair("\u1E9Cy", new SC_NSNull()))), new SC_NSNull()))), new sc_Pair(new sc_Pair("\u1E9Cif", new sc_Pair(new sc_Pair("\u1E9Clessp", new sc_Pair("\u1E9Cx", new sc_Pair("\u1E9Cy", new SC_NSNull()))), new sc_Pair(new sc_Pair("\u1E9Cnot", new sc_Pair(new sc_Pair("\u1E9Clessp", new sc_Pair("\u1E9Cy", new sc_Pair("\u1E9Cz", new SC_NSNull()))), new SC_NSNull())), new sc_Pair(new sc_Pair("\u1E9Cif", new sc_Pair(new sc_Pair("\u1E9Clessp", new sc_Pair("\u1E9Cz", new sc_Pair("\u1E9Cy", new SC_NSNull()))), new sc_Pair(new sc_Pair("\u1E9Cnot", new sc_Pair(new sc_Pair("\u1E9Clessp", new sc_Pair("\u1E9Cy", new sc_Pair("\u1E9Cx", new SC_NSNull()))), new SC_NSNull())), new sc_Pair(new sc_Pair("\u1E9Cequal", new sc_Pair(new sc_Pair("\u1E9Cfix", new sc_Pair("\u1E9Cx", new SC_NSNull())), new sc_Pair(new sc_Pair("\u1E9Cfix", new sc_Pair("\u1E9Cz", new SC_NSNull())), new SC_NSNull()))), new SC_NSNull())))), new SC_NSNull())))), new SC_NSNull()))), new sc_Pair("\u1E9Cequal", new sc_Pair(new sc_Pair("\u1E9Cmeaning", new sc_Pair(new sc_Pair("\u1E9Cplus-tree", new sc_Pair(new sc_Pair("\u1E9Cdelete", new sc_Pair("\u1E9Cx", new sc_Pair("\u1E9Cy", new SC_NSNull()))), new SC_NSNull())), new sc_Pair("\u1E9Ca", new SC_NSNull()))), new sc_Pair(new sc_Pair("\u1E9Cif", new sc_Pair(new sc_Pair("\u1E9Cmember", new sc_Pair("\u1E9Cx", new sc_Pair("\u1E9Cy", new SC_NSNull()))), new sc_Pair(new sc_Pair("\u1E9Cdifference", new sc_Pair(new sc_Pair("\u1E9Cmeaning", new sc_Pair(new sc_Pair("\u1E9Cplus-tree", new sc_Pair("\u1E9Cy", new SC_NSNull())), new sc_Pair("\u1E9Ca", new SC_NSNull()))), new sc_Pair(new sc_Pair("\u1E9Cmeaning", new sc_Pair("\u1E9Cx", new sc_Pair("\u1E9Ca", new SC_NSNull()))), new SC_NSNull()))), new sc_Pair(new sc_Pair("\u1E9Cmeaning", new sc_Pair(new sc_Pair("\u1E9Cplus-tree", new sc_Pair("\u1E9Cy", new SC_NSNull())), new sc_Pair("\u1E9Ca", new SC_NSNull()))), new SC_NSNull())))), new SC_NSNull()))), new sc_Pair("\u1E9Cequal", new sc_Pair(new sc_Pair("\u1E9Ctimes", new sc_Pair("\u1E9Cx", new sc_Pair(new sc_Pair("\u1E9Cadd1", new sc_Pair("\u1E9Cy", new SC_NSNull())), new SC_NSNull()))), new sc_Pair(new sc_Pair("\u1E9Cif", new sc_Pair(new sc_Pair("\u1E9Cnumberp", new sc_Pair("\u1E9Cy", new SC_NSNull())), new sc_Pair(new sc_Pair("\u1E9Cplus", new sc_Pair("\u1E9Cx", new sc_Pair(new sc_Pair("\u1E9Ctimes", new sc_Pair("\u1E9Cx", new sc_Pair("\u1E9Cy", new SC_NSNull()))), new SC_NSNull()))), new sc_Pair(new sc_Pair("\u1E9Cfix", new sc_Pair("\u1E9Cx", new SC_NSNull())), new SC_NSNull())))), new SC_NSNull()))), new sc_Pair("\u1E9Cequal", new sc_Pair(new sc_Pair("\u1E9Cnth", new sc_Pair(new sc_Pair("\u1E9Cnil", new SC_NSNull()), new sc_Pair("\u1E9Ci", new SC_NSNull()))), new sc_Pair(new sc_Pair("\u1E9Cif", new sc_Pair(new sc_Pair("\u1E9Czerop", new sc_Pair("\u1E9Ci", new SC_NSNull())), new sc_Pair(new sc_Pair("\u1E9Cnil", new SC_NSNull()), new sc_Pair(new sc_Pair("\u1E9Czero", new SC_NSNull()), new SC_NSNull())))), new SC_NSNull()))), new sc_Pair("\u1E9Cequal", new sc_Pair(new sc_Pair("\u1E9Clast", new sc_Pair(new sc_Pair("\u1E9Cappend", new sc_Pair("\u1E9Ca", new sc_Pair("\u1E9Cb", new SC_NSNull()))), new SC_NSNull())), new sc_Pair(new sc_Pair("\u1E9Cif", new sc_Pair(new sc_Pair("\u1E9Clistp", new sc_Pair("\u1E9Cb", new SC_NSNull())), new sc_Pair(new sc_Pair("\u1E9Clast", new sc_Pair("\u1E9Cb", new SC_NSNull())), new sc_Pair(new sc_Pair("\u1E9Cif", new sc_Pair(new sc_Pair("\u1E9Clistp", new sc_Pair("\u1E9Ca", new SC_NSNull())), new sc_Pair(new sc_Pair("\u1E9Ccons", new sc_Pair(new sc_Pair("\u1E9Ccar", new sc_Pair(new sc_Pair("\u1E9Clast", new sc_Pair("\u1E9Ca", new SC_NSNull())), new SC_NSNull())), new sc_Pair("\u1E9Cb", new SC_NSNull()))), new sc_Pair("\u1E9Cb", new SC_NSNull())))), new SC_NSNull())))), new SC_NSNull()))), new sc_Pair("\u1E9Cequal", new sc_Pair(new sc_Pair("\u1E9Cequal", new sc_Pair(new sc_Pair("\u1E9Clessp", new sc_Pair("\u1E9Cx", new sc_Pair("\u1E9Cy", new SC_NSNull()))), new sc_Pair("\u1E9Cz", new SC_NSNull()))), new sc_Pair(new sc_Pair("\u1E9Cif", new sc_Pair(new sc_Pair("\u1E9Clessp", new sc_Pair("\u1E9Cx", new sc_Pair("\u1E9Cy", new SC_NSNull()))), new sc_Pair(new sc_Pair("\u1E9Cequal", new sc_Pair(new sc_Pair("\u1E9Ct", new SC_NSNull()), new sc_Pair("\u1E9Cz", new SC_NSNull()))), new sc_Pair(new sc_Pair("\u1E9Cequal", new sc_Pair(new sc_Pair("\u1E9Cf", new SC_NSNull()), new sc_Pair("\u1E9Cz", new SC_NSNull()))), new SC_NSNull())))), new SC_NSNull()))), new sc_Pair("\u1E9Cequal", new sc_Pair(new sc_Pair("\u1E9Cassignment", new sc_Pair("\u1E9Cx", new sc_Pair(new sc_Pair("\u1E9Cappend", new sc_Pair("\u1E9Ca", new sc_Pair("\u1E9Cb", new SC_NSNull()))), new SC_NSNull()))), new sc_Pair(new sc_Pair("\u1E9Cif", new sc_Pair(new sc_Pair("\u1E9Cassignedp", new sc_Pair("\u1E9Cx", new sc_Pair("\u1E9Ca", new SC_NSNull()))), new sc_Pair(new sc_Pair("\u1E9Cassignment", new sc_Pair("\u1E9Cx", new sc_Pair("\u1E9Ca", new SC_NSNull()))), new sc_Pair(new sc_Pair("\u1E9Cassignment", new sc_Pair("\u1E9Cx", new sc_Pair("\u1E9Cb", new SC_NSNull()))), new SC_NSNull())))), new SC_NSNull()))), new sc_Pair("\u1E9Cequal", new sc_Pair(new sc_Pair("\u1E9Ccar", new sc_Pair(new sc_Pair("\u1E9Cgopher", new sc_Pair("\u1E9Cx", new SC_NSNull())), new SC_NSNull())), new sc_Pair(new sc_Pair("\u1E9Cif", new sc_Pair(new sc_Pair("\u1E9Clistp", new sc_Pair("\u1E9Cx", new SC_NSNull())), new sc_Pair(new sc_Pair("\u1E9Ccar", new sc_Pair(new sc_Pair("\u1E9Cflatten", new sc_Pair("\u1E9Cx", new SC_NSNull())), new SC_NSNull())), new sc_Pair(new sc_Pair("\u1E9Czero", new SC_NSNull()), new SC_NSNull())))), new SC_NSNull()))), new sc_Pair("\u1E9Cequal", new sc_Pair(new sc_Pair("\u1E9Cflatten", new sc_Pair(new sc_Pair("\u1E9Ccdr", new sc_Pair(new sc_Pair("\u1E9Cgopher", new sc_Pair("\u1E9Cx", new SC_NSNull())), new SC_NSNull())), new SC_NSNull())), new sc_Pair(new sc_Pair("\u1E9Cif", new sc_Pair(new sc_Pair("\u1E9Clistp", new sc_Pair("\u1E9Cx", new SC_NSNull())), new sc_Pair(new sc_Pair("\u1E9Ccdr", new sc_Pair(new sc_Pair("\u1E9Cflatten", new sc_Pair("\u1E9Cx", new SC_NSNull())), new SC_NSNull())), new sc_Pair(new sc_Pair("\u1E9Ccons", new sc_Pair(new sc_Pair("\u1E9Czero", new SC_NSNull()), new sc_Pair(new sc_Pair("\u1E9Cnil", new SC_NSNull()), new SC_NSNull()))), new SC_NSNull())))), new SC_NSNull()))), new sc_Pair("\u1E9Cequal", new sc_Pair(new sc_Pair("\u1E9Cquotient", new sc_Pair(new sc_Pair("\u1E9Ctimes", new sc_Pair("\u1E9Cy", new sc_Pair("\u1E9Cx", new SC_NSNull()))), new sc_Pair("\u1E9Cy", new SC_NSNull()))), new sc_Pair(new sc_Pair("\u1E9Cif", new sc_Pair(new sc_Pair("\u1E9Czerop", new sc_Pair("\u1E9Cy", new SC_NSNull())), new sc_Pair(new sc_Pair("\u1E9Czero", new SC_NSNull()), new sc_Pair(new sc_Pair("\u1E9Cfix", new sc_Pair("\u1E9Cx", new SC_NSNull())), new SC_NSNull())))), new SC_NSNull()))), new sc_Pair("\u1E9Cequal", new sc_Pair(new sc_Pair("\u1E9Cget", new sc_Pair("\u1E9Cj", new sc_Pair(new sc_Pair("\u1E9Cset", new sc_Pair("\u1E9Ci", new sc_Pair("\u1E9Cval", new sc_Pair("\u1E9Cmem", new SC_NSNull())))), new SC_NSNull()))), new sc_Pair(new sc_Pair("\u1E9Cif", new sc_Pair(new sc_Pair("\u1E9Ceqp", new sc_Pair("\u1E9Cj", new sc_Pair("\u1E9Ci", new SC_NSNull()))), new sc_Pair("\u1E9Cval", new sc_Pair(new sc_Pair("\u1E9Cget", new sc_Pair("\u1E9Cj", new sc_Pair("\u1E9Cmem", new SC_NSNull()))), new SC_NSNull())))), new SC_NSNull()))));
let const_nboyer = new sc_Pair(new sc_Pair("\u1E9Cx", new sc_Pair("\u1E9Cf", new sc_Pair(new sc_Pair("\u1E9Cplus", new sc_Pair(new sc_Pair("\u1E9Cplus", new sc_Pair("\u1E9Ca", new sc_Pair("\u1E9Cb", new SC_NSNull()))), new sc_Pair(new sc_Pair("\u1E9Cplus", new sc_Pair("\u1E9Cc", new sc_Pair(new sc_Pair("\u1E9Czero", new SC_NSNull()), new SC_NSNull()))), new SC_NSNull()))), new SC_NSNull()))), new sc_Pair(new sc_Pair("\u1E9Cy", new sc_Pair("\u1E9Cf", new sc_Pair(new sc_Pair("\u1E9Ctimes", new sc_Pair(new sc_Pair("\u1E9Ctimes", new sc_Pair("\u1E9Ca", new sc_Pair("\u1E9Cb", new SC_NSNull()))), new sc_Pair(new sc_Pair("\u1E9Cplus", new sc_Pair("\u1E9Cc", new sc_Pair("\u1E9Cd", new SC_NSNull()))), new SC_NSNull()))), new SC_NSNull()))), new sc_Pair(new sc_Pair("\u1E9Cz", new sc_Pair("\u1E9Cf", new sc_Pair(new sc_Pair("\u1E9Creverse", new sc_Pair(new sc_Pair("\u1E9Cappend", new sc_Pair(new sc_Pair("\u1E9Cappend", new sc_Pair("\u1E9Ca", new sc_Pair("\u1E9Cb", new SC_NSNull()))), new sc_Pair(new sc_Pair("\u1E9Cnil", new SC_NSNull()), new SC_NSNull()))), new SC_NSNull())), new SC_NSNull()))), new sc_Pair(new sc_Pair("\u1E9Cu", new sc_Pair("\u1E9Cequal", new sc_Pair(new sc_Pair("\u1E9Cplus", new sc_Pair("\u1E9Ca", new sc_Pair("\u1E9Cb", new SC_NSNull()))), new sc_Pair(new sc_Pair("\u1E9Cdifference", new sc_Pair("\u1E9Cx", new sc_Pair("\u1E9Cy", new SC_NSNull()))), new SC_NSNull())))), new sc_Pair(new sc_Pair("\u1E9Cw", new sc_Pair("\u1E9Clessp", new sc_Pair(new sc_Pair("\u1E9Cremainder", new sc_Pair("\u1E9Ca", new sc_Pair("\u1E9Cb", new SC_NSNull()))), new sc_Pair(new sc_Pair("\u1E9Cmember", new sc_Pair("\u1E9Ca", new sc_Pair(new sc_Pair("\u1E9Clength", new sc_Pair("\u1E9Cb", new SC_NSNull())), new SC_NSNull()))), new SC_NSNull())))), new SC_NSNull())))));
let BgL_sc_za2symbolzd2recordszd2alistza2_2z00_nboyer;
let if_constructor_nboyer = "\u1E9C*";
let false_term_nboyer;
let true_term_nboyer;
let rewrite_count_nboyer = 0;
let unify_subst_nboyer = "\u1E9C*";
let sc_number2string = sc_number2jsstring;
/**
 boyer
 */
function apply_subst_lst_nboyer(alist, lst) {
    let sc_lst_7;
    if (lst instanceof SC_NSNull) {
        return new SC_NSNull();
    }
    sc_lst_7 = lst.cdr;
    if (sc_lst_7 instanceof SC_NSNull) {
        return new sc_Pair(apply_subst_nboyer(alist, lst.car), new SC_NSNull());
    }
    else {
        return new sc_Pair(apply_subst_nboyer(alist, lst.car), new sc_Pair(apply_subst_nboyer(alist, sc_lst_7.car), apply_subst_lst_nboyer(alist, sc_lst_7.cdr)));
    }
}
function rewrite_args_nboyer(lst) {
    let sc_lst_14;
    if (lst instanceof SC_NSNull) {
        return new SC_NSNull();
    }
    let ran_car = rewrite_nboyer(lst.car);
    sc_lst_14 = lst.cdr;
    if (sc_lst_14 instanceof SC_NSNull) {
        return new sc_Pair(ran_car, new SC_NSNull());
    }
    return new sc_Pair(ran_car, new sc_Pair(rewrite_nboyer(sc_lst_14.car), rewrite_args_nboyer(sc_lst_14.cdr)));
}
function translate_alist_nboyer(alist) {
    //debugLog('This is translate_alist_nboyer');
    let sc_alist_6;
    let term;
    let car_value;
    if (alist instanceof SC_NSNull) {
        return new SC_NSNull();
    }
    term = alist.car.cdr;
    sc_alist_6 = alist.cdr;
    if (!(term instanceof sc_Pair)) {
        car_value = new sc_Pair(alist.car.car, term);
    }
    else {
        car_value = new sc_Pair(alist.car.car, new sc_Pair(BgL_sc_symbolzd2ze3symbolzd2record_1ze3_nboyer(term.car), translate_args_nboyer(term.cdr)));
    }
    if (sc_alist_6 instanceof SC_NSNull) {
        return new sc_Pair(car_value, new SC_NSNull());
    }
    else {
        return new sc_Pair(car_value, new sc_Pair(new sc_Pair(sc_alist_6.car.car, translate_term_nboyer(sc_alist_6.car.cdr)), translate_alist_nboyer(sc_alist_6.cdr)));
    }
}
function one_way_unify1_nboyer(term1, term2) {
    let lst1;
    let lst2;
    let temp_temp;
    if (!(term2 instanceof sc_Pair)) {
        temp_temp = sc_assq(typeof term2 === "string" ? term2 : String(term2), unify_subst_nboyer);
        if (temp_temp !== false) {
            return is_term_equal_nboyer(term1, temp_temp.cdr);
        }
        else if (sc_isNumber(term2)) {
            return sc_isEqual(term1, term2);
        }
        else {
            unify_subst_nboyer = new sc_Pair(new sc_Pair(term2, term1), unify_subst_nboyer);
            return true;
        }
    }
    else if (!(term1 instanceof sc_Pair)) {
        return false;
    }
    else if (term1.car === term2.car) {
        lst1 = term1.cdr;
        lst2 = term2.cdr;
        while (true) {
            if (lst1 instanceof SC_NSNull) {
                return lst2 instanceof SC_NSNull;
            }
            else if (lst2 instanceof SC_NSNull) {
                return false;
            }
            else if (one_way_unify1_nboyer(lst1.car, lst2.car) !==
                false) {
                lst1 = lst1.cdr;
                lst2 = lst2.cdr;
            }
            else {
                return false;
            }
        }
    }
    else {
        return false;
    }
}
function rewrite_nboyer(term) {
    let term2;
    let sc_term_12;
    let lst;
    let symbol_record;
    let sc_lst_13;
    rewrite_count_nboyer = rewrite_count_nboyer + 1;
    if (!(term instanceof sc_Pair)) {
        return term;
    }
    else {
        sc_lst_13 = term.cdr;
        if (sc_lst_13 instanceof SC_NSNull) {
            sc_term_12 = new sc_Pair(term.car, new SC_NSNull());
        }
        else {
            sc_term_12 = new sc_Pair(term.car, new sc_Pair(rewrite_nboyer(sc_lst_13.car), rewrite_args_nboyer(sc_lst_13.cdr)));
        }
        symbol_record = term.car;
        lst = symbol_record.arr[1];
        while (true) {
            if (lst instanceof SC_NSNull) {
                return sc_term_12;
            }
            else {
                term2 = lst.car.cdr.car;
                unify_subst_nboyer = new SC_NSNull();
                if (one_way_unify1_nboyer(sc_term_12, term2) !== false) {
                    return rewrite_nboyer(apply_subst_nboyer(unify_subst_nboyer, lst.car.cdr
                        .cdr.car));
                }
                else {
                    lst = lst.cdr;
                }
            }
        }
    }
}
function tautologyp_nboyer(sc_x_1, true_lst, false_lst_a) {
    //debugLog('This is tautologyp_nboyer')
    let tmp1125;
    let x;
    let tmp1126;
    let sc_x_8;
    let sc_tmp1125_9;
    let sc_tmp1126_10;
    let sc_x_11;
    let false_lst;
    sc_x_11 = sc_x_1;
    false_lst = false_lst_a;
    while (true) {
        sc_tmp1126_10 = is_term_equal_nboyer(sc_x_11, true_term_nboyer);
        sc_tmp1125_9 = is_term_equal_nboyer(sc_x_11, false_term_nboyer);
        if ((sc_tmp1126_10 != false
            ? sc_tmp1126_10
            : is_term_member_nboyer(sc_x_11, true_lst)) != false) {
            return true;
        }
        else if ((sc_tmp1125_9 != false
            ? sc_tmp1125_9
            : is_term_member_nboyer(sc_x_11, false_lst)) != false) {
            return false;
        }
        else if (!(sc_x_11 instanceof sc_Pair)) {
            return false;
        }
        else if (sc_x_11.car === if_constructor_nboyer) {
            sc_x_8 = sc_x_11.cdr.car;
            tmp1126 = is_term_equal_nboyer(sc_x_8, true_term_nboyer);
            x = sc_x_11.cdr.car;
            tmp1125 = is_term_equal_nboyer(x, false_term_nboyer);
            if ((tmp1126 != false
                ? tmp1126
                : is_term_member_nboyer(sc_x_8, true_lst)) != false) {
                sc_x_11 = sc_x_11.cdr.cdr.car;
            }
            else if ((tmp1125 != false ? tmp1125 : is_term_member_nboyer(x, false_lst)) !=
                false) {
                sc_x_11 = sc_x_11.cdr.cdr.cdr.car;
            }
            else if (tautologyp_nboyer(sc_x_11.cdr.cdr.car, new sc_Pair(sc_x_11.cdr.car, true_lst), false_lst) !== false) {
                false_lst = new sc_Pair(sc_x_11.cdr.car, false_lst);
                sc_x_11 = sc_x_11.cdr.cdr.cdr.car;
            }
            else {
                return false;
            }
        }
        else {
            return false;
        }
    }
}
function BgL_setupzd2boyerzd2() {
    //debugLog('This is BgL_setupzd2boyerzd2')
    let symbol_record;
    let value;
    let BgL_sc_symbolzd2record_16zd2;
    let sym;
    let sc_sym_17;
    let term;
    let lst;
    let sc_term_18;
    let sc_term_19;
    let car_value;
    BgL_sc_za2symbolzd2recordszd2alistza2_2z00_nboyer = new SC_NSNull();
    if_constructor_nboyer =
        BgL_sc_symbolzd2ze3symbolzd2record_1ze3_nboyer("\u1E9Cif");
    sc_term_19 = new sc_Pair("\u1E9Cf", new SC_NSNull());
    false_term_nboyer = new sc_Pair(BgL_sc_symbolzd2ze3symbolzd2record_1ze3_nboyer(sc_term_19.car), translate_args_nboyer(sc_term_19.cdr));
    sc_term_18 = new sc_Pair("\u1E9Ct", new SC_NSNull());
    true_term_nboyer = new sc_Pair(BgL_sc_symbolzd2ze3symbolzd2record_1ze3_nboyer(sc_term_18.car), translate_args_nboyer(sc_term_18.cdr));
    lst = sc_const_3_nboyer;
    while (!(lst instanceof SC_NSNull)) {
        term = lst.car;
        if (term instanceof sc_Pair &&
            term.car === "\u1E9Cequal" &&
            term.cdr.car instanceof sc_Pair) {
            sc_sym_17 = term.cdr.car
                .car;
            car_value = new sc_Pair(BgL_sc_symbolzd2ze3symbolzd2record_1ze3_nboyer(term.car), translate_args_nboyer(term.cdr));
            sym = term.cdr.car.car;
            BgL_sc_symbolzd2record_16zd2 =
                BgL_sc_symbolzd2ze3symbolzd2record_1ze3_nboyer(sym);
            value = new sc_Pair(car_value, BgL_sc_symbolzd2record_16zd2.arr[1]);
            symbol_record = BgL_sc_symbolzd2ze3symbolzd2record_1ze3_nboyer(sc_sym_17);
            symbol_record.arr[1] = value;
        }
        else {
            return false;
        }
        lst = lst.cdr;
    }
    return true;
}
function BgL_testzd2boyerzd2(n) {
    //debugLog('This is BgL_testzd2boyerzd2')
    let optrOpnd;
    let term;
    let sc_n_20;
    let answer;
    let sc_term_21;
    let sc_term_22;
    rewrite_count_nboyer = 0;
    term = sc_const_4_nboyer;
    sc_n_20 = n;
    while (sc_n_20 !== 0) {
        term = sc_list("\u{1E9C}", term, new sc_Pair("\u{1E9C}f", new SC_NSNull()));
        sc_n_20 -= 1;
    }
    sc_term_22 = term;
    if (!(sc_term_22 instanceof sc_Pair)) {
        optrOpnd = sc_term_22;
    }
    else {
        optrOpnd = new sc_Pair(BgL_sc_symbolzd2ze3symbolzd2record_1ze3_nboyer(sc_term_22.car), translate_args_nboyer(sc_term_22.cdr));
    }
    if (const_nboyer instanceof SC_NSNull) {
        sc_term_21 = apply_subst_nboyer(new SC_NSNull(), optrOpnd);
    }
    else {
        sc_term_21 = apply_subst_nboyer(new sc_Pair(new sc_Pair(const_nboyer.car.car, translate_term_nboyer(const_nboyer.car.cdr)), translate_alist_nboyer(const_nboyer.cdr)), optrOpnd);
    }
    answer = tautologyp_nboyer(rewrite_nboyer(sc_term_21), new SC_NSNull(), new SC_NSNull());
    sc_write(rewrite_count_nboyer, new SC_NSNull());
    sc_display(" rewrites", new SC_NSNull());
    if (answer !== false) {
        return rewrite_count_nboyer;
    }
    else {
        return false;
    }
}
function warn(rewrites) {
    let rewrites_value;
    if (sc_isNumber(rewrites)) {
        rewrites_value = rewrites;
        if (rewrites_value == 95024) {
            return true;
        }
        else {
            return false;
        }
    }
    else {
        return false;
    }
}
function run() {
    return BgL_testzd2boyerzd2(0);
}
function BgL_nboyerzd2benchmarkzd2(...argumentsAry) {
    //debugLog("This is BgL_nboyerzd2benchmarkzd2");
    let args;
    let sc_tmp = argumentsAry.length - 1;
    while (sc_tmp >= 0) {
        args = sc_cons(argumentsAry[sc_tmp], args !== null && args !== void 0 ? args : new SC_NSNull());
        sc_tmp -= 1;
    }
    let n = args === undefined ? 0 : args.car;
    BgL_setupzd2boyerzd2();
    return BgL_runzd2benchmarkzd2("nboyer" + sc_number2string(n), 1, run, warn);
}
/**
 earley
 */
function BgL_makezd2parserzd2(grammar, lexer) {
    //debugLog('This is BgL_makezd2parserzd2')
    let i;
    let parser_descr;
    let def_loop;
    let nb_nts;
    let names;
    let steps;
    let predictors;
    let enders;
    let starters;
    let nts;
    let sc_names_1;
    let sc_steps_2;
    let sc_predictors_3;
    let sc_enders_4;
    let sc_starters_5;
    let nb_confs;
    let BgL_sc_defzd2loop_6zd2;
    let BgL_sc_nbzd2nts_7zd2;
    let sc_nts_8;
    let BgL_sc_defzd2loop_9zd2;
    BgL_sc_defzd2loop_9zd2 = (defs, sc_nts_11) => {
        //debugLog('This is BgL_sc_defzd2loop_9zd2')
        let rule_loop;
        let head;
        let def;
        if (defs instanceof sc_Pair) {
            def = defs.car;
            head = def.car;
            rule_loop = (rules, sc_nts_12) => {
                let nt;
                let l;
                let sc_nts_13;
                let rule;
                if (rules instanceof sc_Pair) {
                    rule = rules.car;
                    l = rule;
                    sc_nts_13 = sc_nts_12;
                    while (l instanceof sc_Pair) {
                        nt = l.car;
                        l = l.cdr;
                        sc_nts_13 =
                            sc_member(nt, sc_nts_13) !== false
                                ? sc_nts_13
                                : new sc_Pair(nt, sc_nts_13);
                    }
                    return rule_loop(rules.cdr, sc_nts_13);
                }
                else {
                    return BgL_sc_defzd2loop_9zd2(defs.cdr, sc_nts_12);
                }
            };
            return rule_loop(def.cdr, sc_member(head, sc_nts_11) !== false
                ? sc_nts_11
                : new sc_Pair(head, sc_nts_11));
        }
        else {
            return sc_list2vector(sc_reverse(sc_nts_11)).arr;
        }
    };
    sc_nts_8 = BgL_sc_defzd2loop_9zd2(grammar, new SC_NSNull());
    BgL_sc_nbzd2nts_7zd2 = sc_nts_8.length;
    BgL_sc_defzd2loop_6zd2 = (defs, BgL_sc_nbzd2confs_14zd2) => {
        //debugLog("This is BgL_sc_defzd2loop_6zd2")
        let rule_loop;
        let def;
        if (defs instanceof sc_Pair) {
            def = defs.car;
            rule_loop = (rules, BgL_sc_nbzd2confs_15zd2) => {
                let l;
                let BgL_sc_nbzd2confs_16zd2;
                let rule;
                if (rules instanceof sc_Pair) {
                    rule = rules.car;
                    l = rule;
                    BgL_sc_nbzd2confs_16zd2 = BgL_sc_nbzd2confs_15zd2;
                    while (l instanceof sc_Pair) {
                        l = l.cdr;
                        BgL_sc_nbzd2confs_16zd2 += 1;
                    }
                    return rule_loop(rules.cdr, BgL_sc_nbzd2confs_16zd2 + 1);
                }
                else {
                    return BgL_sc_defzd2loop_6zd2(defs.cdr, BgL_sc_nbzd2confs_15zd2);
                }
            };
            return rule_loop(def.cdr, BgL_sc_nbzd2confs_14zd2);
        }
        else {
            return BgL_sc_nbzd2confs_14zd2;
        }
    };
    nb_confs = BgL_sc_defzd2loop_6zd2(grammar, 0) + BgL_sc_nbzd2nts_7zd2;
    sc_starters_5 = sc_makeVector(BgL_sc_nbzd2nts_7zd2, new SC_NSNull());
    sc_enders_4 = sc_makeVector(BgL_sc_nbzd2nts_7zd2, new SC_NSNull());
    sc_predictors_3 = sc_makeVector(BgL_sc_nbzd2nts_7zd2, new SC_NSNull());
    sc_steps_2 = sc_makeVector(nb_confs, false);
    sc_names_1 = sc_makeVector(nb_confs, false);
    nts = sc_nts_8;
    starters = sc_starters_5;
    enders = sc_enders_4;
    predictors = sc_predictors_3;
    steps = sc_steps_2;
    names = sc_names_1;
    nb_nts = sc_nts_8.length;
    i = nb_nts - 1;
    while (i >= 0) {
        sc_steps_2.arr[i] = i - nb_nts;
        sc_names_1.arr[i] = sc_list(sc_nts_8[i], 0);
        sc_enders_4.arr[i] = sc_list(i);
        i -= 1;
    }
    def_loop = (defs, conf) => {
        //debugLog('This is def_loop')
        let rule_loop;
        let head;
        let def;
        if (defs instanceof sc_Pair) {
            def = defs.car;
            head = def.car;
            rule_loop = (rules, conf, rule_num) => {
                let i;
                let sc_i_17;
                let nt;
                let l;
                let sc_conf_18;
                let sc_i_19;
                let rule;
                if (rules instanceof sc_Pair) {
                    rule = rules.car;
                    names.arr[conf] = sc_list(head, rule_num);
                    sc_i_19 = ind(head, nts);
                    starters.arr[sc_i_19] = new sc_Pair(conf, starters.arr[sc_i_19]);
                    l = rule;
                    sc_conf_18 = conf;
                    while (l instanceof sc_Pair) {
                        nt = l.car;
                        steps.arr[sc_conf_18] = ind(nt, nts);
                        sc_i_17 = ind(nt, nts);
                        predictors.arr[sc_i_17] = new sc_Pair(sc_conf_18, predictors.arr[sc_i_17]);
                        l = l.cdr;
                        sc_conf_18 += 1;
                    }
                    steps.arr[sc_conf_18] = ind(head, nts) - nb_nts;
                    i = ind(head, nts);
                    enders.arr[i] = new sc_Pair(sc_conf_18, enders.arr[i]);
                    return rule_loop(rules.cdr, sc_conf_18 + 1, rule_num + 1);
                }
                else {
                    return def_loop(defs.cdr, conf);
                }
            };
            return rule_loop(def.cdr, conf, 1);
        }
        else {
            return new SC_NSNull();
        }
    };
    def_loop(grammar, sc_nts_8.length);
    parser_descr = [
        lexer,
        sc_nts_8,
        sc_starters_5,
        sc_enders_4,
        sc_predictors_3,
        sc_steps_2,
        sc_names_1,
    ];
    return (input) => {
        //debugLog('This is input')
        let optrOpnd;
        let sc_optrOpnd_20;
        let sc_optrOpnd_21;
        let sc_optrOpnd_22;
        let loop1;
        let BgL_sc_stateza2_23za2;
        let toks;
        let BgL_sc_nbzd2nts_24zd2;
        let sc_steps_25;
        let sc_enders_26;
        let state_num;
        let BgL_sc_statesza2_27za2;
        let states;
        let i;
        let conf;
        let l;
        let tok_nts;
        let sc_i_28;
        let sc_i_29;
        let l1;
        let l2;
        let tok;
        let tail1129;
        let L1125;
        let goal_enders;
        let BgL_sc_statesza2_30za2;
        let BgL_sc_nbzd2nts_31zd2;
        let BgL_sc_nbzd2confs_32zd2;
        let nb_toks;
        let goal_starters;
        let sc_states_33;
        let BgL_sc_nbzd2confs_34zd2;
        let BgL_sc_nbzd2toks_35zd2;
        let sc_toks_36;
        let falseHead1128;
        let sc_names_37;
        let sc_steps_38;
        let sc_predictors_39;
        let sc_enders_40;
        let sc_starters_41;
        let sc_nts_42;
        let make_states;
        let BgL_sc_confzd2setzd2getza2_44za2;
        let conf_set_merge_new_bang;
        let conf_set_adjoin;
        let BgL_sc_confzd2setzd2adjoinza2_45za2;
        let BgL_sc_confzd2setzd2adjoinza2za2_46z00;
        let conf_set_union;
        let forw;
        make_states = (BgL_sc_nbzd2toks_50zd2, BgL_sc_nbzd2confs_51zd2) => {
            //debugLog('This is make_states')
            let v;
            let i;
            let sc_states_52;
            sc_states_52 = sc_makeVector(BgL_sc_nbzd2toks_50zd2 + 1, false);
            i = BgL_sc_nbzd2toks_50zd2;
            while (i >= 0) {
                v = sc_makeVector(BgL_sc_nbzd2confs_51zd2 + 1, false);
                v.arr[0] = -1;
                sc_states_52.arr[i] = v;
                i -= 1;
            }
            return sc_states_52;
        };
        BgL_sc_confzd2setzd2getza2_44za2 = (state, BgL_sc_statezd2num_53zd2, sc_conf_54) => {
            let conf_set;
            let BgL_sc_confzd2set_55zd2;
            BgL_sc_confzd2set_55zd2 = state.arr[sc_conf_54 + 1];
            if (BgL_sc_confzd2set_55zd2 !== false) {
                return BgL_sc_confzd2set_55zd2;
            }
            else {
                conf_set = sc_makeVector(BgL_sc_statezd2num_53zd2 + 6, false);
                conf_set.arr[1] = -3;
                conf_set.arr[2] = -1;
                conf_set.arr[3] = -1;
                conf_set.arr[4] = -1;
                state.arr[sc_conf_54 + 1] = conf_set;
                return conf_set;
            }
        };
        conf_set_merge_new_bang = (conf_set) => {
            conf_set.arr[conf_set.arr[1] + 5] = conf_set.arr[4];
            conf_set.arr[1] = conf_set.arr[3];
            conf_set.arr[3] = -1;
            return (conf_set.arr[4] = -1);
        };
        conf_set_adjoin = (state, conf_set, sc_conf_56, i) => {
            let tail = conf_set.arr[3];
            conf_set.arr[i + 5] = -1;
            conf_set.arr[tail + 5] = i;
            conf_set.arr[3] = i;
            if (tail < 0) {
                conf_set.arr[0] = state.arr[0];
                state.arr[0] = sc_conf_56;
                return sc_conf_56;
            }
            else {
                return new SC_NSNull();
            }
        };
        BgL_sc_confzd2setzd2adjoinza2_45za2 = (sc_states_57, BgL_sc_statezd2num_58zd2, l, i) => {
            //debugLog('This is BgL_sc_confzd2setzd2adjoinza2_45za2')
            let conf_set;
            let sc_conf_59;
            let l1;
            let state;
            state = sc_states_57.arr[BgL_sc_statezd2num_58zd2];
            l1 = l;
            while (l1 instanceof sc_Pair) {
                sc_conf_59 = l1.car;
                conf_set = BgL_sc_confzd2setzd2getza2_44za2(state, BgL_sc_statezd2num_58zd2, sc_conf_59);
                if (conf_set.arr[i + 5] === false) {
                    conf_set_adjoin(state, conf_set, sc_conf_59, i);
                    l1 = l1.cdr;
                }
                else {
                    l1 = l1.cdr;
                }
            }
            return new SC_NSNull();
        };
        BgL_sc_confzd2setzd2adjoinza2za2_46z00 = (sc_states_60, BgL_sc_statesza2_61za2, BgL_sc_statezd2num_62zd2, sc_conf_63, i) => {
            let BgL_sc_confzd2setza2_64z70;
            let BgL_sc_stateza2_65za2;
            let conf_set;
            let state = sc_states_60.arr[BgL_sc_statezd2num_62zd2];
            conf_set = state.arr[sc_conf_63 + 1];
            if ((conf_set !== false ? conf_set.arr[i + 5] : false) !==
                false) {
                BgL_sc_stateza2_65za2 = BgL_sc_statesza2_61za2.arr[BgL_sc_statezd2num_62zd2];
                BgL_sc_confzd2setza2_64z70 = BgL_sc_confzd2setzd2getza2_44za2(BgL_sc_stateza2_65za2, BgL_sc_statezd2num_62zd2, sc_conf_63);
                BgL_sc_confzd2setza2_64z70.arr[i + 5] === false
                    ? conf_set_adjoin(BgL_sc_stateza2_65za2, BgL_sc_confzd2setza2_64z70, sc_conf_63, i)
                    : null;
                return true;
            }
            else {
                return false;
            }
        };
        conf_set_union = (state, conf_set, sc_conf_66, other_set) => {
            //debugLog("This is conf_set_union");
            let i = other_set.arr[2];
            while (i >= 0) {
                if (conf_set.arr[i + 5] === false) {
                    conf_set_adjoin(state, conf_set, sc_conf_66, i);
                    i = other_set.arr[i + 5];
                }
                else {
                    i = other_set.arr[i + 5];
                }
            }
            return new SC_NSNull();
        };
        forw = (sc_states_67, BgL_sc_statezd2num_68zd2, sc_starters_69, sc_enders_70, sc_predictors_71, sc_steps_72, sc_nts_73) => {
            //debugLog('This is forw')
            let next_set;
            let next;
            let conf_set;
            let ender;
            let l;
            let starter_set;
            let starter;
            let sc_l_74;
            let sc_loop1_75;
            let head;
            let BgL_sc_confzd2set_76zd2;
            let BgL_sc_statezd2num_77zd2;
            let state;
            let sc_states_78;
            let preds;
            let BgL_sc_confzd2set_79zd2;
            let step;
            let sc_conf_80;
            let BgL_sc_nbzd2nts_81zd2;
            let sc_state_82;
            sc_state_82 = sc_states_67.arr[BgL_sc_statezd2num_68zd2];
            BgL_sc_nbzd2nts_81zd2 = sc_nts_73.length;
            while (true) {
                sc_conf_80 = sc_state_82.arr[0];
                if (sc_conf_80 >= 0) {
                    step = sc_steps_72.arr[sc_conf_80];
                    BgL_sc_confzd2set_79zd2 = sc_state_82.arr[sc_conf_80 + 1];
                    head = BgL_sc_confzd2set_79zd2.arr[4];
                    sc_state_82.arr[0] = BgL_sc_confzd2set_79zd2.arr[0];
                    conf_set_merge_new_bang(BgL_sc_confzd2set_79zd2);
                    if (step >= 0) {
                        sc_l_74 = sc_starters_69.arr[step];
                        while (sc_l_74 instanceof sc_Pair) {
                            starter = sc_l_74.car;
                            starter_set = BgL_sc_confzd2setzd2getza2_44za2(sc_state_82, BgL_sc_statezd2num_68zd2, starter);
                            if (starter_set.arr[BgL_sc_statezd2num_68zd2 + 5] === false) {
                                conf_set_adjoin(sc_state_82, starter_set, starter, BgL_sc_statezd2num_68zd2);
                                sc_l_74 = sc_l_74.cdr;
                            }
                            else {
                                sc_l_74 = sc_l_74.cdr;
                            }
                        }
                        l = sc_enders_70.arr[step];
                        while (l instanceof sc_Pair) {
                            ender = l.car;
                            conf_set = sc_state_82.arr[ender + 1];
                            if ((conf_set !== false
                                ? conf_set.arr[BgL_sc_statezd2num_68zd2 + 5]
                                : false) !== false) {
                                next = sc_conf_80 + 1;
                                next_set = BgL_sc_confzd2setzd2getza2_44za2(sc_state_82, BgL_sc_statezd2num_68zd2, next);
                                conf_set_union(sc_state_82, next_set, next, BgL_sc_confzd2set_79zd2);
                                l = l.cdr;
                            }
                            else {
                                l = l.cdr;
                            }
                        }
                    }
                    else {
                        preds = sc_predictors_71.arr[step + BgL_sc_nbzd2nts_81zd2];
                        sc_states_78 = sc_states_67;
                        state = sc_state_82;
                        BgL_sc_statezd2num_77zd2 = BgL_sc_statezd2num_68zd2;
                        BgL_sc_confzd2set_76zd2 = BgL_sc_confzd2set_79zd2;
                        sc_loop1_75 = (l) => {
                            let sc_state_83;
                            let BgL_sc_nextzd2set_84zd2;
                            let sc_next_85;
                            let pred_set;
                            let i;
                            let pred;
                            if (l instanceof sc_Pair) {
                                pred = l.car;
                                i = head;
                                while (i >= 0) {
                                    sc_state_83 = sc_states_78.arr[i];
                                    pred_set = sc_state_83.arr[pred + 1];
                                    if (pred_set !== false) {
                                        sc_next_85 = pred + 1;
                                        BgL_sc_nextzd2set_84zd2 = BgL_sc_confzd2setzd2getza2_44za2(state, BgL_sc_statezd2num_77zd2, sc_next_85);
                                        conf_set_union(state, BgL_sc_nextzd2set_84zd2, sc_next_85, pred_set);
                                    }
                                    i = BgL_sc_confzd2set_76zd2.arr[i + 5];
                                }
                                return sc_loop1_75(l.cdr);
                            }
                            else {
                                return new SC_NSNull();
                            }
                        };
                        sc_loop1_75(preds);
                    }
                }
                else {
                    return new SC_NSNull();
                }
            }
        };
        sc_nts_42 = parser_descr[1];
        sc_starters_41 = parser_descr[2];
        sc_enders_40 = parser_descr[3];
        sc_predictors_39 = parser_descr[4];
        sc_steps_38 = parser_descr[5];
        sc_names_37 = parser_descr[6];
        falseHead1128 = new sc_Pair(new SC_NSNull(), new SC_NSNull());
        L1125 = lexer(input);
        tail1129 = falseHead1128;
        while (!(L1125 instanceof SC_NSNull)) {
            tok = L1125.car;
            let l1 = tok.cdr;
            let l2 = new SC_NSNull();
            while (l1 instanceof sc_Pair) {
                sc_i_29 = sc_ind_43(l1.car, sc_nts_42);
                if (sc_i_29 !== false) {
                    l1 = l1.cdr;
                    l2 = new sc_Pair(sc_i_29, l2);
                }
                else {
                    l1 = l1.cdr;
                }
            }
            sc_optrOpnd_22 = new sc_Pair(tok.car, sc_reverse(l2));
            sc_optrOpnd_21 = new sc_Pair(sc_optrOpnd_22, new SC_NSNull());
            tail1129.cdr = sc_optrOpnd_21;
            tail1129 = tail1129.cdr;
            L1125 = L1125.cdr;
        }
        sc_optrOpnd_20 = falseHead1128.cdr;
        sc_toks_36 = sc_list2vector(sc_optrOpnd_20);
        BgL_sc_nbzd2toks_35zd2 = sc_toks_36.arr.length;
        BgL_sc_nbzd2confs_34zd2 = sc_steps_38.arr.length;
        sc_states_33 = make_states(BgL_sc_nbzd2toks_35zd2, BgL_sc_nbzd2confs_34zd2);
        goal_starters = sc_starters_41.arr[0];
        BgL_sc_confzd2setzd2adjoinza2_45za2(sc_states_33, 0, goal_starters, 0);
        forw(sc_states_33, 0, sc_starters_41, sc_enders_40, sc_predictors_39, sc_steps_38, sc_nts_42);
        sc_i_28 = 0;
        while (sc_i_28 < BgL_sc_nbzd2toks_35zd2) {
            tok_nts = sc_toks_36.arr[sc_i_28].cdr;
            BgL_sc_confzd2setzd2adjoinza2_45za2(sc_states_33, sc_i_28 + 1, tok_nts, sc_i_28);
            forw(sc_states_33, sc_i_28 + 1, sc_starters_41, sc_enders_40, sc_predictors_39, sc_steps_38, sc_nts_42);
            sc_i_28 += 1;
        }
        nb_toks = sc_toks_36.arr.length;
        BgL_sc_nbzd2confs_32zd2 = sc_steps_38.arr.length;
        BgL_sc_nbzd2nts_31zd2 = sc_nts_42.length;
        BgL_sc_statesza2_30za2 = make_states(nb_toks, BgL_sc_nbzd2confs_32zd2);
        goal_enders = sc_enders_40.arr[0];
        l = goal_enders;
        while (l instanceof sc_Pair) {
            conf = l.car;
            BgL_sc_confzd2setzd2adjoinza2za2_46z00(sc_states_33, BgL_sc_statesza2_30za2, nb_toks, conf, 0);
            l = l.cdr;
        }
        i = nb_toks;
        while (i >= 0) {
            states = sc_states_33;
            BgL_sc_statesza2_27za2 = BgL_sc_statesza2_30za2;
            state_num = i;
            sc_enders_26 = sc_enders_40;
            sc_steps_25 = sc_steps_38;
            BgL_sc_nbzd2nts_24zd2 = BgL_sc_nbzd2nts_31zd2;
            toks = sc_toks_36;
            BgL_sc_stateza2_23za2 = BgL_sc_statesza2_30za2.arr[i];
            loop1 = () => {
                let sc_loop1_127;
                let prev;
                let BgL_sc_statesza2_128za2;
                let sc_states_129;
                let j;
                let i;
                let sc_i_130;
                let head;
                let conf_set;
                let sc_conf_131 = BgL_sc_stateza2_23za2.arr[0];
                if (sc_conf_131 >= 0) {
                    conf_set = BgL_sc_stateza2_23za2.arr[sc_conf_131 + 1];
                    head = conf_set.arr[4];
                    BgL_sc_stateza2_23za2.arr[0] = conf_set.arr[0];
                    conf_set_merge_new_bang(conf_set);
                    sc_i_130 = head;
                    while (sc_i_130 >= 0) {
                        i = sc_i_130;
                        j = state_num;
                        sc_states_129 = states;
                        BgL_sc_statesza2_128za2 = BgL_sc_statesza2_27za2;
                        prev = sc_conf_131 - 1;
                        if (sc_conf_131 >= BgL_sc_nbzd2nts_24zd2 &&
                            sc_steps_25.arr[prev] >= 0) {
                            sc_loop1_127 = (paramL) => {
                                let k;
                                let ender_set;
                                let state;
                                let ender;
                                let l = paramL;
                                while (true) {
                                    if (l instanceof sc_Pair) {
                                        ender = l.car;
                                        state = sc_states_129.arr[j];
                                        ender_set = state.arr[ender + 1];
                                        if (ender_set !== false) {
                                            k = ender_set.arr[2];
                                            while (k >= 0) {
                                                if (k >= i) {
                                                    if (BgL_sc_confzd2setzd2adjoinza2za2_46z00(sc_states_129, BgL_sc_statesza2_128za2, k, prev, i) !== false) {
                                                        BgL_sc_confzd2setzd2adjoinza2za2_46z00(sc_states_129, BgL_sc_statesza2_128za2, j, ender, k);
                                                    }
                                                }
                                                k = ender_set.arr[k + 5];
                                            }
                                            return sc_loop1_127(l.cdr);
                                        }
                                        else {
                                            l = l.cdr;
                                        }
                                    }
                                    else {
                                        return new SC_NSNull();
                                    }
                                }
                            };
                            sc_loop1_127(sc_enders_26.arr[sc_steps_25.arr[prev]]);
                        }
                        sc_i_130 = conf_set.arr[sc_i_130 + 5];
                    }
                    return loop1();
                }
                else {
                    return new SC_NSNull();
                }
            };
            loop1();
            i -= 1;
        }
        optrOpnd = BgL_sc_statesza2_30za2;
        return [
            sc_nts_42,
            sc_starters_41,
            sc_enders_40,
            sc_predictors_39,
            sc_steps_38,
            sc_names_37,
            sc_toks_36,
            optrOpnd,
            0,
            0,
            0,
        ];
    };
}
function ind(nt, sc_nts_10) {
    //debugLog('This is ind')
    let i = sc_nts_10.length - 1;
    while (true) {
        if (i >= 0) {
            if (sc_isEqual(sc_nts_10[i], nt)) {
                return i;
            }
            else {
                i -= 1;
            }
        }
        else {
            return false;
        }
    }
}
function test(k) {
    //debugLog('This is test')
    let const_earley = new sc_Pair(new sc_Pair("\u1E9Cs", new sc_Pair(new sc_Pair("\u1E9Ca", new SC_NSNull()), new sc_Pair(new sc_Pair("\u1E9Cs", new sc_Pair("\u1E9Cs", new SC_NSNull())), new SC_NSNull()))), new SC_NSNull());
    let x;
    let p;
    p = BgL_makezd2parserzd2(const_earley, lexer);
    x = p(sc_vector2list(sc_makeVector(k, "\u1E9Ca")));
    let result = sc_length(BgL_parsezd2ze3treesz31(x, "\u1E9Cs", 0, k));
    return result;
}
function lexer(l) {
    let sc_x_134;
    let falseHead1133 = new sc_Pair(new SC_NSNull(), new SC_NSNull());
    let tail1134 = falseHead1133;
    let L1130 = l;
    while (!(L1130 instanceof SC_NSNull)) {
        sc_x_134 = L1130.car;
        tail1134.cdr = new sc_Pair(sc_list(sc_x_134, sc_x_134), new SC_NSNull());
        tail1134 = tail1134.cdr;
        L1130 = L1130.cdr;
    }
    return falseHead1133.cdr;
}
function deriv_trees(sc_conf_91, i, j, sc_enders_92, sc_steps_93, sc_names_94, sc_toks_95, sc_states_96, BgL_sc_nbzd2nts_97zd2) {
    //debugLog("This is deriv_trees")
    let sc_loop1_98;
    let prev;
    let name = sc_names_94.arr[sc_conf_91];
    if (name !== false) {
        if (sc_conf_91 < BgL_sc_nbzd2nts_97zd2) {
            return sc_list(sc_list(name, sc_toks_95[i].car));
        }
        else {
            return sc_list(sc_list(name));
        }
    }
    else {
        prev = sc_conf_91 - 1;
        sc_loop1_98 = (l1_param, l2_param) => {
            //debugLog("This is sc_loop1_98")
            let loop2;
            let ender_set;
            let state;
            let ender;
            let l1 = l1_param;
            let l2 = l2_param;
            while (true) {
                if (l1 instanceof sc_Pair) {
                    ender = l1.car;
                    state = sc_states_96.arr[j];
                    ender_set = state.arr[ender + 1];
                    if (ender_set !== false) {
                        loop2 = (paramK, paramL2) => {
                            let loop3;
                            let ender_trees;
                            let prev_trees;
                            let conf_set;
                            let sc_state_99;
                            let k = paramK;
                            let l2 = paramL2;
                            while (true) {
                                if (k >= 0) {
                                    sc_state_99 = sc_states_96.arr[k];
                                    conf_set = sc_state_99.arr[prev + 1];
                                    if (k >= i &&
                                        (conf_set !== false
                                            ? conf_set.arr[i + 5]
                                            : false) !== false) {
                                        prev_trees = deriv_trees(prev, i, k, sc_enders_92, sc_steps_93, sc_names_94, sc_toks_95, sc_states_96, BgL_sc_nbzd2nts_97zd2);
                                        ender_trees = deriv_trees(ender, k, j, sc_enders_92, sc_steps_93, sc_names_94, sc_toks_95, sc_states_96, BgL_sc_nbzd2nts_97zd2);
                                        loop3 = (l3, l2) => {
                                            let l4;
                                            let sc_l2_100;
                                            let ender_tree;
                                            if (l3 instanceof sc_Pair) {
                                                ender_tree = sc_list(l3.car);
                                                l4 = prev_trees;
                                                sc_l2_100 = l2;
                                                while (l4 instanceof sc_Pair) {
                                                    sc_l2_100 = new sc_Pair(sc_append(l4.car, ender_tree), sc_l2_100);
                                                    l4 = l4.cdr;
                                                }
                                                return loop3(l3.cdr, sc_l2_100);
                                            }
                                            else {
                                                return loop2(ender_set.arr[k + 5], l2);
                                            }
                                        };
                                        return loop3(ender_trees, l2);
                                    }
                                    else {
                                        k = ender_set.arr[k + 5];
                                    }
                                }
                                else {
                                    return sc_loop1_98(l1.cdr, l2);
                                }
                            }
                        };
                        return loop2(ender_set.arr[2], l2);
                    }
                    else {
                        l1 = l1.cdr;
                    }
                }
                else {
                    return l2;
                }
            }
        };
        return sc_loop1_98(sc_enders_92.arr[sc_steps_93.arr[prev]], new SC_NSNull());
    }
}
function BgL_sc_derivzd2treesza2_47z70(nt, i, j, sc_nts_101, sc_enders_102, sc_steps_103, sc_names_104, sc_toks_105, sc_states_106) {
    //debugLog('This is BgL_sc_derivzd2treesza2_47z70')
    let conf_set;
    let state;
    let sc_conf_107;
    let l;
    let trees;
    let BgL_sc_nbzd2nts_108zd2;
    let BgL_sc_ntza2_109za2;
    BgL_sc_ntza2_109za2 = sc_ind_43(nt, sc_nts_101);
    if (BgL_sc_ntza2_109za2 !== false) {
        BgL_sc_nbzd2nts_108zd2 = sc_nts_101.length;
        l = sc_enders_102.arr[BgL_sc_ntza2_109za2];
        trees = new SC_NSNull();
        while (l instanceof sc_Pair) {
            sc_conf_107 = l.car;
            state = sc_states_106.arr[j];
            conf_set = state.arr[sc_conf_107 + 1];
            if ((conf_set !== false ? conf_set.arr[i + 5] : false) !==
                false) {
                l = l.cdr;
                trees = sc_append(deriv_trees(sc_conf_107, i, j, sc_enders_102, sc_steps_103, sc_names_104, sc_toks_105, sc_states_106, BgL_sc_nbzd2nts_108zd2), trees);
            }
            else {
                l = l.cdr;
            }
        }
        return trees;
    }
    else {
        return false;
    }
}
function BgL_parsezd2ze3treesz31(parse, nt, i, j) {
    //debugLog('This is BgL_parsezd2ze3treesz31')
    let states;
    let toks;
    let names;
    let steps;
    let enders;
    let nts;
    nts = parse[0];
    enders = parse[2];
    steps = parse[4];
    names = parse[5];
    toks = parse[6].arr;
    states = parse[7];
    return BgL_sc_derivzd2treesza2_47z70(nt, i, j, nts, enders, steps, names, toks, states);
}
function sc_ind_43(nt, sc_nts_49) {
    //debugLog('This is sc_ind_43')
    let i = sc_nts_49.length - 1;
    while (true) {
        if (i >= 0) {
            if (sc_isEqual(sc_nts_49[i], nt)) {
                return i;
            }
            else {
                i -= 1;
            }
        }
        else {
            return false;
        }
    }
}
function sc_number2jsstring(x) {
    //debugLog('This is sc_number2jsstring')
    return x.toString();
}
class sc_ErrorOutputPort {
    appendJSString(s) {
        return;
    }
    close() { }
}
let SC_DEFAULT_OUT = new sc_ErrorOutputPort();
function sc_toWriteString(o) {
    //debugLog('This is sc_toWriteString')
    if (o instanceof SC_NSNull) {
        return "()";
    }
    else if (typeof o === "boolean") {
        if (o === true) {
            return "#t";
        }
        return "#f";
    }
    else if (typeof o === "number") {
        return o.toString();
    }
    return "";
}
function sc_toDisplayString(o) {
    //debugLog('This is sc_toDisplayString')
    if (o instanceof SC_NSNull) {
        return "()";
    }
    else if (typeof o === "boolean") {
        if (o === true) {
            return "#t";
        }
        return "#f";
    }
    else if (typeof o === "number") {
        return o.toString();
    }
    return "";
}
function sc_list(...args) {
    let res = new SC_NSNull();
    let i = args.length - 1;
    while (i >= 0) {
        res = new sc_Pair(args[i], res);
        i -= 1;
    }
    return res;
}
function sc_write(o, p) {
    //debugLog('This is sc_write')
    let p1 = p;
    if (p1 instanceof SC_NSNull) {
        p1 = SC_DEFAULT_OUT;
    }
    p1.appendJSString(sc_toWriteString(o));
}
function sc_display(o, p) {
    //debugLog('This is sc_display')
    let p1 = p;
    if (p1 instanceof SC_NSNull) {
        p1 = SC_DEFAULT_OUT;
    }
    p1.appendJSString(sc_toDisplayString(o));
}
function sc_isNumber(n) {
    return typeof n === "number";
}
function sc_cons(car, cdr) {
    return new sc_Pair(car, cdr);
}
function sc_isPair(p) {
    return p instanceof sc_Pair;
}
function sc_isPairEqual(p1, p2, comp) {
    return comp(p1.car, p2.car) && comp(p1.cdr, p2.cdr);
}
function sc_isVectorEqual(v1, v2, comp) {
    if (v1.arr.length !== v2.arr.length) {
        return false;
    }
    for (let i = 0; i < v1.arr.length; i++) {
        if (!comp(v1.arr[i], v2.arr[i])) {
            return false;
        }
    }
    return true;
}
function sc_isVector(v) {
    return v instanceof sc_Vector;
}
function sc_isEqual(o1, o2) {
    let result_any;
    let result_Pair;
    let result_Vector;
    let result_String;
    result_String =
        typeof o1 === "string" &&
            typeof o2 === "string" &&
            o1 == o2;
    result_any = o1 === o2;
    result_Pair =
        sc_isPair(o1) &&
            sc_isPair(o2) &&
            sc_isPairEqual(o1, o2, sc_isEqual);
    result_Vector =
        sc_isVector(o1) &&
            sc_isVector(o2) &&
            sc_isVectorEqual(o1, o2, sc_isEqual);
    if (result_any || result_Pair || result_Vector || result_String) {
        return true;
    }
    else {
        return false;
    }
}
function sc_assq(o, al) {
    let aln = al;
    while (!(aln instanceof SC_NSNull)) {
        if (aln.car.car === o) {
            return aln.car;
        }
        aln = aln.cdr;
    }
    return false;
}
function BgL_sc_symbolzd2ze3symbolzd2record_1ze3_nboyer(sym) {
    //debugLog('This is BgL_sc_symbolzd2ze3symbolzd2record_1ze3_nboyer');
    let r;
    let x;
    x = sc_assq(sym, BgL_sc_za2symbolzd2recordszd2alistza2_2z00_nboyer);
    if (x instanceof sc_Pair) {
        return x.cdr;
    }
    else {
        r = new sc_Vector([sym, new SC_NSNull()]);
        BgL_sc_za2symbolzd2recordszd2alistza2_2z00_nboyer = new sc_Pair(new sc_Pair(sym, r), BgL_sc_za2symbolzd2recordszd2alistza2_2z00_nboyer);
        return r;
    }
}
function translate_term_nboyer(term) {
    //debugLog('This is translate_term_nboyer');
    let lst;
    let cdr_value;
    if (!(term instanceof sc_Pair)) {
        return term;
    }
    lst = term.cdr;
    if (lst instanceof SC_NSNull) {
        cdr_value = new SC_NSNull();
        return new sc_Pair(BgL_sc_symbolzd2ze3symbolzd2record_1ze3_nboyer(term.car), cdr_value);
    }
    else {
        return new sc_Pair(BgL_sc_symbolzd2ze3symbolzd2record_1ze3_nboyer(term.car), new sc_Pair(translate_term_nboyer(lst.car), translate_args_nboyer(lst.cdr)));
    }
}
function translate_args_nboyer(lst) {
    //debugLog('This is translate_args_nboyer');
    let sc_lst_5;
    let term;
    let car_value;
    let cdr_value;
    if (lst instanceof SC_NSNull) {
        return new SC_NSNull();
    }
    term = lst.car;
    car_value = !(term instanceof sc_Pair)
        ? term
        : new sc_Pair(BgL_sc_symbolzd2ze3symbolzd2record_1ze3_nboyer(term.car), translate_args_nboyer(term.cdr));
    sc_lst_5 = lst.cdr;
    if (sc_lst_5 instanceof SC_NSNull) {
        cdr_value = new SC_NSNull();
    }
    else {
        cdr_value = new sc_Pair(translate_term_nboyer(sc_lst_5.car), translate_args_nboyer(sc_lst_5.cdr));
    }
    return new sc_Pair(car_value, cdr_value);
}
function apply_subst_nboyer(alist, term) {
    let lst;
    let temp_temp;
    if (!(term instanceof sc_Pair)) {
        temp_temp = sc_assq(term, alist);
        if (temp_temp instanceof sc_Pair) {
            return temp_temp.cdr;
        }
        else {
            return term;
        }
    }
    else {
        lst = term.cdr;
        if (lst instanceof SC_NSNull) {
            return new sc_Pair(term.car, new SC_NSNull());
        }
        else {
            return new sc_Pair(term.car, new sc_Pair(apply_subst_nboyer(alist, lst.car), apply_subst_lst_nboyer(alist, lst.cdr)));
        }
    }
}
function is_term_member_nboyer(x, lst) {
    //debugLog('This is is_term_member_nboyer');
    let temp_lst = lst;
    while (true) {
        if (temp_lst instanceof SC_NSNull) {
            return false;
        }
        else if (is_term_equal_nboyer(x, temp_lst.car) !== false) {
            return true;
        }
        else {
            temp_lst = temp_lst.cdr;
        }
    }
}
function is_term_equal_nboyer(x, y) {
    let lst1;
    let lst2;
    let r2;
    let r1;
    if (x instanceof sc_Pair) {
        if (y instanceof sc_Pair) {
            r1 = x.car;
            r2 = y.car;
            if ((r1 === r2) !== false) {
                lst1 = x.cdr;
                lst2 = y.cdr;
                while (true) {
                    if (lst1 instanceof SC_NSNull) {
                        return lst2 instanceof SC_NSNull;
                    }
                    else if (lst2 instanceof SC_NSNull) {
                        return false;
                    }
                    else if (is_term_equal_nboyer(lst1.car, lst2.car) !== false) {
                        lst1 = lst1.cdr;
                        lst2 = lst2.cdr;
                    }
                    else {
                        return false;
                    }
                }
            }
            else {
                return false;
            }
        }
        else {
            return false;
        }
    }
    else {
        return sc_isEqual(x, y);
    }
}
function sc_member(o, l) {
    let current = l;
    while (current instanceof sc_Pair) {
        if (sc_isEqual(current.car, o)) {
            return current;
        }
        current = current.cdr;
    }
    return false;
}
function sc_makeVector(size, fill) {
    let nullArr = [];
    for (let i = 0; i < size; i++) {
        nullArr.push(new SC_NSNull());
    }
    let a = new sc_Vector(nullArr);
    if (!(fill instanceof SC_NSNull)) {
        let i = 0;
        while (i < a.arr.length) {
            a.arr[i] = fill;
            i += 1;
        }
    }
    return a;
}
function sc_vector2list(a) {
    let res = new SC_NSNull();
    let i = a.arr.length - 1;
    while (i >= 0) {
        res = sc_cons(a.arr[i], res);
        i -= 1;
    }
    return res;
}
function sc_list2vector(l) {
    let a = new sc_Vector([]);
    let list = l;
    while (!(list instanceof SC_NSNull)) {
        a.arr.push(list.car);
        list = list.cdr;
    }
    return a;
}
function sc_length(l) {
    let res = 0;
    let l1 = l;
    while (!(l1 instanceof SC_NSNull)) {
        res += 1;
        l1 = l1.cdr;
    }
    return res;
}
function sc_reverseAppendBang(l1, l2) {
    let res = l2;
    let current = l1;
    while (!(current instanceof SC_NSNull)) {
        let tmp = res;
        res = current;
        current = current.cdr;
        res.cdr = tmp;
    }
    return res;
}
function sc_dualAppend(l1, l2) {
    if (l1 === null) {
        return l2;
    }
    if (l2 === null) {
        return l1;
    }
    let rev = sc_reverse(l1);
    return sc_reverseAppendBang(rev, l2);
}
function sc_append(...args) {
    if (args.length === 0) {
        return new SC_NSNull();
    }
    let res = args[args.length - 1];
    let i = args.length - 2;
    while (i >= 0) {
        res = sc_dualAppend(args[i], res);
        i -= 1;
    }
    return res;
}
function sc_reverse(l1) {
    let res = new SC_NSNull();
    let current = l1;
    while (!(current instanceof SC_NSNull)) {
        res = sc_cons(current.car, res);
        current = current.cdr;
    }
    return res;
}
function BgL_earleyzd2benchmarkzd2(...argumentsAry) {
    //debugLog('This is BgL_earleyzd2benchmarkzd2')
    let args = new SC_NSNull();
    let sc_tmp = argumentsAry.length - 1;
    while (sc_tmp >= 0) {
        args = sc_cons(argumentsAry[sc_tmp], args);
        sc_tmp -= 1;
    }
    let k = args instanceof SC_NSNull ? 7 : args.car;
    let warn = (result) => {
        sc_display(result, new SC_NSNull());
        sc_newline(new SC_NSNull());
        return result == 132;
    };
    let run = () => {
        return test(k);
    };
    return BgL_runzd2benchmarkzd2("earley", 1, run, warn);
}
function sc_newline(p) {
    let p1 = p;
    if (p1 instanceof SC_NSNull) {
        // we assume not given
        p1 = SC_DEFAULT_OUT;
    }
    p1.appendJSString("\n");
}
function RunBenchmark(name, count, run, warn) {
    //debugLog('This is RunBenchmark')
    for (let n = 0; n < count; ++n) {
        let result = run();
        //debugLog("result: " + result)
        if (!warn(result)) {
            return;
        }
    }
}
let BgL_runzd2benchmarkzd2 = RunBenchmark;
/*
 * @State
 */
class Benchmark {
    /*
     *@Benchmark
     */
    runIteration() {
        BgL_earleyzd2benchmarkzd2();
        BgL_nboyerzd2benchmarkzd2();
    }
}
class sc_Vector {
    constructor(arr) {
        this.arr = arr;
    }
}
function earleyBoyerRunIteration() {
    let start = Date.now();
    for (let i = 0; i < 120; ++i) {
        new Benchmark().runIteration();
    }
    let end = Date.now();
    console.log("earley-boyer: ms = " + String((end - start)));
}
function debugLog(str) {
    const isLog = false;
    if (isLog) {
        console.log(str);
    }
}
earleyBoyerRunIteration();
