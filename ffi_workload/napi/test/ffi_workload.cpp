/*
 * Copyright (c) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <cstddef>
#include <ctime>
#include <sys/time.h>

#include "gtest/gtest.h"

#include "ecmascript/builtins/builtins_function.h"
#include "ecmascript/builtins/builtins.h"
#include "ecmascript/ecma_global_storage.h"
#include "ecmascript/ecma_vm.h"
#include "ecmascript/global_env.h"
#include "ecmascript/js_bigint.h"
#include "ecmascript/js_runtime_options.h"
#include "ecmascript/js_thread.h"
#include "ecmascript/napi/include/jsnapi.h"
#include "ecmascript/napi/jsnapi_helper.h"
#include "ecmascript/object_factory.h"
#include "ecmascript/tagged_array.h"
#include "ecmascript/tests/test_helper.h"
#include "ecmascript/js_generator_object.h"
#include "ecmascript/js_list_format.h"
#include "ecmascript/compiler/aot_file/an_file_data_manager.h"
#include "ecmascript/compiler/aot_file/aot_file_manager.h"
#include "assembler/assembly-parser.h"
#include "ecmascript/jspandafile/js_pandafile_manager.h"
#include "ecmascript/js_api/js_api_deque.h"
#include "ecmascript/js_api/js_api_queue.h"
#include "ecmascript/builtins/builtins_locale.h"


using namespace panda;
using namespace panda::ecmascript;
using namespace panda::pandasm;
using namespace panda::panda_file;

static constexpr int Num_Count = 2000;
static constexpr int Time_Unit = 1000000;
time_t timefor = 0;
struct timeval time1;
struct timeval time2;
time_t t1 = 0;
time_t t2 = 0;
time_t t = 0;

#define TestTime(name)                                                                \
    {                                                                                 \
        t1 = (time1.tv_sec * Time_Unit) + (time1.tv_usec);                            \
        t2 = (time2.tv_sec * Time_Unit) + (time2.tv_usec);                            \
        t = t2 - t1;                                                                  \
        GTEST_LOG_(INFO) << "lxy-name =" << #name << " = time =" << int(t - timefor); \
    }

namespace panda::test {
class JSNApiFfiTest : public testing::Test {
public:
    void SetUp() override
    {
        RuntimeOption option;
        option.SetLogLevel(RuntimeOption::LOG_LEVEL::ERROR);
        vm_ = JSNApi::CreateJSVM(option);
        thread_ = vm_->GetJSThread();
        vm_->SetEnableForceGC(true);
    }

    void TearDown() override
    {
        vm_->SetEnableForceGC(false);
        JSNApi::DestroyJSVM(vm_);
    }

protected:
    JSThread *thread_ = nullptr;
    EcmaVM *vm_ = nullptr;
};

Local<JSValueRef> FunCallback(JsiRuntimeCallInfo *info)
{
    EscapeLocalScope scope(info->GetVM());
    return scope.Escape(ArrayRef::New(info->GetVM(), info->GetArgsNumber()));
}

void CalculateForTime()
{
    gettimeofday(&time1, nullptr);
    for (int i = 0; i < Num_Count; i++) {
    }
    gettimeofday(&time2, nullptr);
    time_t start = (time1.tv_sec * Time_Unit) + (time1.tv_usec);
    time_t end = (time2.tv_sec * Time_Unit) + (time2.tv_usec);
    timefor = end - start;
    GTEST_LOG_(INFO) << "timefor = " << timefor;
}

// 关于门禁的问题====================================================================================================================

// 383 JSNApi static void InitializeMemMapAllocator()
// 386 JSNApi static void DestroyMemMapAllocator()
HWTEST_F_L0(JSNApiFfiTest, JSNApi_InitializeMemMapAllocator_DestroyMemMapAllocator)
{
    CalculateForTime();
    TearDown();
    gettimeofday(&time1, nullptr);
    for (int i = 0; i < Num_Count; i++) {
        JSNApi::InitializeMemMapAllocator();
        JSNApi::DestroyMemMapAllocator();
    }
    gettimeofday(&time2, nullptr);
    TestTime(JSNApi_InitializeMemMapAllocator_DestroyMemMapAllocator);
    SetUp();
}

// 353 JSNApi	static EcmaVM *CreateJSVM(const RuntimeOption &option)	创建js VM虚拟机
HWTEST_F_L0(JSNApiFfiTest, JSNApi_CreateJSVM_DestroyJSVM)
{
    CalculateForTime();
    TearDown();
    RuntimeOption option;
    option.SetLogLevel(RuntimeOption::LOG_LEVEL::ERROR);
    gettimeofday(&time1, nullptr);
    for (int i = 0; i < Num_Count; i++) {
        vm_ = JSNApi::CreateJSVM(option);
        JSNApi::DestroyJSVM(vm_);
    }
    gettimeofday(&time2, nullptr);
    TestTime(JSNApi_CreateJSVM_DestroyJSVM);
    SetUp();
}

// 323 Exception	static Local<JSValueRef> OOMError(const EcmaVM *vm, Local<StringRef> message)
// 创建一个OOMError对象，并设置其消息为给定的字符串
HWTEST_F_L0(JSNApiFfiTest, Exception_OOMError)
{
    LocalScope scope(vm_);
    CalculateForTime();
    gettimeofday(&time1, nullptr);
    for (int i = 0; i < Num_Count; i++) {
        [[maybe_unused]] Local<JSValueRef> oomError =
            Exception::OOMError(vm_, StringRef::NewFromUtf8(vm_, "test out of memory error"));
    }
    gettimeofday(&time2, nullptr);
    TestTime(Exception_OOMError);
}

// 389 JSNApi	static void PreFork(EcmaVM *vm)	是用于在EcmaVM环境之后进行复制（fork）操作
HWTEST_F_L0(JSNApiFfiTest, JSNApi_PreFork)
{
    LocalScope scope(vm_);
    CalculateForTime();
    JSRuntimeOptions option;
    gettimeofday(&time1, nullptr);
    for (int i = 0; i < Num_Count; i++) {
        EcmaVM *workerVm = JSNApi::CreateEcmaVM(option);
        JSNApi::PreFork(workerVm);
        JSNApi::DestroyJSVM(workerVm);
    }
    gettimeofday(&time2, nullptr);
    TestTime(JSNApi_PreFork);
}

// 117 JSValueRef	bool IsJSListFormat();	判断是否为JS的格式链表
HWTEST_F_L0(JSNApiFfiTest, JSValueRef_IsJSListFormat_true)
{
    LocalScope scope(vm_);
    CalculateForTime();
    JSHandle<GlobalEnv> env = vm_->GetGlobalEnv();
    ObjectFactory *factory = vm_->GetFactory();

    icu::Locale icuLocale("en", "Latn", "US");
    JSHandle<JSTaggedValue> objFun = env->GetObjectFunction();
    JSHandle<JSObject> jsobject = factory->NewJSObjectByConstructor(JSHandle<JSFunction>(objFun), objFun);
    std::map<std::string, std::string> options { { "localeMatcher", "best fit" },
        { "type", "conjunction" },
        { "style", "long" } };

    auto globalConst = thread_->GlobalConstants();
    JSHandle<JSTaggedValue> localeMatcherKey = globalConst->GetHandledLocaleMatcherString();
    JSHandle<JSTaggedValue> typeKey = globalConst->GetHandledTypeString();
    JSHandle<JSTaggedValue> styleKey = globalConst->GetHandledStyleString();
    JSHandle<JSTaggedValue> localeMatcherValue(factory->NewFromASCII(options["localeMatcher"].c_str()));
    JSHandle<JSTaggedValue> typeValue(factory->NewFromASCII(options["type"].c_str()));
    JSHandle<JSTaggedValue> styleValue(factory->NewFromASCII(options["style"].c_str()));
    JSObject::SetProperty(thread_, jsobject, localeMatcherKey, localeMatcherValue);
    JSObject::SetProperty(thread_, jsobject, typeKey, typeValue);
    JSObject::SetProperty(thread_, jsobject, styleKey, styleValue);

    JSHandle<JSTaggedValue> localeCtor = env->GetLocaleFunction();
    JSHandle<JSTaggedValue> listCtor = env->GetListFormatFunction();
    JSHandle<JSLocale> locales =
        JSHandle<JSLocale>::Cast(factory->NewJSObjectByConstructor(JSHandle<JSFunction>(localeCtor), localeCtor));
    JSHandle<JSListFormat> jslistFormat =
        JSHandle<JSListFormat>::Cast(factory->NewJSObjectByConstructor(JSHandle<JSFunction>(listCtor), listCtor));

    JSHandle<JSTaggedValue> optionsVal = JSHandle<JSTaggedValue>::Cast(jsobject);
    factory->NewJSIntlIcuData(locales, icuLocale, JSLocale::FreeIcuLocale);
    jslistFormat =
        JSListFormat::InitializeListFormat(thread_, jslistFormat, JSHandle<JSTaggedValue>::Cast(locales), optionsVal);

    JSHandle<JSTaggedValue> listformat = JSHandle<JSTaggedValue>::Cast(jslistFormat);
    Local<JSValueRef> object = JSNApiHelper::ToLocal<JSValueRef>(listformat);
    gettimeofday(&time1, nullptr);
    for (int i = 0; i < Num_Count; i++) {
        ASSERT_TRUE(object->IsJSListFormat());
    }
    gettimeofday(&time2, nullptr);
    TestTime(JSValueRef_IsJSListFormat_true);
}

// 126 JSValueRef	bool IsDeque()	判断是否为Deque
HWTEST_F_L0(JSNApiFfiTest, JSValueRef_IsDeque)
{
    LocalScope scope(vm_);
    CalculateForTime();
    ObjectFactory *factory = vm_->GetFactory();
    JSThread *thread = vm_->GetJSThread();
    JSHandle<JSTaggedValue> proto = thread->GetEcmaVM()->GetGlobalEnv()->GetFunctionPrototype();
    JSHandle<JSHClass> queueClass = factory->NewEcmaHClass(JSAPIDeque::SIZE, JSType::JS_API_DEQUE, proto);
    JSHandle<JSAPIQueue> jsQueue = JSHandle<JSAPIQueue>::Cast(factory->NewJSObjectWithInit(queueClass));
    JSHandle<TaggedArray> newElements = factory->NewTaggedArray(JSAPIDeque::DEFAULT_CAPACITY_LENGTH);
    jsQueue->SetLength(thread, JSTaggedValue(0));
    jsQueue->SetFront(0);
    jsQueue->SetTail(0);
    jsQueue->SetElements(thread, newElements);
    JSHandle<JSTaggedValue> deQue = JSHandle<JSTaggedValue>::Cast(jsQueue);
    gettimeofday(&time1, nullptr);
    for (int i = 0; i < Num_Count; i++) {
        JSNApiHelper::ToLocal<JSValueRef>(deQue)->IsDeque();
    }
    gettimeofday(&time2, nullptr);
    TestTime(JSValueRef_IsDeque);
}

static JSTaggedValue JSLocaleCreateWithOptionTest(JSThread *thread)
{
    ObjectFactory *factory = thread->GetEcmaVM()->GetFactory();
    JSHandle<GlobalEnv> env = thread->GetEcmaVM()->GetGlobalEnv();
    JSHandle<JSFunction> newTarget(env->GetLocaleFunction());
    JSHandle<JSTaggedValue> objFun = env->GetObjectFunction();
    JSHandle<JSTaggedValue> languageKey = thread->GlobalConstants()->GetHandledLanguageString();
    JSHandle<JSTaggedValue> regionKey = thread->GlobalConstants()->GetHandledRegionString();
    JSHandle<JSTaggedValue> scriptKey = thread->GlobalConstants()->GetHandledScriptString();
    JSHandle<JSTaggedValue> languageValue(factory->NewFromASCII("en"));
    JSHandle<JSTaggedValue> regionValue(factory->NewFromASCII("US"));
    JSHandle<JSTaggedValue> scriptValue(factory->NewFromASCII("Latn"));
    JSHandle<JSTaggedValue> locale(factory->NewFromASCII("en-Latn-US"));
    // set option(language, region, script)
    JSHandle<JSObject> optionsObj = factory->NewJSObjectByConstructor(JSHandle<JSFunction>(objFun), objFun);
    JSObject::SetProperty(thread, optionsObj, languageKey, languageValue);
    JSObject::SetProperty(thread, optionsObj, regionKey, regionValue);
    JSObject::SetProperty(thread, optionsObj, scriptKey, scriptValue);

    auto ecmaRuntimeCallInfo = TestHelper::CreateEcmaRuntimeCallInfo(thread, JSTaggedValue(*newTarget), 8);
    ecmaRuntimeCallInfo->SetFunction(newTarget.GetTaggedValue());
    ecmaRuntimeCallInfo->SetThis(JSTaggedValue::Undefined());
    ecmaRuntimeCallInfo->SetCallArg(0, locale.GetTaggedValue());
    ecmaRuntimeCallInfo->SetCallArg(1, optionsObj.GetTaggedValue());

    auto prev = TestHelper::SetupFrame(thread, ecmaRuntimeCallInfo);
    JSTaggedValue result = ecmascript::builtins::BuiltinsLocale::LocaleConstructor(ecmaRuntimeCallInfo);
    TestHelper::TearDownFrame(thread, prev);
    return result;
}
// 110 JSValueRef	bool IsJSLocale();	判断是否为JSLocale
HWTEST_F_L0(JSNApiFfiTest, JSValueRef_IsJSLocale)
{
    CalculateForTime();
    JSThread *thread = vm_->GetJSThread();
    JSHandle<JSLocale> jsLocale = JSHandle<JSLocale>(thread, JSLocaleCreateWithOptionTest(thread));
    JSHandle<JSTaggedValue> argumentTag = JSHandle<JSTaggedValue>::Cast(jsLocale);
    gettimeofday(&time1, nullptr);
    for (int i = 0; i < Num_Count; i++) {
        JSNApiHelper::ToLocal<ObjectRef>(argumentTag)->IsJSLocale();
    }
    gettimeofday(&time2, nullptr);
    TestTime(JSValueRef_IsJSLocale);
}


// 需要依赖外部文件，如 .abc .ai .an
// ====================================================================================================================

// 357 JSNApi	static bool Execute(EcmaVM *vm, const std::string &fileName, const std::string &entry,
// bool needUpdate = false)	执行一个abc文件
HWTEST_F_L0(JSNApiFfiTest, JSNApi_Execute)
{
    LocalScope scope(vm_);
    CalculateForTime();
    std::string baseFileName = "work.abc";
    JSNApi::EnableUserUncaughtErrorHandler(vm_);

    gettimeofday(&time1, nullptr);
    for (int i = 0; i < Num_Count; i++) {
        [[maybe_unused]] bool result = JSNApi::Execute(vm_, baseFileName, "termination_1");
    }
    gettimeofday(&time2, nullptr);
    TestTime(JSNApi_Execute);
}

// 358 JSNApi	static bool Execute(EcmaVM *vm, const uint8_t *data, int32_t size, const std::string &entry,
// const std::string &filename = "", bool needUpdate = false)
HWTEST_F_L0(JSNApiFfiTest, JSNApi_Execute002)
{
    LocalScope scope(vm_);
    CalculateForTime();
    std::string baseFileName = "test.abc";
    JSNApi::EnableUserUncaughtErrorHandler(vm_);
    const char *data = R"(
        .language ECMAScript
        .function any foo() {
            ldai 1
            return
        }
    )";
    gettimeofday(&time1, nullptr);
    for (int i = 0; i < Num_Count; i++) {
        JSNApi::Execute(vm_, reinterpret_cast<const uint8_t *>(data), sizeof(data), "func_main_0::foo", baseFileName,
            false);
    }
    gettimeofday(&time2, nullptr);
    TestTime(JSNApi_Execute002);
}

// 359 JSNApi	static bool ExecuteModuleBuffer(EcmaVM *vm, const uint8_t *data, int32_t size,
// const std::string &filename = "",bool needUpdate = false)	加载 .ai 和 .an 文件，成功返回true。
HWTEST_F_L0(JSNApiFfiTest, JSNApi_LoadAotFile)
{
    LocalScope scope(vm_);
    CalculateForTime();
    const std::string pathName("succ");
    gettimeofday(&time1, nullptr);
    for (int i = 0; i < Num_Count; i++) {
        JSNApi::LoadAotFile(vm_, pathName);
    }
    gettimeofday(&time2, nullptr);
    TestTime(JSNApi_LoadAotFile);
}

// 360 JSNApi	static bool ExecuteModuleFromBuffer(EcmaVM *vm, const void *data, int32_t size, const std::string &file)
// 执行一个模块化的abc文件
HWTEST_F_L0(JSNApiFfiTest, JSNApi_ExecuteModuleFromBuffer)
{
    LocalScope scope(vm_);
    CalculateForTime();
    std::string baseFileName = "test.abc";
    const char *data = R"(
        .language ECMAScript
        .function any func_main_0(any a0, any a1, any a2) {
            ldai 1
            return
        }
    )";

    gettimeofday(&time1, nullptr);
    for (int i = 0; i < Num_Count; i++) {
        JSNApi::ExecuteModuleFromBuffer(vm_, reinterpret_cast<const uint8_t *>(data), sizeof(data), baseFileName);
    }
    gettimeofday(&time2, nullptr);
    TestTime(JSNApi_ExecuteModuleFromBuffer);
}

// 361 JSNApi	static Local<ObjectRef> GetExportObject(EcmaVM *vm, const std::string &file, const std::string &key)
// 获取导出对象，js 文件中的export
HWTEST_F_L0(JSNApiFfiTest, JSNApi_GetExportObject)
{
    LocalScope scope(vm_);
    CalculateForTime();
    std::string baseFileName = "module_test_module_test_C.abc";
    JSNApi::EnableUserUncaughtErrorHandler(vm_);

    [[maybe_unused]] bool result = JSNApi::Execute(vm_, baseFileName, "module_test_module_test_C");
    gettimeofday(&time1, nullptr);
    for (int i = 0; i < Num_Count; i++) {
        JSNApi::GetExportObject(vm_, "module_test_module_test_B", "a");
    }
    gettimeofday(&time2, nullptr);
    TestTime(JSNApi_GetExportObject);
}

// 362 JSNApi	static Local<ObjectRef> GetExportObjectFromBuffer(EcmaVM *vm, const std::string &file,
// const std::string &key)	获取导出对象，js 文件中的export
HWTEST_F_L0(JSNApiFfiTest, JSNApi_GetExportObjectFromBuffer)
{
    LocalScope scope(vm_);
    CalculateForTime();
    std::string baseFileName = "module_test_module_test_C.abc";
    JSNApi::EnableUserUncaughtErrorHandler(vm_);

    [[maybe_unused]] bool result = JSNApi::Execute(vm_, baseFileName, "module_test_module_test_C");
    gettimeofday(&time1, nullptr);
    for (int i = 0; i < Num_Count; i++) {
        JSNApi::GetExportObjectFromBuffer(vm_, "module_test_module_test_B", "a");
    }
    gettimeofday(&time2, nullptr);
    TestTime(JSNApi_GetExportObjectFromBuffer);
}

// 393 JSNApi	static bool LoadPatch(EcmaVM *vm, const std::string &patchFileName, const std::string &baseFileName)
// 是用于在给定的EcmaVM环境中加载一个已经加载的补丁文件
HWTEST_F_L0(JSNApiFfiTest, JSNApi_LoadPatch_1)
{
    std::string baseFileName = "index.abc";
    std::string patchFileName = "index.abc";
    LocalScope scope(vm_);
    CalculateForTime();
    gettimeofday(&time1, nullptr);
    for (int i = 0; i < Num_Count; i++) {
        JSNApi::LoadPatch(vm_, patchFileName, baseFileName);
    }
    gettimeofday(&time2, nullptr);
    TestTime(JSNApi_LoadPatch_1);
}

// 394 JSNApi	static bool LoadPatch(EcmaVM *vm, const std::string &patchFileName, const void *patchBuffer,
// size_t patchSize, const std::string &baseFileName)	是用于在给定的EcmaVM环境中加载一个已经加载的补丁文件
HWTEST_F_L0(JSNApiFfiTest, JSNApi_LoadPatch_2)
{
    LocalScope scope(vm_);
    CalculateForTime();
    const char *baseFileName = "__base.pa";
    const char *baseData = R"(
        .function void foo() {}
    )";
    const char *patchFileName = "__patch.pa";
    const char *patchData = R"(
        .function void foo() {}
    )";

    JSPandaFileManager *pfManager = JSPandaFileManager::GetInstance();
    Parser parser;
    auto res = parser.Parse(patchData);
    std::unique_ptr<const File> basePF = pandasm::AsmEmitter::Emit(res.Value());
    std::unique_ptr<const File> patchPF = pandasm::AsmEmitter::Emit(res.Value());
    std::shared_ptr<JSPandaFile> baseFile = pfManager->NewJSPandaFile(basePF.release(), CString(baseFileName));
    std::shared_ptr<JSPandaFile> patchFile = pfManager->NewJSPandaFile(patchPF.release(), CString(patchFileName));
    pfManager->AddJSPandaFileVm(vm_, baseFile);
    pfManager->AddJSPandaFileVm(vm_, patchFile);

    gettimeofday(&time1, nullptr);
    for (int i = 0; i < Num_Count; i++) {
        [[maybe_unused]] auto result = JSNApi::LoadPatch(vm_, patchFileName, (void *)patchData, sizeof(patchData),
            baseFileName, (void *)baseData, sizeof(baseData));
    }
    gettimeofday(&time2, nullptr);
    TestTime(JSNApi_LoadPatch_2);
    pfManager->RemoveJSPandaFileVm(vm_, baseFile.get());
    pfManager->RemoveJSPandaFileVm(vm_, patchFile.get());
}

// 395 JSNApi	static bool UnloadPatch(EcmaVM *vm, const std::string &patchFileName)
// 是用于在给定的EcmaVM环境中卸载一个已经加载的补丁文件
HWTEST_F_L0(JSNApiFfiTest, JSNApi_UnloadPatch)
{
    LocalScope scope(vm_);
    CalculateForTime();
    std::string patchFileName = "index.abc";
    gettimeofday(&time1, nullptr);
    for (int i = 0; i < Num_Count; i++) {
        JSNApi::UnloadPatch(vm_, patchFileName);
    }
    gettimeofday(&time2, nullptr);
    TestTime(JSNApi_UnloadPatch);
}

// 396 JSNApi	static bool IsQuickFixCausedException(EcmaVM *vm, Local<ObjectRef> exception,
// const std::string &patchFileName)	是用于判断给定的异常是否由快速修复
HWTEST_F_L0(JSNApiFfiTest, JSNApi_IsQuickFixCausedException)
{
    std::string patchFileName = "index.abc";
    LocalScope scope(vm_);
    CalculateForTime();
    Local<ObjectRef> exception = JSNApi::GetAndClearUncaughtException(vm_);
    gettimeofday(&time1, nullptr);
    for (int i = 0; i < Num_Count; i++) {
        JSNApi::IsQuickFixCausedException(vm_, exception, patchFileName);
    }
    gettimeofday(&time2, nullptr);
    TestTime(JSNApi_IsQuickFixCausedException);
}

bool QuickFixQueryFunc(std::string baseFileName, std::string &patchFileName, void **patchBuffer,
    size_t &patchBufferSize)
{
    if (baseFileName != "merge.abc") {
        return false;
    }
    patchFileName = "__index.pa";
    const char *data = R"(
        .function void foo() {}
    )";
    void *bufferData = reinterpret_cast<void *>((const_cast<char *>(data)));
    *patchBuffer = bufferData;
    patchBufferSize = strlen(data);
    return true;
}

// 397 JSNApi	static void RegisterQuickFixQueryFunc(EcmaVM *vm, QuickFixQueryCallBack callBack)
// 用于在给定的EcmaVM环境中注册一个快速修复查询回调函数
HWTEST_F_L0(JSNApiFfiTest, JSNApi_RegisterQuickFixQueryFunc)
{
    LocalScope scope(vm_);
    CalculateForTime();
    gettimeofday(&time1, nullptr);
    for (int i = 0; i < Num_Count; i++) {
        JSNApi::RegisterQuickFixQueryFunc(vm_, QuickFixQueryFunc);
    }
    gettimeofday(&time2, nullptr);
    TestTime(JSNApi_RegisterQuickFixQueryFunc);
}


// Signal 11  构造 JsiRuntimeCallInfo 有问题。====================================================================
/*
// 411 JsiRuntimeCallInfo	EcmaVM *GetVM() const	返回当前线程关联的EcmaVM对象
HWTEST_F_L0(JSNApiFfiTest, JsiRuntimeCallInfo_GetVM)
{
    LocalScope scope(vm_);
    CalculateForTime();
    JsiRuntimeCallInfo object;
    gettimeofday(&time1, nullptr);
    for (int i = 0; i < Num_Count; i++) {
        // object.GetVM();
    }
    gettimeofday(&time2, nullptr);
    TestTime(JsiRuntimeCallInfo_GetVM);
}
*/

/*
// 413 JsiRuntimeCallInfo	inline void* GetData() const
HWTEST_F_L0(JSNApiFfiTest, JsiRuntimeCallInfo_GetData)
{
    LocalScope scope(vm_);
    CalculateForTime();
    JsiRuntimeCallInfo object;
    gettimeofday(&time1, nullptr);
    for (int i = 0; i < Num_Count; i++) {
        // object.GetData();
    }
    gettimeofday(&time2, nullptr);
    TestTime(JsiRuntimeCallInfo_GetData);
}
*/

// 新版代码更新===========================================================================================================

// 409 JsiRuntimeCallInfo	JsiRuntimeCallInfo(ecmascript::EcmaRuntimeCallInfo* ecmaInfo, void* data)
// 构造变更为无参构造。 构造JsRuntimeCaLLInfo对象
HWTEST_F_L0(JSNApiFfiTest, JsiRuntimeCallInfo_JsiRuntimeCallInfo)
{
    LocalScope scope(vm_);
    CalculateForTime();
    gettimeofday(&time1, nullptr);
    for (int i = 0; i < Num_Count; i++) {
        JsiRuntimeCallInfo();
    }
    gettimeofday(&time2, nullptr);
    TestTime(JsiRuntimeCallInfo_JsiRuntimeCallInfo);
}


/*
void* detach()
{
    GTEST_LOG_(INFO) << "detach is running";
    return nullptr;
}
void attach([[maybe_unused]] void* buffer)
{
    GTEST_LOG_(INFO) << "attach is running";
}

// 196 ObjectRef 	static Local<ObjectRef> New(const EcmaVM *vm, void *attach, void *detach);
// 新版本已经删除的函数
HWTEST_F_L0(JSNApiFfiTest, ObjectRef_New)
{
    LocalScope scope(vm_);
    CalculateForTime();
    gettimeofday(&time1, nullptr);
    for(int i = 0; i < Num_Count; i++) {
        [[maybe_unused]]Local<ObjectRef> object = ObjectRef::New(vm_, reinterpret_cast<void*>(detach),
reinterpret_cast<void*>(attach));
    }
    gettimeofday(&time2, nullptr);
    TestTime(ObjectRef_New);
}
// */

/*
// 355 JSNApi	static void CleanJSVMCache()	清除缓存
// 新版本已经删除的函数
HWTEST_F_L0(JSNApiFfiTest, JSNApi_CleanJSVMCache)
{
    LocalScope scope(vm_);
    CalculateForTime();
    gettimeofday(&time1, nullptr);
    for (int i = 0; i < Num_Count; i++) {
        JSNApi::CleanJSVMCache();
    }
    gettimeofday(&time2, nullptr);
    TestTime(JSNApi_CleanJSVMCache);
}
// */
}
