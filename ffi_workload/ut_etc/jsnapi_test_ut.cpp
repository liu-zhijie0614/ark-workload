/*
 * Copyright (c) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @tc.number: ffi_interface_api_001
 * @tc.name: JSNApi_LoadAotFile
 * @tc.desc: Load *. an and * ai atomic files
 * @tc.type: FUNC
 * @tc.require:
 */
HWTEST_F_L0(JSNApiTests, JSNApi_LoadAotFile)
{
    LocalScope scope(vm_);
    const std::string moduleName = "/data/test/hello-world";
    std::string aotFileName = ecmascript::AnFileDataManager::GetInstance()->GetDir();
    aotFileName += moduleName;
    std::string anFile = aotFileName + AOTFileManager::FILE_EXTENSION_AN;
    AnFileDataManager *anFileDataManager = AnFileDataManager::GetInstance();
    bool res = anFileDataManager->SafeLoad(anFile, AnFileDataManager::Type::AOT);
    EXPECT_TRUE(res);
    JSNApi::LoadAotFile(vm_, aotFileName);
}

/**
 * @tc.number: ffi_interface_api_002
 * @tc.name: JSNApi_Execute001						
 * @tc.desc: Determine if it is a proxy type execution target *. abc type file
 * @tc.type: FUNC
 * @tc.require:
 */
HWTEST_F_L0(JSNApiTests, JSNApi_Execute001)
{
    LocalScope scope(vm_);
    std::string baseFileName = "/data/test/termination_1.abc";
    JSNApi::EnableUserUncaughtErrorHandler(vm_);
    bool result = JSNApi::Execute(vm_, baseFileName, "termination_1");
    EXPECT_TRUE(result);
}

/**
 * @tc.number: ffi_interface_api_003
 * @tc.name: GetExportObject						
 * @tc.desc: This function is to get export object
 * @tc.type: FUNC
 * @tc.require:
 */
HWTEST_F_L0(JSNApiTests, GetExportObject)
{
    LocalScope scope(vm_);
    std::string baseFileName = "/data/test/termination_3.abc";
    JSNApi::EnableUserUncaughtErrorHandler(vm_);
    bool result = JSNApi::Execute(vm_, baseFileName, "termination_3");
    JSNApi::GetExportObject(vm_, "termination_2", "a");
    EXPECT_TRUE(result);
}

/**
 * @tc.number: ffi_interface_api_004
 * @tc.name: GetExportObjectFromBuffer						
 * @tc.desc: This function is to get export object from buffer
 * @tc.type: FUNC
 * @tc.require:
 */
HWTEST_F_L0(JSNApiTests, GetExportObjectFromBuffer)
{
    LocalScope scope(vm_);
    std::string baseFileName = "/data/test/termination_3.abc";
    JSNApi::EnableUserUncaughtErrorHandler(vm_);
    bool result = JSNApi::Execute(vm_, baseFileName, "termination_3");
    JSNApi::GetExportObjectFromBuffer(vm_, "termination_2", "a");
    EXPECT_TRUE(result);
}

/**
 * @tc.number: ffi_interface_api_005
 * @tc.name: SetPrototype
 * @tc.desc: Construct a BufferRef function to determine whether it is a SetPrototype  not Construct function
 * @tc.type: FUNC
 * @tc.require:
 */
HWTEST_F_L0(JSNApiTests, SetPrototype)  // 本地执行成功，上库执行中断
{
    LocalScope scope(vm_);
    Local<ObjectRef> obj = ObjectRef::New(vm_);
    ASSERT_TRUE(obj->SetPrototype(vm_, StringRef::NewFromUtf8(vm_, "abcd")));
}

/**
 * @tc.number: ffi_interface_api_006
 * @tc.name: HasPendingJob
 * @tc.desc: Set a pending task for the object and determine if the object has a pending task.
 * @tc.type: FUNC
 * @tc.require:
 */
HWTEST_F_L0(JSNApiTests, HasPendingJob)  // 本地执行成功，上库执行中断
{
    LocalScope scope(vm_);
    Local<JSValueRef> key = StringRef::NewFromUtf8(vm_, "TestKey");
    JSNApi::SetHostEnqueueJob(vm_, key);
    EXPECT_TRUE(JSNApi::HasPendingJob(vm_));
    JSNApi::ExecutePendingJob(vm_);
    EXPECT_FALSE(JSNApi::HasPendingJob(vm_));
}

/*
 * @tc.number: ffi_interface_api_007
 * @tc.name: JSNApi_IsJSPrimitiveBoolean
 * @tc.desc:  This function is to judge whether object is a jsprimitive boolean
 * @tc.type: FUNC
 * @tc.require:  parameter
 */
HWTEST_F_L0(JSNApiTests, IsJSPrimitiveBoolean)  // 本地执行成功，上库执行中断
{
    LocalScope scope(vm_);
    char16_t utf16[] = u"This is a char16 array";
    int size = sizeof(utf16);
    Local<JSValueRef> obj = StringRef::NewFromUtf16(vm_, utf16, size);
    ASSERT_FALSE(obj->IsJSPrimitiveBoolean());
}

/**
 * @tc.number: ffi_interface_api_008
 * @tc.name: IsJSPrimitiveSymbol
 * @tc.desc: Construct a BufferRef function to determine whether it is a JSPrimitiveSymbol
 * @tc.type: FUNC
 * @tc.require:  parameter
 */
HWTEST_F_L0(JSNApiTests, IsJSPrimitiveSymbol)  // 本地执行成功，上库执行中断
{
    LocalScope scope(vm_);
    Local<StringRef> description = StringRef::NewFromUtf8(vm_, "test");
    Local<JSValueRef> symbol = SymbolRef::New(vm_, description);
    ASSERT_FALSE(symbol->IsJSPrimitiveSymbol());
}

/**
 * @tc.number: ffi_interface_api_009
 * @tc.name: IsJSPrimitiveString
 * @tc.desc: Construct a BufferRef function to determine whether it is a JSPrimitiveString
 * @tc.type: FUNC
 * @tc.require:  parameter
 */
HWTEST_F_L0(JSNApiTests, IsJSPrimitiveString)  // 本地执行成功，上库执行中断
{
    LocalScope scope(vm_);
    Local<JSValueRef> jsprimitiveString = StringRef::NewFromUtf8(vm_, "Test");
    ASSERT_FALSE(jsprimitiveString->IsJSPrimitiveString());
}

/**
 * @tc.number: ffi_interface_api_010
 * @tc.name: IsJSPrimitiveInt
 * @tc.desc: Construct a BufferRef function to determine whether it is a IsJSPrimitiveInt  not Construct function
 * @tc.type: FUNC
 * @tc.require:  parameter
 */
HWTEST_F_L0(JSNApiTests, IsJSPrimitiveInt)  // 本地执行成功，上库执行中断
{
    LocalScope scope(vm_);
    Local<JSValueRef> jsprimitiveInt = ObjectRef::New(vm_);
    ASSERT_FALSE(jsprimitiveInt->IsJSPrimitiveInt());
}