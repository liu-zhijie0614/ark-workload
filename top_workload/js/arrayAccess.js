"use strict";
let input = 3000000;
function GenerateFakeRandomInteger() {
    let resource = new Int32Array([12, 43, 56, 76, 89, 54, 45, 32, 35, 47, 46, 44, 21, 37, 84]);
    return resource;
}
function GenerateFakeRandomFloat() {
    let resource = new Float64Array([12.2, 43.5, 56.2, 76.6, 89.7, 54.9, 45.2, 32.5, 35.6, 47.2, 46.6, 44.3, 21.2, 37.6, 84.57]);
    return resource;
}
function GenerateFakeRandomBool() {
    let resource = [true, false, true, false, true, false, true, false, true, false, true, false, true, false, true];
    return resource;
}
function GenerateFakeRandomString() {
    let resource = ["Op", "HOS", "Op", "HOS", "Op", "HOS", "Op", "HOS", "Op", "HOS", "Op", "HOS", "Op", "HOS", "Op"];
    return resource;
}
function GenerateFakeRandomIndex() {
    let resource = new Int32Array([3, 14, 44, 25, 91, 38, 82, 88, 64, 81, 70, 90, 33, 63, 70]);
    return resource;
}
function IntegerArray() {
    let count = 3000000;
    let integerIndexes = GenerateFakeRandomIndex();
    let resources = GenerateFakeRandomInteger();
    let res = new Int32Array([0, 0, 0, 0, 0]);
    let num = 1;
    let start = Date.now();
    let length = resources.length - 1;
    let resLength = res.length - 1;
    for (let i = 0; i < count; i++) {
        num += integerIndexes[i % num & length];
        res[i & resLength] = resources[i % num & length];
    }
    let end = Date.now();
    let tmp = 1;
    for (let i = 0; i < 5; i++) {
        tmp += res[i];
    }
    console.log(tmp);
    let time = (end - start) ;
    console.log("Array Access - IntegerArray:\t" + String(time) + "\tms");
    return time;
}
IntegerArray();
function FloatArray() {
    let count = 3000000;
    let res = new Float64Array([0.0, 0.0, 0.0, 0.0, 0.0]);
    let resources = GenerateFakeRandomFloat();
    let integerIndexes = GenerateFakeRandomIndex();
    let num = 1;
    let start = Date.now();
    let length = resources.length - 1;
    let resLength = res.length - 1;
    for (let i = 0; i < count; i++) {
        num += integerIndexes[i % num & length];
        res[i & resLength] = resources[i % num & length];
    }
    let end = Date.now();
    let tmp = 0.0;
    for (let i = 0; i < 5; i++) {
        tmp += res[i];
    }
    console.log(tmp);
    let time = (end - start) ;
    console.log("Array Access - FloatArray:\t" + String(time) + "\tms");
    return time;
}
FloatArray();
function BoolArray() {
    let count = 3000000;
    let resources = GenerateFakeRandomBool();
    let integerIndexes = GenerateFakeRandomIndex();
    let res = new Int32Array([0, 0, 0, 0, 0]);
    let num = 1;
    let start = Date.now();
    let length = resources.length - 1;
    let resLength = res.length - 1;
    for (let i = 0; i < count; i++) {
        num += integerIndexes[i % num & length];
        if (resources[i % num & length]) {
            res[i & resLength] = integerIndexes[i & length];
        }
    }
    let end = Date.now();
    let tmp = 0;
    for (let i = 0; i < 5; i++) {
        tmp += res[i];
    }
    console.log(tmp);
    let time = (end - start) ;
    console.log("Array Access - BoolArray:\t" + String(time) + "\tms");
    return time;
}
BoolArray();
function StringArray() {
    let count = 3000000 / 100;
    let resources = GenerateFakeRandomString();
    let res = [0, 0, 0];
    let start = Date.now();
    for (let i = 0; i < count; i++) {
        res[i % res.length] += resources[i % 15].length;
    }
    let end = Date.now();
    let sum = 0;
    for (let i = 0; i < res.length; i++) {
        sum += res[i];
    }
    console.log(sum);
    let time = (end - start) ;
    console.log("Array Access - StringArray:\t" + String(time) + "\tms");
    return time;
}
StringArray();
class Obj {
    constructor(value) {
        this.value = 0;
        this.value = value;
    }
}
;
function GenerateFakeRandomObject() {
    let resource = new Array(15).fill(new Obj(0));
    for (let i = 0; i < 15; i++) {
        let random = Math.random() * (10) + 1;
        resource[i] = new Obj(random);
    }
    return resource;
}
function ObjectArray() {
    let count = 3000000;
    let res = [new Obj(0), new Obj(0), new Obj(0), new Obj(0), new Obj(0)];
    let resources = GenerateFakeRandomObject();
    let start = Date.now();
    for (let i = 0; i < count; i++) {
        res[i % 5] = resources[i % 15];
    }
    let end = Date.now();
    let tmp = 1;
    for (let i = 0; i < 5; i++) {
        tmp += res[i].value;
    }
    console.log(tmp);
    let time = (end - start) ;
    console.log("Array Access - ObjectArray:\t" + String(time) + "\tms");
    return time;
}
ObjectArray();
