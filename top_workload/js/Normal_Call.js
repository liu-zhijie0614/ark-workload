"use strict";
let global_value = 0;
let arr = new Int32Array([12, 43, 56, 76, 89, 54, 45, 32, 35, 47, 46, 44, 21, 37, 84]);
class Obj {
    constructor(value) {
        this.value = 0;
        this.value = value;
    }
}
;
function GenerateFakeRandomObject() {
    let resource = new Array(15).fill(new Obj(0));
    for (let i = 0; i < 15; i++) {
        let random = Math.random() * (10) + 1;
        resource[i] = new Obj(random);
    }
    return resource;
}
function ParameterlessFoo() {
    let res = [new Obj(0), new Obj(0), new Obj(0), new Obj(0), new Obj(0)];
    let resources = GenerateFakeRandomObject();
    for (let i = 0; i < 200; i++) {
        res[i % 5] = resources[i % 15];
    }
    return res[1];
}
function GenerateFakeRandomInteger() {
    let resource = new Int32Array([12, 43, 56, 76, 89, 54, 45, 32, 35, 47, 46, 44, 21, 37, 84]);
    return resource;
}
function Foo(resources, i, i3, resourcesLength) {
    if ((resources[i % i3 & (resourcesLength - 1)] & 1) == 0) {
        i3 += 1;
    }
    else {
        i3 += 2;
    }
    return i3;
}
function RunNormalCall() {
    let count = 10000000;
    let i3 = 1;
    let resources = GenerateFakeRandomInteger();
    let startTime = Date.now();
    let foo = Foo;
    let resourcesLength = resources.length;
    for (let i = 0; i < count; i++) {
        i3 = foo(resources, i, i3, resourcesLength);
    }
    let midTime = Date.now();
    for (let i = 0; i < count; i++) {
    }
    let endTime = Date.now();
    let time = ((midTime - startTime) - (endTime - midTime)) ;
    console.log("Function Call - RunNormalCall:\t" + String(time) + "\tms");
    return time;
}
RunNormalCall();
function RunParameterlessCall() {
    let count = 10000;
    global_value = 0;
    let i3 = new Obj(1);
    let startTime = Date.now();
    let foo = ParameterlessFoo;
    for (let i = 0; i < count; i++) {
        i3 = foo();
    }
    let midTime = Date.now();
    for (let i = 0; i < count; i++) {
    }
    let endTime = Date.now();
    let time = ((midTime - startTime) - (endTime - midTime)) ;
    console.log("Function Call - RunParameterlessCall:\t" + String(time) + "\tms");
    return time;
}
RunParameterlessCall();
function DefaultFoo(resources = arr, i = 1, i3 = 1, resourcesLength = 1) {
    if ((resources[i % i3 & (resourcesLength - 1)] & 1) == 0) {
        i3 += 1;
    }
    else {
        i3 += 2;
    }
    return i3;
}
function RunNormalDefCall() {
    let count = 10000000;
    global_value = 0;
    let i3 = 1;
    let resources = GenerateFakeRandomInteger();
    let resourcesLength = resources.length;
    let startTime = Date.now();
    let foo = DefaultFoo;
    for (let i = 0; i < count; i++) {
        i3 = foo(resources, i, i3, resourcesLength);
    }
    let midTime = Date.now();
    for (let i = 0; i < count; i++) {
    }
    let endTime = Date.now();
    let time = ((midTime - startTime) - (endTime - midTime)) ;
    console.log("Function Call - RunNormalDefCall:\t" + String(time) + "\tms");
    return time;
}
RunNormalDefCall();
function DifferentFoo(resources, i, i3, resourcesLength) {
    if ((resources[i % i3 & (resourcesLength - 1)] & 1) == 0) {
        i3 += 1.1;
    }
    else {
        i3 += 2.1;
    }
    return i3;
}
function RunNormalDifferentCall() {
    let count = 10000000;
    global_value = 0;
    let i3 = 1.1;
    let resources = GenerateFakeRandomInteger();
    let resourcesLength = resources.length;
    let startTime = Date.now();
    let foo = DifferentFoo;
    for (let i = 0; i < count; i++) {
        i3 = foo(resources, i, i3, resourcesLength);
    }
    let midTime = Date.now();
    for (let i = 0; i < count; i++) {
    }
    let endTime = Date.now();
    let time = ((midTime - startTime) - (endTime - midTime)) ;
    console.log("Function Call - RunNormalDifferentCall:\t" + String(time) + "\tms");
    return time;
}
RunNormalDifferentCall();
function VariableFoo(a, b, c) {
    arr[global_value] += 1;
}
function RunNormalVariableFCall() {
    let count = 10000000;
    global_value = 1;
    let startTime = Date.now();
    let foo = VariableFoo;
    for (let i = 0; i < count; i++) {
        foo(1, "1", true);
    }
    let midTime = Date.now();
    for (let i = 0; i < count; i++) {
    }
    let endTime = Date.now();
    let time = ((midTime - startTime) - (endTime - midTime)) ;
    console.log("Function Call - RunNormalVariableFCall:\t" + String(time) + "\tms");
    return time;
}
RunNormalVariableFCall();
console.log("Ts Method Call Is End, global_value value: \t" + String(global_value));
