"use strict";
// declare interface ArkTools{
//  timeInUs(arg:any):number
// }
function GenerateRandoms() {
    const result = [];
    for (let i = 0; i < 2; i++) {
        const randomNum = Math.floor(Math.random() * 2) + 1; // 生成介于1和2之间的随机整数
        result.push(randomNum);
    }
    return result;
}
let generaterandoms = GenerateRandoms();
let global_value = 0;
function GenerateFakeRandomInteger() {
    let resource = new Int32Array([12, 43, 56, 76, 89, 54, 45, 32, 35, 47, 46, 44, 21, 37, 84]);
    return resource;
}
class Obj {
    constructor(value) {
        this.value = 0;
        this.value = value;
    }
}
;
function GenerateFakeRandomObject() {
    let resource = new Array(15).fill(new Obj(0));
    for (let i = 0; i < 15; i++) {
        let random = Math.random() * (10) + 1;
        resource[i] = new Obj(random);
    }
    return resource;
}
let arr = GenerateFakeRandomInteger();
class ClassFunc {
    foo(resources, i, i3, resourcesLength) {
        if ((resources[i % i3 & (resourcesLength - 1)] & 1) == 0) {
            i3 += 1;
        }
        else {
            i3 += 2;
        }
        return i3;
    }
    parameterlessFoo() {
        let res = [new Obj(0), new Obj(0), new Obj(0), new Obj(0), new Obj(0)];
        let resources = GenerateFakeRandomObject();
        for (let i = 0; i < 200; i++) {
            res[i % 5] = resources[i % 15];
        }
        return res[1];
    }
    differentFoo(resources = arr, i = 1, i3 = 1, resourcesLength = 1) {
        if ((resources[i % i3 & (resourcesLength - 1)] & 1) == 0) {
            i3 += 1.1;
        }
        else {
            i3 += 2.1;
        }
        return i3;
    }
    defaultFoo(resources = arr, i = 1, i3 = 1, resourcesLength = 1) {
        if ((resources[i % i3 & (resourcesLength - 1)] & 1) == 0) {
            i3 += 1;
        }
        else {
            i3 += 2;
        }
        return i3;
    }
    variableFoo(a, b, c) {
        arr[global_value] += 1;
    }
    argsFoo(...args) {
        global_value += 1;
    }
}
/***************************** With parameters *****************************/
// Method call
function RunMethodCall() {
    let count = 10000000;
    const cf = new ClassFunc();
    let i3 = 1;
    let resources = GenerateFakeRandomInteger();
    let func = cf.foo;
    let resourcesLength = resources.length;
    let startTime = Date.now();
    for (let i = 0; i < count; i++) {
        i3 = func(resources, i, i3, resourcesLength);
    }
    let midTime = Date.now();
    for (let i = 0; i < count; i++) {
    }
    let endTime = Date.now();
    let time = ((midTime - startTime) - (endTime - midTime)) ;
    console.log("Function Call - RunMethodCall:\t" + String(time) + "\tms");
    // console.log("Method Call:\t"+String(time)+"\tms");
    return time;
}
RunMethodCall();
function RunParameterlessMethodCall() {
    let count = 10000;
    const cf = new ClassFunc();
    global_value = 0;
    let i3 = new Obj(1);
    let func = cf.parameterlessFoo;
    let startTime = Date.now();
    for (let i = 0; i < count; i++) {
        i3 = func();
    }
    let midTime = Date.now();
    for (let i = 0; i < count; i++) {
    }
    let endTime = Date.now();
    let time = ((midTime - startTime) - (endTime - midTime)) ;
    console.log("Function Call - RunParameterlessMethodCall:\t" + String(time) + "\tms");
    return time;
}
RunParameterlessMethodCall();
/***************************** Default parameters *****************************/
// Method call
function RunDefMethodCall() {
    let count = 10000000;
    const cf = new ClassFunc();
    global_value = 0;
    let i3 = 1;
    let resources = GenerateFakeRandomInteger();
    let resourcesLength = resources.length;
    let func = cf.defaultFoo;
    let startTime = Date.now();
    for (let i = 0; i < count; i++) {
        i3 = func(resources, i, i3, resourcesLength);
    }
    let midTime = Date.now();
    for (let i = 0; i < count; i++) {
    }
    let endTime = Date.now();
    let time = ((midTime - startTime) - (endTime - midTime)) ;
    console.log("Function Call - RunDefMethodCall:\t" + String(time) + "\tms");
    return time;
}
RunDefMethodCall();
/********************* Different parameters *********************/
// Method call
function RunDifMethodCall() {
    let count = 10000000;
    const cf = new ClassFunc();
    global_value = 0;
    let i3 = 1.1;
    let resources = GenerateFakeRandomInteger();
    let resourcesLength = resources.length;
    let func = cf.differentFoo;
    let startTime = Date.now();
    for (let i = 0; i < count; i++) {
        i3 = func(resources, i, i3, resourcesLength);
    }
    let midTime = Date.now();
    for (let i = 0; i < count; i++) {
    }
    let endTime = Date.now();
    let time = ((midTime - startTime) - (endTime - midTime)) ;
    console.log("Function Call - RunDifMethodCall:\t" + String(time) + "\tms");
    return time;
}
RunDifMethodCall();
function RunVariableMethodCall() {
    let count = 10000000;
    const cf = new ClassFunc();
    global_value = 0;
    let func = cf.variableFoo;
    let startTime = Date.now();
    for (let i = 0; i < count; i++) {
        func(1, '1', true);
    }
    let midTime = Date.now();
    for (let i = 0; i < count; i++) {
    }
    let endTime = Date.now();
    let time = ((midTime - startTime) - (endTime - midTime)) ;
    console.log("Function Call - RunVariableMethodCall:\t" + String(time) + "\tms");
    return time;
}
RunVariableMethodCall();
console.log("Ts Method Call Is End, global_value value: \t" + String(global_value));
