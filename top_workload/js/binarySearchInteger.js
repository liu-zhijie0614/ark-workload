"use strict";
function ComplexIntegerNumeric() {
    let count = 5000000;
    let testArray = new Int32Array([1, 2, 5, 7, 9, 12, 14, 23, 32, 43, 67, 77, 89, 90, 101, 122, 125, 142, 153]); // GenerateSearchArrayInteger();
    let start = Date.now();
    let res = testArray[2];
    let testArrayLength = testArray.length;
    for (let i = 1; i < count; i++) {
        let value = testArray[i % res & (testArrayLength - 1)];
        let low = 0;
        let tmp = 0;
        let high = testArrayLength - 1; // 1023
        let middle = high >>> 1;
        for (; low <= high; middle = (low + high) >>> 1) {
            const test = testArray[middle];
            if (test > value) {
                high = middle - 1;
            }
            else if (test < value) {
                low = middle + 1;
            }
            else {
                tmp = middle;
                break;
            }
        }
        res += tmp;
    }
    let end = Date.now();
    console.log(res);
    let time = (end - start) ;
    console.log("Numerical Calculation - ComplexIntegerNumeric:\t" + String(time) + "\tms");
    return time;
}
ComplexIntegerNumeric();
