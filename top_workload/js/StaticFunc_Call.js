"use strict";
let global_value = 0;
let arr = new Int32Array([12, 43, 56, 76, 89, 54, 45, 32, 35, 47, 46, 44, 21, 37, 84]);
function GenerateFakeRandomInteger() {
    let resource = new Int32Array([12, 43, 56, 76, 89, 54, 45, 32, 35, 47, 46, 44, 21, 37, 84]);
    return resource;
}
class Obj {
    constructor(value) {
        this.value = 0;
        this.value = value;
    }
}
;
function GenerateFakeRandomObject() {
    let resource = new Array(15).fill(new Obj(0));
    for (let i = 0; i < 15; i++) {
        let random = Math.random() * (10) + 1;
        resource[i] = new Obj(random);
    }
    return resource;
}
class Example {
    static Foo(resources, i, i3, resourcesLength) {
        if ((resources[i % i3 & (resourcesLength - 1)] & 1) == 0) {
            i3 += 1;
        }
        else {
            i3 += 2;
        }
        return i3;
    }
    static parameterlessFoo() {
        let res = [new Obj(0), new Obj(0), new Obj(0), new Obj(0), new Obj(0)];
        let resources = GenerateFakeRandomObject();
        for (let i = 0; i < 200; i++) {
            res[i % 5] = resources[i % 15];
        }
        return res[1];
    }
    static DifferentFoo(resources, i, i3, resourcesLength) {
        if ((resources[i % i3 & (resourcesLength - 1)] & 1) == 0) {
            i3 += 1.1;
        }
        else {
            i3 += 2.1;
        }
        return i3;
    }
    static DefaultFoo(resources = arr, i = 1, i3 = 1, resourcesLength = 1) {
        if ((resources[i % i3 & (resourcesLength - 1)] & 1) == 0) {
            i3 += 1;
        }
        else {
            i3 += 2;
        }
        return i3;
    }
    static VariableFoo(a, b, c) {
        arr[global_value] += 1;
    }
    static argsFoo(...args) {
        global_value += 1;
    }
}
function RunStaticFunction() {
    let count = 10000000;
    let i3 = 1;
    let resources = GenerateFakeRandomInteger();
    let startTime = Date.now();
    let resourcesLength = resources.length;
    for (let i = 0; i < count; i++) {
        i3 = Example.Foo(resources, i, i3, resourcesLength);
    }
    let midTime = Date.now();
    for (let i = 0; i < count; i++) {
    }
    let endTime = Date.now();
    let time = ((midTime - startTime) - (endTime - midTime)) ;
    console.log("Static Function Call - RunStaticFunction:\t" + String(time) + "\tms");
    return time;
}
RunStaticFunction();
function RunParameterlessStaticFunction() {
    let count = 10000;
    global_value = 0;
    let i3 = new Obj(1);
    let startTime = Date.now();
    for (let i = 0; i < count; i++) {
        i3 = Example.parameterlessFoo();
    }
    let midTime = Date.now();
    for (let i = 0; i < count; i++) {
    }
    let endTime = Date.now();
    let time = ((midTime - startTime) - (endTime - midTime)) ;
    console.log("Static Function Call - RunParameterlessStaticFunction:\t" + String(time) + "\tms");
    return time;
}
RunParameterlessStaticFunction();
function RunNormalDifStaticFunc() {
    let count = 10000000;
    global_value = 0;
    let i3 = 1.1;
    let resources = GenerateFakeRandomInteger();
    let resourcesLength = resources.length;
    let startTime = Date.now();
    for (let i = 0; i < count; i++) {
        i3 = Example.DifferentFoo(resources, i, i3, resourcesLength);
    }
    let midTime = Date.now();
    for (let i = 0; i < count; i++) {
    }
    let endTime = Date.now();
    let time = ((midTime - startTime) - (endTime - midTime)) ;
    console.log("Static Function Call - RunNormalDifStaticFunc:\t" + String(time) + "\tms");
    return time;
}
RunNormalDifStaticFunc();
function RunNormalVariableFStaticFunc() {
    let count = 10000000;
    global_value = 0;
    let startTime = Date.now();
    for (let i = 0; i < count; i++) {
        Example.VariableFoo(1, "2", true);
    }
    let midTime = Date.now();
    for (let i = 0; i < count; i++) {
    }
    let endTime = Date.now();
    let time = ((midTime - startTime) - (endTime - midTime)) ;
    console.log("Static Function Call - RunNormalVariableFStaticFunc:\t" + String(time) + "\tms");
    return time;
}
RunNormalVariableFStaticFunc();
function RunNormalDefStaticCall() {
    let count = 10000000;
    global_value = 0;
    let i3 = 1;
    let resources = GenerateFakeRandomInteger();
    let resourcesLength = resources.length;
    let startTime = Date.now();
    for (let i = 0; i < count; i++) {
        i3 = Example.DefaultFoo(resources, i, i3, resourcesLength);
    }
    let midTime = Date.now();
    for (let i = 0; i < count; i++) {
    }
    let endTime = Date.now();
    let time = ((midTime - startTime) - (endTime - midTime)) ;
    console.log("Static Function Call - RunNormalDefStaticCall:\t" + String(time) + "\tms");
    return time;
}
RunNormalDefStaticCall();
console.log("Ts Method Call Is End, global_value value: \t" + String(global_value));
