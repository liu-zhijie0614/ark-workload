"use strict";
function ComplexFloatNumeric() {
    let count = 5000000;
    let testArray = new Float64Array([2.3, 4.6, 5.2, 5.5, 6.1, 6.7, 7.4, 7.6, 7.886, 8.2, 9.11, 10.02, 33.2]); // GenerateSearchArrayFloat();
    let res = 5;
    let start = Date.now();
    let testArrayLength = testArray.length;
    for (let i = 1; i < count; i++) {
        let value = testArray[i % res & (testArrayLength - 1)];
        let tmp = 0;
        let low = 0;
        let high = testArrayLength - 1;
        let middle = high >>> 1;
        for (; low <= high; middle = (low + high) >>> 1) {
            let test = testArray[middle];
            if (test > value) {
                high = middle - 1;
            }
            else if (test < value) {
                low = middle + 1;
            }
            else {
                tmp = middle;
                break;
            }
        }
        res += tmp;
    }
    let end = Date.now();
    console.log(res);
    let time = (end - start) ;
    console.log("Numerical Calculation - ComplexFloatNumeric:\t" + String(time) + "\tms");
    return time;
}
ComplexFloatNumeric();
