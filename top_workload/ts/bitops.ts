// import { ASSERT_NUMBER_EQ, ASSERT_FLOAT_EQ, ASSERT_TRUE, ASSERT_FALSE, ASSERT_EQ } from "../../../utils/assert";
// import { BenchmarkRunner } from "../../../utils/benchmarkTsSuite";
// declare function print(arg:string):string;

declare interface ArkTools{
  timeInUs(arg:any):number
}


function RunBitOp1() {
  let res: number = 0;
  // let func = ;
  let resources: Int32Array = new Int32Array([12, 43, 56, 76, 89, 54, 45, 32, 35, 47, 46, 44, 21, 37, 84])
  let start = ArkTools.timeInUs();
  let resourcesLength = resources.length - 1;
  for (let i = 0; i < 6000000; i++) {
    let bi3b: number = 0xE994;
    let b: number = resources[(i & res) & resourcesLength];
    res += 3 & (bi3b >> ((b << 1) & 14));
    res += 3 & (bi3b >> ((b >> 2) & 14));
    res += 3 & (bi3b >> ((b >> 5) & 6));
  }
  let end = ArkTools.timeInUs();
  print(""+res)
  let time = (end - start) / 1000
  print("Numerical Calculation - RunBitOp1:\t"+String(time)+"\tms");
  return time;
}
RunBitOp1()
// let runner1 = new BenchmarkRunner("Numerical Calculation - RunBitOp1", RunBitOp1);
// runner1.run();


function RunBitOp2() {
  let res: number = 0;
  let start = ArkTools.timeInUs();
  for (let i = 0; i < 6000000; i++) {
    let b = i
    let m = 1, c = 0
    while (m < 0x100) {
      if ((b & m) != 0) {
        c += 1
      }
      m <<= 1
    }
    res += c
  }
  let end = ArkTools.timeInUs();
  print(""+res)
  let time = (end - start) / 1000
  print("Numerical Calculation - RunBitOp2:\t"+String(time)+"\tms");
  return time;
}
RunBitOp2()
// let runner2 = new BenchmarkRunner("Numerical Calculation - RunBitOp2", RunBitOp2);
// runner2.run();

function RunBitOp3() {
  let res = 0;
  let start = ArkTools.timeInUs();
  for (let y = 0; y < 6000000; y++) {
    let x = y;
    let r = 0;
    while (x != 0) {
      x &= x - 1;
      ++r;
    }
    res += r;
  }
  let end = ArkTools.timeInUs();
  print(""+res);
  let time = (end - start) / 1000
  print("Numerical Calculation - RunBitOp3:\t"+String(time)+"\tms");
  return time;
}
RunBitOp3()
// let runner3 = new BenchmarkRunner("Numerical Calculation - RunBitOp3", RunBitOp3);
// runner3.run();

function RunBitOp4() {
  let results: Int32Array = new Int32Array([3, 53, 76, 37, 82, 23, 66, 17, 82, 43, 77, 93, 28, 24, 85]);
  let resources: Int32Array = new Int32Array([12, 43, 56, 76, 89, 54, 45, 32, 35, 47, 46, 44, 21, 37, 84])
  let res: number = 0;
  let start = ArkTools.timeInUs();
  let resultsLength = results.length - 1;
  let resourcesLength = resources.length - 1;
  for (let i = 0; i < 6000000; i++) {
    res |= ~(1 << (resources[i & resourcesLength] ^ 31));
  }
  let end = ArkTools.timeInUs();
  print(""+res);
  let time = (end - start) / 1000
  print("Numerical Calculation - RunBitOp4:\t"+String(time)+"\tms");
  return time;
}
RunBitOp4()
// let runner4 = new BenchmarkRunner("Numerical Calculation - RunBitOp4", RunBitOp4);
// runner4.run();
