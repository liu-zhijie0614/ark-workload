//
//  Splay.swift
//  JetStream2Swift
//
//  Created by iMac on 2023/10/23.
//

import Glibc

class Timer {
    private let CLOCK_REALTIME = 0
    private var time_spec = timespec()
    func getTime() -> Double {
        clock_gettime(Int32(CLOCK_REALTIME),&time_spec)
        return Double(time_spec.tv_sec * 1_000_000 + time_spec.tv_nsec / 1_000)/1000
    }
}

class PayloadTree0 {
    var array: Array<Int>
    var str: String
    init(_ array: Array<Int>, _ str: String) {
        self.array = array
        self.str = str
    }
}

class PayloadTreeDepth {
    var left: Any
    var right: Any
    init(_ left: Any, _ right: Any) {
        self.left = left
        self.right = right
    }
}

class Splay {
    // Configuration.
    let kSplayTreeSize = 8000
    let kSplayTreeModifications = 80
    let kSplayTreePayloadDepth = 5
    var splayTree: SplayTree? = nil
    var splaySampleTimeStart = 0.0
    var splaySamples:[Double] = []
    
    func GeneratePayloadTree(_ depth: Int, _ tag: String) -> Any {
        if depth == 0 {
            return PayloadTree0([0, 1, 2, 3, 4, 5, 6, 7, 8, 9], "String for key " + tag + " in leaf node")
        } else {
            return PayloadTreeDepth(GeneratePayloadTree(depth - 1, tag), GeneratePayloadTree(depth - 1, tag))
        }
    }
    
    func GenerateKey() -> Double {
        // The benchmark framework guarantees that Math.random is
        // deterministic; see base.js.
        return Double.random(in: 0.0...1.0)
    }
    
    func SplayUpdateStats(_ time: Double) {
        let pause = time - splaySampleTimeStart
        splaySampleTimeStart = time
        splaySamples.append(pause)
    }
    
    func InsertNewNode() -> Double {
        // Insert new node with a unique key.
        var key: Double?
        repeat {
            key = GenerateKey()
        } while splayTree?.find(key ?? 0) != nil
        
        let payload = GeneratePayloadTree(kSplayTreePayloadDepth, String(key ?? 0))
        splayTree?.insert(key: key ?? 0, value: payload)
        return key ?? 0
    }
    // @Setup
    func SplaySetup() {
        // Check if the platform has the performance.now high resolution timer.
        // If not, throw exception and quit.
        if Timer().getTime() == 0 {
            //deBugLog("PerformanceNowUnsupported")
            return
        }
        
        splayTree = SplayTree()
        splaySampleTimeStart = Timer().getTime()
        for i in 0..<kSplayTreeSize {
            _ = InsertNewNode()
            if (i + 1) % 20 == 19 {
                SplayUpdateStats(Timer().getTime())
            }
        }
    }
    // @Teardown
    func SplayTearDown() {
        // Allow the garbage collector to reclaim the memory
        // used by the splay tree no matter how we exit the
        // tear down function.
        let keys = splayTree?.exportKeys()
        splayTree = nil
        splaySamples = []
        // Verify that the splay tree has the right size.
        let length = keys?.count ?? 0
        if length != kSplayTreeSize {
            return
        }
        
        // Verify that the splay tree has sorted, unique keys.
        for i in 0..<length - 1 {
            if let key0 = keys?[i], let key1 = keys?[i+1], key0 >= key1 {
                return
            }
        }
    }
    // @Benchmark
    func SplayRun() {
        // Replace a few nodes in the splay tree.
        for _ in 0..<kSplayTreeModifications {
            let key = InsertNewNode()
            let greatest = splayTree?.findGreatestLessThan(key)
            if greatest == nil {
                _ = splayTree?.remove(key: key)
            } else {
                _ = splayTree?.remove(key: greatest!.key)
            }
        }
        SplayUpdateStats(Timer().getTime())
    }
}

/**
 ** Constructs a Splay tree.  A splay tree is a self-balancing binary
 ** search tree with the additional property that recently accessed
 ** elements are quick to access again. It performs basic operations
 ** such as insertion, look-up and removal in O(log(n)) amortized time.
 **
 ** @constructor
 **/
class SplayTree {
    /**
     * Pointer to the root node of the tree.
     *
     * @type {SplayTree.Node}
     * @private
     */
    var root_:Node? = nil
    /**
     * @return {boolean} Whether the tree is empty.
     */
    func isEmpty() -> Bool {
        return root_ == nil
    }
    /**
   ** Inserts a node into the tree with the specified key and value if
   ** the tree does not already contain a node with the specified key. If
   ** the value is inserted, it becomes the root of the tree.
   *
   ** @param {number} key Key to insert into the tree.
   ** @param {*} value Value to insert into the tree.
   **/
    func insert(key: Double, value: Any) {
        if isEmpty() {
            root_ = Node(key: key, value: value)
            return
        }
        // Splay on the key to move the last node on the search path for
        // the key to the root of the tree.
        splay(key)
        if let rootKey = root_?.key, key == rootKey {
            return
        }
        let node = Node(key: key, value: value)
        if key > root_!.key {
            node.left = root_
            node.right = root_?.right
            root_?.right = nil
        } else {
            node.right = root_
            node.left = root_?.left
            root_?.left = nil
        }
        root_ = node
    }
    /**
   ** Removes a node with the specified key from the tree if the tree
   ** contains a node with this key. The removed node is returned. If the
   ** key is not found, an exception is thrown.
   *
   ** @param {number} key Key to find and remove from the tree.
   ** @return {SplayTree.Node} The removed node.
   **/
    func remove(key: Double) -> Node? {
        if isEmpty() {
            return nil
        }
        splay(key)
        
        if root_?.key != key {
            return nil
        }
        let removed = root_
        if root_!.left == nil {
            root_ = root_?.right
        } else {
            let right = root_?.right
            root_ = root_?.left
            // Splay to make sure that the new root has an empty right child.
            splay(key)
            // Insert the original right child as the right child of the new root.
            root_?.right = right
        }
        return removed!
    }
    /**
     * Returns the node having the specified key or null if the tree doesn't contain
     * a node with the specified key.
     *
     * @param {number} key Key to find in the tree.
     * @return {SplayTree.Node} Node having the specified key.
     */
    func find(_ key: Double) -> Node? {
        if isEmpty() {
            return nil
        }
        splay(key)
        return root_?.key == key ? root_ : nil
    }
    /**
     * @return {SplayTree.Node} Node having the maximum key value.
     */
    func findMax(_ opt_startNode: Node? = nil) -> Node? {
        if isEmpty() {
            return nil
        }
        var current = opt_startNode ?? root_!
        while let rightChild = current.right {
            current = rightChild
        }
        return current
    }
    /**
     * @return {SplayTree.Node} Node having the maximum key value that
     *     is less than the specified key value.
     */
    func findGreatestLessThan(_ key: Double) -> Node? {
        if isEmpty() {
            return nil
        }
        // Splay on the key to move the node with the given key or the last
        // node on the search path to the top of the tree.
        splay(key)
        // Now the result is either the root node or the greatest node in
        // the left subtree.
        if root_!.key < key {
            return root_
        } else if let left = root_!.left {
            return findMax(left)
        } else {
            return nil
        }
    }
    /**
     * @return {Array<*>} An array containing all the keys of tree's nodes.
     */
    func exportKeys() -> [Double] {
        var result: [Double] = []
        if !isEmpty() {
            root_?.traverse_(f: { node in
                result.append(node.key)
            })
        }
        return result
    }
    /**
   ** Perform the splay operation for the given key. Moves the node with
   ** the given key to the top of the tree.  If no node has the given
   ** key, the last node on the search path is moved to the top of the
   ** tree. This is the simplified top-down splaying algorithm from:
   ** "Self-adjusting Binary Search Trees" by Sleator and Tarjan
   *
   ** @param {number} key Key to splay the tree on.
   ** @private
   **/
    func splay(_ key: Double) {
        if isEmpty() {
            return
        }
    	// Create a dummy node.  The use of the dummy node is a bit;
    	// counter-intuitive: The right child of the dummy node will hold;
    	// the L tree of the algorithm.  The left child of the dummy node;
    	// will hold the R tree of the algorithm.  Using a dummy node, left
    	// and right will always be nodes and we avoid special cases;
        var dummy:Node?, left:Node?, right:Node?
        dummy = Node(key:0,value:0)
        left = dummy
        right = dummy
        var current = root_!
        while true {
            if key < current.key {
                if current.left == nil {
                    break
                }
                if key < current.left?.key ?? 0 {
                    // Rotate right.
                    let temp = current.left!
                    current.left = temp.right
                    temp.right = current
                    current = temp
                    if current.left == nil {
                        break
                    }
                }
                // Link right.
                right?.left = current
                right = current
                current = current.left!
            } else if key > current.key {
                if current.right == nil {
                    break
                }
                if key > current.right?.key ?? 0 {
                    // Rotate left.
                    let temp = current.right!
                    current.right = temp.left
                    temp.left = current
                    current = temp
                    if current.right == nil {
                        break
                    }
                }
                // Link left;
                left?.right = current
                left = current
                current = current.right!
            } else {
                break
            }
        }
        // Assemble.
        left?.right = current.left
        right?.left = current.right
        current.left = dummy?.right
        current.right = dummy?.left
        root_ = current
    }
}

/**
 * Constructs a Splay tree node.
 *
 * @param {number} key Key.
 * @param {*} value Value.
 */
class Node {
    var key: Double
    var value: Any
    var left: Node?
    var right: Node?
    
    init(key: Double, value: Any) {
        self.key = key
        self.value = value
        self.left = nil
        self.right = nil
    }
    /**
     * Performs an ordered traversal of the subtree starting at
     * this SplayTree.Node.
     *
     * @param {function(SplayTree.Node)} f Visitor function.
     * @private
     */
    func traverse_(f: (Node) -> Void) {
        var current:Node? = self
        while current != nil {
            if let left = current?.left {
                left.traverse_(f: f)
            }
            f(current!)
            current = current?.right
        }
    }
}

fileprivate func deBugLog(_ str: String) {
    let isLog = false
    if isLog {
        print(str)
    }
}

func SplayRunIteration() {
    let setupStart = Timer().getTime()
    let splay = Splay()
    splay.SplaySetup()
    let setupEnd = Timer().getTime()
    //deBugLog("setupTime: " + String(setupEnd - setupStart) + " ms")
    for _ in 0..<20 {
        for _ in 0..<50 {
            splay.SplayRun()
        }
    }
    splay.SplayTearDown()
    let splayEnd = Timer().getTime()
    print("splay: ms = " + String(splayEnd - setupStart))
}

SplayRunIteration()


