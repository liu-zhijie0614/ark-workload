import Glibc

class Benchmark {
    var _players: [Player] = []
    /// @State
    init() {
        self._players.append(Player("Player 1"))
        self._players.append(Player("Player 2"))
        self._players.append(Player("Player 3"))
        self._players.append(Player("Player 4"))
    }
    
    func runIteration() {
        playHands(self._players)
    }
    
    func validate()  {
        let isInBrowser = true
        if self._players.count != playerExpectations.count {
            fatalError("Expect \(playerExpectations.count), but actually have \(self._players.count)")
        }
        
        if isInBrowser {
            for playerIdx in 0 ..< playerExpectations.count {
                playerExpectations[playerIdx].validate(self._players[playerIdx])
            }
        }
    }
}

class PlayerExpectation {
    private let _wins: Int
    private let _handTypeCounts: [Int]
    /// @State
    init(_ wins: Int, _ handTypeCounts: [Int]) {
        self._wins = wins
        self._handTypeCounts = handTypeCounts
    }
    
    func validate(_ player: Player) {
        if player.wins != self._wins {
            fatalError("Expected \(player.name) to have \(self._wins) wins, but they have \(player.wins) wins")
        }
        
        let actualHandTypeCounts = player.handTypeCounts
        if self._handTypeCounts.count != actualHandTypeCounts.count {
            fatalError("Expected \(player.name) to have \(self._handTypeCounts.count) hand types, but they have \(actualHandTypeCounts.count) hand types")
        }
        
        for handTypeIdx in 0..<self._handTypeCounts.count {
            if self._handTypeCounts[handTypeIdx] != actualHandTypeCounts[handTypeIdx] {
                fatalError("Expected \(player.name) to have \(self._handTypeCounts[handTypeIdx]) \(PlayerExpectation._handTypes[handTypeIdx]) hands, but they have \(actualHandTypeCounts[handTypeIdx]) \(PlayerExpectation._handTypes[handTypeIdx]) hands")
            }
        }
    }
    
    private static let _handTypes: [String] = [
        "High Cards",
        "Pairs",
        "Two Pairs",
        "Three of a Kinds",
        "Straights",
        "Flushes",
        "Full Houses",
        "Four of a Kinds",
        "Straight Flushes",
        "Royal Flushes"
    ]
    
    
}

var playerExpectations: [PlayerExpectation] = []
playerExpectations.append(PlayerExpectation(59864, [120476, 101226, 11359, 5083, 982, 456, 370, 45, 3, 0]))
playerExpectations.append(PlayerExpectation(60020, [120166, 101440, 11452, 5096, 942, 496, 333, 67, 8, 0]))
playerExpectations.append(PlayerExpectation(60065, [120262, 101345, 11473, 5093, 941, 472, 335, 76, 3, 0]))
playerExpectations.append(PlayerExpectation(60064, [120463, 101218, 11445, 5065, 938, 446, 364, 58, 3, 0]))

class CardDeck {
    
    private var _cards: [String] = []
    /// @State
    init() {
        self.newDeck()
    }
    
    func newDeck() {
        // Make a shallow copy of a new deck
        self._cards = Array(CardDeck.newDeck)
    }
    
    func shuffle() {
        self.newDeck()
        
        var index = 52
        while index != 0 {
            // Select a random card
            let randomIndex = Int(Float.random(in: 0..<1) * Float(index))
            index -= 1
            
            // Swap the current card with the random card
            let tempCard = self._cards[index]
            self._cards[index] = self._cards[randomIndex]
            self._cards[randomIndex] = tempCard
        }
        
    }
    
    func dealOneCard() -> String?{
        return self._cards.removeFirst()
    }
    
    static func cardRank(_ card: String) -> Int {
        // This returns a numeric value for a card.
        // Ace is highest.
        
        let rankOfCard = card.unicodeScalars.first!.value & 0xf
        if rankOfCard == 0x1 { // Make Aces higher than Kings
            return 0xf
        }
        return Int(rankOfCard)
    }
    
    static func cardName(_ card: String) -> String {
        if type(of: card) == String.self{
            let result = (card.unicodeScalars.first?.value)!
            return CardDeck.rankNames[Int(result & 0xf)]
        }
        return ""
    }
    
    private static let rankNames = ["", "Ace", "2", "3", "4", "5", "6", "7", "8", "9", "10", "Jack", "", "Queen", "King"]
    private static let newDeck = [
        // Spades
        "\u{1f0a1}", "\u{1f0a2}", "\u{1f0a3}", "\u{1f0a4}", "\u{1f0a5}",
        "\u{1f0a6}", "\u{1f0a7}", "\u{1f0a8}", "\u{1f0a9}", "\u{1f0aa}",
        "\u{1f0ab}", "\u{1f0ad}", "\u{1f0ae}",
        // Hearts
        "\u{1f0b1}", "\u{1f0b2}", "\u{1f0b3}", "\u{1f0b4}", "\u{1f0b5}",
        "\u{1f0b6}", "\u{1f0b7}", "\u{1f0b8}", "\u{1f0b9}", "\u{1f0ba}",
        "\u{1f0bb}", "\u{1f0bd}", "\u{1f0be}",
        // Clubs
        "\u{1f0d1}", "\u{1f0d2}", "\u{1f0d3}", "\u{1f0d4}", "\u{1f0d5}",
        "\u{1f0d6}", "\u{1f0d7}", "\u{1f0d8}", "\u{1f0d9}", "\u{1f0da}",
        "\u{1f0db}", "\u{1f0dd}", "\u{1f0de}",
        // Diamonds
        "\u{1f0c1}", "\u{1f0c2}", "\u{1f0c3}", "\u{1f0c4}", "\u{1f0c5}",
        "\u{1f0c6}", "\u{1f0c7}", "\u{1f0c8}", "\u{1f0c9}", "\u{1f0ca}",
        "\u{1f0cb}", "\u{1f0cd}", "\u{1f0ce}"
    ]
}
/// @Generator
class Hand {
    
    private var _rank: Int = 0
    private var _cards: [String] = []
    /// @State
    init() {
        self.clear()
    }
    
    func clear() {
        self._cards = []
        self._rank = 0
    }
    
    func takeCard(_ card: String) {
        self._cards.append(card)
    }
    
    func score() {
        
        // Sort highest rank to lowest
        self._cards.sort { (a, b) -> Bool in
            return CardDeck.cardRank(a) - CardDeck.cardRank(b) > 0
        }
        
        let handString = self._cards.joined()
        
        let flushResult = match(handString, Hand.FlushRegExp!)
        let straightResult = match(handString, Hand.StraightRegExp!)
        let ofAKindResult =  match(handString, Hand.OfAKindRegExp!)
        
        if (flushResult.count != 0)  {
            if (straightResult.count != 0) {
                if(straightResult.count > 1) {
                    self._rank = Hand.RoyalFlush
                }
                else{
                    self._rank = Hand.StraightFlush
                }
            }
            else{
                self._rank = Hand.Flush
            }
            self._rank |= (CardDeck.cardRank(self._cards[0]) << 16) | (CardDeck.cardRank(self._cards[1]) << 12)
        }
        else if (straightResult.count != 0)  {
            self._rank = Hand.Straight | (CardDeck.cardRank(self._cards[0]) << 16) | (CardDeck.cardRank(self._cards[1]) << 12)
        }
        else if (ofAKindResult.count != 0)  {
            // When comparing lengths, a matched unicode character has a length of 1.
            // Therefore expected lengths are doubled, e.g a pair will have a match length of 2.
            if ( ofAKindResult[0].keys.first!.count == 4) {
                self._rank = Hand.FourOfAKind |  CardDeck.cardRank(self._cards[0])
            }
            else{
                // Found pair or three of a kind.  Check for two pair or full house.
                let firstOfAKind = ofAKindResult[0].keys.first! as String
                let remainingCardsIndex = handString.distance(from: handString.startIndex, to: ofAKindResult[0].values.first!.lowerBound) + firstOfAKind.count
                let secondOfAKindResult = match(String(handString[firstOfAKind.endIndex...]), Hand.OfAKindRegExp!)
                if remainingCardsIndex <= 3 , secondOfAKindResult.count != 0 {
                    let secondOfAKind = secondOfAKindResult[0].keys.first! as String
                    // Three of a Kinds + Pairs, or Pairs + Three of a Kinds
                    if (firstOfAKind.count == 3 && secondOfAKind.count == 2)
                        || (firstOfAKind.count == 2 && secondOfAKind.count == 3) {
                        var threeOfAKindCardRank = 0
                        var twoOfAKindCardRank = 0
                        
                        if firstOfAKind.count == 3 {
                            threeOfAKindCardRank = CardDeck.cardRank(firstOfAKind.slice(0, 1))
                            twoOfAKindCardRank = CardDeck.cardRank(secondOfAKind.slice(0, 1))
                        } else {
                            threeOfAKindCardRank = CardDeck.cardRank(secondOfAKind.slice(0, 1))
                            twoOfAKindCardRank = CardDeck.cardRank(firstOfAKind.slice(0, 1))
                        }
                        self._rank = Hand.FullHouse | (threeOfAKindCardRank << 16) | (threeOfAKindCardRank << 12) | (threeOfAKindCardRank << 8) | (twoOfAKindCardRank << 4) | twoOfAKindCardRank
                    }
                    else if firstOfAKind.count == 2 , secondOfAKind.count == 2 {
                        
                        let firstPairCardRank = CardDeck.cardRank(firstOfAKind.slice(0, 1))
                        let secondPairCardRank = CardDeck.cardRank(secondOfAKind.slice(0, 1))
                        var otherCardRank = 0
                        // Due to sorting, the other card is at index 0, 2 or 4
                        if firstOfAKind.unicodeScalars.first!.value == handString.unicodeScalars.first!.value {
                            if secondOfAKind.unicodeScalars.first!.value == handString.unicodeScalars[handString.index(handString.startIndex, offsetBy: 2)].value {
                                otherCardRank = CardDeck.cardRank(handString.slice(4, 5))
                            } else {
                                otherCardRank = CardDeck.cardRank(handString.slice(2, 3))
                            }
                        } else {
                            otherCardRank = CardDeck.cardRank(handString.slice(0, 1))
                        }
                        self._rank = Hand.TwoPair | firstPairCardRank << 16 | firstPairCardRank << 12 | secondPairCardRank << 8 | secondPairCardRank << 4 | otherCardRank
                    }
                }
                else{
                    let ofAKindCardRank =  CardDeck.cardRank(firstOfAKind.slice(0, 1))
                    var otherCardsRank = 0
                    for card in self._cards {
                        let cardRank = CardDeck.cardRank(card)
                        if (cardRank != ofAKindCardRank) {
                            otherCardsRank = (otherCardsRank << 4) | cardRank
                        }
                    }
                    if(firstOfAKind.count == 3){
                        self._rank = Hand.ThreeOfAKind | ofAKindCardRank << 16 | ofAKindCardRank << 12 | ofAKindCardRank << 8 | otherCardsRank
                    }
                    else{
                        self._rank = Hand.Pair | ofAKindCardRank << 16 | ofAKindCardRank << 12 | otherCardsRank
                    }
                }
            }
        }
        else{
            self._rank = 0
            for card in self._cards {
                let cardRank = CardDeck.cardRank(card)
                self._rank = (self.rank << 4) | cardRank
            }
        }
    }
    
    var rank: Int {
        return self._rank
    }
    
    func toString () -> String {
        return self._cards.joined()
    }
    
    private static let FlushRegExp = try? Regex("([\u{1f0a1}-\u{1f0ae}]{5})|([\u{1f0b1}-\u{1f0be}]{5})|([\u{1f0c1}-\u{1f0ce}]{5})|([\u{1f0d1}-\u{1f0de}]{5})")
    private static let StraightRegExp =  try? Regex("([\u{1f0a1}\u{1f0b1}\u{1f0d1}\u{1f0c1}][\u{1f0ae}\u{1f0be}\u{1f0de}\u{1f0ce}][\u{1f0ad}\u{1f0bd}\u{1f0dd}\u{1f0cd}][\u{1f0ab}\u{1f0bb}\u{1f0db}\u{1f0cb}][\u{1f0aa}\u{1f0ba}\u{1f0da}\u{1f0ca}])|[\u{1f0ae}\u{1f0be}\u{1f0de}\u{1f0ce}][\u{1f0ad}\u{1f0bd}\u{1f0dd}\u{1f0cd}][\u{1f0ab}\u{1f0bb}\u{1f0db}\u{1f0cb}][\u{1f0aa}\u{1f0ba}\u{1f0da}\u{1f0ca}][\u{1f0a9}\u{1f0b9}\u{1f0d9}\u{1f0c9}]|[\u{1f0ad}\u{1f0bd}\u{1f0dd}\u{1f0cd}][\u{1f0ab}\u{1f0bb}\u{1f0db}\u{1f0cb}][\u{1f0aa}\u{1f0ba}\u{1f0da}\u{1f0ca}][\u{1f0a9}\u{1f0b9}\u{1f0d9}\u{1f0c9}][\u{1f0a8}\u{1f0b8}\u{1f0d8}\u{1f0c8}]|[\u{1f0ab}\u{1f0bb}\u{1f0db}\u{1f0cb}][\u{1f0aa}\u{1f0ba}\u{1f0da}\u{1f0ca}][\u{1f0a9}\u{1f0b9}\u{1f0d9}\u{1f0c9}][\u{1f0a8}\u{1f0b8}\u{1f0d8}\u{1f0c8}][\u{1f0a7}\u{1f0b7}\u{1f0d7}\u{1f0c7}]|[\u{1f0aa}\u{1f0ba}\u{1f0da}\u{1f0ca}][\u{1f0a9}\u{1f0b9}\u{1f0d9}\u{1f0c9}][\u{1f0a8}\u{1f0b8}\u{1f0d8}\u{1f0c8}][\u{1f0a7}\u{1f0b7}\u{1f0d7}\u{1f0c7}][\u{1f0a6}\u{1f0b6}\u{1f0d6}\u{1f0c6}]|[\u{1f0a9}\u{1f0b9}\u{1f0d9}\u{1f0c9}][\u{1f0a8}\u{1f0b8}\u{1f0d8}\u{1f0c8}][\u{1f0a7}\u{1f0b7}\u{1f0d7}\u{1f0c7}][\u{1f0a6}\u{1f0b6}\u{1f0d6}\u{1f0c6}][\u{1f0a5}\u{1f0b5}\u{1f0d5}\u{1f0c5}]|[\u{1f0a8}\u{1f0b8}\u{1f0d8}\u{1f0c8}][\u{1f0a7}\u{1f0b7}\u{1f0d7}\u{1f0c7}][\u{1f0a6}\u{1f0b6}\u{1f0d6}\u{1f0c6}][\u{1f0a5}\u{1f0b5}\u{1f0d5}\u{1f0c5}][\u{1f0a4}\u{1f0b4}\u{1f0d4}\u{1f0c4}]|[\u{1f0a7}\u{1f0b7}\u{1f0d7}\u{1f0c7}][\u{1f0a6}\u{1f0b6}\u{1f0d6}\u{1f0c6}][\u{1f0a5}\u{1f0b5}\u{1f0d5}\u{1f0c5}][\u{1f0a4}\u{1f0b4}\u{1f0d4}\u{1f0c4}][\u{1f0a3}\u{1f0b3}\u{1f0d3}\u{1f0c3}]|[\u{1f0a6}\u{1f0b6}\u{1f0d6}\u{1f0c6}][\u{1f0a5}\u{1f0b5}\u{1f0d5}\u{1f0c5}][\u{1f0a4}\u{1f0b4}\u{1f0d4}\u{1f0c4}][\u{1f0a3}\u{1f0b3}\u{1f0d3}\u{1f0c3}][\u{1f0a2}\u{1f0b2}\u{1f0d2}\u{1f0c2}]|[\u{1f0a1}\u{1f0b1}\u{1f0d1}\u{1f0c1}][\u{1f0a5}\u{1f0b5}\u{1f0d5}\u{1f0c5}][\u{1f0a4}\u{1f0b4}\u{1f0d4}\u{1f0c4}][\u{1f0a3}\u{1f0b3}\u{1f0d3}\u{1f0c3}][\u{1f0a2}\u{1f0b2}\u{1f0d2}\u{1f0c2}]")
    private static let OfAKindRegExp =  try? Regex("(?:[\u{1f0a1}\u{1f0b1}\u{1f0d1}\u{1f0c1}]{2,4})|(?:[\u{1f0ae}\u{1f0be}\u{1f0de}\u{1f0ce}]{2,4})|(?:[\u{1f0ad}\u{1f0bd}\u{1f0dd}\u{1f0cd}]{2,4})|(?:[\u{1f0ab}\u{1f0bb}\u{1f0db}\u{1f0cb}]{2,4})|(?:[\u{1f0aa}\u{1f0ba}\u{1f0da}\u{1f0ca}]{2,4})|(?:[\u{1f0a9}\u{1f0b9}\u{1f0d9}\u{1f0c9}]{2,4})|(?:[\u{1f0a8}\u{1f0b8}\u{1f0d8}\u{1f0c8}]{2,4})|(?:[\u{1f0a7}\u{1f0b7}\u{1f0d7}\u{1f0c7}]{2,4})|(?:[\u{1f0a6}\u{1f0b6}\u{1f0d6}\u{1f0c6}]{2,4})|(?:[\u{1f0a5}\u{1f0b5}\u{1f0d5}\u{1f0c5}]{2,4})|(?:[\u{1f0a4}\u{1f0b4}\u{1f0d4}\u{1f0c4}]{2,4})|(?:[\u{1f0a3}\u{1f0b3}\u{1f0d3}\u{1f0c3}]{2,4})|(?:[\u{1f0a2}\u{1f0b2}\u{1f0d2}\u{1f0c2}]{2,4})")
    
    private static let RoyalFlush = 0x900000
    private static let StraightFlush = 0x800000
    private static let FourOfAKind = 0x700000
    private static let FullHouse = 0x600000
    private static let Flush = 0x500000
    private static let Straight = 0x400000
    private static let ThreeOfAKind = 0x300000
    private static let TwoPair = 0x200000
    private static let Pair = 0x100000
    
    func match(_ input: String, _ regex: Regex<AnyRegexOutput>) -> [Dictionary<String, Range<String.Index>>] {
        let matches = input.matches(of: regex)
        var results: [Dictionary<String, Range<String.Index>>] = []
        for match in matches {
            let range = match.range
            let matchString = String(input[range])
            var dict = Dictionary<String, Range<String.Index>>()
            dict[matchString] = match.range
            results.append(dict)
        }
        return results;
    }
    
}

class Player: Hand {
    private var _name : String
    private var _wins : Int
    private var _handTypeCounts : Array<Int>
    /// @State
    init(_ name : String) {
        self._name = name
        self._wins = 0
        self._handTypeCounts = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
        super.init()
    }
    
    func scoreHand() {
        self.score()
        let handType = self.rank >> 20
        self._handTypeCounts[handType] += 1
    }
    
    func wonHand() {
        self._wins += 1
    }
    
    var name :String {
        return self._name
    }
    
    var hand :String {
        return super.toString()
    }
    
    var wins : Int {
        return self._wins
    }
    
    var handTypeCounts : Array<Int> {
        return self._handTypeCounts
    }
    
}

func playHands(_ players:[Player]) {
    
    let cardDeck =  CardDeck()
    var handsPlayed = 0
    var highestRank = 0
    
    repeat {
        cardDeck.shuffle()
        for  player in players {
            player.clear()
        }
        for _ in 1...5 {
            for  player in players {
                player.takeCard(cardDeck.dealOneCard()!)
            }
        }
        
        for  player in players {
            player.scoreHand()
        }
        handsPlayed += 1
        
        highestRank = 0
        
        for  player in players {
            if player.rank > highestRank {
                highestRank = player.rank
            }
        }
        
        for  player in players {
            // We count ties as wins for each player.
            if player.rank == highestRank {
                player.wonHand()
            }
        }
    } while (handsPlayed < 2000)
    
}
/// @Benchmark
func run(_ logsEnabled:Bool) {
    let benchmark = Benchmark()
    let startTime = Timer().getTime()
    for _ in 0..<20 {
        benchmark.runIteration()
    }
    let endTime = Timer().getTime()
    let time = (endTime - startTime) / 1000
    print("uni-poker: ms =",time)
    if logsEnabled {
        for index in benchmark._players.indices {
            print("players \(index) wins:\(benchmark._players[index].wins) handTypeCounts:\(benchmark._players[index].handTypeCounts)")
        }
    }
}

class Timer {
    // private let CLOCK_REALTIME = 0
    private var time_spec = timespec()
    func getTime() -> Double {
        //clock_gettime(Int32(CLOCK_REALTIME),&time_spec)
        clock_gettime(CLOCK_REALTIME,&time_spec)
        return Double(time_spec.tv_sec * 1_000_000 + time_spec.tv_nsec / 1_000)
    }
}

extension String {
    func slice(_ start: Int, _ end: Int) -> String {
        let startIndex = self.index(self.startIndex, offsetBy: start)
        let endIndex = self.index(self.startIndex, offsetBy: end)
        return String(self[startIndex..<endIndex])
    }
}

run(false)
