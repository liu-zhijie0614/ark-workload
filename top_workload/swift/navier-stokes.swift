import Glibc

var solver: FluidField?
var nsFrameCounter = 0

func runNavierStokes() {
    solver?.update()
    nsFrameCounter+=1
    if nsFrameCounter == 15 {
        checkResult((solver?.getDens())!)
    }
}

func checkResult(_ dens: [Double]) {
    var result: Int = 0
    for i in 7000..<7100 {
        let sss = dens[i] * 10
        result += Int(sss)
        //DeBugLog("single result[\(i)]= \(sss)")
    }
    
    if result != 77 {
        //DeBugLog("nsFrameCounter= \(nsFrameCounter) checksum failed")
    } else {
        //DeBugLog("nsFrameCounter= \(nsFrameCounter) checksum success")
    }
}
// @Setup
func setupNavierStokes() {
    solver = FluidField()
    solver?.setResolution(128, 128)
    solver?.setIterations(20)
    solver?.reset()
}

var framesTillAddingPoints = 0;
var framesBetweenAddingPoints = 5;

func prepareFrame(_ d: inout [Double], _ u: inout [Double], _ v: inout [Double]) {
    if framesTillAddingPoints == 0 {
        solver?.addPoints(&d, &u, &v)
        framesTillAddingPoints = framesBetweenAddingPoints
        framesBetweenAddingPoints+=1
    } else {
        framesTillAddingPoints-=1
    }
}

class FluidField {
    
    func addFields(_ x: inout [Double], _ s: [Double], _ dt: Double) {
        for i in 0..<size {
            x[i] += dt * s[i]
        }
    }
    
    func set_bnd(_ b: Int, _ x: inout [Double]) {
        if b == 1 {
            for i in 1...width {
                x[i] = x[i+rowSize]
                x[i+(height+1)*rowSize] = x[i+height*rowSize]
            }
            for j in 1...height {
                x[j*rowSize] = -x[1+j*rowSize]
                x[(width+1)+j*rowSize] = -x[width+j*rowSize]
            }
        } else if b == 2 {
            for i in 1...width {
                x[i] = -x[i+rowSize]
                x[i+(height+1)*rowSize] = -x[i+height*rowSize]
            }
            for j in 1...height {
                x[j*rowSize] = x[1+j*rowSize]
                x[(width+1)+j*rowSize] = x[width+j*rowSize]
            }
        } else {
            for i in 1...width {
                x[i] = x[i+rowSize]
                x[i+(height+1)*rowSize] = x[i+height*rowSize]
            }
            for j in 1...height {
                x[j*rowSize] = x[1+j*rowSize]
                x[(width+1)+j*rowSize] = x[width+j*rowSize]
            }
        }
        let maxEdge = (height + 1) * rowSize
        x[0] = 0.5*(x[1]+x[rowSize])
        x[maxEdge]           = 0.5 * (x[1 + maxEdge] + x[height * rowSize])
        x[(width+1)]         = 0.5 * (x[width] + x[(width + 1) + rowSize])
        x[(width+1)+maxEdge] = 0.5 * (x[width + maxEdge] + x[(width + 1) + height * rowSize])
    }
    
    func lin_solve(_ b: Int, _ x: inout [Double], _ x0: [Double], _ a: Double, _ c: Double) {
        if a == 0, c == 1 {
            for j in 1...height {
                var currentRow = j * rowSize
                currentRow+=1
                for _ in 0..<width {
                    x[currentRow] = x0[currentRow]
                    currentRow+=1
                }
            }
            set_bnd(b, &x);
        } else {
            let invC = 1.0 / c
            for  _ in 0..<iterations {
                for j in 1...height {
                    var lastRow = (j - 1) * rowSize
                    var currentRow = j * rowSize
                    var nextRow = (j + 1) * rowSize
                    var lastX = x[currentRow]
                    var changeRow = currentRow
                    currentRow+=1
                    for _ in 1...width {
                        let args1 = x0[currentRow]
                        changeRow = currentRow
                        currentRow+=1
                        lastRow += 1
                        nextRow += 1
                        let args2 = a*(lastX+x[currentRow]+x[lastRow]+x[nextRow])
                        x[changeRow] = (args1 + args2) * Double(invC)
                        lastX = x[changeRow]
                    }
                }
                set_bnd(b, &x)
            }
        }
    }
    
    func diffuse(_ b: Int, _ x: inout [Double], _ x0: [Double], _ dt: Double) {
        let a = 0.0
        lin_solve(b, &x, x0, a, 1+4*a)
    }
    
    func lin_solve2(_ x: inout [Double], _ x0: inout [Double], _ y: inout [Double], _ y0: inout [Double], _ a: Int, _ c: Double) {
        if a == 0, c == 1 {
            for j in 1...height {
                var currentRow = j * rowSize
                currentRow+=1
                for _ in 0..<width {
                    x[currentRow] = x0[currentRow]
                    y[currentRow] = y0[currentRow]
                    currentRow+=1
                }
            }
            set_bnd(1, &x)
            set_bnd(2, &y)
        } else {
            let invC = 1 / c;
            for  _ in 0..<iterations {
                for j in 1...height {
                    var lastRow = (j - 1) * rowSize
                    var currentRow = j * rowSize
                    var nextRow = (j + 1) * rowSize
                    var lastX = x[currentRow]
                    var lastY = y[currentRow]
                    currentRow+=1
                    for _ in 1...width {
                        currentRow+=1
                        lastRow += 1
                        nextRow += 1
                        let test = x0[currentRow] + Double(a)*(lastX+x[currentRow]+x[lastRow]+x[nextRow])
                        x[currentRow] = test * Double(invC)
                        lastX = x[currentRow]
                        let testy = y0[currentRow] + Double(a)*(lastY+y[currentRow]+y[lastRow]+y[nextRow])
                        y[currentRow] = testy * Double(invC)
                        lastY = y[currentRow]
                    }
                }
                set_bnd(1, &x)
                set_bnd(2, &y)
            }
        }
    }
    
    func diffuse2(_ x: inout [Double], _ x0: inout [Double], _ y: inout [Double], _ y0: inout [Double], _ dt: Double) {
        let a = 0
        lin_solve2(&x, &x0, &y, &y0, a, Double(1+4*a))
    }
    
    func advect(_ b: Int, _ d: inout [Double], _ d0: [Double], _ u: [Double], _ v: [Double], _ dt: Double) {
        let aWidth = Double(width)
        let aHeight = Double(height)
        
        let Wdt0 = dt * aWidth
        let Hdt0 = dt * aHeight
        let Wp5 = aWidth + 0.5;
        let Hp5 = aHeight + 0.5;
        for j in 1...height {
            var pos = j*rowSize
            for i in 1...width {
                pos+=1
                var x = Double(i) - Wdt0 * u[pos]
                var y = Double(j) - Hdt0 * v[pos]
                if x < 0.5 {
                    x = 0.5
                } else if x > Wp5 {
                    x = Wp5
                }
                let i0 = Int(x) | 0
                let i1 = i0 + 1
                if y < 0.5 {
                    y = 0.5
                } else if y > Hp5 {
                    y = Hp5
                }
                let j0 = Int(y) | 0
                let j1 = j0 + 1
                let s1 = x - Double(i0)
                let s0 = 1 - s1
                let t1 = y - Double(j0)
                let t0 = 1 - t1
                let row1 = j0 * rowSize
                let row2 = j1 * rowSize
                d[pos] = s0 * (t0 * d0[i0 + row1] + t1 * d0[i0 + row2]) + s1 * (t0 * d0[i1 + row1] + t1 * d0[i1 + row2])
            }
        }
       set_bnd(b, &d)
    }
    
    func project(_ u: inout [Double], _ v: inout [Double], _ p: inout [Double], _ div: inout [Double]) {
        let h = -0.5 / sqrt(Double(width*height))
        for j in 1...height {
            let row = j * rowSize
            var previousRow = (j - 1) * rowSize
            var prevValue = row - 1
            var currentRow = row
            var nextValue = row + 1
            var nextRow = (j + 1) * rowSize
            for _ in  1...width {
                currentRow+=1
                nextValue+=1
                nextRow+=1
                previousRow+=1
                prevValue += 1
                div[currentRow] = h * (u[nextValue] - u[prevValue] + v[nextRow] - v[previousRow])
                p[currentRow] = 0
            }
        }
        set_bnd(0, &div)
        set_bnd(0, &p)
        lin_solve(0, &p, div, 1, 4)
        let wScale = 0.5 * Double(width)
        let hScale = 0.5 * Double(height)
        for j in 1...height {
            var prevPos = j * rowSize - 1
            var currentPos = j * rowSize
            var nextPos = j * rowSize + 1
            var prevRow = (j - 1) * rowSize
            var nextRow = (j + 1) * rowSize
            for _ in 1...width {
                currentPos+=1
                nextPos+=1
                prevPos+=1
                nextRow+=1
                prevRow+=1
                u[currentPos] -= wScale * (p[nextPos] - p[prevPos])
                v[currentPos] -= hScale * (p[nextRow] - p[prevRow])
            }
        }
        set_bnd(1, &u)
        set_bnd(2, &v)
    }
   
    func dens_step(_ x: inout [Double], _ x0: inout [Double], _ u: [Double], _ v: [Double], _ dt: Double) {
        addFields(&x, x0, dt)
        diffuse(0, &x0, x, dt)
        advect(0, &x, x0, u, v, dt)
    }
    
    func vel_step(_ u: inout [Double], _ v: inout [Double], _ u0: inout [Double], _ v0: inout [Double], _ dt: Double) {
        addFields(&u, u0, dt)
        addFields(&v, v0, dt)
        let temp = u0; u0 = u; u = temp;
        let temp1 = v0; v0 = v; v = temp1;
        diffuse2(&u, &u0, &v, &v0, dt)
        project(&u, &v, &u0, &v0)
        let temp2 = u0; u0 = u; u = temp2;
        let temp3 = v0; v0 = v; v = temp3;
        advect(1, &u, u0, u0, v0, dt)
        advect(2, &v, v0, u0, v0, dt)
        project(&u, &v, &u0, &v0)
    }
    
    func queryUI(_ d: inout [Double], _ u: inout [Double], _ v: inout [Double]) {
        for i in 0..<size {
            u[i] = 0.0
            v[i] = 0.0
            d[i] = 0.0
        }
        prepareFrame(&d, &u, &v)
    }
    
    func update() {
        queryUI(&dens_prev, &u_prev, &v_prev)
        vel_step(&u, &v, &u_prev, &v_prev, dt)
        dens_step(&dens, &dens_prev, u, v, dt)
    }
    
    func setIterations(_ iters: Int){
        if iters > 0, iters <= 100 {
            self.iterations = iters
        }
    }
  
    init(canvas: Any? = nil) {
        setResolution(64, 64)
    }
    var iterations = 10
    var visc: Float = 0.5
    var dt = 0.1
    var dens: [Double] = Array()
    var dens_prev: [Double] = Array()
    var u: [Double] = Array()
    var u_prev: [Double] = Array()
    var v: [Double] = Array()
    var v_prev: [Double] = Array()
    var width: Int = 0
    var height: Int = 0
    var rowSize: Int = 0
    var size: Int = 0
    func reset() {
        rowSize = width + 2
        size = (width+2)*(height+2)
        dens = [Double](repeating: 0, count: size)
        dens_prev = [Double](repeating: 0, count: size)
        u_prev = [Double](repeating: 0, count: size)
        v_prev = [Double](repeating: 0, count: size)
        u = [Double](repeating: 0, count: size)
        v = [Double](repeating: 0, count: size)
    }
    
    func setResolution(_ hRes: Int, _ wRes: Int) {
        let res = wRes*hRes
        if res > 0 && res < 1000000 && (wRes != width || hRes != height) {
            width = wRes
            height = hRes
            reset()
        }
    }
    
    func getDens() -> [Double] {
        return self.dens
    }
    
    func addPoints(_ md: inout [Double], _ mu: inout [Double], _ mv: inout [Double]) {
        let n = 64
        for i in 1...n {
            setVelocity(&mu, &mv, i, i, Double(n), Double(n))
            setDensity(&md, i, i, 5)
            setVelocity(&mu, &mv, i, n-i, -Double(n), -Double(n))
            setDensity(&md, i, n-i, 20)
            setVelocity(&mu, &mv, 128-i, n+i, -Double(n), -Double(n))
            setDensity(&md, 128-i, n+i, 30)
        }
    }
    
    func setDensity(_ den:inout[Double], _ x: Int, _ y: Int, _ d: Double) {
        den[(x + 1) + (y + 1) * rowSize] = d
    }
        
    func setVelocity( _ u:inout [Double], _ v:inout [Double], _ x: Int, _ y: Int, _ xv: Double, _ yv: Double) {
        u[(x + 1) + (y + 1) * rowSize] = xv
        v[(x + 1) + (y + 1) * rowSize] = yv
    }
}

// @Benchmark
class Benchmark {
    init() {
        setupNavierStokes()
    }
    func runIteration(with index: Int) {
        runNavierStokes()
    }
}

/// @Benchmark
func run() {
    let start = Timer().now
    let bechMark = Benchmark()
    for _ in 1...120 {
        bechMark.runIteration(with: 0)
    }
    let end = Timer().now
    print("navier-stokes: ms = \(end - start)")
}

run()

class Timer {
    private let CLOCK_REALTIME = 0
    private var time_spec = timespec()
    func getTime() -> Double {
        clock_gettime(Int32(CLOCK_REALTIME),&time_spec)
        return Double(time_spec.tv_sec * 1_000_000 + time_spec.tv_nsec / 1_000)
    }
    
}
extension Timer {
    var now: Double {
        return getTime()/1000.0
    }
}
let isDebug : Bool = false
func DeBugLog(_ msg : Any , file: String = #file, line: Int = #line, fn: String = #function){
    if isDebug {
        let prefix = "\(msg)"
        print(prefix)
    }
}
