/*
 *  Licensed to the Apache Software Foundation (ASF) under one or more
 *  contributor license agreements.  See the NOTICE below for additional
 *  information regarding copyright ownership.
 *  The ASF licenses this file to You under the Apache License, Version 2.0
 *  (the "License"); you may not use this file except in compliance with
 *  the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

/******* NOTICE *********

Apache Harmony
Copyright 2006, 2010 The Apache Software Foundation.

This product includes software developed at
The Apache Software Foundation (http://www.apache.org/).

Portions of Apache Harmony were originally developed by
Intel Corporation and are licensed to the Apache Software
Foundation under the "Software Grant and Corporate Contribution
License Agreement" and for which the following copyright notices
apply
         (C) Copyright 2005 Intel Corporation
         (C) Copyright 2005-2006 Intel Corporation
         (C) Copyright 2006 Intel Corporation


The following copyright notice(s) were affixed to portions of the code
with which this file is now or was at one time distributed
and are placed here unaltered.

(C) Copyright 1997,2004 International Business Machines Corporation.
All rights reserved.

(C) Copyright IBM Corp. 2003. 


This software contains code derived from UNIX V7, Copyright(C)
Caldera International Inc.

************************/

// This code is a manual translation of Apache Harmony's HashMap class to
// JavaScript.

import Glibc

// debug: true release: false
let inDebug = false
func log(_ str: String) {
    if inDebug {
        print(str)
    }
}


class HashMap {
    let DEFAULT_SIZE = 16

    func calculateCapacity(_ x: Int) -> Int {
        if (x >= 1 << 30) {
            return 1 << 30
        }
        if (x == 0) {
            return 16
        }
        var result = x - 1
        result |= result >> 1
        result |= result >> 2
        result |= result >> 4
        result |= result >> 8
        result |= result >> 16
        return result + 1
    }
    
    func computeHashCode(_ key: Any) -> Int {
        switch key {
        case nil:
            return 0
        case let bool as Bool:
            return bool ? 1 : 0
        case let number as Int:
            return number
        case let double as Double:
            return String(double).hashValue
        case let string as String:
            var h = 0
            for char in string {
                h = ((h &* 31) &+ char.hashValue)
            }
            return h
        default:
            fatalError("Internal error: Bad Swift value type")
        }
    }
    
    func equals(_ a: Any?, _ b: Any?) -> Bool {
        if type(of: a) != type(of: b) {
            return false
        }
        switch a {
            case let a as Int:
                return a == b as! Int
                break
            case let a as AnyObject:
            if a == nil {
                return b == nil
                break
            }
        default:
            return a as AnyObject === b as AnyObject
        }
        return false
    }
    
    var capacity:Int = 16
    var elementCount:Int = 0
    var elementData:[Entry?] = []
    var loadFactor:Float = 0.75
    var modCount:Int = 0
    var threshold:Float = 0
    
    init(_ capacity: Int = 16, _ loadFactor: Float = 0.75) {
        self.capacity = calculateCapacity(capacity)
        self.elementCount = 0
        self.elementData = Array<Entry?>(repeating: nil, count: 1)
        self.loadFactor = loadFactor
        self.modCount = 0
        computeThreshold()
    }
    
    func computeThreshold() {
        self.threshold = floor(Float(self.elementData.count) * self.loadFactor)
    }
    
    func clear() {
        if self.elementCount <= 0 {
            return;
        }
        self.elementCount = 0
        for i in 0..<self.elementData.count {
            self.elementData[i] = nil
        }
        self.modCount+=1
    }
    
    func clone() -> HashMap {
        let result = HashMap(self.elementData.count, self.loadFactor)
        result.putAll(self)
        return result
    }
    
    func containsValue(_ value: Any) -> Bool {
        for entry in self.elementData {
            var currentEntry:Entry? = entry
            while let entry = currentEntry {
                if equals(value, entry.value) {
                    return true
                }
                currentEntry = entry.next
            }
        }
        return false
    }
    
    func entrySet() -> EntrySet {
        return EntrySet(self)
    }
    
    func get(_ key:Any) -> Any? {
        let entry = self.getEntry(key)
        if (entry != nil) {
            return entry?.value
        }
        return nil
    }
    
    func getEntry(_ key:Any) -> Entry? {
        let hash = self.computeHashCode(key)
        let index = hash & (self.elementData.count - 1)
        return self.findKeyEntry(key, index, hash)
    }
    
    func findKeyEntry(_ key:Any, _ index:Int, _ keyHash: Int) -> Entry? {
        var entry:Entry? = self.elementData.count == 0 ? nil : self.elementData[index]
        while (entry != nil && (entry?.origKeyHash != keyHash || !self.equals(key, entry?.key))) {
            entry = entry?.next
        }
        return entry
    }
    
    func isEmpty() -> Bool {
        return self.elementCount == 0
    }
    
    func put(_ key:Any, _ value:Any) -> Any? {
        let hash = self.computeHashCode(key)
        let index = hash & Int(self.elementData.count - 1)
        var entry = self.findKeyEntry(key, index, hash)
        if (entry == nil) {
            self.modCount += 1
            entry = self.createHashedEntry(key, index, hash)
            self.elementCount += 1
            if (Float(self.elementCount) > self.threshold) {
                self.rehash(0)
            }
        }
        let result = entry?.value
        entry?.value = value
        // if inDebug {
        //     if let key = key as? Int, key % 10000 == 0 {
        //         log("put:" + (entry?.toString())!)
        //     }
        // }
        return result
    }
    
    func createHashedEntry(_ key:Any, _ index:Int, _ hash: Int) -> Entry {
        let entry = Entry(key, hash, nil)
        entry.next = self.elementData[index];
        self.elementData[index] = entry;
        return entry
    }
    
    func putAll(_ map:HashMap) {
        if (map.isEmpty()) {
            return
        }
        let iterator = map.entrySet().iterator()
        while (iterator.hasNext()) {
            let entry = iterator.next()
            self.put(entry?.key, entry?.value)
        }
    }
    
    func rehash(_ capacity: Int) {
        var aCapacity = capacity
        if aCapacity <= 0 {
            aCapacity = self.elementData.count
        }
        let length = calculateCapacity(aCapacity != 0 ? aCapacity << 1 : 1)
        var newData = Array<Entry?>(repeating: nil, count: length)
        
        for i in 0 ..< self.elementData.count {
            var entry = self.elementData[i]
            self.elementData[i] = nil
            while let currentEntry = entry {
                let index = currentEntry.origKeyHash & (length - 1)
                let next = currentEntry.next
                currentEntry.next = newData[index] ?? nil
                newData[index] = currentEntry
                entry = next
            }
        }
        self.elementData = newData
        self.computeThreshold()
    }
    
    func remove(_ key:Any) ->Any? {
        let entry = self.removeEntryForKey(key)
        if (entry == nil) {
            return nil
        }
        return entry?.value
    }
    
    func removeEntry(_ entry: Entry) {
        let index = entry.origKeyHash & (self.elementData.count - 1)
        var current:Entry? = self.elementData[index]
        if current === entry {
            self.elementData[index] = entry.next
        } else {
            while current?.next !== entry {
                current = current?.next
            }
            current?.next = entry.next
        }
        self.modCount += 1
        self.elementCount -= 1
    }
    
    func removeEntryForKey(_ key: Any) -> Entry? {
        let hash = computeHashCode(key)
        let index = hash & (self.elementData.count - 1)
        var entry = self.elementData[index]
        var last: Entry? = nil
        while entry != nil && !(entry?.origKeyHash == hash && equals(key, entry?.key)) {
            last = entry
            entry = entry?.next
        }
        if entry == nil {
            return nil
        }
        if last == nil {
            self.elementData[index] = entry?.next
        } else {
            last?.next = entry?.next
        }
        self.modCount += 1
        self.elementCount -= 1
        return entry
    }
    
    func size() ->Int {
        return self.elementCount
    }
    
    func values() ->ValueCollection {
        return ValueCollection(self)
    }
}

class Entry {
    var key:Any?
    var value:Any?
    var origKeyHash:Int
    var next:Entry?
    
    init(_ key: Any, _ hash: Int, _ value: Any?) {
        self.key = key
        self.value = value
        self.origKeyHash = hash
    }
    
    func clone() -> Entry {
        let result = Entry(self.key, self.origKeyHash, self.value)
        if let nextEntry = self.next {
            result.next = nextEntry.clone()
        }
        return result
    }
    func toString() -> String {
        return "\(self.key )=\(self.value)"
    }
    func getKey() -> Any {
        return self.key
    }
    func getValue() -> Any {
        return self.value
    }
}

class AbstractMapIterator {
    var associatedMap: HashMap?
    var expectedModCount: Int = 0
    var futureEntry: Entry?
    var currentEntry: Entry?
    var prevEntry: Entry?
    var position: Int = 0
    
    init(_ map: HashMap) {
        self.associatedMap = map
        self.expectedModCount = map.modCount
    }
    
    func hasNext() -> Bool {
        if self.futureEntry != nil {
            return true
        }
        
        while self.position < self.associatedMap?.elementData.count ?? 0 {
            if self.associatedMap?.elementData[self.position] == nil {
		self.position += 1
            } else {
	        return true
	    }
        }
        return false
    }
    
    func checkConcurrentMod() {
        if self.expectedModCount != self.associatedMap?.modCount {
            fatalError("Concurrent HashMap modification detected")
        }
    }
    func makeNext() {
        self.checkConcurrentMod()
        if !self.hasNext() {
            fatalError("No such element")
        }
        if self.futureEntry == nil {
            self.currentEntry = self.associatedMap?.elementData[self.position]
            self.position += 1
            self.futureEntry = self.currentEntry?.next
            self.prevEntry = nil
            return
        }
        if self.currentEntry != nil {
            self.prevEntry = self.currentEntry
        }
        self.currentEntry = self.futureEntry
        self.futureEntry = self.futureEntry?.next
    }
    
    func remove() {
        self.checkConcurrentMod()
        if self.currentEntry == nil {
            fatalError("Illegal state")
        }
        if self.prevEntry == nil  {
            let index = self.currentEntry!.origKeyHash & ((self.associatedMap?.elementData.count ?? 1) - 1)
            var current = self.associatedMap?.elementData[index]
            current = current?.next
        } else {
            self.prevEntry?.next = self.currentEntry!.next
        }
        self.currentEntry = nil
        self.expectedModCount += 1
        self.associatedMap?.modCount += 1
        self.associatedMap?.elementCount -= 1
    }
}


class ValueIterator : AbstractMapIterator {
    func next() -> Entry? {
        self.makeNext()
        return self.currentEntry
    }
}

class EntrySet {
    var associatedMap:HashMap
    
    init(_ map:HashMap) {
        self.associatedMap = map
    }
    
    func size() -> Int {
        return self.associatedMap.elementCount
    }
    
    func clear() {
        self.associatedMap.clear()
    }
    
    func iterator() -> ValueIterator {
        return ValueIterator(self.associatedMap)
    }
}

class ValueCollection {
    var associatedMap:HashMap
    
    init(_ map:HashMap) {
        self.associatedMap = map
    }
    
    func contains(_ object: Any) -> Bool {
        return self.associatedMap.containsValue(object)
    }
    
    func size() -> Int {
        return self.associatedMap.elementCount
    }
    
    func clear() {
        self.associatedMap.clear()
    }
    
    func iterator() -> ValueIterator {
        return ValueIterator(self.associatedMap)
    }
}

/*
 * @State
 * @Tags Jetstream2
 */
class Benchmark {
    func run() {
        let map = HashMap()
        let COUNT = 90000
        for i in 0..<COUNT {
            map.put(i, 42)
        }
        var result = 0
        for _ in 0..<5 {
            for i in 0..<COUNT {
                result += map.get(i) as! Int
                // if inDebug {
                //     if  i % 10000 == 0 {
                //         let value:Int = map.get(i) as! Int
                //         log("get:" + "\(i)=\(value)")
                //     }
                // }
            }
        }
        
        var keySum:Int = 0
        var valueSum:Int = 0
        let iterator = map.entrySet().iterator()
        while (iterator.hasNext()) {
            let entry = iterator.next()
            keySum = keySum + (entry?.key as! Int)
            valueSum = valueSum + (entry?.value as! Int)
        }
    }
    
    /*
    * @Benchmark
    */
    func runIteration() {
        let startTime = currentTimestamp16()
        for _ in 0..<20 {
            run()
        }
        let endTime = currentTimestamp16()
        let time = endTime - startTime
        print("hash-map: ms = \(time)")
    }
}

Benchmark().runIteration();

// get current time, return ms
func currentTimestamp16() -> Double {
     return Timer().getTime()/1000
}

class Timer {
    private let CLOCK_REALTIME = 0
    private var time_spec = timespec()

    func getTime() -> Double {
        clock_gettime(Int32(CLOCK_REALTIME),&time_spec)
        return Double(time_spec.tv_sec * 1_000_000 + time_spec.tv_nsec / 1_000)
    }
}


