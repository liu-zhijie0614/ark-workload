import Glibc

let isDebug : Bool = false
func DeBugLog(_ msg : Any , file: String = #file, line: Int = #line, fn: String = #function){
    if isDebug {
        let prefix = "\(msg)"
        print(prefix)
    }
}

class OrderedCollection<Element: AnyObject> {
    var elms: [Element] = []
        
    func add(_ elm: Element) {
        elms.append(elm)
    }
    
    func at(_ index: Int) -> Element {
        return elms[index]
    }
    
    func size() -> Int {
        return elms.count
    }
    
    func removeFirst() -> Element {
        return elms.popLast()!
    }
    
    func remove(_ elm: Element) {
        var index = 0, skipped = 0
        for i in 0..<elms.count {
            let value = elms[i]
            if value !== elm {
                elms[index] = value
                index += 1
            } else {
                skipped += 1
            }
        }
        
        for _ in 0..<skipped {
            _ = elms.popLast()
        }
    }
}

/**
 * Strengths are used to measure the relative importance of constraints.
 * New strengths may be inserted in the strength hierarchy without
 * disrupting current constraints.  Strengths cannot be created outside
 * this class, so pointer comparison can be used for value comparison.
 */
class Strength {
    var strengthValue: Int
    var name: String
    
    static let REQUIRED         = Strength(0, "required")
    static let STRONG_PREFERRED = Strength(1, "strongPreferred")
    static let PREFERRED        = Strength(2, "preferred")
    static let STRONG_DEFAULT   = Strength(3, "strongDefault")
    static let NORMAL           = Strength(4, "normal")
    static let WEAK_DEFAULT     = Strength(5, "weakDefault")
    static let WEAKEST          = Strength(6, "weakest")
    
    init(_ strengthValue: Int, _ name: String) {
        self.name = name
        self.strengthValue = strengthValue
    }
    
    static func stronger(s1: Strength, s2: Strength) -> Bool {
        return s1.strengthValue < s2.strengthValue
    }
    
    static func weaker(s1: Strength, s2: Strength) -> Bool {
        return s1.strengthValue > s2.strengthValue
    }
    
    static func weakestOf(s1: Strength, s2: Strength) -> Strength? {
        return weaker(s1: s1, s2: s2) ? s1 : s2
    }
    
    static func strongest(s1: Strength, s2: Strength) -> Strength? {
        return stronger(s1: s1, s2: s2) ? s1 : s2
    }
    
    func nextWeaker() -> Strength? {
        switch strengthValue {
        case 0: return Strength.WEAKEST  //WEAKEST
        case 1: return Strength.WEAK_DEFAULT  //WEAK_DEFAULT
        case 2: return Strength.NORMAL  //NORMAL
        case 3: return Strength.STRONG_DEFAULT  //STRONG_DEFAULT
        case 4: return Strength.PREFERRED  //PREFERRED
        case 5: return Strength.REQUIRED  //REQUIRED
        default: return nil
        }
    }
    
}


/**
 ** An abstract class representing a system-maintainable relationship
 ** (or "constraint") between a set of variables. A constraint supplies
 ** a strength instance variable; concrete subclasses provide a means
 ** of storing the constrained variables and other information required
 ** to represent a constraint.
 **/
class Constraint {
    
    var strength: Strength
    init(strength: Strength) {
        self.strength = strength
    }
    
    /**
     * Activate this constraint and attempt to satisfy it.
     */
    func addConstraint() {
        addToGraph()
        Planner.planner.incrementalAdd(c: self)
    }
    
    /**
     ** Attempt to find a way to enforce this constraint. If successful,
     ** record the solution, perhaps modifying the current dataflow
     ** graph. Answer the constraint that this constraint overrides, if
     ** there is one, or nil, if there isn't.
     ** Assume: I am not already satisfied.
     **/
    func satisfy(mark: Int) -> Constraint? {
        chooseMethod(mark: mark)
        if !isSatisfied() {
            if strength === Strength.REQUIRED {
                //DeBugLog("Could not satisfy a required constraint!")
            }
            return nil
        }
        markInputs(mark: mark)
        let out = output()
        let overridden = out?.determinedBy
        if overridden != nil {
            overridden?.markUnsatisfied()
        }
        out?.determinedBy = self
        
        if !Planner.planner.addPropagate(c: self, mark: mark) {
            //DeBugLog("Cycle encountered")
        }
        out?.mark = mark
        return overridden
    }
    
    func destroyConstraint() {
        if isSatisfied() {
            Planner.planner.incrementalRemove(c: self)
        } else {
            removeFromGraph()
        }
    }
    
    /**
     * Normal constraints are not input constraints.  An input constraint
     * is one that depends on external state, such as the mouse, the
     * keybord, a clock, or some arbitraty piece of imperative code.
     */
    func isInput() -> Bool {
        return false
    }
    
    func markUnsatisfied() { }
    func recalculate() { }
    func isSatisfied() -> Bool {
        return true
    }
    
    func removeFromGraph() {}
    func inputsKnown(mark: Int) -> Bool {
        return true
    }
    
    func execute() {}
    func addToGraph() {}
    func chooseMethod(mark: Int) {}
    func markInputs(mark: Int) {}
    func output() -> Variable? {
        return nil
    }

}

class UnaryConstraint: Constraint {
    var myOutput: Variable?
    var satisfied = false
    
    /**
     * Abstract superclass for constraints having a single possible output
     * variable.
     */
    init(v: Variable, strength: Strength) {
        myOutput = v
        super.init(strength: strength)
        addConstraint()
    }
    
    /**
     * Adds this constraint to the constraint graph
     */
    override func addToGraph() {
        myOutput?.addConstraint(self)
        satisfied = false
    }
    
    /**
     * Decides if this constraint can be satisfied and records that
     * decision.
     */
    override func chooseMethod(mark: Int) {
        satisfied = myOutput?.mark != mark && Strength.stronger(s1: strength, s2: myOutput!.walkStrength)
    }
    
    /**
     * Returns true if this constraint is satisfied in the current solution.
     */
    override func isSatisfied() -> Bool {
        return satisfied
    }
    
    override func markInputs(mark: Int) {
        
    }
    
    /**
     * Returns the current output variable.
     */
    override func output() -> Variable? {
        return myOutput
    }
    
    /**
     * Calculate the walkabout strength, the stay flag, and, if it is
     * 'stay', the value for the current output of this constraint. Assume
     * this constraint is satisfied.
     */
    override func recalculate() {
        guard let myOutput = myOutput else { return }
        myOutput.walkStrength = strength
        myOutput.stay = !isInput()
        if myOutput.stay {
            execute()
        }
    }
    
    /**
     * Records that this constraint is unsatisfied
     */
    override func markUnsatisfied() {
        satisfied = false
    }
    
    override func inputsKnown(mark: Int) -> Bool {
        return true
    }
    
    override func removeFromGraph() {
        if myOutput != nil {
            myOutput!.removeConstraint(self)
        }
        satisfied = false
    }
    
}

/**
 * Variables that should, with some level of preference, stay the same.
 * Planners may exploit the fact that instances, if satisfied, will not
 * change their output during plan execution.  This is called "stay
 * optimization".
 */
class StayConstraint: UnaryConstraint {
    
    override init(v: Variable, strength: Strength) {
        super.init(v: v, strength: strength)
    }
    
    override func execute() {}
    
}

/**
 * A unary input constraint used to mark a variable that the client
 * wishes to change.
 */
class EditConstraint: UnaryConstraint {
    
    override init(v: Variable, strength: Strength) {
        super.init(v: v, strength: strength)
    }
    
    /**
     * Edits indicate that a variable is to be changed by imperative code.
     */
    override func isInput() -> Bool {
        return true
    }
    
    override func execute() {}
    
}

enum Direction: Int {
    case NONE = 0
    case FORWARD = 1
    case BACKWARD = -1
}

/**
 * Abstract superclass for constraints having two possible output
 * variables.
 */
class BinaryConstraint: Constraint {

    var v1: Variable?
    var v2: Variable?
    var direction: Direction = .NONE
    
    init(var1: Variable, var2: Variable, strength: Strength) {
        v1 = var1
        v2 = var2
        super.init(strength: strength)
        addConstraint()
    }
    
    /**
     * Decides if this constraint can be satisfied and which way it
     * should flow based on the relative strength of the variables related,
     * and record that decision.
     */
    override func chooseMethod(mark: Int) {
        guard let v1 = v1, let v2 = v2 else {
            direction = .NONE
            return
        }
        if v1.mark == mark { // relative v1.mark
            direction = v2.mark != mark && Strength.stronger(s1: strength, s2: v2.walkStrength) ? .FORWARD : .NONE
        }
        
        if v2.mark == mark { // relative v2.mark
            direction = v1.mark != mark && Strength.stronger(s1: strength, s2: v1.walkStrength) ? .BACKWARD : .NONE
        }
        
        if Strength.weaker(s1: v1.walkStrength, s2: v2.walkStrength) { // weaker v1 and v2
            direction = Strength.stronger(s1: strength, s2: v1.walkStrength) ? .BACKWARD : .NONE
        } else {  // stronger v1 and v2
            direction = Strength.stronger(s1: strength, s2: v2.walkStrength) ? .FORWARD : .BACKWARD
        }
    }
    
    /***
     ** Add this constraint to the constraint graph
     **/
    override func addToGraph() {
        v1?.addConstraint(self)
        v2?.addConstraint(self)
        direction = .NONE
    }
    
    /**
     * Answer true if this constraint is satisfied in the current solution.
     */
    override func isSatisfied() -> Bool {
        return direction != .NONE
    }

    /**
     * Mark the input variable with the given mark.
     */
    override func markInputs(mark: Int) {
        input()?.mark = mark
    }
    
    /**
     * Returns the current output variable
     */
    func input() -> Variable? {
        return direction == .FORWARD ? v1 : v2
    }
    
    /**
     * Returns the current input variable
     */
    override func output() -> Variable?  {
        return direction == .FORWARD ? v2 : v1
    }
    
    /**
     * Calculate the walkabout strength, the stay flag, and, if it is
     * 'stay', the value for the current output of this
     * constraint. Assume this constraint is satisfied.
     */
    override func recalculate() {
        let ihn = input()
        let out = output()
        out?.walkStrength = Strength.weakestOf(s1: strength, s2: ihn!.walkStrength)!
        out?.stay = ihn!.stay
        if (out?.stay) != nil {
            execute()
        }
    }
    
    /**
     * Record the fact that this constraint is unsatisfied.
     */
    override func markUnsatisfied() {
        direction = .NONE
    }
    
    override func inputsKnown(mark: Int) -> Bool {
        let i = input()
        return i?.mark == mark || ((i?.stay) != nil) || i?.determinedBy == nil
    }
    
    override func removeFromGraph() {
        if v1 != nil {
            v1?.removeConstraint(self)
        }
        if v2 != nil {
            v2?.removeConstraint(self)
        }
        direction = .NONE
    }
    
}

/**
 * Relates two variables by the linear scaling relationship: "v2 =
 * (v1 * scale) + offset". Either v1 or v2 may be changed to maintain
 * this relationship but the scale factor and offset are considered
 * read-only.
 */
class ScaleConstraint: BinaryConstraint {
    
    var scale: Variable?
    var offset: Variable?
    
    init(src: Variable, scale: Variable, offset: Variable, dest: Variable, strength: Strength) {
        self.scale = scale
        self.offset = offset
        super.init(var1: src, var2: dest, strength: strength)
    }
    
    override func addToGraph() {
        super.addToGraph()
        scale?.addConstraint(self)
        offset?.addConstraint(self)
    }
    
    override func removeFromGraph() {
        super.removeFromGraph()
        if scale != nil {
            scale?.removeConstraint(self)
        }
        if offset != nil {
            offset?.removeConstraint(self)
        }
    }
    
    override func markInputs(mark: Int) {
        super.markInputs(mark: mark)
        scale?.mark = mark
        offset?.mark = mark
    }
    
    /**
     * Enforce this constraint. Assume that it is satisfied.
     */
    override func execute() {
        if direction == .FORWARD { // Direction.FORWARD
            v2?.value = v1!.value * scale!.value + offset!.value
        } else {
            v1?.value = (v2!.value - offset!.value) / scale!.value
        }
    }
    /**
     ** Calculate the walkabout strength, the stay flag, and, if it is
     ** 'stay', the value for the current output of this constraint. Assume
     ** this constraint is satisfied.
     **/
    override func recalculate() {
        let ihn = input()
        let out = output()
        out?.walkStrength = Strength.weakestOf(s1: strength, s2: ihn!.walkStrength)!
        out!.stay = ihn!.stay && scale!.stay && offset!.stay
        if (out?.stay) != nil {
            execute()
        }
    }
    
}

/**
 * Constrains two variables to have the same value.
 */
class EqualityConstraint: BinaryConstraint {
    
    override init(var1: Variable, var2: Variable, strength: Strength) {
        super.init(var1: var1, var2: var2, strength: strength)
    }
    
    override func execute() {
        output()?.value = input()!.value
    }
    
}

/**
 ** A constrained variable. In addition to its value, it maintain the
 ** structure of the constraint graph, the current dataflow graph, and
 ** various parameters of interest to the DeltaBlue incremental
 ** constraint solver.
 **/
class Variable {
    var value: Int = 0
    var constraints = OrderedCollection<Constraint>()
    var determinedBy: Constraint?
    var mark: Int = 0
    var walkStrength = Strength.WEAKEST
    var stay: Bool = true
    var name: String
    
    init(_ names: String, _ initialValue: Int?) {
        name = names
        value = initialValue ?? 0
    }
    
    /**
     ** Add the given constraint to the set of all constraints that refer
     ** this variable.
     **/
    func addConstraint(_ c: Constraint) {
        constraints.add(c)
    }
    
    /**
     * Removes all traces of c from this variable.
     */
    func removeConstraint(_ c: Constraint) {
        constraints.remove(c)
        if determinedBy === c {
            determinedBy = nil
        }
    }
}

class Planner {
    
    static var planner = Planner()
    
    var currentMark: Int = 0
    
    /**
     ** Attempt to satisfy the given constraint and, if successful,
     ** incrementally update the dataflow graph.  Details: If satifying
     ** the constraint is successful, it may override a weaker constraint
     ** on its output. The algorithm attempts to resatisfy that
     ** constraint using some other method. This process is repeated
     ** until either a) it reaches a variable that was not previously
     ** determined by any constraint or b) it reaches a constraint that
     ** is too weak to be satisfied using any of its methods. The
     ** variables of constraints that have been processed are marked with
     ** a unique mark value so that we know where we've been. This allows
     ** the algorithm to avoid getting into an infinite loop even if the
     ** constraint graph has an inadvertent cycle.
     **/
    func incrementalAdd(c: Constraint) {
        let mark = newMark()
        var overridden = c.satisfy(mark: mark)
        while overridden != nil {
            overridden = overridden?.satisfy(mark: mark)
        }
    }
    
    /**
     ** Entry point for retracting a constraint. Remove the given
     ** constraint and incrementally update the dataflow graph.
     ** Details: Retracting the given constraint may allow some currently
     ** unsatisfiable downstream constraint to be satisfied. We therefore collect
     ** a list of unsatisfied downstream constraints and attempt to
     ** satisfy each one in turn. This list is traversed by constraint
     ** strength, strongest first, as a heuristic for avoiding
     ** unnecessarily adding and then overriding weak constraints.
     ** Assume: c is satisfied.
     **/
    func incrementalRemove(c: Constraint) {
        var out = c.output()
        c.markUnsatisfied()
        c.removeFromGraph()
        let unsatisfied = removePropagateFrom(out: &out)
        var stren = Strength.REQUIRED
        repeat {
            for i in 0..<unsatisfied.size() {
                let u = unsatisfied.at(i)
                if u.strength === stren {
                    incrementalAdd(c: u)
                }
            }
            stren = stren.nextWeaker()!
        } while stren !== Strength.WEAKEST
        
    }
    
    /**
     ** Select a previously unused mark value.
     **/
    func newMark() -> Int {
        currentMark += 1
        return currentMark
    }
    
    /**
     ** Extract a plan for resatisfaction starting from the given source
     ** constraints, usually a set of input constraints. This method
     ** assumes that stay optimization is desired; the plan will contain
     ** only constraints whose output variables are not stay. Constraints
     ** that do no computation, such as stay and edit constraints, are
     ** not included in the plan.
     ** Details: The outputs of a constraint are marked when it is added
     ** to the plan under construction. A constraint may be appended to
     ** the plan when all its input variables are known. A variable is
     ** known if either a) the variable is marked (indicating that has
     ** been computed by a constraint appearing earlier in the plan), b)
     ** the variable is 'stay' (i.e. it is a constant at plan execution
     ** time), or c) the variable is not determined by any
     ** constraint. The last provision is for past states of history
     ** variables, which are not stay but which are also not computed by
     ** any constraint.
     ** Assume: sources are all satisfied.
     **/
    func makePlan(sources: OrderedCollection<Constraint>) -> Plan {
        let mark = newMark()
        let plan = Plan()
        var todo = sources
        while todo.size() > 0 {
            let constraint = todo.removeFirst()
            if constraint.output()?.mark != mark, constraint.inputsKnown(mark: mark) {
                plan.addConstraint(c: constraint)
                constraint.output()?.mark = mark
                addConstraintsConsumingTo(v: constraint.output(), coll: &todo)
            }
        }
        return plan
    }
    
    /**
     ** Extract a plan for resatisfying starting from the output of the
     ** given constraints, usually a set of input constraints.
     **/
    func extractPlanFromConstraints(constraints: OrderedCollection<Constraint>) -> Plan {
        let sources = OrderedCollection<Constraint>()
        for i in 0..<constraints.size() {
            let c = constraints.at(i)
            if c.isInput(), c.isSatisfied() {
                sources.add(c)
            }
        }
        return makePlan(sources: sources)
    }
    
    /**
     ** Recompute the walkabout strengths and stay flags of all variables
     ** downstream of the given constraint and recompute the actual
     ** values of all variables whose stay flag is true. If a cycle is
     ** detected, remove the given constraint and answer
     ** false. Otherwise, answer true.
     ** Details: Cycles are detected when a marked variable is
     ** encountered downstream of the given constraint. The sender is
     ** assumed to have marked the inputs of the given constraint with
     ** the given mark. Thus, encountering a marked node downstream of
     ** the output constraint means that there is a path from the
     ** constraint's output to one of its inputs.
     **/
    func addPropagate(c: Constraint, mark: Int) -> Bool {
        var todo = OrderedCollection<Constraint>()
        todo.add(c)
        while todo.size() > 0 {
            let dered = todo.removeFirst()
            if dered.output()?.mark == mark {
                incrementalRemove(c: c)
                return false
            }
            dered.recalculate()
            addConstraintsConsumingTo(v: dered.output(), coll: &todo)
        }
        return true
    }
    
    func addConstraintsConsumingTo(v: Variable?, coll: inout OrderedCollection<Constraint>) {
        let determining = v?.determinedBy
        let cc = v?.constraints
        for i in 0..<(cc?.size() ?? 0) {
            let c = cc?.at(i)
            if c !== determining, c!.isSatisfied() {
                coll.add(c!)
            }
        }
    }
    
    /**
     * Update the walkabout strengths and stay flags of all variables
     * downstream of the given constraint. Answer a collection of
     * unsatisfied constraints sorted in order of decreasing strength.
     */
    func removePropagateFrom(out: inout Variable?) -> OrderedCollection<Constraint> {
        out?.determinedBy = nil
        out?.walkStrength = Strength.WEAKEST
        out?.stay = true
        let unsatisfied = OrderedCollection<Constraint>()
        let todo = OrderedCollection<Variable>()
        todo.add(out!)
        while todo.size() > 0 {
            let v = todo.removeFirst()
            for i in 0..<v.constraints.size() {
                let c = v.constraints.at(i)
                if c.isSatisfied() == false {
                    unsatisfied.add(c)
                }
            }
            let determining = v.determinedBy
            for i in 0..<v.constraints.size() {
                let next = v.constraints.at(i)
                if next !== determining, next.isSatisfied() {
                    next.recalculate()
                    todo.add((next.output())!)
                }
            }
        }
        return unsatisfied
    }
    
}

/**
 * A Plan is an ordered list of constraints to be executed in sequence
 * to resatisfy all currently satisfiable constraints in the face of
 * one or more changing inputs.
 */
class Plan {
    var v = OrderedCollection<Constraint>()
        
    func addConstraint(c: Constraint) {
        v.add(c)
    }
    
    func size() -> Int {
        return v.size()
    }
    
    func constraintAt(indedx: Int) -> Constraint? {
        return v.at(indedx)
    }
    
    func execute() {
        for i in 0..<v.size() {
            let c = constraintAt(indedx: i)
            c?.execute()
        }
    }
    
}

/**
 ** This is the standard DeltaBlue benchmark. A long chain of equality
 ** constraints is constructed with a stay constraint on one end. An
 ** edit constraint is then added to the opposite end and the time is
 ** measured for adding and removing this constraint, and extracting
 ** and executing a constraint satisfaction plan. There are two cases.
 ** In case 1, the added constraint is stronger than the stay
 ** constraint and values must propagate down the entire length of the
 ** chain. In case 2, the added constraint is weaker than the stay
 ** constraint so it cannot be accomodated. The cost in this case is,
 ** of course, very low. Typical situations lie somewhere between these
 ** two extremes.
 **/
func chainTest(n: Int) {
    Planner.planner = Planner()
    var prev: Variable?
    var first: Variable?
    var last: Variable?
    for i in 0...n {
        let name = "v\(i)"
        let v = Variable(name, nil)
        if prev != nil {
            _ = EqualityConstraint(var1: prev!, var2: v, strength: Strength.REQUIRED)
        }
        if i == 0 {
            first = v
        }
        if i == n {
            last = v
        }
        prev = v
    }
    _ = StayConstraint(v: last!, strength: Strength.STRONG_DEFAULT)
    let edit = EditConstraint(v: first!, strength: Strength.PREFERRED)
    let edits = OrderedCollection<Constraint>()
    edits.add(edit)
    let plan = Planner.planner.extractPlanFromConstraints(constraints: edits)
    for i in 0..<100 {
        first?.value = i
        //DeBugLog("first.value value is \(first!.value)")
        plan.execute()
        //DeBugLog("convert last.value value is \(last!.value)")
        if last?.value != i {
            //DeBugLog("Chain test failed.")
        }
    }
}

/**
 * This test constructs a two sets of variables related to each
 * other by a simple linear transformation (scale and offset). The
 * time is measured to change a variable on either side of the
 * mapping and to change the scale and offset factors.
 */
func projectionTest(n: Int) {
    Planner.planner = Planner()
    let scale = Variable("scale", 10)
    let offset = Variable("offset", 1000)
    var src: Variable?, dst: Variable?
    
    let dest = OrderedCollection<Variable>()
    for i in 0..<n {
        src = Variable("src\(i)", i)
        dst = Variable("dst\(i)", i)
        dest.add(dst!)
        _ = StayConstraint(v: src!, strength: Strength.NORMAL)
        _ = ScaleConstraint(src: src!, scale: scale, offset: offset, dest: dst!, strength: Strength.REQUIRED)
    }
    
    //DeBugLog("src original value is \(src!.value)")
    //DeBugLog("dst original value is \(dst!.value)")
    change(v: src!, newValue: 17)
    //DeBugLog("src new value is \(src!.value)")
    //DeBugLog("dst new value is \(dst!.value)")
    if dst?.value != 1170 {
        //DeBugLog("Projection 1 failed")
    }
    
    //DeBugLog("src original value is \(src!.value)")
    //DeBugLog("dst original value is \(dst!.value)")
    change(v: dst!, newValue: 1050)
    //DeBugLog("src new value is \(src!.value)")
    //DeBugLog("dst new value is \(dst!.value)")
    if src?.value != 5 {
        //DeBugLog("Projection 2 failed")
    }
    change(v: scale, newValue: 5)
    for i in 0..<(n - 1) {
        if dest.at(i).value != (i * 5 + 1000) {
            //DeBugLog("Projection 3 failed")
        }
    }
    change(v: offset, newValue: 2000)
    for i in 0..<(n - 1) {
        if dest.at(i).value != (i * 5 + 2000)  {
            //DeBugLog("Projection 3 failed")
        }
    }
}

func change(v: Variable, newValue: Int) {
    let edit = EditConstraint(v: v, strength: Strength.PREFERRED)
    let edits = OrderedCollection<Constraint>()
    edits.add(edit)
    let plan = Planner.planner.extractPlanFromConstraints(constraints: edits)
    for _ in 0..<10 {
        v.value = newValue
        plan.execute()
    }
    edit.destroyConstraint()
}

func deltaBlue() {
    chainTest(n: 100)
    projectionTest(n: 100)
}


class Benchmark {
    /// @Benchmark
    func runIteration() {
        for _ in 0..<20 {
            deltaBlue()
        }
    }
}

class Timer {
    private let CLOCK_REALTIME = 0
    private var time_spec = timespec()

    func getTime() -> Double {
        clock_gettime(Int32(CLOCK_REALTIME),&time_spec)
        return Double(time_spec.tv_sec * 1_000_000 + time_spec.tv_nsec / 1_000)
    }
}

func run() {
    let start = Timer().getTime()
    let deltaBlue = Benchmark()
    for _ in 1...20 {
        deltaBlue.runIteration()
    }
    let end = Timer().getTime()
    let duration = (end - start)/1000
    print("deltablue: ms = \(duration)")
}

run()

