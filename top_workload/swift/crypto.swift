/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import Glibc

// Bits per digit
var dbits: Int!
var BI_DB: Int!
var BI_DM: Int!
var BI_DV: Int!

var BI_FP: Int!
var BI_FV: Int!
var BI_F1: Int!
var BI_F2: Int!

var DV: Int = 0

// JavaScript engine analysis
var canary: Int = 0xdeadbeefcafe;
var j_lm: Bool = ((canary&0xffffff)==0xefcafe);

let lowprimes = [2,3,5,7,11,13,17,19,23,29,31,37,41,43,47,53,59,61,67,71,73,79,83,89,97,101,103,107,109,113,127,131,137,139,149,151,157,163,167,173,179,181,191,193,197,199,211,223,227,229,233,239,241,251,257,263,269,271,277,281,283,293,307,311,313,317,331,337,347,349,353,359,367,373,379,383,389,397,401,409,419,421,431,433,439,443,449,457,461,463,467,479,487,491,499,503,509]
var lplim = (1<<26)/lowprimes[lowprimes.count - 1]

// am: Compute w_j += (x*this_i), propagate carries,
// c is initial carry, returns final carry.
// c < 3*dvalue, x < 2*dvalue, this_i < dvalue
// We need to select the fastest one that works in this environment.

// am1: use a single mult and divide to get the high bits,
// max digit bits should be 26 because
// max internal value = 2*dvalue^2-2*dvalue (< 2^53)
var am1: (_ arrar: [Int?],_ i: Int, _ x: Int, _ w: BigInteger, _ j: Int, _ c: Int, _ n: Int) -> Int = { array, i, x, w, j, c, n in
    var temp_i = i, temp_j = j, temp_c = c, temp_n = n - 1
    while temp_n >= 0 {
        let v = x * array[temp_i]! + w.array[temp_j]! + temp_c
        temp_c = Int(floor(Double(v) / 0x4000000))
        insertValue(&w.array,v & 0x3ffffff, j)
        temp_i += 1
        temp_j += 1
        temp_n -= 1
    }
    return temp_c
}

// am2 avoids a big mult-and-extract completely.
// Max digit bits should be <= 30 because we do bitwise ops
// on values up to 2*hdvalue^2-hdvalue-1 (< 2^31)
var am2: (_ arrar: [Int?],_ i: Int, _ x: Int, _ w: BigInteger, _ j: Int, _ c: Int, _ n: Int) -> Int = { array, i, x, w, j, c, n in
    let xl = x & 0x7fff, xh = x >> 15
    var temp_i = i, temp_j = j, temp_c = c, temp_n = n - 1
    while temp_n >= 0 {
        var l = array[temp_i]! & 0x7fff
        let h = array[temp_i]! >> 15
        let m = xh * l + h * xl
        l = xl * l + ((m & 0x7fff) << 15) + w.array[temp_j]! + (temp_c & 0x3fffffff)
        temp_c = (l >> 30) + (m >> 15) + xh * h + (temp_c >> 30)
        insertValue(&w.array,l & 0x3fffffff, temp_j)
        temp_i += 1
        temp_j += 1
        temp_n -= 1
    }
    return temp_c
}

// Alternately, set max digit bits to 28 since some
// browsers slow down when dealing with 32-bit numbers.
var am3: (_ arrar: [Int?],_ i: Int, _ x: Int, _ w: BigInteger, _ j: Int, _ c: Int, _ n: Int) -> Int = { array, i, x, w, j, c, n in
    let xl = x & 0x3fff
    let xh = x >> 14
    var temp_i = i, temp_j = j, temp_c = c, temp_n = n - 1
    while temp_n >= 0 {
        var l = array[temp_i]! & 0x3fff
        let h = array[temp_i]! >> 14
        let m = xh * l + h * xl
        l = xl * l + ((m & 0x3fff) << 14) + w.array[temp_j]! + temp_c
        temp_c = (l >> 28) + (m >> 14) + xh * h
        insertValue(&w.array, l & 0xfffffff, temp_j)
        temp_i += 1
        temp_j += 1
        temp_n -= 1
    }
    return temp_c
}

// This is tailored to VMs with 2-bit tagging. It makes sure
// that all the computations stay within the 29 bits available.
var am4: (_ arrar: [Int?],_ i: Int, _ x: Int, _ w: BigInteger, _ j: Int, _ c: Int, _ n: Int) -> Int = { array, i, x, w, j, c, n in
    let xl = x & 0x1fff
    let xh = x >> 13
    var temp_i = i, temp_j = j, temp_c = c, temp_n = n - 1
    while temp_n >= 0 {
        var l = array[temp_i]! & 0x1fff
        let h = array[temp_i]! >> 13
        let m = xh * l + h * xl
        l = xl * l + ((m & 0x1fff) << 13) + w.array[temp_j]! + temp_c
        temp_c = (l >> 26) + (m >> 13) + xh * h
        insertValue(&w.array, l & 0x3ffffff, temp_j)
        temp_i += 1
        temp_j += 1
        temp_n -= 1
    }
    return temp_c
}

var AM: ((_ arrar: [Int?],_ i: Int, _ x: Int, _ w: BigInteger, _ j: Int, _ c: Int, _ n: Int) -> Int)!

// am3/28 is best for SM, Rhino, but am4/26 is best for v8.
// Kestrel (Opera 9.5) gets its best result with am4/26.
// IE7 does 9% better with am3/28 than with am4/26.
// Firefox (SM) gets 10% faster with am3/28 than with am4/26.
func setupEngine(_ fn: @escaping (_ arrar: [Int?],_ i: Int, _ x: Int, _ w: BigInteger, _ j: Int, _ c: Int, _ n: Int) -> Int, _ bits: Int) {
    AM = fn
    dbits = bits
    BI_DB = dbits
    BI_DM = ((1<<dbits)-1);
    BI_DV = (1<<dbits);
    
    BI_FP = 52;
    BI_FV = Int(pow(2, Double(BI_FP)))
    BI_F1 = BI_FP - dbits;
    BI_F2 = 2 * dbits - BI_FP;
}

// Digit conversions
var BI_RM = "0123456789abcdefghijklmnopqrstuvwxyz";
var BI_RC: [Int?] = []

func initBI_RC() {
    var rr: Int
    rr = Int("0".unicodeScalars.first!.value)
    for vv in 0...9 {
        insertValue(&BI_RC, vv, rr)
        rr += 1
    }
    rr = Int("a".unicodeScalars.first!.value)
    for vv in 10..<36 {
        insertValue(&BI_RC, vv, rr)
        rr += 1
    }
    rr = Int("A".unicodeScalars.first!.value)
    for vv in 10..<36 {
        insertValue(&BI_RC, vv, rr)
        rr += 1
    }
}

class BigInteger {
    static let ONE: BigInteger = BigInteger.nbv(1)
    static let ZERO: BigInteger = BigInteger.nbv(0)
    var array: [Int?] = []
    var t: Int = 0
    var s: Int = 0
    
    // (public) Constructor
    init(_ a: Any? = nil,_  b: Any? = nil,_  c: Any? = nil) {
        if a != nil {
            if let a = a as? Int {
                self.fromNumber(a, b, c)
            }else if b == nil && !(a is String) {
                self.fromString(a!, 256)
            }else{
                self.fromString(a!, b as! Int)
            }
        }
    }
  
    // return new, unset BigInteger
    static func nbi() -> BigInteger {
        return BigInteger()
    }

    // return bigint initialized to value
    static func nbv(_ i: Int) -> BigInteger {
        let r = BigInteger.nbi()
        r.fromInt(i)
        return r
    }

    func int2char(_ n: Int) -> String {
        return String(BI_RM[BI_RM.index(BI_RM.startIndex, offsetBy: n)])
    }

    func intAt(_ s: String, _ i: Int) -> Int {
        let c = BI_RC[Int(s.unicodeScalars[s.index(s.startIndex, offsetBy: i)].value)]
        return (c == nil) ?(-1) : c!
    }
    
    // (protected) copy this to r
    func copyTo(_ r: BigInteger) {
        var i = t-1
        while (i >= 0) {
            insertValue(&r.array, array[i]!, i)
            i -= 1;
        }
        r.t = t
        r.s = s
    }
    
    // (protected) set from integer value x, -DV <= x < DV
    func fromInt(_ x: Int) {
        t = 1
        s = (x < 0) ? -1 : 0
        if x > 0 {
            insertValue(&array, x, 0)
        } else if x < -1 {
            insertValue(&array, x + DV, 0)
        } else {
            t = 0
        }
    }
    
    // (protected) set from string and radix
    func fromString(_ s: Any,_ b: Int) {
        var k: Int
        if b == 16 {
            k = 4
        } else if b == 8 {
            k = 3
        } else if b == 256 {
            k = 8 // byte array
        } else if b == 2 {
            k = 1
        } else if b == 32 {
            k = 5
        } else if b == 4 {
            k = 2
        } else {
            fromRadix(s as! String, b)
            return
        }
        self.t = 0
        self.s = 0
        var i:Int,
            mi: Bool = false,
            sh: Int = 0
        if k == 8 {
            let temp_s = s as! [Int?]
            i = temp_s.count
        }else{
            i = (s as! String).count
        }
        i -= 1
        while i >= 0 {
            var x: Int
            if k == 8{
                x = ((s as! [Int?])[i]! & 0xff)
            }else{
                x = intAt(s as! String, i)
            }
            if x < 0 {
                if let s = s as? String, s[s.index(s.startIndex, offsetBy: i)] == "-" {
                    mi = true
                }
                continue
            }
            mi = false
            if sh == 0 {
                insertValue(&array, x, t)
                t += 1
            } else if sh + k > BI_DB {
                insertValue(&array, array[t-1]! | ((x & ((1 << (BI_DB - sh)) - 1)) << sh), t-1)
                insertValue(&array, (x >> (BI_DB - sh)), t)
                t += 1
            } else {
                insertValue(&array, array[t-1]! | (x << sh), t-1)
            }
            sh += k
            if sh >= BI_DB {
                sh -= BI_DB
            }
            i -= 1
        }
        if k == 8, let s = (s as? [Int?]), (s[0]! & 0x80) != 0 {
            self.s = -1
            if sh > 0 {
                array[t-1]! |= ((1 << (BI_DB - sh)) - 1) << sh
            }
        }
        clamp()
        if mi {
            BigInteger.ZERO.subTo(self, self)
        }
    }
    
    // (protected) clamp off excess high words
    func clamp() {
        let c = s & BI_DB
        while t > 0 && array[t - 1]  == c{
            t -= 1
        }
    }
    
    // (public) return string representation in given radix
    func toString(_ b: Int) -> String {
        if s < 0 {
            return "-" + negate().toString(b)
        }
        var k: Int
        if b == 16 {
            k = 4
        } else if b == 8 {
            k = 3
        } else if b == 2 {
            k = 1
        } else if b == 32 {
            k = 5
        } else if b == 4 {
            k = 2
        } else {
            return toRadix(b)
        }
        let km = (1 << k) - 1
        var d: Int?
        var m = false
        var r = ""
        var i = t
        var p = BI_DB - (i * BI_DB) % k
        if i > 0 {
            i -= 1
            d = array[i]! >> p
            if p < BI_DB && d! > 0 {
                m = true
                r = int2char(d!)
            }
            while i >= 0 {
                if p < k {
                    d = (array[i]! & ((1 << p) - 1)) << (k - p)
                    i -= 1
                    p += (BI_DB - k)
                    d! |= array[i]! >> p
                } else {
                    p -= k
                    d = (array[i]! >> p) & km
                    if p <= 0 {
                        p += BI_DB
                        i -= 1
                    }
                }
                if d != nil && d! > 0 {
                    m = true
                }
                if m {
                    r += int2char(d!)
                }
            }
        }
        return m ?r :"0"
    }
   
    // (public) -this
    public func negate() -> BigInteger {
        let r = BigInteger.nbi()
        BigInteger.ZERO.subTo(self, r)
        return r
    }
    
    // (public) |this|
    public func abs() -> BigInteger {
        return  s<0 ?negate() :self
    }
    
    // (public) return + if this > a, - if this < a, 0 if equal
    func compareTo(_ a: BigInteger) -> Int {
        var r = s - a.s
        if r != 0 {
            return r
        }
        var i = t
        r = i - a.t
        if r != 0 {
            return r
        }
        i -= 1
        while i >= 0 {
            r = array[i]! - a.array[i]!
            if r != 0 {
                return r
            }
            i -= 1
        }
        return 0
    }
    
    // returns bit length of the integer x
    func nbits(_ x: Int) -> Int {
        var temp_x = x
        var r = 1
        var t: Int = temp_x >> 16
        if t != 0 {
            temp_x = t
            r += 16
        }
        t = temp_x >> 8
        if t != 0 {
            temp_x = t
            r += 8
        }
        t = temp_x >> 4
        if t != 0 {
            temp_x = t
            r += 4
        }
        t = temp_x >> 2
        if t != 0 {
            temp_x = t
            r += 2
        }
        t = temp_x >> 1
        if t != 0 {
            temp_x = t
            r += 1
        }
        return r
    }
    
    // (public) return the number of bits in "this"
    func bitLength() -> Int {
        if t <= 0 {
            return 0
        }
        return BI_DB * (t - 1) + nbits(array[t - 1]! ^ (s & BI_DM))
    }
    
    // (protected) r = this << n*DB
    func dlShiftTo(_ n: Int, _ r: BigInteger) {
        var i = t - 1
        while (i >= 0) {
            insertValue(&r.array, array[i]!, i + n)
            i -= 1
        }
        i = n - 1
        while (i >= 0) {
            insertValue(&r.array, 0, i)
            i -= 1
        }
        r.t = t + n
        r.s = s
    }
    
    // (protected) r = this >> n*DB
    func drShiftTo(_ n: Int, _ r: BigInteger) {
        var i = n
        while i < t {
            insertValue(&r.array, array[i]!, i - n)
            i += 1
        }
        r.t = max(t - n, 0)
        r.s = s
    }
    
    // (protected) r = this << n
    func lShiftTo(_ n: Int, _ r: BigInteger) {
        let bs = n % BI_DB
        let cbs = BI_DB - bs
        let bm = (1 << cbs) - 1
        let ds = Int(floorf(Float(n) / Float(BI_DB)))
        var c = (s << bs) & BI_DM
        var i = t-1;
        while (i >= 0){
            insertValue(&r.array, (array[i]! >> cbs) | c, i + ds + 1)
            c = (array[i]! & bm) << bs;
            i -= 1;
        }
        i = ds-1;
        while (i >= 0) {
            insertValue(&r.array, 0, i)
          i -= 1;
        }
        insertValue(&r.array, c, ds)
        r.t = t + ds + 1;
        r.s = s
        r.clamp()
    }
    
    // (protected) r = this >> n
    func rShiftTo(_ n: Int, _ r: BigInteger) {
        r.s = s
        let ds = Int(floorf(Float(n) / Float(BI_DB)))
        if ds >= t {
            r.t = 0
            return
        }
        let bs = n % BI_DB
        let cbs = BI_DB - bs
        let bm = (1<<bs) - 1;
        insertValue(&r.array, array[ds]! >> bs, 0)
        for i in (ds + 1)..<t {
            insertValue(&r.array, r.array[i-ds-1]! | ((array[i]! & bm) << cbs), i-ds-1)
            insertValue(&r.array, array[i]! >> bs, i-ds)
        }
        if bs > 0 {
            insertValue(&r.array, r.array[t-ds-1]! | ((s & bm) << cbs), t-ds-1)
        }
        r.t = t - ds
        r.clamp()
    }
    
    // (protected) r = this - a
    func subTo(_ a: BigInteger, _ r: BigInteger) {
        var i = 0,
            c = 0,
            m = min(a.t, t)
        while i < m {
            c += array[i]! - a.array[i]!
            insertValue(&r.array, c & BI_DM, i)
            c >>= BI_DB
            i += 1
        }
        if a.t < t {
            c -= a.s
            while i < t {
                c += array[i]!
                insertValue(&r.array, c & BI_DM, i)
                c >>= BI_DB
                i += 1
            }
            c += self.s
        } else {
            c += self.s
            while i < a.t {
                c -= a.array[i]!
                insertValue(&r.array, c & BI_DM, i)
                c >>= BI_DB
                i += 1
            }
            c -= a.s
        }
        r.s = (c < 0) ? -1 : 0
        if c < -1 {
            insertValue(&r.array, BI_DV + c, i)
            i += 1
        } else if c > 0 {
            insertValue(&r.array, c, i)
            i += 1
        }
        r.t = i
        r.clamp()
    }
    
    // (protected) r = this * a, r != this,a (HAC 14.12)
    // "this" should be the larger one if appropriate.
    func multiplyTo(_ a: BigInteger, _ r: BigInteger) {
        let x = abs()
        let y = a.abs()
        var i = x.t
        r.t = i + y.t
        i -= 1
        while i >= 0 {
            insertValue(&r.array, 0, i)
            i -= 1
        }
        for i in 0..<y.t {
            insertValue(&r.array, x.am(0, y.array[i]!, r, i, 0, x.t), i + x.t)
        }
        r.s = 0
        r.clamp()
        if s != a.s {
            BigInteger.ZERO.subTo(r, r)
        }
    }
    
    /// (protected) r = this^2, r != this (HAC 14.16)
    func squareTo(_ r: BigInteger) {
        let x = abs()
        r.t = 2 * x.t
        var i = r.t - 1
        while i >= 0 {
            insertValue(&r.array, 0, i)
            i -= 1
        }
        i = 0
        while i < x.t - 1 {
            let c = x.am(i, x.array[i]!, r, 2 * i, 0, 1)
            insertValue(&r.array, r.array[i + x.t]! + x.am(i + 1, 2 * x.array[i]!, r, 2 * i + 1, c, x.t - i - 1), i + x.t)
            if r.array[i + x.t]! >= BI_DV {
                insertValue(&r.array, r.array[i + x.t]! - BI_DV, i + x.t)
                insertValue(&r.array, 1, i + x.t + 1)
            }
            i += 1
        }
        if r.t > 0 {
            insertValue(&r.array, r.array[r.t - 1]! + x.am(i, x.array[i]!, r, 2 * i, 0, 1), r.t - 1)
        }
        r.s = 0
        r.clamp()
    }
    
    // (protected) divide this by m, quotient and remainder to q, r (HAC 14.20)
    // r != q, this != m.  q or r may be null.
    func divRemTo(_ m: BigInteger, _ q: BigInteger?, _ r:inout  BigInteger?) {
        let pm = m.abs()
        if pm.t <= 0 {
            return
        }
        let pt = abs()
        if pt.t < pm.t {
            if q != nil {
                q!.fromInt(0)
            }
            if r != nil {
                copyTo(r!)
            }
            return
        }
        if r == nil {
            r = BigInteger.nbi()
        }
        let y = BigInteger.nbi(),
            ts = self.s,
            ms = m.s
        let nsh = BI_DB - nbits(pm.array[pm.t - 1]!) // normalize modulus
        if nsh > 0 {
            pm.lShiftTo(nsh, y)
            pt.lShiftTo(nsh, r!)
        } else {
            pm.copyTo(y)
            pt.copyTo(r!)
        }
        let ys = y.t
        let y0 = y.array[ys - 1]!
        if y0 == 0 {
            return
        }
        let yt = y0 * (1 << BI_F1) + ((ys > 1) ? y.array[ys - 2]! >> BI_F2 : 0)
        let d1 = Double(BI_FV) / Double(yt),
            d2 =  Double(1 << BI_F1) / Double(yt),
            e = 1 << BI_F2
        var i = r!.t,
            j = i - ys,
            t = (q == nil) ?BigInteger.nbi() :q!
        y.dlShiftTo(j, t)
        if r!.compareTo(t) >= 0 {
            insertValue(&r!.array, 1, r!.t)
            r!.t += 1
            r!.subTo(t, r!)
        }
        BigInteger.ONE.dlShiftTo(ys, t)
        t.subTo(y, y) // "negative" y so we can replace sub with am later
        while y.t < ys {
            insertValue(&y.array, 0, y.t)
            y.t += 1
        }
        j -= 1
        while j >= 0 {
            // Estimate quotient digit
            i -= 1
            var qd: Int = (r!.array[i] == y0) ?BI_DM :Int(floor(Double(r!.array[i]!) * d1 + Double((r!.array[i - 1]! + e)) * d2))
            insertValue(&r!.array, r!.array[i]! + y.am(0, qd, r!, j, 0, ys), i)
            if r!.array[i]! < qd { // Try it out
                y.dlShiftTo(j, t)
                r!.subTo(t, r!)
                qd -= 1
                while r!.array[i]! < qd {
                    r!.subTo(t, r!)
                    qd -= 1
                }
            }
            j -= 1
        }
        if q != nil {
            r!.drShiftTo(ys, q!)
            if ts != ms {
                BigInteger.ZERO.subTo(q!, q!)
            }
        }
        r!.t = ys
        r!.clamp()
        if nsh > 0 {
            r!.rShiftTo(nsh, r!)
        } // Denormalize remainder
        if ts < 0 {
            BigInteger.ZERO.subTo(r!, r!)
        }
    }
    
    // (public) this mod a
    func mod(_ a: BigInteger) -> BigInteger {
        var r: BigInteger? = BigInteger.nbi()
        abs().divRemTo(a, nil, &r)
        if self.s < 0 && r!.compareTo(BigInteger.ZERO) > 0 {
            a.subTo(r!, r!)
        }
        return r!
    }
    
    // (protected) return "-1/this % 2^DB"; useful for Mont. reduction
    // justification:
    //         xy == 1 (mod m)
    //         xy =  1+km
    //   xy(2-xy) = (1+km)(1-km)
    // x[y(2-xy)] = 1-k^2m^2
    // x[y(2-xy)] == 1 (mod m^2)
    // if y is 1/x mod m, then y(2-xy) is 1/x mod m^2
    // should reduce x and y(2-xy) by m^2 at each step to keep size bounded.
    // JS multiply "overflows" differently from C/C++, so care is needed here.
    func invDigit() -> Int {
        if t < 1 {
            return 0
        }
        let x = array[0]!
        if (x & 1) == 0 {
            return 0
        }
        var y = x & 3 // y == 1/x mod 2^2
        y = (y * (2 - (x & 0xf) * y)) & 0xf // y == 1/x mod 2^4
        y = (y * (2 - (x & 0xff) * y)) & 0xff // y == 1/x mod 2^8
        y = (y * (2 - ((x & 0xffff) * y) & 0xffff)) & 0xffff // y == 1/x mod 2^16
        // last step - calculate inverse mod DV directly;
        // assumes 16 < DB <= 32 and assumes ability to handle 48-bit ints
        y = (y * (2 - x * y % BI_DV)) % BI_DV // y == 1/x mod 2^dbits
        // we really want the negative inverse, and -DV < y < DV
        return (y > 0) ? BI_DV - y : -y
    }
    
    // (protected) true iff this is even
    func isEven() -> Bool {
        return (t > 0 ?(array[0]! & 1) :s) == 0
    }
    
    // (protected) this^e, e < 2^32, doing sqr and mul with "r" (HAC 14.79)
    func exp(_ e: Int,_ z: ConvertProtocol) -> BigInteger {
        if e > 0xffffffff || e < 1 {
            return BigInteger.ONE
        }
        var r: BigInteger? = BigInteger.nbi(),
            r2: BigInteger? = BigInteger.nbi(),
            g = z.convert(self),
            i = nbits(e) - 1 - 1
        g.copyTo(r!);
        while(i >= 0) {
            z.sqrTo(r!,&r2);
            if((e&(1<<i)) > 0) {
                z.mulTo(r2!, g, &r)
            }else {
                let t: BigInteger? = r
                r = r2
                r2 = t;
            }
            i -= 1
        }
        return z.revert(r!);
    }
    
    // (public) this^e % m, 0 <= e < 2^32
    func modPowInt(_ e: Int,_ m: BigInteger) -> BigInteger {
        var z: ConvertProtocol;
        if e < 256 || m.isEven() {
            z = Classic(m)
        }else{
            z = Montgomery(m)
        }
        return exp(e, z)
    }
    
    // (public)
    func clone() -> BigInteger {
        let r = BigInteger.nbi()
        copyTo(r)
        return r
    }

    // (public) return value as integer
    func intValue() -> Int {
        if self.s < 0 {
            if t == 1 {
                return array[0]! - BI_DV
            } else if t == 0 {
                return -1
            }
        } else if t == 1 {
            return array[0]!
        } else if t == 0 {
            return 0
        }
        // assumes 16 < DB < 32
        return ((array[1]! & ((1 << (32 - BI_DB)) - 1)) << BI_DB) | array[0]!
    }
    
    // (public) return value as byte
    func byteValue() -> Int {
        return (t == 0) ?s :(array[0]! << 24) >> 24
    }
    
    // (public) return value as short (assumes DB>=16)
    func shortValue() -> Int {
        return t == 0 ?s :(array[0]! << 16) >> 16;
    }
    
    // (protected) return x s.t. r^x < DV
    func chunkSize(_ r: Int) -> Int {
        return Int(floor(log(Double(2)) *  Double(BI_DB) / log(Double(r))))
    }
    
    // (public) 0 if this == 0, 1 if this > 0
    func sigNum() -> Int {
        if s < 0 {
            return -1
        } else if t <= 0 || (t == 1 && array[0]! <= 0) {
            return 0
        } else {
            return 1
        }
    }
    
    // (protected) convert to radix string
    func toRadix(_ b: Int?) -> String {
        let temp_b = (b == nil) ?10 :b!
        if sigNum() == 0 || temp_b < 2 || temp_b > 36 {
            return "0"
        }
        let cs = chunkSize(temp_b)
        let a = Int(pow(Double(temp_b), Double(cs)))
        let d = BigInteger.nbi(),
            y = BigInteger.nbi()
        var z: BigInteger? = BigInteger.nbi()
        var r = ""
        divRemTo(d, y, &z)
        while y.sigNum() > 0 {
            let str = String(a + z!.intValue(), radix: temp_b)
            r = String(str[str.index(str.startIndex, offsetBy: 1)...]) + r
            y.divRemTo(d, y, &z)
        }
        return String(z!.intValue(), radix: temp_b) + r
    }
    
    // (protected) convert from radix string
    func fromRadix(_ s: String, _ b: Int?) {
        fromInt(0)
        let temp_b = (b == nil) ?10 :b!
        let cs = chunkSize(temp_b)
        let d = Int(pow(Double(temp_b), Double(cs)))
        var mi = false
        var j = 0
        var w = 0
        for i in 0..<s.count {
            let x = intAt(s, i)
            if x < 0 {
                if s[s.index(s.startIndex, offsetBy: i)] == "-" && sigNum() == 0 {
                    mi = true
                }
                continue
            }
            w = temp_b * w + x
            j += 1
            if j >= cs {
                dMultiply(d)
                dAddOffset(w, 0)
                j = 0
                w = 0
            }
        }
        if j > 0 {
            dMultiply(Int(pow(Double(temp_b), Double(j))))
            dAddOffset(w, 0)
        }
        if mi {
            BigInteger.ZERO.subTo(self, self)
        }
    }
    
    // (protected) alternate constructor
    func fromNumber(_ a: Int, _ b: Any?,_ c: Any? = nil) {
        if b is Int {
            // new BigInteger(int,int,RNG)
            if a < 2 {
                fromInt(1)
            }else{
                fromNumber(a, c)
                if !testBit(a - 1) { // force MSB set
                    bitwiseTo(BigInteger.ONE.shiftLeft(a - 1), op_or, self)
                }
                if isEven() {
                    dAddOffset(1, 0) // force odd
                }
                while isProbablePrime(b as! Int) {
                    dAddOffset(2, 0)
                    if bitLength() > a {
                        subTo(BigInteger.ONE.shiftLeft(a - 1), self)
                    }
                }
            }
        }else {
            // new BigInteger(int,RNG)
            var x: [Int?] = [],
                t = a&7
            x = Array(repeating: nil, count: (a>>3)+1)
            (b as! SecureRandom).nextBytes(&x)
            if t > 0 {
                insertValue(&x, x[0]! & ((1 << t) - 1), 0)
            }else{
                insertValue(&x, 0, 0)
            }
            fromString(x, 256)
        }
    }

    // (public) convert to bigendian byte array
    func toByteArray() -> [Int?] {
        var i = t,
            r: [Int?] = []
        insertValue(&r, s, 0)
        var p = BI_DB - (i * BI_DB) % 8,
            d: Int,
            k = 0
        if i > 0 {
            i -= 1
            d = array[i]! >> p
            if p < BI_DB && d != (s & BI_DM) >> p {
                insertValue(&r, d | (s << (BI_DB - p)), k)
                k += 1
            }
            while i >= 0 {
                if p < 8 {
                    d = (array[i]! & ((1 << p) - 1)) << (8 - p)
                    i -= 1
                    p += BI_DB - 8
                    d |= array[i]! >> p
                } else {
                    p -= 8
                    d = (array[i]! >> p) & 0xFF
                    if p <= 0 {
                        p += BI_DB;
                        i -= 1
                    }
                }
                if (d & 0x80) != 0 {
                    d |= -256
                }
                if k == 0 && (s & 0x80) != (d & 0x80) {
                    k += 1
                }
                if k > 0 || d != s {
                    insertValue(&r, d, k)
                    k += 1
                }
            }
        }
        return r
    }
    
    func bnEquals(_ a: BigInteger) -> Bool {
        return compareTo(a) == 0
    }
    
    func bnMin(_ a: BigInteger) -> BigInteger {
        return compareTo(a) < 0 ?self :a
    }
    
    func bnMax(_ a: BigInteger) -> BigInteger {
        return compareTo(a) > 0 ?self :a
    }
    
    // (protected) r = this op a (bitwise)
    func bitwiseTo(_ a: BigInteger, _ op: ((Int,Int) -> Int), _ r: BigInteger) {
        var f: Int,
            m = min(a.t, t)
        for i in 0..<m {
            insertValue(&r.array, op(array[i]!, a.array[i]!), i)
        }
        if a.t < t {
            f = a.s & BI_DM
            for i in m..<t {
                insertValue(&r.array, op(array[i]!, f), i)
            }
            r.t = t
        }else{
            f = s & BI_DM
            for i in m..<a.t {
                insertValue(&r.array, op(f, a.array[i]!), i)
            }
            r.t = a.t
        }
        r.s = op(s, a.s)
        r.clamp()
    }
    
    // (public) this & a
    var op_and: ((Int, Int) -> Int) = { (x, y) in
        return x & y
    }
    func bnAnd(_ a: BigInteger) -> BigInteger {
        let r = BigInteger.nbi()
        bitwiseTo(a, op_and, r)
        return r
    }
    
    // (public) this | a
    var op_or: ((Int, Int) -> Int) = { (x, y) in
        return x | y
    }
    func bnOr(_ a: BigInteger) -> BigInteger {
        let r = BigInteger.nbi()
        bitwiseTo(a, op_or, r)
        return r
    }
    
    // (public) this ^ a
    var op_xor: ((Int, Int) -> Int) = { (x, y) in
        return x ^ y
    }
    func bnXor(_ a: BigInteger) -> BigInteger {
        let r = BigInteger.nbi()
        bitwiseTo(a, op_xor, r)
        return r
    }
    
    // (public) this & ~a
    var op_andnot: ((Int, Int) -> Int) = { (x, y) in
        return x & ~y
    }
    func bnAndNot(_ a: BigInteger) -> BigInteger {
        let r = BigInteger.nbi()
        bitwiseTo(a, op_andnot, r)
        return r
    }
    
    // (public) ~this
    func bnNot() -> BigInteger {
        let r = BigInteger.nbi();
        for i in 0..<t {
            insertValue(&r.array, BI_DM & ~array[i]!, i)
        }
        r.t = t;
        r.s = ~s;
        return r;
    }
    
    // (public) this << n
    func shiftLeft(_ n: Int) -> BigInteger {
        let r = BigInteger.nbi();
        if n < 0 {
            rShiftTo(-n, r)
        }else{
            lShiftTo(n, r)
        }
        return r
    }
    
    // (public) this >> n
    func shiftRight(_ n: Int) -> BigInteger {
        let r = BigInteger.nbi()
        if n < 0 {
            lShiftTo(-n, r)
        }else{
            rShiftTo(n, r)
        }
        return r
    }
    
    // return index of lowest 1-bit in x, x < 2^31
    func lbit(_ x: Int) -> Int {
        var temp_x = x;
        if temp_x == 0 {
            return -1
        }
        var r = 0
        if (temp_x & 0xffff) == 0 {
            temp_x >>= 16
            r += 16
        }
        if (temp_x & 0xff) == 0 {
            temp_x >>= 8
            r += 8
        }
        if (temp_x & 0xf) == 0 {
            temp_x >>= 4
            r += 4
        }
        if (temp_x & 3) == 0 {
            temp_x >>= 2
            r += 2
        }
        if (temp_x & 1) == 0 {
            r += 1
        }
        return r
    }
    
    // (public) returns index of lowest 1-bit (or -1 if none)
    func getLowestSetBit() -> Int {
        for i in 0..<t {
            if array[i] != 0 {
                return i * BI_DB + lbit(array[i]!)
            }
        }
        if s < 0 {
            return t * BI_DB
        }
        return -1
    }
    
    // return number of 1 bits in x
    func cbit(_ x: Int) -> Int {
        var r = 0
        var tepm_x = x
        while(tepm_x != 0) {
            tepm_x &= tepm_x-1
            r += 1;
        }
        return r;
    }
    
    // (public) return number of set bits
    func bitCount() -> Int {
        var r = 0,
            x = s & BI_DM;
        for i in 0..<t {
            r += cbit(array[i]! ^ x)
        }
        return r;
    }
    
    // (public) true iff nth bit is set
    func testBit(_ n: Int) -> Bool {
        let j =  Int(floorf(Float(n) / Float(BI_DB)))
        if j > t{
            return s != 0
        }
        return (array[j]! & (1 << (n % BI_DB))) != 0
    }
    
    // (protected) this op (1<<n)
    func changeBit(_ n: Int, _ op: ((Int,Int) -> Int)) -> BigInteger {
        let r = BigInteger.ONE.shiftLeft(n);
        bitwiseTo(r,op,r);
        return r;
    }
    
    // (public) this | (1<<n)
    func setBit(_ n: Int) -> BigInteger {
        return changeBit(n,op_or)
    }
    
    // (public) this & ~(1<<n)
    func clearBit(_ n: Int) -> BigInteger {
        return changeBit(n,op_andnot)
    }
    
    // (public) this ^ (1<<n)
    func flipBit(_ n: Int) -> BigInteger {
        return changeBit(n,op_xor)
    }
    
    // (protected) r = this + a
    func addTo(_ a: BigInteger, _ r: BigInteger) {
        var i = 0,
            c = 0,
            m = min(a.t, t)
        while i < m {
            c += array[i]! + a.array[i]!
            insertValue(&r.array, c & BI_DM, i)
            c >>= BI_DB
            i += 1
        }
        if a.t < self.t {
            c += a.s
            while i < self.t {
                c += array[i]!
                insertValue(&r.array, c & BI_DM, i)
                c >>= BI_DB
                i += 1
            }
            c += self.s
        } else {
            c += self.s
            while i < a.t {
                c += a.array[i]!
                insertValue(&r.array, c & BI_DM, i)
                c >>= BI_DB
                i += 1
            }
            c += a.s
        }
        r.s = (c < 0) ? -1 : 0
        if c > 0 {
            insertValue(&r.array, c, i)
            i += 1
        } else if c < -1 {
            insertValue(&r.array, BI_DV + c, i)
            i += 1
        }
        r.t = i
        r.clamp()
    }
    
    // (public) this + a
    func add(_ a: BigInteger) -> BigInteger {
        let r = BigInteger.nbi()
        addTo(a, r)
        return r
    }
   
    // (public) this - a
    func subtract(_ a: BigInteger) -> BigInteger {
        let r = BigInteger.nbi()
        subTo(a, r)
        return r
    }
    
    // (public) this * a
    func multiply(_ a: BigInteger) -> BigInteger {
        let r = BigInteger.nbi()
        multiplyTo(a, r)
        return r
    }
    
    // (public) this / a
    func divide(_ a: BigInteger) -> BigInteger {
        let r = BigInteger.nbi()
        var temp: BigInteger?
        divRemTo(a, r, &temp)
        return r
    }
    
    // (public) this % a
    func remainder(_ a: BigInteger) -> BigInteger {
        var r: BigInteger? = BigInteger.nbi()
        divRemTo(a,nil,&r)
        return r!;
    }

    // (public) [this/a,this%a]
    func divideAndRemainder(_ a: BigInteger) -> [BigInteger]{
        let q = BigInteger.nbi()
        var r: BigInteger? = BigInteger.nbi();
        divRemTo(a,q,&r);
        return [q,r!];
    }
    
    // (protected) this *= n, this >= 0, 1 < n < DV
    func dMultiply(_ n: Int) {
        insertValue(&array, am(0, n - 1, self, 0, 0, t), t)
        t += 1
        clamp()
    }
    
    // (protected) this += n << w words, this >= 0
    func dAddOffset(_ n: Int,_ w: Int) {
        var temp_w = w
        while t <= temp_w {
            insertValue(&array, 0, t)
            t += 1
        }
        insertValue(&array, array[temp_w]! + n, temp_w)
        while array[temp_w]! >= BI_DV {
            insertValue(&array, array[temp_w]! - BI_DV, temp_w)
            temp_w += 1
            if temp_w >= t {
                insertValue(&array, 0, t)
                t += 1
            }
            insertValue(&array, array[temp_w]! + 1, temp_w)
        }
    }
  
    // (public) this^e
    func bnPow(_ e: Int) -> BigInteger {
        return exp(e, NullExp());
    }
   
    // (protected) r = lower n words of "this * a", a.t <= n
    // "this" should be the larger one if appropriate.
    func multiplyLowerTo(_ a: BigInteger,_ n: Int, _ r: BigInteger) {
        var i = min(t + a.t, n)
        r.s = 0 // assumes a, this >= 0
        r.t = i
        while i > 0 {
            i -= 1
            insertValue(&r.array, 0, i)
        }
        while (i < r.t - t) {
            insertValue(&r.array, am(0, a.array[i]!, r, i, 0, t), i + t)
            i += 1
        }
        for j in i..<min(a.t, n) {
            _ = am(0, a.array[j]!, r, j, 0, n - j)
        }
        r.clamp()
    }
    
    // (protected) r = "this * a" without lower n words, n > 0
    // "this" should be the larger one if appropriate.
    func multiplyUpperTo(_ a: BigInteger, _ n: Int, _ r: BigInteger) {
        let temp_n = n - 1
        r.t = t + a.t - temp_n
        var i = r.t - 1
        r.s = 0 // assumes a, this >= 0
        while i >= 0 {
            insertValue(&r.array, 0, i)
            i -= 1
        }
        for i in max(temp_n - t, 0)..<a.t {
            insertValue(&r.array, am(temp_n - i, a.array[i]!, r, 0, 0, t + i - temp_n), t + i - temp_n)
        }
        r.clamp()
        r.drShiftTo(1, r)
    }
    
    // (public) this^e % m (HAC 14.85)
    func modPow(_ e: BigInteger, _ m: BigInteger) -> BigInteger {
        var i = e.bitLength()
        var k: Int
        var r: BigInteger? = BigInteger.nbv(1)
        var z: ConvertProtocol!
        if i <= 0 {
            return r!
        } else if i < 18 {
            k = 1
        } else if i < 48 {
            k = 3
        } else if i < 144 {
            k = 4
        } else if i < 768 {
            k = 5
        } else {
            k = 6
        }
        if i < 8 {
            z = Classic(m)
        } else if m.isEven() {
            z = Barrett(m)
        } else {
            z = Montgomery(m)
        }
        // precomputation
        var g: [BigInteger?] = [],
            n = 3,
            k1 = k - 1,
            km = (1 << k) - 1
        insertBigIntegerValue(&g, z.convert(self), 1)
        if k > 1 {
            var g2: BigInteger? = BigInteger.nbi()
            z.sqrTo(g[1]!, &g2)
            while n <= km {
                insertBigIntegerValue(&g, BigInteger.nbi(), n)
                z.mulTo(g2!, g[n-2]!, &g[n])
                n += 2
            }
        }
        var j = e.t - 1
        var w: Int
        var is1 = true
        var r2: BigInteger? = BigInteger.nbi()
        var t: BigInteger?
        i = nbits(e.array[j]!) - 1
        while j >= 0 {
            if i >= k1 {
                w = (e.array[j]! >> (i - k1)) & km
            } else {
                w = (e.array[j]! & ((1 << (i + 1)) - 1)) << (k1 - i)
                if j > 0 {
                    w |= e.array[j - 1]! >> (BI_DB + i - k1)
                }
            }
            n = k
            while (w & 1) == 0 {
                w >>= 1
                n -= 1
            }
            i -= n
            if i < 0 {
                i += BI_DB
                j -= 1
            }
            if is1 { // ret == 1, don't bother squaring or multiplying it
                g[w]!.copyTo(r!)
                is1 = false
            } else {
                while n > 1 {
                    z.sqrTo(r!, &r2)
                    z.sqrTo(r2!, &r)
                    n -= 2
                }
                if n > 0 {
                    z.sqrTo(r!, &r2)
                } else {
                    t = r
                    r = r2
                    r2 = t
                }
                z.mulTo(r2!, g[w]!, &r)
            }
            while j >= 0 && (e.array[j]! & (1 << i)) == 0 {
                z.sqrTo(r!, &r2)
                t = r
                r = r2
                r2 = t
                i -= 1
                if i < 0 {
                    i = BI_DB - 1
                    j -= 1
                }
            }
        }
        return z.revert(r!)
    }
    
    // (public) gcd(this,a) (HAC 14.54)
    func gcd(_ a: BigInteger) -> BigInteger {
        var x = (s < 0) ?negate() :clone()
        var y = (s < 0) ?negate() :clone()
        if x.compareTo(y) < 0 {
            let t = x
            x = y
            y = t
        }
        var i = x.getLowestSetBit()
        var g = y.getLowestSetBit()
        if g < 0 {
            return x
        }
        if i < g {
            g = i
        }
        if g > 0 {
            x.rShiftTo(g, x)
            y.rShiftTo(g, y)
        }
        while x.sigNum() > 0 {
            i = x.getLowestSetBit()
            if i > 0 {
                x.rShiftTo(i, x)
            }
            i = y.getLowestSetBit()
            if i > 0 {
                y.rShiftTo(i, y)
            }
            if x.compareTo(y) >= 0 {
                x.subTo(y, x)
                x.rShiftTo(1, x)
            } else {
                y.subTo(x, y)
                y.rShiftTo(1, y)
            }
        }
        if g > 0 {
            y.lShiftTo(g, y)
        }
        return y
    }

    // (protected) this % n, n < 2^26
    func modInt(_ n: Int) -> Int {
        if n <= 0 {
            return 0
        }
        var d = BI_DV % n,
            r = (s < 0) ?n-1 :0
        if t > 0 {
            if d == 0 {
                r = array[0]! % n
            }else {
                var i = t - 1
                while i >= 0 {
                    r = (d * r + array[i]!) % n
                    i -= 1
                }
            }
        }
        return r
    }
    
    // (public) 1/this % m (HAC 14.61)
    func modInverse(_ m: BigInteger) -> BigInteger {
        let ac = m.isEven()
        if (isEven() && ac) || m.sigNum() == 0 {
            return BigInteger.ZERO
        }
        let u = m.clone()
        let v = clone()
        let a = BigInteger.nbv(1),
            b = BigInteger.nbv(0),
            c = BigInteger.nbv(0),
            d = BigInteger.nbv(1)
        while u.sigNum() != 0 {
            while u.isEven() {
                u.rShiftTo(1, u)
                if ac {
                    if !a.isEven() || !b.isEven() {
                        a.addTo(self, a)
                        b.subTo(m, b)
                    }
                    a.rShiftTo(1, a)
                } else if !b.isEven() {
                    b.subTo(m, b)
                }
                b.rShiftTo(1, b)
            }
            while v.isEven() {
                v.rShiftTo(1, v)
                if ac {
                    if !c.isEven() || !d.isEven() {
                        c.addTo(self, c)
                        d.subTo(m, d)
                    }
                    c.rShiftTo(1, c)
                } else if !d.isEven() {
                    d.subTo(m, d)
                }
                d.rShiftTo(1, d)
            }
            if u.compareTo(v) >= 0 {
                u.subTo(v, u)
                if ac {
                    a.subTo(c, a)
                }
                b.subTo(d, b)
            } else {
                v.subTo(u, v)
                if ac {
                    c.subTo(a, c)
                }
                d.subTo(b, d)
            }
        }
        if v.compareTo(BigInteger.ONE) != 0 {
            return BigInteger.ZERO
        }
        if d.compareTo(m) >= 0 {
            return d.subtract(m)
        }
        if d.sigNum() < 0 {
            d.addTo(m, d)
        } else {
            return d
        }
        if d.sigNum() < 0 {
            return d.add(m)
        } else {
            return d
        }
    }

    // (public) test primality with certainty >= 1-.5^t
    func isProbablePrime(_ t: Int) -> Bool {
        let x = abs()
        if x.t == 1 && x.array[0]! <= lowprimes[lowprimes.count - 1] {
            for i in 0..<lowprimes.count {
                if x.array[0] == lowprimes[i] {
                    return true
                }
            }
            return false
        }
        if x.isEven(){
            return false
        }
        var i = 1;
        while i < lowprimes.count {
            var m = lowprimes[i],
                j = i + 1
            while j < lowprimes.count && m < lplim {
                m *= lowprimes[j]
                j += 1
            }
            m = x.modInt(m)
            while i < j  {
                if m % lowprimes[i] == 0 {
                    return false
                }
                i += 1
            }
        }
        return x.millerRabin(t)
    }
    
    // (protected) true if probably prime (HAC 4.24, Miller-Rabin)
    func millerRabin(_ t: Int) -> Bool {
        var temp_t = t
        let n1 = subtract(BigInteger.ONE)
        let k = n1.getLowestSetBit()
        if k <= 0 {
            return false
        }
        let r = n1.shiftRight(k)
        temp_t = (temp_t + 1) >> 1
        if temp_t > lowprimes.count {
            temp_t = lowprimes.count
        }
        let a = BigInteger.nbi()
        for i in 0..<temp_t {
            a.fromInt(lowprimes[i])
            var y = a.modPow(r, self)
            if y.compareTo(BigInteger.ONE) != 0 && y.compareTo(n1) != 0 {
                var j = 1
                while j < k && y.compareTo(n1) != 0 {
                    j += 1
                    y = y.modPowInt(2, self)
                    if y.compareTo(BigInteger.ONE) == 0 {
                        return false
                    }
                }
                if y.compareTo(n1) != 0 {
                    return false
                }
            }
        }
        return true
    }
    
    func am(_ i: Int, _ x: Int, _ w: BigInteger, _ j: Int, _ c: Int, _ n: Int) -> Int {
        return AM(array,i, x, w, j, c, n)
    }
}

protocol ConvertProtocol {
    func convert(_ x: BigInteger) -> BigInteger
    func revert(_ x: BigInteger) -> BigInteger
    func reduce(_ x:inout BigInteger?)
    func sqrTo(_ x: BigInteger,_ r: inout BigInteger?)
    func mulTo(_ x: BigInteger,_ y: BigInteger,_ r:inout BigInteger?)
}

// Modular reduction using "classic" algorithm
class Classic: ConvertProtocol {
    var m: BigInteger!
    
    init(_ m: BigInteger) {
        self.m = m
    }
    
    func convert(_ x: BigInteger) -> BigInteger {
        if x.s < 0 || x.compareTo(m) >= 0 {
            return x.mod(m)
        } else {
            return x
        }
    }
    
    func revert(_ x: BigInteger) -> BigInteger {
        return x
    }
    
    func reduce(_ x:inout BigInteger?) {
        x!.divRemTo(m, nil, &x)
    }
    
    func sqrTo(_ x: BigInteger,_ r: inout BigInteger?) {
        x.squareTo(r!)
        reduce(&r)
    }
    
    func mulTo(_ x: BigInteger,_ y: BigInteger,_ r:inout BigInteger?) {
        x.multiplyTo(y, r!)
        reduce(&r)
    }
}

// Montgomery reduction
class Montgomery: ConvertProtocol {
    var m: BigInteger!
    var mp: Int
    var mpl: Int
    var mph: Int
    var um: Int
    var mt2: Int

    init(_ m: BigInteger) {
        self.m = m
        self.mp = m.invDigit()
        self.mpl = self.mp & 0x7fff
        self.mph = self.mp >> 15
        self.um = (1 << (BI_DB - 15)) - 1
        self.mt2 = 2 * m.t
    }

    // xR mod m
    func convert(_ x: BigInteger) -> BigInteger {
        var r: BigInteger? = BigInteger.nbi()
        x.abs().dlShiftTo(m.t, r!)
        r!.divRemTo(m, nil, &r)
        if x.s < 0 && r!.compareTo(BigInteger.ZERO) > 0 {
            m.subTo(r!, r!)
        }
        return r!
    }
    
    // x/R mod m
    func revert(_ x: BigInteger) -> BigInteger {
        var r: BigInteger? = BigInteger.nbi()
        x.copyTo(r!)
        reduce(&r)
        return r!
    }
    
    // x = x/R mod m (HAC 14.32)
    func reduce(_ x:inout BigInteger?)  {
        while x!.t <= mt2 { // pad x so am has enough room later
            insertValue(&x!.array, 0, x!.t)
            x!.t += 1
        }
        for i in 0..<m.t {
            // faster way of calculating u0 = x[i]*mp mod DV
            var j = x!.array[i]! & 0x7fff
            let u0 = (j * mpl + (((j * mph + (x!.array[i]! >> 15) * mpl) & self.um) << 15)) & BI_DM
            // use am to combine the multiply-shift-add into one call
            j = i + m.t
            insertValue(&x!.array, x!.array[j]! + m.am(0, u0, x!, i, 0, m.t), j)
            // propagate carry
            while x!.array[j]! >= BI_DV {
                insertValue(&x!.array, x!.array[j]! - BI_DV, j)
                j += 1
                insertValue(&x!.array, x!.array[j]! + 1, j)
            }
        }
        x!.clamp()
        x!.drShiftTo(self.m.t, x!)
        if x!.compareTo(self.m) >= 0 {
            x!.subTo(self.m, x!)
        }
    }
    
    // r = "x^2/R mod m"; x != r
    func sqrTo(_ x: BigInteger,_ r: inout BigInteger?) {
        x.squareTo(r!)
        reduce(&r)
    }
    
    // r = "xy/R mod m"; x,y != r
    func mulTo(_ x: BigInteger,_ y: BigInteger,_ r:inout BigInteger?) {
        x.multiplyTo(y, r!)
        reduce(&r)
    }
}

class NullExp: ConvertProtocol {
    func convert(_ x: BigInteger) -> BigInteger {
        return x
    }
    
    func revert(_ x: BigInteger) -> BigInteger {
        return x
    }
    
    func reduce(_ x:inout BigInteger?)  {
        //debugLog("Method not implemented.")
    }
    
    func sqrTo(_ x: BigInteger,_ r: inout BigInteger?) {
        x.squareTo(r!)
    }
    
    func mulTo(_ x: BigInteger,_ y: BigInteger,_ r:inout BigInteger?) {
        x.multiplyTo(y, r!)
    }
}

// Barrett modular reduction
class Barrett: ConvertProtocol {
    var r2: BigInteger
    var q3: BigInteger
    var mu: BigInteger
    var m: BigInteger
    
    init(_ m: BigInteger) {
        // setup Barrett
        self.r2 = BigInteger.nbi()
        self.q3 = BigInteger.nbi()
        BigInteger.ONE.dlShiftTo(2 * m.t, self.r2)
        self.mu = self.r2.divide(m)
        self.m = m
    }
    
    func convert(_ x: BigInteger) -> BigInteger {
        if x.s < 0 || x.t > 2 * m.t {
            return x.mod(m)
        } else if x.compareTo(m) < 0 {
            return x
        } else {
            var r: BigInteger? = BigInteger.nbi()
            x.copyTo(r!)
            reduce(&r)
            return r!
        }
    }
    
    func revert(_ x: BigInteger) -> BigInteger {
        return x
    }
    
    // x = x mod m (HAC 14.42)
    func reduce(_ x:inout BigInteger?)  {
        x!.drShiftTo(m.t - 1, r2)
        if x!.t > m.t + 1 {
            x!.t = m.t + 1
            x!.clamp()
        }
        mu.multiplyUpperTo(r2, m.t + 1, q3)
        m.multiplyLowerTo(q3, m.t + 1, r2)
        while x!.compareTo(r2) < 0 {
            x!.dAddOffset(1, m.t + 1)
        }
        x!.subTo(r2, x!)
        while x!.compareTo(m) >= 0 {
            x!.subTo(m, x!)
        }
    }
    
    // r = x^2 mod m; x != r
    func sqrTo(_ x: BigInteger,_ r: inout BigInteger?) {
        x.squareTo(r!)
        reduce(&r)
    }
    
    // r = x*y mod m; x,y != r
    func mulTo(_ x: BigInteger,_ y: BigInteger,_ r:inout BigInteger?) {
        x.multiplyTo(y, r!)
        reduce(&r)
    }
}

class Arcfour {
    var i:Int = 0
    var j: Int = 0
    var s: [Int?] = []
    
    // Initialize arcfour context from key, an array of ints, each from [0..255]
    func arc4Init(_ key: [Int?]) {
        var j = 0
        var t = 0
        for i in 0..<256 {
            insertValue(&s, i, i)
        }
        for i in 0..<256 {
            j = (j + s[i]! + key[i % key.count]!) & 255
            t = s[i]!
            insertValue(&s, s[j]!, i)
            insertValue(&s, t, j)
        }
        self.i = 0
        self.j = 0
    }

    func next() -> Int {
        var t: Int
        i = (i + 1) & 255
        j = (j + s[i]!) & 255
        t = s[i]!
        insertValue(&s, s[j]!, i)
        insertValue(&s, t, j)
        return s[(t + s[i]!) & 255]!
    }
}

// Plug in your RNG constructor here
func prng_newstate() -> Arcfour {
    return Arcfour()
}

// Pool size must be a multiple of 4 and greater than 32.
// An array of bytes the size of the pool will be passed to init()
let rng_psize = 256

// For best results, put code like
// <body onClick='rng_seed_time();' onKeyPress='rng_seed_time();'>
// in your main HTML document.

var rng_state: Arcfour?
var rng_pool: [Int?]?
var rng_pptr: Int = 0

// Mix in a 32-bit integer into the pool
func rng_seed_int(_ x: Int) {
    rng_pool![rng_pptr]! ^= x & 255
    rng_pptr += 1
    rng_pool![rng_pptr]! ^= (x >> 8) & 255
    rng_pptr += 1
    rng_pool![rng_pptr]! ^= (x >> 16) & 255
    rng_pptr += 1
    rng_pool![rng_pptr]! ^= (x >> 24) & 255
    rng_pptr += 1
    if rng_pptr >= rng_psize {
        rng_pptr -= rng_psize
    }
}

// Mix in the current time (w/milliseconds) into the pool
func rng_seed_time() {
    // Use pre-computed date to avoid making the benchmark
    // results dependent on the current date.
    rng_seed_int(1122926989487)
}

// Initialize the pool with junk if needed.
func initRng_pool() {
  if(rng_pool == nil) {
      rng_pool = []
      rng_pptr = 0
      var t: Int
      while rng_pptr < rng_psize { // extract some randomness from Math.random()
          t = Int(floor(65536 * Float.random(in: 0..<1)))
          insertValue(&rng_pool!, t >> 8, rng_pptr)
          rng_pptr += 1
          insertValue(&rng_pool!, t & 255, rng_pptr)
          rng_pptr += 1
      }
      rng_pptr = 0
      rng_seed_time()
  }
}

class SecureRandom {
    func nextBytes(_ ba:inout [Int?]) {
        for i in 0..<ba.count {
            insertValue(&ba, rng_get_byte(), i)
        }
    }
    
    func rng_get_byte() -> Int {
        if rng_state == nil {
            rng_seed_time()
            rng_state = prng_newstate()
            rng_state?.arc4Init(rng_pool!)
            rng_pptr = 0
            while rng_pptr < rng_pool!.count{
                insertValue(&rng_pool!, 0, rng_pptr)
                rng_pptr += 1
            }
            rng_pptr = 0
        }
        // TODO: allow reseeding after first request
        return rng_state!.next()
    }
}

// "empty" RSA key constructor
class RSAKey {
    var n: BigInteger?
    var e: Int
    var d: BigInteger?
    var p: BigInteger?
    var q: BigInteger?
    var dmp1: BigInteger?
    var dmq1: BigInteger?
    var coeff: BigInteger?
    
    init() {
        self.n = nil;
        self.e = 0;
        self.d = nil;
        self.p = nil;
        self.q = nil;
        self.dmp1 = nil;
        self.dmq1 = nil;
        self.coeff = nil;
    }
    
    // convert a (hex) string to a bignum object
    func parseBigInt(_ str: String, _ r: Any) -> BigInteger{
        return BigInteger(str, r)
    }
    
    func linebrk(_ s: String, _ n: Int) -> String {
        var ret = ""
        var i = 0
        while i + n < s.count {
            ret += String(s[s.index(s.startIndex, offsetBy: i)..<s.index(s.startIndex, offsetBy: i + n)]) + "\n"
            i += n
        }
        return ret + String(s[s.index(s.startIndex, offsetBy: i)..<s.endIndex])
    }

    func byte2Hex(_ b: Int) -> String {
        if b < 0x10 {
            return "0" + String(b, radix: 16)
        } else {
            return String(b, radix: 16)
        }
    }

    // PKCS#1 (type 2, random) pad input string s to n bytes, and return a bigint
    func pkcs1pad2(_ s: String, _ n: Int) -> BigInteger? {
        if n < s.count + 11 {
            //debugLog("Message too long for RSA")
            return nil
        }
        var temp_n = n
        var ba: [Int?] = []
        var i = s.count - 1
        while i >= 0 && temp_n > 0 {
            temp_n -= 1
            insertValue(&ba, Int(s[s.index(s.startIndex, offsetBy: i)].unicodeScalars.first!.value), temp_n)
            i -= 1
        }
        temp_n -= 1
        insertValue(&ba, 0, temp_n)
        let rng = SecureRandom()
        var x: [Int?] = []
        while temp_n > 2 { // random non-zero pad
            insertValue(&x, 0, 0)
            while x[0] == 0 {
                rng.nextBytes(&x)
            }
            temp_n -= 1
            insertValue(&ba, x[0]!, temp_n)
        }
        temp_n -= 1
        insertValue(&ba, 2, temp_n)
        temp_n -= 1
        insertValue(&ba, 0, temp_n)
        return BigInteger(ba)
    }
    
    // Set the public key fields N and e from hex strings
    func setPublic(_ N: String, _ E: String) {
        if N.count > 0 && E.count > 0 {
            n = parseBigInt(N, 16)
            e = Int(E, radix: 16) ?? 0
        }else{
            //debugLog("Invalid RSA public key")
        }
    }
    
    // Perform raw public operation on "x": return x^e (mod n)
    func doPublic(_ x: BigInteger) -> BigInteger? {
        return x.modPowInt(e, n!)
    }
    
    // Return the PKCS#1 RSA encryption of "text" as an even-length hex string
    func encrypt(_ text: String) -> String? {
        let m = pkcs1pad2(text, (n!.bitLength() + 7) >> 3)
        guard let m = m else {
            return nil
        }
        let c = doPublic(m)
        if c == nil {
            return nil
        }
        let h = c!.toString(16)
        if (h.count & 1) == 0 {
            return h
        } else {
            return "0" + h
        }
    }
    
    // Undo PKCS#1 (type 2, random) padding and, if valid, return the plaintext
    func pkcs1unpad2(_ d: BigInteger, _ n: Int) -> String? {
        let b = d.toByteArray()
        var i = 0
        while i < b.count && b[i] == 0 {
            i += 1
        }
        if b.count - i != n - 1 || b[i] != 2 {
            return nil
        }
        i += 1
        while b[i] != 0 {
            i += 1
            if i >= b.count {
                return nil
            }
        }
        var ret = ""
        i += 1
        while i < b.count {
            ret += String(UnicodeScalar(b[i]!)!)
            i += 1
        }
        return ret
    }
    
    // Set the private key fields N, e, and d from hex strings
    func setPrivate(_ N: String, _ E: String, _ D: String) {
        if  N.count > 0 && E.count > 0 {
            n = parseBigInt(N, 16)
            e = Int(E, radix: 16)!
            d = parseBigInt(D, 16)
        } else {
            //debugLog("Invalid RSA private key")
        }
    }

    // Set the private key fields N, e, d and CRT params from hex strings
    func setPrivateEx(_ N: String, _ E: String, _ D: String, _ P: String, _ Q: String, _ DP: String, _ DQ: String, _ C: String) {
        if N.count > 0 && E.count > 0 {
            n = parseBigInt(N,16)
            e = Int(E, radix: 16) ?? 0
            d = parseBigInt(D, 16)
            p = parseBigInt(P, 16)
            q = parseBigInt(Q, 16)
            dmp1 = parseBigInt(DP, 16)
            dmq1 = parseBigInt(DQ, 16)
            coeff = parseBigInt(C, 16)
        }else{
            //debugLog("Invalid RSA private key")
        }
    }
    
    // Generate a new random private key B bits long, using public expt E
    func generate(_ B: Int, _ E: String) {
        let rng = SecureRandom()
        let qs = B >> 1
        e = Int(E, radix: 16)!
        let ee = BigInteger(E, 16)
        while true {
            while true {
                p = BigInteger(B - qs, 1, rng)
                if p!.subtract(BigInteger.ONE).gcd(ee).compareTo(BigInteger.ONE) == 0 && p!.isProbablePrime(10) {
                    break
                }
            }
            while true {
                q = BigInteger(qs, 1, rng)
                if q!.subtract(BigInteger.ONE).gcd(ee).compareTo(BigInteger.ONE) == 0 && q!.isProbablePrime(10) {
                    break
                }
            }
            if p!.compareTo(q!) <= 0 {
                let t = p
                p = q
                q = t
            }
            let p1 = p!.subtract(BigInteger.ONE)
            let q1 = q!.subtract(BigInteger.ONE)
            let phi = p1.multiply(q1)
            if phi.gcd(ee).compareTo(BigInteger.ONE) == 0 {
                n = p!.multiply(q!)
                d = ee.modInverse(phi)
                dmp1 = d!.mod(p1)
                dmq1 = d!.mod(q1)
                coeff = q!.modInverse(p!)
                break
            }
        }
    }

    // Perform raw private operation on "x": return x^d (mod n)
    func doPrivate(_ x: BigInteger) -> BigInteger? {
        if p == nil || q == nil {
            return x.modPow(d!, n!)
        }
        // TODO: re-calculate any missing CRT params
        var xp = x.mod(p!).modPow(dmp1!, p!)
        let xq = x.mod(q!).modPow(dmq1!, q!)
        while xp.compareTo(xq) < 0 {
            xp = xp.add(p!)
        }
        return (xp.subtract(xq).multiply(coeff!)).mod(p!).multiply(q!).add(xq)
    }

    // Return the PKCS#1 RSA decryption of "ctext".
    // "ctext" is an even-length hex string and the output is a plain string.
    func decrypt(_ ctext: String) -> String? {
        let c = parseBigInt(ctext, 16)
        let m = doPrivate(c)
        if m == nil {
            return nil
        }
        return pkcs1unpad2(m!, (n!.bitLength() + 7) >> 3)
    }
}

let nValue = "a5261939975948bb7a58dffe5ff54e65f0498f9175f5a09288810b8975871e99af3b5dd94057b0fc07535f5f97444504fa35169d461d0d30cf0192e307727c065168c788771c561a9400fb49175e9e6aa4e23fe11af69e9412dd23b0cb6684c4c2429bce139e848ab26d0829073351f4acd36074eafd036a5eb83359d2a698d3"
let eValue = "10001"
let dValue = "8e9912f6d3645894e8d38cb58c0db81ff516cf4c7e5a14c7f1eddb1459d2cded4d8d293fc97aee6aefb861859c8b6a3d1dfe710463e1f9ddc72048c09751971c4a580aa51eb523357a3cc48d31cfad1d4a165066ed92d4748fb6571211da5cb14bc11b6e2df7c1a559e6d5ac1cd5c94703a22891464fba23d0d965086277a161"
let pValue = "d090ce58a92c75233a6486cb0a9209bf3583b64f540c76f5294bb97d285eed33aec220bde14b2417951178ac152ceab6da7090905b478195498b352048f15e7d"
let qValue = "cab575dc652bb66df15a0359609d51d1db184750c00c6698b90ef3465c99655103edbf0d54c56aec0ce3c4d22592338092a126a0cc49f65a4a30d222b411e58f"
let dmp1Value = "1a24bca8e273df2f0e47c199bbf678604e7df7215480c77c8db39f49b000ce2cf7500038acfff5433b7d582a01f1826e6f4d42e1c57f5e1fef7b12aabc59fd25"
let dmq1Value = "3d06982efbbe47339e1f6d36b1216b8a741d410b0c662f54f7118b27b9a4ec9d914337eb39841d8666f3034408cf94f5b62f11c402fc994fe15a05493150d9fd"
let coeffValue = "3a3e731acd8960b7ff9eb81a7ff93bd1cfa74cbd56987db58b4594fb09c09084db1734c8143f98b602b981aaa9243ca28deb69b5b280ee8dcee0fd2625e53250"

let TEXT = "The quick brown fox jumped over the extremely lazy frog! " +
    "Now is the time for all good men to come to the party.";
var encrypted: String?

func encrypt() {
    let RSA = RSAKey()
    RSA.setPublic(nValue, eValue)
    RSA.setPrivateEx(nValue, eValue, dValue, pValue, qValue, dmp1Value, dmq1Value, coeffValue)
    encrypted = RSA.encrypt(TEXT)
}

func decrypt() {
    let RSA = RSAKey()
    RSA.setPublic(nValue, eValue);
    RSA.setPrivateEx(nValue, eValue, dValue, pValue, qValue, dmp1Value, dmq1Value, coeffValue)
    let decrypted = RSA.decrypt(encrypted!)
    if decrypted != TEXT {
//        debugLog("Crypto operation failed")
    }
}

/*
 * @State
 * @Tags Jetstream2
 */
class Benchmark {
    /*
     * @Setup
     */
    func cryptoSetup() {
        initBI_RC()
        initRng_pool()
        setupEngine(am3, 28)
    }
    
    /*
     * @Benchmark
     */
    func runIteration() {
        encrypt()
        decrypt()
    }
}

func cryptoRunIteration() {
    let benchmark = Benchmark()
    let setupStart = Timer().getTime()
    benchmark.cryptoSetup()
    let setupEnd = Timer().getTime()
    //debugLog("setupTime: " + String(setupEnd - setupStart) + " ms")
    var runTimes: [Double] = []
    let runStart = Timer().getTime()
    for _ in 0..<120 {
        let start = Timer().getTime()
        benchmark.runIteration()
        let end = Timer().getTime()
        let runTime = end - start
        runTimes.append(runTime)
        //  debugLog("onceRunTime: " + String(runTime) + " ms")
    }
    let endTime = Timer().getTime()
    //debugLog("runTime: " + String(endTime - runStart) + " ms")
    print("crypto: ms = " + String(endTime - setupStart))
}

fileprivate func debugLog(_ str: String) {
    let isLog = false
    if isLog {
        print(str)
    }
}

class Timer {
   private let CLOCK_REALTIME = 0
   private var time_spec = timespec()
   func getTime() -> Double {
       clock_gettime(Int32(CLOCK_REALTIME), &time_spec)
       return Double(time_spec.tv_sec * 1_000_000 + time_spec.tv_nsec / 1_000) / 1000
   }
}

func insertBigIntegerValue( _ arr: inout [BigInteger?],_ value: BigInteger,_  i: Int) {
    if i < arr.count{
        arr[i] = value
    }else{
        let surplus = i - arr.count
        let surplusArr = [BigInteger?](repeating: nil, count: surplus + 1)
        arr = arr + surplusArr
        arr[i] = value
    }
}

func insertValue( _ arr: inout [Int?],_ value: Int,_  i: Int) {
    if i < arr.count{
        arr[i] = value
    }else{
        let surplus = i - arr.count
        let surplusArr = [Int?](repeating: nil, count: surplus + 1)
        arr = arr + surplusArr
        arr[i] = value
    }
}

cryptoRunIteration()


