/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import Glibc

var checkNumber: Int?

// ------------------------------------------------------------------------
// ------------------------------------------------------------------------

// The rest of this file is the actual ray tracer written by Adam
// Burmister. It's a concatenation of the following files:
//
//   flog/color.js
//   flog/light.js
//   flog/vector.js
//   flog/ray.js
//   flog/scene.js
//   flog/material/basematerial.js
//   flog/material/solid.js
//   flog/material/chessboard.js
//   flog/shape/baseshape.js
//   flog/shape/sphere.js
//   flog/shape/plane.js
//   flog/intersectioninfo.js
//   flog/camera.js
//   flog/background.js
//   flog/engine.js

let debug: Bool = false
func debugLog(_ msg: String) {
    if debug {
        print(msg)
    }
}

// Defined class Color
class Color {
    var red: Float = 0.0
    var green: Float = 0.0
    var blue: Float = 0.0
    
    init(_ r: Float?, _ g: Float?, _ b: Float?) {
        red = r ?? 0.0
        green = g ?? 0.0
        blue = b ?? 0.0
    }
    
    static func add(_ c1: Color, _ c2: Color) -> Color {
        let result = Color(0, 0, 0)
        
        result.red = c1.red + c2.red
        result.green = c1.green + c2.green
        result.blue = c1.blue + c2.blue
        
        return result
    }
    
    static func addScalar(_ c1: Color, _ s: Float) -> Color {
        let result = Color(0, 0, 0)
        
        result.red = c1.red + s
        result.green = c1.green + s
        result.blue = c1.blue + s
        
        result.limit()
        
        return result
    }
    
    func subtract(_ c1: Color, _ c2: Color) -> Color {
        let result = Color(0, 0, 0)
        
        result.red = c1.red - c2.red
        result.green = c1.green - c2.green
        result.blue = c1.blue - c2.blue
        
        return result
    }
    
    static func multiply(_ c1: Color, _ c2: Color) -> Color {
        let result = Color(0, 0, 0)
        
        result.red = c1.red * c2.red
        result.green = c1.green * c2.green
        result.blue = c1.blue * c2.blue
        
        return result
    }
    
    static func multiplyScalar(_ c1: Color, _ f: Float) -> Color {
        let result = Color(0, 0, 0)
        
        result.red = c1.red * f
        result.green = c1.green * f
        result.blue = c1.blue * f
        
        return result
    }
    
    func divideFactor(_ c1: Color, _ f: Float) -> Color {
        let result = Color(0, 0, 0)
        
        result.red = c1.red / f
        result.green = c1.green / f
        result.blue = c1.blue / f
        
        return result
    }
    
    func limit() {
        red = (red > 0.0) ? ((red > 1.0) ? 1.0 : red) : 0.0
        green = (green > 0.0) ? ((green > 1.0) ? 1.0 : green) : 0.0
        blue = (blue > 0.0) ? ((blue > 1.0) ? 1.0 : blue) : 0.0
    }
    
    func distance(_ color: Color) -> Float {
        let d = abs(red - color.red) + abs(green - color.green) + abs(blue - color.blue)
        return d
    }
    
    static func blend(_ c1: Color, _ c2: Color, _ w: Float) -> Color {
        var result = Color(0, 0, 0)
        result = Color.add(Color.multiplyScalar(c1, 1 - w), Color.multiplyScalar(c2, w))
        return result
    }
    
    func brightness() -> Int {
        let r: Int = Int(floor(red * 255))
        let g: Int = Int(floor(green * 255))
        let b: Int = Int(floor(blue * 255))
        return (r * 77 + g * 150 + b * 29) >> 8
    }
    
    func toString() -> String {
        let r = floor(red * 255)
        let g = floor(green * 255)
        let b = floor(blue * 255)
        return "rgb(" + String(r) + "," + String(g) + "," + String(b) + ")"
    }
}

// Defined class Light
class Light {
    var position: Vector?
    var color: Color?
    var intensity: Float = 10.0
    init(_ pos: Vector, _ c: Color, _ i: Float = 10.0) {
        position = pos
        color = c
        intensity = i
    }
    
    func toString() -> String {
        return "Light [" + String(position!.x) + "," + String(position!.y) + "," + String(position!.z) + "]"
    }
}

// Defined class Vector
class Vector {
    var x: Float = 0.0
    var y: Float = 0.0
    var z: Float = 0.0
    
    init(_ xF: Float?, _ yF: Float?, _ zF: Float?) {
        x = xF ?? 0
        y = yF ?? 0
        z = zF ?? 0
    }
    
    func copy(_ vector: Vector) {
        x = vector.x
        y = vector.y
        z = vector.z
    }
    
    func normalize() -> Vector {
        let m = magnitude()
        return Vector(x / m, y / m, z / m)
    }
    
    func magnitude() -> Float {
        return sqrt((x * x) + (y * y) + (z * z))
    }
    
    func cross(_ w: Vector) -> Vector {
        return Vector(-z * w.y + y * w.z, z * w.x - x * w.z, -y * w.x + x * w.y)
    }
    
    func dot(_ w: Vector) -> Float {
        return x * w.x + y * w.y + z * w.z
    }
    
    static func add(_ v: Vector, _ w: Vector) -> Vector {
        return Vector(w.x + v.x, w.y + v.y, w.z + v.z)
    }
    
    static func subtract(_ v: Vector?, _ w: Vector?) -> Vector? {
        if w == nil || v == nil {
            print("Vectors must be defined")
        }
        return Vector(v!.x - w!.x, v!.y - w!.y, v!.z - w!.z)
    }
    
    func multiplyVector(_ v: Vector, _ w: Vector) -> Vector {
        return Vector(v.x * w.x, v.y * w.y, v.z * w.z)
    }
    
    static func multiplyScalar(_ v: Vector, _ w: Float) -> Vector {
        return Vector(v.x * w, v.y * w, v.z * w)
    }
    
    func toString() -> String {
        return "Vector [" + String(x) + "," + String(y) + "," + String(z) + "]"
    }
}

// Defined class Ray
class Ray {
    var position: Vector?
    var direction: Vector?
    
    init(_ pos: Vector, _ dir: Vector) {
        position = pos
        direction = dir
    }
    
    func toString() -> String {
        return "Ray [" + (position?.toString())! + "," + (direction?.toString())! + "]"
    }
}

// Defined class Scene
class Scene {
    var camera = Camera(Vector(0, 0, -5), Vector(0, 0, 1), Vector(0, 1, 0))
    var shapes = Array<Baseshape>()
    var lights = Array<Light>()
    var background = Background(Color(0, 0, 0.5), 0.2)
}

// Defined class BaseMaterial
class BaseMaterial {
    var gloss: Float = 2.0
    var transparency: Float = 0.0
    var reflection: Float = 0.0
    var refraction: Float = 0.50
    var hasTexture: Bool = false
    
    func getColor(_ u: Float, _ v: Float) -> Color? {
        return Color(0, 0, 0)
    }
    
    func wrapUp(_ t: Float) -> Float {
        var p = t.truncatingRemainder(dividingBy: 2.0)
        if p < -1 {
            p += 2.0
        }
        if p >= 1 {
            p -= 2.0
        }
        return p
    }
    
    func toString() -> String {
        return "Material [gloss=" + String(gloss) + ", transparency=" + String(transparency) + ", hasTexture=" + String(hasTexture) + "]"
    }
}

// Defined class Solid
class Solid: BaseMaterial {
    var color: Color?
    init(_ c: Color, _ refle: Float, _ refra: Float, _ tra: Float, _ g: Float) {
        super.init()
        color = c
        reflection = refle
        transparency = tra
        gloss = g
        hasTexture = false
    }
    
    override func getColor(_ u: Float, _ v: Float) -> Color? {
        return color
    }
    
    override func toString() -> String {
        return "SolidMaterial [gloss=" + String(gloss) + ", transparency=" + String(transparency) + ", hasTexture=" + String(hasTexture) + "]"
    }
}

// Defined class Chessboard
class Chessboard: BaseMaterial {
    var colorEven: Color?
    var colorOdd: Color?
    var density: Float = 0.5
    init(_ cEven: Color, _ cOdd: Color, _ refle: Float, _ tra: Float, _ g: Float, _ den: Float) {
        super.init()
        colorEven = cEven
        colorOdd = cOdd
        reflection = refle
        transparency = tra
        gloss = g
        density = den
        hasTexture = true
    }
    
    override func getColor(_ u: Float, _ v: Float) -> Color? {
        let t = wrapUp(u * density) * wrapUp(v * density)
        
        if t < 0.0 {
            return colorEven
        } else {
            return colorOdd
        }
    }
    
    override func toString() -> String {
        return "ChessMaterial [gloss=" + String(gloss) + ", transparency=" + String(transparency) + ", hasTexture=" + String(hasTexture) + "]"
    }
}

// Defined class Shape
class Baseshape {
    var position: Vector = Vector(0, 0, 0)
    var radius: Float = 0.0
    var material: BaseMaterial = BaseMaterial()
    
    func intersect(_ ray: Ray) -> IntersectionInfo? {
        return nil
    }
}

// Defined class Sphere
class Sphere: Baseshape {
    init(_ pos: Vector, _ rad: Float, _ mater: BaseMaterial) {
        super.init()
        radius = rad
        position = pos
        material = mater
    }
    
    override func intersect(_ ray: Ray) -> IntersectionInfo {
        let info = IntersectionInfo()
        info.shape = self
        
        let dst = Vector.subtract(ray.position , position)
        
        let B = dst!.dot(ray.direction!)
        let C = dst!.dot(dst!) - (radius * radius)
        let D = (B * B) - C

        if D > 0 {
            info.isHit = true
            info.distance = Float(-B) - sqrt(D)
            info.position = Vector.add(ray.position!, Vector.multiplyScalar(ray.direction!, info.distance!))
            info.normal = Vector.subtract(info.position, position)!.normalize()
            
            info.color = material.getColor(0, 0)!
        } else {
            info.isHit = false
        }
        return info
    }
    
    func toString() -> String {
        return "Sphere [position=" + position.toString() + ", radius=" + String(radius) + "]"
    }
}

// Defined class Plane
class Plane: Baseshape {
    var d: Float = 0.0
    
    init(_ pos: Vector, _ dF: Float, _ mater: BaseMaterial) {
        super.init()
        position = pos
        d = dF
        material = mater
    }
    
    override func intersect(_ ray: Ray) -> IntersectionInfo? {
        let info = IntersectionInfo()
        
        let Vd = position.dot(ray.direction!)
        if Vd == 0 {
            return info
        }
        
        let t = -(position.dot(ray.position!) + d) / Vd
        if t <= 0 {
            return info
        }
        
        info.shape = self
        info.isHit = true
        
        info.position = Vector.add(ray.position!, Vector.multiplyScalar(ray.direction! , t))
        info.normal = position
        info.distance = t
        
        if (material.hasTexture != nil) {
            let vU = Vector(position.y, position.z, -position.x)
            let vV = vU.cross(position)
            let u = info.position.dot(vU)
            let v = info.position.dot(vV)
            info.color = material.getColor(u, v)!
        } else {
            info.color = material.getColor(0, 0)!
        }
        
        return info
    }
    
    func toString() -> String {
        return "Plane [" + position.toString() + ", d=" + String(d) + "]"
    }
}

// Defined class IntersectionInfo
class IntersectionInfo {
    var isHit: Bool = false
    var hitCount: Int = 0
    var shape: Baseshape = Baseshape()
    var position: Vector = Vector(0, 0, 0)
    var normal: Vector = Vector(0, 0, 0)
    var color: Color = Color(0, 0, 0)
    var distance: Float?

    func toString() -> String {
        return "Intersection [" + position.toString() + "]"
    }
}

// Defined class Camera
class Camera {
    var position: Vector?
    var lookAt: Vector?
    var equator: Vector?
    var up: Vector?
    var screne: Vector?
    
    init(_ pos: Vector, _ lookV: Vector, _ upV: Vector) {
        position = pos
        lookAt = lookV
        up = upV
        equator = lookAt?.normalize().cross(upV)
        screne = Vector.add(position!, lookAt!)
    }
    
    func getRay(_ vx: Float, _ vy: Float) -> Ray {
        let pos = Vector.subtract(screne, Vector.subtract(Vector.multiplyScalar(equator!, vx), Vector.multiplyScalar(up!, vy)))
        pos!.y *= -1
        let dir = Vector.subtract(pos, position)
        
        let ray = Ray(pos!, dir!.normalize())
        
        return ray
    }
    
    func toString() -> String {
        return "Ray []"
    }
}

// Defined class Background
class Background {
    var color: Color?
    var ambience: Float = 0.0
    
    init(_ c: Color, _ amb: Float) {
        color = c
        ambience = amb
    }
}

// Defined class Engine
class Engine {
    var options: Options
    
    init(_ opt: Options) {
        options = opt
        options.canvasHeight /= options.pixelHeight
        options.canvasWidth /= options.pixelWidth
    }
    
    func setPixel(_ x: Float, _ y: Float, _ color: Color) {
        if(x == y) {
            checkNumber! += color.brightness()
        }
    }
    
    func renderScene(_ scene: Scene) {
        checkNumber = 0
        
        let canvasHeight = options.canvasHeight
        let canvasWidth = options.canvasWidth
        

        for y in 0..<canvasHeight {
            for x in 0..<canvasWidth {
                let yp = Float(y) * 1.0 / Float(canvasHeight) * 2 - 1
                let xp = Float(x) * 1.0 / Float(canvasWidth) * 2 - 1
                
                let ray = scene.camera.getRay(Float(xp), Float(yp))
                
                let color = getPixelColor(ray, scene)
                
                setPixel(Float(x), Float(y), color)
                
                //debugLog("y = \(y), x = \(x), ray.position = \(String(describing: ray.position?.toString()))")
                //debugLog("y = \(y), x = \(x), ray.direction = \(String(describing: ray.direction?.toString()))")
                //debugLog("y = \(y), x = \(x), color = \(color.toString())")
            }
        }
        //debugLog("checkNumber = \(String(describing: checkNumber))")
        if checkNumber != 2321 {
            print("Scene rendered incorrectly")
        }
    }
    
    func getPixelColor(_ ray: Ray, _ scene: Scene) -> Color {
        let info = testIntersection(ray, scene, nil)
        if info.isHit {
            let color = rayTrace(info, ray, scene, 0)
            return color
        }
        return scene.background.color!
    }
    
    func testIntersection(_ ray: Ray, _ scene: Scene, _ exclude: Baseshape?) -> IntersectionInfo {
        var hits: Int = 0
        var best = IntersectionInfo()
        best.distance = 2000
        
        for i in 0..<scene.shapes.count {
            let shape = scene.shapes[i]
            
            if shape !== exclude {
                let info = shape.intersect(ray)
                if info!.isHit && (info?.distance!)! >= 0 && (info?.distance!)! < best.distance! {
                    best = info!
                    hits += 1
                }
            }
        }
        best.hitCount = hits
        return best
    }
    
    func getReflectionRay(_ P: Vector, _ N: Vector, _ V: Vector) -> Ray {
        let c1 = -N.dot(V)
        let R1 = Vector.add(Vector.multiplyScalar(N, 2 * c1), V)
        return Ray(P, R1)
    }
    
    func rayTrace(_ info: IntersectionInfo, _ ray: Ray, _ scene: Scene, _ depth: Int) -> Color {
        // Calc ambient
        var color = Color.multiplyScalar(info.color, scene.background.ambience)
        let oldColor = color
        let shininess = pow(10.0, info.shape.material.gloss + 1)
        
        for i in 0..<scene.lights.count {
            let light = scene.lights[i]
            
            // Calc diffuse lighting
            let v = Vector.subtract(light.position, info.position)!.normalize()
            if options.renderDiffuse {
                let L = v.dot(info.normal)
                if L > 0.0 {
                    color = Color.add(color, Color.multiply(info.color, Color.multiplyScalar(light.color!, L)))
                }
            }
            
            // The greater the depth the more accurate the colours, but
            // this is exponentially (!) expensive
            if depth <= options.rayDepth {
                // calculate reflection ray
                if options.renderReflections && (info.shape.material.reflection) > 0 {
                    let reflectionRay = getReflectionRay(info.position, info.normal, ray.direction!)
                    let refl = testIntersection(reflectionRay, scene, info.shape)
                    
                    if refl.isHit && refl.distance! > 0 {
                        refl.color = rayTrace(refl, reflectionRay, scene, depth + 1)
                    } else {
                        refl.color = scene.background.color!
                    }
                    color = Color.blend(color, refl.color, info.shape.material.reflection)
                }
            }

            // Render shadows and highlights

            var shadowInfo = IntersectionInfo()
            
            if options.renderShadows {
                let shadowRay = Ray(info.position, v)
                
                shadowInfo = testIntersection(shadowRay, scene, info.shape)
                if shadowInfo.isHit && shadowInfo.shape !== info.shape {
                    let vA = Color.multiplyScalar(color, 0.5)
                    let dB = 0.5 * pow(shadowInfo.shape.material.transparency, 0.5)
                    color = Color.addScalar(vA, dB)
                }
            }

            // Phong specular highlights
            if options.renderHighlights && !shadowInfo.isHit && (info.shape.material.gloss) > 0 {
                let Lv = Vector.subtract(info.shape.position, light.position)!.normalize()
                
                let E = Vector.subtract(scene.camera.position, info.shape.position)!.normalize()
                
                let H = Vector.subtract(E, Lv)!.normalize()
                
                let glossWeight = pow(max(info.normal.dot(H), 0.0), shininess)
                color = Color.add(Color.multiplyScalar(light.color!, Float(glossWeight)), color)
            }
        }
        color.limit()
        return color
    }
}

// Defined class Options
class Options {
    var canvasHeight = 100;
    var canvasWidth  = 100;
    var pixelWidth  = 2;
    var pixelHeight  = 2;
    var renderDiffuse = false;
    var renderShadows = false;
    var renderHighlights = false;
    var renderReflections = false;
    var rayDepth = 2;
}

func renderScene() {
    let scene = Scene()
    
    scene.camera = Camera(Vector(0, 0, -15), Vector(-0.2, 0, 5), Vector(0, 1, 0))
    
    scene.background = Background(Color(0.5, 0.5, 0.5), 0.4)
    
    let sphere = Sphere(Vector(-1.5, 1.5, 2), 1.5, Solid(Color(0, 0.5, 0.5), 0.3, 0.0, 0.0, 2.0))
    
    let sphere1 = Sphere(Vector(1, 0.25, 1), 0.5, Solid(Color(0.9, 0.9, 0.9), 0.1, 0.0, 0.0, 1.5))

    let plane = Plane(Vector(0.1, 0.9, -0.5).normalize(), 1.2, Chessboard(Color(1, 1, 1), Color(0, 0, 0), 0.2, 0.0, 1.0, 0.7))
    
    scene.shapes.append(sphere)
    scene.shapes.append(sphere1)
    scene.shapes.append(plane)
            
    let light = Light(Vector(5, 10, -1), Color(0.8, 0.8, 0.8))

    let light1 = Light(Vector(-3, 5, -15), Color(0.8, 0.8, 0.8), 100)
    
    scene.lights.append(light)
    scene.lights.append(light1)
    
    let option = Options()
    option.canvasWidth = 100
    option.canvasHeight = 100
    option.pixelWidth = 5
    option.pixelHeight = 5
    option.renderDiffuse = true
    option.renderHighlights = true
    option.renderShadows = true
    option.renderReflections = true
    option.rayDepth = 2
    
    let raytracer = Engine(option)
    
    raytracer.renderScene(scene)
    
    //debugLog("Create camera: ")
    //debugLog(scene.camera.toString())
    //debugLog("Create Background： ")
    //debugLog((scene.background.color?.toString())!)
    //debugLog("Create sphere: ")
    //debugLog(sphere.toString())
    //debugLog("Create sphere1: ")
    //debugLog(sphere1.toString())
    //debugLog("Create plane: ")
    //debugLog(plane.toString())
    //debugLog("Create light: ")
    //debugLog(light.toString())
    //debugLog("Create light1: ")
    //debugLog(light1.toString())
    //debugLog("Create option ")
    //debugLog(String(describing: option))
}

/**
 * @State
 * @Tags Jetstream2
 */
class Benchmark {
    /**
     * @Benchmark
     */
    func runIteration() {
        for _ in 0..<5 {
            renderScene()
        }
    }
}

class Timer {
    private let CLOCK_REALTIME = 0
    private var time_spec = timespec()

    func getTime() -> Double {
        clock_gettime(Int32(CLOCK_REALTIME),&time_spec)
        return Double(time_spec.tv_sec * 1_000_000 + time_spec.tv_nsec / 1_000)
    }
}

func run() {
    let startTime = Timer().getTime()
    let raytrace = Benchmark()
    for _ in 0..<20 {
        raytrace.runIteration()
    }
    let endTime = Timer().getTime()
    let duration = (endTime - startTime) / 1000
    print("raytrace: ms = \(duration)")
}

run()





