--case-path ts-swift-workload
--ts-tools-path ~/workspace/daily/dev/code
--tools-type dev
--swift-tools-path ~/tools/swift-5.7.3-RELEASE-ubuntu22.04/usr/bin
--android-ndk ~/apple/android-ndk-r25c
--Ninja-ReleaseAssert ~/apple/build/Ninja-ReleaseAssert
end