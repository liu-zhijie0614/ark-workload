/*
 * Copyright (c) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef FOUNDATION_ACE_NAPI_INTERFACES_KITS_NAPI_NATIVE_API_H
#define FOUNDATION_ACE_NAPI_INTERFACES_KITS_NAPI_NATIVE_API_H

#ifndef NAPI_VERSION
#define NAPI_VERSION 8
#endif

#ifndef NAPI_EXPERIMENTAL
#define NAPI_EXPERIMENTAL
#endif

#include "common.h"
#include "node_api.h"

#ifdef NAPI_TEST
#ifdef _WIN32
#define NAPI_INNER_EXTERN __declspec(dllexport)
#else
#define NAPI_INNER_EXTERN __attribute__((visibility("default")))
#endif
#else
#ifdef _WIN32
#define NAPI_INNER_EXTERN __declspec(deprecated)
#else
#define NAPI_INNER_EXTERN __attribute__((__deprecated__))
#endif
#endif

NAPI_INNER_EXTERN napi_status napi_set_instance_data(napi_env env, void *data, napi_finalize finalize_cb,
    void *finalize_hint);

NAPI_INNER_EXTERN napi_status napi_get_instance_data(napi_env env, void **data);

NAPI_INNER_EXTERN napi_status napi_fatal_exception(napi_env env, napi_value err);

NAPI_INNER_EXTERN napi_status napi_add_env_cleanup_hook(napi_env env, void (*fun)(void *arg), void *arg);

NAPI_INNER_EXTERN napi_status napi_remove_env_cleanup_hook(napi_env env, void (*fun)(void *arg), void *arg);

NAPI_INNER_EXTERN napi_status napi_add_async_cleanup_hook(napi_env env, napi_async_cleanup_hook hook, void *arg,
    napi_async_cleanup_hook_handle *remove_handle);

NAPI_INNER_EXTERN napi_status napi_remove_async_cleanup_hook(napi_async_cleanup_hook_handle remove_handle);

NAPI_EXTERN napi_status napi_create_string_utf16(napi_env env, const char16_t *str, size_t length, napi_value *result);

NAPI_EXTERN napi_status napi_get_value_string_utf16(napi_env env, napi_value value, char16_t *buf, size_t bufsize,
    size_t *result);

NAPI_EXTERN napi_status napi_type_tag_object(napi_env env, napi_value value, const napi_type_tag *type_tag);

NAPI_EXTERN napi_status napi_check_object_type_tag(napi_env env, napi_value value, const napi_type_tag *type_tag,
    bool *result);

NAPI_INNER_EXTERN napi_status napi_add_finalizer(napi_env env, napi_value js_object, void *native_object,
    napi_finalize finalize_cb, void *finalize_hint, napi_ref *result);

NAPI_INNER_EXTERN napi_status napi_async_init(napi_env env, napi_value async_resource, napi_value async_resource_name,
    napi_async_context *result);

NAPI_INNER_EXTERN napi_status napi_async_destroy(napi_env env, napi_async_context async_context);

NAPI_INNER_EXTERN napi_status napi_open_callback_scope(napi_env env, napi_value resource_object,
    napi_async_context context, napi_callback_scope *result);

NAPI_INNER_EXTERN napi_status napi_close_callback_scope(napi_env env, napi_callback_scope scope);

NAPI_INNER_EXTERN napi_status napi_adjust_external_memory(napi_env env, int64_t change_in_bytes,
    int64_t *adjusted_value);

NAPI_INNER_EXTERN napi_status node_api_get_module_file_name(napi_env env, const char **result);

#ifdef __cplusplus
extern "C" {
#endif

NAPI_EXTERN napi_status napi_run_script_path(napi_env env, const char *path, napi_value *result);
NAPI_EXTERN napi_status napi_queue_async_work_with_qos(napi_env env, napi_async_work work, napi_qos_t qos);
NAPI_EXTERN napi_status napi_test_isfunction(napi_env env, bool *result);
NAPI_EXTERN napi_status napi_test_isnativepointer(napi_env env, bool *result);
NAPI_EXTERN napi_status napi_test_isnull(napi_env env, bool *result);
NAPI_EXTERN napi_status napi_test_isboolean(napi_env env, bool *result, bool *res);
NAPI_EXTERN napi_status napi_test_isundefined(napi_env env, bool *result);
NAPI_EXTERN napi_status napi_test_symbol(napi_env env, bool *result, bool *res);
NAPI_EXTERN napi_status napi_test_isbigint(napi_env env, bool *result, bool *res);
NAPI_EXTERN napi_status napi_test_isobject(napi_env env, bool *result);
NAPI_EXTERN napi_status napi_test_writelatin1(napi_env env, int64_t res);
NAPI_EXTERN napi_status napi_test_utf8length(napi_env env, int64_t result);
NAPI_EXTERN napi_status napi_test_writeutf8(napi_env env, int64_t BufferSize, bool *result);
NAPI_EXTERN napi_status napi_test_length(napi_env env, uint32_t result);
NAPI_EXTERN napi_status napi_test_objectref_new(napi_env env, bool *result);
NAPI_EXTERN napi_status napi_test_arrayref_new(napi_env env, bool *result, bool *res);
NAPI_EXTERN napi_status napi_test_numberref_new_double(napi_env env, bool *result);
NAPI_EXTERN napi_status napi_test_numberref_new_int32(napi_env env, bool *result);
NAPI_EXTERN napi_status napi_load_module(napi_env env, const char *path, napi_value *result);
NAPI_EXTERN napi_status napi_test_stringref_to_string(napi_env env, bool *result);
NAPI_EXTERN napi_status napi_test_string_write16(napi_env env, int32_t *result);
NAPI_EXTERN napi_status napi_test_value_to_boolean(napi_env env, bool *result);
NAPI_EXTERN napi_status napi_test_value_to_number(napi_env env, int32_t *result);
NAPI_EXTERN napi_status napi_test_value_to_object(napi_env env, bool *result);
NAPI_EXTERN napi_status napi_test_object_get_prototype(napi_env env, bool *result);
NAPI_EXTERN napi_status napi_test_is_int8_array(napi_env env, bool *result);
NAPI_EXTERN napi_status napi_test_object_seal(napi_env env, bool *result);
NAPI_EXTERN napi_status napi_test_object_freeze(napi_env env, bool *result);
NAPI_EXTERN napi_status napi_test_get_function_prototype(napi_env env, bool *result);
NAPI_EXTERN napi_status napi_test_value_undefined(napi_env env, bool *result);
NAPI_EXTERN napi_status napi_test_value_null(napi_env env, bool *result);
NAPI_EXTERN napi_status napi_test_get_global_object(napi_env env, bool *result);
NAPI_EXTERN napi_status napi_test_boolean_new(napi_env env, bool *result);
NAPI_EXTERN napi_status napi_test_bytelength(napi_env env, bool *result);
NAPI_EXTERN napi_status napi_test_newpromise(napi_env env, bool *result);
NAPI_EXTERN napi_status napi_test_bigint_to_int64(napi_env env, bool *result);
NAPI_EXTERN napi_status napi_test_bigint_to_uint64(napi_env env, bool *result);
NAPI_EXTERN napi_status napi_test_isdetach(napi_env env, bool *result);
NAPI_EXTERN napi_status napi_test_get_property_names(napi_env env, bool *result);
NAPI_EXTERN napi_status napi_test_gettime(napi_env env, bool *result);
NAPI_EXTERN napi_status napi_test_create_bigwords(napi_env env, bool *result);
NAPI_EXTERN napi_status napi_test_get_wordsarray(napi_env env, bool *result);
NAPI_EXTERN napi_status napi_test_isnumber(napi_env env, bool *result);
NAPI_EXTERN napi_status napi_test_isstring(napi_env env, bool *result);
NAPI_EXTERN napi_status napi_test_functionref(napi_env env, bool *result);
NAPI_EXTERN napi_status napi_test_hasobject(napi_env env, bool *result);
NAPI_EXTERN napi_status napi_test_setobject(napi_env env, bool *result);
NAPI_EXTERN napi_status napi_test_deleteobject(napi_env env, bool *result);
NAPI_EXTERN napi_status napi_test_getobject(napi_env env, bool *result);
NAPI_EXTERN napi_status napi_test_throwexception1(napi_env env, napi_value error);
NAPI_EXTERN napi_status napi_test_arraybufferref_new(napi_env env, bool *result);
NAPI_EXTERN napi_status napi_test_bufferref_new(napi_env env, bool *result);
NAPI_EXTERN napi_status napi_test_reinstanceof(napi_env env, napi_value object, napi_value constructor, bool *result);
NAPI_EXTERN napi_status napi_test_isarray(napi_env env, bool &result);
NAPI_EXTERN napi_status napi_test_getownenumberablepropertynames(napi_env env, bool &result);
NAPI_EXTERN napi_status napi_test_isarraybuffer(napi_env env, bool &result);
NAPI_EXTERN napi_status napi_test_isdataview(napi_env env, bool &result);
NAPI_EXTERN napi_status napi_test_isdate_t(napi_env env, bool &result);
NAPI_EXTERN napi_status napi_test_ispromise_t(napi_env env, bool &result);
NAPI_EXTERN napi_status napi_test_has_t(napi_env env, bool &result);
NAPI_EXTERN napi_status napi_test_delete_t(napi_env env, bool &result);
NAPI_EXTERN napi_status napi_test_isbuffer_t(napi_env env, bool &result);
NAPI_EXTERN napi_status napi_test_istypedarray_t(napi_env env, bool &result);
NAPI_EXTERN napi_status napi_test_set_t(napi_env env, bool &result);
NAPI_EXTERN napi_status napi_test_get_t(napi_env env, bool &result);
NAPI_EXTERN napi_status napi_test_new_t(napi_env env, bool &result);
NAPI_EXTERN napi_status napi_test_newformutf_t(napi_env env, bool &result);

#ifdef __cplusplus
}
#endif
#endif /* FOUNDATION_ACE_NAPI_INTERFACES_KITS_NAPI_NATIVE_API_H */
