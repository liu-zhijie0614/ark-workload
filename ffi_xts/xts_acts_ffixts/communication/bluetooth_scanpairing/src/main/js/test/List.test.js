/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import btBleScanTest from './BtBleScanResult.test.js'
import btDiscoveryTest from './BtDiscovery.test.js'
import btManagerBleScanTest from './BtManagerBleScanResult.test.js'
import btPairTest from './BtPair.test.js'
import btManagerDiscoveryTest from './BtManagerDiscovery.test.js'
import btManagerPairTest from './BtManagerPair.test.js'
import btGattManagerTest from './BtGattManager.test.js'

export default function testsuite() {  
    btPairTest()
    btBleScanTest()
    btDiscoveryTest()
    btManagerBleScanTest()
    btManagerDiscoveryTest()
    btManagerPairTest()
    btGattManagerTest()
}
