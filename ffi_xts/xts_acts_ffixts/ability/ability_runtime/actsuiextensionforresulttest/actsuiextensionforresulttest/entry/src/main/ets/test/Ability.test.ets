/*
 * Copyright (c) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import hilog from '@ohos.hilog';
import { describe, beforeAll, beforeEach, afterEach, afterAll, it, expect } from '@ohos/hypium'
import commonEvent from '@ohos.commonEventManager'
import abilityDelegatorRegistry from '@ohos.app.ability.abilityDelegatorRegistry';
import { Driver } from '@ohos.UiTest';

let abilityDelegator = abilityDelegatorRegistry.getAbilityDelegator()
var ACTS_CallFunction = {
  events: ['ACTS_CALL_EVENT', 'ACTS_CALL_EVENT_ANOTHER']
};

export default function abilityTest() {
  describe('ActsAbilityTest', function () {
    // Defines a test suite. Two parameters are supported: test suite name and test suite function.
    beforeAll(function () {
      // Presets an action, which is performed only once before all test cases of the test suite start.
      // This API supports only one parameter: preset action function.
    })
    beforeEach(function () {
      // Presets an action, which is performed before each unit test case starts.
      // The number of execution times is the same as the number of test cases defined by **it**.
      // This API supports only one parameter: preset action function.
      // setTimeout(() => {
      //   console.info(`====>beforeEach setTimeout'`)
      // }, 2000)
    })
    afterEach(function () {
      // Presets a clear action, which is performed after each unit test case ends.
      // The number of execution times is the same as the number of test cases defined by **it**.
      // This API supports only one parameter: clear action function.
    })
    afterAll(function () {
      // Presets a clear action, which is performed after all test cases of the test suite end.
      // This API supports only one parameter: clear action function.
    })

    it('ACTS_UIExtensionAbility_StartAbilityForResult_AsyncCallback_0100',0, async function (done) {
      var subscriber
      commonEvent.createSubscriber(ACTS_CallFunction).then(async (data) => {
        console.info(`====>AsyncCallback_0100 createSubscriber  ${data}`)
        subscriber = data
        commonEvent.subscribe(data, subscribeCallBack)
        globalThis.context.startAbility({
          bundleName: 'com.example.mainhap',
          abilityName: 'EntryAbility',
          action: 'AsyncCallback_0100'
        },(err) => {
          console.info('====>startAbility err:' + JSON.stringify(err));
        })
      })

      function subscribeCallBack(err, data) {
        if(data.event == 'ACTS_CALL_EVENT') {
          console.info(`====>AsyncCallback_0100 subscribeCallBack 'ACTS_CALL_EVENT'  ${data}`)
          expect(data.parameters.str).assertEqual('ACTION');
          expect(data.parameters.result).assertEqual(0);
          commonEvent.unsubscribe(subscriber, unSubscribeCallback)
        }
      }

      function unSubscribeCallback() {
        console.info(`====>AsyncCallback_0100 unSubscribeCallback 'ACTS_CALL_EVENT'`)
        globalThis.terminate('AsyncCallback_0100')
        done()
      }
    })

    it('ACTS_UIExtensionAbility_StartAbilityForResult_AsyncCallback_0200',0, async function (done) {
      await Driver.create().delayMs(2000)
      var subscriber
      commonEvent.createSubscriber(ACTS_CallFunction).then(async (data) => {
        console.info(`====>AsyncCallback_0200 createSubscriber  ${data}`)
        subscriber = data
        commonEvent.subscribe(data, subscribeCallBack)
        globalThis.context.startAbility({
          bundleName: 'com.example.forresulttestsecond',
          abilityName: 'EntryAbility',
          action: 'AsyncCallback_0200'
        },(err) => {
          console.info('====>startAbility err:' + JSON.stringify(err));
        })
      })

      function subscribeCallBack(err, data) {
        if(data.event == 'ACTS_CALL_EVENT') {
          console.info(`====>AsyncCallback_0200 subscribeCallBack 'ACTS_CALL_EVENT'  ${data}`)
          expect(data.parameters.str).assertEqual('ACTION');
          expect(data.parameters.result).assertEqual(0);
          commonEvent.unsubscribe(subscriber, unSubscribeCallback)
        }
      }

      function unSubscribeCallback() {
        console.info(`====>AsyncCallback_0200 unSubscribeCallback 'ACTS_CALL_EVENT'`)
        setTimeout(() => {
          abilityDelegator.executeShellCommand('aa force-stop com.example.forresulttestsecond').then(result =>{
            console.info('====>AsyncCallback_0200 unSubscribeCallback pkill ok' + JSON.stringify(result));
            done()
          }).catch(err => {
            console.info('====>AsyncCallback_0200 unSubscribeCallback pkill ng' + JSON.stringify(err));
            done()
          })
        }, 2000)
      }
    })

    it('ACTS_UIExtensionAbility_StartAbilityForResult_AsyncCallback_0300',0, async function (done) {
      await Driver.create().delayMs(2000)
      var subscriber
      commonEvent.createSubscriber(ACTS_CallFunction).then(async (data) => {
        console.info(`====>AsyncCallback_0300 createSubscriber  ${data}`)
        subscriber = data
        commonEvent.subscribe(data, subscribeCallBack)
        globalThis.context.startAbility({
          bundleName: 'com.example.mainhap',
          abilityName: 'EntryAbility',
          action: 'AsyncCallback_0300'
        },(err) => {
          console.info('====>start Own Ability err:' + JSON.stringify(err));
        })
        setTimeout(() => {
          globalThis.context.startAbility({
            bundleName: 'com.example.forresulttestsecond',
            abilityName: 'EntryAbility',
            action: 'AsyncCallback_0300'
          },(err) => {
            console.info('====>start Another Ability err:' + JSON.stringify(err));
          })
        }, 2500);
      })

      function subscribeCallBack(err, data) {
        if(data.event == 'ACTS_CALL_EVENT') {
          console.info(`====>AsyncCallback_0300 subscribeCallBack 'ACTS_CALL_EVENT'  ${data}`)
          expect(data.parameters.result).assertEqual(-1);
        }
        if(data.event == 'ACTS_CALL_EVENT_ANOTHER') {
          console.info(`====>AsyncCallback_0300 subscribeCallBack 'ACTS_CALL_EVENT_ANOTHER'  ${data}`)
          expect(data.parameters.str).assertEqual('ACTION');
          expect(data.parameters.result).assertEqual(0);
          commonEvent.unsubscribe(subscriber, unSubscribeCallback)
        }
      }

      function unSubscribeCallback() {
        console.info(`====>AsyncCallback_0300 unSubscribeCallback 'ACTS_CALL_EVENT'`)
        setTimeout(() => {
          globalThis.terminate('AsyncCallback_0300')
          abilityDelegator.executeShellCommand('aa force-stop com.example.forresulttestsecond').then((result) =>{
            console.info('====>AsyncCallback_0300 unSubscribeCallback pkill ok' + JSON.stringify(result));
            done()
          }).catch((err) => {
            console.info('====>AsyncCallback_0300 unSubscribeCallback pkill ng' + JSON.stringify(err));
            done()
          })
        }, 1000)
      }
    })

    it('ACTS_UIExtensionAbility_StartAbilityForResult_AsyncCallback_0400',0, async function (done) {
      await Driver.create().delayMs(2000)
      var subscriber
      commonEvent.createSubscriber(ACTS_CallFunction).then(async (data) => {
        console.info(`====>AsyncCallback_0400 createSubscriber  ${data}`)
        subscriber = data
        commonEvent.subscribe(data, subscribeCallBack)
        globalThis.context.startAbility({
          bundleName: 'com.example.mainhap',
          abilityName: 'EntryAbility',
          action: 'AsyncCallback_0400'
        },(err) => {
          console.info('====>start Own Ability err:' + JSON.stringify(err));
        })

        setTimeout(() => {
          abilityDelegator.executeShellCommand('aa force-stop com.example.forresulttestsecond').then(result =>{
            console.info('====>AsyncCallback_0400 pkill ok' + JSON.stringify(result));
          }).catch(err => {
            console.info('====>AsyncCallback_0400 pkill ng' + JSON.stringify(err));
          })
        }, 2000)
      })

      function subscribeCallBack(err, data) {
        console.info(`====>AsyncCallback_0400 subscribeCallBack all  ${data.event}`)
        if(data.event == 'ACTS_CALL_EVENT') {
          console.info(`====>AsyncCallback_0400 subscribeCallBack 'ACTS_CALL_EVENT'  ${data}`)
          expect(data.parameters.result).assertEqual(-1);
          commonEvent.unsubscribe(subscriber, unSubscribeCallback)
        }
      }

      function unSubscribeCallback() {
        console.info(`====>AsyncCallback_0400 unSubscribeCallback 'ACTS_CALL_EVENT'`)

        setTimeout(() => {
          globalThis.terminate('AsyncCallback_0400')
          done()
        }, 1000)
      }
    })

    it('ACTS_UIExtensionAbility_StartAbilityForResult_AsyncCallback_0500',0, async function (done) {
      await Driver.create().delayMs(2000)
      var subscriber
      commonEvent.createSubscriber(ACTS_CallFunction).then(async (data) => {
        console.info(`====>AsyncCallback_0500 createSubscriber  ${data}`)
        subscriber = data
        commonEvent.subscribe(data, subscribeCallBack)
        globalThis.context.startAbility({
          bundleName: 'com.example.mainhap',
          abilityName: 'EntryAbility',
          action: 'AsyncCallback_0500'
        },(err) => {
          console.info('====>start Own Ability err:' + JSON.stringify(err));
        })
      })

      function subscribeCallBack(err, data) {
        console.info(`====>AsyncCallback_0500 subscribeCallBack all  ${data.event}`)
        if(data.event == 'ACTS_CALL_EVENT') {
          console.info(`====>AsyncCallback_0500 subscribeCallBack 'ACTS_CALL_EVENT'  ${data}`)
          expect(data.parameters.num).assertEqual(16000001);
          commonEvent.unsubscribe(subscriber, unSubscribeCallback)
        }
      }

      function unSubscribeCallback() {
        console.info(`====>AsyncCallback_0500 unSubscribeCallback 'ACTS_CALL_EVENT'`)
        setTimeout(() => {
          globalThis.terminate('AsyncCallback_0500')
          done()
        }, 2000)
      }
    })

    it('ACTS_UIExtensionAbility_StartAbilityForResult_AsyncCallback_0600',0, async function (done) {
      await Driver.create().delayMs(2000)
      var subscriber
      commonEvent.createSubscriber(ACTS_CallFunction).then(async (data) => {
        console.info(`====>AsyncCallback_0600 createSubscriber  ${data}`)
        subscriber = data
        commonEvent.subscribe(data, subscribeCallBack)
        globalThis.context.startAbility({
          bundleName: 'com.example.forresulttestthrid',
          abilityName: 'EntryAbility',
          action: 'AsyncCallback_0600'
        },(err) => {
          console.info('====>start Own Ability err:' + JSON.stringify(err));
        })
      })

      function subscribeCallBack(err, data) {
        console.info(`====>AsyncCallback_0600 subscribeCallBack all  ${data.event}`)
        if(data.event == 'ACTS_CALL_EVENT') {
          console.info(`====>AsyncCallback_0600 subscribeCallBack 'ACTS_CALL_EVENT'  ${data}`)
          console.info(`====>AsyncCallback_0600 subscribeCallBack  ${data.parameters.num}`)
          expect(data.parameters.num).assertEqual(16000004);
          commonEvent.unsubscribe(subscriber, unSubscribeCallback)
        }
      }

      function unSubscribeCallback() {
        console.info(`====>AsyncCallback_0600 unSubscribeCallback 'ACTS_CALL_EVENT'`)
        setTimeout(() => {
          abilityDelegator.executeShellCommand('aa force-stop com.example.forresulttestthrid').then(result =>{
            console.info('====>AsyncCallback_0600 unSubscribeCallback pkill ok' + JSON.stringify(result));
            done()
          }).catch(err => {
            console.info('====>AsyncCallback_0600 unSubscribeCallback pkill ng' + JSON.stringify(err));
            done()
          })
        }, 1000)
      }
    })


    it('ACTS_UIExtensionAbility_StartAbilityForResult_AsyncCallback_0700',0, async function (done) {
      await Driver.create().delayMs(2000)
      var subscriber
      commonEvent.createSubscriber(ACTS_CallFunction).then(async (data) => {
        console.info(`====>AsyncCallback_0700 createSubscriber  ${data}`)
        subscriber = data
        commonEvent.subscribe(data, subscribeCallBack)
        globalThis.context.startAbility({
          bundleName: 'com.example.forresulttestthrid',
          abilityName: 'EntryAbility',
          action: 'AsyncCallback_0700'
        },(err) => {
          console.info('====>start Own Ability err:' + JSON.stringify(err));
        })
      })

      function subscribeCallBack(err, data) {
        console.info(`====>AsyncCallback_0700 subscribeCallBack all  ${data.event}`)
        if(data.event == 'ACTS_CALL_EVENT') {
          console.info(`====>AsyncCallback_0700 subscribeCallBack 'ACTS_CALL_EVENT'  ${data}`)
          expect(data.parameters.num).assertEqual(201);
          commonEvent.unsubscribe(subscriber, unSubscribeCallback)
        }
      }

      function unSubscribeCallback() {
        console.info(`====>AsyncCallback_0700 unSubscribeCallback 'ACTS_CALL_EVENT'`)

        setTimeout(() => {
          abilityDelegator.executeShellCommand('aa force-stop com.example.forresulttestthrid').then(result =>{
            console.info('====>AsyncCallback_0700 unSubscribeCallback pkill ok' + JSON.stringify(result));
            done()
          }).catch(err => {
            console.info('====>AsyncCallback_0700 unSubscribeCallback pkill ng' + JSON.stringify(err));
            done()
          })
        }, 2000)
      }
    })

    it('ACTS_UIExtensionAbility_StartAbilityForResult_Promise_0100',0, async function (done) {
      await Driver.create().delayMs(2000)
      var subscriber
      commonEvent.createSubscriber(ACTS_CallFunction).then(async (data) => {
        console.info(`====>Promise_0100 createSubscriber  ${data}`)
        subscriber = data
        commonEvent.subscribe(data, subscribeCallBack)
        globalThis.context.startAbility({
          bundleName: 'com.example.mainhap',
          abilityName: 'EntryAbility',
          action: 'Promise_0100'
        },(err) => {
          console.info('====>startAbility err:' + JSON.stringify(err));
        })
      })

      function subscribeCallBack(err, data) {
        if(data.event == 'ACTS_CALL_EVENT') {
          console.info(`====>Promise_0100 subscribeCallBack 'ACTS_CALL_EVENT'  ${data}`)
          expect(data.parameters.str).assertEqual('ACTION');
          expect(data.parameters.result).assertEqual(0);
          commonEvent.unsubscribe(subscriber, unSubscribeCallback)
        }
      }

      function unSubscribeCallback() {
        console.info(`====>Promise_0100 unSubscribeCallback 'ACTS_CALL_EVENT'`)
        globalThis.terminate('Promise_0100')
        done()
      }
    })

    it('ACTS_UIExtensionAbility_StartAbilityForResult_Promise_0200',0, async function (done) {
      var subscriber
      commonEvent.createSubscriber(ACTS_CallFunction).then(async (data) => {
        console.info(`====>Promise_0200 createSubscriber  ${data}`)
        subscriber = data
        commonEvent.subscribe(data, subscribeCallBack)
        globalThis.context.startAbility({
          bundleName: 'com.example.forresulttestsecond',
          abilityName: 'EntryAbility',
          action: 'Promise_0200'
        },(err) => {
          console.info('====>startAbility err:' + JSON.stringify(err));
        })
      })

      function subscribeCallBack(err, data) {
        if(data.event == 'ACTS_CALL_EVENT') {
          console.info(`====>Promise_0200 subscribeCallBack 'ACTS_CALL_EVENT'  ${data}`)
          expect(data.parameters.str).assertEqual('ACTION');
          expect(data.parameters.result).assertEqual(0);
          commonEvent.unsubscribe(subscriber, unSubscribeCallback)
        }
      }

      function unSubscribeCallback() {
        console.info(`====>Promise_0200 unSubscribeCallback 'ACTS_CALL_EVENT'`)
          abilityDelegator.executeShellCommand('aa force-stop com.example.forresulttestsecond').then(result =>{
            console.info('====>Promise_0200 unSubscribeCallback pkill ok' + JSON.stringify(result));
            done()
          })
      }
    })

    it('ACTS_UIExtensionAbility_StartAbilityForResult_Promise_0300',0, async function (done) {
      await Driver.create().delayMs(2000)
      var subscriber
      await commonEvent.createSubscriber(ACTS_CallFunction).then(async (data) => {
        console.info(`====>Promise_0300 createSubscriber  ${data}`)
        subscriber = data
        commonEvent.subscribe(data, subscribeCallBack)
        globalThis.context.startAbility({
          bundleName: 'com.example.mainhap',
          abilityName: 'EntryAbility',
          action: 'Promise_0300'
        },(err) => {
          console.info('====>start Own Ability err:' + JSON.stringify(err));
        })
        setTimeout(() => {
          globalThis.context.startAbility({
            bundleName: 'com.example.forresulttestsecond',
            abilityName: 'EntryAbility',
            action: 'Promise_0300'
          },(err) => {
            console.info('====>start Another Ability err:' + JSON.stringify(err));
          })
        }, 2000);
      })

      function subscribeCallBack(err, data) {
        if(data.event == 'ACTS_CALL_EVENT') {
          console.info(`====>Promise_0300 subscribeCallBack 'ACTS_CALL_EVENT'  ${data}`)
          expect(data.parameters.result).assertEqual(-1);
        }
        if(data.event == 'ACTS_CALL_EVENT_ANOTHER') {
          console.info(`====>Promise_0300 subscribeCallBack 'ACTS_CALL_EVENT_ANOTHER'  ${data}`)
          expect(data.parameters.str).assertEqual('ACTION');
          expect(data.parameters.result).assertEqual(0);
        }
        globalThis.terminate('Promise_0300')
        abilityDelegator.executeShellCommand('aa force-stop com.example.forresulttestsecond').then(result =>{
          console.info('====>Promise_0300 unSubscribeCallback pkill ok' + JSON.stringify(result));
          commonEvent.unsubscribe(subscriber, unSubscribeCallback)
        }).catch(err => {
          console.info('====>Promise_0300 unSubscribeCallback pkill ng' + JSON.stringify(err));
          commonEvent.unsubscribe(subscriber, unSubscribeCallback)
        })
      }

      function unSubscribeCallback() {
        console.info(`====>Promise_0300 unSubscribeCallback 'ACTS_CALL_EVENT'`)
        done()
      }
    })

    it('ACTS_UIExtensionAbility_StartAbilityForResult_Promise_0400',0, async function (done) {
      await Driver.create().delayMs(2000)
      var subscriber
      commonEvent.createSubscriber(ACTS_CallFunction).then(async (data) => {
        console.info(`====>Promise_0400 createSubscriber  ${data}`)
        subscriber = data
        commonEvent.subscribe(data, subscribeCallBack)
        globalThis.context.startAbility({
          bundleName: 'com.example.mainhap',
          abilityName: 'EntryAbility',
          action: 'Promise_0400'
        },(err) => {
          console.info('====>start Own Ability err:' + JSON.stringify(err));
        })

        setTimeout(() => {
          abilityDelegator.executeShellCommand('aa force-stop com.example.forresulttestsecond').then(result =>{
            console.info('====>Promise_0400 pkill ok' + JSON.stringify(result));
          }).catch(err => {
            console.info('====>Promise_0400 pkill ng' + JSON.stringify(err));
          })
        }, 2000)
      })

      function subscribeCallBack(err, data) {
        console.info(`====>Promise_0400 subscribeCallBack all  ${data.event}`)
        if(data.event == 'ACTS_CALL_EVENT') {
          console.info(`====>Promise_0400 subscribeCallBack 'ACTS_CALL_EVENT'  ${data}`)
          expect(data.parameters.result).assertEqual(-1);
          commonEvent.unsubscribe(subscriber, unSubscribeCallback)
        }
      }

      function unSubscribeCallback() {
        console.info(`====>Promise_0400 unSubscribeCallback 'ACTS_CALL_EVENT'`)
        setTimeout(() => {
          globalThis.terminate('Promise_0400')
          done()
        }, 1000)
      }
    })

    it('ACTS_UIExtensionAbility_StartAbilityForResult_Promise_0500',0, async function (done) {
      await Driver.create().delayMs(2000)
      var subscriber
      commonEvent.createSubscriber(ACTS_CallFunction).then(async (data) => {
        console.info(`====>Promise_0500 createSubscriber  ${data}`)
        subscriber = data
        commonEvent.subscribe(data, subscribeCallBack)
        globalThis.context.startAbility({
          bundleName: 'com.example.mainhap',
          abilityName: 'EntryAbility',
          action: 'Promise_0500'
        },(err) => {
          console.info('====>start Own Ability err:' + JSON.stringify(err));
        })
      })

      function subscribeCallBack(err, data) {
        console.info(`====>Promise_0500 subscribeCallBack all  ${data.event}`)
        if(data.event == 'ACTS_CALL_EVENT') {
          console.info(`====>Promise_0500 subscribeCallBack 'ACTS_CALL_EVENT'  ${data}`)
          expect(data.parameters.num).assertEqual(16000001);
          commonEvent.unsubscribe(subscriber, unSubscribeCallback)
        }
      }

      function unSubscribeCallback() {
        console.info(`====>Promise_0500 unSubscribeCallback 'ACTS_CALL_EVENT'`)
        globalThis.terminate('Promise_0500')
        setTimeout(() => {
          done()
        }, 1000)
      }
    })

    it('ACTS_UIExtensionAbility_StartAbilityForResult_Promise_0600',0, async function (done) {
      await Driver.create().delayMs(2000)
      var subscriber
      commonEvent.createSubscriber(ACTS_CallFunction).then(async (data) => {
        console.info(`====>Promise_0600 createSubscriber  ${data}`)
        subscriber = data
        commonEvent.subscribe(data, subscribeCallBack)
        globalThis.context.startAbility({
          bundleName: 'com.example.forresulttestthrid',
          abilityName: 'EntryAbility',
          action: 'Promise_0600'
        },(err) => {
          console.info('====>start Own Ability err:' + JSON.stringify(err));
        })
      })

      function subscribeCallBack(err, data) {
        console.info(`====>Promise_0600 subscribeCallBack all  ${data.event}`)
        if(data.event == 'ACTS_CALL_EVENT') {
          console.info(`====>Promise_0600 subscribeCallBack 'ACTS_CALL_EVENT'  ${data}`)
          console.info(`====>Promise_0600 subscribeCallBack  ${data.parameters.num}`)
          expect(data.parameters.num).assertEqual(16000004);
          commonEvent.unsubscribe(subscriber, unSubscribeCallback)
        }
      }

      function unSubscribeCallback() {
        console.info(`====>Promise_0600 unSubscribeCallback 'ACTS_CALL_EVENT'`)
        setTimeout(() => {
          abilityDelegator.executeShellCommand('aa force-stop com.example.forresulttestthrid').then(result =>{
            console.info('====>AsyncCallback_0600 unSubscribeCallback pkill ok' + JSON.stringify(result));
            done()
          }).catch(err => {
            console.info('====>AsyncCallback_0600 unSubscribeCallback pkill ng' + JSON.stringify(err));
            done()
          })
        }, 2000)
      }
    })

    it('ACTS_UIExtensionAbility_StartAbilityForResult_Promise_0700',0, async function (done) {
      await Driver.create().delayMs(2000)
      var subscriber
      commonEvent.createSubscriber(ACTS_CallFunction).then(async (data) => {
        console.info(`====>Promise_0700 createSubscriber  ${data}`)
        subscriber = data
        commonEvent.subscribe(data, subscribeCallBack)
        globalThis.context.startAbility({
          bundleName: 'com.example.forresulttestthrid',
          abilityName: 'EntryAbility',
          action: 'Promise_0700'
        },(err) => {
          console.info('====>start Own Ability err:' + JSON.stringify(err));
        })
      })

      function subscribeCallBack(err, data) {
        console.info(`====>Promise_0700 subscribeCallBack all  ${data.event}`)
        if(data.event == 'ACTS_CALL_EVENT') {
          console.info(`====>Promise_0700 subscribeCallBack 'ACTS_CALL_EVENT'  ${data}`)
          expect(data.parameters.num).assertEqual(201);
          commonEvent.unsubscribe(subscriber, unSubscribeCallback)
        }
      }

      function unSubscribeCallback() {
        console.info(`====>Promise_0700 unSubscribeCallback 'ACTS_CALL_EVENT'`)
        setTimeout(() => {
          abilityDelegator.executeShellCommand('aa force-stop com.example.forresulttestthrid').then(result =>{
            console.info('====>AsyncCallback_0700 unSubscribeCallback pkill ok' + JSON.stringify(result));
            done()
          }).catch(err => {
            console.info('====>AsyncCallback_0700 unSubscribeCallback pkill ng' + JSON.stringify(err));
            done()
          })
        }, 2000)
      }
    })
  })
}