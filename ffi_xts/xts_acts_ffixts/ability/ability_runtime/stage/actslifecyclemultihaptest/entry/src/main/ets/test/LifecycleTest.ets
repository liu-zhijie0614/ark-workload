/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { describe, beforeAll, beforeEach, afterEach, afterAll, it, expect } from "@ohos/hypium"
export default function abilityTest() {

  describe('AbilityTest', function () {

    console.info("-----------------LifecycleTest is start----------------")
    beforeEach(async function (done) {
      console.info('LifeCycleTest before each called');
      await sleep(1000);
      done()
    });

    afterEach(async function (done) {
      console.info('LifeCycleTest after each called');
      if ("Multihap_LifecycleTest_007" != TAG || "Multihap_LifecycleTest_011" != TAG
        || "Multihap_LifecycleTest_006" != TAG) {
        let para = {
          resultCode: 2,
          want: {
            "abilityName": "MainAbility4",
            bundleName: "com.example.lifecycletest"
          }
        }
        console.log("LifeCycleTest terminateSelfwithresult para: " + JSON.stringify(para));
        await globalThis.ability4context.terminateSelfWithResult(para)
          .then((data) => {
            console.log("LifeCycleTest terminateSelfwithresult successful data: " + JSON.stringify(data));
          }).catch((error) => {
            console.log("LifeCycleTest terminateSelfwithresult error: " + JSON.stringify(error));
          });
      }
      done();
    });

    let TAG;

    function sleep(time) {
      return new Promise((resolve) => setTimeout(resolve, time))
    }

    /*
     * @tc.number  Multihap_LifecycleTest_001
     * @tc.name    The ability of HAP A listens to the ability lifecycle callback of HAP B
     * @tc.desc    Function test
     * @tc.level   0
     */
    it("Multihap_LifecycleTest_001", 0, async (done) => {
      console.info("---------------Multihap_LifecycleTest_001 is start---------------")
      TAG = "Multihap_LifecycleTest_001";

      globalThis.abilityContext.startAbility({
        bundleName: "com.example.lifecycletest",
        abilityName: "MainAbility4"
      }, (error, data) => {
        console.log(TAG + ": MainAbility4 startAbility success, err: " + JSON.stringify(error) +
          ",data: " + JSON.stringify(data));
      });

      await sleep(1000);

      globalThis.abilityContext.startAbility({
        bundleName: "com.example.lifecycletest",
        abilityName: "Hap2MainAbility3"
      }, (error, data) => {
        setTimeout(() => {
          console.log(TAG + ": Hap2MainAbility3 startAbility success, err: " + JSON.stringify(error) +
            ",data: " + JSON.stringify(data));
          let listTemp = JSON.stringify(globalThis.mainAbility4ListKey);
          console.log(TAG + "listTemp is :" + listTemp);
          console.log(TAG + "globalThis.mainAbility4CallBackId is :" + globalThis.mainAbility4CallBackId);
          expect(listTemp.indexOf("Hap2MainAbility3 onAbilityCreate") !== -1).assertTrue();
          expect(listTemp.indexOf("Hap2MainAbility3 onWindowStageCreate") !== -1).assertTrue();
          expect(listTemp.indexOf("Hap2MainAbility3 onAbilityForeground") !== -1).assertTrue();
          expect(listTemp.indexOf("Hap2MainAbility3 onAbilityBackground") !== -1).assertTrue();
          expect(listTemp.indexOf("Hap2MainAbility3 onWindowStageDestroy") !== -1).assertTrue();
          expect(listTemp.indexOf("Hap2MainAbility3 onAbilityDestroy") !== -1).assertTrue();
          console.info(TAG + "globalThis.ApplicationContext4 is :" + JSON.stringify(globalThis.ApplicationContext4));
          globalThis.ApplicationContext4
            .unregisterAbilityLifecycleCallback(globalThis.mainAbility4CallBackId, (error, data) => {
              console.log(TAG + ": unregisterAbilityLifecycleCallback success, err: " + JSON.stringify(error) +
                ",data: " + JSON.stringify(data));
              expect(error.code).assertEqual(0);
              done();
            });
        }, 4000)
      });
      console.info("---------------Multihap_LifecycleTest_001 is end---------------")
    });

    /*
     * @tc.number  Multihap_LifecycleTest_002
     * @tc.name    The ability of HAP A listens to the ability lifecycle callback of HAP B (single instance)
     * @tc.desc    Function test
     * @tc.level   0
     */
    it("Multihap_LifecycleTest_002", 0, async function (done) {
      console.log("------------Multihap_LifecycleTest_002 start-------------");
      TAG = "Multihap_LifecycleTest_002";

      globalThis.abilityContext.startAbility({
        bundleName: "com.example.lifecycletest",
        abilityName: "MainAbility4"
      }, (error, data) => {
        console.log(TAG + ": MainAbility4 startAbility success, err: " + JSON.stringify(error) +
          ",data: " + JSON.stringify(data));
      });

      await sleep(1000);

      globalThis.abilityContext.startAbility({
        bundleName: "com.example.lifecycletest",
        abilityName: "Hap2MainAbility4"
      }, (error, data) => {
        console.log(TAG + ":first Hap2MainAbility4 startAbility success, err: " + JSON.stringify(error) +
          ",data: " + JSON.stringify(data));
      });

      await sleep(1000);

      globalThis.abilityContext.startAbility({
        bundleName: "com.example.lifecycletest",
        abilityName: "Hap2MainAbility4"
      }, (error, data) => {
        setTimeout(() => {
          console.log(TAG + ": second Hap2MainAbility4 startAbility success, err: " + JSON.stringify(error) +
            ",data: " + JSON.stringify(data));
          let listTemp = JSON.stringify(globalThis.mainAbility4ListKey);
          console.log(TAG + "listTemp is :" + listTemp);
          console.log(TAG + "globalThis.mainAbility4CallBackId is :" + globalThis.mainAbility4CallBackId);
          expect(listTemp.indexOf("Hap2MainAbility4 onAbilityCreate") !== -1).assertTrue();
          expect(listTemp.indexOf("Hap2MainAbility4 onWindowStageCreate") !== -1).assertTrue();
          expect(listTemp.indexOf("Hap2MainAbility4 onAbilityForeground") !== -1).assertTrue();
          expect(listTemp.indexOf("Hap2MainAbility4 onAbilityBackground") !== -1).assertTrue();
          expect(listTemp.indexOf("Hap2MainAbility4 onWindowStageDestroy") !== -1).assertTrue();
          expect(listTemp.indexOf("Hap2MainAbility4 onAbilityDestroy") !== -1).assertTrue();
          globalThis.ApplicationContext4
            .unregisterAbilityLifecycleCallback(globalThis.mainAbility4CallBackId, (error, data) => {
              console.log(TAG + ": unregisterAbilityLifecycleCallback success, err: " + JSON.stringify(error) +
                ",data: " + JSON.stringify(data));
              expect(error.code).assertEqual(0);
              done();
            });
        }, 4000)
      });
      console.log("------------Multihap_LifecycleTest_002 end-------------");
    });

    /*
     * @tc.number  Multihap_LifecycleTest_003
     * @tc.name    The ability of HAP A listens to the ability lifecycle callback of HAP B (multiple cases)
     * @tc.desc    Function test
     * @tc.level   0
     */
    it("Multihap_LifecycleTest_003", 0, async function (done) {
      console.log("------------Multihap_LifecycleTest_003 start-------------");
      TAG = "Multihap_LifecycleTest_003";

      globalThis.abilityContext.startAbility({
        bundleName: "com.example.lifecycletest",
        abilityName: "MainAbility4"
      }, (error, data) => {
        console.log(TAG + ": MainAbility4 startAbility success, err: " + JSON.stringify(error) +
          ",data: " + JSON.stringify(data));
      });

      await sleep(1000);

      globalThis.abilityContext.startAbility({
        bundleName: "com.example.lifecycletest",
        abilityName: "Hap2MainAbility5"
      }, (error, data) => {
        console.log(TAG + ":first Hap2MainAbility5 startAbility success, err: " + JSON.stringify(error) +
          ",data: " + JSON.stringify(data));
      });

      await sleep(1000);

      globalThis.abilityContext.startAbility({
        bundleName: "com.example.lifecycletest",
        abilityName: "Hap2MainAbility5"
      }, (error, data) => {
        setTimeout(() => {
          console.log(TAG + ": second Hap2MainAbility5 startAbility success, err: " + JSON.stringify(error) +
            ",data: " + JSON.stringify(data));
          let listTemp = JSON.stringify(globalThis.mainAbility4ListKey);
          console.log(TAG + "listTemp is :" + listTemp);
          console.log(TAG + "globalThis.mainAbility4CallBackId is :" + globalThis.mainAbility4CallBackId);
          expect(listTemp.split("Hap2MainAbility5 onAbilityCreate").length - 1).assertEqual(2);
          expect(listTemp.split("Hap2MainAbility5 onWindowStageCreate").length - 1).assertEqual(2);
          expect(listTemp.split("Hap2MainAbility5 onAbilityForeground").length - 1).assertEqual(2);
          expect(listTemp.split("Hap2MainAbility5 onAbilityBackground").length - 1).assertEqual(2);
          expect(listTemp.split("Hap2MainAbility5 onWindowStageDestroy").length - 1).assertEqual(2);
          expect(listTemp.split("Hap2MainAbility5 onAbilityDestroy").length - 1).assertEqual(2);
          globalThis.ApplicationContext4
            .unregisterAbilityLifecycleCallback(globalThis.mainAbility4CallBackId, (error, data) => {
              console.log(TAG + ": unregisterAbilityLifecycleCallback success, err: " + JSON.stringify(error) +
                ",data: " + JSON.stringify(data));
              expect(error.code).assertEqual(0);
              done();
            });
        }, 4000)
      });
      console.log("------------Multihap_LifecycleTest_003 end-------------");
    });

    /*
     * @tc.number  Multihap_LifecycleTest_004
     * @tc.name    The ability of HAP A monitors the ability life cycle callback of HAP B and HAP C
     * @tc.desc    Function test
     * @tc.level   0
     */
    it("Multihap_LifecycleTest_004", 0, async function (done) {
      console.log("------------Multihap_LifecycleTest_004 start-------------");
      TAG = "Multihap_LifecycleTest_004";

      globalThis.abilityContext.startAbility({
        bundleName: "com.example.lifecycletest",
        abilityName: "MainAbility4"
      }, (error, data) => {
        console.log(TAG + ": MainAbility4 startAbility success, err: " + JSON.stringify(error) +
          ",data: " + JSON.stringify(data));
      });

      await sleep(1000);

      globalThis.abilityContext.startAbility({
        bundleName: "com.example.lifecycletest",
        abilityName: "Hap2MainAbility6"
      }, (error, data) => {
        console.log(TAG + ":first Hap2MainAbility6 startAbility success, err: " + JSON.stringify(error) +
          ",data: " + JSON.stringify(data));
      });

      await sleep(1000);

      globalThis.abilityContext.startAbility({
        bundleName: "com.example.lifecycletest",
        abilityName: "Hap3MainAbility2"
      }, (error, data) => {
        setTimeout(() => {
          console.log(TAG + ": second Hap3MainAbility2 startAbility success, err: " + JSON.stringify(error) +
            ",data: " + JSON.stringify(data));

          let listTemp = JSON.stringify(globalThis.mainAbility4ListKey);
          console.log(TAG + "listTemp is :" + listTemp);
          console.log(TAG + "globalThis.mainAbility4CallBackId is :" + globalThis.mainAbility4CallBackId);
          expect(listTemp.indexOf("Hap2MainAbility6 onAbilityCreate") !== -1).assertTrue();
          expect(listTemp.indexOf("Hap2MainAbility6 onWindowStageCreate") !== -1).assertTrue();
          expect(listTemp.indexOf("Hap2MainAbility6 onAbilityForeground") !== -1).assertTrue();
          expect(listTemp.indexOf("Hap2MainAbility6 onAbilityBackground") !== -1).assertTrue();
          expect(listTemp.indexOf("Hap2MainAbility6 onWindowStageDestroy") !== -1).assertTrue();
          expect(listTemp.indexOf("Hap2MainAbility6 onAbilityDestroy") !== -1).assertTrue();
          expect(listTemp.indexOf("Hap3MainAbility2 onAbilityCreate") !== -1).assertTrue();
          expect(listTemp.indexOf("Hap3MainAbility2 onWindowStageCreate") !== -1).assertTrue();
          expect(listTemp.indexOf("Hap3MainAbility2 onAbilityForeground") !== -1).assertTrue();
          expect(listTemp.indexOf("Hap3MainAbility2 onAbilityBackground") !== -1).assertTrue();
          expect(listTemp.indexOf("Hap3MainAbility2 onWindowStageDestroy") !== -1).assertTrue();
          expect(listTemp.indexOf("Hap3MainAbility2 onAbilityDestroy") !== -1).assertTrue();

          globalThis.ApplicationContext4
            .unregisterAbilityLifecycleCallback(globalThis.mainAbility4CallBackId, (error, data) => {
              console.log(TAG + ": unregisterAbilityLifecycleCallback success, err: " + JSON.stringify(error) +
                ",data: " + JSON.stringify(data));
              expect(error.code).assertEqual(0);
              done();
            });
        }, 4000)
      });
      console.log("------------Multihap_LifecycleTest_004 end-------------");
    });

    /*
     * @tc.number  Multihap_LifecycleTest_005
     * @tc.name    The ability of HAP A listens to two ability life cycle callbacks in HAP B
     * @tc.desc    Function test
     * @tc.level   0
     */
    it("Multihap_LifecycleTest_005", 0, async function (done) {
      console.log("------------Multihap_LifecycleTest_005 start-------------");
      TAG = "Multihap_LifecycleTest_005";

      globalThis.abilityContext.startAbility({
        bundleName: "com.example.lifecycletest",
        abilityName: "MainAbility4"
      }, (error, data) => {
        console.log(TAG + ": MainAbility4 startAbility success, err: " + JSON.stringify(error) +
          ",data: " + JSON.stringify(data));
      });

      await sleep(500);

      globalThis.abilityContext.startAbility({
        bundleName: "com.example.lifecycletest",
        abilityName: "Hap2MainAbility6"
      }, (error, data) => {
        console.log(TAG + ":first Hap2MainAbility6 startAbility success, err: " + JSON.stringify(error) +
          ",data: " + JSON.stringify(data));
      });

      await sleep(500);

      globalThis.abilityContext.startAbility({
        bundleName: "com.example.lifecycletest",
        abilityName: "Hap2MainAbility7"
      }, (error, data) => {
        setTimeout(() => {
          console.log(TAG + ": second Hap2MainAbility7 startAbility success, err: " + JSON.stringify(error) +
            ",data: " + JSON.stringify(data));
          let listTemp = JSON.stringify(globalThis.mainAbility4ListKey);
          console.log(`${TAG} listTemp is: ${listTemp}`)
          console.log(TAG + "globalThis.mainAbility4CallBackId is :" + globalThis.mainAbility4CallBackId);
          expect(listTemp.indexOf("Hap2MainAbility6 onAbilityCreate") !== -1).assertTrue();
          expect(listTemp.indexOf("Hap2MainAbility6 onWindowStageCreate") !== -1).assertTrue();
          expect(listTemp.indexOf("Hap2MainAbility6 onAbilityForeground") !== -1).assertTrue();
          expect(listTemp.indexOf("Hap2MainAbility6 onAbilityBackground") !== -1).assertTrue();
          expect(listTemp.indexOf("Hap2MainAbility6 onWindowStageDestroy") !== -1).assertTrue();
          expect(listTemp.indexOf("Hap2MainAbility6 onAbilityDestroy") !== -1).assertTrue();
          expect(listTemp.indexOf("Hap2MainAbility7 onAbilityCreate") !== -1).assertTrue();
          expect(listTemp.indexOf("Hap2MainAbility7 onWindowStageCreate") !== -1).assertTrue();
          expect(listTemp.indexOf("Hap2MainAbility7 onAbilityForeground") !== -1).assertTrue();
          expect(listTemp.indexOf("Hap2MainAbility7 onAbilityBackground") !== -1).assertTrue();
          expect(listTemp.indexOf("Hap2MainAbility7 onWindowStageDestroy") !== -1).assertTrue();
          expect(listTemp.indexOf("Hap2MainAbility7 onAbilityDestroy") !== -1).assertTrue();

          globalThis.ApplicationContext4
            .unregisterAbilityLifecycleCallback(globalThis.mainAbility4CallBackId, (error, data) => {
              console.log(TAG + ": unregisterAbilityLifecycleCallback success, err: " + JSON.stringify(error) +
                ",data: " + JSON.stringify(data));
              expect(error.code).assertEqual(0);
              done();
            });
        }, 3000)
      });
      console.log("------------end Multihap_LifecycleTest_005-------------");
    });

    /*
     * @tc.number  Multihap_LifecycleTest_006
     * @tc.name    Repeat the registration. The ability of HAP A listens to the life
                    cycle callback of the ability of HAP B
     * @tc.desc    Function test
     * @tc.level   0
     */
    it("Multihap_LifecycleTest_006", 0, async function (done) {
      console.log("------------Multihap_LifecycleTest_006 start-------------");
      TAG = "Multihap_LifecycleTest_006";
      let firstCallbackId;
      let secondCallbackId;

      globalThis.abilityContext.startAbility({
        bundleName: "com.example.lifecycletest",
        abilityName: "MainAbility7"
      }, (error, data) => {
        console.log(TAG + ": first MainAbility7 startAbility success, err: " + JSON.stringify(error) +
          ",data: " + JSON.stringify(data));
      });

      await sleep(700);

      globalThis.abilityContext.startAbility({
        bundleName: "com.example.lifecycletest",
        abilityName: "Hap2MainAbility8"
      }, (error, data) => {
        setTimeout(function () {
          console.log(TAG + ": first Hap2MainAbility8 startAbility success, err: " + JSON.stringify(error) +
            ",data: " + JSON.stringify(data));
          console.log(TAG + "first globalThis.mainAbility7CallBackId is :" + globalThis.mainAbility7CallBackId);
          firstCallbackId = globalThis.mainAbility7CallBackId;
          console.log(TAG + "firstCallbackId is : " + firstCallbackId);
        }, 3500)
      });

      await sleep(1400);

      globalThis.abilityContext.startAbility({
        bundleName: "com.example.lifecycletest",
        abilityName: "MainAbility7"
      }, (error, data) => {
        console.log(TAG + ": second MainAbility7 startAbility success, err: " + JSON.stringify(error) +
          ",data: " + JSON.stringify(data));
      });

      await sleep(700);

      globalThis.abilityContext.startAbility({
        bundleName: "com.example.lifecycletest",
        abilityName: "Hap2MainAbility8"
      }, (error, data) => {
        setTimeout(() => {
          console.log(TAG + ": second Hap2MainAbility8 startAbility success, err: " + JSON.stringify(error) +
            ",data: " + JSON.stringify(data));
          console.log(TAG + "second globalThis.mainAbility7CallBackId is :" + globalThis.mainAbility7CallBackId);
          secondCallbackId = globalThis.mainAbility7CallBackId;
          console.log(TAG + "secondCallbackId is : " + secondCallbackId);
          expect(secondCallbackId).assertEqual(firstCallbackId + 1)
          setTimeout(() => {
            globalThis.ApplicationContext7
              .unregisterAbilityLifecycleCallback(globalThis.mainAbility7CallBackId, (error, data) => {
                console.log(TAG + ": unregisterAbilityLifecycleCallback success, err: " + JSON.stringify(error) +
                  ",data: " + JSON.stringify(data));
                expect(error.code).assertEqual(0);
                done();
              });
          }, 1000)
        }, 4000)
      });

      console.log("------------Multihap_LifecycleTest_006 end-------------");
    });

    /*
     * @tc.number  Multihap_LifecycleTest_007
     * @tc.name    Repeat registration and deregistration. The ability of HAP A listens to the life
                    cycle callback of the ability of HAP B
     * @tc.desc    Function test
     * @tc.level   0
     */
    it("Multihap_LifecycleTest_007", 0, async function (done) {
      console.log("------------Multihap_LifecycleTest_007 start-------------");
      TAG = "Multihap_LifecycleTest_007";
      let callBackId1;
      let callBackId2;
      let flag;

      globalThis.abilityContext.startAbility({
        bundleName: "com.example.lifecycletest",
        abilityName: "MainAbility6"
      }, (error, data) => {
        console.log(TAG + ": first MainAbility6 startAbility success, err: " + JSON.stringify(error) +
          ",data: " + JSON.stringify(data));
      });

      setTimeout(function () {
        globalThis.abilityContext.startAbility({
          bundleName: "com.example.lifecycletest",
          abilityName: "Hap2MainAbility9"
        }, (error, data) => {
          setTimeout(() => {
            console.log(TAG + ":first Hap2MainAbility9 startAbility success, err: " +
              JSON.stringify(error) + ",data: " + JSON.stringify(data));

            let listTemp = JSON.stringify(globalThis.mainAbility6ListKey);
            console.log(TAG + "listTemp is :" + listTemp);
            console.log(TAG + "first globalThis.mainAbility6CallBackId is :" + globalThis.mainAbility6CallBackId);
            callBackId1 = globalThis.mainAbility6CallBackId;
            console.log(TAG + "callBackId1 is :" + callBackId1);
            expect(listTemp.indexOf("Hap2MainAbility9 onAbilityCreate") !== -1).assertTrue();
            expect(listTemp.indexOf("Hap2MainAbility9 onWindowStageCreate") !== -1).assertTrue();
            expect(listTemp.indexOf("Hap2MainAbility9 onAbilityForeground") !== -1).assertTrue();
            expect(listTemp.indexOf("Hap2MainAbility9 onAbilityBackground") !== -1).assertTrue();
            expect(listTemp.indexOf("Hap2MainAbility9 onWindowStageDestroy") !== -1).assertTrue();
            expect(listTemp.indexOf("Hap2MainAbility9 onAbilityDestroy") !== -1).assertTrue();

            globalThis.ApplicationContext6
              .unregisterAbilityLifecycleCallback(globalThis.mainAbility6CallBackId, (error, data) => {
                console.log(TAG + ": first unregisterAbilityLifecycleCallback success, err: " + JSON.stringify(error) +
                  ",data: " + JSON.stringify(data));
                expect(error.code).assertEqual(0);
                flag = error.code;
                console.log(TAG + "flag is:" + flag);
              });
          }, 3000)
        });
      }, 1000)


      setTimeout(function () {
        if (flag == 0) {

          globalThis.abilityContext.startAbility({
            bundleName: "com.example.lifecycletest",
            abilityName: "MainAbility6"
          }, (error, data) => {
            console.log(TAG + ": first MainAbility6 startAbility success, err: " + JSON.stringify(error) +
              ",data: " + JSON.stringify(data));
          });

          setTimeout(function () {
            globalThis.abilityContext.startAbility({
              bundleName: "com.example.lifecycletest",
              abilityName: "Hap2MainAbility9"
            }, (error, data) => {

              setTimeout(() => {
                console.log(TAG + ":second Hap2MainAbility9 startAbility success, err: " +
                  JSON.stringify(error) + ",data: " + JSON.stringify(data));

                let listTemp1 = JSON.stringify(globalThis.mainAbility6ListKey);
                console.log(TAG + "listTemp1 is :" + listTemp1);
                console.log(TAG + "second globalThis.mainAbility6CallBackId is :" + globalThis.mainAbility6CallBackId);
                callBackId2 = globalThis.mainAbility6CallBackId;
                console.log(TAG + "callBackId2 is :" + callBackId2);
                expect(callBackId2).assertEqual(callBackId1 + 1);
                expect(listTemp1.indexOf("Hap2MainAbility9 onAbilityCreate") !== -1).assertTrue();
                expect(listTemp1.indexOf("Hap2MainAbility9 onWindowStageCreate") !== -1).assertTrue();
                expect(listTemp1.indexOf("Hap2MainAbility9 onAbilityForeground") !== -1).assertTrue();
                expect(listTemp1.indexOf("Hap2MainAbility9 onAbilityBackground") !== -1).assertTrue();
                expect(listTemp1.indexOf("Hap2MainAbility9 onWindowStageDestroy") !== -1).assertTrue();
                expect(listTemp1.indexOf("Hap2MainAbility9 onAbilityDestroy") !== -1).assertTrue();

                globalThis.ApplicationContext6
                  .unregisterAbilityLifecycleCallback(globalThis.mainAbility6CallBackId, (error, data) => {
                    console.log(TAG + ": second unregisterAbilityLifecycleCallback success, err: " + JSON.stringify(error) +
                      ",data: " + JSON.stringify(data));
                    expect(error.code).assertEqual(0);
                    done();
                  });
              }, 3000)
            })
          }, 2000)
        }
      }, 10000)

      console.log("------------Multihap_LifecycleTest_007 end-------------");
    });

    /*
     * @tc.number  Multihap_LifecycleTest_008
     * @tc.name    Repeat deregistration. The ability of HAP A listens to the life
                    cycle callback of the ability of HAP B
     * @tc.desc    Function test
     * @tc.level   0
     */
    it("Multihap_LifecycleTest_008", 0, async function (done) {
      console.log("------------Multihap_LifecycleTest_008 start-------------");
      TAG = "Multihap_LifecycleTest_008";

      globalThis.abilityContext.startAbility({
        bundleName: "com.example.lifecycletest",
        abilityName: "MainAbility4"
      }, (error, data) => {
        console.log(TAG + ": MainAbility4 startAbility success, err: " + JSON.stringify(error) +
          ",data: " + JSON.stringify(data));
      });

      await sleep(500);

      globalThis.abilityContext.startAbility({
        bundleName: "com.example.lifecycletest",
        abilityName: "Hap2MainAbility6"
      }, (error, data) => {
        setTimeout(() => {
          console.log(TAG + ": Hap2MainAbility6 startAbility success, err: " +
            JSON.stringify(error) + ",data: " + JSON.stringify(data));

          let listTemp = JSON.stringify(globalThis.mainAbility4ListKey);
          console.log(TAG + "listTemp is :" + listTemp);
          console.log(TAG + "globalThis.mainAbility4CallBackId is :" + globalThis.mainAbility4CallBackId);
          expect(listTemp.indexOf("Hap2MainAbility6 onAbilityCreate") !== -1).assertTrue();
          expect(listTemp.indexOf("Hap2MainAbility6 onWindowStageCreate") !== -1).assertTrue();
          expect(listTemp.indexOf("Hap2MainAbility6 onAbilityForeground") !== -1).assertTrue();
          expect(listTemp.indexOf("Hap2MainAbility6 onAbilityBackground") !== -1).assertTrue();
          expect(listTemp.indexOf("Hap2MainAbility6 onWindowStageDestroy") !== -1).assertTrue();
          expect(listTemp.indexOf("Hap2MainAbility6 onAbilityDestroy") !== -1).assertTrue();

          globalThis.ApplicationContext4
            .unregisterAbilityLifecycleCallback(globalThis.mainAbility4CallBackId, (error, data) => {
              console.log(TAG + ": first unregisterAbilityLifecycleCallback, err: " + JSON.stringify(error) +
                ",data: " + JSON.stringify(data));
              expect(error.code).assertEqual(0);
              globalThis.ApplicationContext4
                .unregisterAbilityLifecycleCallback(globalThis.mainAbility4CallBackId, (error, data) => {
                  console.log(TAG + ": second unregisterAbilityLifecycleCallback, err: " + JSON.stringify(error) +
                    ",data: " + JSON.stringify(data));
                  expect(error.code).assertEqual(1);
                  done()
                });
            });
        }, 4000)
      });
      console.log("------------Multihap_LifecycleTest_008 end-------------");
    });

    /*
     * @tc.number  Multihap_LifecycleTest_010
     * @tc.name    Switch the front and background for many times to monitor the life cycle
     * @tc.desc    Function test
     * @tc.level   0
     */
    it('Multihap_LifecycleTest_010', 0, async function (done) {
      console.info("---------------Multihap_LifecycleTest_010 is start---------------")
      TAG = "Multihap_LifecycleTest_010";

      globalThis.abilityContext.startAbility({
        bundleName: "com.example.lifecycletest",
        abilityName: "MainAbility4",
      }, (error, data) => {
        console.log(TAG + ": MainAbility4 startAbility success, err: " +
          JSON.stringify(error) + ",data: " + JSON.stringify(data));
      })

      await sleep(1000)
      globalThis.abilityContext.startAbility({
        bundleName: "com.example.lifecycletest",
        abilityName: "Hap2MainAbility10",
      }, (error, data) => {
        console.log(TAG + ": first Hap2MainAbility10 startAbility success, err: " +
          JSON.stringify(error) + ",data: " + JSON.stringify(data));
      })

      await sleep(1000)
      globalThis.abilityContext.startAbility({
        bundleName: "com.example.lifecycletest",
        abilityName: "Hap3MainAbility3",
      }, (error, data) => {
        console.log(TAG + ": first Hap3MainAbility3 startAbility success, err: " +
          JSON.stringify(error) + ",data: " + JSON.stringify(data));
      })

      await sleep(1000)
      globalThis.abilityContext.startAbility({
        bundleName: "com.example.lifecycletest",
        abilityName: "Hap2MainAbility10",
      }, (error, data) => {
        console.log(TAG + ": second Hap2MainAbility10 startAbility success, err: " +
          JSON.stringify(error) + ",data: " + JSON.stringify(data));
      })

      await sleep(1000)
      globalThis.abilityContext.startAbility({
        bundleName: "com.example.lifecycletest",
        abilityName: "Hap3MainAbility3",
      }, (error, data) => {
        console.log(TAG + ": second Hap3MainAbility3 startAbility success, err: " +
          JSON.stringify(error) + ",data: " + JSON.stringify(data));
      })


      await sleep(1000)
      globalThis.abilityContext.startAbility({
        bundleName: "com.example.lifecycletest",
        abilityName: "Hap2MainAbility10",
      }, (error, data) => {
        console.log(TAG + ": third Hap2MainAbility10 startAbility success, err: " +
          JSON.stringify(error) + ",data: " + JSON.stringify(data));
      })

      await sleep(1000)
      globalThis.abilityContext.startAbility({
        bundleName: "com.example.lifecycletest",
        abilityName: "Hap3MainAbility3",
      }, (error, data) => {

        setTimeout(() => {
          console.log(TAG + ": third Hap3MainAbility3 startAbility success, err: " +
            JSON.stringify(error) + ",data: " + JSON.stringify(data));

          let listTemp = JSON.stringify(globalThis.mainAbility4ListKey);
          console.log(TAG + " listTemp is :" + listTemp);
          console.log(TAG + " globalThis.mainAbility4CallBackId is :" + globalThis.mainAbility4CallBackId);
          expect(listTemp.indexOf("Hap2MainAbility10 onAbilityCreate") !== -1).assertTrue();
          expect(listTemp.indexOf("Hap2MainAbility10 onWindowStageCreate") !== -1).assertTrue();
          expect(listTemp.split("Hap2MainAbility10 onAbilityForeground").length - 1).assertEqual(3);
          expect(listTemp.split("Hap2MainAbility10 onAbilityBackground").length - 1).assertEqual(3);
          expect(listTemp.indexOf("Hap3MainAbility3 onAbilityCreate") !== -1).assertTrue();
          expect(listTemp.indexOf("Hap3MainAbility3 onWindowStageCreate") !== -1).assertTrue();
          expect(listTemp.split("Hap3MainAbility3 onAbilityForeground").length - 1).assertEqual(3);
          expect(listTemp.split("Hap3MainAbility3 onAbilityBackground").length - 1).assertEqual(2);

          globalThis.ApplicationContext4
            .unregisterAbilityLifecycleCallback(globalThis.mainAbility4CallBackId, (error, data) => {
              console.log(TAG + ": unregisterAbilityLifecycleCallback success, err: " + JSON.stringify(error) +
                ",data: " + JSON.stringify(data));
              expect(error.code).assertEqual(0);
              done();
            });
        }, 4000)
      })
      console.info("---------------Multihap_LifecycleTest_010 is end---------------")
    })

    /*
     * @tc.number  Multihap_LifecycleTest_011
     * @tc.name    The ability of HAP A listens to the life cycle callback
                    of the ability of HAP B in different processes
     * @tc.desc    Function test
     * @tc.level   0
     */
    it('Multihap_LifecycleTest_011', 0, async function (done) {
      console.info("---------------Multihap_LifecycleTest_011 is start---------------")
      TAG = "Multihap_LifecycleTest_011";
      let listKeyTemp = [];

      globalThis.abilityContext.startAbility({
        bundleName: "com.example.lifecycletest",
        abilityName: "MainAbility2",
      }, (error, data) => {
        console.log(TAG + ": Hap1MainAbility2 startAbility success, err: " +
        JSON.stringify(error) + ",data: " + JSON.stringify(data));
      })

      await sleep(500);

      globalThis.abilityContext.startAbility({
        bundleName: "com.example.lifecycletest",
        abilityName: "Hap4MainAbility1",
      }, (error, data) => {
        setTimeout(() => {
          console.log(TAG + ": Hap4MainAbility1 startAbility success, err: " +
          JSON.stringify(error) + ",data: " + JSON.stringify(data));

          for (var i = 0; i < globalThis.mainAbility2ListKey.length; i++) {
            if (globalThis.mainAbility2ListKey[i].substring(0, 16) == "Hap4MainAbility1") {
              listKeyTemp.push(globalThis.mainAbility2ListKey[i]);
            }
          }
          console.log(TAG + " listKeyTemp is :" + listKeyTemp);
          console.log(TAG + " globalThis.mainAbility2CallBackId is :" + globalThis.mainAbility2CallBackId);
          expect(listKeyTemp.length).assertEqual(0);

          globalThis.ApplicationContext2
            .unregisterAbilityLifecycleCallback(globalThis.mainAbility2CallBackId, (error, data) => {
              console.log(TAG + ": unregisterAbilityLifecycleCallback success, err: " + JSON.stringify(error) +
              ",data: " + JSON.stringify(data));
              expect(error.code).assertEqual(0);
              done();
            });
        }, 4000)
      });
      console.info("---------------Multihap_LifecycleTest_011 is end---------------")
    })
  })
}