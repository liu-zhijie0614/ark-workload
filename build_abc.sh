#!/bin/bash

sdk_path=~/workspace/daily/dev/code

toolspath=./toolspath.txt
tools_type="dev"
# ts_path=ts-swift-workload
ts_path=ts-swift-workload
code_ts_lib="BenchmarkMeasure"
target_path=""


# if [ -s "$toolspath" ]; then
#     while IFS= read -r line; do
#         case $line in
#             *--ts-tools-path\ *)
#                 sdk_path="${line#*--ts-tools-path\ }"
#                 ;;
#             *--ark_js_vm\ *)
#                 ark_js_vm="${line#*--ark_js_vm\ }"
#                 ;;
#             *--es2abc\ *)
#                 es2abc="${line#*--es2abc\ }"
#                 ;;
#             *--ark_aot_compiler\ *)
#                 ark_aot_compiler="${line#*--ark_aot_compiler\ }"
#                 ;;
#             *--tools-type\ *)
#                 tools_type="${line#*--tools-type\ }"
#                 ;;
#             *--case-path\ *)
#                 ts_path="${line#*--case-path\ }"
#                 ;;
#         esac
#     done < "toolspath.txt"
# fi

arg_name=""
true="true"
false="false"
run_count=1
aarch64=""
date=$(date +'%Y%m%d%H%M%S')
code_v=1

pgo2path="abc_build/${date}"

function help_explain(){
    echo "[--help,--h]"
    echo "[--run-count <count>]"
    echo "[--case <case-name>]"
    echo "[--date <date>]"
    echo "[--build]"
    echo "[--excel]"
    echo "[--aarch64]"
    echo "[--code-v] <code_path>"
}
if [ -z "$1" ]; then
    echo "No input provided."
else
    for arg in "$@"; do
        case $arg_name in
            "--case")
                arg_name=""
                case_name=$arg
                ;;
            "--date")
                arg_name=""
                date=$arg
                ;;
            "--run-count")
                arg_name=""
                run_count=$arg
                ;;
            "--code-v")
                arg_name=""
                echo $arg
                case $arg in
                    "0")
                    ts_path=daily
                    pgo2path="abc_build-daily/${date}"
                        ;;
                    "1")
                    ts_path=ts-swift-workload
                    pgo2path="abc_build/${date}"
                        ;;
                    "2")
                    ts_path=mix-case
                    pgo2path="abc_build-mix-case/${date}"
                        ;;
                esac
                ;;
        esac
        case $arg in
            "--h")
                help_explain
                exit
                ;;
            "--help")
                help_explain
                exit
                ;;
            "--build")
                build="true"
                ;;
            "--case")
                arg_name=$arg
                ;;
            "--date")
                arg_name=$arg
                ;;
            "--excel")
                excel="true"
                ;;
            "--run-count")
                arg_name=$arg
                ;;
            "--aarch64")
                aarch64="--compiler-target-triple=aarch64-unknown-linux-gnu "
                ;;
            "--code-v")
                arg_name=$arg
                ;;
        esac
    done
fi

chmod 777 -R $ts_path
case $tools_type in
    "dev")
        # export LD_LIBRARY_PATH=${sdk_path}/out/x64.release/arkcompiler/ets_runtime:${sdk_path}/out/x64.release/thirdparty/icu:${sdk_path}/prebuilts/clang/ohos/linux-x86_64/llvm/lib:${sdk_path}/out/x64.release/thirdparty/zlib
        target_path=x64.release
        ;;
    "rk3568")
        # export LD_LIBRARY_PATH=${sdk_path}/out/rk3568/clang_x64/arkcompiler/ets_runtime:${sdk_path}/out/rk3568/clang_x64/thirdparty/icu:${sdk_path}/out/rk3568/clang_x64/thirdparty/zlib:${sdk_path}/out/rk3568/clang_x64/thirdparty/cjson:${sdk_path}/prebuilts/clang/ohos/linux-x86_64/llvm/lib
        target_path=rk3568/clang_x64
        ;;
    "hispark_taurus")
        # export LD_LIBRARY_PATH=${sdk_path}/out/hispark_taurus/clang_x64/arkcompiler/ets_runtime:${sdk_path}/out/hispark_taurus/clang_x64/thirdparty/icu:${sdk_path}/out/hispark_taurus/clang_x64/thirdparty/zlib:${sdk_path}/prebuilts/clang/ohos/linux-x86_64/llvm/lib
        target_path=hispark_taurus/clang_x64
        ;;
esac

# user_lib=${sdk_path}/out/${target_path}/lib.unstripped
user_lib=${sdk_path}/out/${target_path}
# user_exe=${sdk_path}/out/${target_path}/exe.unstripped
user_exe=${sdk_path}/out/${target_path}
export LD_LIBRARY_PATH=${user_lib}/arkcompiler/ets_runtime:${user_lib}/thirdparty/icu:${user_lib}/thirdparty/zlib:${user_lib}/thirdparty/cjson:${sdk_path}/prebuilts/clang/ohos/linux-x86_64/llvm/lib        

es2abc=${user_exe}/arkcompiler/ets_frontend/es2abc

$es2abc $sdk_path/arkcompiler/ets_runtime/ecmascript/ts_types/lib_ark_builtins.d.ts --type-extractor --type-dts-builtin --module --merge-abc --extension=ts --output $sdk_path/arkcompiler/ets_runtime/ecmascript/ts_types/lib_ark_builtins.d.abc
function build_pgo(){
    local ts_file=$1
    local file_name=$2
    local abc_file=${pgo2path}/${file_name}.abc
    echo "build : ${ts_file} to ${pgo2path}"
    ${es2abc} ${ts_file} --type-extractor --module --merge-abc --extension=ts --output ${pgo2path}/${file_name}.abc
}

function run_pgo(){
    local file_name=$1
    local abc_file=${pgo2path}/${file_name}.abc
    echo run ${file_name}
    ${ark_js_vm} --entry-point=${file_name} --aot-file=./${pgo2path}/${file_name} ${abc_file}
}

function run(){
    if [[ "$build" = "true" ]]; then
        if [ ! -d $pgo2path ];then
            mkdir -p $pgo2path
        fi
        if [ -z "$case_name" ]; then
            txt_files=($(find $ts_path -type f -name "*.ts"))
            for file in "${txt_files[@]}"; do
                echo "Content of $file:"
                local ts_file_path=$file
                local file_name=$(basename "${file}" | cut -f1 -d'.')
                build_pgo ${ts_file_path} ${file_name}
                if [[ "$excel" = "true" ]]; then
                    python pgo2excel.py --case-dir $pgo2path --run-case $file_name --run-count $run_count --ark_js_vm $ark_js_vm
                fi
                done
        else
            local ts_file_path=$(find ${ts_path} -name "${case_name}.ts")
            local file_name=$(basename "$ts_file_path" | cut -f1 -d'.')
            if [ -f "${ts_file_path}" ]; then
                build_pgo ${ts_file_path} ${file_name}
                if [[ "$excel" = "true" ]]; then
                    python pgo2excel.py --case-dir $pgo2path --run-case $file_name --run-count $run_count --ark_js_vm $ark_js_vm
                fi
            fi
        fi
    fi

    if [ ! -d $pgo2path ];then
        echo "${pgo2path} No such file or directory"
        exit
    fi
}
run