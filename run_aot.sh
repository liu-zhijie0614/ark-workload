#!/bin/sh

declare -A testSets
testSets["arrayAccess"]="./ts-swift-workload/array-access/ts/"
testSets["fannkuc-redux"]="./ts-swift-workload/array-access/ts/"
testSets["fastarepeat"]="./ts-swift-workload/array-access/ts/"
testSets["fastarandom1"]="./ts-swift-workload/array-access/ts/"
testSets["fastarandom2"]="./ts-swift-workload/array-access/ts/"
testSets["mandelbrot"]="./ts-swift-workload/array-access/ts/"
testSets["spectralnorm"]="./ts-swift-workload/array-access/ts/"
testSets["threeDCube"]="./ts-swift-workload/array-access/ts/"
testSets["threeDRaytrace"]="./ts-swift-workload/array-access/ts/"
# testSets["Deep_Nested_Call"]="./ts-swift-workload/function-call/ts/"
testSets["Method_Call"]="./ts-swift-workload/function-call/ts/"
testSets["Normal_Call"]="./ts-swift-workload/function-call/ts/"
testSets["Pointer_Call"]="./ts-swift-workload/function-call/ts/"
testSets["Pointer_Paraless_Call"]="./ts-swift-workload/function-call/ts/"
# testSets["Shallow_Nested_Call"]="./ts-swift-workload/function-call/ts/"
testSets["nbody"]="./ts-swift-workload/nbody/ts/"
testSets["cocos"]="./ts-swift-workload/cocos/ts/"
testSets["cocos_airplane"]="./ts-swift-workload/cocos/ts/"
testSets["bitops"]="./ts-swift-workload/numerical-calculation/ts/"
testSets["cordic"]="./ts-swift-workload/numerical-calculation/ts/"
testSets["numericalComputation"]="./ts-swift-workload/numerical-calculation/ts/"
testSets["partialSum"]="./ts-swift-workload/numerical-calculation/ts/"
# testSets["propertyaccess1"]="./ts-swift-workload/property-access/ts/"
testSets["propertyaccess2"]="./ts-swift-workload/property-access/ts/"
testSets["splay"]="./ts-swift-workload/splay/ts/"

read -p "is_standalone? true or false: " choice
echo $choice

True="true"
False="false"
if [[ "$choice" == "false" ]] ; then
	read -p "Choose your target from [rk3568, hispark_taurus_standard]: " target
	if [[ "$target" == "rk3568" || "$target" == "hispark_taurus_standard" ]]; then

        ../../out/${target}/clang_x64/arkcompiler/ets_frontend/es2abc ../../arkcompiler/ets_runtime/ecmascript/ts_types/lib_ark_builtins.d.ts --type-extractor --type-dts-builtin --module --merge-abc --extension=ts --output ../../arkcompiler/ets_runtime/ecmascript/ts_types/lib_ark_builtins.d.abc

        ../../out/${target}/clang_x64/arkcompiler/ets_frontend/es2abc ./utils/assert.ts --merge-abc --module --output ./utils/assert.abc
        ../../out/${target}/clang_x64/arkcompiler/ets_frontend/es2abc ./utils/benchmarkTsSuite.ts --merge-abc --module --output ./utils/benchmarkTsSuite.abc

        export LD_LIBRARY_PATH=../../out/${target}/clang_x64/lib.unstripped/clang_x64/arkcompiler/ets_runtime:../../out/${target}/clang_x64/lib.unstripped/clang_x64/test/test:../../out/${target}/clang_x64/lib.unstripped/clang_x64/thirdparty/icu:prebuilts/clang/ohos/linux-x86_64/llvm/lib:../../out/${target}/clang_x64/lib.unstripped/clang_x64/thirdparty/zlib

        for key in ${!testSets[@]};do
            ts_file=${testSets[${key}]}${key}.ts
            file_name=${key}
            abc_file=${testSets[${key}]}${key}.abc
            echo ${ts_file}
            echo ${file_name}
            echo ${abc_file}

            ../../out/${target}/clang_x64/arkcompiler/ets_frontend/es2abc ${ts_file} --type-extractor --type-dts-builtin --module --merge-abc --extension=ts --output ${abc_file}

            ../../out/${target}/clang_x64/exe.unstripped/clang_x64/arkcompiler/ets_runtime/ark_aot_compiler --builtins-dts=../../arkcompiler/ets_runtime/ecmascript/ts_types/lib_ark_builtins.d.abc --aot-file=${file_name} ${abc_file}

            ../../out/${target}/clang_x64/exe.unstripped/clang_x64/arkcompiler/ets_runtime/ark_js_vm --icu-data-path "../../third_party/icu/ohos_icu4j/data" --log-level=info --asm-interpreter=true --entry-point=${file_name} --aot-file=${file_name} ${abc_file}

        done

	else
		echo "Invalid target, please choose in [rk3568, hispark_taurus_standard]"
	fi
elif [[ "$choice" == "true" ]] ; then

    ../../out/x64.release/arkcompiler/ets_frontend/es2abc ../../arkcompiler/ets_runtime/ecmascript/ts_types/lib_ark_builtins.d.ts --type-extractor --type-dts-builtin --module --merge-abc --extension=ts --output ../../arkcompiler/ets_runtime/ecmascript/ts_types/lib_ark_builtins.d.abc

    ../../out/x64.release/arkcompiler/ets_frontend/es2abc ./utils/assert.ts --merge-abc --module --output ./utils/assert.abc
    ../../out/x64.release/arkcompiler/ets_frontend/es2abc ./utils/benchmarkTsSuite.ts --merge-abc --module --output ./utils/benchmarkTsSuite.abc

    export LD_LIBRARY_PATH=../../out/x64.release/lib.unstripped/arkcompiler/ets_runtime:../../out/x64.release/lib.unstripped/test/test:../../out/x64.release/lib.unstripped/thirdparty/icu:prebuilts/clang/ohos/linux-x86_64/llvm/lib:../../out/x64.release/lib.unstripped/thirdparty/zlib

    for key in ${!testSets[@]};do
        ts_file=${testSets[${key}]}${key}.ts
        file_name=${key}
        abc_file=${testSets[${key}]}${key}.abc
        echo ${ts_file}
        echo ${file_name}
        echo ${abc_file}

        ../../out/x64.release/arkcompiler/ets_frontend/es2abc ${ts_file} --type-extractor --type-dts-builtin --module --merge-abc --extension=ts --output ${abc_file}

        ../../out/x64.release/arkcompiler/ets_runtime/ark_aot_compiler --builtins-dts=../../arkcompiler/ets_runtime/ecmascript/ts_types/lib_ark_builtins.d.abc --aot-file=${file_name} ${abc_file}

        ../../out/x64.release/arkcompiler/ets_runtime/ark_js_vm --icu-data-path "../../third_party/icu/ohos_icu4j/data" --log-level=info --asm-interpreter=true --entry-point=${file_name} --aot-file=${file_name} ${abc_file}

    done

else
	echo "Invalid input, please enter true or false"
fi

