#include "napi/native_api.h"
#include <ctime>
#include <locale.h>
#include <sys/time.h>
#include <hilog/log.h>
#include <string>

static constexpr int Num_Count = 10000;
static constexpr int Time_Unit = 1000000;
time_t timefor = 0;
#define TestTime(name)\
{\
    t1 = (time1.tv_sec * Time_Unit) + (time1.tv_usec);\
    t2 = (time2.tv_sec * Time_Unit) + (time2.tv_usec);\
    t = t2 - t1;\
    OH_LOG_INFO(LOG_APP, "lq-%{public}s, %{public}d", #name, int(t - timefor));\
}

static napi_value GetValue(napi_env env, napi_callback_info info)
{
    size_t requireArgc = 5;
    size_t argc = 5;
    napi_value args[5] = {nullptr};
    napi_get_cb_info(env, info, &argc, args , nullptr, nullptr);
    napi_valuetype valuetype0;
    struct timeval time1;
    struct timeval time2;
    time_t t1;
    time_t t2;
    time_t t;
    gettimeofday(&time1, NULL);
    for(int i = 0; i < Num_Count; i++) {}
    gettimeofday(&time2, NULL);
    time_t start = (time1.tv_sec * Time_Unit) + (time1.tv_usec);
    time_t end = (time2.tv_sec * Time_Unit) + (time2.tv_usec);
    timefor = end - start;
    OH_LOG_INFO(LOG_APP, "lq-for-loop,%{public}ld", timefor);

    gettimeofday(&time1, NULL);
    for (int i = 0; i < Num_Count; i++) {
        napi_typeof(env, args[0], &valuetype0);
    }
    gettimeofday(&time2, NULL);
    TestTime(napi_typeof);

    double value0;
    gettimeofday(&time1, NULL);
    for (int i = 0; i < Num_Count; i++) {
        napi_get_value_double(env, args[0], &value0);
    }
    gettimeofday(&time2, NULL);
    TestTime(napi_get_value_double);

    int32_t value1;
    gettimeofday(&time1, NULL);
    for (int i = 0; i < Num_Count; i++) {
        napi_get_value_int32(env, args[1], &value1);
    }
    gettimeofday(&time2, NULL);
    TestTime(napi_get_value_int32);

    uint32_t value2;
    gettimeofday(&time1, NULL);
    for (int i = 0; i < Num_Count; i++) {
        napi_get_value_uint32(env, args[2], &value2);
    }
    gettimeofday(&time2, NULL);
    TestTime(napi_get_value_uint32);

    int64_t value3;
    gettimeofday(&time1, NULL);
    for (int i = 0; i < Num_Count; i++) {
        napi_get_value_int64(env, args[2], &value3);
    }
    gettimeofday(&time2, NULL);
    TestTime(napi_get_value_int64);

    bool value4;
    gettimeofday(&time1, NULL);
    for (int i = 0; i < Num_Count; i++) {
        napi_get_value_bool(env, args[4], &value4);
    }
    gettimeofday(&time2, NULL);
    TestTime(napi_get_value_bool);

    char str[3] = "13";
    size_t strsize = strlen(str);
    size_t result = 0;
    gettimeofday(&time1, NULL);
    for (int i = 0; i < Num_Count; i++) {
        napi_get_value_string_latin1(env, args[3], str, strsize, &result);
    }
    gettimeofday(&time2, NULL);
    TestTime(napi_get_value_string_latin1);

    gettimeofday(&time1, NULL);
    for (int i = 0; i < Num_Count; i++) {
        napi_get_value_string_utf8(env, args[3], str, strsize, &result);
    }
    gettimeofday(&time2, NULL);
    TestTime(napi_get_value_string_utf8);

    bool val = false;
    napi_value res;
    gettimeofday(&time1, NULL);
    for (int i = 0; i < Num_Count; i++) {
        napi_get_boolean(env, val,  &res);
    }
    gettimeofday(&time2, NULL);
    TestTime(napi_get_boolean);

    napi_value res1;
    gettimeofday(&time1, NULL);
    for (int i = 0; i < Num_Count; i++) {
        napi_get_global(env, &res1);
    }
    gettimeofday(&time2, NULL);
    TestTime(napi_get_global);

    gettimeofday(&time1, NULL);
    for (int i = 0; i < Num_Count; i++) {
        napi_get_null(env, &res1);
    }
    gettimeofday(&time2, NULL);
    TestTime(napi_get_null);

    gettimeofday(&time1, NULL);
    for (int i = 0; i < Num_Count; i++) {
        napi_get_undefined(env, &res1);
    }
    gettimeofday(&time2, NULL);
    TestTime(napi_get_undefined);

    const napi_extended_error_info* PLastError = nullptr;
    gettimeofday(&time1, NULL);
    for (int i = 0; i < Num_Count; i++) {
        napi_get_last_error_info(env, &PLastError);
    }
    gettimeofday(&time2, NULL);
    TestTime(napi_get_last_error_info);
    return NULL;
}

napi_value SayHello(napi_env env, napi_callback_info info)
{
    return NULL;
}

napi_value CreateValue(napi_env env, napi_callback_info info)
{
    struct timeval time1;
    struct timeval time2;
    time_t t1;
    time_t t2;
    time_t t;
    napi_value object = nullptr;
    gettimeofday(&time1, NULL);
    for (int i = 0; i < Num_Count; i++) {
        napi_create_object(env, &object);
    }
    gettimeofday(&time2, NULL);
    TestTime(napi_create_object);

    int32_t num = 123;
    napi_value res = nullptr;
    gettimeofday(&time1, NULL);
    for (int i = 0; i < Num_Count; i++) {
        napi_create_int32(env, num, &res);
    }
    gettimeofday(&time2, NULL);
    TestTime(napi_create_int32);

    uint32_t num1 = 123;
    napi_value res1 = nullptr;
    gettimeofday(&time1, NULL);
    for (int i = 0; i < Num_Count; i++) {
        napi_create_uint32(env, num1, &res1);
    }
    gettimeofday(&time2, NULL);
    TestTime(napi_create_uint32);

    int64_t num2 = 123;
    napi_value res2 = nullptr;
    gettimeofday(&time1, NULL);
    for (int i = 0; i < Num_Count; i++) {
        napi_create_int64(env, num2, &res2);
    }
    gettimeofday(&time2, NULL);
    TestTime(napi_create_int64);

    double num3 = 123.0;
    napi_value res3 = nullptr;
    gettimeofday(&time1, NULL);
    for (int i = 0; i < Num_Count; i++) {
        napi_create_double(env, num3, &res3);
    }
    gettimeofday(&time2, NULL);
    TestTime(napi_create_double);

    napi_value messageKey = nullptr;
    const char* messageKeyStr = "message";
    gettimeofday(&time1, NULL);
    for (int i = 0; i < Num_Count; i++) {
        napi_create_string_latin1(env, messageKeyStr, strlen(messageKeyStr), &messageKey);
    }
    gettimeofday(&time2, NULL);
    TestTime(napi_create_string_latin1);

    napi_value testString = nullptr;
    const char* testStringStr = "test";
    gettimeofday(&time1, NULL);
    for (int i = 0; i < Num_Count; i++) {
        napi_create_string_utf8(env, testStringStr, strlen(testStringStr), &testString);
    }
    gettimeofday(&time2, NULL);
    TestTime(napi_create_string_utf8);

    napi_value array = nullptr;
    gettimeofday(&time1, NULL);
    for (int i = 0; i < Num_Count; i++) {
        napi_create_array(env, &array);
    }
    gettimeofday(&time2, NULL);
    TestTime(napi_create_array);

    napi_value arrayres = nullptr;
    size_t length = 8;
    gettimeofday(&time1, NULL);
    for (int i = 0; i < Num_Count; i++) {
        napi_create_array_with_length(env, length, &arrayres);
    }
    gettimeofday(&time2, NULL);
    TestTime(napi_create_array_with_length);

    const char* testStr1 = "testSymbol";
    napi_value result1 = nullptr;
    napi_create_string_latin1(env, testStr1,strlen(testStr1), &result1);
    napi_value symbolVal = nullptr;
    gettimeofday(&time1, NULL);
    for (int i = 0; i < Num_Count; i++) {
        napi_create_symbol(env, result1, &symbolVal);
    }
    gettimeofday(&time2, NULL);
    TestTime(napi_create_symbol);

    napi_value fn;
    gettimeofday(&time1, NULL);
    for (int i = 0; i < Num_Count; i++) {
        napi_create_function(env, NULL, 0, SayHello, NULL, &fn);
    }
    gettimeofday(&time2, NULL);
    TestTime(napi_create_function);

    napi_value code = nullptr;
    napi_value message = nullptr;
    napi_create_string_latin1(env, "500", NAPI_AUTO_LENGTH, &code);
    napi_create_string_latin1(env, "common error", NAPI_AUTO_LENGTH, &message);
    napi_value error = nullptr;
    gettimeofday(&time1, NULL);
    for (int i = 0; i < Num_Count; i++) {
        napi_create_error(env, code, message, &error);
    }
    gettimeofday(&time2, NULL);
    TestTime(napi_create_error);

    napi_value code1 = nullptr;
    napi_value message1 = nullptr;
    napi_create_string_latin1(env, "500", NAPI_AUTO_LENGTH, &code1);
    napi_create_string_latin1(env, "type error", NAPI_AUTO_LENGTH, &message1);
    napi_value error1 = nullptr;
    gettimeofday(&time1, NULL);
    for (int i = 0; i < Num_Count; i++) {
        napi_create_type_error(env, code1, message1, &error1);
    }
    gettimeofday(&time2, NULL);
    TestTime(napi_create_type_error);

    napi_value code2 = nullptr;
    napi_value message2 = nullptr;
    napi_create_string_latin1(env, "500", NAPI_AUTO_LENGTH, &code2);
    napi_create_string_latin1(env, "range error", NAPI_AUTO_LENGTH, &message2);
    napi_value error2 = nullptr;
    gettimeofday(&time1, NULL);
    for (int i = 0; i < Num_Count; i++) {
        napi_create_range_error(env, code2, message2, &error2);
    }
    gettimeofday(&time2, NULL);
    TestTime(napi_create_range_error);
    return NULL;
}

static napi_value TestApi(napi_env env, napi_callback_info info)
{
    struct timeval time1;
    struct timeval time2;
    time_t t1;
    time_t t2;
    time_t t;
    bool num = false;
    napi_value res = nullptr;
    napi_value result = nullptr;
    napi_get_boolean(env, num, &res);
    gettimeofday(&time1, NULL);
    for (int i = 0; i < Num_Count; i++) {
        napi_coerce_to_bool(env, res, &result);
    }
    gettimeofday(&time2, NULL);
    TestTime(napi_coerce_to_bool);

    double num1 = 1.0;
    napi_value res1 = nullptr;
    napi_value result1 = nullptr;
    napi_create_double(env, num1, &res1);
    gettimeofday(&time1, NULL);
    for (int i = 0; i < Num_Count; i++) {
        napi_coerce_to_number(env, res1, &result1);
    }
    gettimeofday(&time2, NULL);
    TestTime(napi_coerce_to_number);

    napi_value obj = nullptr;
    napi_value res2 = nullptr;
    napi_create_object(env, &obj);
    gettimeofday(&time1, NULL);
    for (int i = 0; i < Num_Count; i++) {
        napi_coerce_to_object(env, obj, &res2);
    }
    gettimeofday(&time2, NULL);
    TestTime(napi_coerce_to_object);

    const char* testStringStr = "test";
    napi_value testString = nullptr;
    napi_create_string_utf8(env, testStringStr, strlen(testStringStr), &testString);
    napi_value res3 = nullptr;
    gettimeofday(&time1, NULL);
    for (int i = 0; i < Num_Count; i++) {
        napi_coerce_to_string(env, testString, &res3);
    }
    gettimeofday(&time2, NULL);
    TestTime(napi_coerce_to_object);

    napi_value res4 = nullptr;
    napi_create_object(env, &res4);
    napi_value messageKey = nullptr;
    const char* messageKeyStr = "message";
    napi_create_string_latin1(env, messageKeyStr, strlen(messageKeyStr), &messageKey);
    napi_value messageValue = nullptr;
    const char* messageValueStr = "ok";
    napi_create_string_latin1(env, messageValueStr, strlen(messageValueStr), &messageValue);
    gettimeofday(&time1, NULL);
    for (int i = 0; i < Num_Count; i++) {
        napi_set_property(env, res4, messageKey, messageValue);
    }
    gettimeofday(&time2, NULL);
    TestTime(napi_set_property);

    napi_value res5 = nullptr;
    gettimeofday(&time1, NULL);
    for (int i = 0; i < Num_Count; i++) {
        napi_get_prototype(env, res4, &res5);
    }
    gettimeofday(&time2, NULL);
    TestTime(napi_get_property);

    bool hasProperty = false;
    gettimeofday(&time1, NULL);
    for (int i = 0; i < Num_Count; i++) {
        napi_has_property(env, res4, messageKey, &hasProperty);
    }
    gettimeofday(&time2, NULL);
    TestTime(napi_has_property);

    napi_value res6 = nullptr;
    gettimeofday(&time1, NULL);
    for (int i = 0; i < Num_Count; i++) {
        napi_get_property(env, res4, messageKey, &res6);
    }
    gettimeofday(&time2, NULL);
    TestTime(napi_get_property);

    bool res7 = false;
    gettimeofday(&time1, NULL);
    for (int i = 0; i < Num_Count; i++) {
        napi_delete_property(env, res4, messageKey, &res7);
    }
    gettimeofday(&time2, NULL);
    TestTime(napi_delete_property);

    bool res8 = false;
    gettimeofday(&time1, NULL);
    for (int i = 0; i < Num_Count; i++) {
        napi_has_own_property(env, res4, messageKey, &res8);
    }
    gettimeofday(&time2, NULL);
    TestTime(napi_has_own_property);

    napi_value res9 = nullptr;
    napi_create_object(env, &res9);
    const char testStr[] = "1234567";
    napi_value strAttribute = nullptr;
    napi_create_string_utf8(env, testStr, strlen(testStr), &strAttribute);
    gettimeofday(&time1, NULL);
    for (int i = 0; i < Num_Count; i++) {
        napi_set_named_property(env, res9, "strAttribute", strAttribute);
    }
    gettimeofday(&time2, NULL);
    TestTime(napi_set_named_property);

    bool res10 = false;
    gettimeofday(&time1, NULL);
    for (int i = 0; i < Num_Count; i++) {
        napi_has_named_property(env, res9, "strAttribute", &res10);
    }
    gettimeofday(&time2, NULL);
    TestTime(napi_has_named_property);

    napi_value resStrAttribute = nullptr;
    gettimeofday(&time1, NULL);
    for (int i = 0; i < Num_Count; i++) {
        napi_get_named_property(env, res9, "strAttribute", &resStrAttribute);
    }
    gettimeofday(&time2, NULL);
    TestTime(napi_get_named_property);

    napi_value propNames = nullptr;
    gettimeofday(&time1, NULL);
    for (int i = 0; i < Num_Count; i++) {
        napi_get_property_names(env, res9, &propNames);
    }
    gettimeofday(&time2, NULL);
    TestTime(napi_get_property_names);

    napi_value array = nullptr;
    napi_create_array(env, &array);
    bool isArray = false;
    gettimeofday(&time1, NULL);
    for (int i = 0; i < Num_Count; i++) {
        napi_is_array(env, array, &isArray);
    }
    gettimeofday(&time2, NULL);
    TestTime(napi_is_array);

    napi_value nums = nullptr;
    uint32_t index = 1;
    napi_create_uint32(env, index, &nums);
    gettimeofday(&time1, NULL);
    for (int i = 0; i < Num_Count; i++) {
        napi_set_element(env, array, index, nums);
    }
    gettimeofday(&time2, NULL);
    TestTime(napi_set_element);

    uint32_t arrayLenth = 0;
    gettimeofday(&time1, NULL);
    for (int i = 0; i < Num_Count; i++) {
        napi_get_array_length(env, array, &arrayLenth);
    }
    gettimeofday(&time2, NULL);
    TestTime(napi_get_array_length);

    bool hasIndex = false;
    gettimeofday(&time1, NULL);
    for (int i = 0; i < Num_Count; i++) {
        napi_has_element(env, array, 1, &hasIndex);
    }
    gettimeofday(&time2, NULL);
    TestTime(napi_has_element);

    napi_value element = nullptr;
    gettimeofday(&time1, NULL);
    for (int i = 0; i < Num_Count; i++) {
        napi_get_element(env, array, 1, &element);
    }
    gettimeofday(&time2, NULL);
    TestTime(napi_get_element);

    bool isDelete = false;
    gettimeofday(&time1, NULL);
    for (int i = 0; i < Num_Count; i++) {
        napi_delete_element(env, array, 1, &isDelete);
    }
    gettimeofday(&time2, NULL);
    TestTime(napi_delete_element);

    const char* stringStr = "test";
    napi_value string = nullptr;
    napi_create_string_utf8(env, stringStr, strlen(stringStr), &string);
    bool isStrictEquals = false;
    gettimeofday(&time1, NULL);
    for (int i = 0; i < Num_Count; i++) {
        napi_strict_equals(env, string, string, &isStrictEquals);
    }
    gettimeofday(&time2, NULL);
    TestTime(napi_strict_equals);

    napi_value recv = nullptr;
    napi_value funcValue = nullptr;
    napi_get_undefined(env, &recv);
    napi_create_function(env, NULL, 0, SayHello, nullptr, &funcValue);
    napi_value funcResultValue = nullptr;
    gettimeofday(&time1, NULL);
    for (int i = 0; i < Num_Count; i++) {
        napi_call_function(env, recv, funcValue, 0, nullptr, &funcResultValue);
    }
    gettimeofday(&time2, NULL);
    TestTime(napi_call_function);
    return NULL;
}

time_t open_handle_time = 0;
time_t close_handle_time = 0;
time_t open_escapable_handle_time = 0;
time_t escape_handle_time = 0;
time_t close_escapable_time = 0;
time_t delete_reference_time = 0;
time_t external_arraybuffer_time = 0;
time_t resolve_deferred_time = 0;
time_t reject_deferred_time = 0;
time_t create_async_work_time = 0;
time_t queue_async_work_time = 0;
time_t cancel_async_work_time = 0;
time_t delete_async_work_time = 0;
time_t create_threadsafe_function_time = 0;
time_t get_threadsafe_function_time = 0;
time_t acquire_threadsafe_function_time = 0;
time_t call_threadsafe_function_time = 0;
time_t ref_threadsafe_function_time = 0;
time_t unref_threadsafe_function_time = 0;
time_t release_threadsafe_function_time = 0;
int counts = 0;
int numtimes = 0;
napi_threadsafe_function tsfn;
napi_ref functionRef;
static void CallJs(napi_env env, napi_value js_cb, void *context, void *data)
{
    napi_value undefined;
    napi_get_undefined(env, &undefined);
    napi_value cb;
    napi_get_reference_value(env, functionRef, &cb);
    napi_value ret;
    int32_t num = 0;
    napi_call_function(env, undefined, cb, 0, nullptr, &ret);
    napi_get_value_int32(env, ret, &num);
}
static napi_value TestScopeAndWork(napi_env env, napi_callback_info info)
{
    struct timeval time1;
    struct timeval time2;
    napi_value recv = nullptr;
    napi_value funcValue = nullptr;
    napi_get_undefined(env, &recv);
    napi_create_function(env, NULL, 0, SayHello, nullptr, &funcValue);
    napi_handle_scope parentScope = nullptr;
    gettimeofday(&time1, NULL);
    napi_open_handle_scope(env, &parentScope);
    gettimeofday(&time2, NULL);
    time_t t3 = (time1.tv_sec * Time_Unit) + (time1.tv_usec);
    time_t t4 = (time2.tv_sec * Time_Unit) + (time2.tv_usec);
    time_t openTime = t4 - t3;
    open_handle_time += openTime;

    gettimeofday(&time1, NULL);
    napi_close_handle_scope(env, parentScope);
    gettimeofday(&time2, NULL);
    time_t t5 = (time1.tv_sec * Time_Unit) + (time1.tv_usec);
    time_t t6 = (time2.tv_sec * Time_Unit) + (time2.tv_usec);
    time_t closeTime = t6 - t5;
    close_handle_time += closeTime;

    napi_escapable_handle_scope childScope = nullptr;
    gettimeofday(&time1, NULL);
    napi_open_escapable_handle_scope(env, &childScope);
    gettimeofday(&time2, NULL);
    time_t t7 = (time1.tv_sec * Time_Unit) + (time1.tv_usec);
    time_t t8 = (time2.tv_sec * Time_Unit) + (time2.tv_usec);
    time_t open_escapableTime = t8 - t7;
    open_escapable_handle_time += open_escapableTime;

    napi_value funcResultValue = nullptr;
    napi_value newFuncResultValue = nullptr;
    napi_call_function(env, recv, funcValue, 0, nullptr, &funcResultValue);
    gettimeofday(&time1, NULL);
    napi_escape_handle(env, childScope, funcResultValue, &newFuncResultValue);
    gettimeofday(&time2, NULL);
    time_t t9 = (time1.tv_sec * Time_Unit) + (time1.tv_usec);
    time_t t10 = (time2.tv_sec * Time_Unit) + (time2.tv_usec);
    time_t escapeTime = t10 - t9;
    escape_handle_time += escapeTime;

    gettimeofday(&time1, NULL);
    napi_close_escapable_handle_scope(env, childScope);
    gettimeofday(&time2, NULL);
    time_t t11 = (time1.tv_sec * Time_Unit) + (time1.tv_usec);
    time_t t12 = (time2.tv_sec * Time_Unit) + (time2.tv_usec);
    time_t close_escapable_time = t12 - t11;
    close_escapable_time += close_escapable_time;

    napi_value result = nullptr;
    napi_ref resultRef = nullptr;
    napi_create_object(env, &result);
    napi_create_reference(env, result, 1, &resultRef);
    gettimeofday(&time1, NULL);
    napi_delete_reference(env, resultRef);
    gettimeofday(&time2, NULL);
    time_t t13 = (time1.tv_sec * Time_Unit) + (time1.tv_usec);
    time_t t14 = (time2.tv_sec * Time_Unit) + (time2.tv_usec);
    time_t delete_reference = t14 - t13;
    delete_reference_time += delete_reference;

    int* testInt = (int*)malloc(sizeof(int));
    *testInt = 666;
    napi_value external = nullptr;
    size_t arrayBufferSize1 = 1024;
    gettimeofday(&time1, NULL);
    napi_create_external_arraybuffer(
        env, (void *)testInt, arrayBufferSize1,
        [](napi_env env, void *data, void *hint) {
        int *temp = static_cast<int *>(data);
        free(temp);
        temp = nullptr;
        },
        (void *)testInt, &external);
    gettimeofday(&time2, NULL);
    time_t t15 = (time1.tv_sec * Time_Unit) + (time1.tv_usec);
    time_t t16 = (time2.tv_sec * Time_Unit) + (time2.tv_usec);
    time_t external_arraybuffer = t16 - t15;
    external_arraybuffer_time += external_arraybuffer;

    struct AsyncWorkContext {
        napi_async_work work = nullptr;
    };
    auto asyncWorkContext = new AsyncWorkContext();
    napi_value resourceName = nullptr;
    napi_create_string_utf8(env, "AsyncWorkTest", NAPI_AUTO_LENGTH, &resourceName);
    gettimeofday(&time1, NULL);
    napi_create_async_work(
        env, nullptr, resourceName, [](napi_env value, void* data) {},
        [](napi_env env, napi_status status, void* data) {
        AsyncWorkContext* asyncWorkContext = (AsyncWorkContext*)data;
        napi_delete_async_work(env, asyncWorkContext->work);
        delete asyncWorkContext;
        },
        asyncWorkContext, &asyncWorkContext->work);
    gettimeofday(&time2, NULL);
    time_t t17 = (time1.tv_sec * Time_Unit) + (time1.tv_usec);
    time_t t18 = (time2.tv_sec * Time_Unit) + (time2.tv_usec);
    time_t create_async_work = t18 -t17;
    create_async_work_time += create_async_work;

    gettimeofday(&time1, NULL);
    napi_queue_async_work(env, asyncWorkContext->work);
    gettimeofday(&time2, NULL);
    time_t t19 = (time1.tv_sec * Time_Unit) + (time1.tv_usec);
    time_t t20 = (time2.tv_sec * Time_Unit) + (time2.tv_usec);
    time_t queue_async_work = t20 -t19; 
    queue_async_work_time += queue_async_work;

    gettimeofday(&time1, NULL);
    napi_cancel_async_work(env, asyncWorkContext->work);
    gettimeofday(&time2, NULL);
    time_t t21 = (time1.tv_sec * Time_Unit) + (time1.tv_usec);
    time_t t22 = (time2.tv_sec * Time_Unit) + (time2.tv_usec);
    time_t cancel_async_work = t22 -t21;
    cancel_async_work_time += cancel_async_work;

    size_t argc = 1;
    napi_value js_cb, work_name;
    napi_get_cb_info(env, info, &argc, &js_cb, NULL, NULL);
    napi_create_reference(env, js_cb, 1, &functionRef);
    napi_create_string_utf8(env, "work_name", NAPI_AUTO_LENGTH, &work_name);
    const char context[] = "context";
    gettimeofday(&time1, NULL);
    napi_create_threadsafe_function(
        env, js_cb, NULL, work_name, 0, 1, NULL, NULL, (void*)context, CallJs, &tsfn);
    gettimeofday(&time2, NULL);
    time_t t23 = (time1.tv_sec * Time_Unit) + (time1.tv_usec);
    time_t t24 = (time2.tv_sec * Time_Unit) + (time2.tv_usec);
    time_t create_threadsafe_function = t24 -t23;
    create_threadsafe_function_time += create_threadsafe_function;

    void* retContext = nullptr;
    gettimeofday(&time1, NULL);
    napi_get_threadsafe_function_context(tsfn, &retContext);
    gettimeofday(&time2, NULL);
    time_t t25 = (time1.tv_sec * Time_Unit) + (time1.tv_usec);
    time_t t26 = (time2.tv_sec * Time_Unit) + (time2.tv_usec);
    time_t get_threadsafe_function = t26 -t25;
    get_threadsafe_function_time += get_threadsafe_function;

    gettimeofday(&time1, NULL);
    napi_acquire_threadsafe_function(tsfn);
    gettimeofday(&time2, NULL);
    time_t t27 = (time1.tv_sec * Time_Unit) + (time1.tv_usec);
    time_t t28 = (time2.tv_sec * Time_Unit) + (time2.tv_usec);
    time_t acquire_threadsafe_function = t28 -t27;
    acquire_threadsafe_function_time += acquire_threadsafe_function;

    napi_threadsafe_function_call_mode blockMode = napi_tsfn_nonblocking;
    static int32_t gSendData = 10;
    gettimeofday(&time1, NULL);
    napi_call_threadsafe_function(tsfn, &gSendData, blockMode);
    gettimeofday(&time2, NULL);
    time_t t29 = (time1.tv_sec * Time_Unit) + (time1.tv_usec);
    time_t t30 = (time2.tv_sec * Time_Unit) + (time2.tv_usec);
    time_t call_threadsafe_function = t30 -t29;
    call_threadsafe_function_time += call_threadsafe_function;

    gettimeofday(&time1, NULL);
    napi_ref_threadsafe_function(env, tsfn);
    gettimeofday(&time2, NULL);
    time_t t31 = (time1.tv_sec * Time_Unit) + (time1.tv_usec);
    time_t t32 = (time2.tv_sec * Time_Unit) + (time2.tv_usec);
    time_t ref_threadsafe_function = t32 -t31;
    ref_threadsafe_function_time += ref_threadsafe_function;

    gettimeofday(&time1, NULL);
    napi_unref_threadsafe_function(env, tsfn);
    gettimeofday(&time2, NULL);
    time_t t33 = (time1.tv_sec * Time_Unit) + (time1.tv_usec);
    time_t t34 = (time2.tv_sec * Time_Unit) + (time2.tv_usec);
    time_t unref_threadsafe_function = t34 -t33;
    unref_threadsafe_function_time += unref_threadsafe_function;

    gettimeofday(&time1, NULL);
    napi_release_threadsafe_function(tsfn, napi_tsfn_release);
    gettimeofday(&time2, NULL);
    time_t t35 = (time1.tv_sec * Time_Unit) + (time1.tv_usec);
    time_t t36 = (time2.tv_sec * Time_Unit) + (time2.tv_usec);
    time_t relese_threadsafe_function = t36 -t35;
    release_threadsafe_function_time += relese_threadsafe_function;
    return NULL;
}

struct AddonData {
    napi_async_work asyncWork = nullptr;
    napi_deferred deferred = nullptr;
    napi_ref callback = nullptr;
    double args = 0;
    double result = 0;
};
static void AddExecuteCB(napi_env env, void *data) {
    AddonData *addonData = (AddonData *)data;
    addonData->result = addonData->args;
}

static void AddPromiseCompleteCB(napi_env env, napi_status status, void *data) 
{
    struct timeval time1;
    struct timeval time2;
    AddonData *addonData = (AddonData *)data;
    napi_value result = nullptr;
    napi_create_double(env, addonData->result, &result);
    if (addonData->result > 0) {
        gettimeofday(&time1, NULL);
        napi_resolve_deferred(env, addonData->deferred, result);
        gettimeofday(&time2, NULL);
        time_t  t1 = (time1.tv_sec * Time_Unit) + (time1.tv_usec);
        time_t  t2 = (time2.tv_sec * Time_Unit) + (time2.tv_usec);
        time_t  resolve_deferred = t2 -t1;
        resolve_deferred_time += resolve_deferred;     
    } else {
        gettimeofday(&time1, NULL);
        napi_reject_deferred(env, addonData->deferred, result);
        gettimeofday(&time2, NULL);
        time_t  t3 = (time1.tv_sec * Time_Unit) + (time1.tv_usec);
        time_t  t4 = (time2.tv_sec * Time_Unit) + (time2.tv_usec);
        time_t  reject_deferred = t4 -t3;
        reject_deferred_time += reject_deferred;
    }
    gettimeofday(&time1, NULL);
    napi_delete_async_work(env, addonData->asyncWork);
    gettimeofday(&time2, NULL);
    time_t  t5 = (time1.tv_sec * Time_Unit) + (time1.tv_usec);
    time_t  t6 = (time2.tv_sec * Time_Unit) + (time2.tv_usec);
    time_t  delete_async_work = t6 -t5;
    delete_async_work_time += delete_async_work;
    free(addonData);
    addonData = nullptr;
}

static napi_value TestWorkAndPromise(napi_env env, napi_callback_info info)
{
    size_t argc = 1;
    napi_value args[1];
    napi_get_cb_info(env, info, &argc, args, nullptr, nullptr);
    napi_value promise = nullptr;
    napi_deferred deferred = nullptr;
    napi_create_promise(env, &deferred, &promise);
    struct AddonData* addonData = (struct AddonData*)malloc(sizeof(struct AddonData));
    if (addonData == nullptr) {
        return nullptr;
    }
    addonData->deferred = deferred;
    napi_get_value_double(env, args[0], &addonData->args);
    napi_value resourceName = nullptr;
    napi_create_string_utf8(env, "AsyncCallback", NAPI_AUTO_LENGTH, &resourceName);
    napi_create_async_work(env, nullptr, resourceName, AddExecuteCB,
        AddPromiseCompleteCB, (void *)addonData, &addonData->asyncWork);
    napi_queue_async_work(env, addonData->asyncWork);
    return promise;    
}

static napi_value GetResult(napi_env env, napi_callback_info info)
{
    numtimes += 1;
    OH_LOG_INFO(LOG_APP, "lq-numtimes-times,%{public}d", numtimes);
    OH_LOG_INFO(LOG_APP, "lq-napi_open_handle_scope,%{public}ld", open_handle_time - timefor);
    OH_LOG_INFO(LOG_APP, "lq-napi_close_handle_scope,%{public}ld", close_handle_time - timefor);
    OH_LOG_INFO(LOG_APP, "lq-napi_open_escapable_handle_scope,%{public}ld", open_escapable_handle_time - timefor);
    OH_LOG_INFO(LOG_APP, "lq-napi_escape_handle,%{public}ld", escape_handle_time - timefor);
    OH_LOG_INFO(LOG_APP, "lq-napi_close_escapable_handle_scope,%{public}ld", close_escapable_time - timefor);
    OH_LOG_INFO(LOG_APP, "lq-napi_delete_reference,%{public}ld", delete_reference_time - timefor);
    OH_LOG_INFO(LOG_APP, "lq-napi_create_external_arraybuffer,%{public}ld", external_arraybuffer_time - timefor);
    OH_LOG_INFO(LOG_APP, "lq-napi_create_async_work,%{public}ld", create_async_work_time - timefor);
    OH_LOG_INFO(LOG_APP, "lq-napi_queue_async_work,%{public}ld", queue_async_work_time - timefor);
    OH_LOG_INFO(LOG_APP, "lq-napi_cancel_async_work,%{public}ld", cancel_async_work_time - timefor);
    OH_LOG_INFO(LOG_APP, "lq-napi_create_threadsafe_function,%{public}ld", create_threadsafe_function_time - timefor);
    OH_LOG_INFO(LOG_APP, "lq-napi_get_threadsafe_function_context,%{public}ld", get_threadsafe_function_time - timefor);
    OH_LOG_INFO(LOG_APP, "lq-napi_acquire_threadsafe_function,%{public}ld", acquire_threadsafe_function_time - timefor);
    OH_LOG_INFO(LOG_APP, "lq-napi_call_threadsafe_function,%{public}ld", call_threadsafe_function_time - timefor);
    OH_LOG_INFO(LOG_APP, "lq-napi_ref_threadsafe_function,%{public}ld", ref_threadsafe_function_time - timefor);
    OH_LOG_INFO(LOG_APP, "lq-napi_unref_threadsafe_function,%{public}ld", unref_threadsafe_function_time - timefor);
    OH_LOG_INFO(LOG_APP, "lq-napi_release_threadsafe_function,%{public}ld", release_threadsafe_function_time - timefor);
    return NULL;
}

static napi_value GetResult1(napi_env env, napi_callback_info info)
{
    counts += 1;
    OH_LOG_INFO(LOG_APP, "lq-counts-times,%{public}d", counts);
    OH_LOG_INFO(LOG_APP, "lq-napi_delete_async_work,%{public}ld", delete_async_work_time - timefor);
    OH_LOG_INFO(LOG_APP, "lq-napi_resolve_deferred,%{public}ld", resolve_deferred_time - timefor);
    OH_LOG_INFO(LOG_APP, "lq-napi_reject_deferred,%{public}ld", reject_deferred_time - timefor);
    return NULL;
}

static napi_value TestInstance(napi_env env, napi_callback_info info)
{
    struct timeval time1;
    struct timeval time2;
    time_t  t1;
    time_t  t2;
    time_t t;
    napi_value thisVar = nullptr;
    gettimeofday(&time1, NULL);
    for (int i = 0; i < Num_Count; i++) {
        napi_get_cb_info(env, info, nullptr, nullptr, &thisVar, nullptr);
    }
    gettimeofday(&time2, NULL);
    TestTime(napi_get_cb_info);

    napi_value testClass = nullptr;
    gettimeofday(&time1, NULL);
    for (int i = 0; i < Num_Count; i++) {
        napi_define_class(
            env, "TestClass", NAPI_AUTO_LENGTH,
            [](napi_env env, napi_callback_info info) -> napi_value {
                napi_value thisVar = nullptr;
                napi_get_cb_info(env, info, nullptr, nullptr, &thisVar, nullptr);
                return thisVar;
            },
            nullptr, 0, nullptr, &testClass);
    }
    gettimeofday(&time2, NULL);
    TestTime(napi_define_class);

    napi_value testWrapClass = nullptr;
    napi_define_class(env, "TestWrapClass", NAPI_AUTO_LENGTH, [](napi_env env, napi_callback_info info) -> napi_value {
                  napi_value thisVar = nullptr;
                  napi_get_cb_info(env, info, nullptr, nullptr, &thisVar, nullptr);
                  return thisVar;
              }, nullptr, 0, nullptr, &testWrapClass);
      
    napi_value instanceValue = nullptr;
    gettimeofday(&time1, NULL);
    for (int i = 0; i < Num_Count; i++) {
        napi_new_instance(env, testWrapClass, 0, nullptr, &instanceValue);
    }
    gettimeofday(&time2, NULL);
    TestTime(napi_new_instance);

    bool isInstanceOf = false;
    gettimeofday(&time1, NULL);
    for (int i = 0; i < Num_Count; i++) {
        napi_instanceof(env, instanceValue, testWrapClass, &isInstanceOf);
    }
    gettimeofday(&time2, NULL);
    TestTime(napi_instanceof);

    const char* testStr = "test";
    gettimeofday(&time1, NULL);
    for (int i = 0; i < Num_Count; i++) {
        napi_wrap(
            env, instanceValue, (void *)testStr, [](napi_env env, void *data, void *hint) {}, nullptr, nullptr);
    }
    gettimeofday(&time2, NULL);
    TestTime(napi_wrap);

    char* tmpTestStr = nullptr;
    gettimeofday(&time1, NULL);
    for (int i = 0; i < Num_Count; i++) {
        napi_unwrap(env, instanceValue, (void **)&tmpTestStr);
    }
    gettimeofday(&time2, NULL);
    TestTime(napi_unwrap);

    char* tmpTestStr1 = nullptr;
    gettimeofday(&time1, NULL);
    for (int i = 0; i < Num_Count; i++) {
        napi_remove_wrap(env, instanceValue, (void **)&tmpTestStr1);
    }
    gettimeofday(&time2, NULL);
    TestTime(napi_remove_wrap);
    return NULL;
}

static napi_value MyConstructor(napi_env env, napi_callback_info info)
{
    struct timeval time1;
    struct timeval time2;
    time_t  t1;
    time_t  t2;
    time_t t;
    napi_value thisVar = nullptr;
    size_t argc1 = 0;
    napi_value constructor = nullptr;
    napi_get_cb_info(env, info, &argc1, nullptr, &thisVar, nullptr);
    gettimeofday(&time1, NULL);
    for (int i = 0; i < Num_Count; i++) {
        napi_get_new_target(env, info, &constructor);
    }
    gettimeofday(&time2, NULL);
    TestTime(napi_get_new_target);
    return thisVar;
}

static napi_value TestReferenceAndError(napi_env env, napi_callback_info info)
{
    struct timeval time1;
    struct timeval time2;
    time_t  t1;
    time_t  t2;
    time_t t;
    const char testStr[] = "test";
    napi_value external = nullptr;
    gettimeofday(&time1, NULL);
    for (int i = 0; i < Num_Count; i++) {
        napi_create_external(
            env, (void *)testStr,
            [](napi_env env, void *data, void *hint) {},
            (void *)testStr, &external);
    }
    gettimeofday(&time2, NULL);
    TestTime(napi_create_external);

    void* tmpExternal = nullptr;
    gettimeofday(&time1, NULL);
    for (int i = 0; i < Num_Count; i++) {
        napi_get_value_external(env, external, &tmpExternal);
    }
    gettimeofday(&time2, NULL);
    TestTime(napi_get_value_external);

    napi_value result = nullptr;
    napi_ref resultRef = nullptr;
    napi_create_object(env, &result);
    gettimeofday(&time1, NULL);
    for (int i = 0; i < Num_Count; i++) {
        napi_create_reference(env, result, 1, &resultRef);
    }
    gettimeofday(&time2, NULL);
    TestTime(napi_create_reference);

    uint32_t resultRefCount = 0;
    gettimeofday(&time1, NULL);
    for (int i = 0; i < Num_Count; i++) {
        napi_reference_ref(env, resultRef, &resultRefCount);
    }
    gettimeofday(&time2, NULL);
    TestTime(napi_reference_ref);

    gettimeofday(&time1, NULL);
    for (int i = 0; i < Num_Count; i++) {
        napi_reference_unref(env, resultRef, &resultRefCount);
    }
    gettimeofday(&time2, NULL);
    TestTime(napi_reference_unref);

    napi_value refValue = nullptr;
    gettimeofday(&time1, NULL);
    for (int i = 0; i < Num_Count; i++) {
        napi_get_reference_value(env, resultRef, &refValue);
    }
    gettimeofday(&time2, NULL);
    TestTime(napi_get_reference_value);

    napi_value code = nullptr;
    napi_value message = nullptr;
    napi_create_string_latin1(env, "100", NAPI_AUTO_LENGTH, &code);
    napi_create_string_latin1(env, "common error", NAPI_AUTO_LENGTH, &message);
    napi_value error = nullptr;
    napi_create_error(env, code, message, &error);
    bool isError = false;
    gettimeofday(&time1, NULL);
    for (int i = 0; i < Num_Count; i++) {
        napi_is_error(env, error, &isError);
    }
    gettimeofday(&time2, NULL);
    TestTime(napi_is_error);

    gettimeofday(&time1, NULL);
    for (int i = 0; i < Num_Count; i++) {
        napi_throw(env, error);
    }
    gettimeofday(&time2, NULL);
    TestTime(napi_throw);

    gettimeofday(&time1, NULL);
    for (int i = 0; i < Num_Count; i++) {
        napi_throw_error(env, "100", "Common error");
    }
    gettimeofday(&time2, NULL);
    TestTime(napi_throw_error);

    gettimeofday(&time1, NULL);
    for (int i = 0; i < Num_Count; i++) {
        napi_throw_range_error(env, "200", "Range error");
    }
    gettimeofday(&time2, NULL);
    TestTime(napi_throw_range_error);

    gettimeofday(&time1, NULL);
    for (int i = 0; i < Num_Count; i++) {
        napi_throw_type_error(env, "300", "Type error");
    }
    gettimeofday(&time2, NULL);
    TestTime(napi_throw_type_error);

    napi_value ex;
    gettimeofday(&time1, NULL);
    for (int i = 0; i < Num_Count; i++) {
        napi_get_and_clear_last_exception(env, &ex);
    }
    gettimeofday(&time2, NULL);
    TestTime(napi_get_and_clear_last_exception);

    bool isExceptionPending = false;
    gettimeofday(&time1, NULL);
    for (int i = 0; i < Num_Count; i++) {
        napi_is_exception_pending(env, &isExceptionPending);
    }
    gettimeofday(&time2, NULL);
    TestTime(napi_is_exception_pending);
    return NULL;
}

static napi_value TestArrayAndPromise(napi_env env, napi_callback_info info)
{
    struct timeval time1;
    struct timeval time2;
    time_t  t1;
    time_t  t2;
    time_t t;    
    napi_value arrayBuffer = nullptr;
    void* arrayBufferPtr = nullptr;
    size_t arrayBufferSize = 1024;
    gettimeofday(&time1, NULL);
    for (int i = 0; i < Num_Count; i++) {
        napi_create_arraybuffer(env, arrayBufferSize, &arrayBufferPtr, &arrayBuffer);
    }
    gettimeofday(&time2, NULL);
    TestTime(napi_create_arraybuffer);

    bool isArrayBuffer = false;
    gettimeofday(&time1, NULL);
    for (int i = 0; i < Num_Count; i++) {
        napi_is_arraybuffer(env, arrayBuffer, &isArrayBuffer);
    }
    gettimeofday(&time2, NULL);
    TestTime(napi_is_arraybuffer);

    void* tmpArrayBufferPtr = nullptr;
    size_t arrayBufferLength = 0;
    gettimeofday(&time1, NULL);
    for (int i = 0; i < Num_Count; i++) {
        napi_get_arraybuffer_info(env, arrayBuffer, &tmpArrayBufferPtr, &arrayBufferLength);
    }
    gettimeofday(&time2, NULL);
    TestTime(napi_get_arraybuffer_info);

    napi_value typedarray = nullptr;
    gettimeofday(&time1, NULL);
    for (int i = 0; i < Num_Count; i++) {
        napi_create_typedarray(env, napi_int8_array, arrayBufferSize, arrayBuffer, 0, &typedarray);
    }
    gettimeofday(&time2, NULL);
    TestTime(napi_create_typedarray);

    bool isTypedArray = false;
    gettimeofday(&time1, NULL);
    for (int i = 0; i < Num_Count; i++) {
        napi_is_typedarray(env, typedarray, &isTypedArray);
    }
    gettimeofday(&time2, NULL);
    TestTime(napi_is_typedarray);

    napi_typedarray_type typedarrayType;
    size_t typedarrayLength = 0;
    void *typedarrayBufferPtr = nullptr;
    napi_value tmpArrayBuffer = nullptr;
    size_t byteOffset = 0;
    gettimeofday(&time1, NULL);
    for (int i = 0; i < Num_Count; i++) {
        napi_get_typedarray_info(env, typedarray, &typedarrayType, &typedarrayLength, &typedarrayBufferPtr,
                                 &tmpArrayBuffer, &byteOffset);
    }
    gettimeofday(&time2, NULL);
    TestTime(napi_get_typedarray_info);

    napi_value result = nullptr;
    gettimeofday(&time1, NULL);
    for (int i = 0; i < Num_Count; i++) {
        napi_create_dataview(env, arrayBufferSize, arrayBuffer, 0, &result);
    }
    gettimeofday(&time2, NULL);
    TestTime(napi_create_dataview);

    bool isDataView = false;
    gettimeofday(&time1, NULL);
    for (int i = 0; i < Num_Count; i++) {
        napi_is_dataview(env, result, &isDataView);
    }
    gettimeofday(&time2, NULL);
    TestTime(napi_is_dataview);

    napi_value retArrayBuffer = nullptr;
    void *data = nullptr;
    size_t byteLength = 0;
    size_t byteOffset1 = 0;
    gettimeofday(&time1, NULL);
    for (int i = 0; i < Num_Count; i++) {
        napi_get_dataview_info(env, result, &byteLength, &data, &retArrayBuffer, &byteOffset1);
    }
    gettimeofday(&time2, NULL);
    TestTime(napi_get_dataview_info);

    uint32_t version;
    gettimeofday(&time1, NULL);
    for (int i = 0; i < Num_Count; i++) {
        napi_get_version(env, &version);
    }
    gettimeofday(&time2, NULL);
    TestTime(napi_get_version);

    napi_deferred deferred = nullptr;
    napi_value promise = nullptr;
    gettimeofday(&time1, NULL);
    for (int i = 0; i < Num_Count; i++) {
        napi_create_promise(env, &deferred, &promise);
    }
    gettimeofday(&time2, NULL);
    TestTime(napi_create_promise);

    bool isPromise = false;
    gettimeofday(&time1, NULL);
    for (int i = 0; i < Num_Count; i++) {
        napi_is_promise(env, promise, &isPromise);
    }
    gettimeofday(&time2, NULL);
    TestTime(napi_is_promise);

    napi_value script;
    napi_value result1;
    size_t argc = 1;
    napi_get_cb_info(env, info, &argc, &script, NULL, NULL);
    gettimeofday(&time1, NULL);
    for (int i = 0; i < Num_Count; i++) {
        napi_run_script(env, script, &result1);
    }
    gettimeofday(&time2, NULL);
    TestTime(napi_run_script);

    napi_value createResult = nullptr;
    double timeTest = 202110181203150;
    gettimeofday(&time1, NULL);
    for (int i = 0; i < Num_Count; i++) {
        napi_create_date(env, timeTest, &createResult);
    }
    gettimeofday(&time2, NULL);
    TestTime(napi_create_date);

    bool result2 = false;
    gettimeofday(&time1, NULL);
    for (int i = 0; i < Num_Count; i++) {
        napi_is_date(env, createResult, &result2);
    }
    gettimeofday(&time2, NULL);
    TestTime(napi_is_date);

    double getTime = false;
    gettimeofday(&time1, NULL);
    for (int i = 0; i < Num_Count; i++) {
        napi_get_date_value(env, createResult, &getTime);
    }
    gettimeofday(&time2, NULL);
    TestTime(napi_get_date_value);

    uint64_t testValue1= 9007199254740991;
    napi_value result3 = nullptr;
    gettimeofday(&time1, NULL);
    for (int i = 0; i < Num_Count; i++) {
        napi_create_bigint_uint64(env, testValue1, &result3);
    }
    gettimeofday(&time2, NULL);
    TestTime(napi_create_bigint_uint64);

    bool flag = false;
    uint64_t resultValue = 0;
    gettimeofday(&time1, NULL);
    for (int i = 0; i < Num_Count; i++) {
        napi_get_value_bigint_uint64(env, result3, &resultValue, &flag);
    }
    gettimeofday(&time2, NULL);
    TestTime(napi_get_value_bigint_uint64);

    int64_t testValue2 = 9007199254740991;
    napi_value result4 = nullptr;
    gettimeofday(&time1, NULL);
    for (int i = 0; i < Num_Count; i++) {
        napi_create_bigint_int64(env, testValue2, &result4);
    }
    gettimeofday(&time2, NULL);
    TestTime(napi_create_bigint_int64);

    bool flag1 = false;
    int64_t resultValue1 = 0;
    gettimeofday(&time1, NULL);
    for (int i = 0; i < Num_Count; i++) {
        napi_get_value_bigint_int64(env, result4, &resultValue1, &flag1);
    }
    gettimeofday(&time2, NULL);
    TestTime(napi_get_value_bigint_int64);

    int signBit = 0;
    size_t wordCount = 4;
    uint64_t words[] = { 0xFFFFFFFFFFFFFFFF, 34ULL, 56ULL, 0xFFFFFFFFFFFFFFFF };
    uint64_t wordsOut[] = { 0ULL, 0ULL, 0ULL, 0ULL };
    napi_value result5 = nullptr;
    gettimeofday(&time1, NULL);
    for (int i = 0; i < Num_Count; i++) {
        napi_create_bigint_words(env, signBit, wordCount, words, &result5);
    }
    gettimeofday(&time2, NULL);
    TestTime(napi_create_bigint_words);

    gettimeofday(&time1, NULL);
    for (int i = 0; i < Num_Count; i++) {
        napi_get_value_bigint_words(env, result5, &signBit, &wordCount, wordsOut);
    }
    gettimeofday(&time2, NULL);
    TestTime(napi_get_value_bigint_words);

    const napi_node_version* version1;
    gettimeofday(&time1, NULL);
    for (int i = 0; i < Num_Count; i++) {
        napi_get_node_version(env, &version1);
    }
    gettimeofday(&time2, NULL);
    TestTime(napi_get_node_version);

    struct uv_loop_s* loop = nullptr;
    gettimeofday(&time1, NULL);
    for (int i = 0; i < Num_Count; i++) {
        napi_get_uv_event_loop(env, &loop);
    }
    gettimeofday(&time2, NULL);
    TestTime(napi_get_uv_event_loop);
    return NULL;
}    

static napi_value Init1(napi_env env, napi_value exports)
{
    napi_property_descriptor desc[] = {
            { "getValue", nullptr, GetValue, nullptr, nullptr, nullptr, napi_default, nullptr }
        };
    napi_define_properties(env, exports, sizeof(desc) / sizeof(desc[0]), desc);
    return exports;
}
static napi_module demoModule1 = {
    .nm_version =1,
    .nm_flags = 0,
    .nm_filename = nullptr,
    .nm_register_func = Init1,
    .nm_modname = "entry",
    .nm_priv = ((void*)0),
    .reserved = { 0 },
};
static napi_value TestModuleRegister(napi_env env, napi_callback_info info)
{
    struct timeval time1;
    struct timeval time2;
    time_t  t1;
    time_t  t2;
    time_t t;
    gettimeofday(&time1, NULL);
    for (int i = 0; i < Num_Count; i++) {
        napi_module_register(&demoModule1);
    }
    gettimeofday(&time2, NULL);
    TestTime(napi_module_register);
    return NULL;
}

static napi_value TestDefineProperties(napi_env env, napi_callback_info info)
{
    napi_value object;
    napi_property_descriptor desc[] = {
        { "getValue", nullptr, GetValue, nullptr, nullptr, nullptr, napi_default, nullptr }
    };
    napi_create_object(env, &object);
    struct timeval time1;
    struct timeval time2;
    time_t  t1;
    time_t  t2;
    time_t t;
    gettimeofday(&time1, NULL);
    for (int i = 0; i < Num_Count; i++) {
        napi_define_properties(env, object, sizeof(desc) / sizeof(desc[0]), desc);
    }
    gettimeofday(&time2, NULL);
    TestTime(napi_define_properties);
    return NULL;
}

EXTERN_C_START
static napi_value Init(napi_env env, napi_value exports)
{
    napi_property_descriptor desc[] = {
        { "getValue", nullptr, GetValue, nullptr, nullptr, nullptr, napi_default, nullptr }
    };
    napi_property_descriptor desc1[] = {
        { "createValue", nullptr, CreateValue, nullptr, nullptr, nullptr, napi_default, nullptr }
    };
    napi_property_descriptor desc2[] = {
        { "testApi", nullptr, TestApi, nullptr, nullptr, nullptr, napi_default, nullptr }
    };
    napi_property_descriptor desc3[] = {
        { "getResult", nullptr, GetResult, nullptr, nullptr, nullptr, napi_default, nullptr }
    };
    napi_property_descriptor desc4[] = {
        { "testScopeAndWork", nullptr, TestScopeAndWork, nullptr, nullptr, nullptr, napi_default, nullptr }
    };
    napi_property_descriptor desc5[] = {
        { "testInstance", nullptr, TestInstance, nullptr, nullptr, nullptr, napi_default, nullptr }
    };
    napi_property_descriptor desc6[] = {
        { "testReferenceAndError", nullptr, TestReferenceAndError, nullptr, nullptr, nullptr, napi_default, nullptr }
    };
    napi_property_descriptor desc7[] = {
        { "testArrayAndPromise", nullptr, TestArrayAndPromise, nullptr, nullptr, nullptr, napi_default, nullptr }
    };
    napi_property_descriptor desc8[] = {
        { "testWorkAndPromise", nullptr, TestWorkAndPromise, nullptr, nullptr, nullptr, napi_default, nullptr }
    };
    napi_property_descriptor desc9[] = {
        { "getResult1", nullptr, GetResult1, nullptr, nullptr, nullptr, napi_default, nullptr }
    };
    napi_property_descriptor desc10[] = {
        { "testModuleRegister", nullptr, TestModuleRegister, nullptr, nullptr, nullptr, napi_default, nullptr }
    };
    napi_property_descriptor desc11[] = {
        { "testDefineProperties", nullptr, TestDefineProperties, nullptr, nullptr, nullptr, napi_default, nullptr }
    };
    napi_define_properties(env, exports, sizeof(desc) / sizeof(desc[0]), desc);
    napi_define_properties(env, exports, sizeof(desc1) / sizeof(desc1[0]), desc1);
    napi_define_properties(env, exports, sizeof(desc2) / sizeof(desc2[0]), desc2);
    napi_define_properties(env, exports, sizeof(desc3) / sizeof(desc3[0]), desc3);
    napi_define_properties(env, exports, sizeof(desc4) / sizeof(desc4[0]), desc4);
    napi_define_properties(env, exports, sizeof(desc5) / sizeof(desc5[0]), desc5);
    napi_define_properties(env, exports, sizeof(desc6) / sizeof(desc6[0]), desc6);
    napi_define_properties(env, exports, sizeof(desc7) / sizeof(desc7[0]), desc7);
    napi_define_properties(env, exports, sizeof(desc8) / sizeof(desc8[0]), desc8);
    napi_define_properties(env, exports, sizeof(desc9) / sizeof(desc9[0]), desc9);
    napi_define_properties(env, exports, sizeof(desc10) / sizeof(desc10[0]), desc10);
    napi_define_properties(env, exports, sizeof(desc11) / sizeof(desc11[0]), desc11);
    napi_property_descriptor descClass[] = {
        { "sayHello", nullptr, SayHello, nullptr, nullptr, nullptr, napi_default, nullptr }
    };
    napi_value myClass;
    napi_define_class(env, "myClass", NAPI_AUTO_LENGTH, MyConstructor, nullptr,
                      sizeof(descClass) / sizeof(descClass[0]), descClass, &myClass);
    napi_set_named_property(env, exports, "myClass", myClass);
    
    return exports;
}
EXTERN_C_END

static napi_module demoModule = {
    .nm_version =1,
    .nm_flags = 0,
    .nm_filename = nullptr,
    .nm_register_func = Init,
    .nm_modname = "entry",
    .nm_priv = ((void*)0),
    .reserved = { 0 },
};

extern "C" __attribute__((constructor)) void RegisterEntryModule(void)
{
    napi_module_register(&demoModule);
}