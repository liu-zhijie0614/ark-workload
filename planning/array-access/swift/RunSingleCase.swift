// func runSingleCases(){
//     let caseType = "Array Access"
//     let benchmark = BenchmarkSuite()

//     benchmark.addTests(caseType,"Array Access - Integer Array", integerArray)
//     benchmark.addTests(caseType,"Array Access - Float Array", floatArray)
//     benchmark.addTests(caseType,"Array Access - Bool Array", boolArray)
//     benchmark.addTests(caseType,"Array Access - String Array", stringArray)
//     benchmark.addTests(caseType,"Array Access - Object Array", objectArray)
//     benchmark.addTests(caseType,"Array Access - fannkuc-redux", runFannkuc_Redux)
//     benchmark.addTests(caseType,"Array Access - repeatFasta1", runFasta1)
//     benchmark.addTests(caseType,"Array Access - repeatFasta2", runFasta2)
//     benchmark.addTests(caseType,"Array Access - repeatFasta3", runFasta3)
//     benchmark.addTests(caseType,"Array Access - mandelbrot", runMandelbrot)
//     benchmark.addTests(caseType,"Array Access - spectralnorm", runSpectralnorm)
//     benchmark.addTests(caseType,"Array Access - threeD Cube", runThreeDCube)
//     benchmark.addTests(caseType,"Array Access - threeDRaytrace", runThreeDRaytrace)
//     benchmark.addTests(caseType,"Array Access - integerArray", integerArray)
//     benchmark.addTests(caseType,"Array Access - floatArray", floatArray)
//     benchmark.addTests(caseType,"Array Access - boolArray", boolArray)
//     benchmark.addTests(caseType,"Array Access - stringArray", stringArray)
//     benchmark.addTests(caseType,"Array Access - objectArray", objectArray)
//     benchmark.runSingleTests()
// }