import Glibc
class Timer {
    private let CLOCK_REALTIME = 0
    private var start_timespec = timespec()
    private var end_timespec = timespec()
    private var time_spec = timespec()
    func start() {
        clock_gettime(Int32(CLOCK_REALTIME),&start_timespec)
    }
    
    func stop() -> Double {
        clock_gettime(Int32(CLOCK_REALTIME),&end_timespec)
        let start_time = Double(start_timespec.tv_sec * 1_000_000 + start_timespec.tv_nsec / 1_000)
        let end_time = Double(end_timespec.tv_sec * 1_000_000 + end_timespec.tv_nsec / 1_000)
        let time = end_time - start_time
        return time / 1_000
    }
    func getTime() -> Double {
        clock_gettime(Int32(CLOCK_REALTIME),&time_spec)
        return Double(time_spec.tv_sec * 1_000_000 + time_spec.tv_nsec / 1_000)
    }
}

var global = 0

class Obj {
    var value: Int = 0
    init(value: Int) {
        self.value = value
    }
}

func GenerateFakeRandomObject() -> [Obj] {
    let resource: [Obj] = [Obj](repeating: Obj(value: Int.random(in: 1..<10)), count: 15)
    return resource
}

func GenerateFakeRandomInteger() -> [Int] {
    let resource: [Int] = [12, 43, 56, 76, 89, 54, 45, 32, 35, 47, 46, 44, 21, 37, 84]
    return resource;
}

var arr : [Int] = GenerateFakeRandomInteger()

/***************** With parameters *****************/
func foo(_ a: Int, _ b: Int, _ c: Int) {
    arr[global] += 1
}
func callFoo(_ f: (Int, Int, Int) -> Void, _ a: Int, _ b: Int, _ c: Int) {
    f(a, b, c)
}

/***************************** Default  parameters *****************************/
func defaultFoo(_ resources: [Int]! = arr, _ i: Int! = 2, _ i3: Int! = 1, _ resourcesLength: Int! = 15) -> Int {
    var i4 = 1;
    if ((resources[i % Int(i3) & (resourcesLength - 1)] & 1) == 0) {
        i4 += 1;
    } else {
        i4 += 2;
    }
    return i4;
}
func callDefaultFoo(_ f: ([Int]?, Int?, Int?, Int?) -> Int,
                    _ resources: [Int]! = arr, _ i: Int! = 2, _ i3: Int! = 1, _ resourcesLength: Int! = 15) -> Int {
    return f(resources,i,i3,resourcesLength)
}
/********************* Different  parameters *********************/
func differentFoo(_ a: Int, _ b: String, _ c: Bool) {
    arr[global] += 1
}
func callDifferentFoo(_ f: (Int, String, Bool) -> Void, _ a: Int, _ b: String, _ c: Bool) {
    f(a, b, c)
}
/************************* Variable  parameters *************************/
func variableFoo(_ a: Int?, _ b: String?, _ c: Bool?) {
    arr[global] += 1
}
func callVariableFoo(f: (Int?, String?, Bool?) -> Void, _ a: Int?, _ b: String?, _ c: Bool?) {
    f(a, b, c)
}
/***************************** ...Args  parameters *****************************/
func argsFoo(_ args: Int...) {
    global += 1
}
func callArgsFoo(_ f: (Int...) -> Void, _ args: Int...) {
    f(args[0], args[1], args[2], args[3], args[4], args[5])
}
// Pointer function call
func RunFunctionPtr() -> Int {
    let count = 10000000
    let timer = Timer()
    global = 0
    timer.start()
    for i in 0..<count {
        callFoo(foo, 1, 2, i)
    }
    let time = timer.stop()
    print("Function Call - RunFunctionPtr:\t"+String(time)+"\tms");
    return Int(time)
}
_ = RunFunctionPtr()
// Pointer function call
func RunDefaultfunctionPtr() -> Int {
    let count = 10000000
    let timer = Timer()
    global = 0
    var i3 = 1
    let resources = GenerateFakeRandomInteger();
    let resourcesLength = resources.count
    timer.start()
    for i in 0..<count {
        i3 = callDefaultFoo(defaultFoo, resources, i, i3, resourcesLength)
    }
    let time = timer.stop()
    print("Function Call - RunDefaultfunctionPtr:\t"+String(time)+"\tms");
    return Int(time)
}
_ = RunDefaultfunctionPtr()
// Pointer function call
func RunDifferentFunctionPtr() -> Int {
    let count = 10000000
    let timer = Timer()
    global = 0
    timer.start()
    for _ in 0..<count {
        callDifferentFoo(differentFoo, 1, "1", true)
    }
    let time = timer.stop()
    print("Function Call - RunDifferentFunctionPtr:\t"+String(time)+"\tms");
    return Int(time)
}
_ = RunDifferentFunctionPtr()
// Pointer function call
func RunVariableFunctionPtr() -> Int {
    let count = 10000000
    let timer = Timer()
    global = 0
    timer.start()
    for _ in 0..<count {
        callVariableFoo(f: variableFoo, 1, "1", true)
    }
    let time = timer.stop()
    print("Function Call - RunVariableFunctionPtr:\t"+String(time)+"\tms");
    return Int(time)
}
_ = RunVariableFunctionPtr()

print("Swift Method Call Is End, global value : \(global)")