class C {
    p1: String;
    p2: number;
    p3: number;
    p4: String;
    p5: number;
    p6: number;
    p7: String;
    p8: number;
    p9: number;
    p10: String;
    p11: number;
    p12: number;
    p13: String;
    p14: number;
    p15: number;
    p16: String;
    p17: number;
    p18: number;
    p19: String;
    p20: number;
    p21: number;
    p22: String;
    p23: number;
    p24: number;
    p25: String;
    p26: number;
    p27: number;
    p28: String;
    p29: number;
    p30: number;
    p31: String;
    p32: number;
    p33: number;
    p34: String;
    p35: number;
    p36: number;
    p37: String;
    p38: number;
    p39: number;
    p40: String;
    p41: number;
    p42: number;
    p43: String;
    p44: number;
    p45: number;
    p46: String;
    p47: number;
    p48: number;
    p49: String;
    p50: number;
    p51: number;
  constructor(i1: String,
              i2: number,
              i3: number,
              i4: String,
              i5: number,
              i6: number,
              i7: String,
              i8: number,
              i9: number,
              i10: String,
              i11: number,
              i12: number,
              i13: String,
              i14: number,
              i15: number,
              i16: String,
              i17: number,
              i18: number,
              i19: String,
              i20: number,
              i21: number,
              i22: String,
              i23: number,
              i24: number,
              i25: String,
              i26: number,
              i27: number,
              i28: String,
              i29: number,
              i30: number,
              i31: String,
              i32: number,
              i33: number,
              i34: String,
              i35: number,
              i36: number,
              i37: String,
              i38: number,
              i39: number,
              i40: String,
              i41: number,
              i42: number,
              i43: String,
              i44: number,
              i45: number,
              i46: String,
              i47: number,
              i48: number,
              i49: String,
              i50: number,
              i51: number) {
	this.p1 = i1;
    this.p2 = i2;
    this.p3 = i3;
    this.p4 = i4;
    this.p5 = i5;
    this.p6 = i6;
    this.p7 = i7;
    this.p8 = i8;
    this.p9 = i9;
    this.p10 = i10;
    this.p11 = i11;
    this.p12 = i12;
    this.p13 = i13;
    this.p14 = i14;
    this.p15 = i15;
    this.p16 = i16;
    this.p17 = i17;
    this.p18 = i18;
    this.p19 = i19;
    this.p20 = i20;
    this.p21 = i21;
    this.p22 = i22;
    this.p23 = i23;
    this.p24 = i24;
    this.p25 = i25;
    this.p26 = i26;
    this.p27 = i27;
    this.p28 = i28;
    this.p29 = i29;
    this.p30 = i30;
    this.p31 = i31;
    this.p32 = i32;
    this.p33 = i33;
    this.p34 = i34;
    this.p35 = i35;
    this.p36 = i36;
    this.p37 = i37;
    this.p38 = i38;
    this.p39 = i39;
    this.p40 = i40;
    this.p41 = i41;
    this.p42 = i42;
    this.p43 = i43;
    this.p44 = i44;
    this.p45 = i45;
    this.p46 = i46;
    this.p47 = i47;
    this.p48 = i48;
    this.p49 = i49;
    this.p50 = i50;
    this.p51 = i51;
  }
}
function foo(obj:C) {
  // write properties
  obj.p1 = "object";
  obj.p2 = 1;
  obj.p3 = 2;
  obj.p4 = "object";
  obj.p5 = 1;
  obj.p6 = 2;
  obj.p7 = "object";
  obj.p8 = 1;
  obj.p9 = 2;
  obj.p10 = "object";
  obj.p11 = 1;
  obj.p12 = 2;
  obj.p13 = "object";
  obj.p14 = 1;
  obj.p15 = 2;
  obj.p16 = "object";
  obj.p17 = 1;
  obj.p18 = 2;
  obj.p19 = "object";
  obj.p20 = 1;
  obj.p21 = 2;
  obj.p22 = "object";
  obj.p23 = 1;
  obj.p24 = 2;
  obj.p25 = "object";
  obj.p26 = 1;
  obj.p27 = 2;
  obj.p28 = "object";
  obj.p29 = 1;
  obj.p30 = 2;
  obj.p31 = "object";
  obj.p32 = 1;
  obj.p33 = 2;
  obj.p34 = "object";
  obj.p35 = 1;
  obj.p36 = 2;
  obj.p37 = "object";
  obj.p38 = 1;
  obj.p39 = 2;
  obj.p40 = "object";
  obj.p41 = 1;
  obj.p42 = 2;
  obj.p43 = "object";
  obj.p44 = 1;
  obj.p45 = 2;
  obj.p46 = "object";
  obj.p47 = 1;
  obj.p48 = 2;
  obj.p49 = "object";
  obj.p50 = 1;
  obj.p51 = 2;
  // read properties
  let a1 = obj.p1;
  let a2 = obj.p2;
  let a3 = obj.p3;
  let a4 = obj.p4;
  let a5 = obj.p5;
  let a6 = obj.p6;
  let a7 = obj.p7;
  let a8 = obj.p8;
  let a9 = obj.p9;
  let a10 = obj.p10;
  let a11 = obj.p11;
  let a12 = obj.p12;
  let a13 = obj.p13;
  let a14 = obj.p14;
  let a15 = obj.p15;
  let a16 = obj.p16;
  let a17 = obj.p17;
  let a18 = obj.p18;
  let a19 = obj.p19;
  let a20 = obj.p20;
  let a21 = obj.p21;
  let a22 = obj.p22;
  let a23 = obj.p23;
  let a24 = obj.p24;
  let a25 = obj.p25;
  let a26 = obj.p26;
  let a27 = obj.p27;
  let a28 = obj.p28;
  let a29 = obj.p29;
  let a30 = obj.p30;
  let a31 = obj.p31;
  let a32 = obj.p32;
  let a33 = obj.p33;
  let a34 = obj.p34;
  let a35 = obj.p35;
  let a36 = obj.p36;
  let a37 = obj.p37;
  let a38 = obj.p38;
  let a39 = obj.p39;
  let a40 = obj.p40;
  let a41 = obj.p41;
  let a42 = obj.p42;
  let a43 = obj.p43;
  let a44 = obj.p44;
  let a45 = obj.p45;
  let a46 = obj.p46;
  let a47 = obj.p47;
  let a48 = obj.p48;
  let a49 = obj.p49;
  let a50 = obj.p50;
  let a51 = obj.p51;
}
function runAcc() {
    let c = new C("hello, 1",
    1,
    1,
    "hello, 2",
    2,
    0,
    "hello, 3",
    3,
    1,
    "hello, 4",
    4,
    0,
    "hello, 5",
    5,
    1,
    "hello, 6",
    6,
    0,
    "hello, 7",
    7,
    1,
    "hello, 8",
    8,
    0,
    "hello, 9",
    9,
    1,
    "hello, 10",
    10,
    0,
    "hello, 11",
    11,
    1,
    "hello, 12",
    12,
    0,
    "hello, 13",
    13,
    1,
    "hello, 14",
    14,
    0,
    "hello, 15",
    15,
    1,
    "hello, 16",
    16,
    0,
    "hello, 17",
    17,
    1);
  var obj;
  for (var i=0;i < 10000;i++) {
    foo(obj=c);
  }
}
runAcc();