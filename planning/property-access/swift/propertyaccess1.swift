
// import Foundation
// let MAXSIZE = 5000
// func setObjectFunc(id: Int, endid: Int) -> Any {
//     if id >= endid {
//         return [
//             "Name": "Person_\(id)",
//             "lastName": "Person_\(id - 1)",
//             "person": nil,
//             "id": id
//         ]
//     }
//     return [
//         "Name": "Person_\(id)",
//         "lastName": "Person_\(id - 1)",
//         "person": setObjectFunc(id: id + 1, endid: endid),
//         "id": id
//     ]
// }
// func findObjectFunc(find_obj: Any, size: Int) -> Any {
//     var obj = find_obj
//     for i in 1...size {
//         obj = (obj as? [String: Any])?["person"] ?? [:]
//     }
//     return obj
// }
// func checkObjectFunc(obj: Any, index: Any) -> Bool {
//     let find_obj = findObjectFunc(find_obj: obj, size: (index as? [String: Any])?["id"] as? Int ?? 0)
//     if "\(((try? JSONSerialization.data(withJSONObject: find_obj))))" != "\(((try? JSONSerialization.data(withJSONObject: index))))" {
//         print("find_obj: \n  \((try? JSONSerialization.data(withJSONObject: find_obj)))")
//         print("\n\nindex: \n  \((try? JSONSerialization.data(withJSONObject: index)))")
//         return false
//     }
//     return true
// }
// func checkObjectIdFunc(obj: Any, id: Int) -> Bool {
//     let find_obj = findObjectFunc(find_obj: obj, size: id) as? [String: Any] ?? [:]
//     if find_obj["id"] as? Int != id {
//         print("find_obj.id: \(find_obj["id"] ?? ""), id: \(id) fail")
//         return false
//     }
//     if find_obj["Name"] as? String != "Person_\(id)" {
//         print("find_obj.Name: \(find_obj["Name"] ?? ""),id: \(id) fail")
//         return false
//     }
//     if find_obj["lastName"] as? String != "Person_\(id - 1)" {
//         print("find_obj.lastName: \(find_obj["lastName"] ?? ""),id: \(id) fail")
//         return false
//     }
//     return true
// }
// func runObjectAttributefunc() {
//     for i in 1...MAXSIZE {
//         let obj: [String: Any] = [
//             "Name": "Person",
//             "lastName": "Person",
//             "person": setObjectFunc(id: 1, endid: i),
//             "id": 0
//         ]
//         let checkId = Int.random(in: 1...i)
//         let check = checkObjectIdFunc(obj: obj, id: checkId)
//         if i == 77 {
//             let find_obj = findObjectFunc(find_obj: obj, size: 77)
//             checkObjectFunc(obj: obj, index: find_obj)
//         }
//         if i == 777 {
//             let objcheck: [String: Any] = [
//                 "Name": "Person_\(i / 2)",
//                 "lastName": "Person_\(i / 2 - 1)",
//                 "person": setObjectFunc(id: i / 2 + 1, endid: i),
//                 "id": i / 2
//             ]
//             checkObjectFunc(obj: obj, index: objcheck)
//         }
//     }
// }
// class Person {
//     var name?: String
//     var lastName?: String
//     var person?: Person
//     var id: Int
//     init(name: String, lastName: String, person?: Person=nil, id: Int) {
//         self.name = name
//         self.lastName = lastName
//         self.person = person!
//         self.id = id
//     }
// }
// func setPersonFunc(id: Int, endid: Int) -> Person {
//     if id == endid {
//         return Person(name: "Person_\(id)", lastName: "Person_\(id-1)", person: nil, id: id)
//     }
//     return Person(name: "Person_\(id)", lastName: "Person_\(id-1)", person: setPersonFunc(id: id+1, endid: endid), id: id)
// }
// func findPersonFunc(findObj: Person, size: Int) -> Person {
//     var obj:Person = findObj
    
//     for _ in 1...size {
//         obj = obj.person!
//     }
//     return obj
// }
// func checkPersonFunc(obj: Person, index: Person) -> Bool {
//     return checkPersonIdFunc(obj: obj, id: index.id)
// }
// func checkPersonIdFunc(obj: Person, id: Int) -> Bool {
//     let findObj = findPersonFunc(findObj: obj, size: id)
//     if findObj.id != id {
//         print("id: \(id) fail")
//         return false
//     }
//     if findObj.name != "Person_\(id)" {
//         print("id: \(id) fail")
//         return false
//     }
//     if findObj.lastName != "Person_\(id-1)" {
//         print("id: \(id) fail")
//         return false
//     }
//     return true
// }
// func runClassAttributefunc() {
//     for i in 1...MAXSIZE {
//         let obj = Person(name: "Person", lastName: "Person", person: setPersonFunc(id: 1, endid: i), id: 0)
//         let checkId = Int.random(in: 1...i)
//         let check = checkPersonIdFunc(obj: obj, id: checkId)
//         if i == 77 {
//             let findClass = findPersonFunc(findObj: obj, size: 77)
//             checkPersonFunc(obj: obj, index: findClass)
//         }
//     }
// }
// func run() {
//     var start = Date().timeIntervalSince1970
//     runObjectAttributefunc()
//     var end = Date().timeIntervalSince1970
//     print("runObjectAttributefunc run time : \((end-start)*1000) ms")
//     start = Date().timeIntervalSince1970
//     runClassAttributefunc()
//     end = Date().timeIntervalSince1970
//     print("runClassAttributefunc run time : \((end-start)*1000) ms")
// }
// /* run() */
