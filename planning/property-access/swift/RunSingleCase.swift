// func runSingleCases(){
//     let caseType = "PropertyAccess"
//     let benchmark = BenchmarkSuite()
//     benchmark.addTests(caseType,"Property Access - polymorphism", polymorphism)
//     benchmark.addTests(caseType,"Property Access - singleICClass", singleICClass)
//     benchmark.addTests(caseType,"Property Access - noneExtension", noneExtension)
//     benchmark.addTests(caseType,"Property Access - getterTest", getterTest)
//     benchmark.addTests(caseType,"Property Access - setterTest", setterTest)
//     benchmark.addTests(caseType,"Property Access - stringObject", stringObject)
//     benchmark.addTests(caseType,"Property Access - numberObject", numberObject)
//     benchmark.addTests(caseType,"Property Access - complexObject", complexObject)
//     benchmark.addTests(caseType,"Property Access - overLoadObject", overLoadObject)
//     benchmark.runSingleTests()
// }