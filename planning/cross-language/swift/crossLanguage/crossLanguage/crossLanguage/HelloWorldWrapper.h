//
//  HelloWorldWrapper.h
//  crossLanguage
//
//  Created by Yao Yuan on 6/4/2023.
//

#import <Foundation/Foundation.h>
#import "CustomObject.h"

@interface HelloWorldWrapper : NSObject
- (void) executeCallBack : (void (*)())fp;
- (NSString *) sayHello;
- (void) benchmarkZero;
- (void) benchmarkOne:(NSInteger)a;
- (void) benchmarkTwo:(NSInteger)a :(NSInteger)b;
- (void) benchmarkThree:(NSInteger)a :(NSInteger)b :(NSInteger)c;
- (void) benchmarkFour:(NSInteger)a :(NSInteger)b :(NSInteger)c :(NSInteger)d;
- (void) benchmarkFive:(NSInteger)a :(NSInteger)b :(NSInteger)c :(NSInteger)d :(NSInteger)e;
- (void) benchmarkSix:(NSInteger)a :(NSInteger)b :(NSInteger)c :(NSInteger)d :(NSInteger)e :(NSInteger)f;
- (void) benchmarkSeven:(NSInteger)a :(NSInteger)b :(NSInteger)c :(NSInteger)d :(NSInteger)e :(NSInteger)f :(NSInteger)g;
- (void) benchmarkEight:(NSInteger)a :(NSInteger)b :(NSInteger)c :(NSInteger)d :(NSInteger)e :(NSInteger)f :(NSInteger)g :(NSInteger)h;
- (void) benchmarkNine:(NSInteger)a :(NSInteger)b :(NSInteger)c :(NSInteger)d :(NSInteger)e :(NSInteger)f :(NSInteger)g :(NSInteger)h :(NSInteger)i;
- (void) benchmarkTen:(NSInteger)a :(NSInteger)b :(NSInteger)c :(NSInteger)d :(NSInteger)e :(NSInteger)f :(NSInteger)g :(NSInteger)h :(NSInteger)i :(NSInteger)j;
- (void) benchmarkObjectOne: (CustomObject* ) obj;
- (void) benchmarkObjectTwo:(CustomObject*) a :(CustomObject*) b;
- (void) benchmarkObjectThree:(CustomObject*) a :(CustomObject*) b :(CustomObject*) c;
- (void) benchmarkObjectFour:(CustomObject*) a :(CustomObject*) b :(CustomObject*) c :(CustomObject*) d;
- (void) benchmarkObjectFive:(CustomObject*) a :(CustomObject*) b :(CustomObject*) c :(CustomObject*) d :(CustomObject*) e;
- (void) benchmarkObjectSix:(CustomObject*) a :(CustomObject*) b :(CustomObject*) c :(CustomObject*) d :(CustomObject*) e :(CustomObject*) f;
- (void) benchmarkObjectSeven:(CustomObject*) a :(CustomObject*) b :(CustomObject*) c :(CustomObject*) d :(CustomObject*) e :(CustomObject*) f :(CustomObject*) g;
- (void) benchmarkObjectEight:(CustomObject*) a :(CustomObject*) b :(CustomObject*) c :(CustomObject*) d :(CustomObject*) e :(CustomObject*) f :(CustomObject*) g :(CustomObject*) h;
- (void) benchmarkObjectNine:(CustomObject*) a :(CustomObject*) b :(CustomObject*) c :(CustomObject*) d :(CustomObject*) e :(CustomObject*) f :(CustomObject*) g :(CustomObject*) h :(CustomObject*) i;
- (void) benchmarkObjectTen:(CustomObject*) a :(CustomObject*) b :(CustomObject*) c :(CustomObject*) d :(CustomObject*) e :(CustomObject*) f :(CustomObject*) g :(CustomObject*) h :(CustomObject*) i :(CustomObject*) j;
- (void) benchmarkSettingValue:(CustomObject*) obj;
- (NSArray *) benchmarkSettingLargeArray:(NSArray *) mutableArray :(NSInteger)size;
@end
