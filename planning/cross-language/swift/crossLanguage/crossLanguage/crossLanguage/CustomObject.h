//
//  CustomObject.h
//  crossLanguage
//
//  Created by Yao Yuan on 18/4/2023.
//

#import <Foundation/Foundation.h>

@interface CustomObject : NSObject

@property NSString *value1;
@property NSString *value2;
@property NSString *value3;

@end
