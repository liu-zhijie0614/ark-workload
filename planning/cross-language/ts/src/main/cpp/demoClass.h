//
// Created on 2023/4/12.
//
// Node APIs are not fully supported. To solve the compilation error of the interface cannot be found,
// please include "napi/native_api.h".

#ifndef crossLanguageBenchmark_demoClass_H
#define crossLanguageBenchmark_demoClass_H

#include "napi/native_api.h"

class DemoClass {
public:
    static void Init(napi_env env, napi_value exports);
    static void Destructor(napi_env env, void* nativeObject, void* finalize_hint);
    
private:
    explicit DemoClass();
    ~DemoClass();
    
    static napi_value Constructor(napi_env env, napi_callback_info info);
    static napi_value SayHello(napi_env env, napi_callback_info info);

    napi_env env_;
    napi_ref wrapper_;
};

#endif //crossLanguageBenchmark_demoClass_H
