// @ts-nocheck
import hilog from '@ohos.hilog';
import testNapi from 'libentry.so'

@Entry
@Component
struct Index {
  @State message: string = 'Hello World'

  build() {
    Row() {
      Column() {
        Text(this.message)
          .fontSize(50)
          .fontWeight(FontWeight.Bold)
          .onClick(() => {
            hilog.info(0x0000, 'testTag', 'Test NAPI 2 + 3 = %{public}d', testNapi.add(2, 3));
            this.simpleCallInteger();
            this.simpleCallObject();
            this.complexCallBack();
            this.complexSettingValue();
            this.complexSettingLargeArrayValue();
            this.complexCppConstructor();
            this.complexCppMethod();
          })
      }
      .width('100%')
    }
    .height('100%')
  }

  forLoopTime(count) {
    var start = Date.now();
    for (var i = 0; i < count; i++) {
    }
    var end = Date.now();
    return (end - start);
  }

  // Minimum cases
  // Aims to test JS -> C++ cost
  // Simply call napi functions with passing 1 to 10 numbers of primitive parameters
  simpleCallInteger() {
    var LOOPCOUNT = 10000;
    var start = Date.now();
    for (var i = 0; i < LOOPCOUNT; i++) {
      testNapi.benchmarkZero();
      testNapi.benchmarkOne(1);
      testNapi.benchmarkTwo(1, 2);
      testNapi.benchmarkThree(1, 2, 3);
      testNapi.benchmarkFour(1, 2, 3, 4);
      testNapi.benchmarkFive(1, 2, 3, 4, 5);
      testNapi.benchmarkSix(1, 2, 3, 4, 5, 6);
      testNapi.benchmarkSeven(1, 2, 3, 4, 5, 6, 7);
      testNapi.benchmarkEight(1, 2, 3, 4, 5, 6, 7, 8);
      testNapi.benchmarkNine(1, 2, 3, 4, 5, 6, 7, 8, 9);
      testNapi.benchmarkTen(1, 2, 3, 4, 5, 6, 7, 8, 9, 10);
    }
    var end = Date.now();
    console.log("Benchmark - TS - simpleCallInteger - running " + LOOPCOUNT + " times is " + (end - start - this.forLoopTime(LOOPCOUNT)) + " ms");
  }

  // Minimum cases
  // Aims to test JS -> C++ cost
  // Simply call napi functions with passing 1 to 10 number of JSObject parameters
  simpleCallObject() {
    var sampleObject = {
      a : '1',
      b : '2',
      c : '3'
    }
    var LOOPCOUNT = 10000;
    var start = Date.now();
    for (var i = 0; i < LOOPCOUNT; i++) {
      testNapi.benchmarkZero();
      testNapi.benchmarkOne(sampleObject);
      testNapi.benchmarkTwo(sampleObject, sampleObject);
      testNapi.benchmarkThree(sampleObject, sampleObject, sampleObject);
      testNapi.benchmarkFour(sampleObject, sampleObject, sampleObject, sampleObject);
      testNapi.benchmarkFive(sampleObject, sampleObject, sampleObject, sampleObject, sampleObject);
      testNapi.benchmarkSix(sampleObject, sampleObject, sampleObject, sampleObject, sampleObject, sampleObject);
      testNapi.benchmarkSeven(sampleObject, sampleObject, sampleObject, sampleObject, sampleObject, sampleObject, sampleObject);
      testNapi.benchmarkEight(sampleObject, sampleObject, sampleObject, sampleObject, sampleObject, sampleObject, sampleObject, sampleObject);
      testNapi.benchmarkNine(sampleObject, sampleObject, sampleObject, sampleObject, sampleObject, sampleObject, sampleObject, sampleObject, sampleObject);
      testNapi.benchmarkTen(sampleObject, sampleObject, sampleObject, sampleObject, sampleObject, sampleObject, sampleObject, sampleObject, sampleObject, sampleObject);
    }
    var end = Date.now();
    console.log("Benchmark - TS - simpleCallObject - running " + LOOPCOUNT + " times is " + (end - start - this.forLoopTime(LOOPCOUNT)) + " ms");
  }

  // Complex callbacks
  // Aims to test JS -> C++ -> JS cost
  // Pass a JS function callback to C++ and execute callback
  complexCallBack() {
    function printHello() {
      // do something
    }
    let obj = {
      print : printHello()
    }
    var LOOPCOUNT = 10000;
    var start = Date.now();
    for (var i = 0; i < LOOPCOUNT; i++) {
      testNapi.benchmarkCallBack(obj);
    }
    var end = Date.now();
    console.log("Benchmark - TS - complexCallBack - running " + LOOPCOUNT + " times is " + (end - start - this.forLoopTime(LOOPCOUNT)) + " ms");
  }

  // Complex Setting Value - light workload - one object
  // Aims to test JS -> C++ -> JS cost
  // Pass a JSObject to C++ and change and return the value to JS
  complexSettingValue() {
    let obj = {
      a : "1",
      b : "2",
      c : "3"
    }
    var LOOPCOUNT = 10000;
    var start = Date.now();
    for (var i = 0; i < LOOPCOUNT; i++) {
      testNapi.benchmarkSetJSValue(obj);
    }
    var end = Date.now();
    console.log("Benchmark - TS - complexSettingValue - running " + LOOPCOUNT + " times is " + (end - start - this.forLoopTime(LOOPCOUNT)) + " ms");
  }

  // Complex Setting Values - heavy workload - large array
  // Aims to test JS -> C++ -> JS cost
  // Pass a JSArray to C++, change values at C++
  complexSettingLargeArrayValue() {
    let size = 5000;
    let largeArray = new Array(size).fill(0);
    var LOOPCOUNT = 10;
    var start = Date.now();
    for (var i = 0; i < LOOPCOUNT; i++) {
      testNapi.benchmarkSetLargeArrayValue(largeArray, size);
    }
    var end = Date.now();
    console.log("Benchmark - TS - complexSettingLargeArrayValue - running " + LOOPCOUNT + " times is " + (end - start - this.forLoopTime(LOOPCOUNT)) + " ms");
  }

  // Complex C++ class
  // Aims to test C++ -> JS cost
  // Pass a C++ constructor to JS and create a new instance at JS layer
  complexCppConstructor() {
    var LOOPCOUNT = 10000;
    var start = Date.now();
    for (var i = 0; i < LOOPCOUNT; i++) {
      let obj = new testNapi.DemoClass();
    }
    var end = Date.now();
    console.log("Benchmark - TS - complexCppConstructor - running " + LOOPCOUNT + " times is " + (end - start - this.forLoopTime(LOOPCOUNT)) + " ms");
  }

  // Complex C++ class
  // Aims to test JS -> C++ methods
  // Create a C++ class instance and call its method
  complexCppMethod() {
    let obj = new testNapi.DemoClass();
    var LOOPCOUNT = 10000;
    var start = Date.now();
    for (var i = 0; i < LOOPCOUNT; i++) {
      obj.sayHello();
    }
    var end = Date.now();
    console.log("Benchmark - TS - complexCppMethod - running " + LOOPCOUNT + " times is " + (end - start - this.forLoopTime(LOOPCOUNT)) + " ms");
  }
}
