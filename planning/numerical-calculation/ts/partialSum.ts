// import { ASSERT_NUMBER_EQ, ASSERT_FLOAT_EQ, ASSERT_TRUE, ASSERT_FALSE, ASSERT_EQ } from "../../../utils/assert";
// import { BenchmarkRunner } from "../../../utils/benchmarkTsSuite";
declare function print(arg:any) : string;

declare interface ArkTools {
	timeInUs(arg:any):number
}


function partial(n: number) {
    let a1 = 0.0;
	let a2 = 0.0;
	let a3 = 0.0;
	let a4 = 0.0;
	let a5 = 0.0;
	let a6 = 0.0;
	let a7 = 0.0;
	let a8 = 0.0;
	let a9 = 0.0;
    let twothirds = 2.0 / 3.0;
    let alt = -1.0;
    let k2 = 0.0;
	let k3 = 0.0;
	let sk = 0.0;
	let ck = 0.0;

    for (var k = 1; k <= n; k++) {
        k2 = k * k;
        k3 = k2 * k;
        sk = Math.sin(k);
        ck = Math.cos(k);
        alt = -alt;

        a1 += Math.pow(twothirds, k - 1);
        a2 += Math.pow(k, -0.5);
        a3 += 1.0 / (k * (k + 1.0));
        a4 += 1.0 / (k3 * sk * sk);
        a5 += 1.0 / (k3 * ck * ck);
        a6 += 1.0 / k;
        a7 += 1.0 / k2;
        a8 += alt / k;
        a9 += alt / (2 * k - 1);
    }

    // NOTE: We don't try to validate anything from pow(),  sin() or cos() because those aren't
    // well-specified in ECMAScript.
    return a6 + a7 + a8 + a9;
}

export function RunPartialSum() {
	let res = 0.0;
	let start = ArkTools.timeInUs();
	for (var i = 1024; i <= 16384; i *= 2) {
		res = partial(i);
	}
	let end = ArkTools.timeInUs();
	// ASSERT_FLOAT_EQ(res, 13.404679312084602);
    let time = (end - start) / 1000
    print("Numerical Calculation - RunPartialSum:\t"+String(time)+"\tms");
	return time;
}
RunPartialSum()
// let runner1 = new BenchmarkRunner("Numerical Calculation - RunPartialSum", RunPartialSum);
// runner1.run();